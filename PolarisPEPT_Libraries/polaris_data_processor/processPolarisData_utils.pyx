#!/usr/bin/python
from __future__ import print_function   # fixes print error in Progress Bar function

"""
Description:
Contains functions that will be used with processPolarisData.py for Compton camera event processing.

NOTES:
- written using python v2.7.14 / updated to also work in python 3.6.2

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "September 04, 2018"
__version__ = "$Revision: 3.3.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import sys, os
import numpy
cimport numpy as np
from math import sin, cos, pi, log, floor
import pandas
import cProfile
import re
import matplotlib
import matplotlib.pyplot as plt
import pylab

from random import randint


#------------------------------------------------------------------
# CONSTANTS
#------------------------------------------------------------------
MeCsq = 0.5109989461  # electron mass in energy units (MeV)


#------------------------------------------------------------------
# FUNCTION/CLASS DEFINITIONS
#------------------------------------------------------------------

#  takes 3D rotation and 3D translation and produces transformation matrix, input is an array
def get_transformation_matrix_array(TM):

    # break up array into individual elements -> units are degrees and mm
    rot_x, rot_y, rot_z = TM[0], TM[1], TM[2]
    trans_x, trans_y, trans_z = TM[3], TM[4], TM[5]

    # convert rotations into radians
    rot_x *= pi/180.0
    rot_y *= pi/180.0
    rot_z *= pi/180.0

    Rx = numpy.matrix([
            [1, 0, 0, 0],
            [0, cos(rot_x), sin(rot_x), 0],
            [0, -sin(rot_x), cos(rot_x), 0],
            [0, 0, 0, 1]
            ]
    )

    Ry = numpy.matrix([
            [cos(rot_y), 0, -sin(rot_y), 0],
            [0, 1, 0, 0],
            [sin(rot_y), 0, cos(rot_y), 0],
            [0, 0, 0, 1]
            ]
    )

    Rz = numpy.matrix([
            [cos(rot_z), sin(rot_z), 0, 0],
            [-sin(rot_z), cos(rot_z), 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
            ]
    )

    Trans = numpy.matrix(
        [
            [1, 0, 0, -trans_x],
            [0, 1, 0, -trans_y],
            [0, 0, 1, -trans_z],
            [0, 0, 0, 1]
        ]
    )

    return Rz*Ry*Rx*Trans


#  takes 3D rotation and 3D translation and produces transformation matrix, input is 6 values
def get_transformation_matrix(rot_x, rot_y, rot_z, trans_x, trans_y, trans_z):
    rot_x *= pi/180.0
    rot_y *= pi/180.0
    rot_z *= pi/180.0

    Rx = numpy.matrix([
            [1, 0, 0, 0],
            [0, cos(rot_x), sin(rot_x), 0],
            [0, -sin(rot_x), cos(rot_x), 0],
            [0, 0, 0, 1]
            ]
    )

    Ry = numpy.matrix([
            [cos(rot_y), 0, -sin(rot_y), 0],
            [0, 1, 0, 0],
            [sin(rot_y), 0, cos(rot_y), 0],
            [0, 0, 0, 1]
            ]
    )

    Rz = numpy.matrix([
            [cos(rot_z), sin(rot_z), 0, 0],
            [-sin(rot_z), cos(rot_z), 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
            ]
    )

    Trans = numpy.matrix(
        [
            [1, 0, 0, -trans_x],
            [0, 1, 0, -trans_y],
            [0, 0, 1, -trans_z],
            [0, 0, 0, 1]
        ]
    )

    return Rz*Ry*Rx*Trans


#  transforms xyz coordinates
def apply_transformation(df, transformation_matrices):

    #  takes df.detector values and turns into a set of possible values, i.e. [0, 1] if two detectors
    detector_numbers = set(df.detector)
    #  error checking - if the number of detector_numbers is greater than the number of transform_matrices, code will break
    assert(len(detector_numbers) <= len(transformation_matrices))

    #  splits df data into individual groups for each detector
    #   - as_matrix() -> Convert the frame to its Numpy-array representation
    #   - also re-ordering events based detector number [destroying ordering of events] <- re-sort by timestamp at the end
    # detector_slices = [df[df['detector'] == n].as_matrix() for n in detector_numbers]     # Depreciated 
    detector_slices = [df[df['detector'] == n].to_numpy() for n in detector_numbers]
    #  "cdef" re-assigns the variable xyz is a cython array in order to gain the speed-up benefit
    cdef np.ndarray[np.float_t, ndim=2] xyz

    #  loop through detector_slices [which have been rearranged by detector number]
    for i, n in enumerate(detector_numbers):
        xyz = detector_slices[i][:, 3:6]
        #xyz = detector_slices[i][:, 4:7]

        #  the POLARIS detectors have left handed Coordinate system (so we first flip y-axis)
        xyz[:,1] *= -1  # flip y-axis
        print ('   - detector {} - converted to right handed coordinates (flip y-axis)'.format(i))
        xyz = xyz.T
        xyz_four_length = numpy.append(xyz, [numpy.ones(xyz.shape[1])], axis=0)

        #  the detector to the reconstruction transformation is defined as the inverse transformation
        M = numpy.linalg.inv(transformation_matrices[n])
        xyz_transformed = numpy.dot(M, xyz_four_length)
        detector_slices[i][:, 3:6] = xyz_transformed[:-1, :].T
        #detector_slices[i][:, 4:7] = xyz_transformed[:-1, :].T
        print ('   - detector {} - successful coordinate transformation!'.format(i))

    #  recombines transformed coordinates back into full dataFrame
    new_df = pandas.DataFrame(numpy.concatenate(detector_slices), columns=df.columns)

    #  re-sort data by time stamp [important for coincidence grouping]
    new_df = new_df.sort_values(by = ['time', 'y'], ascending = [True, True])
    #  not sure if this is important
    new_df = new_df.reset_index(drop = True)

    return new_df


#  returns interaction data (E, X, Y, Z) for the specified number of scatters and detector number
def get_interaction_data(df, energy_sort = True, num_scatters = 2, det_num = None):

    print ('   - Grouping data for detector {} with scatter number {}'.format(det_num, num_scatters))

    #  stores the data with the relevant number of scatters & detector number
    if det_num is not None:
        chosen = df.loc[numpy.isclose(df.scatters, num_scatters) & numpy.isclose(df.detector, det_num)]
    else:
        chosen = df[numpy.isclose(df.scatters, num_scatters)]

    #  sorts the scatter of each event by energy deposited, highest first
    if energy_sort:
        chosen = chosen.sort_values(by = ['time', 'energy'], ascending = [True, False])

    #  re-formatting scatter data
    #   - to_numpy() -> Convert the frame to its Numpy-array representation
    scatters = chosen.to_numpy()
    #  only store columns 2, 3, 4, 5 (i.e. eng, x, y, z)
    scatters = scatters[:, [2, 3, 4, 5]]
    #  FIXED THIS -> only store columns 1, 4, 5, 6 (i.e. eng, x, y, z) <- columns reordered alphabetically by label
    #scatters = scatters[:, [1, 4, 5, 6]]

    #  store scatter (and origin) data as float instead of as object (default), important for calcuation later in code
    scatters = scatters[:, [0, 1, 2, 3]].astype(float)

    #  checks that the final number of event is evenly divisible by the number of scatters <- disabled warning
    if len(scatters) % num_scatters != 0:
        # Change Exception to Warning
        #raise Exception("Wrong number of scatters (%d) for scatters (%d), mod = (%d)" \
        #                % (len(scatters), num_scatters, len(scatters) % num_scatters))
        print ("!!! WARNING: Wrong number of scatters (%d) for scatters (%d), mod = (%d)" % (len(scatters), num_scatters, len(scatters) % num_scatters))

    #  print counts to screen
    print ('    - found {} events out of {}'.format(len(scatters), len(df)))

    return scatters


#  rescales energy column from keV to MeV
def scale_energy(scatters):
    '''
    Args:
        scatters in 2D array with rows energy (keV), x, y, z

    Returns:
        scatters in 2D array with rows energy (MeV), x, y, z
    '''
    if numpy.average(scatters[:,0]) > 10:
        scatters[:,0] = scatters[:,0] * 0.001

    return scatters


#  re-arranges scatter data from consecutive lines to all Compton scatter data on a single line for CSV output / also re-scales energy
def format_data_for_output(scatters, num_scatters, det_num = None):

    print ('   - Formatting {} events from detector {} and number of scatters: {}'.format(len(scatters), det_num, num_scatters))

    #  check if any data in scatters array
    if scatters.shape[0] == 0:
        return scatters

    #  re-ordering interactions [CURRENTLY EMPTY FUNCTION]
    #scatters = put_interactions_in_order(scatters, num_scatters)
    #  converting energy values from keV into MeV
    scatters = scale_energy(scatters)

    #  re-arranges double scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2 format
    if num_scatters == 2:
        #  moves next event (i + 1) to the same line as current event
        temp = numpy.concatenate((scatters[0:-1], scatters[1:]), axis = 1)
        #  stores data from every other line
        out = temp[0::2]

    #  re-arranges triple scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2, E3, X3, Y3, Z3 format
    if num_scatters == 3:
        #  moves next two events (i + 1 & i + 2) to the same line as current event
        temp = numpy.concatenate((scatters[0:-2], scatters[1:-1], scatters[2:]), axis = 1)
        #  stores data from every third line
        out = temp[0::3]

    return out


#  plots 2D histogram
def plot_2D(np.ndarray[np.float_t, ndim=1] x, x_label, np.ndarray[np.float_t, ndim=1] y, y_label, title, output_folder=".", is_log=False):

    print ('  Creating 2D Plot - {} v {} with title: {} . . .'.format(x_label, y_label, title))
    plt.clf()
    ax = plt.subplot(111)
    ax.set_title(title)

    nbins = int(max(max(x) - min(x), max(y) - min(y)))
    if nbins < 1: nbins = 1

    H, xedges, yedges = numpy.histogram2d(y, x, bins=(nbins, nbins))
    if(is_log): H = numpy.log(H)
    im = plt.imshow(H, interpolation='none', origin='lower', extent=[min(yedges), max(yedges), min(xedges), max(xedges)])

    plt.title(title + ', total = {}'.format(len(x)), fontsize = 18)
    ax.xaxis.set_tick_params(labeltop = 'off')
    ax.xaxis.set_tick_params(labelbottom = 'on')

    ax.grid(True,linestyle='-',which='both', color='0.50')

    plt.xlabel(x_label, fontsize = 18)
    plt.ylabel(y_label, fontsize = 18)
    ax.xaxis.set_label_position('bottom')
    ax.tick_params(axis='both', bottom = 'on', top = 'off')

    plot_name = "%s/%s_%s_%s.png" % (os.path.abspath(output_folder), title.replace(":", "_").replace(" ", "_"), x_label, y_label)
    plt.savefig(plot_name)

    return plt


#  manually bins data
def bin_data(np.ndarray[np.float_t, ndim=1] x, int num_bins):

    cdef float range_min = min(x)
    cdef float range_max = max(x) + 1.0E-2 # make bin edge bigger than largest x.
    cdef float range_length = range_max - range_min
    print ('    - plot details: range_max = {}, range_min = {}, range_length = {}'.format(range_max, range_min, range_length))
    if (range_length * range_length < 1.0E-6):
        print ("ERROR: Range length is 0 (min=%.3e, max=%.3e, range=%.3e) . .. " % (range_min, range_max, range_length))

    cdef float step_factor = float(num_bins - 1)/range_length

    cdef np.ndarray[np.float_t, ndim=1] xshifted = x - range_min
    cdef np.ndarray[np.float_t, ndim=1] x_bin_number = xshifted * step_factor

    bins = numpy.linspace(0, num_bins - 1, num_bins)
    hist_dict = dict(zip(bins, numpy.zeros(len(bins))))
    hist_bin_values = bins

    def index_dict(int key):
        hist_dict[key] += 1

    map(index_dict, x_bin_number)

    cdef np.ndarray[np.float_t, ndim=1] x_vals = bins * range_length/num_bins + range_min
    cdef np.ndarray[np.float_t, ndim=1] y_vals = numpy.array([float(hist_dict[key]) for key in bins])

    return x_vals, y_vals


#  plots 1D profiles, no longer uses manually binned data
def plot_1D(x, num_bins, x_label, y_label, title, output_folder = ".", isLog = False):
    '''
    The builtin python histogram function choke if the number of values gets too large.
    This function has a built in work around. It handles the binning and counting itself
    and then plots an x, y line plot.
    '''

    print ('  Creating 1D Plot - {} v {} with title: {} . . .'.format(x_label, y_label, title))
    plt.clf()
    plt.gcf().set_size_inches(8, 8)
    ax = plt.subplot(111)

    try:
        x = x.to_numpy()
    except AttributeError:
        True

    # not using this (doesn't seem to work in python3)
    """
    x_vals, y_vals = bin_data(x, num_bins)
    if (isLog): plt.hist(x_vals, num_bins, weights = y_vals, log = True)
    #else: plt.hist(x_vals, num_bins, weights = y_vals)
    """
    # using this instead
    if (isLog): plt.hist(x, num_bins, log = True)
    else: plt.hist(x, num_bins)

    ax.set_title(title + ', total = {}'.format(len(x)), fontsize = 18)
    plt.xlabel(x_label, fontsize = 18)
    plt.ylabel(y_label, fontsize = 18)

    ax.grid(True, linestyle='-', which='both', color='0.750')

    if (isLog): plot_name = "%s/%s_%s_%s_log.png" % (os.path.abspath(output_folder), title.replace(":", "_").replace(" ", "_"), x_label, y_label)
    else: plot_name = "%s/%s_%s_%s.png" % (os.path.abspath(output_folder), title.replace(":", "_").replace(" ", "_"), x_label, y_label)
    plt.savefig(plot_name)

    return plt


#  function to create series of plots from basic detector data
def make_basic_detector_plots(df, moniker, output_folder):

    # check for data in array (if array is full, will return True)
    if not df.empty:

        #  creates list of unique detector indices
        #    pandas.unique - returns unique values of the Series object and are returned in order of appearance
        detectors = pandas.unique(df.detector)
        print ('  set of detectors: {}'.format(detectors))

        #  loop through each detector
        for detector in detectors:
            #  pull out data for each detector
            df_detector = df[df.detector == detector]
            print ('    - detector {} / label: {}'.format(detector, moniker))
            #  plot 1D profiles for each detector
            plot_1D(df_detector.x.as_matrix(), 200, "x", "Counts", "%s_D%d" % (moniker, detector), output_folder)
            plot_1D(df_detector.y.as_matrix(), 200, "y", "Counts", "%s_D%d" % (moniker, detector), output_folder)
            plot_1D(df_detector.z.as_matrix(), 200, "z", "Counts", "%s_D%d" % (moniker, detector), output_folder)
            #  plot 2D profiles for each detector
            plot_2D(df_detector.y.as_matrix(), "y", df_detector.x.as_matrix(), "x", "%s_D%d" % (moniker, detector), output_folder, True)
            plot_2D(df_detector.y.as_matrix(), "y", df_detector.z.as_matrix(), "z", "%s_D%d" % (moniker, detector), output_folder, True)
            plot_2D(df_detector.z.as_matrix(), "z", df_detector.x.as_matrix(), "x", "%s_D%d" % (moniker, detector), output_folder, True)

        #  plot energy for all detectors
        plot_1D(df.energy.as_matrix(), 200, "Energy", "Counts", "%s" % moniker, output_folder, False)
        #  plot energy for all detectors (log)
        plot_1D(df.energy.as_matrix(), 200, "Energy", "Counts", "%s" % moniker, output_folder, True)

#  function used by filtering_unphysical_double_scatters() to find unphysical events
#   - calculate the inner product of Compton equation to find theta1 from E0 and E1
def physical_energy_ordering_double(E1, E2):

    #  check if energy ordering of double scatter event produces physical scatter angle
    E0 = E1 + E2

    if numpy.abs(1 + MeCsq * ( 1.0/(E0) - 1.0/(E0 - E1) ) ) < 1:
        return 1
    else:
        return 0

#  checks for unphysical events (theta1 = nan) from double scatter event data
def filtering_unphysical_double_scatters(scatters):
    #   - input format (scatters): eng1, x1, y1, z1, eng2, x2, y2, z2
    #   - returns data in same format

    scatters_physical = []
    count_both, count_one, count_flip, count_none = [0] * 4

    #  loop through list of events
    for index in range( len(scatters) ):

        #  check original energy ordering
        order1 = physical_energy_ordering_double(scatters[index][0], scatters[index][4])

        #  check flipped energy ordering
        order2 = physical_energy_ordering_double(scatters[index][4], scatters[index][0])

        #  storing appropriate events into final output array: scatters_physical
        if (order1 == 1 & order2 == 1):
            scatters_physical.append(scatters[index])
            count_both += 1
        elif (order1 == 1):
            scatters_physical.append(scatters[index])
            count_one += 1
        elif (order2 == 1):
            SE = scatters[index]
            flipped_scatter = numpy.array((SE[4], SE[5], SE[6], SE[7], SE[0], SE[1], SE[2], SE[3]))
            scatters_physical.append(flipped_scatter)
            count_one += 1; count_flip += 1
        else:
            count_none += 1

    #  print counts to screen
    print ('    - results of event ordering -> number of physical events returned: {} | both work: {} | only one order works: {} | order flipped: {} | neither work: {}'.format(len(scatters_physical), count_both, count_one, count_flip, count_none))

    #  return physical events
    #    vstack takes list of numpy arrays and converts into single numpy array
    return numpy.vstack(scatters_physical)

#  function used by compton_line_filtering() to calculate the energy of the first scatter
def calculate_expected_first_scatter_energy(E0, theta):

    #  re-arrangement for Compton scatter equation to solve for E1
    alpha = E0 / MeCsq
    beta = alpha * ( 1 - numpy.cos(theta) )
    return E0 * beta / ( 1 + beta )

#  filtering events using Compton Line Filtering (based on expected gamma energies, input variable: CL_energies)
def compton_line_filtering(scatters, CL_range, CL_energies):
    #   - input format (scatters): eng1, x1, y1, z1, eng2, x2, y2, z2
    #   - returns data in same format

    scatters_filtered = []
    count_compton = [0] * len(CL_energies)

    #  loop through list of events
    for index in range( len(scatters) ):

        #  use the first and second energies to calculate E0 and theta1
        E1 = scatters[index][0]; E2 = scatters[index][4]
        E0 = E1 + E2
        theta1 = numpy.arccos( 1 + MeCsq * ( 1.0/(E0) - 1.0/(E0-E1) ) );

        #  looping through expected gamma energies
        for i, e in enumerate(CL_energies):
            #  calculating range of Compton values for given total energy and first scatter angle
            minE1 = CL_range[0] * calculate_expected_first_scatter_energy(e, theta1)
            maxE1 = CL_range[1] * calculate_expected_first_scatter_energy(e, theta1)

            #  check if energy falls within expected range
            if E1 > minE1 and E1 < maxE1:

                #  add filtered data to output array: scatters_filtered
                scatters_filtered.append(scatters[index])
                count_compton[i] += 1

    #  total up number of Compton filtered events
    total_compton = numpy.sum(count_compton)

    #  print results to screen
    print ('    - results of Compton filtering -> number of filtered events returned: {} -> counts: {} by energy {}, respectively'.format(total_compton, count_compton, CL_energies))

    #  return filtered events
    #    vstack takes list of numpy arrays and converts into single numpy array
    return numpy.vstack(scatters_filtered)

#
def create_plot_range(plot_data, x_label, nb_bins, rng_min, rng_max):
    plt.hist(plot_data, bins=nb_bins, range=[rng_min,rng_max], alpha = 1.0)
    plt.xlabel(x_label);
    plt.ylabel('Number of Gammas');

#
def create_plot_bins(plot_data, x_label, nb_bins):
    plt.hist(plot_data, bins=nb_bins)
    plt.xlabel(x_label);
    plt.ylabel('Number of Gammas');

# source: https://tomspur.blogspot.co.za/2015/08/publication-ready-figures-with.html
def save_to_file(filename, dir, fig=None):
    """Save to @filename with a custom set of file formats.

    By default, this function takes to most recent figure,
    but a @fig can also be passed to this function as an argument.
    """
    formats = [
                #"pdf",
                #"eps",
                "png",
                #"svg",
                #"pgf",   # ERROR: LatexError: LaTeX returned an error, probably missing font or error in preamble:
              ]
    if fig is None:
        for form in formats:
            plt.savefig("%s/%s.%s"%(dir, filename, form), dpi=300)
    else:
        for form in formats:
            fig.savefig("%s/%s.%s"%(dir, filename, form), dpi=300)

#  code from paul maggi [modified only in formatting]
def coincCheckMod(timeStamps, ene, modList, xList, yList, zList, cutOffClk, peakMin, peakMax):
    '''
    deltaT, mod1, mod2, doubs = coincCheckMod(timeStamps, ene, modList, xList, yList, zList, cutOffClk, peakMin, peakMax)

    this function should only be passed events that are single pixel events, e.g. ene[npx==1]. For each interaction, it looks up to cutOffClk beyond that point
	for a combination of events that is within the energy windows specified by peakMin and peakMax.

    input:
        timeStamps, ene, modList, xList, yList, zList:
            these are the t, edep, mdl, x, y, and z array from the output of readInC, after only selecting one pixel events
        cutOffClk:
            the number of 10 ns clock cycles beyond a point to look for coincidence events.
				example:
					cutOffSec = 1E-6 %this is a 1 us time window
					cutOffClk = cutOffSec/(10E-9) %this gives a value of 100
    output:
        deltaT:
			time difference (in clk cycles) between the two accepted coincidence points
		mod1, mod2:
			module numbers of the first and second interactions of a coincidence pair, respectively
		doubs:
			list of doubles in the standard format (edep1, x1, y1, z1, edep2, x2, y2, z2)

    '''
    # note: maxPts is the max number of coincidence Singles->doubles this code looks for.
    maxPts = 500000

    deltaT = numpy.zeros((maxPts,))
    mod1 = numpy.zeros((maxPts,))
    mod2 = numpy.zeros((maxPts,))
    yList1 = numpy.zeros((maxPts,))
    yList2 = numpy.zeros((maxPts,))
    ed1 = numpy.zeros((maxPts,))
    ed2 = numpy.zeros((maxPts,))
    xList1 = numpy.zeros((maxPts,))
    xList2 = numpy.zeros((maxPts,))
    zList1 = numpy.zeros((maxPts,))
    zList2 = numpy.zeros((maxPts,))
    jjj = 0
    timeDiff = timeStamps[1:] - timeStamps[0:-1]

    for iii in range(len(timeDiff) - 1):
        checkNum = 1
        buffTime = timeDiff[iii]
        if ene[iii] == 0:
            break
        while (buffTime <= cutOffClk) & ((checkNum + iii) < len(timeStamps) - 3):
            eneSum = ene[iii] + ene[checkNum + iii]
            if (eneSum <= peakMax) & (eneSum >= peakMin):
                deltaT[jjj] = buffTime
                mod1[jjj] = modList[iii] + 1
                mod2[jjj] = modList[checkNum + iii] + 1
                ed1[jjj] = ene[iii]
                ed2[jjj] = ene[iii + checkNum]
                yList1[jjj] = yList[iii]
                yList2[jjj] = yList[checkNum + iii]
                xList1[jjj] = xList[iii]
                xList2[jjj] = xList[checkNum + iii]
                zList1[jjj] = zList[iii]
                zList2[jjj] = zList[checkNum + iii]
                jjj += 1

            checkNum = checkNum + 1
            buffTime = buffTime + timeDiff[iii + checkNum]

        if jjj > maxPts - 2:

            break
    mod1 = numpy.trim_zeros(mod1) - 1
    mod2 = numpy.trim_zeros(mod2) - 1
    doubs = numpy.array((numpy.trim_zeros(ed1), numpy.trim_zeros(xList1), numpy.trim_zeros(yList1), numpy.trim_zeros(zList1), numpy.trim_zeros(ed2), numpy.trim_zeros(xList2), numpy.trim_zeros(yList2), numpy.trim_zeros(zList2))).T

    return numpy.trim_zeros(deltaT), mod1, mod2, doubs

#  updated code to check PET coincidences [modifed from original code by paul maggi]
def coincCheckPET(timeStamps, ene, modList, xList, yList, zList, cutOffClk, peakMin, peakMax):
    '''
    deltaT, mod1, mod2, doubs, eng1, eng2 = coincCheckPET(timeStamps, ene, modList, xList, yList, zList, cutOffClk, peakMin, peakMax)

    this function should only be passed events that are single pixel events, e.g. ene[npx==1]. For each interaction, it looks up to cutOffClk beyond that point
	for a combination of events that are both within the energy window specified by peakMin and peakMax.

    input:
        timeStamps, ene, modList, xList, yList, zList:
            these are the t, edep, mdl, x, y, and z array from the output of readInC, after only selecting one pixel events
        cutOffClk:
            the number of 10 ns clock cycles beyond a point to look for coincidence events.
				example:
					cutOffSec = 1E-6 %this is a 1 us time window
					cutOffClk = cutOffSec/(10E-9) %this gives a value of 100
    output:
        deltaT:
			time difference (in clk cycles) between the two accepted coincidence points
		mod1, mod2:
			module numbers of the first and second interactions of a coincidence pair, respectively
		doubs:
			list of doubles in the standard format (time1, x1, y1, z1, time2, x2, y2, z2)
		eng1, eng2:
			energy deposited in the first and second interactions of a coincidence pair, respectively

    '''
    #  note: maxPts is the max number of coincidence Singles->doubles this code looks for.
    maxPts = 100000

    print ('  -- Length of timeStamps = {} / maxPts = {}'.format(len(timeStamps), maxPts))

    deltaT = numpy.zeros((maxPts,))
    mod1 = numpy.zeros((maxPts,))
    mod2 = numpy.zeros((maxPts,))
    yList1 = numpy.zeros((maxPts,))
    yList2 = numpy.zeros((maxPts,))
    ed1 = numpy.zeros((maxPts,))
    ed2 = numpy.zeros((maxPts,))
    t1 = numpy.zeros((maxPts,))
    t2 = numpy.zeros((maxPts,))
    xList1 = numpy.zeros((maxPts,))
    xList2 = numpy.zeros((maxPts,))
    zList1 = numpy.zeros((maxPts,))
    zList2 = numpy.zeros((maxPts,))
    jjj = 0

    #  timeDiff is the next time step minus the current time stamp [always positive]
    timeDiff = timeStamps[1:] - timeStamps[0:-1]

    #  loop through all of the time steps
    for iii in range(len(timeDiff) - 1):
        checkNum = 1
        buffTime = timeDiff[iii]

        #  kills loop if deposited energy is zero
        if ene[iii] == 0:
            break

        #  loops successive time steps (using checkNum) until outside cutOff time (cutOffClk)
        while (buffTime <= cutOffClk) & ((checkNum + iii) < len(timeStamps) - 3):

            #  saves a data point if the energy value for both time steps (iii & checkNum + iii) are within energy window
            if (ene[iii] <= peakMax) & (ene[iii] >= peakMin) & (ene[checkNum + iii] <= peakMax) & (ene[checkNum + iii] >= peakMin):

                deltaT[jjj] = buffTime
                mod1[jjj] = modList[iii] + 1
                mod2[jjj] = modList[checkNum + iii] + 1
                ed1[jjj] = ene[iii]
                ed2[jjj] = ene[iii + checkNum]
                # convert time into microseconds
                t1[jjj] = timeStamps[iii] * (10E-9) / (1E-6)
                t2[jjj] = timeStamps[iii + checkNum] * (10E-9) / (1E-6)
                yList1[jjj] = yList[iii]
                yList2[jjj] = yList[checkNum + iii]
                xList1[jjj] = xList[iii]
                xList2[jjj] = xList[checkNum + iii]
                zList1[jjj] = zList[iii]
                zList2[jjj] = zList[checkNum + iii]
                jjj += 1

            #  increment checkNum and time difference (buffTime)
            checkNum = checkNum + 1
            buffTime = buffTime + timeDiff[iii + checkNum]

        #  kill loop if max number reached
        if jjj > maxPts - 2:
            break

    #  clean up output arrays (remove zeros from the end)
    mod1 = numpy.trim_zeros(mod1) - 1
    mod2 = numpy.trim_zeros(mod2) - 1
    doubs = numpy.array((numpy.trim_zeros(t1), numpy.trim_zeros(xList1), numpy.trim_zeros(yList1), numpy.trim_zeros(zList1), numpy.trim_zeros(t2), numpy.trim_zeros(xList2), numpy.trim_zeros(yList2), numpy.trim_zeros(zList2))).T
    eng1 = numpy.trim_zeros(ed1)
    eng2 = numpy.trim_zeros(ed2)

    return numpy.trim_zeros(deltaT), mod1, mod2, doubs, eng1, eng2

#  filtering raw data to produce list of back to back coincident 511 keV gammas
#   - function pulls timing/energy settings from main program
def grouping_backtoback_coincidence_events(df, timing, tWindow, eWindow):

    #  converting data from pandas to numpy arrays (not really necessary)
    npx = numpy.array(df['scatters'])
    mdl = numpy.array(df['detector'])
    edep = numpy.array(df['energy'])
    x = numpy.array(df['x'])
    y = numpy.array(df['y'])
    z = numpy.array(df['z'])
    t = numpy.array(df['time'])

    #  calculating number of 10 ns clock cycles for grouping (based on timing setting)
    number_of_clock_cycles = timing/(10E-9)
    print ('   - Using timing window of {} s, thus looking at {} 10 ns clock cycles for coincidences'.format(timing, number_of_clock_cycles))

    #  convert time into seconds (t given in units of 10 ns clock cycles), so tS = t * 10E-9
    tS = t * 10E-9
    tMin = numpy.amin(tWindow[0])
    tMax = numpy.amax(tWindow[1])

    #  filtering out single pixel events (npx == 1) within time window
    checkInds = numpy.logical_and(npx == 1, tS > tMin)
    checkInds = numpy.logical_and(checkInds, tS < tMax)
    #  print time filtered data  to screen
    numFiltered = numpy.sum(checkInds)
    perFiltered = numpy.sum(checkInds) / float(npx.size)
    print ('    - data filter: single pixel events within time window: {0} to {1} s / number of filtered events: {2} ({3:.2f}%)'.format(tMin, tMax, numFiltered, perFiltered * 100.0))

    #  setting energy window (in keV)
    engMin = eWindow[0] * 1000.0
    engMax = eWindow[1] * 1000.0
    #  print energy filtered data  to screen
    engFiltered = numpy.sum(numpy.logical_and(edep > engMin, edep < engMax))
    print ('    - number of events between {} and {} kev is {}'.format(engMin, engMax, engFiltered))

    #  grouping singles into PET coincidences
    #   - looks at single pixel events in different detectors with the given time window with each gamma having a value within the given energy window
    deltaTM, modL1, modL2, time_position_doubles, E1, E2 = coincCheckPET(t[checkInds], edep[checkInds], mdl[checkInds], x[checkInds], y[checkInds], z[checkInds], number_of_clock_cycles, engMin, engMax)

    #  print counts to screen
    print ('    - results -> number of coincidence events returned: {}'.format(deltaTM.size))

    #  return two arrays
    #   - time_position_doubles: time1, x1, y1, z1, time2, x2, y2, z2
    #   - coincidence_data: deltaT, module1, module2, eng1, eng2
    return time_position_doubles, numpy.array( (deltaTM, modL1, modL2, E1, E2) )


# Code added by Nicholas Hyslop for PolarisPEPT coincidence processing
# --------------------------------------------------------------------

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s'%(prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Determine the rate of random coincidences. Returns rate and coincidences per event
def find_randoms_rate(allevents_data, t_window):
    '''
    Takes in a pandas dataframe of the energy-filtered data and determines the
    rate of random coincidences based on given timing window.

    @params:
        allevents_data      - Required : Pandas dataframe of polaris data that's already been energy filtered (DataFrame)
        t_window            - Required : Timing window in nanoseconds to look for coincidences within (Int)

    @returns:
        tot_rand_coins          - Total number of random coincidences found (Int)
        tot_rands_per_event     - Average number of coincidences found per event, for the events that have at least one coincidence (Float)
    '''

    print('Determining randoms rate with timing window', t_window, 'nanoseconds...')
    randoms_t_window = 2e5          # time ahead of current event to search for randoms (in units of 10ns)
    l = len(allevents_data.index)   # length of the events array for progress bar
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    count = 0               # variable for counting rows
    sum_j = 0               # sum of all coincidences
    tot_j = 0               # total number of events that have coincidences
    for row in allevents_data.itertuples():     # loop through each event in the raw data
        if count % int(l/100) == 0:
            printProgressBar(count + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

        i = 1       # for accessing subsequent events
        j = 0       # for counting number of coincidences per event

        # Break loop if reached end of the data file
        if (count + i) >= len(allevents_data.index):
            break

        next_event = allevents_data.iloc[count + i]             # get subsequent event

        # events_in_timing_window = 0
        # events_to_timing_window = 0

        # Check for all coincidence data later than the current event

        # print(row.time)
        # print(next_event['time'])
        # print(row.time + (randoms_t_window + t_window/10.0))
        # print(row.time + (randoms_t_window - t_window/10.0))

        while next_event['time'] <= (row.time + (randoms_t_window + t_window/10.0)):

            # events_to_timing_window += 1

            # Only check events within the randoms timing window
            # if next_event['time'] <= (row.time + (randoms_t_window - t_window/10.0)):
            if next_event['time'] <= (row.time + randoms_t_window):
                i += 1      # increment event count

                # Break loop if reached end of the data file
                if (count + i) >= len(allevents_data.index):
                    break

                next_event = allevents_data.iloc[count + i]             # get subsequent event
                continue

            # events_in_timing_window += 1

            # Reject coincidences within the same detector
            if next_event['detector'] == row.detector:
                i += 1      # increment coincidence count

                # Break loop if reached end of the data file
                if (count + i) >= len(allevents_data.index):
                    break

                next_event = allevents_data.iloc[count + i]             # get subsequent event
                continue

            # If this line is reached, a coincidence was found #

            j += 1      # increment coincidence count
            i += 1      # increment next event counter

            # Break loop if reached end of the data file
            if (count + i) >= len(allevents_data.index):
                break

            next_event = allevents_data.iloc[count + i]             # get subsequent event

        # print('Events to timing window: ', events_to_timing_window)
        # print('Events within timing window: ', events_in_timing_window)
        # For calculating running average of coindicidences per event
        if j > 0:   # if at least one coincidence was found
            sum_j += j        # increment sum
            tot_j += 1        # increment total

        count += 1      # increment counter

    tot_rand_coins = sum_j                  # total number of random coincidences
    tot_rands_per_event = sum_j/tot_j       # total number fo random coincidences per event
    printProgressBar(count + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    print()
    print('Done.')
    print()

    return tot_rand_coins, tot_rands_per_event

# Takes in the raw, energy-filtered polaris data and finds the coincidences for PEPT processing
def find_coincidences(allevents_data, t_window):
    '''
    Takes in a pandas dataframe of the energy-filtered data and finds all the
    coincidences, storing them in a pandas dataframe, returning the dataframe
    and other information related to the coincidence processing.

    @params:
        allevents_data      - Required : Pandas dataframe of polaris data that's already been energy filtered (DataFrame)
        t_window            - Required : Timing window in nanoseconds to look for coincidences within (Int)

    @returns:
        coincidence_data        - Pandas DataFrame containing the coincidences found (DataFrame)
        tot_coins               - Total number of coincidences found (Int)
        tot_coins_per_event     - Average number of coincidences found per event, for the events that have at least one coincidence (Float)
    '''

    l = len(allevents_data.index)   # length of the events array for progress bar
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    coin_dict_list = []     # create a list of dictionaries to append coincdence data rows to. Faster than appending to a DataFrame.
    count = 0               # variable for counting rows
    sum_j = 0               # sum of all coincidences per event
    tot_j = 0               # total number of events that have coincidences
    for row in allevents_data.itertuples():     # loop through each event in the energy-filtered data
        if count % int(l/100) == 0:
            printProgressBar(count + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

        i = 1       # for accessing subsequent events
        j = 0       # for counting number of coincidences per event

        # Break loop if reached end of the data file
        if (count + i) >= len(allevents_data.index):
            break

        next_event = allevents_data.iloc[count + i]             # get subsequent event

        # Check for all coincidence data later than the current event
        while next_event['time'] <= (row.time + t_window/10.0):

            # Reject coincidences within the same detector
            if next_event['detector'] == row.detector:
                i += 1      # increment coincidence count

                # Break loop if reached end of the data file
                if (count + i) >= len(allevents_data.index):
                    break

                next_event = allevents_data.iloc[count + i]             # get subsequent event
                continue

            # Add coincidence data to dictionary list
            coin_dict_list.append({'time_1':row.time, 'x_1':row.x, 'y_1':row.y, 'z_1':row.z,
                    'time_2':next_event['time'], 'x_2':next_event['x'], 'y_2':next_event['y'], 'z_2':next_event['z']})
            j += 1      # increment coincidence count
            i += 1      # increment next event counter

            # Break loop if reached end of the data file
            if (count + i) >= len(allevents_data.index):
                break

            next_event = allevents_data.iloc[count + i]             # get subsequent event

        # For calculating running average of coindicidences per event
        if j > 0:   # if at least one coincidence was found
            sum_j += j        # increment sum
            tot_j += 1        # increment total

        count += 1      # increment counter

    printProgressBar(count + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    print()
    print('Done. Converting to a pandas DataFrame...')
    coincidence_data = pandas.DataFrame(coin_dict_list, columns = ['time_1', 'x_1', 'y_1', 'z_1', 'time_2', 'x_2', 'y_2', 'z_2'])
    print()

    tot_coins = sum_j
    tot_coins_per_event = sum_j/tot_j
    return coincidence_data, tot_coins, tot_coins_per_event

# Takes in the raw, energy-filtered polaris data and finds the coincidences for PEPT processing
def find_coincidences2(events, coin_window_, single_counting = True, choose_random = True, choose_all = False):
    '''
    Takes in a pandas dataframe of the energy-filtered data and finds all the
    coincidences, storing them in a pandas dataframe, returning the dataframe
    and other information related to the coincidence processing.

    events array is expected in the following format:

     detector | x | y | z | time | counted
    ----------|---|---|---|------|--------

    coincidence_data is returned in the following format:

    time_1 | x_1 | y_1 | z_1 | time_2 | x_2 | y_2 | z_2
    -------|-----|-----|-----|--------|-----|-----|-----

    @params:
        allevents           - Required : Numpy array of polaris data that's already been energy filtered (DataFrame)
        coin_window_        - Required : Timing window in 10s of nanoseconds to look for coincidences within (Int)
        single_counting     - Optional : Flag events to prevent double counting the same events (Bool, default = True)
        choose_random       - Optional : Whether to choose coincidences randomly, or choose the event closest to current (Bool, default = True)
        choose_all          - Optional : Whether to add all of the events found in coincidence, or pick one and discard the rest (Bool, default = False)

    @returns:
        coincidence_data        - Pandas DataFrame containing the coincidences found (DataFrame)
        tot_coins               - Total number of coincidences found (Int)
        avg_coins_per_event     - Average number of coincidences found per event, for the events that have at least one coincidence (Float)
    '''

    # Define c types for looping
    cdef:
        unsigned long tot_coins = 0
        unsigned long coins_per_event = 0
        int i
        int j
        int coin_window
        long start_time


    l = len(events)   # length of the events array for progress bar
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    coin_dict_list = []     # create a list of dictionaries to append coincdence data rows to. Faster than appending to a DataFrame.

    try:
        coin_window = coin_window_/2
        start_time = events[0][4]

        # Loop through all events in array
        for i in range(len(events)):

            if i % int(l/100) == 0:
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # Check we're not going to have an IndexError when iterating backwards
            if (events[i][4] - coin_window) <= start_time:
                continue

            # Check if the event has already been counted
            if single_counting and events[i][5]:
                continue

            coin_indicies = []      # python list to hold indicies of found coincidences

            # First look forwards
            j = i + 1
            while events[j][4] <= (events[i][4] + coin_window):

                # print('Event to check:', j, events[j])
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][0] != events[i][0] and not events[j][5]:
                        coin_indicies.append(j)

                    j += 1
                else:
                    # Check if the event is the correct detector number
                    if events[j][0] != events[i][0]:
                        coin_indicies.append(j)

                    j += 1

            # Then look backwards
            j = i - 1
            while events[j][4] >= (events[i][4] - coin_window):
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][0] != events[i][0] and not events[j][5]:
                        coin_indicies.append(j)

                    j -= 1
                else:
                    # Check if the event is the correct detector number
                    if events[j][0] != events[i][0]:
                        coin_indicies.append(j)

                    j -= 1

            # If at least one coincidence has been found
            if len(coin_indicies) > 0:
                events[i][5] = 1                            # flag current event as counted

                if choose_all:
                    coins_per_event += len(coin_indicies)       # increment coincidences per event

                    for index in coin_indicies:                 # loop through all the found coincidences
                        tot_coins += 1                          # increment coincidence counter

                        # Add coincidence data to dictionary list
                        coin_dict_list.append({'time_1':events[i][4], 'x_1':events[i][1], 'y_1':events[i][2], 'z_1':events[i][3],
                                               'time_2':events[index][4], 'x_2':events[index][1], 'y_2':events[index][2], 'z_2':events[index][3]})

                else:
                    chosen_index = coin_indicies[0]     # start with the first index

                    if choose_random:       # if choosing the index at random
                        rand_index = randint(0, len(coin_indicies)-1)    # pick an index at random
                        chosen_index = coin_indicies[rand_index]


                    else:       # pick the event closest to the current event in time
                        for k in range(len(coin_indicies)):     # loop through coincidence indicies
                            if abs(i - coin_indicies[k]) < abs(i - chosen_index):    # if index is closer to the event than the current minimum
                                chosen_index = coin_indicies[k]

                    events[i][5] = 1                        # flag current event as counted
                    events[chosen_index][5] = 1             # flag coincidence as counted
                    tot_coins += 1                          # increment coincidence counter
                    coins_per_event += len(coin_indicies)   # increment coincidences per event

                    # Check for any multiple scatter events and mark all as counted
                    for index in coin_indicies:
                        if events[chosen_index][4] == events[index][4]:   # if the event time is the same as the chosen index time
                            events[index][5] = 1                            # also flag that event as counted

                    # Add coincidence data to dictionary list
                    coin_dict_list.append({'time_1':events[i][4], 'x_1':events[i][1], 'y_1':events[i][2], 'z_1':events[i][3],
                                           'time_2':events[chosen_index][4], 'x_2':events[chosen_index][1], 'y_2':events[chosen_index][2], 'z_2':events[chosen_index][3]})


    except IndexError:
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
        print('\nDone. Converting to a pandas DataFrame...\n')
        coincidence_data = pandas.DataFrame(coin_dict_list, columns = ['time_1', 'x_1', 'y_1', 'z_1', 'time_2', 'x_2', 'y_2', 'z_2'])

        avg_coins_per_event = coins_per_event/tot_coins
        return coincidence_data, tot_coins, avg_coins_per_event

    except Exception as e:
        print(e)

# Takes in the raw polaris data and finds the coincidences for PEPT processing, preserving detector and energy information
def find_coincidences3(events, coin_window_, single_counting = True, choose_random = True, choose_all = False):
    '''
    Takes in a pandas dataframe of the energy-filtered data and finds all the
    coincidences, storing them in a pandas dataframe, returning the dataframe
    and other information related to the coincidence processing.

    events array is expected in the following format:

    scatters | detector | energy | x | y | z | time | counted
    ---------|----------|--------|---|---|---|------|--------

    coincidence_data is returned in the following format:

    scatters_1 | detector_1 | energy_1 | time_1 | x_1 | y_1 | z_1 | scatters_2 | detector_2 | energy_2 | time_2 | x_2 | y_2 | z_2
    -----------|------------|----------|--------|-----|-----|-----|------------|------------|----------|--------|-----|-----|-----

    @params:
        allevents           - Required : Numpy array of polaris data that's already been energy filtered (DataFrame)
        coin_window_        - Required : Timing window in 10s of nanoseconds to look for coincidences within (Int)
        single_counting     - Optional : Flag events to prevent double counting the same events (Bool, default = True)
        choose_random       - Optional : Whether to choose coincidences randomly, or choose the event closest to current (Bool, default = True)

    @returns:
        coincidence_data        - Pandas DataFrame containing the coincidences found (DataFrame)
        tot_coins               - Total number of coincidences found (Int)
        avg_coins_per_event     - Average number of coincidences found per event, for the events that have at least one coincidence (Float)
    '''

    # Define c types for looping
    cdef:
        unsigned long tot_coins = 0
        unsigned long coins_per_event = 0
        int i
        int j
        int coin_window
        long start_time


    l = len(events)   # length of the events array for progress bar
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    coin_dict_list = []     # create a list of dictionaries to append coincdence data rows to. Faster than appending to a DataFrame.

    try:
        coin_window = coin_window_/2
        start_time = events[0][6]

        # Loop through all events in array
        for i in range(len(events)):

            if i % int(l/100) == 0:
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # Check we're not going to have an IndexError when iterating backwards
            if (events[i][6] - coin_window) <= start_time:
                continue

            # Check if the event has already been counted
            if single_counting and events[i][7]:
                continue

            coin_indicies = []      # python list to hold indicies of found coincidences

            # First look forwards
            j = i + 1
            while events[j][6] <= (events[i][6] + coin_window):
                # print('Event to check:', j, events[j])
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][1] != events[i][1] and not events[j][7]:
                        coin_indicies.append(j)

                    j += 1
                else:
                    # Check if the event is the correct detector number
                    if events[j][1] != events[i][1]:
                        coin_indicies.append(j)

                    j += 1

            # Then look backwards
            j = i - 1
            while events[j][6] >= (events[i][6] - coin_window):
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][1] != events[i][1] and not events[j][7]:
                        coin_indicies.append(j)

                    j -= 1
                else:
                    # Check if the event is the correct detector number
                    if events[j][1] != events[i][1]:
                        coin_indicies.append(j)

                    j -= 1

            # If at least one coincidence has been found
            if len(coin_indicies) > 0:

                # Check to see if there are two coincidences with the same timestamp - if so, add both coincidences
                # This is because double events have been counted in the coincidence processing
                if (len(coin_indicies) == 2) and (events[coin_indicies[0]][6] == events[coin_indicies[1]][6]):
                    # Add coincidence data to dictionary list
                    k = coin_indicies[0]
                    l = coin_indicies[1]
                    coin_dict_list.append({'scatters_1': events[i][0], 'detector_1': events[i][1], 'energy_1': events[i][2], 'time_1':events[i][6], 'x_1':events[i][3], 'y_1':events[i][4], 'z_1':events[i][5],
                                           'scatters_2': events[k][0], 'detector_2': events[k][1], 'energy_2': events[k][2], 'time_2':events[k][6], 'x_2':events[k][3], 'y_2':events[k][4], 'z_2':events[k][5]})
                    coin_dict_list.append({'scatters_1': events[i][0], 'detector_1': events[i][1], 'energy_1': events[i][2], 'time_1':events[i][6], 'x_1':events[i][3], 'y_1':events[i][4], 'z_1':events[i][5],
                                           'scatters_2': events[l][0], 'detector_2': events[l][1], 'energy_2': events[l][2], 'time_2':events[l][6], 'x_2':events[l][3], 'y_2':events[l][4], 'z_2':events[l][5]})

                    events[i][7] = 1                            # flag current event as counted
                    events[k][7] = 1                            # flag first coincidence as counted
                    events[l][7] = 1                            # flag second coincidence as counted
                    tot_coins += 1                              # increment coincidence counter (double scatter counted as one coincidence)
                    coins_per_event += len(coin_indicies)       # increment coincidences per event

                else:
                    # print(i)
                    # print(coin_indicies)
                    chosen_index = coin_indicies[0]     # start with the first index

                    if choose_random:       # if choosing the index at random
                        rand_index = randint(0, len(coin_indicies)-1)    # pick an index at random
                        # print(rand_index)
                        chosen_index = coin_indicies[rand_index]
                        # print(chosen_index)

                    else:       # pick the event closest to the current event in time
                        for k in range(len(coin_indicies)):     # loop through coincidence indicies
                            if abs(i - coin_indicies[k]) < abs(i - chosen_index):    # if index is closer to the event than the current minimum
                                chosen_index = coin_indicies[k]
                        # print(chosen_index)

                    events[i][7] = 1                        # flag current event as counted
                    events[chosen_index][7] = 1             # flag coincidence as counted
                    tot_coins += 1                          # increment coincidence counter
                    coins_per_event += len(coin_indicies)   # increment coincidences per event

                    # Add coincidence data to dictionary list
                    coin_dict_list.append({'scatters_1': events[i][0], 'detector_1': events[i][1], 'energy_1': events[i][2], 'time_1':events[i][6], 'x_1':events[i][3], 'y_1':events[i][4], 'z_1':events[i][5],
                                           'scatters_2': events[chosen_index][0], 'detector_2': events[chosen_index][1], 'energy_2': events[chosen_index][2], 'time_2':events[chosen_index][6], 'x_2':events[chosen_index][3], 'y_2':events[chosen_index][4], 'z_2':events[chosen_index][5]})

    except IndexError:
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
        print('\nDone. Converting to a pandas DataFrame...\n')
        coincidence_data = pandas.DataFrame(coin_dict_list, columns = ['scatters_1', 'detector_1', 'energy_1', 'time_1', 'x_1', 'y_1', 'z_1', 'scatters_2', 'detector_2', 'energy_2', 'time_2', 'x_2', 'y_2', 'z_2'])

        avg_coins_per_event = coins_per_event/tot_coins
        return coincidence_data, tot_coins, avg_coins_per_event

    except Exception as e:
        print(e)

# Find the coincidence rate based on a delay window
def find_delay_coincidences(allevents, time_delay, coin_window):
    '''
    Takes in a pandas dataframe of the energy-filtered data and finds all the
    coincidences, storing them in a pandas dataframe, returning the dataframe
    and other information related to the coincidence processing.

    @params:
        allevents       - Required : Pandas dataframe of polaris data that's already been energy filtered (DataFrame)
        time_delay      - Required : Delay between detector heads in 10s of nanoseconds (Int)
        coin_window     - Required : Time window inside of which to search for coincidences (Int)

    @returns:
        tot_coincidences    - total number of coincidences found in the given DataFrame (Int)
    '''

    l = len(allevents.index)    # get the length of the array after filtering

    tot_coincidences = 0        # coincidence counter
    curr_index = 0              # counter because energy filtering removes events and indicies don't reset

    try:
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

        start_time = allevents.iloc[0]['time']
        # Iterating through all the events in the energy-filtered dataset
        for row in allevents.itertuples():

            # Progress bar
            if curr_index % int(l/100) == 0:
                printProgressBar(curr_index + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # Make sure that we won't hit the beginning of the array if moving backwards
            # if row.time < (start_time + time_delay - coin_window/2.0):
            if row.time < (start_time + 100000):
                curr_index += 1
                continue

            # Create placeholders for events to check
            nxt_index = curr_index
            nxt_event = allevents.iloc[nxt_index]

            if time_delay >= 0:     # if the centre of the coincidence window is forward in time
                # print('Delay >= 0')
                if row.time < (row.time + time_delay - coin_window/2.0):       # if the entire coincidence window is ahead of current event
                    # print('Case 1')
                    # Only search forward
                    while nxt_event.time <= (row.time + time_delay + coin_window/2.0):
                        # Check you're inside the delayed timing window
                        if nxt_event.time <= (row.time + time_delay - coin_window/2.0):
                            nxt_index += 1
                            nxt_event = allevents.iloc[nxt_index]
                            continue

                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index += 1
                        nxt_event = allevents.iloc[nxt_index]

                else:   # if the lower half of the coincidence window is behind the current event
                    # print('Case 2')
                    # Search first forwards, then backwards
                    while nxt_event.time <= (row.time + time_delay + coin_window/2.0):
                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index += 1
                        nxt_event = allevents.iloc[nxt_index]

                    # Reset for next while loop
                    nxt_index = curr_index
                    nxt_event = allevents.iloc[nxt_index]
                    while nxt_event.time >= (row.time + time_delay - coin_window/2.0):
                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index -= 1
                        nxt_event = allevents.iloc[nxt_index]

            else:   # if the centre of the coincidence window is backwards in time
                # print('Delay < 0')
                if row.time < (row.time + time_delay + coin_window/2.0):     # if the upper half of the coincidence window is ahead of the current event
                    # print('Case 3')
                    # Search first backwards, then forwards
                    while nxt_event.time >= (row.time + time_delay - coin_window/2.0):
                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index -= 1
                        nxt_event = allevents.iloc[nxt_index]

                    # Reset for next while loop
                    nxt_index = curr_index
                    nxt_event = allevents.iloc[nxt_index]
                    while nxt_event.time <= (row.time + time_delay + coin_window/2.0):
                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index += 1
                        nxt_event = allevents.iloc[nxt_index]

                else: # if the entire coincidence window is behind the current event
                    # print('Case 4')
                    # Only seach backwards
                    while nxt_event.time >= (row.time + time_delay - coin_window/2.0):
                        # Check you're inside the delayed timing window
                        if nxt_event.time >= (row.time + time_delay + coin_window/2.0):
                            nxt_index -= 1
                            nxt_event = allevents.iloc[nxt_index]
                            continue

                        # Count coincidence if the detectors aren't the same
                        if nxt_event.detector != row.detector:
                            tot_coincidences += 1       # increment the total number of coincidences
                            # break                       # break the while loop and move onto next event

                        nxt_index -= 1
                        nxt_event = allevents.iloc[nxt_index]

            curr_index += 1

    except IndexError:
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
        # print('End of array reached.')
    except:
        print('Aaaaaah, something went wrong :(')

    return tot_coincidences

# Fast implementation of coicidence counting
def count_coincidences(events, coin_window_, time_delay_):
    '''
    Takes in a 2D integer array of all the event times with detector identifiers
    in order. Then proceeds to loop through each event and counts the number of
    coincidences based on the given timing window. Expects filtered data. All
    times are expected to be in 10s of nanoseconds

    @params:
        events          - Required : 2D array of detector number and time stamp for each event (Int[][2])
        coin_window_    - Required : Time window in which to look for coicidences (Int)
        time_delay_     - Required : Delay between detector heads in 10s of nanoseconds (Int)

    @returns:
        tot_coin    - total number of coicidences found (Int)
        coin_rate   - number of coicidences found per second (Int)
    '''

    # Define c types for looping
    cdef:
        unsigned long tot_coins = 0
        int i
        int j
        int coin_window
        int time_delay
        long curr_event_time
        int upper_window
        int lower_window
        long start_time
        #int coin_rate

    try:
        time_delay = int(time_delay_)
        coin_window = coin_window_/2

        start_time = events[0][1]

        # l = len(events)    # get the length of the array after filtering

        # printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

        # start_time = events[0][1]       # get the very first time
        # print(start_time)

        upper_window = time_delay + coin_window
        lower_window = time_delay - coin_window

        print('Upper window:', upper_window)
        print('Lower window:', lower_window)

        num_events = 0

        for i in range(1,len(events)):
            # Progress bar
            # if i % int(l/100) == 0:
            #     printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            curr_event_time = events[i][1]

            # Check we're not going to have an IndexError when iterating backwards or if we've already counted a coicidence
            if (curr_event_time + lower_window) <= start_time or events[i][2]:
                continue

            # First look forward
            j = i + 1
            while (events[j][1] <= curr_event_time + upper_window):     # while the subsequent event time is less than current event time + coincidence window
                if (events[j][0] != events[i][0]) and (events[j][1] >= curr_event_time + lower_window):    # if the events are not within the same detector
                    events[j][2] = 1        # flag event as counted for a coincidence
                    tot_coins += 1
                j += 1

            # Then look backwards
            j = i - 1
            while events[j][1] >= curr_event_time + lower_window:     # while the subsequent event time is less than current event time + coincidence window
                if (events[j][0] != events[i][0]) and (events[j][1] <= curr_event_time + upper_window):    # if the events are not within the same detector and inside the coicidence window
                    events[j][2] = 1        # flag event as counted for a coincidence
                    tot_coins += 1
                j -= 1

            num_events = i

        tot_time = events[-1][1] - events[0][1]

    except IndexError:
        print(tot_coins)
        print(num_events)
        print()
        return tot_coins
        # printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    except:
        print('Aaaaaah, something went wrong :(')


    # coin_rate = tot_coin / tot_time
    #
    return tot_coins#, coin_rate

# Fast implementation of coicidence counting second attempt
def count_coincidences2(d0, d1, coin_window_, time_delay_):
    '''
    Takes in a 2D integer array of all the event times with detector identifiers
    in order. Then proceeds to loop through each event and counts the number of
    coincidences based on the given timing window. Expects filtered data. All
    times are expected to be in 10s of nanoseconds

    @params:
        events          - Required : 2D array of detector number and time stamp for each event (Int[][2])
        coin_window_    - Required : Time window in which to look for coicidences (Int)
        time_delay_     - Required : Delay between detector heads in 10s of nanoseconds (Int)

    @returns:
        tot_coin    - total number of coicidences found (Int)
        coin_rate   - number of coicidences found per second (Int)
    '''

    # Define c types for looping
    cdef:
        unsigned long tot_coins = 0
        int i
        # int j
        int coin_window
        int time_delay
        # long curr_event_time
        long upper_window
        long lower_window
        # long start_time
        #int coin_rate

    time_delay = int(time_delay_)
    coin_window = coin_window_/2
    # coin_indicies = [0]

    for i in range(len(d0)):
        upper_window = d0[i] + time_delay + coin_window
        lower_window = d0[i] + time_delay - coin_window

        # print(lower_window, upper_window,'\n')

        coin_indicies = numpy.where((d1 >= lower_window) & (d1 <= upper_window))
        # coin_indicies = numpy.searchsorted(d1,(d1 >= lower_window) & (d1 <= upper_window))

        if len(coin_indicies[0]) > 0:
            # print(i, coin_indicies, '\n')
            tot_coins += 1

    # coin_rate = tot_coin / tot_time
    #
    return tot_coins#, coin_rate

# Fast implementation of coicidence counting third attempt
def count_coincidences3(events, coin_window_, single_counting = True, choose_all = False):
    '''
    Takes in a 2D integer array of all the event times with detector identifiers
    in order. Then proceeds to loop through each event and counts the number of
    coincidences based on the given timing window. Expects filtered data. All
    times are expected to be in 10s of nanoseconds.

    events array is expected in the following format:

    detector | time | counted
    -------------------------
             |      |

    Where detector is the number of the detector the event was recorded in,
    time is the timestamp of the event in 10s of nanoseconds and counted is
    a flag to indicate whether an event has been counted as a coincidence or
    not yet (initially all 0/False)

    @params:
        events              - Required : 2D array of detector number and time stamp for each event (Int[][2])
        coin_window_        - Required : Time window in which to look for coicidences in 10s of nanoseconds (Int)
        single_counting     - Optional : Flag events to prevent double counting the same events (Bool, default = True)
        choose_all          - Optional : Whether to count all of the events found in coincidence, or pick one and discard the rest (Bool, default = False)

    @returns:
        tot_coin    - total number of coicidences found (Int)
    '''

    # Define c types for looping
    cdef:
        unsigned long tot_coins = 0
        int i
        int j
        int coin_window
        long start_time

    try:
        coin_window = coin_window_/2
        start_time = events[0][1]

        # Loop through all events in array
        for i in range(len(events)):

            # if events[i][0] == 1:
            #     print("FOUND ONE!")
            #     print(i)
            #     break

            # if i == 10000:
            #     break

            # print('Current event: ', events[i])

            # Check we're not going to have an IndexError when iterating backwards
            if (events[i][1] - coin_window) <= start_time:
                # print('-------------------------------------------------------------\n')
                continue

            # Check if the event has already been counted
            if single_counting and events[i][2]:
                continue

            coin_indicies = []      # python list to hold indicies of found coincidences

            # First look forwards
            j = i + 1
            while events[j][1] <= (events[i][1] + coin_window):
                # print('Event to check:', j, events[j])
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][0] != events[i][0] and not events[j][2]:
                        # print('Found coincidence!')
                        coin_indicies.append(j)
                        # print('Index array: ', coin_indicies)
                else:
                    # Check if the event is the correct detector number
                    if events[j][0] != events[i][0]:
                        # print('Found coincidence!')
                        coin_indicies.append(j)
                        # print('Index array: ', coin_indicies)

                j += 1

            # print('Now looking back...')
            # Then look backwards
            j = i - 1
            while events[j][1] >= (events[i][1] - coin_window):
                # print('Event to check:', j, events[j])
                if single_counting:
                    # Check if the event is the correct detector number and hasn't already been counted
                    if events[j][0] != events[i][0] and not events[j][2]:
                        # print('Found coincidence!')
                        coin_indicies.append(j)
                        # print('Index array: ', coin_indicies)

                    j -= 1

                else:
                    # Check if the event is the correct detector number
                    if events[j][0] != events[i][0]:
                        # print('Found coincidence!')
                        coin_indicies.append(j)
                        # print('Index array: ', coin_indicies)

                    j -= 1

            # If at least one coincidence has been found
            if len(coin_indicies) > 0:
                events[i][2] = 1                            # flag current event as counted

                if choose_all:
                    for index in coin_indicies:                 # loop through all the found coincidences
                        # events[index][2] = 1                    # flag coincidence as counted
                        tot_coins += 1                          # increment coincidence counter

                else:
                    # print('Coincidence indicies: ', coin_indicies)
                    # rand_index = randint(0, len(coin_indicies)-1)    # pick an index at random
                    # chosen_index = coin_indicies[rand_index]
                    chosen_index = coin_indicies[0]     # choose the first index (closest in time)
                    # print('Chosen index: ', rand_index)
                    # print('Flagged current: ', events[i])
                    events[chosen_index][2] = 1   # flag chosen coincidence as counted
                    # Check for any multiple scatter events and mark all as counted
                    for index in coin_indicies:
                      if events[chosen_index][1] == events[index][1]:   # if the event time is the same as the chosen index time
                        events[index][2] = 1                            # also flag that event as counted
                    # print('Flagged chosen: ', events[rand_index])
                    tot_coins += 1              # increment coincidence counter
                    # print('Total coincidences to date: ', tot_coins)

            # print('-------------------------------------------------------------\n')

    except IndexError:
        # print(tot_coins)
        # print(num_events)
        # print()
        return tot_coins
        # printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    except Exception as e:
        print(e)

    return tot_coins

# Read the .txt files produced by the BSSReader.exe supplied by H3D and return activity rates
def rate_process_allevents(data_file, time_interval = 1):
    '''
    Takes in a AllEvents.txt file produced by BEFReader.exe and produces a 2D NumPy
    array of the event rate as a function of time.

    Expected format of .txt file:
    [Number of Scatters, Energy (keV), x (mm), y (mm), z (mm), Time (10s of ns), Total Number of Events]

    @params:
        data_file           - Required : Path to a AllEvents.txt file to read data from (Str)
        time_interval       - Optional : Time (in seconds) within which to calculate event rate (Int, default = 1)

    @returns:
        rates_data - NumPy array of event rate as a function of time
    '''

    # Define c types for looping
    cdef:
        int i

    print('Reading in data from', data_file, 'into a DataFrame and extracting time column...')
    allevents_pd = pandas.read_csv(data_file, sep = '	', header = None,
                               names = ['num_scatters', 'energy', 'x', 'y', 'z', 'time', 'tot_events'],
                               usecols = ['time'])#, dtype = {'time': np.uint64})
    print('Done\n')
    # print(allevents_pd, '\n')

    event_times = allevents_pd.to_numpy(dtype = numpy.uint64)
    l = len(event_times)

    # np.set_printoptions(edgeitems = 15)
    # print(event_times, '\n')

    print('Adding time stamps for multiple scatter events...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    # Add time stamps for multiple scatter events
    for i in range(l-1, -1, -1):

        if event_times[i] == 0:
            event_times[i] = event_times[i+1]

        if i % int(l/100) == 0:
            printProgressBar(l - (i + 1), l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    print('Done\n')

    # print(event_times, '\n')

    print('Shifting all times to count from 0...')
    event_times -= event_times[0]      # move all times to count from 0
    print('Done\n')

    # print(event_times, '\n')

    print('Calculating event rates for time intervals of', time_interval, 'seconds...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
    rates = []
    num_events = 0
    start_time = event_times[0]
    for i in range(l):

        num_events += 1

        if event_times[i] - start_time > (time_interval*1e8):     # if the time difference is greater than a second
            rates.append(num_events/time_interval)
            num_events = 0
            start_time = event_times[i]

        if i % int(l/100) == 0:
            printProgressBar(i, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    print('Done\n')

    rates = numpy.array(rates)

    print('Inserting a seconds time column...')
    times = [i*time_interval for i in range(1, len(rates)+1)]
    rates = numpy.c_[times, rates]
    print('Done\n')

    return rates

# Take all the double scatter events replace the energy with the energy sum
def sum_double_scatter_energies(allevents):
    '''
    Takes in a pandas dataframe of all the recorded events, loops through each
    event and for all double scatter events sums the energies and replaces the
    recorded energy with the sum

    allevents array is expected in the following format:

     scatters | detector | energy | x | y | z | time
    ----------|----------|--------|---|---|---|------

    @params:
        allevents           - Required : Pandas DataFrame of polaris data (DataFrame)

    @returns:
        allevents           - Pandas DataFrame with processed double scatters (DataFrame)
    '''

    # Define c types for looping
    cdef:
        int i

    tot_missing = 0

    # Loop through each event in the array
    for i in range(len(allevents)-1):


      # if i%1e4 == 0:
      #   print(i)

      if allevents[i, 0] != 2:   # if current event isn't a double scatter
        continue

      if allevents[i+1, 0] != 2 or allevents[i, 6] != allevents[i+1, 6]:   # skip over orphaned doubles
        tot_missing += 1
        continue

      # Sum the energies and replace event energies
      energy_sum = allevents[i, 2] + allevents[i+1, 2]
      allevents[i, 2] = energy_sum
      allevents[i+1, 2] = energy_sum

      # print(allevents[i])
      # print(allevents[i+1])

      # Skip over the next event as it's already processed
      i += 1

    return allevents, tot_missing

# Code specific for Polaris Geant4 Monte Carlo scatter processing
# --------------------------------------------------------------------

#  converts scatter data (+ tag) from ROOT arrays and converts for use panda dataframe
def convert_root_arrays(det, geng, engd, xpos, ypos, zpos, tag):
    #  det, eng, xpos, ypos and zpos are arrays with size = 3
    #  count and tag are single values

    #  initializing variables
    count = 0
    di = []
    ni = []; dn = []; ed = []; xp = []; yp = []; zp = []; st = []

    #  loop through array to count number of interactions & convert detector string to index
    for i in range(3):
        if (geng[i] != 0):
            count += 1
            if b'D01' in det[i]: di.append(0)
            if b'D02' in det[i]: di.append(1)
            #print(' i: {}, detector: {}, di: {}, engD/MeV: {}, xpos/mm: {}, ypos/mm: {}, zpos/mm: {}, stag: {}'.format(i, det[i], di, engd[i], xpos[i], ypos[i], zpos[i], tag))

    #  loop through values to fill lists to return
    for j in range(count):
        ni.append(count);  dn.append(di[j]);  ed.append(engd[j]);  xp.append(xpos[j]);  yp.append(ypos[j]);  zp.append(zpos[j]);  st.append(tag)

    return ni, dn, ed, xp, yp, zp, st

#  converts scatter (+ tag & origin) data from ROOT arrays and converts for use with panda dataframe
def convert_root_arrays_origins(evt, det, geng, engd, xpos, ypos, zpos, tag, eorg, xorg, yorg, zorg):
    #  det, eng, xpos, ypos and zpos are arrays with size = 3
    #  evt, count, tag, eorg, xorg, yorg and zorg are single values

    #  initializing variables
    count = 0
    di = []; ev = []
    ni = []; dn = []; ed = []; xp = []; yp = []; zp = []; st = []
    eo = []; xo = []; yo = []; zo = []; ss = []

    #  loop through array to count number of interactions & convert detector string to index
    for i in range(3):
        if (geng[i] != 0):
            count += 1
            if b'D01' in det[i]: di.append(0)
            if b'D02' in det[i]: di.append(1)
            if b'D03' in det[i]: di.append(2)
            if b'D04' in det[i]: di.append(3)
            if b'D05' in det[i]: di.append(4)
            if b'D06' in det[i]: di.append(5)
            if b'D07' in det[i]: di.append(6)
            if b'D08' in det[i]: di.append(7)
    #  check for multi-stage events / set sStg flag (true = single-stage, false = multi-stage)
    if   (count == 1):                                              single_stage = True
    elif (count == 2 and di[0] == di[1]):                           single_stage = True
    elif (count == 3 and di[0] == di[1]):                           single_stage = True
    else:                                                           single_stage = False
    #  loop through values to fill lists to return
    for j in range(count):
        ev.append(evt);    ni.append(count);  dn.append(di[j]);  ed.append(engd[j]);  xp.append(xpos[j]);  yp.append(ypos[j]);  zp.append(zpos[j]);  st.append(tag)
        eo.append(eorg);   xo.append(xorg);   yo.append(yorg);   zo.append(zorg);     ss.append(single_stage)

    return ev, ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss

# #  converts scatter (+ tag & origin) data from ROOT arrays and converts for use with panda dataframe
# def convert_root_arrays_origins(det, geng, engd, xpos, ypos, zpos, tag, eorg, xorg, yorg, zorg):
#     #  det, eng, xpos, ypos and zpos are arrays with size = 3
#     #  count, tag, eorg, xorg, yorg and zorg are single values
#
#     #  initializing variables
#     count = 0
#     di = []
#     ni = []; dn = []; ed = []; xp = []; yp = []; zp = []; st = []
#     eo = []; xo = []; yo = []; zo = []; ss = []
#
#     #  loop through array to count number of interactions & convert detector string to index
#     for i in range(3):
#         if (geng[i] != 0):
#             count += 1
#             if b'D01' in det[i]: di.append(0)
#             if b'D02' in det[i]: di.append(1)
#             if b'D03' in det[i]: di.append(2)
#             if b'D04' in det[i]: di.append(3)
#             if b'D05' in det[i]: di.append(4)
#             if b'D06' in det[i]: di.append(5)
#             if b'D07' in det[i]: di.append(6)
#             if b'D08' in det[i]: di.append(7)
#     #  check for multi-stage events / set sStg flag (true = single-stage, false = multi-stage)
#     if   (count == 1):                                              single_stage = True
#     elif (count == 2 and di[0] == di[1]):                           single_stage = True
#     elif (count == 3 and di[0] == di[1]):                           single_stage = True
#     else:                                                           single_stage = False
#     #  loop through values to fill lists to return
#     for j in range(count):
#         ni.append(count);  dn.append(di[j]);  ed.append(engd[j]);  xp.append(xpos[j]);  yp.append(ypos[j]);  zp.append(zpos[j]);  st.append(tag)
#         eo.append(eorg);   xo.append(xorg);   yo.append(yorg);     zo.append(zorg);     ss.append(single_stage)
#
#     return ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss

#  returns interactions (E, X, Y, Z), tags (4 character string) and origins (E, X, Y, Z) for the specified number of scatters and detector number
def get_interaction_data_tags_origin(df, energy_sort = True, num_scatters = 2, det_num = None):
    # input dataframe format: cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg']]

    print ('   - Grouping data for detector {} with scatter number {}'.format(det_num, num_scatters))

    #  stores the data with the relevant number of scatters & detector number
    if det_num is not None:
        chosen = df.loc[numpy.isclose(df.scatters, num_scatters) & numpy.isclose(df.detector, det_num)]
    else:
        chosen = df[numpy.isclose(df.scatters, num_scatters)]

    #  sorts the scatter of each event by energy deposited, highest first
    if energy_sort:
        print ('     -> !!! Big Energy Sort disabled until can figure out how to use it effectively with Monte Carlo Data !!!')
        #chosen = chosen.sort_values(by = ['time', 'energy'], ascending = [True, False])
        #chosen = chosen.sort_values(by = 'energy', ascending = False)  # this is not working!!!! because there is not time stamp, sorts everything!!!!!

    #  remove multi-detector events if det_num is fixed, i.e. 0 or 1 / compares zOrg values to previous [shift(1)] and next [shift(-1)] values
    chosen_start_number = len(chosen)
    if det_num is not None:
        chosen = chosen[(chosen.sStg == True)]

    number_dropped = chosen_start_number - len(chosen)

    #  re-formatting scatter data
    #   - to_numpy() -> Convert the frame to its Numpy-array representation
    scatters = chosen.to_numpy()

    #  create array for scatter tags and origin data / needs to come before scatters array is truncated
    tags = scatters[:, 6]
    origins = scatters[:, [7, 8, 9, 10]]
    #  only store columns 2, 3, 4, 5 (i.e. eng, x, y, z)
    scatters = scatters[:, [2, 3, 4, 5]]

    #  store scatter (and origin) data as float instead of as object (default), important for calcuation later in code
    scatters = scatters[:, [0, 1, 2, 3]].astype(float)
    origins = origins[:, [0, 1, 2, 3]].astype(float)

    #  checks that the final number of event is evenly divisible by the number of scatters <- disabled warning
    if len(scatters) % num_scatters != 0:
        # Change Exception to Warning
        #raise Exception("Wrong number of scatters (%d) for scatters (%d), mod = (%d)" \
        #                % (len(scatters), num_scatters, len(scatters) % num_scatters))
        print ("!!! WARNING: Wrong number of scatters (%d) for scatters (%d), mod = (%d)" % (len(scatters), num_scatters, len(scatters) % num_scatters))
        """
        # delete the last row from the arrays (not sure if this will also work for triples scatter)
        if num_scatters == 2:
            print ("    - removing last scatter from array to rectify problem")
            scatters = numpy.delete(scatters, (-1), axis = 0)
            tags = numpy.delete(tags, (-1), axis = 0)
            origins = numpy.delete(origins, (-1), axis = 0)
        """

    #  print counts to screen
    print ('     - found {} interactions out of {} / number of interactions dropped: {}'.format(len(scatters), len(df), number_dropped))

    return scatters, tags, origins

#  re-arranges scatter, tag, origin data from consecutive lines to all Compton scatter data on a single line for CSV output
def format_data_for_output_tags_origins(scatters, tags, origins, num_scatters, det_num = None):

    print ('   - Formatting {} interactions from detector {} and number of scatters: {}'.format(len(scatters), det_num, num_scatters))

    #  check if any data in scatters array
    if scatters.shape[0] == 0:
        return scatters

    #  re-arranges double scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2 format
    if num_scatters == 2:
        #  moves next event (i + 1) to the same line as current event
        temp = numpy.concatenate((scatters[0:-1], scatters[1:]), axis = 1)
        #  stores data from every other line
        out = temp[0::2]
        tagOut = tags[0::2]
        originsOut = origins[0::2]

    #  re-arranges triple scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2, E3, X3, Y3, Z3 format
    if num_scatters == 3:
        #  moves next two events (i + 1 & i + 2) to the same line as current event
        temp = numpy.concatenate((scatters[0:-2], scatters[1:-1], scatters[2:]), axis = 1)
        #  stores data from every third line
        out = temp[0::3]
        tagOut = tags[0::3]
        originsOut = origins[0::3]

    print ('     - returned {} events'.format(len(out)))

    return out, tagOut, originsOut

#  counting gamma scatters using scatter_tag
def count_gamma_scatter(scatters, tags):

    count_PZ = 0; count_NZ = 0; count_PS = 0; count_NS = 0; count_PM = 0; count_NM = 0

    if len(tags) > 0:
        for i in range(len(tags)):
            #print (' ****** tags[{}][:2]: {}'.format(i, tags[i][:2]))
            #  count prompt/nonprompt events + number of scatter events
            #   -> tag[0] / 1st [particleType]: (P)rompt or (N)onprompt
            #   -> tag[1] / 2nd [scatterNumber]: (Z)ero, (S)ingle, (M)ultiple scatters
            if tags[i][:2] == 'PZ':  count_PZ += 1
            elif tags[i][:2] == 'NZ':  count_NZ += 1
            elif tags[i][:2] == 'PS':  count_PS += 1
            elif tags[i][:2] == 'NS':  count_NS += 1
            elif tags[i][:2] == 'PM':  count_PM += 1
            else:  count_NM += 1

        #  print to screen
        #print ('   - Total Events: {} | PZ: {} ({:.2f}%) | NZ: {} ({:.2f}%) | PS: {} ({:.2f}%) | NS: {} ({:.2f}%) | PM: {} ({:.2f}%) | NM: {} ({:.2f}%)'.format(len(tags), count_PZ, (float(count_PZ)/len(tags))*100.0, count_NZ, (float(count_NZ)/len(tags))*100.0, count_PS, (float(count_PS)/len(tags))*100.0, count_NS, (float(count_NS)/len(tags))*100.0, count_PM, (float(count_PM)/len(tags))*100.0, count_NM, (float(count_NM)/len(tags))*100.0))
        print ('   - Total Events: {} | PZ: {} ({:.2f}%) | PS: {} ({:.2f}%) | PM: {} ({:.2f}%) | NZ: {} ({:.2f}%) | NS: {} ({:.2f}%) | NM: {} ({:.2f}%)'.format(len(tags), count_PZ, (float(count_PZ)/len(tags))*100.0, count_PS, (float(count_PS)/len(tags))*100.0, count_PM, (float(count_PM)/len(tags))*100.0, count_NZ, (float(count_NZ)/len(tags))*100.0, count_NS, (float(count_NS)/len(tags))*100.0, count_NM, (float(count_NM)/len(tags))*100.0))
        print ('   ->  {}, {}, {}, {}, {}, {}, {}'.format(len(tags), count_PZ, count_PS, count_PM, count_NZ, count_NS, count_NM))

    else:
        print('    - No events with this scatter number found.  Returning zeros')

    #  return counts
    return count_PZ, count_NZ, count_PS, count_NS, count_PM, count_NM

#  removing events below energy threshold (set by MINIMUM_ENERGY_THRESHOLD)
def filtering_energy_threshold_tags_origins(scatters, tags, origins, num_scatters, MINIMUM_ENERGY_THRESHOLD):

    scatters_threshold = [];  tags_threshold = [];  origins_threshold = []
    count_removed = 0

    print ('   - Applying energy threshold ({} MeV) for {} events with {} scatters'.format(MINIMUM_ENERGY_THRESHOLD, len(scatters), num_scatters))

    if len(tags) > 0:
        #  loop through list of events
        for index in range( len(scatters) ):

            #  checks E1 single scatters to see if below threshold
            if num_scatters == 1:
                if (scatters[index][0] > MINIMUM_ENERGY_THRESHOLD):
                    scatters_threshold.append(scatters[index])
                    tags_threshold.append(tags[index])
                    origins_threshold.append(origins[index])
                else:
                    count_removed += 1

            #  checks E1 and E2 for double scatters to see if below threshold
            if num_scatters == 2:
                if (scatters[index][0] > MINIMUM_ENERGY_THRESHOLD and scatters[index][4] > MINIMUM_ENERGY_THRESHOLD):
                    scatters_threshold.append(scatters[index])
                    tags_threshold.append(tags[index])
                    origins_threshold.append(origins[index])
                else:
                    count_removed += 1

            #  checks E1, E2 and E3 for triple scatters to see if below threshold
            if num_scatters == 3:
                if (scatters[index][0] > MINIMUM_ENERGY_THRESHOLD and scatters[index][4] > MINIMUM_ENERGY_THRESHOLD and scatters[index][7] > MINIMUM_ENERGY_THRESHOLD):
                    scatters_threshold.append(scatters[index])
                    tags_threshold.append(tags[index])
                    origins_threshold.append(origins[index])
                else:
                    count_removed += 1

        #    vstack takes list of numpy arrays and converts into single numpy array
        scatters_threshold = numpy.vstack(scatters_threshold)
        origins_threshold = numpy.vstack(origins_threshold)

    #  print counts to screen
    print ('    - results of energy threshold -> number of {}x events returned: {} | removed: {}'.format(num_scatters, len(scatters_threshold), count_removed))

    #  return energy threshold events
    return scatters_threshold, tags_threshold, origins_threshold

#  checks for unphysical events (theta1 = nan) from double scatter event data
def filtering_unphysical_double_scatters_tags_origins(scatters, tags, origins):
    #   - input format (scatters): eng1, x1, y1, z1, eng2, x2, y2, z2; [tags]; [orgEng, xOrg, yOrg, zOrg]
    #   - returns data in same format

    scatters_physical = [];  tags_physical = [];  origins_physical = []
    count_both, count_one, count_flip, count_none = [0] * 4

    #  loop through list of events
    for index in range( len(scatters) ):

        #  check original energy ordering
        order1 = physical_energy_ordering_double(scatters[index][0], scatters[index][4])

        #  check flipped energy ordering
        order2 = physical_energy_ordering_double(scatters[index][4], scatters[index][0])

        #  storing appropriate events into final output array: scatters_physical
        if (order1 == 1 & order2 == 1):
            scatters_physical.append(scatters[index])
            tags_physical.append(tags[index])
            origins_physical.append(origins[index])
            count_both += 1
        elif (order1 == 1):
            scatters_physical.append(scatters[index])
            tags_physical.append(tags[index])
            origins_physical.append(origins[index])
            count_one += 1
        elif (order2 == 1):
            SE = scatters[index]
            flipped_scatter = numpy.array((SE[4], SE[5], SE[6], SE[7], SE[0], SE[1], SE[2], SE[3]))
            scatters_physical.append(flipped_scatter)
            tags_physical.append(tags[index])
            origins_physical.append(origins[index])
            count_one += 1; count_flip += 1
        else:
            count_none += 1

    #  print counts to screen
    print ('    - results of event ordering -> number of physical events returned: {} | both work: {} | only one order works: {} | order flipped: {} | neither work: {}'.format(len(scatters_physical), count_both, count_one, count_flip, count_none))

    #    vstack takes list of numpy arrays and converts into single numpy array
    scatters_physical = numpy.vstack(scatters_physical)
    origins_physical = numpy.vstack(origins_physical)

    #  return physical events
    #    vstack takes list of numpy arrays and converts into single numpy array
    return scatters_physical, tags_physical, origins_physical

#  filtering events using Compton Line Filtering (based on expected gamma energies, input variable: CL_energies)
def compton_line_filtering_tags_origins(scatters, tags, origins, CL_range, CL_energies):
    #   - input format (scatters): eng1, x1, y1, z1, eng2, x2, y2, z2; [tags]; [orgEng, xOrg, yOrg, zOrg]
    #   - returns data in same format

    scatters_filtered = [];  tags_filtered = [];  origins_filtered = []
    count_compton = [0] * len(CL_energies)

    #  loop through list of events
    for index in range( len(scatters) ):

        #  use the first and second energies to calculate E0 and theta1
        E1 = scatters[index][0]; E2 = scatters[index][4]
        E0 = E1 + E2
        theta1 = numpy.arccos( 1 + MeCsq * ( 1.0/(E0) - 1.0/(E0-E1) ) );

        #  looping through expected gamma energies
        for i, e in enumerate(CL_energies):
            #  calculating range of Compton values for given total energy and first scatter angle
            minE1 = CL_range[0] * calculate_expected_first_scatter_energy(e , theta1)
            maxE1 = CL_range[1] * calculate_expected_first_scatter_energy(e , theta1)

            #  check if energy falls within expected range
            if E1 > minE1 and E1 < maxE1:

                #  add filtered data to output array: scatters_filtered
                scatters_filtered.append(scatters[index])
                tags_filtered.append(tags[index])
                origins_filtered.append(origins[index])
                count_compton[i] += 1

    #  total up number of Compton filtered events
    total_compton = numpy.sum(count_compton)

    #  print results to screen
    print ('    - results of Compton filtering -> number of filtered events returned: {} -> counts: {} by energy {}, respectively'.format(total_compton, count_compton, CL_energies))

    #    vstack takes list of numpy arrays and converts into single numpy array
    scatters_filtered = numpy.vstack(scatters_filtered)
    origins_filtered = numpy.vstack(origins_filtered)

    #  return filtered events
    #    vstack takes list of numpy arrays and converts into single numpy array
    return scatters_filtered, tags_filtered, origins_filtered

#
def create_plot_range_ieee(plot_data, x_label, nb_bins, rng_min, rng_max):
    plt.figure(figsize=(8, 8)) # (width, height in inches)
    plt.hist(plot_data, bins=nb_bins, range=[rng_min,rng_max], alpha = 1.0)
    plt.xlabel(x_label, fontsize = 20);
    plt.ylabel('Number of Gammas', fontsize = 20);


### End of Functions ###
