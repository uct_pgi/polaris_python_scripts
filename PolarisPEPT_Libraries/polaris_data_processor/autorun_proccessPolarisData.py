'''
Automates the running of the processPolarisData python scripts on all the data
files within a given directory.
'''

__author__ = 'Nicholas Hyslop (nhyslop2@gmail.com)'
__date__ = '20 February 2019'
__version__ = '1.0'

## IMPORT STATEMENTS ##
import os
import subprocess
import shlex
import sys


## GLOBAL VARIABLES ##
DATA_DIR = '/home/user/Documents/data/190612/'                                  # directory containing Polaris Data
PROCESS_DIR = '/home/user/Documents/code/polaris/polaris_data_processor/'       # directory containing processPolarisData python script

## FUNCTIONS ##

def run_command(command, out_dir):
    '''
    Runs bash commands and produces live output

    @params
    command     - Required: Command to run in terminal (Str)

    '''

    with open(out_dir + 'pet_processing.log', 'w') as f:
        process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
        for c in iter(lambda: process.stdout.read(1), b''):  # replace '' with b'' for Python 3
            sys.stdout.write(c.decode(sys.stdout.encoding))
            f.write(c.decode(sys.stdout.encoding))

    # process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    # return process.poll()
    # while True:
    #     line = process.stdout.readline().rstrip()
    #     if not line:
    #         break
    #     print(line)
    #     # yield line
    #
    # return
    #
    # while True:
    #     output = process.stdout.readline()
    #     print(output)
    #     if output == '' and process.poll() is not None:
    #         break
    #     if output:
    #         print(output.strip())
    rc = process.poll()
    return rc

# Adjust the filtration setup file to adjust filtration parameters
def adjust_process003(input_dir):
    '''
    Adjusts the processPolarisData_003.py script to have the correct input directory before running

    @params
    input_dir       - Required: The directory the Polaris data is held in

    '''

    f = open(PROCESS_DIR + 'processPolarisData_003.py', 'r')       # open the file
    lines = f.readlines()                   # read all the lines into an array

    lines[56] = 'INPUT_DIR = \'' + input_dir + '/\'\n'

    f.close()

    wf = open(PROCESS_DIR + 'processPolarisData_003.py', 'w')       # open the file

    for line in lines:
        wf.write(line)

    wf.close()

    return

def main():
    for subdir, dirs, files in os.walk(DATA_DIR):
        for dir in dirs:
            # # Ignore irrelevant directories
            # if dir[0] != 'J':
            #     continue
            adjust_process003(DATA_DIR + dir)
            print('Running process on ', DATA_DIR + dir)
            run_command('python processPolarisData_003.py', DATA_DIR + dir + '/')
            # run_command('python test.py', DATA_DIR + dir + '/')

        break


    return


if __name__ == '__main__':
    main()
