'''
Script to automate the energy and timing window sweeps on polaris data to further
optimise the f parameter for PolarisPEPT
'''

__author__ = 'Nicholas Hyslop'
__date__ = '20 March 2019'

# Import statements
# =================
import os
import subprocess
import shlex
import sys

import numpy as np
sys.path.insert(0, '/home/user/Documents/code/pept')
import polarisPEPT_processing as ppp
import matplotlib.pyplot as plt
# =================

# Global Variables
# =================
DATA_DIR = '/home/user/Documents/data/190226/'                                  # directory containing Polaris Data
PROCESS_DIR = '/home/user/Documents/code/processPolarisData/'                   # directory containing processPolarisData python script
# =================

def run_command(command, out_dir):
    '''
    Runs bash commands and produces live output

    @params
    command     - Required: Command to run in terminal (Str)

    '''

    with open(out_dir + 'pet_processing.log', 'w') as f:
        process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
        for c in iter(lambda: process.stdout.read(1), b''):  # replace '' with b'' for Python 3
            sys.stdout.write(c.decode(sys.stdout.encoding))
            f.write(c.decode(sys.stdout.encoding))

    # process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    # return process.poll()
    # while True:
    #     line = process.stdout.readline().rstrip()
    #     if not line:
    #         break
    #     print(line)
    #     # yield line
    #
    # return
    #
    # while True:
    #     output = process.stdout.readline()
    #     print(output)
    #     if output == '' and process.poll() is not None:
    #         break
    #     if output:
    #         print(output.strip())
    rc = process.poll()
    return rc

# Adjust the filtration setup file to adjust input file
def adjust_process003_input(input_dir):
    '''
    Adjusts the processPolarisData_003.py script to have the correct input directory before running

    @params
    input_dir       - Required: The directory the Polaris data is held in

    '''

    f = open(PROCESS_DIR + 'processPolarisData_003.py', 'r')       # open the file
    lines = f.readlines()                   # read all the lines into an array

    lines[50] = 'INPUT_DIR = \'' + input_dir + '/\'\n'

    f.close()

    wf = open(PROCESS_DIR + 'processPolarisData_003.py', 'w')       # open the file

    for line in lines:
        wf.write(line)

    wf.close()

    return

# Adjust the filtration setup file to adjust energy window
def adjust_process003_energy(window):
    '''
    Adjusts the processPolarisData_003.py script to have the correct energy window before running

    @params
    window       - Required: The energy window upper and lower bound in MeV (Int[])

    '''

    f = open(PROCESS_DIR + 'processPolarisData_003.py', 'r')       # open the file
    lines = f.readlines()                   # read all the lines into an array

    lines[143] = 'BACKTOBACK_COINCIDENCE_ENERGY_WINDOW = numpy.array( [' + str(window[0]) + ', ' + str(window[1]) + '] )  # energy in MeV, [min, max] values\n'

    f.close()

    wf = open(PROCESS_DIR + 'processPolarisData_003.py', 'w')       # open the file

    for line in lines:
        wf.write(line)

    wf.close()

    return

# Adjust the filtration setup file to adjust timing window
def adjust_process003_timing(t):
    '''
    Adjusts the processPolarisData_003.py script to have the correct timing window before running

    @params
    t       - Required: The timing window in 100*nanoseconds (Int)

    '''

    f = open(PROCESS_DIR + 'processPolarisData_003.py', 'r')       # open the file
    lines = f.readlines()                   # read all the lines into an array

    lines[141] = 'BACKTOBACK_COINCIDENCE_TIMING = ' + str(t) + 'E-7     # time in seconds for grouping events, i.e. 5E-7 = 500 ns\n'

    f.close()

    wf = open(PROCESS_DIR + 'processPolarisData_003.py', 'w')       # open the file

    for line in lines:
        wf.write(line)

    wf.close()

    return

# Run a sweep on the energy window with the f optimisation
def e_sweep(input_dir, e_params):
    '''
    Runs a sweep on the energy window around the 511keV peak, running the f
    optimisation on each window, and plots the results to find the maximum.

    @params
        input_dir       - Required: The directory the Polaris dataset is held in (Str)
        e_params        - Required: The (start, stop, step) parameters for the energy sweep in MeV (Int[])

    @returns
        Null
    '''

    e_windows = np.arange(e_params[0], e_params[1], e_params[2])        # set of energy windows to sweep over

    adjust_process003_input(input_dir)        # adjust the processPolarisData003.py file to run on the correct dataset

    fopts = []
    e_wins = []

    for e in e_windows:
        window = [0.511-e, 0.511+e]             # energy window
        adjust_process003_energy(window)    # adjust processing script
        run_command('python processPolarisData_003.py', input_dir)      # run the coincidence processing

        best_opt, best_pos, best_ucert = ppp.f_optimisation(input_dir + '/pet_setup-coincidences_btb.csv', [80,99,1], 2000, False)       # run f optimisation
        fopts.append(best_opt)
        e_wins.append(e)

    plt.scatter(e_wins, fopts)
    plt.show()

    return

# Run a sweep on the timing window with the f optimisation
def t_sweep(input_dir, t_params):
    '''
    Runs a sweep on the timing window to group coincidence events, running the f
    optimisation on each window, and plots the results to find the maximum.

    @params
        input_dir       - Required: The directory the Polaris dataset is held in (Str)
        t_params        - Required: The (start, stop, step) parameters for the timing sweep in 100*ns (Int[])

    @returns
        Null
    '''

    t_windows = np.arange(t_params[0], t_params[1], t_params[2])        # set of timing windows to sweep over

    adjust_process003_input(input_dir)        # adjust the processPolarisData003.py file to run on the correct dataset

    fopts = []
    t_wins = []

    for t in t_windows:
        adjust_process003_timing(t)    # adjust processing script
        run_command('python processPolarisData_003.py', input_dir)      # run the coincidence processing

        best_opt, best_pos, best_ucert = ppp.f_optimisation(input_dir + '/pet_setup-coincidences_btb.csv', [80,99,1], 2000, False)       # run f optimisation
        fopts.append(best_opt)
        t_wins.append(t)

    plt.scatter(t_wins, fopts)
    plt.show()

    return

def main():

    # e_sweep('/home/user/Documents/data/190226/J_190226-run10-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s', [0.01, 0.11, 0.01])
    t_sweep('/home/user/Documents/data/190226/J_190226-run10-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s', [1, 11, 1])


    return

if __name__ == '__main__':
    main()
