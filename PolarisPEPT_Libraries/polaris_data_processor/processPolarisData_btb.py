#!/usr/bin/python
"""
Description:
  Python script that reads in raw data from Polaris (typically named: AllEventsCombined.txt)
    and can: perform a coordinate transformation on the data
             look for back-to-back coincidence events (but not multiple-stage) - new for v003
             creates csv data file
             output lots of interesting plots

  To Run: python processPolarisData_003.py
    Settings are handled through Global Variable under SETTINGS
      - INPUT_FILE                  = name (and directory) of input file (raw Polaris data file)
      - OUTPUT_FILE                 = name of output file, will save in current directory
      - PRODUCE_PLOTS               = True (create plots) / False (do not create plots)
      - PLOTS_OUTPUT_DIR            = directory for plots to be saved (will create if does not exist)
      - D0_TRANSFORMATION           = rotation/translation details for D0
      - D1_TRANSFORMATION           = rotation/translation details for D1


NOTES: 
- written using python v2.7.14 / updated to also work in python 3.6.2
- processPolarisData_utils.pyx required to run this code
- some aspects of the code only function for double-scatters
- removed cython aspects of processPolarisData code

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "September 04, 2018"
__version__ = "$Revision: 3.3.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import os
import numpy
import matplotlib.pyplot as plt
import pandas

#import pyximport; pyximport.install()
import processPolarisData_btb_utils as utils
import timeit



#------------------------------------------------------------------
# SETTINGS / DECLARATIONS
#------------------------------------------------------------------

### INPUT FILES
## 180425 Data ##
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180425/J_180425-na22-pet_setup-run01-1200s/AllEventsCombined-500.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180425/J_180425-na22-pet_setup-run01-1200s/AllEventsCombined.txt"
## 180808 Data ##
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180808/J_180808-run1-na22-oem2_x_-68mm-oem3_x_68mm-1200s/AllEventsCombined.txt"
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180808/J_180808-run2-na22-oem2_x_-93mm-oem3_x_68mm-1800s/AllEventsCombined.txt"
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180808/J_180808-run3-na22-oem2_x_-93mm-oem3_x_93mm-2400s/AllEventsCombined.txt"
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180808/J_180808-run4-na22-oem2_x_-118mm-oem3_x_93mm-3600s/AllEventsCombined.txt"
## 180823 Data ##
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180823/J_180823-run1-na22-oem2_x_-68mm-oem3_x_68mm-9000s/AllEventsCombined-1000.txt"
INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180823/J_180823-run1-na22-oem2_x_-68mm-oem3_x_68mm-9000s/AllEventsCombined.txt"
#INPUT_FILE = "/Volumes/BATMAN/PGI/data/uct/polaris/180823/J_180823-run3-na22-oem2_x_-93mm-oem3_x_93mm-14400s/AllEventsCombined.txt"

### OUTPUT FILES
## 180425 Data ##
#OUTPUT_FILE = "180425-na22-pet_setup-run01-1200s-coincidences.csv"
## 180808 Data ##
#OUTPUT_FILE = "180808-run1-na22-pet_setup-coincidences.csv"
#OUTPUT_FILE = "180808-run2-na22-pet_setup-coincidences.csv"
#OUTPUT_FILE = "180808-run3-na22-pet_setup-coincidences.csv"
#OUTPUT_FILE = "180808-run4-na22-pet_setup-coincidences.csv"
## 180823 Data ##
OUTPUT_FILE = "180823-run1-na22-pet_setup-coincidences.csv"
#OUTPUT_FILE = "180823-run3-na22-pet_setup-coincidences.csv"
#OUTPUT_FILE = "temp.csv"

### PLOTTING
PRODUCE_BASIC_PLOTS = False
PRODUCE_COINCIDENCE_PLOTS = True
## 180425 Data ##
#PLOTS_OUTPUT_DIR = "180425-na22-pet_setup-run01-1200s-plots"
## 180808 Data ##
#PLOTS_OUTPUT_DIR = "180808-run1-na22-pet_setup-plots"
#PLOTS_OUTPUT_DIR = "180808-run2-na22-pet_setup-plots"
#PLOTS_OUTPUT_DIR = "180808-run3-na22-pet_setup-plots"
#PLOTS_OUTPUT_DIR = "180808-run4-na22-pet_setup-plots"
## 180823 Data ##
PLOTS_OUTPUT_DIR = "180823-run1-na22-pet_setup-plots"
#PLOTS_OUTPUT_DIR = "180823-run3-na22-pet_setup-plots"
#PLOTS_OUTPUT_DIR = "temp"
MAX_ENERGY = 600   # maximum energy (in keV) for alternate energy plots

### Detector details / coordinate transformation
# format: (rot[0], rot[1], rot[2], pos[0], pos[1], pos[2]) -> units are degrees and mm
## 180425 Data ##
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -48.0, 0.0, 0.0]  # D0 / 180425 / Run 1
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 48.0, 0.0, 0.0]    # D1 / 180425 / Run 1
## 180808 Data ##
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -68.0, 0.0, 0.0]  # D0 / 180808 / Run 1
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 68.0, 0.0, 0.0]    # D1 / 180808 / Run 1
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -93.0, 0.0, 0.0]  # D0 / 180808 / Run 2
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 68.0, 0.0, 0.0]    # D1 / 180808 / Run 2
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -93.0, 0.0, 0.0]  # D0 / 180808 / Run 3
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 93.0, 0.0, 0.0]    # D1 / 180808 / Run 3
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -118.0, 0.0, 0.0]  # D0 / 180808 / Run 4
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 93.0, 0.0, 0.0]    # D1 / 180808 / Run 4
## 180823 Data ##
D0_TRANSFORMATION = [180.0, 90.0, 0.0, -68.0, 0.0, 0.0]  # D0 / 180823 / Run 1
D1_TRANSFORMATION = [0.0, 270.0, 0.0, 68.0, 0.0, 0.0]    # D1 / 180823 / Run 1
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -93.0, 0.0, 0.0]  # D0 / 180823 / Run 3
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 93.0, 0.0, 0.0]    # D1 / 180823 / Run 3

### Coincidence grouping
APPLY_COINCIDENCE_GROUPING_BACKTOBACK = True
BACKTOBACK_COINCIDENCE_TIMING = 5E-7   # time in seconds for grouping events, i.e. 5E-7 = 500 ns
BACKTOBACK_COINCIDENCE_TIME_WINDOW = numpy.array( [0.00, 1.0E10] )   # time in sec, [min, max] values
BACKTOBACK_COINCIDENCE_ENERGY_WINDOW = numpy.array( [0.450, 0.550] )  # energy in MeV, [min, max] values



#------------------------------------------------------------------
# MAIN PROGRAM
#------------------------------------------------------------------
def main():
    print ('\n--- Running processPolarisData_btb.py ---')

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  checking if plots directory exists, if not, create
    if (PRODUCE_BASIC_PLOTS or PRODUCE_COINCIDENCE_PLOTS):
        if not os.path.exists(PLOTS_OUTPUT_DIR):
            os.makedirs(PLOTS_OUTPUT_DIR)
            print ('  Creating directory: {}'.format(PLOTS_OUTPUT_DIR))


    ### READ IN INPUT FILE ###

    """
    # reads file into array
    # loadtxt - Load data from a text file / Each row in the text file must have the same number of values.
    #
    # format of data
    # column 0 - Number of interactions
    # column 1 - Detector Number [the first IP is System 0 and the second IP is system 1]
    # column 2 - Energy in keV
    # column 3 - X-position in mm
    # column 4 - Y-position in mm
    # column 5 - Z-position in mm
    # column 6 - time stamp in 10 * nanoseconds
    """

    #  reading data file into a structured numpy data type array (dtype) 
    print ('  Reading input file, could take several minutes . . .'.format(INPUT_FILE))
    print ('   - Filename : {}'.format(INPUT_FILE))
    data = numpy.loadtxt(INPUT_FILE, dtype={'names': ('NumInt', 'DetNum', 'EngDep', 'xPos', 'yPos', 'zPos', 'Time'),
                                         'formats': (numpy.int64, numpy.int64, float, float, float, float, numpy.int64)})  # int64 to fix Windows error: OverflowError: Python int too large to convert to C long
    print ('    - read in {} events / time required {} s'.format(len(data), timeit.default_timer() - start))


    #  changing data format in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('  Converting the data into pandas format . . .')
    #  pandas.DataFrame is a two-dimensional size-mutable, potentially heterogeneous tabular data structure with labeled axes
    pandas_data = {'scatters': data["NumInt"], 'detector': data["DetNum"], 'energy': data["EngDep"], 'x': data["xPos"], 'y': data["yPos"], 'z': data["zPos"], 'time': data["Time"]}
    cc_dataframe = pandas.DataFrame(pandas_data)

    # reorder cc_dataframe into same order as input file (becomes important in utils.apply_transformation)
    cc_dataframe = cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']]

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)

    #  output time details
    #   - convert time into seconds (t given in units of 10 ns clock cycles), so tS = t * 10E-9
    time_in_sec = cc_dataframe['time'] * (10E-9)
    time_start = numpy.amin(time_in_sec)
    time_end = numpy.amax(time_in_sec)
    #   - print to screen
    print ('   - Start time (s): {} | End time (s): {} | Run duration (s): {}'.format(time_start, time_end, (time_end - time_start)))
    print ('    - run activity (events/s): {}'.format(len(cc_dataframe) / (time_end - time_start)))

    # Counters for run summary
    num_1px, num_2px, num_3px, num_4px = [float('nan')] * 4                # count events based on pixel number
    num_ss_bb = [float('nan')]                                             # count number of back-to-back coincidence events


    ### PLOTTING BASIC DETECTOR DATA ###

    if PRODUCE_BASIC_PLOTS:
        print ('\n--- Making Basic Detector Plots (using raw data) ---')

        #  plotting raw data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'raw_all', PLOTS_OUTPUT_DIR)
        #  alternate plots for raw data of 2D xy profiles
        d0_xPos = cc_dataframe[cc_dataframe.detector == 0].x.as_matrix()
        d0_yPos = cc_dataframe[cc_dataframe.detector == 0].y.as_matrix()
        d1_xPos = cc_dataframe[cc_dataframe.detector == 1].x.as_matrix()
        d1_yPos = cc_dataframe[cc_dataframe.detector == 1].y.as_matrix()    
        plt.cla(); plt.clf()
        plt.plot(d0_yPos, d0_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D0 / Raw Data, total: %s'%(len(d0_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D0_y_x_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        plt.plot(d1_yPos, d1_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D1 / Raw Data, total: %s'%(len(d1_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D1_y_x_alternate", PLOTS_OUTPUT_DIR)
        #  alternate plots for energy deposition of raw data
        plt.cla(); plt.clf()
        utils.create_plot_range(cc_dataframe.energy, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters, total: %s'%(len(cc_dataframe.energy)))
        utils.save_to_file("raw_all_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        d0_eng = cc_dataframe[cc_dataframe.detector == 0].energy.as_matrix()
        utils.create_plot_range(d0_eng, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters / D0, total: %s'%(len(d0_eng)))
        utils.save_to_file("raw_all_D0_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)
        d1_eng = cc_dataframe[cc_dataframe.detector == 1].energy.as_matrix()
        plt.cla(); plt.clf()
        utils.create_plot_range(d1_eng, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters / D1, total: %s'%(len(d1_eng)))
        utils.save_to_file("raw_all_D1_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)


    ### TRANSFORMING DATA COORDINATES ###

    #  taking pieces of Dennis' ccanywhere code to translate and rotate detector coordinates
    print ('\n--- Transforming Detector Coordinates ---')
    transformation_matrices = {}

    #  creating transformation matrices
    transformation_matrices[0] = utils.get_transformation_matrix_array(D0_TRANSFORMATION)
    transformation_matrices[1] = utils.get_transformation_matrix_array(D1_TRANSFORMATION)

    #  applying coordinate transformation to data
    print ('  Transforming events to reconstruction coordinate system . . .')
    cc_dataframe = utils.apply_transformation(cc_dataframe, transformation_matrices)

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)

    # count number of pixel events
    num_1px = len(cc_dataframe[cc_dataframe.scatters == 1]);  num_2px = len(cc_dataframe[cc_dataframe.scatters == 2])
    num_3px = len(cc_dataframe[cc_dataframe.scatters == 3]);  num_4px = len(cc_dataframe[cc_dataframe.scatters  > 3])



    ### PLOTTING TRANSFORMED DETECTOR DATA ###

    if PRODUCE_BASIC_PLOTS:
        print ('\n--- Making Basic Detector Plots (using transformed data) ---')

        #  plotting transformed data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'transformed_all', PLOTS_OUTPUT_DIR)
        #  plotting based on number of pixel interactions (1, 2, 3, 4+)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 1], 'transformed_1px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 2], 'transformed_2px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 3], 'transformed_3px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters > 3], 'transformed_4px+', PLOTS_OUTPUT_DIR)
        #  plotting 1D profiles (x, y, z) across all detectors
        utils.plot_1D(cc_dataframe.x.as_matrix(), 200, "x", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.y.as_matrix(), 200, "y", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.z.as_matrix(), 200, "z", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)


    ### GROUPING COINCIDENCE EVENTS ###

    print ('\n--- Grouping Coincidence Events ---')

    #  groups events from different modules based on energy and time stamp
    #   - checks for single pixel coincident ~511 keV energy deposition in D0 and D1 with time window
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_COINCIDENCE_GROUPING_BACKTOBACK:
        print ('  Grouping back-to-back coincidence events (511 keV from positron source) . . .')

        #  filtering raw data to produce list of back to back coincident 511 keV gammas
        #   - btb_coin_evts: time1, x1, y1, z1, time2, x2, y2, z2
        #   - btb_coin_data: deltaT, module1, module2, eng1, eng2
        btb_coin_evts, btb_coin_data = utils.grouping_backtoback_coincidence_events(cc_dataframe, BACKTOBACK_COINCIDENCE_TIMING, BACKTOBACK_COINCIDENCE_TIME_WINDOW, BACKTOBACK_COINCIDENCE_ENERGY_WINDOW)

        # count final number of back-to-back events
        num_ss_bb = len(btb_coin_evts);

        ### SAVING DATA TO CSV ###
        print ('\n--- Saving Coincidence Data ---')

        #  creating output file names
        output_file = OUTPUT_FILE.replace(".txt", "").replace(".dat", "").replace(".csv", "") + ".csv"
        output_file_btb = output_file.replace(".csv", "_btb.csv")

        # Outputting data to CSV 
        #  format: time1, x1, y1, z1, time2, x2, y2, z2
        #  units: time in us and pos in mm
        print ('  Saving data array ({}) to file (CSV format): {}'.format('btb_coin_evts', output_file_btb))
        print ('    - storing {} coincidences'.format(num_ss_bb))
        numpy.savetxt(output_file_btb, btb_coin_evts, delimiter=',', fmt='%.4f')



        ### PLOTTING COINCIDENCE DATA ###

        if PRODUCE_COINCIDENCE_PLOTS:

            #  converting btb_coin_data
            deltaTM = btb_coin_data[0]
            E1 = btb_coin_data[3]; E2 = btb_coin_data[4]

            #  calculating number of 10 ns clock cycles for grouping (based on timing setting)
            number_of_clock_cycles = BACKTOBACK_COINCIDENCE_TIMING/(10E-9)
    
            #  binning number of coincidences vs time difference
            binRedux = 1
            n1, b = numpy.histogram(deltaTM, bins = int(number_of_clock_cycles/binRedux))
            t1 = 0.5 * (b[1:] + b[:-1])

            #  creating corresponding CDF
            totCounts = float(numpy.sum(n1))
            cdf = numpy.zeros((len(t1), 1))
            for iii in range(len(t1)):
                cdf[iii] = numpy.sum(n1[:iii]) / totCounts

            # Plotting coincidences [two subplots]
            plt.cla(); plt.clf()  # need to clear the plot if not running show(), which clears automatically
            f, (ax1, ax2) = plt.subplots(2, 1, sharex = True, sharey = False)

            # Plotting number of coincidences vs time difference
            ax1.plot(t1[:-1], n1[:-1], '-')
            ax1.set_title(' %i coincidence events' % deltaTM.shape[0], fontsize=9)
            ax1.set_xlabel('Time difference [10 ns]', fontsize=9)
            ax1.set_ylabel('Number of doubles', fontsize=9)
            ax1.grid()

            # Plotting CDF of number of coincidences vs time difference
            ax2.plot(t1[:-1], cdf[:-1])
            ax2.set_xlabel('Time difference [10 ns]', fontsize=9)
            ax2.set_ylabel('CDF', fontsize=9)
            ax2.grid()

            # save current plot to file
            utils.save_to_file("coincidence_events_timing_plot", PLOTS_OUTPUT_DIR)

            # Plotting coincidence events
            plt.cla(); plt.clf()  # need to clear the plot if not running show(), which clears automatically
            utils.create_plot_range(E1[:], 'Energy Deposited (keV)', 200, 400, MAX_ENERGY)
            plt.title('Energy Deposited / First Coincidence Event, total: %s' % deltaTM.shape[0], fontsize=9)
            # save current plot to file
            utils.save_to_file("coincidence_events_energy_deposited1_plot", PLOTS_OUTPUT_DIR)

            plt.cla(); plt.clf()  # need to clear the plot if not running show(), which clears automatically
            utils.create_plot_range(E2[:], 'Energy Deposited (keV)', 200, 400, MAX_ENERGY)
            plt.title('Energy Deposited / Second Coincidence Event, total: %s' % deltaTM.shape[0], fontsize=9)
            # save current plot to file
            utils.save_to_file("coincidence_events_energy_deposited2_plot", PLOTS_OUTPUT_DIR)


            ### SIMPLE LINE OF RESPONSE PLOTS ###
            print ('\n--- Plotting Simple Line of Response Images ---')

            #  save position data
            p0_xPos = btb_coin_evts[:, 1]; p0_yPos = btb_coin_evts[:, 2]
            p1_xPos = btb_coin_evts[:, 5]; p1_yPos = btb_coin_evts[:, 6]
            all_xPos = numpy.concatenate((p0_xPos, p1_xPos), axis = 0)
            all_yPos = numpy.concatenate((p0_yPos, p1_yPos), axis = 0)

            # Plot coincidences - points
            plt.cla(); plt.clf()
            plt.plot(all_xPos, all_yPos, color='green', marker='o', linestyle='None', markersize=0.5)
            plt.title('XY Position of all coincidences locations / Translated Coords, total: %s'%(len(p0_xPos)), fontsize=9)
            plt.xlabel('X-Position (mm)', fontsize=9); plt.ylabel('Y-Position (mm)', fontsize=9)
            utils.save_to_file("coincidence_events_xy_position_2D", PLOTS_OUTPUT_DIR)

            # Plot coincidences - lines (100)
            if len(p0_xPos) > 100:
                plt.cla(); plt.clf()
                num = 100
                for i in range(0, num):
                    plt.plot([p0_xPos[i], p1_xPos[i]], [p0_yPos[i], p1_yPos[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
                plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=9)
                plt.xlabel('X-Position (mm)', fontsize=9); plt.ylabel('Y-Position (mm)', fontsize=9)
                utils.save_to_file("coincidence_events_xy_position_2D-lines-100", PLOTS_OUTPUT_DIR)

            # Plot coincidences - lines (5000)
            if len(p0_xPos) > 5000:
                plt.cla(); plt.clf()
                num = 5000
                for i in range(0, num):
                    plt.plot([p0_xPos[i], p1_xPos[i]], [p0_yPos[i], p1_yPos[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
                plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=9)
                plt.xlabel('X-Position (mm)', fontsize=9); plt.ylabel('Y-Position (mm)', fontsize=9)
                utils.save_to_file("coincidence_events_xy_position_2D-lines-5000", PLOTS_OUTPUT_DIR)
            else:
            # Plot coincidences - lines (all)
                plt.cla(); plt.clf()
                for i in range(0, len(p0_xPos)):
                    plt.plot([p0_xPos[i], p1_xPos[i]], [p0_yPos[i], p1_yPos[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
                plt.title('XY Position of all coincidences locations with lines / Translated Coords, total: %s'%(len(p0_xPos)), fontsize=9)
                plt.xlabel('X-Position (mm)', fontsize=9); plt.ylabel('Y-Position (mm)', fontsize=9)
                utils.save_to_file("coincidence_events_xy_position_2D-lines-all", PLOTS_OUTPUT_DIR)


    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    print ('-- Total events: {}, in file: {}'.format(len(data), INPUT_FILE))
    print ('-- Number of 1x pixel events: {}, 2x pixel events: {}, 3x pixel events: {}, 4x+ pixel events: {}'.format(num_1px, num_2px, num_3px, num_4px))
    print ('--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    print ('-- Run Options -> Back-to-Back Coincidence: {}'.format(APPLY_COINCIDENCE_GROUPING_BACKTOBACK))
    if APPLY_COINCIDENCE_GROUPING_BACKTOBACK: print ('--- Back-to-back coincidence settings -> Timing (s): {} / Time Window [min, max] (s): {} / Energy Window [min, max] (MeV): {}'.format(BACKTOBACK_COINCIDENCE_TIMING, BACKTOBACK_COINCIDENCE_TIME_WINDOW, BACKTOBACK_COINCIDENCE_ENERGY_WINDOW))
    print ('-- Back-to-Back Scatters Events -> Initial: {} | BB coincidence: {} | Final: {}'.format(num_1px, num_ss_bb, num_ss_bb))
    print ('\n-------------------------\n')



if __name__ == "__main__":

    main()
