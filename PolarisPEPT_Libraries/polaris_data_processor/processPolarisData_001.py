#!/usr/bin/python
"""
Description:
  Python script that reads in raw data from Polaris (typically named: AllEventsCombined.txt)
    and can: perform a coordinate transformation on the data
             does Big Energy sorting on the data
             creates csv data file
             output lots of interesting plots

  To Run: python processPolarisData001.py
    Settings are handled through Global Variable under SETTINGS
      - INPUT_FILE                  = name (and directory) of input file (raw Polaris data file)
      - OUTPUT_FILE                 = name of output file, will save in current directory
      - PRODUCE_PLOTS               = True (create plots) / False (do not create plots)
      - PLOTS_OUTPUT_DIR            = directory for plots to be saved (will create if does not exist)
      - D0_TRANSFORMATION           = rotation/translation details for D0
      - D1_TRANSFORMATION           = rotation/translation details for D1
      - APPLY_BIG_ENERGY_SORTING    = apply big energy sorting (autoamitcally arrange events so that bigger energy deposited comes first, default is True)

NOTES:
- written using python v2.7.14
- processPolarisData_utils.pyx required to run this code

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "June 22, 2018"
__version__ = "$Revision: 1.0.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import os
import numpy as np
import matplotlib.pyplot as plt
import pandas

import pyximport; pyximport.install()
import processPolarisData_utils as utils
import timeit



#------------------------------------------------------------------
# SETTINGS / DECLARATIONS
#------------------------------------------------------------------

### Input / output files
# INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run1-co60-cs137-oem2_x_-93mm-oem3_y_94mm-4hrs/AllEventsCombined.txt"
INPUT_DIR = '/home/user/Documents/data/190612/190612-run21-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_30/AllEventsCombined.txt'
OUTPUT_FILE = "180618-run1-final_coords-energy_sort.csv"

### Plotting
PRODUCE_PLOTS = True
PLOTS_OUTPUT_DIR = "plots"

### Detector details / coordinate transformation
# format: (rot[0], rot[1], rot[2], pos[0], pos[1], pos[2]) -> units are degrees and mm
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -48.0, 0.0, 0.0]  # D0 / 180425 / Run 1
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 48.0, 0.0, 0.0]    # D1 / 180425 / Run 1
D0_TRANSFORMATION = [180.0, 90.0, 0.0, -93.0, 0.0, 0.0]  # D0 / 180618 / Run 1
D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 94.0, 0.0]    # D1 / 180618 / Run 1

### Data filtering
APPLY_BIG_ENERGY_SORTING = True    # default value is True



#------------------------------------------------------------------
# MAIN PROGRAM
#------------------------------------------------------------------
def main():
    print '\n--- Running processPolarisData_001.py ---'

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  checking if plots directory exists, if not, create
    if PRODUCE_PLOTS:
        if not os.path.exists(PLOTS_OUTPUT_DIR):
            os.makedirs(PLOTS_OUTPUT_DIR)
            print '  Creating directory: {}'.format(PLOTS_OUTPUT_DIR)


    ### READ IN INPUT FILE ###

    """
    # reads file into array
    # loadtxt - Load data from a text file / Each row in the text file must have the same number of values.
    #
    # format of data
    # column 0 - Number of interactions
    # column 1 - Detector Number [the first IP is System 0 and the second IP is system 1]
    # column 2 - Energy in keV
    # column 3 - X-position in mm
    # column 4 - Y-position in mm
    # column 5 - Z-position in mm
    # column 6 - time stamp in 10 * nanoseconds
    """

    #  reading data file into a structured numpy data type array (dtype)
    print '  Reading input file, could take several minutes . . .'.format(INPUT_FILE)
    print '   - filename : {}'.format(INPUT_FILE)
    data = np.loadtxt(INPUT_FILE, dtype={'names': ('NumInt', 'DetNum', 'EngDep', 'xPos', 'yPos', 'zPos', 'Time'),
                                         'formats': (int, int, float, float, float, float, int)})
    print '   - read in {} events / time required {} s'.format(len(data), timeit.default_timer() - start)

    #  changing data format in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print '  Converting the data into pandas format . . .'
    #  pandas.DataFrame is a two-dimensional size-mutable, potentially heterogeneous tabular data structure with labeled axes
    pandas_data = {'scatters': data["NumInt"], 'detector': data["DetNum"], 'energy': data["EngDep"], 'x': data["xPos"], 'y': data["yPos"], 'z': data["zPos"], 'time': data["Time"]}
    cc_dataframe = pandas.DataFrame(pandas_data)

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)


    ### PLOTTING BASIC DETECTOR DATA ###

    if PRODUCE_PLOTS:
        print '\n--- Making Basic Detector Plots (using raw data) ---'

        #  plotting raw data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'raw_all', PLOTS_OUTPUT_DIR)
        #  alternate plots for raw data of 2D xy profiles
        d0_xPos = cc_dataframe[cc_dataframe.detector == 0].x.as_matrix()
        d0_yPos = cc_dataframe[cc_dataframe.detector == 0].y.as_matrix()
        d1_xPos = cc_dataframe[cc_dataframe.detector == 1].x.as_matrix()
        d1_yPos = cc_dataframe[cc_dataframe.detector == 1].y.as_matrix()
        plt.cla(); plt.clf()
        plt.plot(d0_yPos, d0_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D0 / Raw Data, total: %s'%(len(d0_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D0_y_x_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        plt.plot(d1_yPos, d1_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D1 / Raw Data, total: %s'%(len(d1_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D1_y_x_alternate", PLOTS_OUTPUT_DIR)



    ### TRANSFORMING DATA COORDINATES ###

    #  taking pieces of Dennis' ccanywhere code to translate and rotate detector coordinates
    print '\n--- Transforming Detector Coordinates ---'
    transformation_matrices = {}

    #  creating transformation matrices
    transformation_matrices[0] = utils.get_transformation_matrix_array(D0_TRANSFORMATION)
    transformation_matrices[1] = utils.get_transformation_matrix_array(D1_TRANSFORMATION)

    #  applying coordinate transformation to data
    print '  Transforming events to reconstruction coordinate system . . .'
    cc_dataframe = utils.apply_transformation(cc_dataframe, transformation_matrices)

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)


    ### PLOTTING TRANSFORMED DETECTOR DATA ###

    if PRODUCE_PLOTS:
        print '\n--- Making Basic Detector Plots (using transformed data) ---'

        #  plotting transformed data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'transformed_all', PLOTS_OUTPUT_DIR)
        #  plotting based on number of pixel interactions (1, 2, 3, 4+)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 1], 'transformed_1px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 2], 'transformed_2px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 3], 'transformed_3px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters > 3], 'transformed_4px+', PLOTS_OUTPUT_DIR)
        #  plotting 1D profiles (x, y, z) across all detectors
        utils.plot_1D(cc_dataframe.x.as_matrix(), 200, "x", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.y.as_matrix(), 200, "y", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.z.as_matrix(), 200, "z", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)



    ### GROUPING COMPTON SCATTER DATA ###

    print '\n--- Grouping Compton Scatter Data ---'

    if APPLY_BIG_ENERGY_SORTING:
        print '  Sorting scatters to place largest energy deposition first . . .'

    #  returns interaction data (E, X, Y, Z) for the specified number of scatters and detector number
    #   format -> def get_interaction_data(df, energy_sort = True, num_scatters = 2, det_num = None)
    scatters_2x = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_d0 = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2, 0)
    scatters_2x_d1 = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2, 1)
    scatters_3x = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 3)



    ### FORMATTING DATA FOR OUTPUT ###

    print '\n--- Formatting Compton Scatter Data for Output ---'
    print '  Re-arranging data array for CSV output / re-scaling energy from keV to MeV . . .'

    #  re-arranges interaction data into CSV format (entire event on one line)
    #   format -> def format_data_for_output(scatters, num_scatters, det_num = None):
    scatters_2x_out = utils.format_data_for_output(scatters_2x, 2)
    scatters_2x_d0_out = utils.format_data_for_output(scatters_2x_d0, 2, 0)
    scatters_2x_d1_out = utils.format_data_for_output(scatters_2x_d1, 2, 1)
    scatters_3x_out = utils.format_data_for_output(scatters_3x, 3)



    ### SAVING DATA TO CSV ###

    print '\n--- Saving Polaris Data to CSV ---'

    #  creating output file names
    output_file = OUTPUT_FILE.replace(".txt", "").replace(".dat", "").replace(".csv", "") + ".csv"
    output_file_2x = output_file.replace(".csv", "_2x.csv")
    output_file_2x_d0 = output_file.replace(".csv", "_2x_d0.csv")
    output_file_2x_d1 = output_file.replace(".csv", "_2x_d1.csv")
    output_file_3x = output_file.replace(".csv", "_3x.csv")

    #  outputting data to CSV
    #   format: eng1, x1, y1, z1, eng2, x2, y2, z2, (eng3, x3, y3, z3) <- if triple scatter
    #   units: time in us and pos in mm
    print '  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x)
    np.savetxt(output_file_2x, scatters_2x_out, delimiter=',', fmt='%.5f')
    print '  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_out), 'scatters_2x_d0_out', output_file_2x_d0)
    np.savetxt(output_file_2x_d0, scatters_2x_d0_out, delimiter=',', fmt='%.5f')
    print '  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_out), 'scatters_2x_d1_out', output_file_2x_d1)
    np.savetxt(output_file_2x_d1, scatters_2x_d1_out, delimiter=',', fmt='%.5f')
    print '  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_3x)
    np.savetxt(output_file_3x, scatters_3x_out, delimiter=',', fmt='%.5f')

    #  saving combined 2x and 3x data (using file.open in order to append data to file)
    print '  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file)
    ofile = open(output_file, "w")
    np.savetxt(ofile, scatters_2x_out, delimiter=',', fmt='%.5f')
    print '  Saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file)
    np.savetxt(ofile, scatters_3x_out, delimiter=',', fmt='%.5f')
    ofile.close()

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    print '\n--- Completed processing / time required {} s'.format(stop - start)


if __name__ == "__main__":

    main()
