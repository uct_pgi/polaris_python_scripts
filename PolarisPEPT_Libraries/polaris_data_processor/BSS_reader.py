#!/usr/bin/python
'''
Binary reader script for reading the .bss binary files produced by the H3D Polaris system.
Produces a .txt file containing the sync pulse information.
Able to handle the .bss files with the included mask position information.

NOTES:
Further details for reading binary in python can be found at:
https://www.devdungeon.com/content/working-binary-data-python#writefile

Seek can be called one of two ways:
  x.seek(offset)
  x.seek(offset, starting_point)

starting_point can be 0, 1, or 2
0 - Default. Offset relative to beginning of file
1 - Start from the current position in the file
2 - Start from the end of a file (will require a negative offset)
'''

__author__ = 'Nicholas Hyslop (nhyslop2@gmail.com)'
__date__ = '11 November 2019'

## IMPORT STATEMENTS ##
import os

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Function to read the .bss binary files and convert them to .txt
def read_binary(data_dir, filename, output_dir):
    '''
    Takes in the data directory and name of the .bss binary file, decodes it and writes it to a .txt file.

    Expected binary format:

    The first byte indicates what will follow.

    If the first byte is 122, then it indicates a timestamp data and is in the following format:
        1 byte: 122
        1 byte: 2
        8 byte: Sync pulse index
        8 byte: Sync pulse time stamp

    If the first byte is 255, then it indicates mask position data. Skip the next 23 bytes.

    @params:
        data_dir            - Required : Path to the data file (Str)
        filename            - Required : Name of the file (Str)
        output_dir          - Required : directory to write the merged data to (Str)

    @returns:
        None
    '''

    # output_file = filename[:filename.rfind('.')] + '.txt'     # Create an output filename from the given filename
    output_file = 'SyncPulse.txt'
    input_file = data_dir + filename

    # Reading in binary data file
    # ---------------------------
    print ('Reading input file', input_file, '. Processing binary and writing output to text; could take several minutes . . .')
    print ('   - Filename: {}'.format(input_file))
    print ('   - Size: {} bytes'.format(os.path.getsize(input_file)))
    print()

    # Open and read binary:
    with open(input_file, "rb") as binary_file:

        """
        The sync pulse data array has the following format:

        [
            [Sync pulse indicator (122)],
            [Number of sync pulse],
            [Sync pulse timestamp (10ns)]
        ]
        """

        curr_pos = 0            # current read position in the file
        num_pulses = 0        # to count number of sync pulses read out

        with open(output_dir + output_file, 'w') as write_file:   # open write file

            skip_header = True      # boolean to skip the file header

            while True:

                # print(curr_pos)

                # testing for end of file
                curr_pos_test = binary_file.tell()
                test = binary_file.read(1)
                if not test:
                    break
                binary_file.seek(curr_pos_test)

                # Seek a specific position in the file
                binary_file.seek(curr_pos, 0)

                while skip_header:
                    # Number of interactions
                    indicator = binary_file.read(1)
                    indicator_int = int.from_bytes(indicator, byteorder='little')

                    # print(indicator_int)

                    if indicator_int == 122:
                        skip_header = False
                        binary_file.seek(curr_pos, 0)
                        break

                    curr_pos += 1
                    # print(curr_pos)

                # Number of interactions
                indicator = binary_file.read(1)
                indicator_int = int.from_bytes(indicator, byteorder='little')

                # print(indicator_int)

                if indicator_int == 122:

                    # Next byte should be 0. If not, keep looking.
                    next_byte = binary_file.read(1)
                    next_byte_int = int.from_bytes(next_byte, byteorder='little')
                    if next_byte_int != 0:
                        curr_pos += 1
                        continue

                    # Filler for sync pulse
                    filler = binary_file.read(2)
                    # filler_int = int.from_bytes(filler, byteorder='little')

                    # Read index and time stamp information
                    index = int.from_bytes(binary_file.read(8), byteorder='little')
                    timestamp = int.from_bytes(binary_file.read(8), byteorder='little')

                    write_str = "{}\t{}".format(index,
                                                timestamp)

                    # print(write_str)

                    write_file.write(write_str + "\n")

                    curr_pos += 20
                    num_pulses += 1

                elif indicator_int == 255:
                    # print('Mask information - skip')
                    curr_pos += 24

                else:
                    # if (os.path.getsize(input_file) - curr_pos) < 200:
                    #     print('Unexpected indicator int', indicator_int, 'at position', curr_pos)
                        # break
                    curr_pos += 1

    print('Writted', output_file, 'to', output_dir)

# Function to read the .mod binary files and convert them to .txt
def read_mod_binary(data_dir, filename, output_dir):
    '''
    Takes in the data directory and name of the .mod binary file, decodes it and writes it to a .txt file.

    Expected binary format:

    The first byte indicates what will follow.

    If the first byte is 122, then it indicates a timestamp data and is in the following format:
        1 byte: 122
        1 byte: 2
        8 byte: Sync pulse index
        8 byte: Sync pulse time stamp

    If the first byte is 255, then it indicates mask position data. Skip the next 23 bytes.

    @params:
        data_dir            - Required : Path to the data file (Str)
        filename            - Required : Name of the .mod file (Str)
        output_dir          - Required : directory to write the merged data to (Str)

    @returns:
        None
    '''

    output_file = '{}.txt'.format(filename[:filename.rfind('.')])   # Create an output filename from the given filename
    input_file = data_dir + filename

    # Reading in binary data file
    # ---------------------------
    print ('Reading input file', input_file, '. Processing binary and writing output to text; could take several minutes . . .')
    print ('   - Filename: {}'.format(input_file))
    print ('   - Size: {} bytes'.format(os.path.getsize(input_file)))
    print()

    # Open and read binary:
    with open(input_file, "rb") as binary_file:

        """
        The sync pulse data array has the following format:

        [
            [Sync pulse indicator (122)],
            [Number of sync pulse],
            [Sync pulse timestamp (10ns)]
        ]
        """

        curr_pos = 0            # current read position in the file
        num_pulses = 0        # to count number of sync pulses read out
        byteorder = 'big'

        with open(output_dir + output_file, 'w') as write_file:   # open write file

            skip_header = True      # boolean to skip the file header

            while True:

                # print(curr_pos)

                # testing for end of file
                curr_pos_test = binary_file.tell()
                test = binary_file.read(1)
                if not test:
                    break
                binary_file.seek(curr_pos_test)

                # Seek a specific position in the file
                binary_file.seek(curr_pos, 0)

                # Number of interactions
                indicator = binary_file.read(1)
                indicator_int = int.from_bytes(indicator, byteorder=byteorder)

                # print(indicator_int)

                if indicator_int == 122:

                    next_byte = binary_file.read(1)     # next byte should be a 2

                    # Read index and time stamp information
                    index = int.from_bytes(binary_file.read(8), byteorder=byteorder)
                    timestamp = int.from_bytes(binary_file.read(8), byteorder=byteorder)

                    write_str = "{}\t{}\t{}\n".format(indicator_int, index, timestamp)
                    write_file.write(write_str)

                    curr_pos += 18

                # elif indicator_int == 255:
                #     print('Mask information - skip')
                #     curr_pos += 24

                elif indicator_int == 0:
                    # print('Mask information - skip')
                    curr_pos += 24

                else:
                    timestamp = int.from_bytes(binary_file.read(8), byteorder=byteorder)     # get the timestamp
                    curr_pos += 9

                    for i in range(indicator_int):      # for each interaction
                        det_num = int.from_bytes(binary_file.read(1), byteorder=byteorder)     # detector number
                        x = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                        y = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                        z = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                        energy = int.from_bytes(binary_file.read(4), byteorder=byteorder)     # get the timestamp

                        write_str = "{}\t{}\t{}\t{}\t{}\t{}\n".format(indicator_int, x, y, z, energy, timestamp)
                        write_file.write(write_str)

                        curr_pos += 17


    print('Writted', output_file, 'to', output_dir)

def main():

    data_dir = '/Volumes/Nicholas/Polaris/data/191113/P_191113-run7-na22_1uCi-centre-30min/11132019_PET4P_191113-run7-na22_1uCi-centre-30min_181818/'

    read_binary(data_dir, 'AllEvents.bss', data_dir)

    return


    data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run5-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'

    read_mod_binary(data_dir, 'mod51.dat', data_dir)
    read_mod_binary(data_dir, 'mod52.dat', data_dir)

    return

    # dataDir = '/Volumes/Nicholas/Polaris/data/191115/'
    #
    # for Subdir, Dirs, Files in os.walk(dataDir):
    #     for data_dir in Dirs:
    #         print(dataDir + data_dir)
    #         if 'ignore' in data_dir:
    #             continue
    #         for subdir, dirs, files in os.walk(dataDir + data_dir):
    #             for dir in dirs:
    #                 # print(dataDir + data_dir + '/' + dir + '/')
    #                 if 'PET' not in dir:
    #                     continue
    #                 read_binary(dataDir + data_dir + '/' + dir + '/', 'AllEvents.bss', dataDir + data_dir + '/' + dir + '/')
    #                 print('-----------------------------------------------------', '\n')
    #             break
    #     break
    #
    # return


    # dataDir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191112/P_191112-run1-na22_1uCi_centre-5min/'
    # dataDir = '/Volumes/Nicholas/Polaris/data/191112/P_191112-run9-na22_1uCi-circle_10mm_2mms/'
    dataDir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/'

    for subdir, dirs, files in os.walk(dataDir):
        for dir in dirs:
            if dir[0] == 'b' or dir[0] == 't':
                continue
            read_binary(dataDir + dir + '/', 'AllEvents.bss', dataDir + dir + '/')
        break

    return


if __name__ == '__main__':
    main()
