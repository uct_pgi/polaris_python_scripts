#!/usr/bin/python
"""
Description:
  Python script that reads in raw data from Polaris (typically named: AllEventsCombined.txt)
    and can: perform a coordinate transformation on the data
             does Big Energy sorting on the data
             do Compton line filtering on the data - new for v002
             remove non-physical events (double scatters only) - new for v002
             look for coincidence events (multiple-stage) - not yet functioning
             creates csv data file
             output lots of interesting plots

  To Run: python processPolarisData_002.py &> output-log.txt &
    Settings are handled through Global Variable under SETTINGS
      - INPUT_FILE                  = name (and directory) of input file (raw Polaris data file)
      - OUTPUT_FILE                 = name of output file, will save in current directory
      - PRODUCE_PLOTS               = True (create plots) / False (do not create plots)
      - PLOTS_OUTPUT_DIR            = directory for plots to be saved (will create if does not exist)
      - D0_TRANSFORMATION           = rotation/translation details for D0
      - D1_TRANSFORMATION           = rotation/translation details for D1
      - APPLY_BIG_ENERGY_SORTING    = apply big energy sorting (automatically arrange events so that bigger energy deposited comes first, default is True)

NOTES:
- written using python v2.7.14 / updated to also work in python 3.6.2
- processPolarisData_utils.pyx required to run this code
- some aspects of the code only function for double-scatters

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "September 05, 2018"
__version__ = "$Revision: 2.3.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import os
import numpy
import matplotlib.pyplot as plt
import pandas

import pyximport; pyximport.install(setup_args={"include_dirs":numpy.get_include()})
import processPolarisData_utils as utils
import timeit



#------------------------------------------------------------------
# SETTINGS / DECLARATIONS
#------------------------------------------------------------------

### INPUT FILES
## 180315 Data ##
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run1-co60_at_15_-05_11mm-oem2_x_-92mm-oem3_y_94mm-3hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run2-co60_at_05_-05_11mm-oem2_x_-92mm-oem3_y_94mm-3hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run3-co60_at_-05_-05_11mm-oem2_x_-92mm-oem3_y_94mm-3hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run4-co60_at_-15_-05_11mm-oem2_x_-92mm-oem3_y_94mm-3hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run5-cs137_at_-15_-05_11mm-oem2_x_-92mm-oem3_y_94mm-2hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run6-cs137_at_-05_-05_11mm-oem2_x_-92mm-oem3_y_94mm-2hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run7-cs137_at_05_-05_11mm-oem2_x_-92mm-oem3_y_94mm-2hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run7-cs137_at_05_-05_11mm-oem2_x_-92mm-oem3_y_94mm-2hrs/AllEventsCombined-500.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180315/J_180315-run8-cs137_at_15_-05_11mm-oem2_x_-92mm-oem3_y_94mm-2hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180425/J_180425-na22-pet_setup-run01-1200s/AllEventsCombined-500.txt"
## 180618 Data ##
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run1-co60-cs137-oem2_x_-93mm-oem3_y_94mm-4hrs/AllEventsCombined-500.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run2-co60-cs137-oem2_x_-243mm-oem3_y_244mm-18hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run3-co60-cs137-oem2_x_-93mm-oem3_y_94mm-5hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run4-co60-cs137-oem2_x_-193mm-oem3_y_194mm-18hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Volumes/Batman/PGI/data/uct/polaris/180618/J_180618-run5-co60-cs137-oem2_x_-143mm-oem3_y_144mm-22hrs/AllEventsCombined-2000.txt"
#INPUT_FILE = "/Volumes/Batman/PGI/data/uct/polaris/180618/J_180618-run5-co60-cs137-oem2_x_-143mm-oem3_y_144mm-22hrs/AllEventsCombined.txt"
#INPUT_FILE = "/Users/stevepeterson/PGI/data/uct/polaris/180618/J_180618-run6-co60-cs137-oem2_x_-118mm-oem3_y_119mm-8hrs/AllEventsCombined.txt"
## 180810 Data ##
#INPUT_FILE = "/Volumes/Batman/PGI/data/uct/polaris/180810/J_180810-run1-co60-oem2_x_-318mm-oem3_y_321mm-66hrs/AllEventsCombined.txt"
## 190717 Data ##
INPUT_FILE = "/home/user/Documents/data/190717/190717-run1-cs137_at_-27_43_-5mm-oem2_x_-90mm-oem3_y_94mm-15min/AllEventsCombined.txt"

### OUTPUT FILES
## 180315 Data ##
#OUTPUT_FILE = "180315-run1-co60_at_15_-05_11mm-3hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run2-co60_at_05_-05_11mm-3hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run3-co60_at_-05_-05_11mm-3hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run4-co60_at_-15_-05_11mm-3hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run5-cs137_at_-15_-05_11mm-2hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run5-cs137_at_-15_-05_11mm-2hrs-BigE-UnPhy-ComL-Shuf.csv"
#OUTPUT_FILE = "180315-run6-cs137_at_-05_-05_11mm-2hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run6-cs137_at_-05_-05_11mm-2hrs-BigE-UnPhy-ComL-Shuf.csv"
#OUTPUT_FILE = "180315-run7-cs137_at_05_-05_11mm-2hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run7-cs137_at_05_-05_11mm-2hrs-BigE-UnPhy-ComL-Shuf.csv"
#OUTPUT_FILE = "180315-run8-cs137_at_15_-05_11mm-2hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180315-run8-cs137_at_15_-05_11mm-2hrs-BigE-UnPhy-ComL-Shuf.csv"
#OUTPUT_FILE = "180425-na22-pet_setup-run01-1200s-coincidences.csv"
## 180618 Data ##
#OUTPUT_FILE = "180618-run1-co60-cs137-4hrs-BigE-UnPhy-ComL.csv"
#OUTPUT_FILE = "180618-run2-co60-cs137-18hrs-BigE-UnPhy-ComL-Cs137+Co60.csv"
#OUTPUT_FILE = "180618-run3-co60-cs137-5hrs-BigE-UnPhy-ComL-Cs137+Co60.csv"
#OUTPUT_FILE = "180618-run4-co60-cs137-18hrs-BigE-UnPhy-ComL-Cs137.csv"
#OUTPUT_FILE = "180618-run5-co60-cs137-22hrs-BigE-UnPhy-ComL-Co60.csv"
#OUTPUT_FILE = "180618-run6-co60-cs137-8hrs-BigE-UnPhy-ComL-Cs137.csv"
## 180810 Data ##
#OUTPUT_FILE = "180810-run1-co60-66hrs-UnPhy-ComL.csv"
## 190717 Data ##
OUTPUT_FILE = "190717-run1-cas137-15min-UnPhy-ComL.csv"
#OUTPUT_FILE = "temp.csv"

### PLOTTING
PRODUCE_PLOTS = True
## 180315 Data ##
#PLOTS_OUTPUT_DIR = "180315-run1-co60_at_15_-05_11mm-3hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run2-co60_at_05_-05_11mm-3hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run3-co60_at_-05_-05_11mm-3hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run4-co60_at_-15_-05_11mm-3hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run5-cs137_at_-15_-05_11mm-2hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run6-cs137_at_-05_-05_11mm-2hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run7-cs137_at_05_-05_11mm-2hrs-BigE-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "180315-run8-cs137_at_15_-05_11mm-2hrs-BigE-UnPhy-ComL-plots"
## 180618 Data ##
#PLOTS_OUTPUT_DIR = "180618-run3-co60-cs137-5hrs-BigE-UnPhy-ComL-Cs137-plots"
#PLOTS_OUTPUT_DIR = "180618-run5-co60-cs137-22hrs-BigE-UnPhy-ComL-Co60-plots"
## 190717 Data ##
PLOTS_OUTPUT_DIR = "190717-run1-cas137-15min-UnPhy-ComL-plots"
#PLOTS_OUTPUT_DIR = "temp"
MAX_ENERGY = 700   # maximum energy (in keV) for alternate energy plots

### Detector details / coordinate transformation
# format: (rot[0], rot[1], rot[2], pos[0], pos[1], pos[2]) -> units are degrees and mm
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -92.0, 0.0, 0.0]  # D0 / 180315 / All Runs
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 94.0, 0.0]    # D1 / 180315 / All Runs
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -48.0, 0.0, 0.0]  # D0 / 180425 / Run 1
#D1_TRANSFORMATION = [0.0, 270.0, 0.0, 48.0, 0.0, 0.0]    # D1 / 180425 / Run 1
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -93.0, 0.0, 0.0]  # D0 / 180618 / Run 1 & 3
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 94.0, 0.0]    # D1 / 180618 / Run 1 & 3
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -243.0, 0.0, 0.0]  # D0 / 180618 / Run 2
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 244.0, 0.0]    # D1 / 180618 / Run 2
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -193.0, 0.0, 0.0]  # D0 / 180618 / Run 4
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 194.0, 0.0]    # D1 / 180618 / Run 4
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -143.0, 0.0, 0.0]  # D0 / 180618 / Run 5
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 144.0, 0.0]    # D1 / 180618 / Run 5
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -118.0, 0.0, 0.0]  # D0 / 180618 / Run 6
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 119.0, 0.0]    # D1 / 180618 / Run 6
#D0_TRANSFORMATION = [180.0, 90.0, 0.0, -318.0, 0.0, 0.0]  # D0 / 180810 / Run 1
#D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 321.0, 0.0]    # D1 / 180810 / Run 1
D0_TRANSFORMATION = [180.0, 90.0, 0.0, -90.0, 0.0, 0.0]  # D0 / 190717 / Run 1
D1_TRANSFORMATION = [90.0, 0.0, 90.0, 0.0, 94.0, 0.0]    # D1 / 190717 / Run 1

### Data filtering
APPLY_BIG_ENERGY_SORTING = False    # default value is True
REMOVE_UNPHYSICAL_EVENTS = True
APPLY_COMPTON_LINE_FILTERING = True
COMPTON_LINE_RANGE = numpy.array( [0.960, 1.040] )  # min / max values (percentage) for Compton line filtering
#COMPTON_LINE_ENERGIES = numpy.array( [0.6617] )
# COMPTON_LINE_ENERGIES = numpy.array( [1.173, 1.332] )
COMPTON_LINE_ENERGIES = numpy.array( [0.511] )
#COMPTON_LINE_ENERGIES = numpy.array( [0.6617, 1.173, 1.332] )
#  possible gamma energies for Compton Line Filtering
#   - Oxygen Prompts: 2.742, 5.240, 6.129, 6.916, 7.116
#   - Carbon: 4.444
#   - Nitrogen: 1.635, 2.313, 5.269, 5.298
#   - Boron: 0.718
#   - Cobalt-60: 1.173, 1.332
#   - Cesium-137: 0.6617
#   - Sodium-22 : 0.5110
SHUFFLE_SCATTER_EVENTS = False

### Coincidence grouping
APPLY_COINCIDENCE_GROUPING_MULTISTAGE = False



#------------------------------------------------------------------
# MAIN PROGRAM
#------------------------------------------------------------------
def main():
    print ('\n--- Running processPolarisData_002.py ---')

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  checking if plots directory exists, if not, create
    if PRODUCE_PLOTS:
        if not os.path.exists(PLOTS_OUTPUT_DIR):
            os.makedirs(PLOTS_OUTPUT_DIR)
            print ('  Creating directory: {}'.format(PLOTS_OUTPUT_DIR))


    ### READ IN INPUT FILE ###

    """
    # reads file into array
    # loadtxt - Load data from a text file / Each row in the text file must have the same number of values.
    #
    # format of data
    # column 0 - Number of interactions
    # column 1 - Detector Number [the first IP is System 0 and the second IP is system 1]
    # column 2 - Energy in keV
    # column 3 - X-position in mm
    # column 4 - Y-position in mm
    # column 5 - Z-position in mm
    # column 6 - time stamp in 10 * nanoseconds
    """

    #  reading data file into a structured numpy data type array (dtype)
    print ('  Reading input file, could take several minutes . . .'.format(INPUT_FILE))
    print ('   - Filename : {}'.format(INPUT_FILE))
    data = numpy.loadtxt(INPUT_FILE, dtype={'names': ('NumInt', 'DetNum', 'EngDep', 'xPos', 'yPos', 'zPos', 'Time'),
                                         'formats': (numpy.int64, numpy.int64, float, float, float, float, numpy.int64)})  # int64 to fix Windows error: OverflowError: Python int too large to convert to C long
    print ('    - read in {} events / time required {} s'.format(len(data), timeit.default_timer() - start))


    #  changing data format in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('\n  Converting the data into pandas format . . .')
    #  pandas.DataFrame is a two-dimensional size-mutable, potentially heterogeneous tabular data structure with labeled axes
    pandas_data = {'scatters': data["NumInt"], 'detector': data["DetNum"], 'energy': data["EngDep"], 'x': data["xPos"], 'y': data["yPos"], 'z': data["zPos"], 'time': data["Time"]}
    cc_dataframe = pandas.DataFrame(pandas_data)

    # reorder cc_dataframe into same order as input file (becomes important in utils.apply_transformation)
    cc_dataframe = cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']]

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)

    #  output time details
    #   - convert time into seconds (t given in units of 10 ns clock cycles), so tS = t * 10E-9
    time_in_sec = cc_dataframe['time'] * (10E-9)
    time_start = numpy.amin(time_in_sec)
    time_end = numpy.amax(time_in_sec)
    #   - print to screen
    print ('   - Start time (s): {} | End time (s): {} | Run duration (s): {}'.format(time_start, time_end, (time_end - time_start)))
    print ('    - run activity (events/s): {}'.format(len(cc_dataframe) / (time_end - time_start)))

    # Counters for run summary
    num_1px, num_2px, num_3px, num_4px = [float('nan')] * 4                # count events based on pixel number
    num_ds, num_ds_d0, num_ds_d1, num_ts = [float('nan')] * 4              # count total number of double/triple scatters
    num_ds_pe, num_ds_d0_pe, num_ds_d1_pe = [float('nan')] * 3             # count number of physical events
    num_ds_cl, num_ds_d0_cl, num_ds_d1_cl = [float('nan')] * 3             # count number of Compton line filtered events
    num_ds_ms, num_ds_d0_ms, num_ds_d1_ms = [float('nan')] * 3             # count number of multistage coincidence events
    num_ds_fi, num_ds_d0_fi, num_ds_d1_fi, num_ts_fi = [float('nan')] * 4  # final number of double/triple scatters


    ### PLOTTING BASIC DETECTOR DATA ###

    if PRODUCE_PLOTS:
        print ('\n--- Making Basic Detector Plots (using raw data) ---')

        #  plotting raw data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'raw_all', PLOTS_OUTPUT_DIR)
        #  alternate plots for raw data of 2D xy profiles
        d0_xPos = cc_dataframe[cc_dataframe.detector == 0].x.as_matrix()
        d0_yPos = cc_dataframe[cc_dataframe.detector == 0].y.as_matrix()
        d1_xPos = cc_dataframe[cc_dataframe.detector == 1].x.as_matrix()
        d1_yPos = cc_dataframe[cc_dataframe.detector == 1].y.as_matrix()
        plt.cla(); plt.clf()
        plt.plot(d0_yPos, d0_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D0 / Raw Data, total: %s'%(len(d0_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D0_y_x_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        plt.plot(d1_yPos, d1_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D1 / Raw Data, total: %s'%(len(d1_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D1_y_x_alternate", PLOTS_OUTPUT_DIR)
        #  alternate plots for energy deposition of raw data
        plt.cla(); plt.clf()
        utils.create_plot_range(cc_dataframe.energy, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters, total: %s'%(len(cc_dataframe.energy)))
        utils.save_to_file("raw_all_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        d0_eng = cc_dataframe[cc_dataframe.detector == 0].energy.as_matrix()
        utils.create_plot_range(d0_eng, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters / D0, total: %s'%(len(d0_eng)))
        utils.save_to_file("raw_all_D0_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)
        d1_eng = cc_dataframe[cc_dataframe.detector == 1].energy.as_matrix()
        plt.cla(); plt.clf()
        utils.create_plot_range(d1_eng, 'Energy Deposited (keV)', 300, 0, MAX_ENERGY)
        plt.title('Total energy deposted by all scatters / D1, total: %s'%(len(d1_eng)))
        utils.save_to_file("raw_all_D1_Energy_Counts_alternate", PLOTS_OUTPUT_DIR)


    ### TRANSFORMING DATA COORDINATES ###

    #  taking pieces of Dennis' ccanywhere code to translate and rotate detector coordinates
    print ('\n--- Transforming Detector Coordinates ---')
    transformation_matrices = {}

    #  creating transformation matrices
    transformation_matrices[0] = utils.get_transformation_matrix_array(D0_TRANSFORMATION)
    transformation_matrices[1] = utils.get_transformation_matrix_array(D1_TRANSFORMATION)

    #  applying coordinate transformation to data
    print ('  Transforming events to reconstruction coordinate system . . .')
    cc_dataframe = utils.apply_transformation(cc_dataframe, transformation_matrices)

    #  convert detector, scatters, time columns from float (default) into ints
    cc_dataframe['detector'] = cc_dataframe['detector'].astype(int)
    cc_dataframe['scatters'] = cc_dataframe['scatters'].astype(int)
    cc_dataframe['time'] = cc_dataframe['time'].astype(int)

    # count number of pixel events
    num_1px = len(cc_dataframe[cc_dataframe.scatters == 1]);  num_2px = len(cc_dataframe[cc_dataframe.scatters == 2])
    num_3px = len(cc_dataframe[cc_dataframe.scatters == 3]);  num_4px = len(cc_dataframe[cc_dataframe.scatters  > 3])



    ### PLOTTING TRANSFORMED DETECTOR DATA ###

    if PRODUCE_PLOTS:
        print ('\n--- Making Basic Detector Plots (using transformed data) ---')

        #  plotting transformed data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
        utils.make_basic_detector_plots(cc_dataframe, 'transformed_all', PLOTS_OUTPUT_DIR)
        #  plotting based on number of pixel interactions (1, 2, 3, 4+)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 1], 'transformed_1px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 2], 'transformed_2px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 3], 'transformed_3px', PLOTS_OUTPUT_DIR)
        utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters > 3], 'transformed_4px+', PLOTS_OUTPUT_DIR)
        #  plotting 1D profiles (x, y, z) across all detectors
        utils.plot_1D(cc_dataframe.x.as_matrix(), 200, "x", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.y.as_matrix(), 200, "y", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)
        utils.plot_1D(cc_dataframe.z.as_matrix(), 200, "z", "Counts", "transformed_all", PLOTS_OUTPUT_DIR)



    ### GROUPING COMPTON SCATTER DATA ###

    print ('\n--- Grouping Compton Scatter Data ---')

    if APPLY_BIG_ENERGY_SORTING:
        print ('  Sorting scatters to place largest energy deposition first . . .')

    #  returns interaction data (E, X, Y, Z) for the specified number of scatters and detector number
    #   format -> def get_interaction_data(df, energy_sort = True, num_scatters = 2, det_num = None)
    scatters_2x = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_d0 = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2, 0)
    scatters_2x_d1 = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 2, 1)
    scatters_3x = utils.get_interaction_data(cc_dataframe, APPLY_BIG_ENERGY_SORTING, 3)

    ### FORMATTING DATA FOR OUTPUT ###

    print ('\n--- Formatting Compton Scatter Data for Output ---')
    print ('  Re-arranging data array for CSV output / re-scaling energy from keV to MeV . . .')

    #  re-arranges interaction data into CSV format (entire event on one line)
    #   format -> def format_data_for_output(scatters, num_scatters, det_num = None):
    scatters_2x_out = utils.format_data_for_output(scatters_2x, 2)
    scatters_2x_d0_out = utils.format_data_for_output(scatters_2x_d0, 2, 0)
    scatters_2x_d1_out = utils.format_data_for_output(scatters_2x_d1, 2, 1)
    scatters_3x_out = utils.format_data_for_output(scatters_3x, 3)

    # count initial number of double/triple scatters
    num_ds = len(scatters_2x_out);  num_ds_d0 = len(scatters_2x_d0_out)
    num_ds_d1 = len(scatters_2x_d1_out);  num_ts = len(scatters_3x_out)

    ### FILTERING DETECTOR DATA ###

    print ('\n--- Filtering Detector Data ---')

    #  removing unphysical events (Compton scatter angle == nan)
    #   - checks both ordering of two scatters and flips if original ordering in unphysical
    #   - modified code from Matt Leigh's Filter.py & CPUFunctions.cu
    if REMOVE_UNPHYSICAL_EVENTS:
        print ('  Removing unphysical events (double scatters) . . .')

        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out = utils.filtering_unphysical_double_scatters(scatters_2x_out)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out = utils.filtering_unphysical_double_scatters(scatters_2x_d0_out)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out = utils.filtering_unphysical_double_scatters(scatters_2x_d1_out)

        # count number of physical double scatters
        num_ds_pe = len(scatters_2x_out);  num_ds_d0_pe = len(scatters_2x_d0_out);  num_ds_d1_pe = len(scatters_2x_d1_out)


    #  applying Compton line filtering (checks if E1 and theta1 are consistent with Compton formula)
    #   - use COMPTON_LINE_RANGE (accepted energy range) & COMPTON_LINE_ENERGIES (expected gamma energies) global variables
    #   - modified code from Matt Leigh's Filter.py
    if APPLY_COMPTON_LINE_FILTERING:
        print ('\n  Applying Compton line filtering / Energies (MeV): {} / Range of values: {}'.format(COMPTON_LINE_ENERGIES, COMPTON_LINE_RANGE))

        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out = utils.compton_line_filtering(scatters_2x_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out = utils.compton_line_filtering(scatters_2x_d0_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out = utils.compton_line_filtering(scatters_2x_d1_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)

        # count number of Compton line filtered double scatters
        num_ds_cl = len(scatters_2x_out);  num_ds_d0_cl = len(scatters_2x_d0_out);  num_ds_d1_cl = len(scatters_2x_d1_out)


    #  shuffle the 2x & 3x scatter events
    #   - to overcome timing errors (like runs 6, 7, 8 for 180315 data)
    if SHUFFLE_SCATTER_EVENTS:

        #  - numpy.random.shuffle: Modify a sequence in-place by shuffling its contents.
        numpy.random.shuffle(scatters_2x_out)
        numpy.random.shuffle(scatters_3x_out)



    ### GROUPING COINCIDENCE EVENTS ###

    print ('\n--- Grouping Coincidence Events ---')

    #  groups events from different modules based on energy and time stamp
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE:
        print ('  Grouping multi-stage coincidence events . . .')

        print ('    ######  CURRENTLY NOT FUNCTIONING  ######')

        # count number of multi-stage coincidence double scatters
        # num_ds_ms = len(scatters_2x_out);  num_ds_d0_ms = len(scatters_2x_d0_out);  num_ds_d1_ms = len(scatters_2x_d1_out)



    ### PLOTTING FINAL DETECTOR DATA ###

    if PRODUCE_PLOTS:
        print ('\n--- Making Double Scatter Energy Plots (using final data) ---')

        #  producing required data
        MeCsq = 0.5109989461  # electron mass in energy units (MeV)
        #   - scatters_2x_out
        ds_1st_eng = scatters_2x_out[:, 0];  ds_2nd_eng = scatters_2x_out[:, 4];  ds_tot_eng = ds_1st_eng + ds_2nd_eng
        ds_all_eng = numpy.concatenate((ds_1st_eng, ds_2nd_eng), axis = 0)
        ds_theta1 = numpy.arccos( 1 + MeCsq * ( 1.0/(ds_tot_eng) - 1.0/(ds_tot_eng - ds_1st_eng) ) )
        #   - scatters_2x_d0_out
        ds_d0_1st_eng = scatters_2x_d0_out[:, 0];  ds_d0_2nd_eng = scatters_2x_d0_out[:, 4];  ds_d0_tot_eng = ds_d0_1st_eng + ds_d0_2nd_eng
        ds_d0_all_eng = numpy.concatenate((ds_d0_1st_eng, ds_d0_2nd_eng), axis = 0)
        ds_d0_theta1 = numpy.arccos( 1 + MeCsq * ( 1.0/(ds_d0_tot_eng) - 1.0/(ds_d0_tot_eng - ds_d0_1st_eng) ) )
        #   - scatters_2x_d1_out
        ds_d1_1st_eng = scatters_2x_d1_out[:, 0];  ds_d1_2nd_eng = scatters_2x_d1_out[:, 4];  ds_d1_tot_eng = ds_d1_1st_eng + ds_d1_2nd_eng
        ds_d1_all_eng = numpy.concatenate((ds_1st_eng, ds_d1_2nd_eng), axis = 0)
        ds_d1_theta1 = numpy.arccos( 1 + MeCsq * ( 1.0/(ds_d1_tot_eng) - 1.0/(ds_d1_tot_eng - ds_d1_1st_eng) ) )

        #  plots for energy deposition of final data
        #   - scatters_2x_out
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_tot_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by all double scatters (summed total), total: %s'%(len(ds_tot_eng)))
        utils.save_to_file("final_doubles_all_Energy_Sum_Counts_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_all_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by all double scatters (individual scatters), total: %s'%(len(ds_all_eng)))
        utils.save_to_file("final_doubles_all_Energy_Each_Counts_alternate", PLOTS_OUTPUT_DIR)
        #   - scatters_2x_d0_out
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_d0_tot_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by D0 double scatters (summed total), total: %s'%(len(ds_d0_tot_eng)))
        utils.save_to_file("final_doubles_D0_Energy_Sum_Counts_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_d0_all_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by D0 double scatters (individual scatters), total: %s'%(len(ds_d0_all_eng)))
        utils.save_to_file("final_doubles_D0_Energy_Each_Counts_alternate", PLOTS_OUTPUT_DIR)
        #   - scatters_2x_d1_out
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_d1_tot_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by D1 double scatters (summed total), total: %s'%(len(ds_d1_tot_eng)))
        utils.save_to_file("final_doubles_D1_Energy_Sum_Counts_alternate", PLOTS_OUTPUT_DIR)
        plt.cla(); plt.clf()
        utils.create_plot_range(ds_d1_all_eng, 'Energy Deposited (MeV)', 300, 0, MAX_ENERGY/1000.0)
        plt.title('Energy deposited by D1 double scatters (individual scatters), total: %s'%(len(ds_d1_all_eng)))
        utils.save_to_file("final_doubles_D1_Energy_Each_Counts_alternate", PLOTS_OUTPUT_DIR)

        #  plotting E1 vs Theta1
        print ('  Creating energy 1 vs theta 1 plots . . .')
        #   - scatters_2x_out
        plt.cla(); plt.clf()
        plt.plot(ds_theta1, ds_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
        plt.title('First Energy Deposited vs Calculated First Scatter Angle / All, total: %s'%(len(ds_theta1)))
        plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
        plt.axis([0, 3, 0, MAX_ENERGY/1000.0])
        utils.save_to_file("final_doubles_all_Energy1_Theta1", PLOTS_OUTPUT_DIR)
        #   - scatters_2x_d0_out
        plt.cla(); plt.clf()
        plt.plot(ds_d0_theta1, ds_d0_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
        plt.title('First Energy Deposited vs Calculated First Scatter Angle / D0, total: %s'%(len(ds_d0_theta1)))
        plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
        plt.axis([0, 3, 0, MAX_ENERGY/1000.0])
        utils.save_to_file("final_doubles_D0_Energy1_Theta1", PLOTS_OUTPUT_DIR)
        #   - scatters_2x_d1_out
        plt.cla(); plt.clf()
        plt.plot(ds_d1_theta1, ds_d1_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
        plt.title('First Energy Deposited vs Calculated First Scatter Angle / D1, total: %s'%(len(ds_d1_theta1)))
        plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
        plt.axis([0, 3, 0, MAX_ENERGY/1000.0])
        utils.save_to_file("final_doubles_D1_Energy1_Theta1", PLOTS_OUTPUT_DIR)



    ### SAVING DATA TO CSV ###

    print ('\n--- Saving Polaris Data to CSV ---')

    #  creating output file names
    output_file = OUTPUT_FILE.replace(".txt", "").replace(".dat", "").replace(".csv", "") + ".csv"
    output_file_2x = output_file.replace(".csv", "_2x.csv")
    output_file_2x_d0 = output_file.replace(".csv", "_2x_d0.csv")
    output_file_2x_d1 = output_file.replace(".csv", "_2x_d1.csv")
    output_file_3x = output_file.replace(".csv", "_3x.csv")

    #  outputting data to CSV
    #   format: eng1, x1, y1, z1, eng2, x2, y2, z2, (eng3, x3, y3, z3) <- if triple scatter
    #   units: time in us and pos in mm
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x))
    numpy.savetxt(output_file_2x, scatters_2x_out, delimiter=',', fmt='%.5f')
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_out), 'scatters_2x_d0_out', output_file_2x_d0))
    numpy.savetxt(output_file_2x_d0, scatters_2x_d0_out, delimiter=',', fmt='%.5f')
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_out), 'scatters_2x_d1_out', output_file_2x_d1))
    numpy.savetxt(output_file_2x_d1, scatters_2x_d1_out, delimiter=',', fmt='%.5f')
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_3x))
    numpy.savetxt(output_file_3x, scatters_3x_out, delimiter=',', fmt='%.5f')

    #  saving combined 2x and 3x data (using file.open in order to append data to file)
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file))
    ofile = open(output_file, "w")
    numpy.savetxt(ofile, scatters_2x_out, delimiter=',', fmt='%.5f')
    print ('   - saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file))
    numpy.savetxt(ofile, scatters_3x_out, delimiter=',', fmt='%.5f')
    ofile.close()

    # count final number of double/triple scatters
    num_ds_fi = len(scatters_2x_out);  num_ds_d0_fi = len(scatters_2x_d0_out)
    num_ds_d1_fi = len(scatters_2x_d1_out);  num_ts_fi = len(scatters_3x_out)

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    print ('-- Total events: {}, in file: {}'.format(len(data), INPUT_FILE))
    print ('--- Start time (s): {} | End time (s): {} | Run duration (s): {} ({} hrs) | Run activity (events/s): {}'.format(time_start, time_end, (time_end - time_start), (time_end - time_start)/3600, len(cc_dataframe) / (time_end - time_start)))
    print ('-- Number of 1x pixel events: {}, 2x pixel events: {}, 3x pixel events: {}, 4x+ pixel events: {}'.format(num_1px, num_2px, num_3px, num_4px))
    print ('--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    print ('-- Run Options -> Big Energy Sort: {} / Remove Unphysical Events: {} / Compton Line Filtering: {} / Multistage Coincidence: {}'.format(APPLY_BIG_ENERGY_SORTING, REMOVE_UNPHYSICAL_EVENTS, APPLY_COMPTON_LINE_FILTERING, APPLY_COINCIDENCE_GROUPING_MULTISTAGE))
    if APPLY_COMPTON_LINE_FILTERING: print ('--- Compton line filter settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE: print ('--- Multistage coincidence settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    print ('-- Double Scatters Events       -> Initial: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds, num_ds_pe, num_ds_cl, num_ds_ms, num_ds_fi, ((float(num_ds_fi)/num_ds) * 100.0)))
    print ('-- Double Scatters Events in D0 -> Initial: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d0, num_ds_d0_pe, num_ds_d0_cl, num_ds_d0_ms, num_ds_d0_fi, ((float(num_ds_d0_fi)/num_ds_d0) * 100.0)))
    print ('-- Double Scatters Events in D1 -> Initial: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d1, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_d1_fi, ((float(num_ds_d1_fi)/num_ds_d1) * 100.0)))
    print ('-- Triple Scatters Events       -> Initial: {} | Final: {}'.format(num_ts, num_ts_fi))
    print ('--------------------------')


if __name__ == "__main__":

    main()
