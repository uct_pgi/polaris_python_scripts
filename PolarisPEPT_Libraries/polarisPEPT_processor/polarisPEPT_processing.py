#!/usr/bin/python
'''
Library of processing functions to perform Positron Emission Particle Tracking (PEPT)
using the Cadmium-Zinc-Telluride PolarisJ detectors made by H3D Inc. (Ann Arbor, MI).

Written as part of the M.Sc. thesis of Nicholas Hyslop, Physics Department, University of Cape Town.

Uses the PEPT algorithm written in the ctrack.c file. Code written by Tom Leadbeater.
'''

__author__ = 'Nicholas Hyslop (nhyslop2@gmail.com)'
__date__ = '22 March 2021'

## IMPORT STATEMENTS ##
import os
import subprocess
import shlex
import sys
import time
import re
import linfit
import math
import matplotlib
import warnings
warnings.filterwarnings("ignore")
import statistics

import numpy as np
import pandas as pd
import scipy.stats as stats
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import multiprocessing as mp
import matplotlib.font_manager as fm
import polarisMC_processing as pmcp

from subprocess import Popen, PIPE
from scipy.optimize import curve_fit
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib.pyplot import figure
from scipy.stats import norm
from matplotlib import rcParams
from matplotlib.ticker import MaxNLocator
# from matplotlib.collections import namedtupl
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D
from scipy.odr import ODR, Model, Data, RealData

from pathos.multiprocessing import ProcessingPool as Pool

# Import cython utils library from polaris_data_processor
import pyximport; pyximport.install(setup_args={"include_dirs":np.get_include()})
sys.path.append('/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/polaris/polaris_data_processor/')       # Nicholas latop
# sys.path.append('/home/nicholas/polarisPEPT/polaris/polaris_data_processor/')                 # Steve server
import processPolarisData_utils as utils

## GLOBAL PARAMETERS ##
FOPT_STEP = 2       # increment in fopt parameter when finding the optimum fopt parameter, i.e. start at 1 and check every FOPT_STEP value
NUM_COIN = 10       # number of coincidence lines to use per position

matplotlib.rc('font',family='Palatino')     # globally change the plot font

## CLASSES ##

# Class to log output both to the console and to a log file
class Logger(object):

    def __init__(self, log_filename):
        self.terminal = sys.stdout
        self.log = open(log_filename, 'w')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass


## FUNCTIONS ##

# Simple funtion to create boxes out of errorbars
def make_error_boxes(ax, xdata, ydata, xerror, yerror, facecolor='r',
                     edgecolor='None', alpha=0.5):

    # Create list for all the error patches
    errorboxes = []

    # Loop over data points; create box from errors at each point
    # print(tuple(zip(xdata, ydata, xerror.T, yerror.T)))
    for x, y, xe, ye in zip(xdata, ydata, xerror.T, yerror.T):
        # print(x,y,xe,ye)
        rect = Rectangle((x - xe, y - ye), 2*xe, 2*ye)
        errorboxes.append(rect)

    # Create patch collection with specified colour/alpha
    pc = PatchCollection(errorboxes, facecolor=facecolor, alpha=alpha,
                         edgecolor=edgecolor, zorder = 2)

    # Add collection to axes
    ax.add_collection(pc)

    # Plot errorbars
    artists = ax.errorbar(xdata, ydata, xerr=xerror, yerr=yerror,
                          fmt='None', ecolor='k', lw = 0.5,
                          zorder = 2)

    return artists

# Simple linear function for fitting
def linear(x, m, c):
    return m*x + c

# Weighted least squares linear fit
def weightedLinear(x, y, ux, uy):
    def f(P, x):
        return P[0]*x + P[1]

    # Get initial guess of parameter values based on unwieghted fit
    m0, c0, r, p, se = stats.linregress(x, y)

    # Setup and run the least squares fit
    linear = Model(f)                                           # create the model
    data = RealData(x, y, sx = ux, sy = uy)   # create a RealData instance since we know the uncertainties
    odr = ODR(data, linear, beta0 = [m0, c0])                   # instantiate ODR with data, model and initial parameter estimate
    output = odr.run()                                          # run the fit

    # Extract the fitted parameters
    mfit = output.beta[0]
    cfit = output.beta[1]
    umfit = output.sd_beta[0]
    ucfit = output.sd_beta[1]

    return mfit, umfit, cfit, ucfit

# General Gaussian distribution function for fitting
def gaus(x, A, mu, sigma, B):
    return A * np.exp(-(x-mu)**2/(2*sigma**2)) + B

# General normalised Gaussian distribution function for fitting
def gaus_normalised(x, mu, sigma, B):
    return 1/(sigma*np.sqrt(2*np.pi)) * np.exp(-(x-mu)**2/(2*sigma**2)) + B

# Print iterations progress
def printProgressBar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Run the pept algorithm on position
def run_ctrack(data_f, fopt, N):
    '''
    Takes in a user-specified file, runs the ctrack algorithm on it and returns
    two arrays with the position + uncertainty of the source.
    @params:
        data_f  - Required : Name of the data file to run ctrack algorithm on (Str)
        fopt    - Required : Optimal F parameter, an integer between 0 and 100 (Int)

    @returns
        positions       - x, y and z position of the source (NumPy Arr)
        uncertainties   - x, y and z uncertainties of the position (NumPy Arr)
    '''

    if (fopt/100)*N < 2:
        # N = int(2/(fopt/100)) + 1
        # print('Error: Need at least two lines per position. Changing N to', N)
        print('Error: Need at least two lines per position. Cannot run ctrack with fopt = {} and N = {}'.format(fopt,N))
        return None, None

    # print('Running ctrack on', data_f + '...')
    ctrack = Popen(['./ctrack', data_f, str(fopt), str(N)], stdin=PIPE, stdout=PIPE)       # run ctrack

    lines = []      # holds the output of ctrack

    # Loop through stdout and store the result
    for i in range(11):
        lines.append(ctrack.stdout.readline().decode("utf-8").strip())

    # for line in lines:
    #     print(line)

    # Save the positions and their uncertainties as numpy float arrays
    positions = np.array(lines[7].split(), dtype=float)
    uncertainties = np.array(lines[8].split(), dtype=float)

    # print('Done.')

    return positions, uncertainties

# Run an F optimisation and plot the results
def f_optimisation(data_dir, fstart, fstop, fstep, N, write_logfile = True, log_filename = '', plot_curve = True):
    '''
    Takes in a detector file with all the singles in it and runs the ctrack
    algorithm on it for a range of F values to determine the optimal F.
    Plots the final optimisation curve

    @params:
        data_dir        - Required : Path to directories containing 'pept_coincidence_data' file containing the positions (Str)
        fopt_step       - Required : Step between successive fopt values (Int)
        N               - Required : Number of lines per position (Int)
        write_logfile   - Optional : Whether or not to write a log file of the results (Bool, default = True)
        log_filename    - Optional : Pass in a filename for the log file if the default isn't detailed enough (Str)
        plot_curve      - Optional : Select whether to plot the F-opt curve (Bool, default = True)

    @returns:
        best_opt    - Optimal F value, fopt (Int)
        best_pos    - X, Y & Z coordinates with the fopt value (Float[])
        best_ucert  - Uncertainties on X, Y & Z coordinates
    '''

    singles_fname = data_dir + 'pept_coincidence_data'

    fs = np.arange(fstart, fstop, fstep)                                        # define a range of fopt parameters
    l = len(fs)                                                              # length of the fopt array
    # xs, ys, zs, u_xs, u_ys, u_zs, rs, u_rs = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties
    xs, ys, zs, u_xs, u_ys, u_zs, rs, u_rs, fopts = ([] for i in range(9))    # create empty numpy arrays to hold positions and uncertainties

    print('Running fopt parameter sweep...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, fopt in enumerate(fs):
        pos, ucert = run_ctrack(singles_fname, fopt, N)   # run ctrack
        if pos is None:
            continue
        fopts.append(fopt)
        xs.append(pos[0])
        ys.append(pos[1])
        zs.append(pos[2])
        rs.append(np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2))
        u_xs.append(ucert[0])
        u_ys.append(ucert[1])
        u_zs.append(ucert[2])
        u_rs.append(np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2))

        # time.sleep(0.1)
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    xs = np.array(xs); ys = np.array(ys); zs = np.array(zs); u_xs = np.array(u_xs); u_ys = np.array(u_ys); u_zs = np.array(u_zs); fopts = np.array(fopts)

    # Creating a log file to store terminal output to
    if log_filename == '':
        log_filename = '{}/fopt-N{:d}.log'.format(data_dir, N)

    log_str = 'Running F-optimisation on {}pept_coincidence_data using N = {}\n\n'.format(data_dir, N)

    # log_file = open(log_filename, 'w')
    # sys.stdout = log_file

    ## Determine the position and uncertainty using the optimal fopt ##
    print('\nGetting the position based on the best fopt parameter...')

    # Get the fopt for the minimum uncertainty
    min_fopt_x = fopts[np.where(u_xs == min(u_xs))]
    min_fopt_y = fopts[np.where(u_ys == min(u_ys))]
    min_fopt_z = fopts[np.where(u_zs == min(u_zs))]
    min_fopt_r = fopts[np.where(u_rs == min(u_rs))]

    best_opt = min_fopt_r
    best_pos, best_ucert = run_ctrack(singles_fname, best_opt[0], N)   # run ctrack

    print()
    print('Optimal F values:', best_opt)
    print('Position with Fopt:', best_pos)
    print('Uncertainties with Fopt:', best_ucert)
    print('Radial uncertainty: ', np.sqrt(best_ucert[0]**2 + best_ucert[1]**2 + best_ucert[2]**2))
    print()

    log_str += '{:30} {}\n'.format('Optimal F values:', best_opt)
    log_str += '{:30} {}\n'.format('Position with Fopt:', best_pos)
    log_str += '{:30} {}\n'.format('Uncertainties with Fopt:', best_ucert)

    if plot_curve:
        # Plot the final results
        # ----------------------
        print('\nPlotting the final results...')

        # Plot the x, y and z parameters on the same scatter plot
        plt.scatter(fopts, u_rs, marker = '.', color = 'm', label = 'u(r), min = ' + str(min_fopt_r))
        plt.scatter(fopts, u_xs, marker = '^', color = 'red', label = 'u(x), min = ' + str(min_fopt_x))
        plt.scatter(fopts, u_ys, marker = '+', color = 'blue', label = 'u(y), min = ' + str(min_fopt_y))
        plt.scatter(fopts, u_zs, marker = 'x', color = 'green', label = 'u(z), min = ' + str(min_fopt_z))
        plt.legend()
        plt.xlabel('F parameter (%)')
        plt.ylabel('Uncertainty on position (mm)')
        plt.show()

    if write_logfile:
        # Write the output to log file
        with open(log_filename, 'w') as f:
            f.write(log_str)

    return best_opt, best_pos, best_ucert

# Run an F optimisation and plot the results
def f_optimisation_averaged(singles_dir, N, fopt_step = 10):
    '''
    Takes in a detector file with all the singles in it and runs the ctrack
    algorithm on it for a range of F values to determine the optimal F.
    Plots the final optimisation curve

    @params:
        singles_fname  - Required : Name of the data file to run ctrack algorithm on (Str)

    @returns:
        best_opt    - Optimum F value, fopt (Int)
        best_pos    - X, Y & Z coordinates with the fopt value (Float[])
        best_ucert  - Uncertainties on X, Y & Z coordinates
    '''

    fopts = np.arange(10, 100, fopt_step)                                        # define a range of fopt parameters
    l = len(fopts)                                                              # length of the fopt array
    # xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties
    ux_arr, uy_arr, uz_arr, ur_arr = [], [], [], []

    # To keep track of the optimal position
    best_u_r = 100
    best_opt = 0
    best_pos = np.zeros(3)
    best_ucert = np.zeros(3)

    print('Running f-parameter sweep...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, fopt in enumerate(fopts):
        xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = 0,0,0,0,0,0,0,0

        j = 0
        for subdir, dirs, files in os.walk(singles_dir):        # loop through all datasets in directory
            for dir in dirs:

                # Skip the apparatus directory
                if dir[0] != 'J':
                    continue

                # fname = singles_dir + dir + '/pet_setup-coincidences_btb.csv'   # construct directory to coincidences file
                fname = singles_dir + dir + '/pept_coincidence_data'   # construct directory to coincidences file

                if not os.path.isfile(fname):
                    print ('File', fname, 'does not exist.')
                    return

                pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                # Extract individual pieces of information
                xs = pos[0]
                ys = pos[1]
                zs = pos[2]
                rs = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
                u_xs = ucert[0]
                u_ys = ucert[1]
                u_zs = ucert[2]
                u_rs = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                # print(dir)
                # print('Pos = ({:.5f}, {:.5f}, {:.5f}) +- ({:.5f}, {:.5f}, {:.5f})\n'.format(xs, ys, zs, u_xs, u_ys, u_zs))

                # Update the averages (ignoring nan values)
                if not math.isnan(xs):
                    xs_avg = j*xs_avg/(j+1) + xs/(j+1)
                if not math.isnan(ys):
                    ys_avg = j*ys_avg/(j+1) + ys/(i+1)
                if not math.isnan(zs):
                    zs_avg = j*zs_avg/(j+1) + zs/(j+1)
                if not math.isnan(rs):
                    rs_avg = j*rs_avg/(j+1) + rs/(j+1)
                if not math.isnan(u_xs):
                    u_xs_avg = j*u_xs_avg/(j+1) + u_xs/(j+1)
                if not math.isnan(u_ys):
                    u_ys_avg = j*u_ys_avg/(j+1) + u_ys/(j+1)
                if not math.isnan(u_zs):
                    u_zs_avg = j*u_zs_avg/(j+1) + u_zs/(j+1)
                if not math.isnan(u_rs):
                    u_rs_avg = j*u_rs_avg/(j+1) + u_rs/(j+1)

                j += 1

            break

        if u_rs_avg <= best_u_r:
            best_opt = fopt
            best_pos = np.array([xs_avg, ys_avg, zs_avg])              # save new best position
            best_ucert = np.array([u_xs_avg, u_ys_avg, u_zs_avg])      # save new best uncertainty
            best_u_r = u_rs_avg

        ux_arr.append(u_xs_avg)
        uy_arr.append(u_ys_avg)
        uz_arr.append(u_zs_avg)
        ur_arr.append(u_rs_avg)

        # print('Pos = ({:.5f}, {:.5f}, {:.5f}) +- ({:.5f}, {:.5f}, {:.5f})\n'.format(xs_avg[i], ys_avg[i], zs_avg[i], u_xs_avg[i], u_ys_avg[i], u_zs_avg[i]))

        # time.sleep(0.1)
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    ## Determine the position and uncertainty using the optimal fopt ##
    print('\nGetting the position based on the best fopt parameter...')

    # Get the fopt for the minimum uncertainty
    min_fopt_x = fopts[np.where(ux_arr == min(ux_arr))]
    min_fopt_y = fopts[np.where(uy_arr == min(uy_arr))]
    min_fopt_z = fopts[np.where(uz_arr == min(uz_arr))]
    min_fopt_r = fopts[np.where(ur_arr == min(ur_arr))]

    # best_opt = min_fopt_r
    # best_pos, best_ucert = run_ctrack(singles_fname, best_opt[0], NUM_COIN)   # run ctrack

    # Plot the final results
    # ----------------------
    print('\nPlotting the final results...')

    figure(num=None, figsize=(15, 10), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size
    plt.rcParams.update({'font.size': 15})

    ur_arr = np.array(ur_arr)
    fopts = np.array(fopts)

    ur_arr *= 1000      # convert to microns
    fopts = fopts/100       # convert to decimal

    # Plot the x, y and z parameters on the same scatter plot
    plt.scatter(fopts, ur_arr, marker = 'x', color = 'k', label = 'u(r), min = ' + str(min_fopt_r))
    # plt.scatter(fopts, ux_arr, marker = '^', color = 'red', label = 'u(x), min = ' + str(min_fopt_x))
    # plt.scatter(fopts, uy_arr, marker = '+', color = 'blue', label = 'u(y), min = ' + str(min_fopt_y))
    # plt.scatter(fopts, uz_arr, marker = 'x', color = 'green', label = 'u(z), min = ' + str(min_fopt_z))

    poly = np.polyfit(fopts, ur_arr, 4)
    p = np.poly1d(poly)
    xp = np.linspace(min(fopts), max(fopts), 100)
    # np.savetxt(singles_dir + '/fopt_v_ur_' + str(N) + '_fit' + '.txt', np.c_[xp/100.0, p(xp)], fmt = '%.4f', delimiter = ',')
    plt.plot(xp, p(xp), color = 'k', alpha = 0.5)

    plt.legend()
    plt.xlabel('F-parameter')
    plt.ylabel(r'Uncertainty on position ($\mu$m)')

    plt.xticks(np.arange(min(fopts), max(fopts), 0.1))

    fig_filename = '{}fopt-avg-N{}.png'.format(singles_dir, N)
    print('Saving figure to', fig_filename)
    plt.savefig(fig_filename)
    # plt.show()

    return best_opt, best_pos, best_ucert

# Plot the position of a point
def plot_pos(pos, ucert):
    '''
    Takes in a position and uncertainty of a point and plots the position in
    between the detector crystals.

    @params:
        pos     - Required : Array of the position (Float[])
        ucert   - Required : Array of the uncertainty on the position (Float[])

    @returns:
        null
    '''

    # Plotting the position of the particle showing the detector crystal
    # ------------------------------------------------------------------

    print('Plotting the position of the particle in between the detector crystals')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5), tight_layout=True)
    # fig, ax1 = plt.subplots(1)

    ## Create the crystals for plotting ##
    # Top down view
    errorboxes = []
    errorboxes.append(Rectangle((-44, 1.25), 10, 20))
    errorboxes.append(Rectangle((-44, -21.25), 10, 20))
    errorboxes.append(Rectangle((34, 1.25), 10, 20))
    errorboxes.append(Rectangle((34, -21.25), 10, 20))

    pc = PatchCollection(errorboxes, facecolor='skyblue', edgecolor='None', alpha=0.5)
    ax1.add_collection(pc)

    ax1.set_xlim(-50,50)
    ax1.set_ylim(-25,30)

    # # Add splines
    # ax1.spines['left'].set_position('center')
    # ax1.spines['right'].set_color('none')
    # ax1.spines['bottom'].set_position('center')
    # ax1.spines['top'].set_color('none')
    # ax1.spines['left'].set_smart_bounds(True)
    # ax1.spines['bottom'].set_smart_bounds(True)
    # ax1.xaxis.set_ticks_position('bottom')
    # ax1.yaxis.set_ticks_position('left')

    # Front on view
    errorboxes = []
    errorboxes.append(Rectangle((-44, 0), 10, 20))
    errorboxes.append(Rectangle((34, 0), 10, 20))

    pc = PatchCollection(errorboxes, facecolor='skyblue', edgecolor='None', alpha=0.5)
    ax2.add_collection(pc)

    ax2.set_xlim(-50,50)
    ax2.set_ylim(-2,25)

    # # Add splines
    # ax2.spines['left'].set_position('center')
    # ax2.spines['right'].set_color('none')
    # ax2.spines['bottom'].set_position('center')
    # ax2.spines['top'].set_color('none')
    # ax2.spines['left'].set_smart_bounds¸(True)
    # ax2.spines['bottom'].set_smart_bounds(True)
    # ax2.xaxis.set_ticks_position('bottom')
    # ax2.yaxis.set_ticks_position('left')

    ## Plotting the measured position of the source ##

    # Top view #
    # Get the position and uncertainty
    x = pos[0]
    y = pos[1]
    u_x = ucert[0]
    u_y = ucert[1]
    # Plot position with rectangular error
    pos_label = '(' + str(x) + ', ' + str(y) + ')' + r' $\pm$ ' + '(' + str(u_x) + ', ' + str(u_y) + ') mm'
    ax1.errorbar(x, y, xerr = u_x, yerr = u_y, ecolor = 'k', label = 'XY Position = ' + pos_label)
    ax1.add_patch(Rectangle((x-u_x, y-u_y), 2*u_x, 2*u_y, angle = 0.0, facecolor='r', edgecolor='None', alpha=0.5))
    ax1.legend(loc = 9)

    # Front view #
    # Get the position and uncertainty
    z = pos[2]
    u_z = ucert[2]
    # Plot position with rectangular error
    pos_label = '(' + str(x) + ', ' + str(z) + ')' + r' $\pm$ ' + '(' + str(u_x) + ', ' + str(u_z) + ') mm'
    ax2.errorbar(x, z, xerr = u_x, yerr = u_z, ecolor = 'k', label = 'XZ Position = ' + pos_label)
    ax2.add_patch(Rectangle((x-u_x, z-u_z), 2*u_x, 2*u_z, angle = 0.0, facecolor='r', edgecolor='None', alpha=0.5))
    ax2.legend(loc = 9)

    plt.show()

    return

# Run a comparison on N vs uncertainty
def plot_N_vs_sigma(singles_fname, fopt, start, stop, step):
    '''
    Calculate and plot the uncertainty as a function of number of lines per position

    @params:
        singles_fname   - Required : Name of the data file to run ctrack algorithm on (Str)
        fopt            - Required : Fopt parameter, an integer between 0 and 100 (Int)
        start           - Required : Starting number of lines per position (Int)
        stop            - Required : Ending number of lines per position (Int)
        step            - Required : Number of N values to calculate (Int)

    @returns:
        null
    '''

    Ns = np.arange(start, stop, step)                                           # define a range of N inputs
    l = len(Ns)                                                                 # length of the fopt array
    xs, ys, zs, u_xs, u_ys, u_zs, rs, u_rs = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties

    print('Running N sweep...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, N in enumerate(Ns):
        # print(N)
        pos, ucert = run_ctrack(singles_fname, fopt, N)   # run ctrack
        if pos is None:
            print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
            return
        # print(ucert)
        xs[i] = pos[0]
        ys[i] = pos[1]
        zs[i] = pos[2]
        rs[i] = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
        u_xs[i] = ucert[0]
        u_ys[i] = ucert[1]
        u_zs[i] = ucert[2]
        u_rs[i] = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

        time.sleep(0.1)
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    # Plot the final results
    # ----------------------
    print('\nPlotting the final results...')

    # Plot the x, y and z parameters on the same scatter plot
    plt.plot(Ns, u_rs, '-o', color = 'm', label = 'u(r)')#, min = ' + str(min_fopt_r))
    plt.plot(Ns, u_xs, '-.^', color = 'red', label = 'u(x)')#, min = ' + str(min_fopt_x))
    plt.plot(Ns, u_ys, ':+', color = 'blue', label = 'u(y)')#, min = ' + str(min_fopt_y))
    plt.plot(Ns, u_zs, '--x', color = 'green', label = 'u(z)')#, min = ' + str(min_fopt_z))
    plt.legend()
    plt.xlabel('Number of lines per position (N)')
    plt.ylabel('Uncertainty on position (mm)')
    plt.show()

    return

# Run a comparison on N vs uncertainty, averaging over multiple runs
def plot_N_vs_sigma_averaged(singles_dir, fopt, start, stop, step):
    '''
    Calculate and plot the uncertainty as a function of number of lines per position,
    averaged over a number of identical apparatus setups

    @params:
        singles_dir     - Required : Directory containing the data files to run ctrack algorithm on (Str)
        fopt            - Required : Fot parameter, an integer between 0 and 100 (Int)
        start           - Required : Starting number of lines per position (Int)
        stop            - Required : Ending number of lines per position (Int)
        step            - Required : Number of N values to calculate (Int)

    @returns:
        null
    '''

    Ns = np.arange(start, stop, step)                                           # define a range of N inputs
    l = len(Ns)                                                                 # length of the fopt array

    xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties

    print('Running N sweep...')
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, N in enumerate(Ns):
        # for fname in singles_fnames:    # loop through the multiple datasets
        for subdir, dirs, files in os.walk(singles_dir):        # loop through all datasets in directory
            for dir in dirs:

                # Skip the apparatus directory
                if dir == 'apparatus':
                    continue

                fname = singles_dir + dir + '/pet_setup-coincidences_btb.csv'   # construct directory to coincidences file

                pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                # Extract individual pieces of information
                xs = pos[0]
                ys = pos[1]
                zs = pos[2]
                rs = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
                u_xs = ucert[0]
                u_ys = ucert[1]
                u_zs = ucert[2]
                u_rs = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                # Update the averages (ignoring nan values)
                if not math.isnan(xs):
                    xs_avg[i] = i*xs_avg[i]/(i+1) + xs/(i+1)
                if not math.isnan(ys):
                    ys_avg[i] = i*ys_avg[i]/(i+1) + ys/(i+1)
                if not math.isnan(zs):
                    zs_avg[i] = i*zs_avg[i]/(i+1) + zs/(i+1)
                if not math.isnan(rs):
                    rs_avg[i] = i*rs_avg[i]/(i+1) + rs/(i+1)
                if not math.isnan(u_xs):
                    u_xs_avg[i] = i*u_xs_avg[i]/(i+1) + u_xs/(i+1)
                if not math.isnan(u_ys):
                    u_ys_avg[i] = i*u_ys_avg[i]/(i+1) + u_ys/(i+1)
                if not math.isnan(u_zs):
                    u_zs_avg[i] = i*u_zs_avg[i]/(i+1) + u_zs/(i+1)
                if not math.isnan(u_rs):
                    u_rs_avg[i] = i*u_rs_avg[i]/(i+1) + u_rs/(i+1)

            break

        time.sleep(0.1)
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    # Plot the final results
    # ----------------------
    print('\nPlotting the final results...')

    # Plot the x, y and z parameters on the same scatter plot
    plt.plot(Ns, u_rs_avg, '-o', color = 'm', label = 'u(r)')#, min = ' + str(min_fopt_r))
    plt.plot(Ns, u_xs_avg, '-.^', color = 'red', label = 'u(x)')#, min = ' + str(min_fopt_x))
    plt.plot(Ns, u_ys_avg, ':+', color = 'blue', label = 'u(y)')#, min = ' + str(min_fopt_y))
    plt.plot(Ns, u_zs_avg, '--x', color = 'green', label = 'u(z)')#, min = ' + str(min_fopt_z))
    plt.legend()
    plt.xlabel('Number of lines per position (N)')
    plt.ylabel('Uncertainty on position (mm)')
    plt.show()

    return

# 2D plot of N and fopt
def multi_fopt(data_dir, Ns, x_lim = [], y_lim = [], fopt_params = [5, 100, 5], write_data = False, read_txt = False):
    '''
    Plots a series of f-optimisation curves for different values of N (number of
    lines used per position) on the same plot.

    @params:
        data_dir        - Required : Path to directories containing 'pept_coincidence_data' file containing the positions (Str)
        Ns              - Required : Array of the N values to plot (Int[])
        x_lim           - Optional : Specify the plot domain (Float[] : default = [])
        y_lim           - Optional : Specify the plot range (Float[] : default = [])
        fopt_params     - Optional : Start, stop, step values for the fopt curve (Int[] : default = [5, 100, 5])
        write_data      - Optional : Whether or not to write the plot data to a file (Bool, default = False)
        read_txt        - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)

    @returns:
        null
    '''

    singles_fname = data_dir + 'pept_coincidence_data'

    print('Running multiple Fopt on dataset', singles_fname, '\n')

    # Setting general plotting parameters
    colors_fvs_polyfit = ['lightcoral', 'lightblue', 'bisque', 'lightgreen', 'violet']
    colors_fvs = ['red', 'darkblue', 'darkorange', 'darkgreen', 'darkmagenta']
    markers = ['.', 'x', '^', '+', '*']
    figure(num=None, figsize=(15, 10), dpi=300, facecolor='w', edgecolor='k', )      # Set plot size
    plt.rcParams.update({'font.size': 20})

    fopts = np.arange(fopt_params[0], fopt_params[1], fopt_params[2])                                        # define a range of fopt parameters
    l = len(fopts)                                                              # length of the fopt array

    write_dir = ''
    if write_data:
        write_dir = '{}plot_data/multi_fopt/'.format(data_dir)
        if not os.path.exists(write_dir):
            os.makedirs(write_dir)
            print ('Created directory: {}'.format(write_dir))

    if not read_txt:        # if you want to recalculate the plot data from the raw data
        # Run for all given N values
        count = 0
        for N in Ns:
            xs, ys, zs, u_xs, u_ys, u_zs, rs, u_rs = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties

            print('Running fopt parameter sweep for', N, 'lines per position...')
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            for i, fopt in enumerate(fopts):
                pos, ucert = run_ctrack(singles_fname, fopt, N)   # run ctrack
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return
                xs[i] = pos[0]
                ys[i] = pos[1]
                zs[i] = pos[2]
                rs[i] = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
                u_xs[i] = ucert[0]
                u_ys[i] = ucert[1]
                u_zs[i] = ucert[2]
                u_rs[i] = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                # time.sleep(0.1)
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            if write_data:
                write_filename = '{}fopt_v_ur_N{:.0f}.txt'.format(write_dir, N)
                np.savetxt(write_filename, np.c_[fopts/100.0, u_rs*1000], fmt = '%.4f', delimiter = ',')

            # Plot the x, y and z parameters on the same scatter plot
            plt.plot(fopts/100.0, u_rs*1000, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            # if fit_poly:
            poly = np.polyfit(fopts, u_rs*1000, 4)
            p = np.poly1d(poly)
            xp = np.linspace(min(fopts), max(fopts), 100)
            # np.savetxt(singles_dir + '/fopt_v_ur_' + str(N) + '_fit' + '.txt', np.c_[xp/100.0, p(xp)], fmt = '%.4f', delimiter = ',')
            plt.plot(xp/100.0, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            count += 1
            print()

    else:

        print('\nReading Fopt vs sigma data')
        print('--------------------------')

        txt_files = []
        for txt_file in os.listdir('{}plot_data/multi_fopt/'.format(data_dir)):
            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue
            txt_files.append(txt_file)
        txt_files = sorted(txt_files, key = lambda x: int(re.search(r'N(\d+)', x).group(1)))       # sort the measured data by the run number


        count = 0
        for txt_file_ in txt_files:

            txt_file = '{}plot_data/multi_fopt/{}'.format(data_dir, txt_file_)

            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue

            plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
            fopts = plot_data[:,0]
            u_rs = plot_data[:,1]

            N = int(re.search(r'N(\d+)', txt_file).group(1))

            plt.plot(fopts, u_rs, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            poly = np.polyfit(fopts, u_rs, 5)
            p = np.poly1d(poly)
            xp = np.linspace(min(fopts), max(fopts), 100)

            plt.plot(xp, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            count += 1

    print('\nPlotting the final results...')

    plt.legend()
    plt.xlabel('F parameter')
    plt.ylabel(r'Uncertainty on position ($\mu$m)')

    # Setting the x and y limits, along with the ticks
    if x_lim != []:
        plt.xticks(np.arange(x_lim[0], x_lim[1], 0.1))
    else:
        plt.xticks(np.arange(0.1, 1.0, 0.1))

    if x_lim != []:
        plt.xlim(x_lim[0], x_lim[1])
    if y_lim != []:
        plt.ylim(y_lim[0], y_lim[1])

    print('Saving figure to ', data_dir + 'plot_multi_fopt.png')
    plt.savefig(data_dir + 'plot_multi_fopt.png')
    # plt.show()

    return

# Plotting f vs sigma for multiple N, with N vs sigma inset in the figure
def plot_multi_fopt_NvS(data_dir, write_dir, Ns, Frange, Ns_pois, Nfopt, write_data = False, read_txt = False, fit_poly = True, plot_N_curve = True, x_lim = [], y_lim = []):
    '''
    Plots a f versus sigma graph, averaged over multiple meaurements taken with
    identical apparatus setup, for a number of different lines per position (N)
    parameters. Also plots a sub-figure of N versus sigma, also averaged over
    multiple measurements.

    @params:
        data_dir        - Required : Input directory path containing the multiple datasets (Str)
        write_dir       - Required : Path to write final plot to (Str)
        Ns              - Required : Array of the N values to plot for each fopt curve (Int[])
        Frange          - Required : Triplet containing (start, stop, step) values for f sweep (Int[])
        Ns_pois         - Required : Array of the N values to plot for poisson uncertainty subfigure (Int[])
        Nfopt           - Required : Optimal f parameter to run on N sweep
        write_data      - Optional : Whether or not to write the plot data to a file (Bool, default = False)
        read_txt        - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)
        fit_poly        - Optional : Perform a polynomial fit to the fopt curves (Bool : default = True)
        plot_N_curve    - Optional : Plot the inset 1/sqrt(N) curve (Bool : default = True)
        x_lim           - Optional : Specify the plot domain (Float[] : default = [])
        y_lim           - Optional : Specify the plot range (Float[] : default = [])

    @returns:
        null
    '''

    # # Setting general plotting parameters
    colors_fvs_polyfit = ['lightcoral', 'lightblue', 'bisque', 'lightgreen', 'violet']
    colors_fvs = ['red', 'darkblue', 'darkorange', 'darkgreen', 'darkmagenta']
    markers = ['.', 'x', '^', '+', '*']
    figure(num=None, figsize=(15, 10), dpi=300, facecolor='w', edgecolor='k', )      # Set plot size
    plt.rcParams.update({'font.size': 15})

    write_dir = '{}plot_data/multi_fopt-N_inset/'.format(write_dir)
    if not os.path.exists(write_dir):
        os.makedirs(write_dir)
        print ('Created directory: {}'.format(write_dir))

    if not read_txt:        # if you want to recalculate the plot data from the raw data
        print('Running f versus sigma processing')
        print('---------------------------------')

        fopts = np.arange(Frange[0], Frange[1], Frange[2])                          # define a range of fopt parameters
        l = len(fopts)                                                              # length of the fopt array

        # Run for all given N values
        for count, N in enumerate(Ns):
            print('Running F optimisation with N =', N)
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            u_rs_avg = []
            for i, fopt in enumerate(fopts):
                fname = data_dir + '/pept_coincidence_data'   # construct directory to coincidences file

                pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                u_rs_avg.append(np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2))

                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            u_rs_avg = np.array(u_rs_avg)

            plt.plot(fopts/100.0, u_rs_avg*1000, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            if write_data:
                write_filename = '{}fopt_v_ur_N{:.0f}.txt'.format(write_dir, N)
                np.savetxt(write_filename, np.c_[fopts/100.0, u_rs_avg*1000], fmt = '%.4f', delimiter = ',')


            if fit_poly:
                poly = np.polyfit(fopts, u_rs_avg*1000, 5)
                p = np.poly1d(poly)
                xp = np.linspace(min(fopts), max(fopts), 100)
                # np.savetxt(singles_dir + '/fopt_v_ur_' + str(N) + '_fit' + '.txt', np.c_[xp/100.0, p(xp)], fmt = '%.4f', delimiter = ',')
                plt.plot(xp/100.0, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            print()

    else:       # else just read and plot the data from the text files
        print('\nReading Fopt vs sigma data')
        print('--------------------------')

        txt_files = []
        for txt_file in os.listdir(write_dir):
            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue
            txt_files.append(txt_file)
        print(txt_files)
        txt_files = sorted(txt_files, key = lambda x: int(re.search(r'N(\d+)', x).group(1)))       # sort the measured data by the run number

        count = 0
        for txt_file_ in txt_files:

            txt_file = '{}/{}'.format(write_dir, txt_file_)

            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue

            plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
            fopts = plot_data[:,0]
            u_rs = plot_data[:,1]

            N = int(re.search(r'N(\d+)', txt_file).group(1))

            plt.plot(fopts, u_rs, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            if fit_poly:
                poly = np.polyfit(fopts, u_rs, 5)
                p = np.poly1d(poly)
                xp = np.linspace(min(fopts), max(fopts), 100)

                plt.plot(xp, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            count += 1


    print('\nPlotting the final results...')

    plt.legend()
    plt.xlabel('F parameter')
    plt.ylabel(r'Uncertainty on position ($\mu$m)')

    if x_lim != []:
        # plt.xticks(np.arange(x_lim[0]+0.02, x_lim[1], 0.04))
        plt.xticks(np.arange(x_lim[0]+0.05, x_lim[1], 0.05))
    else:
        plt.xticks(np.arange(0.1, 1.0, 0.1))

    # Set the x and y limits to ensure the inset plot doesn't cover the fopt data
    if x_lim != []:
        plt.xlim(x_lim[0], x_lim[1])
    if y_lim != []:
        plt.ylim(y_lim[0], y_lim[1])

    # plt.show()

    if plot_N_curve:

        if not read_txt:        # if you want to recalculate the plot data from the raw data
            print('\nRunning N versus sigma processing')
            print('---------------------------------')

            # Ns = np.arange(Nrange[0], Nrange[1], Nrange[2])                             # define a range of N inputs

            l = len(Ns_pois)                                                                 # length of the fopt array

            # xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties
            ur_arr = []

            # print('Running N sweep...')
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            u_rs = []
            for i, N in enumerate(Ns_pois):
                # print('Running F optimisation with N =', N)

                fname = data_dir + '/pept_coincidence_data'   # construct directory to coincidences file

                pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                u_rs.append(np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2))

                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # print(Ns_pois, ur_arr)

            u_rs = np.array(u_rs)

            if write_data:
                write_filename = '{}N_vs_ur.txt'.format(write_dir)
                np.savetxt(write_filename, np.c_[Ns_pois, u_rs*1000], fmt = '%.4f', delimiter = ',')

            # Fit a 1/sqrt(N) curve to the data
            def u_pois(x, a, b):
                return a/np.sqrt(x + b)

            popt, pcov = curve_fit(u_pois, Ns_pois, u_rs*1000, [2000, 5])

            # print(popt, np.sqrt(np.diag(pcov)))

            # Plot the final results
            # ----------------------
            print('\nPlotting the final results...')

            a = plt.axes([.35, .57, .3, .3])

            # np.savetxt(data_dir + '/N_v_ur.txt', np.c_[Ns_pois, u_rs*1000], fmt = '%.4f', delimiter = ',')
            plt.plot(Ns_pois, u_rs*1000, '.', label = 'u(r)', color = 'lightgrey', alpha = 1) #, min = ' + str(min_fopt_r))
            # Plot 1/sqrt(N) fit
            x_fit = np.linspace(min(Ns_pois), max(Ns_pois), 100)
            # np.savetxt(data_dir + '/N_v_ur_fit.txt', np.c_[x_fit, u_pois(x_fit, popt[0])], fmt = '%.4f', delimiter = ',')
            plt.plot(x_fit, u_pois(x_fit, popt[0], popt[1]), color = 'grey', alpha = 0.2)

        else:
            print('\nReading N vs sigma data')
            print('-----------------------')

            txt_file = '{}N_vs_ur.txt'.format(write_dir)
            plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
            Ns = plot_data[:,0]
            u_rs = plot_data[:,1]

            # Fit a 1/sqrt(N) curve to the data
            def u_pois(x, a, b):
                return a/np.sqrt(x + b)

            popt, pcov = curve_fit(u_pois, Ns, u_rs, [2000, 5])

            # Plot the final results
            # ----------------------
            print('\nPlotting the final results...')

            a = plt.axes([.4, .57, .3, .3])

            # np.savetxt(data_dir + '/N_v_ur.txt', np.c_[Ns_pois, ur_arr*1000], fmt = '%.4f', delimiter = ',')
            plt.plot(Ns, u_rs, '.', label = 'u(r)', color = 'lightgrey', alpha = 1) #, min = ' + str(min_fopt_r))
            # Plot 1/sqrt(N) fit
            x_fit = np.linspace(min(Ns), max(Ns), 100)
            # np.savetxt(data_dir + '/N_v_ur_fit.txt', np.c_[x_fit, u_pois(x_fit, popt[0])], fmt = '%.4f', delimiter = ',')
            plt.plot(x_fit, u_pois(x_fit, popt[0], popt[1]), color = 'grey', alpha = 0.2)


    plt.xlabel('Number of lines per position (N)')
    plt.ylabel(r'Uncertainty ($\mu$m)')

    # Edit output image name appropriately
    fig_filename = ''
    print(write_dir)
    if plot_N_curve:
        fig_filename = '{}plot_multi_fopt_NvS.png'.format(write_dir)
    else:
        fig_filename = '{}plot_multi_fopt.png'.format(write_dir)

    print('\nSaving plot to', fig_filename)
    plt.savefig(fig_filename)
    # plt.savefig(fig_filename[:-3] + 'eps')

    # plt.show()

    return

# 2D plot of N and fopt averaged over multiple measurements
def multi_fopt_averaged(singles_dir, Ns):
    '''
    Plots a series of f-optimisation curves for different values of N (number of
    lines used per position) on the same plot, averaged over a series of identical
    apparatus setup measurements.

    @params:
        singles_fname  - Required : Name of the data file to run ctrack algorithm on (Str)
        Ns             - Required : Array of the N values to plot (Int[])

    @returns:
        null
    '''

    fopts = np.arange(25, 100, 5)                                        # define a range of fopt parameters
    l = len(fopts)                                                              # length of the fopt array

    # Run for all given N values
    for N in Ns:
        xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties

        print('Running F optimisation with N =', N)
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

        for i, fopt in enumerate(fopts):
            # for fname in singles_fnames:    # loop through the multiple datasets
            for subdir, dirs, files in os.walk(singles_dir):        # loop through all datasets in directory
                for dir in dirs:

                    # Skip the apparatus directory
                    if dir[0] != 'J':
                        continue

                    # fname = singles_dir + dir + '/pet_setup-coincidences_btb.csv'   # construct directory to coincidences file
                    fname = singles_dir + dir + '/pept_coincidence_data'   # construct directory to coincidences file

                    pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                    if pos is None:
                        print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                        return

                    # Extract individual pieces of information
                    xs = pos[0]
                    ys = pos[1]
                    zs = pos[2]
                    rs = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
                    u_xs = ucert[0]
                    u_ys = ucert[1]
                    u_zs = ucert[2]
                    u_rs = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                    # Update the averages (ignoring nan values)
                    if not math.isnan(xs):
                        xs_avg[i] = i*xs_avg[i]/(i+1) + xs/(i+1)
                    if not math.isnan(ys):
                        ys_avg[i] = i*ys_avg[i]/(i+1) + ys/(i+1)
                    if not math.isnan(zs):
                        zs_avg[i] = i*zs_avg[i]/(i+1) + zs/(i+1)
                    if not math.isnan(rs):
                        rs_avg[i] = i*rs_avg[i]/(i+1) + rs/(i+1)
                    if not math.isnan(u_xs):
                        u_xs_avg[i] = i*u_xs_avg[i]/(i+1) + u_xs/(i+1)
                    if not math.isnan(u_ys):
                        u_ys_avg[i] = i*u_ys_avg[i]/(i+1) + u_ys/(i+1)
                    if not math.isnan(u_zs):
                        u_zs_avg[i] = i*u_zs_avg[i]/(i+1) + u_zs/(i+1)
                    if not math.isnan(u_rs):
                        u_rs_avg[i] = i*u_rs_avg[i]/(i+1) + u_rs/(i+1)

                break

            time.sleep(0.1)
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

        # ## Determine the position and uncertainty using the optimal fopt ##
        # print('\nGetting the position based on the best fopt parameter...')
        #
        # # Get the fopt for the minimum uncertainty
        # min_fopt_x = fopts[np.where(u_xs == min(u_xs))]
        # min_fopt_y = fopts[np.where(u_ys == min(u_ys))]
        # min_fopt_z = fopts[np.where(u_zs == min(u_zs))]
        # min_fopt_r = fopts[np.where(u_rs == min(u_rs))]
        #
        # best_opt = min_fopt_r
        # best_pos, best_ucert = run_ctrack(singles_fname, best_opt[0], NUM_COIN)   # run ctrack

        # Plot the final results
        # ----------------------
        # print('\nPlotting the final results...')

        # Plot the x, y and z parameters on the same scatter plot
        plt.plot(fopts, u_rs_avg, '-x', label = 'N = ' + str(N)) #, label = 'u(r), min = ' + str(min_fopt_r))
        #plt.scatter(fopts, u_rs, marker = '.', color = 'm', label = 'N = ' + str(N)) #, label = 'u(r), min = ' + str(min_fopt_r))
        # plt.scatter(fopts, u_xs, marker = '^', color = 'red', label = 'u(x), min = ' + str(min_fopt_x))
        # plt.scatter(fopts, u_ys, marker = '+', color = 'blue', label = 'u(y), min = ' + str(min_fopt_y))
        # plt.scatter(fopts, u_zs, marker = 'x', color = 'green', label = 'u(z), min = ' + str(min_fopt_z))

        print()

    print('\nPlotting the final results...')

    plt.legend()
    plt.xlabel('F parameter (%)')
    plt.ylabel('Uncertainty on position (mm)')
    plt.show()

    return

# Plotting measured vs known position
def measured_vs_known(data_dirs, fopt, N, coordinate, plot_results = True):
    '''
    Applies the PEPT algorithm to a series of given datasets, extracts the measured
    position and compares it to the known position. Performs a linear fit.

    @params:
        data_dirs       - Required : Array of directories to find the data in (Str[])
        fopt            - Required : Optimal F parameter, an integer between 0 and 100, representing the fraction of lines too keep (Int)
        N               - Required : Number of lines per position (Int)
        coordinate      - Required : Which coordinate to plot. 1 = x, 2 = y, 3 = z (Int)
        plot_results    - Optional : Boolean to plot the results or not (Bool, default = True)

    @returns:
        null
    '''

    # Create arrays to store positions and uncertainties
    # Data is a 2D array with columns: run number (+10 for 190218 data), y-position, u(y-position)
    meas_alldata = []
    known_alldata = []

    print('Running PEPT on all positions')
    print('-----------------------------\n')
    # Loop through all the data directories, ignoring the apparatus directories

    for data_dir in data_dirs:

        # First get all the known positions
        with open(data_dir + '/known_positions.txt') as f:

            header = f.readline()       # discard the header

            for line in f:
                columns = line.split(',')
                row = [int(columns[0]), float(columns[coordinate]), float(columns[coordinate + 3])]

                # Add 10 to 190218 data to keep numbering continuous
                if '190218' in data_dir:
                    row[0] += 10

                # print(row)
                known_alldata.append(row)      # add row to data array

        # Then process the measured values
        for subdir, dirs, files in os.walk(data_dir):
            for dir in dirs:

                if dir == 'ignore' or dir == 'apparatus':
                    continue

                # Getting the measured data
                input = data_dir + dir + '/pept_coincidence_data'
                print('Processing dataset from', input, '...')

                positions, uncertainties = run_ctrack(input, fopt, N)          # run pept algorithm
                if positions is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                # Get the run number
                run_num = int(re.search(r'run(\d+)', dir).group(1))

                # Add 10 to 190218 data to keep numbering continuous
                if '190218' in data_dir:
                    run_num += 10

                row = [run_num, positions[coordinate - 1], uncertainties[coordinate - 1]]
                meas_alldata.append(row)

                print('Done.\n')

                # meas_rs.append(rs)
                # meas_u_rs.append(u_rs)

                # meas_rs.append(np.sqrt(rs[0]**2 + rs[1]**2 + rs[2]**2))
                # meas_u_rs.append(np.sqrt(u_rs[0]**2 + u_rs[1]**2 + u_rs[2]**2))

            break

    meas_alldata = sorted(meas_alldata, key = lambda x: x[0])       # sort the measured data by the run number

    # Delete the 10th item in 190213 dataset: corrupted
    # del known_alldata[9]
    # del meas_alldata[9]

    # # DEBUG: Print the arrays
    # for row in known_alldata:
    #     print('{:5} {:5} {:5}'.format(row[0], row[1], row[2]))
    #
    # for row in meas_alldata:
    #     print('{:5} {:5} {:5}'.format(row[0], row[1], row[2]))

    # Extract the columns
    known_data = np.array([row[1] for row in known_alldata])
    known_udata = np.array([row[2] for row in known_alldata])
    meas_data = np.array([row[1] for row in meas_alldata])
    meas_udata = np.array([row[2] for row in meas_alldata])

    # Remove first three elements; nan errors
    # known_data = known_data[3:]
    # known_udata = known_udata[3:]
    # meas_data = meas_data[3:]
    # meas_udata = meas_udata[3:]

    # Run a linear fit
    # ----------------

    # Plotting the results
    # ====================
    if plot_results:
        # Perform the parameter fit
        _m, _c, r, p_value, std_err = stats.linregress(known_data, meas_data)
        m, um, c, uc = linfit.linfit(known_data, meas_data)

        print('Plotting the results...')

        ## Setting up the figure ##
        # fig, axs = plt.subplots(nrows=3, ncols=1, sharey=False, gridspec_kw = {'height_ratios':[4, 1, 1]}, figsize=(21, 16), dpi = 150)
        # fig, axs = plt.subplots(nrows=2, ncols=1, sharey=False, gridspec_kw = {'height_ratios':[3, 1]}, figsize=(21, 16), dpi = 150)
        fig, axs = plt.subplots(nrows=2, ncols=1, sharey=False, gridspec_kw = {'height_ratios':[3, 1]}, figsize=(8, 8), dpi = 150)
        # figure(num=None, figsize=(21, 16), dpi=200, facecolor='w', edgecolor='k')      # Set plot size

        # Remove horizontal space between axes
        fig.subplots_adjust(hspace=0)

        m_v_k = axs[0]      # plotting axes for measured vs known linear plot
        # m_o_k = axs[1]      # plotting axes for measured/known
        res = axs[1]        # plotting axes for the residuals

        # Plot the data
        # m_v_k.errorbar(known_data, meas_data, xerr = known_udata, yerr = meas_udata, fmt = 'none', ecolor = 'k', capsize = 2)
        _ = make_error_boxes(m_v_k, known_data, meas_data, 3*known_udata, 3*meas_udata)

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(known_data), max(known_data))
        m_v_k.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)), zorder = 1, lw = 0.5)
        m_v_k.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
        m_v_k.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

        # ## Calculate measured/known ##
        # x = np.linspace(-20, 20, len(known_data))
        # ratio = meas_data/known_data
        # u_ratio = ratio*np.sqrt((meas_udata/meas_data)**2 + (known_udata/known_data)**2)
        #
        # ## Plot line along y = 1 ##
        # m_o_k.plot(fit_x, np.ones(len(fit_x)), color = 'lightgrey')
        # ## Plot measured/known ##
        # m_o_k.errorbar(known_data, ratio, yerr = u_ratio, fmt = 'none', ecolor = 'k', capsize = 2)
        #
        # m_o_k.set_ylabel(r'$\frac{\mathrm{Measured}}{\mathrm{Known}}$', rotation=0, fontsize = 15, labelpad=35)
        # m_o_k.set_ylim(-2.7,3.7)
        # m_o_k.tick_params(  axis='x',          # changes apply to the x-axis
        #                     which='both',      # both major and minor ticks are affected
        #                     bottom=False,      # ticks along the bottom edge are off
        #                     top=False,         # ticks along the top edge are off
        #                     labelbottom=False) # labels along the bottom edge are off

        ## Plot residuals ##
        # residuals = abs(ratio - 1)
        # residuals = abs(meas_data - linear(known_data, m, c))
        residuals = meas_data - linear(known_data, m, c)
        res.plot(known_data, residuals, '.r')
        res.plot(fit_x, np.zeros(len(fit_x)), 'k')

        res.set_ylabel('|Residuals| (mm)')
        # res.tick_params(  axis='x',          # changes apply to the x-axis
        #                     which='both',      # both major and minor ticks are affected
        #                     bottom=False,      # ticks along the bottom edge are off
        #                     top=False,         # ticks along the top edge are off
        #                     labelbottom=False) # labels along the bottom edge are off


        # Additional graph information
        res.set_xlabel('Known y-location (mm)')
        m_v_k.set_ylabel('Measured y-location (mm)')
        m_v_k.legend()

        plt.show()

    return known_data, known_udata, meas_data, meas_udata

# Plotting measured vs known positions for all three coordinate axes
def measured_vs_known2(output_dir, mvk_x_dirs, mvk_y_dirs, mvk_z_dirs, write_data = False, read_txt = False):
    '''
    Takes in a series of datasets and extracts the x, y and z coordinates for
    each one, plotting the distribution of the results.

    @params:
        output_dir          - Required : Path to save final plot to (String)
        mvk_x_dirs          - Required : Path to the directories to determine the x-axis measured vs known data (String[])
        mvk_y_dirs          - Required : Path to the directories to determine the y-axis measured vs known data (String[])
        mvk_z_dirs          - Required : Path to the directories to determine the z-axis measured vs known data (String[])
        write_data          - Optional : Whether or not to write the plot data to a file (Bool, default = False)
        read_txt            - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)

    @returns:
        null
    '''

    # Defining the plot space
    # -----------------------
    fig, axs = plt.subplots(nrows=2, ncols=3, sharey=False, sharex=False, gridspec_kw = {'height_ratios':[1, 1]}, figsize=(16, 9), dpi = 500)

    mvk_plot_x = axs[0][0]           # measured vs known positions of z-axis
    mvkr_plot_x = axs[1][0]          # residuals on measured vs known positions of z-axis

    mvk_plot_y = axs[0][1]           # measured vs known positions of z-axis
    mvkr_plot_y = axs[1][1]          # residuals on measured vs known positions of z-axis

    mvk_plot_z = axs[0][2]           # measured vs known positions of z-axis
    mvkr_plot_z = axs[1][2]          # residuals on measured vs known positions of z-axis

    font_size = 14

    write_dir = ''
    if write_data:
        write_dir = '{}plot_data/meas_vs_known/'.format(output_dir)
        if not os.path.exists(write_dir):
            os.makedirs(write_dir)
            print ('Created directory: {}'.format(write_dir))


    # x position
    # ----------

    # Plotting the measured vs known positions and residuals #
    known_data_x, known_udata_x, meas_data_x, meas_udata_x = [], [], [], []
    if not read_txt:
        known_data_x, known_udata_x, meas_data_x, meas_udata_x = measured_vs_known(mvk_x_dirs, 80, 100, 1, plot_results = False)
    else:
        print('\nReading measured vs known for the x-coordinate')
        print('----------------------------------------------')

        txt_file = '{}plot_data/meas_vs_known/meas_vs_known_xcoord.txt'.format(output_dir)
        plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
        known_data_x = plot_data[:,0]
        known_udata_x = plot_data[:,1]
        meas_data_x = plot_data[:,2]
        meas_udata_x = plot_data[:,3]

    print(meas_udata_x, '\n')

    _m, _c, r, p_value, std_err = stats.linregress(known_data_x, meas_data_x)
    m, um, c, uc = linfit.linfit(known_data_x, meas_data_x)

    # Plot the data
    _ = make_error_boxes(mvk_plot_x, known_data_x, meas_data_x, 3*known_udata_x, 3*meas_udata_x)

    # Plot the fit + uncertainties
    fit_x = np.linspace(min(known_data_x), max(known_data_x))
    mvk_plot_x.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = r'm = {:.4f} $\pm$ {:.4f}'.format(m, um) + '\n' +
                        r'c = {:.3f} $\pm$ {:.3f} mm'.format(c, uc) + '\n' +
                        r'r$^2$ = {:.4f}'.format(r**2),
                    zorder = 1, lw = 0.5)
    mvk_plot_x.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
    mvk_plot_x.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

    ## Plot residuals ##
    residuals_x = meas_data_x - linear(known_data_x, m, c)
    mvkr_plot_x.plot(known_data_x, residuals_x*1000, '.r')
    mvkr_plot_x.plot(fit_x, np.zeros(len(fit_x)), 'k')


    # y position
    # ----------

    # Plotting the measured vs known positions and residuals #
    known_data_y, known_udata_y, meas_data_y, meas_udata_y = [], [], [], []
    if not read_txt:
        known_data_y, known_udata_y, meas_data_y, meas_udata_y = measured_vs_known(mvk_y_dirs, 80, 100, 2, plot_results = False)
    else:
        print('\nReading measured vs known for the y-coordinate')
        print('----------------------------------------------')

        txt_file = '{}plot_data/meas_vs_known/meas_vs_known_ycoord.txt'.format(output_dir)
        plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
        known_data_y = plot_data[:,0]
        known_udata_y = plot_data[:,1]
        meas_data_y = plot_data[:,2]
        meas_udata_y = plot_data[:,3]

    print(meas_udata_y, '\n')

    _m, _c, r, p_value, std_err = stats.linregress(known_data_y, meas_data_y)
    m, um, c, uc = linfit.linfit(known_data_y, meas_data_y)

    # Plot the data
    _ = make_error_boxes(mvk_plot_y, known_data_y, meas_data_y, 3*known_udata_y, 3*meas_udata_y)

    # Plot the fit + uncertainties
    fit_x = np.linspace(min(known_data_y), max(known_data_y))
    mvk_plot_y.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = r'm = {:.4f} $\pm$ {:.4f}'.format(m, um) + '\n' +
                        r'c = {:.3f} $\pm$ {:.3f} mm'.format(c, uc) + '\n' +
                        r'r$^2$ = {:.4f}'.format(r**2),
                    zorder = 1, lw = 0.5)
    mvk_plot_y.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
    mvk_plot_y.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

    ## Plot residuals ##
    residuals_y = meas_data_y - linear(known_data_y, m, c)
    mvkr_plot_y.plot(known_data_y, residuals_y*1000, '.r')
    mvkr_plot_y.plot(fit_x, np.zeros(len(fit_x)), 'k')


    # z position
    # ----------

    # Plotting the measured vs known positions and residuals #
    known_data_z, known_udata_z, meas_data_z, meas_udata_z = [], [], [], []
    if not read_txt:
        known_data_z, known_udata_z, meas_data_z, meas_udata_z = measured_vs_known(mvk_z_dirs, 80, 100, 3, plot_results = False)
    else:
        print('\nReading measured vs known for the z-coordinate')
        print('----------------------------------------------')

        txt_file = '{}plot_data/meas_vs_known/meas_vs_known_zcoord.txt'.format(output_dir)
        plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
        known_data_z = plot_data[:,0]
        known_udata_z = plot_data[:,1]
        meas_data_z = plot_data[:,2]
        meas_udata_z = plot_data[:,3]

    print(meas_udata_z, '\n')

    _m, _c, r, p_value, std_err = stats.linregress(known_data_z, meas_data_z)
    m, um, c, uc = linfit.linfit(known_data_z, meas_data_z)

    # Plot the data
    _ = make_error_boxes(mvk_plot_z, known_data_z, meas_data_z, 3*known_udata_z, 3*meas_udata_z)

    # Plot the fit + uncertainties
    fit_x = np.linspace(min(known_data_z), max(known_data_z))
    mvk_plot_z.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = r'm = {:.4f} $\pm$ {:.4f}'.format(m, um) + '\n' +
                        r'c = {:.3f} $\pm$ {:.3f} mm'.format(c, uc) + '\n' +
                        r'r$^2$ = {:.4f}'.format(r**2),
                    zorder = 1, lw = 0.5)
    mvk_plot_z.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
    mvk_plot_z.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

    ## Plot residuals ##
    residuals_z = meas_data_z - linear(known_data_z, m, c)
    mvkr_plot_z.plot(known_data_z, residuals_z*1000, '.r')
    mvkr_plot_z.plot(fit_x, np.zeros(len(fit_x)), 'k')

    # Write the plot data
    if write_data:
        write_filename = '{}meas_vs_known_xcoord.txt'.format(write_dir)
        np.savetxt(write_filename, np.c_[known_data_x, known_udata_x, meas_data_x, meas_udata_x, residuals_x], fmt = '%.4f', delimiter = ',')

        write_filename = '{}meas_vs_known_ycoord.txt'.format(write_dir)
        np.savetxt(write_filename, np.c_[known_data_y, known_udata_y, meas_data_y, meas_udata_y, residuals_y], fmt = '%.4f', delimiter = ',')

        write_filename = '{}meas_vs_known_zcoord.txt'.format(write_dir)
        np.savetxt(write_filename, np.c_[known_data_z, known_udata_z, meas_data_z, meas_udata_z, residuals_z], fmt = '%.4f', delimiter = ',')



    # Plotting the scatter distribution
    # ---------------------------------

    mvkr_plot_x.set_ylabel(r'Residuals ($\mu$m)', fontsize = font_size)
    mvk_plot_x.set_ylabel('Measured \ncoordinate \n(mm)', fontsize = font_size)

    mvkr_plot_x.set_xlabel('X-Coordinate (mm)', fontsize = font_size)
    mvkr_plot_y.set_xlabel('Y-Coordinate (mm)', fontsize = font_size)
    mvkr_plot_z.set_xlabel('Z-Coordinate (mm)', fontsize = font_size)

    mvk_plot_x.legend(loc = 'upper left', fontsize = font_size)
    mvk_plot_y.legend(loc = 'upper left', fontsize = font_size)
    mvk_plot_z.legend(loc = 'upper left', fontsize = font_size)

    mvk_plot_x.set_xlim(-15, 15)
    mvk_plot_x.set_ylim(-15, 25)
#
    mvk_plot_y.set_xlim(-12, 15)
    mvk_plot_y.set_ylim(-12, 30)

    mvk_plot_z.set_xlim(2, 20)
    mvk_plot_z.set_ylim(2, 30)

    # Global settings
    for ax_arr in axs:
        for ax in ax_arr:
            ax.tick_params(labelsize = font_size)       # ensure the tick font size is the same as the lables

    print('\nSaving image to', output_dir + 'measured_vs_known.png')
    plt.savefig(output_dir + 'measured_vs_known.png')
    # plt.show()

    return

# Plotting singles, coindicidence rates
def plot_rates(pet_output_txts, activities, fopt, N, plot_singles = True, plot_coins = True):
    '''
    Takes in a series of datasets and extracts the singles and coindicidence rates
    for each one, plotting the results as a function of source activity.

    @params:
        pet_output_txts     - Required : Array of paths to output files from the processPolarisData003.py script (Str[])
        activities          - Required : Array of initial source activities, corresponding to pet_output_txts (Float[])
        fopt                - Required : Optimal f value to use for ctrack processing (Int)
        N                   - Required : Number of LORs per position (Int)
        plot_singles        - Optional : Plot the trend for the singles rate (Bool, default = True)
        plot_coins          - Optional : Plot the trend for the coincidence rate (Bool, default = True)

    @returns:
        null
    '''

    # First extract the relevant information from the txt files
    singles_rates = []               # detector singles rate (events/sec)
    singles_filtered_rates = []      # detector singles rate after energy filter (events/sec)
    coin_rates = []                  # detector coincidence rate (coincidences/sec)
    # Compile regular expressions for extracting relevent information from txt files
    re_time = re.compile(r'Run duration \(s\): (\d+\.\d+)')
    re_singles = re.compile(r'run activity \(events/s\): (\d+\.\d+)')
    re_singles_filtered = re.compile(r'kev is (\d+)')
    re_coin = re.compile(r'number of coincidence events returned: (\d+)')

    for pet_out in pet_output_txts:
        with open(pet_out, 'r') as f:   # open file
            file_str = f.read()     # read entire file as string
            run_time = float(re_time.findall(file_str)[0])
            singles_rate = float(re_singles.findall(file_str)[0])
            singles_filtered_tot = float(re_singles_filtered.findall(file_str)[0])
            tot_coin = float(re_coin.findall(file_str)[0])

            singles_rates.append(singles_rate)
            singles_filtered_rates.append(singles_filtered_tot/run_time)
            coin_rates.append(tot_coin/run_time)

    # Plotting the results
    # ====================

    # Coincidence Rate
    # ----------------
    if plot_coins:
        # First perform a linear fit
        _m, _c, r, p_value, std_err = stats.linregress(activities, coin_rates)
        m, um, c, uc = linfit.linfit(np.array(activities), np.array(coin_rates))

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(activities), max(activities))
        plt.plot(fit_x, linear(fit_x, m, c), color = 'grey',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)))
        plt.plot(fit_x, linear(fit_x, m+um, c+uc), '--', color = 'grey')
        plt.plot(fit_x, linear(fit_x, m-um, c-uc), '--', color = 'grey')

        plt.scatter(activities, coin_rates, marker='.', color='k', label = 'Coincidence Rate')


    # Singles Rate
    # ------------
    if plot_singles:
        # First perform a linear fit
        _m, _c, r, p_value, std_err = stats.linregress(activities, singles_filtered_rates)
        m, um, c, uc = linfit.linfit(np.array(activities), np.array(singles_filtered_rates))

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(activities), max(activities))
        plt.plot(fit_x, linear(fit_x, m, c), color = 'lightblue',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)))
        plt.plot(fit_x, linear(fit_x, m+um, c+uc), '--', color = 'lightblue')
        plt.plot(fit_x, linear(fit_x, m-um, c-uc), '--', color = 'lightblue')

        plt.scatter(activities, singles_filtered_rates, marker='.', color='b', label = 'Singles Rate (Filtered)')


    plt.xlabel('Activity (kBq)')
    plt.ylabel('Rate (Events/Second)')
    plt.legend()
    plt.show()

    return

# Plotting f vs sigma for multiple N, with N vs sigma inset in the figure
def plot_multi_fopt_NvS_avg(data_dir, write_dir, Ns, Frange, Ns_pois, Nfopt, write_data = False, read_txt = False, fit_poly = True, plot_N_curve = True, x_lim = [], y_lim = []):
    '''
    Plots a f versus sigma graph, averaged over multiple meaurements taken with
    identical apparatus setup, for a number of different lines per position (N)
    parameters. Also plots a sub-figure of N versus sigma, also averaged over
    multiple measurements.

    @params:
        data_dir        - Required : Input directory path containing the multiple datasets (Str)
        write_dir       - Required : Path to write final plot to (Str)
        Ns              - Required : Array of the N values to plot for each fopt curve (Int[])
        Frange          - Required : Triplet containing (start, stop, step) values for f sweep (Int[])
        Ns_pois         - Required : Array of the N values to plot for poisson uncertainty subfigure (Int[])
        Nfopt           - Required : Optimal f parameter to run on N sweep
        write_data      - Optional : Whether or not to write the plot data to a file (Bool, default = False)
        read_txt        - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)
        fit_poly        - Optional : Perform a polynomial fit to the fopt curves (Bool : default = True)
        plot_N_curve    - Optional : Plot the inset 1/sqrt(N) curve (Bool : default = True)
        x_lim           - Optional : Specify the plot domain (Float[] : default = [])
        y_lim           - Optional : Specify the plot range (Float[] : default = [])

    @returns:
        null
    '''

    # # Setting general plotting parameters
    colors_fvs_polyfit = ['lightcoral', 'lightblue', 'bisque', 'lightgreen', 'violet']
    colors_fvs = ['red', 'darkblue', 'darkorange', 'darkgreen', 'darkmagenta']
    markers = ['.', 'x', '^', '+', '*']
    figure(num=None, figsize=(15, 10), dpi=300, facecolor='w', edgecolor='k', )      # Set plot size
    plt.rcParams.update({'font.size': 15})

    if write_data:
        write_dir = '{}plot_data/multi_fopt_avg/'.format(write_dir)
        if not os.path.exists(write_dir):
            os.makedirs(write_dir)
            print ('Created directory: {}'.format(write_dir))

    if not read_txt:        # if you want to recalculate the plot data from the raw data
        print('Running f versus sigma processing')
        print('---------------------------------')

        fopts = np.arange(Frange[0], Frange[1], Frange[2])                          # define a range of fopt parameters
        l = len(fopts)                                                              # length of the fopt array

        # Run for all given N values
        for count, N in enumerate(Ns):
            xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties

            print('Running F optimisation with N =', N)
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            for i, fopt in enumerate(fopts):
                # for fname in singles_fnames:    # loop through the multiple datasets
                for subdir, dirs, files in os.walk(data_dir):        # loop through all datasets in directory
                    for dir in dirs:

                        if dir[0] != 'J':
                            continue

                        # fname = data_dir + dir + '/pet_setup-coincidences_btb.csv'   # construct directory to coincidences file
                        fname = data_dir + dir + '/pept_coincidence_data'   # construct directory to coincidences file

                        pos, ucert = run_ctrack(fname, fopt, N)   # run ctrack
                        if pos is None:
                            print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                            return

                        # Extract individual pieces of information
                        xs = pos[0]
                        ys = pos[1]
                        zs = pos[2]
                        rs = np.sqrt(pos[0]**2 + pos[1]**2 + pos[2]**2)
                        u_xs = ucert[0]
                        u_ys = ucert[1]
                        u_zs = ucert[2]
                        u_rs = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                        # Update the averages (ignoring nan values)
                        if not math.isnan(xs):
                            xs_avg[i] = i*xs_avg[i]/(i+1) + xs/(i+1)
                        if not math.isnan(ys):
                            ys_avg[i] = i*ys_avg[i]/(i+1) + ys/(i+1)
                        if not math.isnan(zs):
                            zs_avg[i] = i*zs_avg[i]/(i+1) + zs/(i+1)
                        if not math.isnan(rs):
                            rs_avg[i] = i*rs_avg[i]/(i+1) + rs/(i+1)
                        if not math.isnan(u_xs):
                            u_xs_avg[i] = i*u_xs_avg[i]/(i+1) + u_xs/(i+1)
                        if not math.isnan(u_ys):
                            u_ys_avg[i] = i*u_ys_avg[i]/(i+1) + u_ys/(i+1)
                        if not math.isnan(u_zs):
                            u_zs_avg[i] = i*u_zs_avg[i]/(i+1) + u_zs/(i+1)
                        if not math.isnan(u_rs):
                            u_rs_avg[i] = i*u_rs_avg[i]/(i+1) + u_rs/(i+1)

                    break

                time.sleep(0.1)
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar


            plt.plot(fopts/100.0, u_rs_avg*1000, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            if write_data:
                write_filename = '{}fopt_v_ur_N{:.0f}.txt'.format(write_dir, N)
                np.savetxt(write_filename, np.c_[fopts/100.0, u_rs_avg*1000], fmt = '%.4f', delimiter = ',')


            if fit_poly:
                poly = np.polyfit(fopts, u_rs_avg*1000, 5)
                p = np.poly1d(poly)
                xp = np.linspace(min(fopts), max(fopts), 100)
                # np.savetxt(singles_dir + '/fopt_v_ur_' + str(N) + '_fit' + '.txt', np.c_[xp/100.0, p(xp)], fmt = '%.4f', delimiter = ',')
                plt.plot(xp/100.0, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            print()

    else:       # else just read and plot the data from the text files
        print('\nReading Fopt vs sigma data')
        print('--------------------------')

        txt_files = []
        for txt_file in os.listdir(write_dir):
            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue
            txt_files.append(txt_file)
        print(txt_files)
        txt_files = sorted(txt_files, key = lambda x: int(re.search(r'N(\d+)', x).group(1)))       # sort the measured data by the run number

        count = 0
        for txt_file_ in txt_files:

            txt_file = '{}/{}'.format(write_dir, txt_file_)

            # Ignore text files that aren't fopt vs u(r)
            if 'fopt_v_ur' not in txt_file:
                continue

            plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
            fopts = plot_data[:,0]
            u_rs = plot_data[:,1]

            N = int(re.search(r'N(\d+)', txt_file).group(1))

            plt.plot(fopts, u_rs, '.', label = 'N = ' + str(N), color = colors_fvs[4-count], marker = markers[4-count], alpha = 1)

            if fit_poly:
                poly = np.polyfit(fopts, u_rs, 5)
                p = np.poly1d(poly)
                xp = np.linspace(min(fopts), max(fopts), 100)

                plt.plot(xp, p(xp), color = colors_fvs_polyfit[4-count], alpha = 0.5)

            count += 1


    print('\nPlotting the final results...')

    plt.legend()
    plt.xlabel('F parameter')
    plt.ylabel(r'Uncertainty on position ($\mu$m)')

    if x_lim != []:
        plt.xticks(np.arange(x_lim[0], x_lim[1], 0.1))
    else:
        plt.xticks(np.arange(0.1, 1.0, 0.1))

    # Set the x and y limits to ensure the inset plot doesn't cover the fopt data
    if x_lim != []:
        plt.xlim(x_lim[0], x_lim[1])
    if y_lim != []:
        plt.ylim(y_lim[0], y_lim[1])

    # plt.show()

    if plot_N_curve:

        if not read_txt:        # if you want to recalculate the plot data from the raw data
            print('\nRunning N versus sigma processing')
            print('---------------------------------')

            # Ns = np.arange(Nrange[0], Nrange[1], Nrange[2])                             # define a range of N inputs

            l = len(Ns_pois)                                                                 # length of the fopt array

            # xs_avg, ys_avg, zs_avg, u_xs_avg, u_ys_avg, u_zs_avg, rs_avg, u_rs_avg = (np.zeros(l) for i in range(8))    # create empty numpy arrays to hold positions and uncertainties
            ur_arr = []

            # print('Running N sweep...')
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            for i, N in enumerate(Ns_pois):
                u_rs_avg = 0

                j = 0
                for subdir, dirs, files in os.walk(data_dir):        # loop through all datasets in directory
                    for dir in dirs:

                        # Skip the apparatus directory
                        if dir[0] != 'J':
                            continue

                        # fname = data_dir + dir + '/pet_setup-coincidences_btb.csv'   # construct directory to coincidences file
                        fname = data_dir + dir + '/pept_coincidence_data'   # construct directory to coincidences file

                        if not os.path.isfile(fname):
                            print ('File', fname, 'does not exist.')
                            return

                        pos, ucert = run_ctrack(fname, Nfopt, N)   # run ctrack
                        if pos is None:
                            print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                            return

                        # Extract position uncertainty
                        u_x = ucert[0]
                        u_y = ucert[1]
                        u_z = ucert[2]
                        u_r = np.sqrt(ucert[0]**2 + ucert[1]**2 + ucert[2]**2)

                        # Update the average (ignoring nan values)
                        if not math.isnan(u_r):
                            u_rs_avg = j*u_rs_avg/(j+1) + u_r/(j+1)

                        j += 1

                    break

                ur_arr.append(u_rs_avg)

                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # print(Ns_pois, ur_arr)

            ur_arr = np.array(ur_arr)

            if write_data:
                write_filename = '{}N_vs_ur.txt'.format(write_dir)
                np.savetxt(write_filename, np.c_[Ns_pois, ur_arr*1000], fmt = '%.4f', delimiter = ',')

            # Fit a 1/sqrt(N) curve to the data
            def u_pois(x, a, b):
                return a/np.sqrt(x + b)

            popt, pcov = curve_fit(u_pois, Ns_pois, ur_arr*1000, [2000, 5])

            # print(popt, np.sqrt(np.diag(pcov)))

            # Plot the final results
            # ----------------------
            print('\nPlotting the final results...')

            a = plt.axes([.45, .57, .3, .3])

            # np.savetxt(data_dir + '/N_v_ur.txt', np.c_[Ns_pois, ur_arr*1000], fmt = '%.4f', delimiter = ',')
            plt.plot(Ns_pois, ur_arr*1000, '.', label = 'u(r)', color = 'lightgrey', alpha = 1) #, min = ' + str(min_fopt_r))
            # Plot 1/sqrt(N) fit
            x_fit = np.linspace(min(Ns_pois), max(Ns_pois), 100)
            # np.savetxt(data_dir + '/N_v_ur_fit.txt', np.c_[x_fit, u_pois(x_fit, popt[0])], fmt = '%.4f', delimiter = ',')
            plt.plot(x_fit, u_pois(x_fit, popt[0], popt[1]), color = 'grey', alpha = 0.2)

        else:
            print('\nReading N vs sigma data')
            print('-----------------------')

            txt_file = '{}plot_data/multi_fopt_avg/N_vs_ur.txt'.format(data_dir)
            plot_data = np.loadtxt(txt_file, dtype = np.float64, delimiter = ',')     # read in the text file
            Ns = plot_data[:,0]
            u_rs = plot_data[:,1]

            # Fit a 1/sqrt(N) curve to the data
            def u_pois(x, a, b):
                return a/np.sqrt(x + b)

            popt, pcov = curve_fit(u_pois, Ns, u_rs, [2000, 5])

            # Plot the final results
            # ----------------------
            print('\nPlotting the final results...')

            a = plt.axes([.42, .57, .3, .3])

            # np.savetxt(data_dir + '/N_v_ur.txt', np.c_[Ns_pois, ur_arr*1000], fmt = '%.4f', delimiter = ',')
            plt.plot(Ns, u_rs, '.', label = 'u(r)', color = 'lightgrey', alpha = 1) #, min = ' + str(min_fopt_r))
            # Plot 1/sqrt(N) fit
            x_fit = np.linspace(min(Ns), max(Ns), 100)
            # np.savetxt(data_dir + '/N_v_ur_fit.txt', np.c_[x_fit, u_pois(x_fit, popt[0])], fmt = '%.4f', delimiter = ',')
            plt.plot(x_fit, u_pois(x_fit, popt[0], popt[1]), color = 'grey', alpha = 0.2)


    plt.xlabel('Number of lines per position (N)')
    plt.ylabel(r'Uncertainty ($\mu$m)')

    # Edit output image name appropriately
    fig_filename = ''
    print(write_dir)
    if plot_N_curve:
        fig_filename = '{}plot_multi_fopt_NvS_avg.png'.format(write_dir)
    else:
        fig_filename = '{}plot_multi_fopt_avg.png'.format(write_dir)

    print('\nSaving plot to', fig_filename)
    plt.savefig(fig_filename)

    # plt.show()

    return

# Copy of the above function with BGO data supplied by Tom Leadbeater
def bgo_plot_multi_fopt_NvS_avg(f_v_s_data, n_v_s_data):
    '''
    Plots a f versus sigma graph, averaged over multiple meaurements taken with
    identical apparatus setup, for a number of different lines per position (N)
    parameters. Also plots a sub-figure of N versus sigma, also averaged over
    multiple measurements. Takes in processed BGO data from txt file.

    @params:
        f_v_s_data      - Required : Input directory path to the f vs sigma dataset (Str)
        n_v_s_data      - Required : Input directory path to the n vs sigma dataset (Str)

    @returns:
        null
    '''

    # Setting general plotting parameters
    colors_fvs = ['lightcoral', 'lightblue', 'bisque', 'lightgreen', 'violet']
    colors_fvs_polyfit = ['red', 'darkblue', 'darkorange', 'darkgreen', 'darkmagenta']
    markers = ['.', 'x', '^', '+', '*']
    figure(num=None, figsize=(21, 16), dpi=200, facecolor='w', edgecolor='k')      # Set plot size


    print('Reading in data from', f_v_s_data)

    fs_5000 = []            # list to hold all the f vs sigma data for N = 5000
    fs_5000.append([])
    fs_5000.append([])
    fs_2000 = []            # list to hold all the f vs sigma data for N = 2000
    fs_2000.append([])
    fs_2000.append([])
    fs_1000 = []            # list to hold all the f vs sigma data for N = 1000
    fs_1000.append([])
    fs_1000.append([])
    fs_500 = []             # list to hold all the f vs sigma data for N = 500
    fs_500.append([])
    fs_500.append([])
    fs_250 = []             # list to hold all the f vs sigma data for N = 250
    fs_250.append([])
    fs_250.append([])

    with open(f_v_s_data) as f:
        heading = f.readline()      # ignore header

        for line in f:      # loop through lines in file
            cols = line.split(',')

            if cols[1] == '5000':
                fs_5000[0].append(float(cols[0]))
                fs_5000[1].append(float(cols[2]))

            if cols[1] == '2000':
                fs_2000[0].append(float(cols[0]))
                fs_2000[1].append(float(cols[2]))

            if cols[1] == '1000':
                fs_1000[0].append(float(cols[0]))
                fs_1000[1].append(float(cols[2]))

            if cols[1] == '500':
                fs_500[0].append(float(cols[0]))
                fs_500[1].append(float(cols[2]))

            if cols[1] == '250':
                fs_250[0].append(float(cols[0]))
                fs_250[1].append(float(cols[2]))

    print('Plotting f vs sigma for multiple N')

    plt.plot(fs_5000[0], fs_5000[1], '.', label = 'N = 5000', color = colors_fvs[0], marker = markers[0])
    poly_5000 = np.polyfit(fs_5000[0], fs_5000[1], 3)
    p = np.poly1d(poly_5000)
    xp = np.linspace(min(fs_5000[0]), max(fs_5000[0]), 100)
    plt.plot(xp, p(xp), color = colors_fvs_polyfit[0])

    plt.plot(fs_2000[0], fs_2000[1], '.', label = 'N = 2000', color = colors_fvs[1], marker = markers[1])
    poly_2000 = np.polyfit(fs_2000[0], fs_2000[1], 3)
    p = np.poly1d(poly_2000)
    xp = np.linspace(min(fs_2000[0]), max(fs_2000[0]), 100)
    plt.plot(xp, p(xp), color = colors_fvs_polyfit[1])

    plt.plot(fs_1000[0], fs_1000[1], '.', label = 'N = 1000', color = colors_fvs[2], marker = markers[2])
    poly_1000 = np.polyfit(fs_1000[0], fs_1000[1], 3)
    p = np.poly1d(poly_1000)
    xp = np.linspace(min(fs_1000[0]), max(fs_1000[0]), 100)
    plt.plot(xp, p(xp), color = colors_fvs_polyfit[2])

    plt.plot(fs_500[0], fs_500[1], '.', label = 'N = 500', color = colors_fvs[3], marker = markers[3])
    poly_500 = np.polyfit(fs_500[0], fs_500[1], 3)
    p = np.poly1d(poly_500)
    xp = np.linspace(min(fs_500[0]), max(fs_500[0]), 100)
    plt.plot(xp, p(xp), color = colors_fvs_polyfit[3])

    plt.plot(fs_250[0], fs_250[1], '.', label = 'N = 250', color = colors_fvs[4], marker = markers[4])
    poly_250 = np.polyfit(fs_250[0], fs_250[1], 3)
    p = np.poly1d(poly_250)
    xp = np.linspace(min(fs_5000[0]), max(fs_250[0]), 100)
    plt.plot(xp, p(xp), color = colors_fvs_polyfit[4])

    plt.legend()
    plt.xlabel('F parameter')
    plt.ylabel('Uncertainty on position (mm)')
    print()


    print('Reading in data from', n_v_s_data)

    ns_data = []
    ns_data.append([])
    ns_data.append([])

    with open(n_v_s_data) as f:
        header = f.readline()       # ignore header

        for line in f:
            cols = line.split(',')
            ns_data[0].append(float(cols[0]))
            ns_data[1].append(float(cols[1]))

    ns_data = np.array(ns_data)
    # Fit a 1/sqrt(N) curve to the data
    def u_pois(x, a):
        return a/np.sqrt(x)

    popt, pcov = curve_fit(u_pois, ns_data[0], ns_data[1], [1])

    print('Plotting N vs sigma subfigure')

    a = plt.axes([.35, .55, .25, .3])
    plt.plot(ns_data[0], ns_data[1], '.', color = 'lightgrey', label = 'u(r)') #, min = ' + str(min_fopt_r))   # plot data
    # Plot 1/sqrt(N) fit
    x_fit = np.linspace(min(ns_data[0]), max(ns_data[0]), 100)
    plt.plot(x_fit, u_pois(x_fit, popt[0]), color = 'grey')

    plt.xlabel('Number of lines per position (N)')
    plt.ylabel('Uncertainty (mm)')

    plt.show()

    return

# Plot x, y and z position distributions
def position_distr(data_dir, multiple_runs = False, plot_mvk = True, mvk_x_dirs = [], mvk_y_dirs = [], mvk_z_dirs = []):
    '''
    Takes in a series of datasets and extracts the x, y and z coordinates for
    each one, plotting the distribution of the results.

    @params:
        data_dir            - Required : Path to directories containing *.a files containing the position (Str)
        multiple_runs       - Optional : Whether or not the given directory contains multiple runs to read (Bool, default = False)
        mvk_x_dirs          - Optional : Path to the directories to determine the x-axis measured vs known data (String[], default = [])
        mvk_y_dirs          - Optional : Path to the directories to determine the y-axis measured vs known data (String[], default = [])
        mvk_z_dirs          - Optional : Path to the directories to determine the z-axis measured vs known data (String[], default = [])

    @returns:
        null
    '''

    font_size = 30
    # matplotlib.rc('font',family='Palatino')
    plt.rcParams.update({'font.size': font_size})

    # Empty lists to hold the position data
    t_s = []
    x_s = []
    y_s = []
    z_s = []

    # # Compile regular expressions for extracting relevent information from txt files
    # re_time = re.compile(r'Run duration \(s\): (\d+\.\d+)')
    # re_singles = re.compile(r'run activity \(events/s\): (\d+\.\d+)')
    # re_coin = re.compile(r'number of coincidence events returned: (\d+)')

    if multiple_runs:
        for subdir, dirs, files in os.walk(data_dir):        # loop through all datasets in directory
            for dir in dirs:

                # Skip irrelevant directories
                if dir[0] != 'J':
                    continue

                fname = data_dir + dir + '/pept_coincidence_data.a'   # construct directory to position file

                with open(fname, 'r') as f:
                    f.readline()        # discard empty line

                    for line in f:
                        columns = line.split()
                        t_s.append(float(columns[0]))
                        x_s.append(float(columns[1]))
                        y_s.append(float(columns[2]))
                        z_s.append(float(columns[3]))
            break

    else:
        fname = data_dir + 'pept_coincidence_data.a'   # construct directory to position file

        with open(fname, 'r') as f:
            f.readline()        # discard empty line

            for line in f:
                columns = line.split()
                t_s.append(float(columns[0]))
                x_s.append(float(columns[1]))
                y_s.append(float(columns[2]))
                z_s.append(float(columns[3]))

    # Convert all lists into numpy arrays
    t_s = np.array(t_s)
    x_s = np.array(x_s)
    y_s = np.array(y_s)
    z_s = np.array(z_s)

    t_s = t_s - t_s[0]  # subtract off smallest time to plot from t = 0
    t_s = t_s/60e3      # convert to minutes

    t_s = t_s*60        # convert to seconds

    # Only take the first sixth of the data
    # j = int(len(t_s)/6)
    # t_s = t_s[:j]
    # x_s = x_s[:j]
    # y_s = y_s[:j]
    # z_s = z_s[:j]

    print('Plotting total of {:.0f} locations.'.format(len(t_s)))

    # Plotting the results
    # ====================
    if plot_mvk:
        fig, axs = plt.subplots(nrows=4, ncols=3, sharey=False, sharex=False, gridspec_kw = {'height_ratios':[1, 1, 1, 1]}, figsize=(15, 15), dpi = 500)
    else:
        fig, axs = plt.subplots(nrows=2, ncols=3, sharey=False, sharex=False, gridspec_kw = {'height_ratios':[1, 1]}, figsize=(21, 16), dpi = 500)

    # print(axs)
    # figure(num=None, figsize=(21, 16), dpi=200, facecolor='w', edgecolor='k')      # Set plot size

    norm_plot_x = axs[1][0]          # normal distribution plot of the x position
    scat_plot_x = axs[0][0]          # scatter plot of the x positions

    norm_plot_y = axs[1][1]          # normal distribution plot of the y position
    scat_plot_y = axs[0][1]          # scatter plot of the y positions

    norm_plot_z = axs[1][2]          # normal distribution plot of the z position
    scat_plot_z = axs[0][2]          # scatter plot of the z positions

    if plot_mvk:
        mvk_plot_x = axs[2][0]           # measured vs known positions of z-axis
        mvkr_plot_x = axs[3][0]          # residuals on measured vs known positions of z-axis

        mvk_plot_y = axs[2][1]           # measured vs known positions of z-axis
        mvkr_plot_y = axs[3][1]          # residuals on measured vs known positions of z-axis

        mvk_plot_z = axs[2][2]           # measured vs known positions of z-axis
        mvkr_plot_z = axs[3][2]          # residuals on measured vs known positions of z-axis

    scat_style = 'o'                 # scatter plot marker style
    scat_size = 1                    # scatter plot marker size
    scat_mkr_col = 'k'               # scatter plot marker colour
    scat_ln_col = 'r'                # scatter plot mean line colour

    # Plotting the normal distribution
    # --------------------------------

    # plt.rcParams.update({'font.size': 20})

    def gaus(x, mu, sigma):
        return 1.0/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x - mu)**2/(2*sigma**2))

    x_scl = 8
    y_scl = 1.3

    # x position
    # ----------
    # Get data/paramters
    mu_x, sigma = norm.fit(x_s)             # get normal distribution parameters
    fwhm = 2*np.sqrt(2*np.log(2))*sigma     # full width at half maximum
    # lbl = r'$\mu$ = ' + str(round(mu_x,3)) + 'mm' + '\nFWHM = ' + str(round(2*np.sqrt(2*np.log(2))*sigma,3)) + 'mm'
    norm_x = np.linspace(min(x_s), max(x_s), 100)
    norm_y = norm.pdf(norm_x, mu_x, sigma)
    # norm_y = gaus(norm_x, mu_x, sigma)

    # Plot the results
    norm_plot_x.plot(norm_x, norm_y)#, label = lbl)
    norm_plot_x.plot(np.linspace(mu_x-fwhm/2, mu_x+fwhm/2, 100), np.ones(100)*max(norm_y)/2, 'r', label = 'FWHM = {:.3f}mm'.format(fwhm))       # plot the FWHM line
    # norm_plot_x.plot((mu_x, mu_x), (0, max(norm_y)*0.05), '-r')      # plot the centre line
    norm_plot_x.hist(x_s, bins = 25, density = True, color = 'lightgray')        # histogram the positions

    # Set plot parameters, add labels, etc.
    # norm_plot_x.axis('off')
    norm_plot_x.legend(loc = 'upper right', fontsize = font_size)
    norm_plot_x.set_xlim(np.mean(x_s) - x_scl*np.std(x_s), np.mean(x_s) + x_scl*np.std(x_s))
    norm_plot_x.set_ylim(0, max(norm_y)*y_scl)
    # norm_plot_x.set_xlabel('x position (mm)', fontsize = font_size)
    # norm_plot_x.annotate('FWHM = {:.3f}mm'.format(fwhm), (mu_x, max(norm_y)/2), (mu_x + fwhm*0.9, max(norm_y)/2*1.5), arrowprops = dict(arrowstyle='-|>'))
    # norm_plot_x.annotate(r'$\mu$ = {:.3f}mm'.format(mu_x), (mu_x, max(norm_y)*0.025), (mu_x + fwhm, max(norm_y)*0.15), arrowprops = dict(arrowstyle='-|>'))

    np.savetxt(data_dir + 'x_norm_fit.txt', np.c_[norm_x, norm_y], fmt = '%.4f', delimiter = ',')

    if plot_mvk:
        # Plotting the measured vs known positions and residuals #
        known_data_x, known_udata_x, meas_data_x, meas_udata_x = measured_vs_known(mvk_x_dirs, 80, 100, 1, plot_results = False)

        _m, _c, r, p_value, std_err = stats.linregress(known_data_x, meas_data_x)
        m, um, c, uc = linfit.linfit(known_data_x, meas_data_x)

        # Plot the data
        _ = make_error_boxes(mvk_plot_x, known_data_x, meas_data_x, 3*known_udata_x, 3*meas_udata_x)

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(known_data_x), max(known_data_x))
        mvk_plot_x.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)), zorder = 1, lw = 0.5)
        mvk_plot_x.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
        mvk_plot_x.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

        ## Plot residuals ##
        residuals = meas_data_x - linear(known_data_x, m, c)
        mvkr_plot_x.plot(known_data_x, residuals, '.r')
        mvkr_plot_x.plot(fit_x, np.zeros(len(fit_x)), 'k')


    # y position
    # ----------
    # Get data/paramters
    mu_y, sigma = norm.fit(y_s)
    fwhm = 2*np.sqrt(2*np.log(2))*sigma     # full width at half maximum
    # lbl = r'$\mu$ = ' + str(round(mu_y,3)) + 'mm' + '\nFWHM = ' + str(round(2*np.sqrt(2*np.log(2))*sigma,3)) + 'mm'
    norm_x = np.linspace(min(y_s), max(y_s), 100)
    norm_y = gaus(norm_x, mu_y, sigma)

    # Plot the results
    norm_plot_y.plot(norm_x, norm_y)#, label = lbl)
    norm_plot_y.plot(np.linspace(mu_y-fwhm/2, mu_y+fwhm/2, 100), np.ones(100)*max(norm_y)/2, 'r', label = 'FWHM = {:.3f}mm'.format(fwhm))       # plot the FWHM line
    # norm_plot_y.plot((mu_y, mu_y), (0, max(norm_y)*0.05), '-r')      # plot the centre line
    norm_plot_y.hist(y_s, bins = 25, density = True, color = 'lightgray', stacked = True)        # histogram the positions

    # Set plot parameters, add labels, etc.
    # norm_plot_y.axis('off')
    norm_plot_y.legend(loc = 'upper right', fontsize = font_size)
    norm_plot_y.set_xlim(np.mean(y_s) - x_scl*np.std(y_s), np.mean(y_s) + x_scl*np.std(y_s))
    norm_plot_y.set_ylim(0, max(norm_y)*y_scl)
    # norm_plot_y.set_xlabel('y position (mm)', fontsize = font_size)
    # norm_plot_y.annotate('FWHM = {:.3f}mm'.format(fwhm), (mu_y, max(norm_y)/2), (mu_y + fwhm*0.9, max(norm_y)/2*1.5), arrowprops = dict(arrowstyle='-|>'))
    # norm_plot_y.annotate(r'$\mu$ = {:.3f}mm'.format(mu_y), (mu_y, max(norm_y)*0.025), (mu_y + fwhm, max(norm_y)*0.15), arrowprops = dict(arrowstyle='-|>'))

    np.savetxt(data_dir + 'y_norm_fit.txt', np.c_[norm_x, norm_y], fmt = '%.4f', delimiter = ',')

    if plot_mvk:
        # Plotting the measured vs known positions and residuals #
        known_data_y, known_udata_y, meas_data_y, meas_udata_y = measured_vs_known(mvk_y_dirs, 80, 100, 2, plot_results = False)

        _m, _c, r, p_value, std_err = stats.linregress(known_data_y, meas_data_y)
        m, um, c, uc = linfit.linfit(known_data_y, meas_data_y)

        # Plot the data
        _ = make_error_boxes(mvk_plot_y, known_data_y, meas_data_y, 3*known_udata_y, 3*meas_udata_y)

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(known_data_y), max(known_data_y))
        mvk_plot_y.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)), zorder = 1, lw = 0.5)
        mvk_plot_y.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
        mvk_plot_y.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

        ## Plot residuals ##
        residuals = meas_data_y - linear(known_data_y, m, c)
        mvkr_plot_y.plot(known_data_y, residuals, '.r')
        mvkr_plot_y.plot(fit_x, np.zeros(len(fit_x)), 'k')


    # z position
    # ----------
    # Get data/paramters
    mu_z, sigma = norm.fit(z_s)
    fwhm = 2*np.sqrt(2*np.log(2))*sigma     # full width at half maximum
    # lbl = r'$\mu$ = ' + str(round(mu_z,3)) + 'mm' + '\nFWHM = ' + str(round(2*np.sqrt(2*np.log(2))*sigma,3)) + 'mm'
    norm_x = np.linspace(min(z_s), max(z_s), 100)
    norm_y = gaus(norm_x, mu_z, sigma)

    # Plot the results
    norm_plot_z.plot(norm_x, norm_y)#, label = lbl)
    norm_plot_z.plot(np.linspace(mu_z-fwhm/2, mu_z+fwhm/2, 100), np.ones(100)*max(norm_y)/2, 'r', label = 'FWHM = {:.3f}mm'.format(fwhm))       # plot the FWHM line
    # norm_plot_z.plot((mu_z, mu_z), (0, max(norm_y)*0.05), '-r')      # plot the centre line
    norm_plot_z.hist(z_s, bins = 25, density = True, color = 'lightgray')        # histogram the positions

    # Set plot parameters, add labels, etc.
    # norm_plot_z.axis('off')
    norm_plot_z.legend(loc = 'upper right', fontsize = font_size)
    norm_plot_z.set_xlim(np.mean(z_s) - x_scl*np.std(z_s), np.mean(z_s) + x_scl*np.std(z_s))
    norm_plot_z.set_ylim(0, max(norm_y)*y_scl)
    # norm_plot_z.set_xlabel('z position (mm)', fontsize = font_size)
    # norm_plot_z.annotate('FWHM = {:.3f}mm'.format(fwhm), (mu_z, max(norm_y)/2), (mu_z + fwhm*0.9, max(norm_y)/2*1.5), arrowprops = dict(arrowstyle='-|>'))
    # norm_plot_z.annotate(r'$\mu$ = {:.3f}mm'.format(mu_z), (mu_z, max(norm_y)*0.025), (mu_z + fwhm, max(norm_y)*0.15), arrowprops = dict(arrowstyle='-|>'))

    np.savetxt(data_dir + 'z_norm_fit.txt', np.c_[norm_x, norm_y], fmt = '%.4f', delimiter = ',')

    if plot_mvk:
        # Plotting the measured vs known positions and residuals #
        known_data_z, known_udata_z, meas_data_z, meas_udata_z = measured_vs_known(mvk_z_dirs, 80, 100, 3, plot_results = False)

        _m, _c, r, p_value, std_err = stats.linregress(known_data_z, meas_data_z)
        m, um, c, uc = linfit.linfit(known_data_z, meas_data_z)

        # Plot the data
        _ = make_error_boxes(mvk_plot_z, known_data_z, meas_data_z, 3*known_udata_z, 3*meas_udata_z)

        # Plot the fit + uncertainties
        fit_x = np.linspace(min(known_data_z), max(known_data_z))
        mvk_plot_z.plot(fit_x, linear(fit_x, m, c), 'grey',
                    label = 'm = ' + str(round(m, 3)) + r' $\pm$ ' + str(round(um ,3)) + '\n' +
                            'c = ' + str(round(c, 3)) + r' $\pm$ ' + str(round(uc ,3)) + '\n' +
                            r'r$^{2}$ = ' + str(round(r**2, 5)), zorder = 1, lw = 0.5)
        mvk_plot_z.plot(fit_x, linear(fit_x, m+um, c+uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)
        mvk_plot_z.plot(fit_x, linear(fit_x, m-um, c-uc), linestyle = '--', color = 'lightgrey', zorder = 1, lw = 0.5)

        ## Plot residuals ##
        residuals = meas_data_z - linear(known_data_z, m, c)
        mvkr_plot_z.plot(known_data_z, residuals, '.r')
        mvkr_plot_z.plot(fit_x, np.zeros(len(fit_x)), 'k')


    # Plotting the scatter distribution
    # ---------------------------------

    np.savetxt(data_dir + 'positions_v_time.txt', np.c_[t_s, x_s, y_s, z_s], fmt = '%.4f', delimiter = ',')

    # x position
    scat_plot_x.scatter(x_s, t_s, s = scat_size, marker = scat_style, color = scat_mkr_col)
    scat_plot_x.plot((mu_x, mu_x), (min(t_s), max(t_s)), color = scat_ln_col)
    # scat_plot_x.set_xlabel('x position (mm)', fontsize = font_size)
    scat_plot_x.set_xlim(np.mean(x_s) - x_scl*np.std(x_s), np.mean(x_s) + x_scl*np.std(x_s))

    # y position
    scat_plot_y.scatter(y_s, t_s, s = scat_size, marker = scat_style, color = scat_mkr_col)
    scat_plot_y.plot((mu_y, mu_y), (min(t_s), max(t_s)), color = scat_ln_col)
    # scat_plot_y.set_xlabel('y position (mm)', fontsize = font_size)
    scat_plot_y.set_xlim(np.mean(y_s) - x_scl*np.std(y_s), np.mean(y_s) + x_scl*np.std(y_s))

    # x position
    scat_plot_z.scatter(z_s, t_s, s = scat_size, marker = scat_style, color = scat_mkr_col)
    scat_plot_z.plot((mu_z, mu_z), (min(t_s), max(t_s)), color = scat_ln_col)
    # scat_plot_z.set_xlabel('z position (mm)', fontsize = font_size)
    scat_plot_z.set_xlim(np.mean(z_s) - x_scl*np.std(z_s), np.mean(z_s) + x_scl*np.std(z_s))

    scat_plot_x.set_ylabel('Time (s)', fontsize = font_size)
    norm_plot_x.set_ylabel(r'P (mm$^{-1}$)', fontsize = font_size)

    if plot_mvk:
        mvkr_plot_x.set_ylabel('Residuals \n(mm)', fontsize = font_size)
        mvk_plot_x.set_ylabel('Measured \ncoordinate \n(mm)', fontsize = font_size)

        mvkr_plot_x.set_xlabel('X-Coordinate (mm)', fontsize = font_size)
        mvkr_plot_y.set_xlabel('Y-Coordinate (mm)', fontsize = font_size)
        mvkr_plot_z.set_xlabel('Z-Coordinate (mm)', fontsize = font_size)
    else:
        norm_plot_x.set_xlabel('X-Coordinate (mm)', fontsize = font_size)
        norm_plot_y.set_xlabel('Y-Coordinate (mm)', fontsize = font_size)
        norm_plot_z.set_xlabel('Z-Coordinate (mm)', fontsize = font_size)

    # Global settings
    for ax_arr in axs:
        for ax in ax_arr:
            ax.tick_params(labelsize = font_size)       # ensure the tick font size is the same as the lables
    # fig.tight_layout()              # ensure that all the labels and ticks are visible
    # fig.subplots_adjust(hspace=0)   # remove horizontal space between axes

    plt.savefig(data_dir + 'position_distributions_stationary.png')
    # plt.show()

    return

# Run ctrack on all files in given directory
def ctrack_all(data_dir, fopt, N):
    '''
    Takes in a directory of data files and runs the ctrack algorithm on all of
    them to generate the positions output .a file.

    @params:
        data_dir        - Required : Directory containing all of the Polaris data (Str)
        fopt            - Required : F parameter to run ctrack with (int)
        N               - Required : Number of lines per position

    @returns:
        null
    '''

    # Create progress bar

    for subdir, dirs, files in os.walk(data_dir):        # loop through all datasets in directory
        for dir in dirs:

            print('Running ctrack on ', dir)

            # Skip the apparatus directory
            if dir == 'apparatus':
                continue

            fname = data_dir + dir + '/pept_coincidence_data'   # construct directory to coincidences file

            run_ctrack(fname, fopt, N)   # run ctrack

        break

    return

# Plot the positions data_file of moving source
def plot_moving(data_dir, data_filename, timing, N, fopt, plot_errbars = True, err = [0.0, 0.0, 0.0], time_cutoff = [], y_limit = 10, apply_fit = False, fit_functions = None, p0s = [], p0_labels = []):
    '''
    Takes in a .a file (produced by the ctrack algorithm) and extracts the x,y,z
    positions, and then plots those as a function of time on a single plot.
    Saves the plot to a pept_plots directory in the data directory.

    NOTE: The transformed coordinate system of the detectors and the 3D printer
    are not the same.
        * Detector coordinate system (right-handed):

                              Y
        ------               / \                 ------
        |    |                |                  |    |
        | D0 | ------------------------------> X | D1 |
        |    |                |                  |    |
        ------                |                  ------

        * 3D Printer coordinate system (left handed)

        ------                |                  ------
        |    |                |                  |    |
        | D0 | Y <------------------------------ | D1 |
        |    |                |                  |    |
        ------               \ /                 ------
                              X

    @params:
        data_dir        - Required : Path to .a file to extract positions from (Str)
        data_filename   - Required : Name of .a file to extract positions from (Str)
        timing          - Required : Coincidence timing window used in nanoseconds. For file naming (Int)
        N               - Required : Number of lines per position. For file naming (Int)
        fopt            - Required : Optimal f value for PEPT processing. For file naming (Int)
        err             - Optional : Error for x,y and z (Float[], default = [0.0, 0.0, 0.0])
        time_cutoff     - Optional : Time before and after which to cutoff the plot (Float[], default = [])
        y_limit         - Optional : Specified y-axis limit for plotting (Float, default = 10)
        plot_fit        - Optional : Whether to plot known movement over the measured (Bool, default = False)
        fit_file        - Optional : Path to file containing the known coordinates. Expects gcode file (Str, default = '')
        fit_params      - Optional : Array of parameters for plotting known coordinates. Includes x,y,z offsets and speed (Float[], default = [])
        apply_fit       - Optional : Whether to apply a least-squares fit to the data (Bool, default = False)
        fit_functions   - Optional : Array of functions to fit to x, y and z data (Func[3], default = None)
        p0s             - Optional : Array of initial guess arrays for each fit (Float[3][], default = [])
        p0_labels       - Optional : Array of names of the function parameters for fit (Str[3][], default = [])

    @returns:
        null
    '''
    data_file = data_dir + data_filename        # create full file path
    N = int(N)

    # Arrays to hold x,y,z positions and time stamps
    ts = []
    xs = []
    ys = []
    zs = []
    us = []

    # Open file and read in all information
    print('Reading in positions...')
    with open(data_file) as f:
        header = f.readline()       # ignore blank first row

        for line in f:
            line = line.strip()
            columns = line.split()
            ts.append(float(columns[0]))
            xs.append(float(columns[1]))
            ys.append(float(columns[2]))
            zs.append(float(columns[3]))
            us.append(float(columns[4]))

    # Convert python lists to numpy arrays
    ts = np.array(ts)
    xs = np.array(xs)
    ys = np.array(ys)
    zs = np.array(zs)

    # Convert time to seconds
    # ts = ts/4e3 + 3
    ts = (ts - ts[0])/1e3

    # If a time cutoff has been specified
    if len(time_cutoff) > 0:
        cutoff_index_lower = np.where(ts < time_cutoff[0])[0][-1]     # get the cutoff index of the first time less than the specified time cutoff
        cutoff_index_upper = np.where(ts > time_cutoff[1])[0][0]     # get the cutoff index of the first time less than the specified time cutoff

        # Shorten all the array
        ts = ts[cutoff_index_lower:cutoff_index_upper + 1]
        xs = xs[cutoff_index_lower:cutoff_index_upper + 1]
        ys = ys[cutoff_index_lower:cutoff_index_upper + 1]
        zs = zs[cutoff_index_lower:cutoff_index_upper + 1]
        us = us[cutoff_index_lower:cutoff_index_upper + 1]

    pos_datasets = [xs, ys, zs]

    print('Done.')

    # Plotting the results
    # ====================

    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size

    print('Plotting raw data...')

    # Create x,y,z position plots above each other
    plt.rcParams.update({'font.size': 30})
    fig, axs = plt.subplots(nrows=3, ncols=1, sharey=False, gridspec_kw = {'height_ratios':[1, 1, 1]}, figsize=(21, 16), dpi = 150)   # horizontal plot
    # fig, axs = plt.subplots(nrows=1, ncols=3, sharey=False, gridspec_kw = {'width_ratios':[1, 1, 1]}, figsize=(21, 16), dpi = 150)     # verticle plot

    # Plot x positions
    if plot_errbars:
        axs[0].errorbar(ts, xs, yerr = us, fmt = 's', ecolor = 'black', mfc = 'red')
    else:
        axs[0].plot(ts, xs, 'k.')   # horizontal
        # axs[0].plot(xs, ts, 'k.')   # vertical
    axs[0].set_ylabel('X-Coordinate (mm)')      # horizontal
    # axs[0].set_xlabel('X-Coordinate (mm)')      # vertical
    # axs[0].set_ylabel('Time (s)')               # vertical
    axs[0].set_ylim(-y_limit, y_limit)


    # Plot y positions
    if plot_errbars:
        axs[1].errorbar(ts, ys, yerr = us, fmt = 's', ecolor = 'black', mfc = 'red')
    else:
        axs[1].plot(ts, ys, 'k.')   # horizontal
        # axs[1].plot(ys, ts, 'k.')   # vertical
    axs[1].set_ylabel('Y-Coordinate (mm)')      # horizontal
    # axs[1].set_xlabel('Y-Coordinate (mm)')      # vertical
    axs[1].set_ylim(-y_limit, y_limit)

    # Plot z positions
    if plot_errbars:
        axs[2].errorbar(ts, zs, yerr = us, fmt = 's', ecolor = 'black', mfc = 'red')
    else:
        axs[2].plot(ts, zs, 'k.')   # horizontal
        # axs[2].plot(zs, ts, 'k.')   # vertical
    axs[2].set_xlabel('Time (s)')               # horizontal
    axs[2].set_ylabel('Z-Coordinate (mm)')      # horizontal
    axs[2].set_ylim(-9, 9)                      # horizontal
    # axs[2].set_xlabel('Z-Coordinate (mm)')  # vertical
    # axs[2].set_xlim(-9, 9)                  # vertical

    print('Done.')

    # for ax in axs:
    #     # ax.set_xlim(0, 500000)
    #     ax.set_ylim(-3, 3)

    # Create plotting directory
    if not os.path.exists(data_dir + 'tracking_plots/'):
        os.makedirs(data_dir + 'tracking_plots/')
        print ('Creating directory: {}'.format(data_dir + 'tracking_plots/'))

    # Apply and plot a least squares fit
    # ==================================
    if apply_fit:

        # Creating a log file to write fit results to
        log_filename = '{}/tracking_plots/tracking_fit_results-N{:d}-F{:d}.log'.format(data_dir, N, fopt)
        log_file = open(log_filename, 'w')

        print('Applying least squares fit on measured data')
        print('-------------------------------------------')
        coord_labels = ['x', 'y', 'z']
        for i in range(3):      # for each x, y, z dataset

            popt, pcov = curve_fit(fit_functions[i], ts, pos_datasets[i], p0s[i], sigma = us)
            perr = np.sqrt(np.diag(pcov))

            print()
            print('Fit parameters for', coord_labels[i] + ':')
            log_file.write('Fit parameters for ' + coord_labels[i] + ':\n')
            for j in range(len(p0_labels[i])):
                print_str = '{:8}{:^3}{:6.8f}{:^4}{:.8f}'.format(p0_labels[i][j], '=', popt[j], '+-', perr[j])
                print(print_str)
                log_file.write(print_str + '\n')

            x_fit = np.linspace(min(ts), max(ts), 200)
            y_fit = fit_functions[i](x_fit, *popt)
            axs[i].plot(x_fit, y_fit, 'r')      # horizontal
            # axs[i].plot(y_fit, x_fit, 'r')      # vertical

        log_file.close()
        print('Done')

    print('Saving plot to', data_dir + 'tracking_plots/xyz_positions_' + str(timing) + 'ns_N' + str(N) + '_F' + str(fopt) + '.png')
    plt.savefig(data_dir + 'tracking_plots/xyz_positions_' + str(timing) + 'ns_N' + str(N) + '_F' + str(fopt) + '.png')
    # plt.clf()
    # plt.show()

    # # TEMP: Plotting yz plane
    # figure(num=None, figsize=(5, 5), dpi=200, facecolor='w', edgecolor='k', )      # Set plot size
    # plt.plot(ys, zs, '.')
    # plt.show()

    return

# Plot the positions data_file of moving source
def plot_moving_2D(data_dirs, write_dir, data_filename, timing, N, fopt, n_avg = 1, rad_labels = [''], plot_pos = True, pos_size = 2, pos_colors = ['black'], plot_vel = True, vel_size = 0.5, vel_colors = ['black']):
    '''
    Takes in a ctrack-processed datafile of the positions as a function of time
    and produces position and velocity plots in 2-dimensions.

    @params:
        data_dirs       - Required : List of paths to .a file to extract positions from (Str)
        write_dir       - Required : Path to write final plot to (Str)
        data_filename   - Required : Name of .a file to extract positions from (Str)
        timing          - Required : Coincidence timing window used in nanoseconds. For file naming (Int)
        N               - Required : Number of lines per position. For file naming (Int)
        fopt            - Required : Optimal f value for PEPT processing. For file naming (Int)
        n_avg           - Required : Number of points to average over (Int)
        rad_labels      - Optional : List of labels for the radius of each fit (String[], default = [])
        plot_pos        - Optional : Whether to plot the positions or not (Bool, default = True)
        pos_size        - Optional : Markersize for the position plots (Float, default = 1)
        pos_colors      - Optional : List of colors for the position points (String[], default = [])
        plot_vel        - Optional : Whether to plot the velocity arrows or not (Bool, default = True)
        vel_size        - Optional : Scale for the velocity arrows (Float, default = 0.5)
        vel_colors      - Optional : List of colors for the velocity arrows (String[], default = [])

    @returns:
        null
    '''
    plt.rcParams.update({'font.size': 15})

    if plot_pos and plot_vel:
        fig, axs = plt.subplots(nrows=1, ncols=2, sharex=False, gridspec_kw = {'width_ratios':[1, 1]}, figsize=(8, 4), dpi = 500)
        pos_ax = axs[0]
        vel_ax = axs[1]

    if plot_pos and not plot_vel:
        fig, pos_ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 8), dpi = 500)

    if not plot_pos and plot_vel:
        fig, vel_ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 8), dpi = 500)

    for i, data_dir in enumerate(data_dirs):
        print('Running analysis on', data_dir)

        run_ctrack(data_dir + 'pept_coincidence_data', fopt, N)

        data_file = data_dir + data_filename        # create full file path

        pt_data = []    # empty list to hold the position-time data

        # Open file and read in all information
        print('Reading in positions...')
        with open(data_file) as f:
            header = f.readline()       # ignore blank first row

            for line in f:
                line = line.strip()
                columns = line.split()

                t = float(columns[0])/1e3       # get time in seconds
                x = float(columns[1])
                y = float(columns[2])
                z = float(columns[3])

                pt_data.append(np.array([t,x,y,z]))

        # Convert python lists to numpy arrays
        pt_data = np.array(pt_data)

        # Average out points
        n = n_avg
        num_pnts = len(pt_data)
        pt_data = pt_data[:num_pnts-num_pnts%n]     # shorten array to make averaging work
        num_pnts = len(pt_data)
        pt_data = pt_data.T
        pt_data_avg = []
        for j in range(0, num_pnts, n):
            ta = sum(pt_data[0][j:j+n])/n
            xa = sum(pt_data[1][j:j+n])/n
            ya = sum(pt_data[2][j:j+n])/n
            za = sum(pt_data[3][j:j+n])/n
            pt_data_avg.append(np.array([ta,xa,ya,za]))

        pt_data = np.array(pt_data_avg)

        print('Done.')

        # Velocity plots
        # --------------
        if plot_vel:
            print('Calculating velocities...')

            vt_data = calculate_velocities(pt_data)
            pt_data = pt_data
            pt_data = pt_data[5:-5]

            vel_mags = np.sqrt(vt_data[:,1]**2 + vt_data[:,2]**2 + vt_data[:,3]**2)
            avg_speed = np.mean(vel_mags)
            u_avg_speed = np.sqrt(np.var(avg_speed))

            print('Average speed = {:.2f} +- {:.2f} mm/s'.format(avg_speed, u_avg_speed))

            print('Done.')

        # Plotting the results
        # ====================

        # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size

        print('Plotting the results...')

        # Plot the positions
        if plot_pos:
            lim_scale = 1.3         # to set the coordinate limits
            pos_ax.plot(pt_data[:,1], pt_data[:,2], '.', ms = pos_size, color = pos_colors[i], label = rad_labels[i])
            # pos_ax.set_xlim(lim_scale*min(pt_data[:,1]), lim_scale*max(pt_data[:,1]))
            # pos_ax.set_ylim(lim_scale*min(pt_data[:,2]), lim_scale*max(pt_data[:,2]))
            pos_ax.set_xlim(-10, 10)
            pos_ax.set_ylim(-10, 10)
            pos_ax.set_xlabel('X-coordinate (mm)')
            pos_ax.set_ylabel('Y-coordinate (mm)')

        # Plot the velcity arrows
        if plot_vel:
            for j in range(len(pt_data)):
                # vel_ax.plot(pt_data[i,1], pt_data[i,2], 'b.')
                vel_ax.arrow(pt_data[j,1], pt_data[j,2], vt_data[j,1]*vel_size, vt_data[j,2]*vel_size, width = 0.025, fc = vel_colors[i])
            vel_ax.set_xlim(lim_scale*min(pt_data[:,1]), lim_scale*max(pt_data[:,1]))
            vel_ax.set_ylim(lim_scale*min(pt_data[:,2]), lim_scale*max(pt_data[:,2]))
            vel_ax.set_xlabel('X-coordinate (mm)')

        print('-------------------------------------------------\n')

    # plt.show()

    plt.legend(markerscale = 3, framealpha = 1)

    print('Done.')

    figname = '{}xy_positions_{}ns_N{}_F{}'.format(write_dir, timing, N, fopt)

    if plot_vel:
        figname += '_vel'
    if plot_pos:
        figname += '_pos'

    for rad in rad_labels:
        figname += '_{}'.format(rad)

    figname += '.png'

    print('Saving plot to', figname)
    plt.savefig(figname)

    return

# Calculate the velocity vectors from a given set of position vectors
def calculate_velocities(pt_data):
    '''
    Takes in a 2D array with time in the first column and position in the second
    and calcuates the velocity at each time based on a six-point method.

    @params:
        pt_data        - Required : 2D numpy array with time in the first column and position in the second

    @returns:
        vt_data        - 2D numpy array with time in the first column and velocity in the second
    '''

    vt_data = pt_data.copy()      # copy the position-time data
    vt_data[:,1] *= 0                        # zero out the position data
    vt_data[:,2] *= 0                        # zero out the position data
    vt_data[:,3] *= 0                        # zero out the position data

    # print(len(vt_data))
    # print(len(pt_data))

    for i in range(5, len(pt_data) - 5):       # loop through the array, starting at element 5 and ending 5 before the end
        pos_vec5 = np.array([pt_data[i+5][1], pt_data[i+5][2], pt_data[i+5][3]])
        pos_vec4 = np.array([pt_data[i+4][1], pt_data[i+4][2], pt_data[i+4][3]])
        pos_vec3 = np.array([pt_data[i+3][1], pt_data[i+3][2], pt_data[i+3][3]])
        pos_vec2 = np.array([pt_data[i+2][1], pt_data[i+2][2], pt_data[i+2][3]])
        pos_vec1 = np.array([pt_data[i+1][1], pt_data[i+1][2], pt_data[i+1][3]])
        pos_vec0 = np.array([pt_data[i][1], pt_data[i][2], pt_data[i][3]])
        pos_vec1_ = np.array([pt_data[i-1][1], pt_data[i-1][2], pt_data[i-1][3]])
        pos_vec2_ = np.array([pt_data[i-2][1], pt_data[i-2][2], pt_data[i-2][3]])
        pos_vec3_ = np.array([pt_data[i-3][1], pt_data[i-3][2], pt_data[i-3][3]])
        pos_vec4_ = np.array([pt_data[i-4][1], pt_data[i-4][2], pt_data[i-4][3]])
        pos_vec5_ = np.array([pt_data[i-5][1], pt_data[i-5][2], pt_data[i-5][3]])

        t1 = (pos_vec5 - pos_vec0)/(pt_data[i+5][0] - pt_data[i][0])        # term 1
        t2 = (pos_vec4 - pos_vec1_)/(pt_data[i+4][0] - pt_data[i-1][0])    # term 2
        t3 = (pos_vec3 - pos_vec2_)/(pt_data[i+3][0] - pt_data[i-2][0])    # term 3
        t4 = (pos_vec2 - pos_vec3_)/(pt_data[i+2][0] - pt_data[i-3][0])    # term 4
        t5 = (pos_vec1 - pos_vec4_)/(pt_data[i+1][0] - pt_data[i-4][0])    # term 5
        t6 = (pos_vec0 - pos_vec5_)/(pt_data[i][0] - pt_data[i-5][0])        # term 6

        vel_vec = 0.1*t1 + 0.15*t2 + 0.25*t3 + 0.25*t4 + 0.15*t5 + 0.1*t6     # calculate the velocity at the point
        vt_data[i][1] = vel_vec[0]
        vt_data[i][2] = vel_vec[1]
        vt_data[i][3] = vel_vec[2]

    vt_data = vt_data[5:-5,]      # remove first and last five elements as they won't be calculated

    return vt_data

# Read in polaris data from a text file and convert into pandas array, including transformations
def read_polaris_data(data_file, transformations = [], e_window = [], singles_only = False, write_transformed_data = False):
    '''
    Takes in an AllEventsCombined.txt file and reads the information into a
    pandas DataFrame. If the supplied transformations list is not empty, a
    coordinate transformation is applied with the given transformations array.
    If the supplied energy window has no arguments, no energy filtering is done,
    if it has two items, peak filtering is applied and if it has four items then
    Compton Edge filtering will also be applied.

    @params:
        data_file                   - Required : Path to a AllEventsCombined.txt file to read data from (Str)
        transformations             - Optional : Array of transformation arrays, used for coordinate transformations (Float[][], default = [])
        e_window                    - Optional : Energy windows to energy-filter the raw data (Int[], default = [])
        singles_only                - Optional : Whether or not to remove all multiple-interaction events (bool, default = False)
        write_transformed_data      - Optional : Write the final processed data to the same data directory (bool, default = False)

    @returns:
        allevents  - Pandas array of the AllEventsCombined.txt data
        log_str         - String of the log information for writing out to a log file
    '''
    log_str = ''        # string to hold information to write to log file

    print('Reading in data from', data_file + '.', '\nThis may take several minutes...')
    log_str += 'Reading in data from {}.\n\n'.format(data_file)

    allevents = pd.read_csv(data_file, sep = '	', header = None)                        # read in text file as a csv with tab separation
    print('Done.')
    allevents.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']       # separate into columns and name them

    #  convert detector, scatters, time columns from float (default) into ints
    allevents['scatters'] = allevents['scatters'].astype(int)
    allevents['detector'] = allevents['detector'].astype(int)
    allevents['time'] = allevents['time'].astype(int)

    print()
    print('{:30} {:d}'.format('Total number of events:', len(allevents.index)))
    print('{:30} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
    print('{:30} {:.0f}'.format('Event rate (events/sec):', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))
    print()

    log_str += '{:30} {:d}\n'.format('Total number of events:', len(allevents.index))
    log_str += '{:30} {:.1f}\n'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
    log_str += '{:30} {:.0f}\n'.format('Event rate (events/sec):\n', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) )

    if singles_only:
        print('Dropping all multiple interaction events.')
        log_str += '\nDropping all multiple interaction events.\n'
        allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)
        allevents.reset_index(drop = True, inplace = True)

    if transformations != []:
        print('\nApplying transformations to convert data into isocentric coordinate system...')
        print('-----------------------------------------------------------------------------')
        log_str += 'Applying transformations to convert data into isocentric coordinate system.\n'
        transformation_matrices = {}

        # Creating transformation matrices
        for i, arr in enumerate(transformations):
            transformation_matrices[i] = utils.get_transformation_matrix_array(arr)

        #  applying coordinate transformation to data
        allevents = utils.apply_transformation(allevents, transformation_matrices)

    if len(e_window) == 2:
         print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1]))
         log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV\n'.format(e_window[0], e_window[1])

         allevents.drop(allevents[ (allevents['energy'] < e_window[0]) |      # ...less than the lower photopeak limit...
                                             (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper photopeak limit.
         allevents.reset_index(drop = True, inplace = True)

    elif len(e_window) == 4:
        print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1], e_window[2], e_window[3]))
        log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV.\n'.format(e_window[0], e_window[1], e_window[2], e_window[3])

        allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                            ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                            (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
        allevents.reset_index(drop = True, inplace = True)
    print('Done.\n')

    if singles_only or transformations != [] or e_window != []:
        print()
        print('{:30} {:d}'.format('Total number of events:', len(allevents.index)))
        print('{:30} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
        print('{:30} {:.0f}'.format('Event rate (events/sec):', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

        log_str += '\n{:30} {:d}\n'.format('Total number of events:', len(allevents.index))
        log_str += '{:30} {:.1f}\n'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
        log_str += '{:30} {:.0f}\n\n'.format('Event rate (events/sec):', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) )

    log_str += '-------------------------------------------\n\n'

    # Write the AllEventsCombined data with coordinate transformations to AllEventsCombinedTransformed.txt
    if write_transformed_data:
        write_file = data_file[:-4]

        if transformations != []:
            write_file += 'Transformed'

        if singles_only:
            write_file += 'SinglesOnly'

        if len(e_window) == 2:
            write_file += 'Filtered_{}_{}'.format(e_window[0], e_window[1])

        if len(e_window) == 4:
            write_file += 'Filtered_{}_{}_{}_{}'.format(e_window[0], e_window[1], e_window[2], e_window[3])

        write_file += '.txt'
        print('Writing data to {}...'.format(write_file))

        allevents.to_csv(write_file, index = False, sep = '	', header = False, float_format = '%.2f')
        print('Done.\n')

    return allevents, log_str

# Process the all_events data and extract coincidences
def process_coincidences(data_dir, coin_window, transforms, e_window = [], singles_only = False, include_all_coincidences = False, determine_randoms = False, det_delays = [], count_doubles = False, count_deadtime_peak = False, allevents = pd.DataFrame(), log_str = ''):
    '''
    Takes in an AllEventsCombined.txt file, reads the information into a pandas
    array and then applies coincidence processing with the given energy and
    timing windows.

    @params:
        data_dir                    - Required : Path to combined detector data .txt file (Str)
        coin_window                 - Required : Time period after an event to look for coindicidences in nanoseconds (Int)
        transforms                  - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        e_window                    - Optional : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[], default = [])
        singles_only                - Optional : Whether to drop all non-single-interaction events or not (Bool, default = False)
        include_all_coincidences    - Optional : Managing multiple scatter events, this will add all possible coincidences found (Bool, default = False)
        determine_randoms           - Optional : Choose whether or not to determine the random coincidence rate (Bool, default = False)
        det_delays                  - Optional : Add a specific delay (in nanoseconds) to detectors to correct for any timing misaligment (Int[], default = [])
        apply_ce_filtering          - Optional : Whether or not to energy filter on the compton edge (Bool, default = True)
        count_doubles               - Optional : Whether or not to count double scatter events in the coincidence processing (Bool, default = False)
        count_deadtime_peak         - Optional : Whether or not to count coincidences for the deadtime delay peak (Bool, default = False)
        allevents                   - Optional : Pass the method the raw data to avoid re-reading it for multiple runs. (DataFrame, default = None)
        log_prefix                  - Optional : Optional prefix to add to log file string (Str, default = '')


    @returns:
        allevents    - pandas array of the raw data recorded by polaris
        coincidence_data  - pandas array of the coincidences found in the AllEventsCombined.txt data
    '''
    pd.set_option('display.max_rows', 40)
    pd.set_option('display.min_rows', 40)

    # Creating a log file to store terminal output to
    log_filename = '{}/coincidence_processing-{}ns'.format(data_dir, coin_window)

    if len(e_window) == 0:
        log_filename += '-no_e_filter'

    elif len(e_window) == 2:
        log_filename += '-{}_{}keV'.format(e_window[0], e_window[1])

        # Filter the events if allevents is not empty
        if not allevents.empty:
            print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1]))
            log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV\n'.format(e_window[0], e_window[1])

            allevents.drop(allevents[ (allevents['energy'] < e_window[0]) |      # ...less than the lower photopeak limit...
                                                (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper photopeak limit.

    elif len(e_window) == 4:
        log_filename += '-{}_{}_{}_{}keV'.format(e_window[0], e_window[1], e_window[2], e_window[3])

        # Filter the events if allevents is not empty
        if not allevents.empty:
            print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1], e_window[2], e_window[3]))
            log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV.\n'.format(e_window[0], e_window[1], e_window[2], e_window[3])

            allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                                ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                                (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.

    if include_all_coincidences:
        log_filename += '-allcoins.log'
    else:
        log_filename += '-singcoins.log'

    if allevents.empty:
        allevents, read_log = read_polaris_data(data_dir + 'AllEventsCombined.txt', transforms, e_window, singles_only)       # read in the raw polaris data
        log_str += read_log

    tot_raw_events = len(allevents.index)          # get the total number of events before coincidence processing

    print('\nProcessing coincidences')
    print('-----------------------')

    log_str += 'Processing coincidences\n-----------------------\n'

    allevents.reset_index(drop = True, inplace = True)                                        # reset the index numbering

    # Takes all the double scatter events, adds their energies up and saves both events with the total energy for processing
    # if count_doubles:
    #
    #     # print(allevents, '\n')
    #     # plot_energy_spectrum(data_dir, allevents, e_range = (0, 600))
    #
    #     allevents_numpy = allevents.to_numpy()
    #
    #     print('Adding up double scatter energies...')
    #     log_str += 'Adding up double scatter energies...\n'
    #
    #     allevents_numpy = utils.sum_double_scatter_energies(allevents_numpy)
    #
    #     # print('Converting back to DataFrame')
    #
    #     allevents = pd.DataFrame({'scatters': allevents_numpy[:, 0],
    #                                    'detector': allevents_numpy[:, 1],
    #                                    'energy': allevents_numpy[:, 2],
    #                                    'x': allevents_numpy[:, 3],
    #                                    'y': allevents_numpy[:, 4],
    #                                    'z': allevents_numpy[:, 5],
    #                                    'time': allevents_numpy[:, 6],})

    l = len(allevents.index)

    coin_window /= 10      #  convert coincidence window to 10s of nanoseconds

    # Find the coincidences in the energy filtered data
    # -------------------------------------------------
    print('Counting coincidences using to a timing window of {:.0f} nanoseconds...'.format(coin_window*10) )
    log_str += 'Counting coincidences using to a timing window of {:.0f} nanoseconds...\n'.format(coin_window*10)

    np.set_printoptions(edgeitems = 15, linewidth = 200)

    # Add timing correction
    if det_delays != []:
        print('Adding timing correction and sorting...')
        for i, delay in enumerate(det_delays):
            allevents.loc[allevents.detector == i+1, 'time'] += (delay/10)            # add time delay (in 10s of ns)

        allevents.sort_values(by = 'time', inplace = True)                                 # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering

    # Determine the random coincidence rate
    tot_rand_coins = 0
    if determine_randoms:

        print('Counting random coincidence rate...')

        allevents_ = allevents.copy()                                           # make copy for adding time delay
        for i in range(len(det_delays)):                                        # for each detector in the array
            allevents_.loc[allevents_.detector == (i+1), 'time'] += 6400*(i+1)  # add time delay to detector
        allevents_.sort_values(by = 'time', inplace = True)                     # sort the columns by time
        allevents_.reset_index(drop = True, inplace = True)                     # reset the indicies

        det_time_array_ = allevents_[['detector', 'time']].values                           # get the detector and time columns as a 2D numpy array
        # det_time_array_ = det_time_array_[det_time_array_[:,1].argsort(kind='stable')]      # sort by time

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((l,3))
        det_time_array[:,:-1] = det_time_array_

        # print('Running coincidence processing for delay of', time_delay/100, 'microseconds')
        tot_rand_coins = utils.count_coincidences3(det_time_array, coin_window, single_counting = True, choose_all = include_all_coincidences)

    # Process coincidences for no delay
    # ---------------------------------
    det_time_array_ = allevents[['detector', 'x', 'y', 'z', 'time']].values        # get the detector and time columns as a 2D numpy array

    # det_time_array_ = det_time_array_[det_time_array_[:,4].argsort(kind='stable')]      # sort by time

    # Add column of zeros for flagging coincidences
    det_time_array = np.zeros((l,6))
    det_time_array[:,:-1] = det_time_array_

    coincidence_data, tot_coins, tot_coins_per_event = utils.find_coincidences2(det_time_array, coin_window,
                                                                                single_counting = True,
                                                                                choose_random = False,
                                                                                choose_all = include_all_coincidences)

    if count_deadtime_peak:
        print('{:45} {:.0f}\n'.format('Coincidence rate of main peak (coins/sec):', len(coincidence_data.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

    # Process coincidences for deadtime-delayed peak
    # ----------------------------------------------
    # TODO: Add 3+ detector support
    if count_deadtime_peak:
        print('Counting coincidences with a delay of 1.5us on module 1...')
        allevents.loc[allevents.detector == 1, 'time'] += 150         # add time delay of 1.5us

        allevents.sort_values(by = 'time', inplace = True)                                 # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering

        det_time_array_ = allevents[['detector', 'x', 'y', 'z', 'time']].values        # get the detector and time columns as a 2D numpy array

        # det_time_array_ = det_time_array_[det_time_array_[:,4].argsort(kind='stable')]      # sort by time

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((l,6))
        det_time_array[:,:-1] = det_time_array_

        coincidence_data_, tot_coins_, tot_coins_per_event_ = utils.find_coincidences2(det_time_array, coin_window, single_counting = True, choose_random = False)

        print('{:45} {:.0f}\n'.format('Coincidence rate of deadtime peak (coins/sec):', len(coincidence_data_.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

        coincidence_data = coincidence_data.append(coincidence_data_, ignore_index = True)        # add additional coincidences into the coincidence dataframe
        coincidence_data.sort_values(by = 'time_1', inplace = True)                               # sort the columns by time
        coincidence_data.reset_index(drop = True, inplace = True)                                 # reset the index numbering

        tot_coins += tot_coins_         # Update the total number of coincidences

        tot_coins_per_event = (tot_coins_per_event + tot_coins_per_event_)/2        # Update the total number of coincidences per event

        print('Done.\n')

    print('{:45} {:d}'.format('Total number of coincidences:', len(coincidence_data.index)))
    print('{:45} {:.5f}'.format('Average number of coincidences per event:', tot_coins_per_event))
    print('{:45} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
    print('{:45} {:.0f}'.format('Total coincidence rate (coins/sec):', len(coincidence_data.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

    log_str += '{:45} {:d}\n'.format('Total number of coincidences:', len(coincidence_data.index))
    log_str += '{:45} {:.5f}\n'.format('Average number of coincidences per event:', tot_coins_per_event)
    log_str += '{:45} {:.1f}\n'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
    log_str += '{:45} {:.0f}\n'.format('Total coincidence rate (coins/sec):', len(coincidence_data.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) )

    if determine_randoms:
        print('{:45} {:d}'.format('Total number of random coincidences:', int(tot_rand_coins)))
        # print('{:45} {:.5f}'.format('Average number of random coincidences per event:', tot_rands_per_event))
        print('{:45} {:.0f}'.format('Total random coincidence rate (coins/sec):', tot_rand_coins / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

        log_str += '{:45} {:d}\n'.format('Total number of random coincidences:', int(tot_rand_coins))
        log_str += '{:45} {:.0f}\n'.format('Total random coincidence rate (coins/sec):', tot_rand_coins / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) )

    # Saving data to file for ctrack processing
    print('\nSaving data to file for PEPT processing...')

    # Convert to us for ctrack processing
    coincidence_data['time_1'] = (coincidence_data['time_1']/100.0)
    coincidence_data['time_2'] = (coincidence_data['time_2']/100.0)

    output_filename = data_dir + 'pept_coincidence_data'

    print('Saving log file to', log_filename)
    with open(log_filename, 'w') as f:
        f.write(log_str)

    print('Saving coincidence data to', output_filename)
    np.savetxt(output_filename, coincidence_data, delimiter=',', fmt='%.4f')

    print('Done.')

    # log_file.close()

    return allevents, coincidence_data

# Process the root file Monte Carlo data produced by Geant4 and return a coincidence dataframe
def process_root_coincidences(data_dir, e_window):
    '''
    Takes in a directory path to a series of root files, reads them in using the
    read_root_data function in the polarisMC_processing library with the
    'scatterData' format and counts councidences, returning a DataFrame with the
    resulting coincidences


    @params:
        data_dir                    - Required : Path to combined detector data .txt file (Str)
        e_window                    - Optional : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[], default = [])

    @returns:
        allcoins    - pandas array of the final coincidence data
    '''
    print('Reading in root file data')
    allevents, rfl = pmcp.read_root_data(data_dir, 'scatterData', 'scatterData')

    # pd.set_option('display.max_rows', 40)
    # pd.set_option('display.min_rows', 40)

    # print(allevents)

    # print('Plotting XY cross section for all modules...')
    # # XZ Crystal cross-section of all modules
    # # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
    # x = allevents['x']
    # y = allevents['y']
    # plt.xlabel('x-coordinate (mm)')
    # plt.ylabel('y-coordinate (mm)')
    # plt.plot(x, y, '.g')
    # # plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_all.png')
    # plt.show()
    # plt.cla(); plt.clf()
    #
    # print('Plotting XZ cross section for all modules...')
    # # XZ Crystal cross-section of all modules
    # # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
    # x = allevents['x']
    # y = allevents['z']
    # plt.xlabel('x-coordinate (mm)')
    # plt.ylabel('z-coordinate (mm)')
    # plt.plot(x, y, '.g')
    # # plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_all.png')
    # plt.show()
    # plt.cla(); plt.clf()
    #
    # print('Plotting YZ cross section for all modules...')
    # # XZ Crystal cross-section of all modules
    # # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
    # x = allevents['y']
    # y = allevents['z']
    # plt.xlabel('y-coordinate (mm)')
    # plt.ylabel('z-coordinate (mm)')
    # plt.plot(x, y, '.g')
    # # plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_all.png')
    # plt.show()
    # plt.cla(); plt.clf()
    #
    # return

    print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1]))
    allevents['energy'] = allevents['energy']*1000
    allevents.drop(allevents[ (allevents['energy'] < e_window[0]) |      # ...less than the lower photopeak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper photopeak limit.
    allevents.reset_index(drop = True, inplace = True)
    print('{:45}{:d}\n'.format('Total number of events after filtering:', len(allevents.index)))

    print('Dropping all multiple interaction events.')
    allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)
    allevents.reset_index(drop = True, inplace = True)
    print('{:45}{:d}\n'.format('Total number of single-interaction events:', len(allevents.index)))

    print('Counting coincidences...')
    all_coins_list = []
    num_coins = 0
    counted = {}
    for i in range(len(allevents.index)-5):

        for j in range(i, i+5):

            if (allevents.loc[j, 'event'] == allevents.loc[j+1, 'event']) and (allevents.loc[i, 'detector'] != allevents.loc[i+1, 'detector']) and (allevents.loc[j, 'event'] not in counted.values()):

                all_coins_list.append({'t1':allevents.loc[j,'event'], 'x1':allevents.loc[j,'x'], 'y1':allevents.loc[j,'y'], 'z1':allevents.loc[j,'z'],
                                       't2':allevents.loc[j+1,'event'], 'x2':allevents.loc[j+1,'x'], 'y2':allevents.loc[j+1,'y'], 'z2':allevents.loc[j+1,'z']})

                num_coins += 1

                # Add to dictionary for checking if counted
                counted[allevents.loc[j, 'event']] = allevents.loc[j, 'event']

    allcoins = pd.DataFrame(all_coins_list, columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2'])

    print('Total number of coincidences:', len(allcoins.index), '\n')

    output_filename = data_dir + 'pept_coincidence_data'

    print('Saving coincidence data to', output_filename)
    np.savetxt(output_filename, allcoins, delimiter=',', fmt='%.4f')

    print('Done.')

    return allcoins

# Produce some basic detector plots
def plot_basic(data_dir, transformations, coincidences_file, num_modules = -1, allevents = pd.DataFrame(), plot_singles = True, plot_coincidences = True, subset = False, subset_times = []):
    '''
    Takes in pandas dataframes of the raw polaris data and extracted coincidence
    data and produce the following plots:

        * Positions of recorded events within the detector crystals

    @params:
        data_dir            - Required : Path to detector data directory (Str)
        transformations     - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        coincidences_file   - Required : Name of file with coincidence polaris data (Str)
        num_modules         - Optional : Number of modules in apparatus if different to length of transformations list (Int, default = -1)
        plot_singles        - Optional : Plot all events within crystals (Bool, default = True)
        plot_coincidences   - Optional : Plot coincidences and LORs (Bool, default = True)
        subset              - Optional : Flag to plot a subset of the total data file (Bool, default = False)
        subset_times        - Optional : Tuple with the start and end times for the subset to plot in seconds (Float[2], default = [])

    @returns:
        null
    '''

    # Automatically get the number of detector modules if not specified
    if num_modules == -1:
        num_modules = len(transformations)

    coincidences = None

    if plot_singles:
        if allevents.empty:
            allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations)

    if subset:
        allevents = allevents[(allevents.time > subset_times[0]*1e8) & (allevents.time < subset_times[1]*1e8)]

    if plot_coincidences:
        coincidence_path = data_dir + coincidences_file
        print('Reading in coincidence data from', coincidence_path + '.', '\nThis may take several minutes...')
        coincidences = pd.read_csv(coincidence_path, sep = ',', header = None)       # read in text file as a csv with tab separation
        print('Done.')
        coincidences.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']       # separate into columns and name them

    if subset:
        subset_times[0] *= 1e6; subset_times[1] *= 1e6
        subset_times += coincidences.iloc[0][0]
        coincidences = coincidences[(coincidences.t1 > subset_times[0]) & (coincidences.t1 < subset_times[1])]

    if not os.path.exists(data_dir + 'basic_plots/'):
        os.makedirs(data_dir + 'basic_plots/')
        print ('Creating directory: {}'.format(data_dir + 'basic_plots/'))

    # # Plot the positions within the module of all recorded events
    # # -------------------------------------------------------------

    if plot_singles:

        print('\n', 'Plotting event data within modules')
        print('-------------------------------------', '\n')

        for module in range(num_modules):

            print('Plotting XY cross section for module:', str(module) + '...')
            # XY Crystal cross-section
            figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
            x = allevents[ allevents['detector'] == module ]['y']
            y = allevents[ allevents['detector'] == module ]['x']
            plt.xlabel('y-coordinate (mm)')
            plt.ylabel('x-coordinate (mm)')
            plt.plot(x, y, '.g')
            plt.savefig(data_dir + 'basic_plots/xy_crystal_cross-section_D{:d}.png'.format(module))
            # plt.show()
            plt.clf()

            print('Plotting XZ cross section for module:', str(module) + '...')
            # XZ Crystal cross-section
            # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
            x = allevents[ allevents['detector'] == module ]['z']
            y = allevents[ allevents['detector'] == module ]['x']
            plt.xlabel('z-coordinate (mm)')
            plt.ylabel('x-coordinate (mm)')
            plt.plot(x, y, '.g')
            plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_D{:d}.png'.format(module))
            # plt.show()
            plt.clf()

            print('Plotting YZ cross section for module:', str(module) + '...')
            # YZ Crystal cross-section of D0
            # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
            x = allevents[ allevents['detector'] == module ]['y']
            y = allevents[ allevents['detector'] == module ]['z']
            plt.xlabel('y-coordinate (mm)')
            plt.ylabel('z-coordinate (mm)')
            plt.plot(x, y, '.g')
            plt.savefig(data_dir + 'basic_plots/yz_crystal_cross-section_D{:d}.png'.format(module))
            # plt.show()
            plt.clf()

        print('Plotting XY cross section for all modules...')
        # XY Crystal cross-section of all modules
        figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
        x = allevents['x']
        y = allevents['y']
        plt.plot(x, y, '.g')
        plt.xlabel('x-coordinate (mm)')
        plt.ylabel('y-coordinate (mm)')
        plt.savefig(data_dir + 'basic_plots/xy_crystal_cross-section_all.png')
        # plt.show()
        plt.cla(); plt.clf()

        print('Plotting XZ cross section for all modules...')
        # XZ Crystal cross-section of all modules
        # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
        x = allevents['x']
        y = allevents['z']
        plt.xlabel('x-coordinate (mm)')
        plt.ylabel('z-coordinate (mm)')
        plt.plot(x, y, '.g')
        plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_all.png')
        # plt.show()
        plt.cla(); plt.clf()

        print('Plotting YZ cross section for all modules...')
        # XZ Crystal cross-section of all modules
        # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
        x = allevents['y']
        y = allevents['z']
        plt.xlabel('y-coordinate (mm)')
        plt.ylabel('z-coordinate (mm)')
        plt.plot(x, y, '.g')
        plt.savefig(data_dir + 'basic_plots/yz_crystal_cross-section_all.png')
        # plt.show()
        plt.cla(); plt.clf()

    # Plotting coincidence data
    # -------------------------

    if plot_coincidences:

        print('\n', 'Plotting coincidence data')
        print('-------------------------', '\n')

        # # Plot XY cross-section of first N coincidences
        # for i in range(200):
        #     x = [coincidences['x1'][i], coincidences['x2'][i]]
        #     y = [coincidences['y1'][i], coincidences['y2'][i]]
        #     plt.plot(x,y,'g')
        #
        # # Plot XZ cross-section of first N coincidences
        # for i in range(50):
        #     x = [coincidences['x1'][i], coincidences['x2'][i]]
        #     y = [coincidences['z1'][i], coincidences['z2'][i]]
        #     plt.plot(x,y,'g')

        p0_xPos = coincidences.iloc[:, 1]; p0_yPos = coincidences.iloc[:, 2]; p0_zPos = coincidences.iloc[:, 3]
        p1_xPos = coincidences.iloc[:, 5]; p1_yPos = coincidences.iloc[:, 6]; p1_zPos = coincidences.iloc[:, 7]
        # all_xPos = np.concatenate((p0_xPos, p1_xPos), axis = 0)
        # all_yPos = np.concatenate((p0_yPos, p1_yPos), axis = 0)
        # all_zPos = np.concatenate((p0_zPos, p1_zPos), axis = 0)
        #
        # # print(len(p0_xPos.index)); print(len(p1_xPos.index)); print(len(p0_yPos.index)); print(len(p1_yPos.index))
        # #
        # # Plot coincidences - points
        # num = len(p0_xPos)
        # # num = 5000
        #
        # print('Plotting XY positions of all coincidences...')
        # plt.cla(); plt.clf()
        # figure(num=None, figsize=(21, 16), dpi=600, facecolor='w', edgecolor='k')      # Set plot
        # plt.rcParams.update({'font.size': 30})
        # plt.plot(all_xPos, all_yPos, color='green', marker='o', linestyle='None', markersize=0.5)
        # plt.title('XY Position of all coincidences locations / Translated Coords, total: %s'%(len(p0_xPos)), fontsize=30)
        # plt.xlabel('X-Position (mm)', fontsize=30); plt.ylabel('Y-Position (mm)', fontsize=30)
        # plt.savefig(data_dir + 'basic_plots/xy-positions_N-' + str(num) + '.png')
        #
        # plt.cla(); plt.clf()
        figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
        plt.rcParams.update({'font.size': 30})

        # if num > 5000:
        num = 1000

        if num > len(p0_xPos):
            num = len(p0_xPos)

        print('Plotting XY coincidences of first', num, 'coincidences...')
        for i in range(0, num):
            # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
        # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
        plt.xlabel('X-Position (mm)', fontsize=30); plt.ylabel('Y-Position (mm)', fontsize=30)
        plt.savefig(data_dir + 'basic_plots/xy-coincidences_N-' + str(num) + '.png')
        plt.cla(); plt.clf()

        # figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
        print('Plotting XZ coincidences of first', num, 'coincidences...')
        for i in range(0, num):
            # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_zPos.iloc[i], p1_zPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
        # plt.title('XZ Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
        plt.xlabel('X-Position (mm)', fontsize=30); plt.ylabel('Z-Position (mm)', fontsize=30)
        plt.savefig(data_dir + 'basic_plots/xz-coincidences_N-' + str(num) + '.png')
        plt.cla(); plt.clf()

        # figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
        print('Plotting YZ coincidences of first', num, 'coincidences...')
        for i in range(0, num):
            # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
            plt.plot([p0_yPos.iloc[i], p1_yPos.iloc[i]], [p0_zPos.iloc[i], p1_zPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
        # plt.title('YZ Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
        plt.xlabel('Y-Position (mm)', fontsize=30); plt.ylabel('Z-Position (mm)', fontsize=30)
        plt.savefig(data_dir + 'basic_plots/yz-coincidences_N-' + str(num) + '.png')
        plt.cla(); plt.clf()

        # Plot XY cross-section of coincidences in first N milliseconds
        # figure(num=None, figsize=(21, 16), dpi=150, facecolor='w', edgecolor='k')      # Set plot size
        # start_time = coincidences['t1'][0]
        # i = 1
        # while (coincidences['t1'][i] - start_time) < 2e6:
        #     x = [coincidences['x1'][i-1], coincidences['x2'][i-1]]
        #     y = [coincidences['y1'][i-1], coincidences['y2'][i-1]]
        #     plt.plot(x,y,'g')
        #     i += 1

        # plt.savefig(data_dir + 'basic_plots/xz_crystal_cross-section_D1.png')
        # plt.show()

    return

# Read the .txt files produced by PolarisRate.c code written by Tom Leadbeater
def read_ratefiles(data_file):
    '''
    Takes in a rate5%.txt file produced by PolarisRate.c and produces a 2D NumPy
    array of the event rate as a function of time.

    Expected format of .txt file:
    [Time (ns*10), Total events, Total interactions, Events crystal 2, Events crystal 4, Total rate (Hz)]

    @params:
        data_file           - Required : Path to a rate5%.txt file to read data from (Str)

    @returns:
        rates_data - NumPy array of event rate as a function of time
    '''

    rate_pd = pd.read_csv(data_file, sep = '	', header = None,
                          names = ['time', 'tot_events', 'tot_interactions', 'events_2', 'events_4', 'rate'],
                          usecols = ['time', 'rate'], dtype = {'time': np.uint64, 'rate': np.float64},
                          skiprows = 5)

    return rate_pd.to_numpy(dtype = np.uint64)

# Read the .txt files produced by the rate_process_allevents function
def read_ratefiles2(data_file):
    '''
    Takes in a rates.txt file produced by the rate_process_allevents function
    and produces a 2D NumPy array of the event rate as a function of time.

    Expected format of .txt file:
    [Time (s), Total rate (Hz)]

    @params:
        data_file   - Required : Path to a rates.txt file to read data from (Str)

    @returns:
        rates_data - NumPy array of event rate as a function of time
    '''

    rate_pd = pd.read_csv(data_file, sep = ' ', header = None,
                          names = ['time', 'rate'],
                          dtype = {'time': np.uint64, 'rate': np.uint64})

    # print(rate_pd.to_numpy(dtype = np.uint64))
    return rate_pd.to_numpy(dtype = np.uint64)

# Plot deadtime curves as a function of activity
def plot_deadtime(data_dirs, filenames, save_dir, read_func, a0, t_half, tmp = False):
    '''
    Reads in a given list of AllEventsCombined.txt files and plots the recorded
    activity as a function of the source activity in order to demonstrate
    deadtime behaviour.

    @params:
        data_dirs           - Required : List of paths to detector data directories. Assumes files are given in temporal order (Str[])
        filenames           - Required : Names of the files at those directories to extract data from (Str[])
        save_dir            - Required : Directory to save the plotting txt files and images to (Str)
        read_func           - Required : Boolean array of which function to use to read the rate file (Bool[])
        a0                  - Required : Initial activity of source (Float)
        t_half              - Required : Halflife of source in minutes (Float)

    @returns:
        null
    '''

    assert len(data_dirs) == len(filenames), 'Number of directories doesn\'t equal number of file names.'

    rates = np.empty([0,2], dtype = np.uint64)

    for i, dir in enumerate(data_dirs):
        # Read in the next file and extract rates
        if read_func[i] == 1:
            new_rates = read_ratefiles(dir + filenames[i])
            new_rates[:,1] += np.uint64(5222)
        elif read_func[i] == 0:
            new_rates = read_ratefiles2(dir + filenames[i])
            new_rates[:,0] *= 100000000
            new_rates[:,1] = np.uint64(new_rates[:,1] * 1.21390193)
            # new_rates[:,1] -= 2400
            # new_rates[:,1] += np.uint64(2500)
        else:
            new_rates = read_ratefiles(dir + filenames[i])
            # if dir[72] == '1':
                # new_rates[:,0] -= np.uint64(370e8)
            # new_rates[:,1] -= np.uint64(5372)

        if i != 0:                 # if the array isn't empty
            new_rates[:,0] += rates[-1,0]       # adjust all the new times to continue from the last time of the initial file

        rates = np.append(rates, new_rates, axis = 0)      # concatenate new array to old one
        # print(rates)
        # print('---------------------------------------------', '\n')

    def A(t, A0, th):
        return A0*2**(-t/th)

    acts = A(rates[:,0]/(1e8*60), a0, t_half)

    expected = np.linspace(0, a0, 100)

    # Plot the final results
    # ======================
    # plt.rcParams.update({'font.size': 20})
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size

    # plt.plot(rates[:,0]/(1e8*3600), rates[:,1], '.')
    # np.savetxt(save_dir + 'activity_versus_rate.txt', np.c_[acts, rates[:,1]], fmt = '%.4f', delimiter = ',')
    scale_factor = 900
    if tmp:
        plt.plot(acts, rates[:,1], '.', label = 'Measured Rate', color = 'b')
        plt.plot(expected, scale_factor * expected, label = 'Expected Rate', color = 'r')
    else:
        plt.plot(acts, rates[:,1], '.', color = 'b')
        plt.plot(expected, scale_factor * expected, color = 'r')
    # np.savetxt(save_dir + 'expected_rate.txt', np.c_[expected, scale_factor * expected], fmt = '%.4f', delimiter = ',')
    # plt.xlabel('Time (Hours)')
    # plt.xlim(0, 105)
    plt.xlabel(r'Activity ($\mu$Cu)')
    plt.ylabel('Event Rate (Hz)')
    # plt.legend()
    # plt.savefig(save_dir+ 'activity_versus_rate.png')
    # plt.show()

    return

# Plot detector count rate as a function of distance for H3D dataset
def plot_deadtime_h3d(data_dirs, read_txt_file = False, txt_path = ''):
    '''
    Reads in a given list of data directories holding the individual runs taken
    at H3D of the NIST system, reads in the AllEventsCombined.txt file, extracts
    the event rate and plots it as a function of distance as given in the
    directory name.

    @params:
        data_dirs       - Required : List of root directories to all the individual runs (Str[])
        read_txt_file   - Optional : Read the rates from a txt file instead of processing them (Bool, default = False)
        txt_path        - Optional : Path to the txt file if above is True (Str)

    @returns:
        null
    '''

    # Empty lists to hold activity and event rates
    distances = []
    det_rates = []

    if not read_txt_file:

        all_data_dirs = []      # empty list holding all the directories of each run

        # Populte the all_data_dirs list
        for data_dir in data_dirs:
            for subdir, dirs, files in os.walk(data_dir):
                for dir in dirs:
                    all_data_dirs.append(data_dir + dir + '/')
                break

        # Loop through each run and extract the distance and rate data
        for dir in all_data_dirs:

            # Ignore corrupted dataset
            if '191111-run6' in dir:
                continue

            # First get the distance from the directory name
            dist = float(re.findall(r'(\d+.\d+)mm', dir)[0])
            distances.append(dist)

            # Next get the event rate from the AllEventsCombined.txt file
            data_file = dir + 'AllEventsCombined.txt'

            print('Reading in data from', data_file + '.', '\nThis may take several minutes...')
            allevents = pd.read_csv(data_file, sep = '	', header = None)                        # read in text file as a csv with tab separation
            print('Done.')
            allevents.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']       # separate into columns and name them

            event_rate = len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)

            print()
            print('{:30} {:d}'.format('Total number of events:', len(allevents.index)))
            print('{:30} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
            print('{:30} {:.0f}'.format('Event rate (events/sec):', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

            det_rates.append(event_rate)

    else:
        rate_data = np.loadtxt(txt_path, delimiter = ',')
        distances = rate_data[:,0]
        det_rates = rate_data[:,1]

    distances = np.array(distances)
    det_rates = np.array(det_rates)

    distances[distances < 70] += 400

    if not read_txt_file:
        # Write the processed data to file
        np.savetxt(data_dirs[-1] + 'distance_versus_rate.txt', np.c_[distances, det_rates], fmt = '%.4f', delimiter = ',')

    plt.plot(distances, det_rates, '.')
    plt.xlabel('Detector Separation (mm)')
    plt.ylabel('Total event rate (Hz)')
    plt.show()

    return

# Plot timing window curve
def timing_resolution(data_file, transformations, e_window, time_delays, coin_window):
    '''
    Reads the raw polaris data and produces a plot of coincidence rate as a
    function of timing delay between detector heads.

    @params:
        data_file           - Required : Path to a AllEventsCombined.txt file to read data from (Str)
        transformations     - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        time_delays         - Required : Numpy array of delays (in microseconds) to run coindicidence processing for (Numpy Int[])
        coin_window         - Required : Time period after an event to look for coindicidences in microseconds (Int)

    @returns:
        null
    '''

    allevents, log_str = read_polaris_data(data_file, transformations)       # get the transformed data
    l = len(allevents.index)    # get the length of the array

    # Apply energy window filtering
    print('\nProcessing coincidences')
    print('-----------------------')

    print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
          str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

    # Drop all events that are...
    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    print('Total number of events after energy filter:', len(allevents.index), '(' + str(round(len(allevents.index)/l*100, 1)) + '%)')
    print()

    l = len(allevents.index)    # get the length of the array

    # Time windowing variables
    # time_delay = 100              # time delay between detectors in microseconds - positive is forward, negative is backwards
    # coin_window = 100         # coincidence window in microseconds

    # Convert to time units of 10s of nanoseconds
    time_delays = time_delays * 100
    # time_delay *= 100
    coin_window *= 100

    tot_coins = []          # empty list to store tot_coincidences for each time delay

    # np.set_printoptions(edgeitems = 15)
    # time_delay = 1000 * 100
    # allevents_ = allevents.copy()                                                       # make copy for adding time delay
    # print(allevents_, '\n')
    # allevents_.loc[allevents_.detector == 1, 'time'] += time_delay                      # add time delay to detector 1
    # print(allevents_, '\n')
    # det_time_array_ = allevents_[['detector', 'time']].values                           # get the detector and time columns as a 2D numpy array
    # print(det_time_array_, '\n')
    # det_time_array_ = det_time_array_[det_time_array_[:,1].argsort(kind='stable')]      # sort by time
    # print(det_time_array_, '\n')
    #
    # # Add column of zeros for flagging coincidences
    # det_time_array = np.zeros((l,3))
    # det_time_array[:,:-1] = det_time_array_
    # print(det_time_array, '\n')

    # return

    lng = len(time_delays)
    print('Determining coincidence rates for', lng, 'time delays between', time_delays[0]/100, 'and', time_delays[-1]/100, 'microseconds:')
    # tot_coins.append(utils.count_coincidences(det_time_array, coin_window, time_delay))

    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
    for i, time_delay in enumerate(time_delays):
        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

        allevents_ = allevents.copy()                                                       # make copy for adding time delay
        allevents_.loc[allevents_.detector == 1, 'time'] += time_delay                      # add time delay to detector 1
        det_time_array_ = allevents_[['detector', 'time']].values                           # get the detector and time columns as a 2D numpy array
        det_time_array_ = det_time_array_[det_time_array_[:,1].argsort(kind='stable')]      # sort by time

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((l,3))
        det_time_array[:,:-1] = det_time_array_

        # print('Running coincidence processing for delay of', time_delay/100, 'microseconds')
        tot_coins.append(utils.count_coincidences3(det_time_array, coin_window))

        # tot_coins.append(utils.count_coincidences(det_time_array, coin_window, time_delay))

    print()
    print(tot_coins)

    tot_coins = np.array(tot_coins)
    tot_coins = tot_coins/58        # change to coincidence rate
    time_delays = time_delays/100   # change to microseconds

    def gaus(x, A, mu, sigma, B):
        return A*np.exp(-(x-mu)**2/(2*sigma**2)) + B

    popt, pcov = curve_fit(gaus, time_delays, tot_coins, [max(tot_coins), np.mean(time_delays), np.std(time_delays), min(tot_coins)])
    perr = np.sqrt(np.diag(pcov))

    print('Fitted Parameters for Normal Distribution')
    print('-----------------------------------------')
    print('{:10}{:.2f}{:^4}{:.2f}'.format('A', popt[0], '+-', perr[0]))
    print('{:10}{:.2f}{:^4}{:.2f}'.format('mu', popt[1], '+-', perr[1]))
    print('{:10}{:.2f}{:^4}{:.2f}'.format('sigma', popt[2], '+-', perr[2]))
    print('{:10}{:.2f}{:^4}{:.2f}'.format('B', popt[3], '+-', perr[3]))


    x_fit = np.linspace(min(time_delays), max(time_delays), 200)
    y_fit = gaus(x_fit, *popt)

    plt.plot(time_delays, tot_coins, '.')
    plt.plot(x_fit, y_fit)
    plt.show()

    return

# Calculate and draw the timing resolution curve
def timing_resolution2(data_dir, transformations, e_window, delays, read_txt = False, window = 200, num_detectors = -1, perform_fit = False, shade_window = []):
    '''
    Takes in a directory to an AllEventsCombined.txt file, extracts the data and
    runs coincidence processing on the events for a series of windows. Then
    historgrams the coincidence rate as a function of delay time. Assumes that
    detector 0 is the master detector for timing purposes. Runs a resolution
    plot for each detector in the array.

    @params:
        data_dir            - Required : Path to AllEventsCombined.txt file (Str)
        transformations     - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        delays              - Required : Array of delays to use for the resolution plotting in nanoseconds (Int[])
        read_txt            - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)
        window              - Optional : Timing window to look for coincidences in nanoseconds (Int, default = 200)
        delay_det           - Optional : Number of detectors in the array (Int, default = 2)
        perform_fit         - Optional : Whether or not to do a Gaussian fit on the timing resolution curve (Bool, default = False)
        shade_window        - Optional : Coincidence window to shade. If empty no window is shaded (Int[2], default = [])

    @returns:
        null
    '''
    figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 20})

    # Automatically get the number of detector modules if not specified
    if num_detectors == -1:
        num_detectors = len(transformations)

    # Create plotting arrays
    rates = []

    if not read_txt:
        allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations = [], e_window = e_window)      # read data and perform coordinate transformations
        tot_raw_events = len(allevents.index)

        if tot_raw_events > 1E6:      # shorten the dataset for faster processing
            allevents = allevents.iloc[:500000]
            tot_raw_events = len(allevents.index)    # get the length of the filtered array

        total_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time'])*1e-8     # get the total time in units of 1e-8 seconds

        lng = len(delays)

        window = window/10      # timing window to look for coincidences in 10s of nanoseconds

        # Run timing resolution for each detector in the array
        for delay_det in range(1, num_detectors):
            print('Running timing resolution between detector 0 and detector', delay_det)

            rates = []      # array of rates

            printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            for i, delay_ns in enumerate(delays):

                delay = delay_ns/10     # convert nanoseconds into 10s of nanoseconds
                # print('Copying all events dataframe and addition time delay for randoms counting...')
                allevents_delay = allevents.copy()                                   # make copy for adding time delay
                # allevents_delay.loc[allevents_delay.detector == 1, 'time'] += -6.2  # add time delay to detector 1
                # allevents_delay.loc[allevents_delay.detector == 2, 'time'] += -1.1  # add time delay to detector 2
                # allevents_delay.loc[allevents_delay.detector == 2, 'time'] += 2.1  # add time delay to detector 3
                allevents_delay.loc[allevents_delay.detector == delay_det, 'time'] += delay  # add time delay to detector
                allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
                # print('Done.', '\n')

                det_time_array_delay_ = allevents_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

                # Add column of zeros for flagging coincidences
                det_time_array_delay = np.zeros((len(det_time_array_delay_),3))
                det_time_array_delay[:,:-1] = det_time_array_delay_

                tot_coins = utils.count_coincidences3(det_time_array_delay, window)

                rates.append(tot_coins/total_time)

                printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # Plotting the normal distribution
            # --------------------------------

            print('\n')

            if perform_fit:

                print('Performing a normal distribution fit...')

                def gaus(x, A, mu, sigma, B):
                    return A*np.exp(-(x-mu)**2/(2*sigma**2)) + np.abs(B)

                popt, pcov = curve_fit(gaus, delays, rates, [max(rates), np.mean(delays)-1, np.std(delays), min(rates)])
                perr = np.sqrt(np.diag(pcov))

                fit_results =  'Fitted Parameters for Normal Distribution\n'
                fit_results += '-----------------------------------------\n'
                fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('A', popt[0], '+-', perr[0])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('mu', popt[1], '+-', perr[1])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('sigma', popt[2], '+-', perr[2])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('B', popt[3], '+-', perr[3])

                with open(data_dir + 'timing_resolution_fit_results-detector_{:d}.log'.format(delay_det), 'w') as f:
                    f.write(fit_results)

                # print('Fitted Parameters for Normal Distribution')
                # print('-----------------------------------------')
                # print('{:10}{:.2f}{:^4}{:.2f}'.format('A', popt[0], '+-', perr[0]))
                # print('{:10}{:.2f}{:^4}{:.2f}'.format('mu', popt[1], '+-', perr[1]))
                # print('{:10}{:.2f}{:^4}{:.2f}'.format('sigma', popt[2], '+-', perr[2]))
                # print('{:10}{:.2f}{:^4}{:.2f}'.format('B', popt[3], '+-', perr[3]))

                print(fit_results)

                x_fit = np.linspace(min(delays), max(delays), 200)
                y_fit = gaus(x_fit, *popt)
                plt.plot(x_fit, y_fit, label = r'A = {:.2f} $\pm$ {:.2f} Hz'.format(popt[0], perr[0]) + '\n' +
                                               r'$\mu$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[1], perr[1]) + '\n' +
                                               r'$\sigma$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[2], perr[2]) + '\n' +
                                               r'B = {:.2f} $\pm$ {:.2f} Hz'.format(popt[3], perr[3]))
                plt.legend()

            if not read_txt:
                # Write data to write directory
                write_dir = data_dir + 'plot_data/'
                if not os.path.exists(write_dir):
                    os.makedirs(write_dir)
                    print ('Created directory: {}'.format(write_dir))

                write_filename = '{}timing_resolution-range_{:.0f}_{:.0f}_{:.0f}-window_{:.0f}-detector_{:d}.txt'.format(write_dir, delays[0], delays[-1], len(delays), window*10, delay_det)
                np.savetxt(write_filename, np.c_[delays, rates], fmt = '%.4f', delimiter = ',')

            # Plot the final results
            plt.xlabel('Time Delay (ns)')
            plt.ylabel('Coincidence Rate (Hz)')

            plt.plot(delays, rates, '.', markersize = 20)

            if shade_window != []:
                start_e = shade_window[0]; end_e = shade_window[1]
                max_e = max(rates)

                colour = shade_window[2]

                # Plot vertical lines indicating the sca windows
                plt.plot([start_e, start_e], [0, max_e], '--', color = colour)
                plt.plot([end_e, end_e], [0, max_e], '--', color = colour)

                # Shade the area between the lines
                plt.fill_between([start_e, end_e], [0, 0], [max_e, max_e],
                                  facecolor = colour,                 # fill colour
                                  color = colour,                     # outline colour
                                  alpha = 0.2)                              # transparency

            plot_filename = '{}timing_resolution-detector_{:d}.png'.format(data_dir, delay_det)
            print('\nSaving plot to', plot_filename, '\n\n')
            plt.savefig(plot_filename)
            plt.clf(); plt.cla()
            # plt.show()

    else:
        # Run timing resolution for each detector in the array
        for delay_det in range(1, num_detectors):

            read_file = '{}plot_data/timing_resolution-range_{:.0f}_{:.0f}_{:.0f}-window_{:.0f}-detector_{:d}.txt'.format(data_dir, delays[0], delays[-1], len(delays), window, delay_det)

            plot_data = np.loadtxt(read_file, dtype = np.float64, delimiter = ',')
            delays = plot_data[:,0]
            rates = plot_data[:,1]

            # Plotting the normal distribution
            # --------------------------------

            print('\n')

            if perform_fit:

                print('Performing a normal distribution fit...')

                def gaus(x, A, mu, sigma, B):
                    return A*np.exp(-(x-mu)**2/(2*sigma**2)) + np.abs(B)

                popt, pcov = curve_fit(gaus, delays, rates, [max(rates), np.mean(delays)-1, np.std(delays), min(rates)])
                perr = np.sqrt(np.diag(pcov))

                fit_results =  'Fitted Parameters for Normal Distribution\n'
                fit_results += '-----------------------------------------\n'
                fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('A', popt[0], '+-', perr[0])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('mu', popt[1], '+-', perr[1])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('sigma', popt[2], '+-', perr[2])
                fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('B', popt[3], '+-', perr[3])

                with open(data_dir + 'timing_resolution_fit_results-detector_{:d}.log'.format(delay_det), 'w') as f:
                    f.write(fit_results)

                print(fit_results)

                x_fit = np.linspace(min(delays), max(delays), 200)
                y_fit = gaus(x_fit, *popt)
                plt.plot(x_fit, y_fit, label = r'A = {:.2f} $\pm$ {:.2f} Hz'.format(popt[0], perr[0]) + '\n' +
                                               r'$\mu$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[1], perr[1]) + '\n' +
                                               r'$\sigma$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[2], perr[2]) + '\n' +
                                               r'B = {:.2f} $\pm$ {:.2f} Hz'.format(abs(popt[3]), perr[3]))
                plt.legend()

            # Plot the final results
            plt.xlabel('Time Delay (ns)')
            plt.ylabel('Coincidence Rate (Hz)')

            plt.plot(delays, rates, '.', markersize = 20)

            if shade_window != []:
                start_e = shade_window[0]; end_e = shade_window[1]
                max_e = max(rates*1.125)

                colour = shade_window[2]

                # Plot vertical lines indicating the sca windows
                plt.plot([start_e, start_e], [0, max_e], '--', color = colour)
                plt.plot([end_e, end_e], [0, max_e], '--', color = colour)

                # Shade the area between the lines
                plt.fill_between([start_e, end_e], [0, 0], [max_e, max_e],
                                  facecolor = colour,                 # fill colour
                                  color = colour,                     # outline colour
                                  alpha = 0.2)                              # transparency

            plot_filename = '{}timing_resolution-detector_{:d}.png'.format(data_dir, delay_det)
            print('\nSaving plot to', plot_filename)
            plt.savefig(plot_filename)
            plt.clf(); plt.cla()

    return

# Plots the coincidence rate, trues rate and randoms rate as a function of activity
def count_coincidence_rates(data_dir, time_window, transformations, e_window, coin_window, a0, t_half, plot_results = False, add_delay = 0, count_doubles = False):
    '''
    Takes in an AllEventsCombined.txt file, reads in the data, and counts the
    total coincidences for a given time frame, as well as the randoms. It then
    subtracts the two curves to produce a trues curve. Finally, plots all three
    curves on the same figure as a function of source activity.

    @params:
        data_dir            - Required : Path to a AllEventsCombined.txt file to read data from (Str)
        time_window         - Required : Size of time window in which to perform coincidence counting in seconds (Int)
        transformations     - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        coin_window         - Required : Time period after an event to look for coincidences in nanoseconds (Int)
        a0                  - Required : Initial activity of source (Float)
        t_half              - Required : Halflife of source in minutes (Float)
        plot_results        - Optional : Whether to plot the final rate curves or not (Bool, default = False)
        add_delay           - Optional : Add a specific delay (in nanoseconds) to detector 2 to correct for any timing misaligment (Int, default = 0)
        count_doubles       - Optional : Whether or not to count double scatter events in the coincidence processing (Bool, default = False)

    @returns:
        null
    '''

    coin_window /= 10       # converting time to 10s of nanoseconds

    data_file = data_dir + 'AllEventsCombined.txt'
    allevents, log_str = read_polaris_data(data_file, transformations)       # get the transformed data
    l = len(allevents.index)    # get the length of the array

    if not count_doubles:

        # Apply energy window filtering
        print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
              str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

        # Drop all events that are...
        allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                            ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                            (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
        print('Total number of events after energy filter:', len(allevents.index), '(' + str(round(len(allevents.index)/l*100, 1)) + '%)')
        print()

    else:
        dblevents = allevents[allevents.scatters == 2]      # first get the double scatter events
        # print(dblevents)
        dbleventsnxt = dblevents.shift(-1)          # get a copy of the double scatters shifted up by 1
        # print(dbleventsnxt)
        dblevents.energy += dbleventsnxt.energy     # add the energies together
        # print(dblevents)
        dblevents = dblevents.reset_index(drop = True)          # reset the index numbering
        # print(dblevents)
        dblevents = dblevents[(dblevents.index % 2) == 0]   # drop the even numbered events
        # print(dblevents)

        allevents = pd.concat([allevents, dblevents])           # add these new scatters back to the allevents dataframe
        allevents.sort_values('time', inplace = True)           # sort by event time
        allevents = allevents.reset_index(drop = True)          # reset the index numbering

        # Drop all events that are...
        allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                            ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                            (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
        print('Total number of events after energy filter:', len(allevents.index), '(' + str(round(len(allevents.index)/l*100, 1)) + '%)')
        print()

    l = len(allevents.index)    # get the length of the array

    if add_delay != 0:
        print('Adding timing correction and sorting...')
        allevents.loc[allevents.detector == 1, 'time'] += (add_delay/10)             # add time delay (in 10s of ns) to detector 1
        allevents.sort_values(by = 'time', inplace = True)                           # sort the columns by time
        print('Done.\n')

    total_time = allevents.iloc[-1]['time'] - allevents.iloc[0]['time']     # get the total time in units of 1e-8 seconds
    num_sub_dfs = int((total_time/1e8)/time_window)            # calculate the number of sub-dataframes based on supplied time window

    print('Copying all events dataframe and addition time delay for randoms counting...')
    allevents_delay = allevents.copy()                                   # make copy for adding time delay
    allevents_delay.loc[allevents_delay.detector == 1, 'time'] += 5e7    # add time delay to detector 1 of half a second
    allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
    print('Done.', '\n')

    print('Splitting up large dataframes into sub-dataframes...')
    sub_allevents = np.array_split(allevents, num_sub_dfs)                    # split the dataframe into smaller dataframes
    sub_allevents_delay = np.array_split(allevents_delay, num_sub_dfs)        # split the dataframe into smaller dataframes
    print('Done.', '\n')

    # Create empty arrays to hold the number of coincidence and randoms rates for each sub-dataframe
    coin_rates = np.zeros(num_sub_dfs)
    rand_rates = np.zeros(num_sub_dfs)
    true_rates = np.zeros(num_sub_dfs)

    print('Counting total coincidence rate and random coincidence rate:')
    printProgressBar(0, num_sub_dfs, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i in range(num_sub_dfs):        # loop through each sub dataframe

        # Get the total time period of the sub-dataframe in seconds
        sub_df_time = ((sub_allevents[i].iloc[-1]['time'] - sub_allevents[i].iloc[0]['time']) / 1e8)

        # First count the total number of coincidences
        # --------------------------------------------
        det_time_array_ = sub_allevents[i][['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        coin_rates[i] = utils.count_coincidences3(det_time_array, coin_window) / sub_df_time

        # Then count the total number of randoms
        # --------------------------------------
        det_time_array_ = sub_allevents_delay[i][['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        rand_rates[i] = utils.count_coincidences3(det_time_array, coin_window) / sub_df_time

        # Finally store the number of true coincidences
        true_rates[i] = coin_rates[i] - rand_rates[i]

        printProgressBar(i+1, num_sub_dfs, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    print('Done.\n')

    # Saving the results
    # ==================

    # print(coin_rates)
    # print(rand_rates)
    # print(true_rates)

    # Calculate the activity for plotting
    def A(t, A0, th):
        return A0*2**(-t/th)

    time_interval_min = time_window/60                             # convert time interval into minutes
    fit_times = np.linspace(time_interval_min/2, (total_time/1e8)/60 - (time_interval_min/2), num_sub_dfs)     # create an array of times for calculating activity
    # print(fit_times)
    activity = A(fit_times, a0, t_half)                              # create activity array for x-axis values
    # print(activity)
    # activity = np.linspace(len(coin_rates), 0, len(coin_rates))

    # Write data to write directory
    if not os.path.exists(data_dir + 'rate_files/'):
        os.makedirs(data_dir + 'rate_files/')
        print ('Creating directory: {}'.format(data_dir + 'rate_files/'))

    print('Saving results to .txt...')
    if count_doubles:
        np.savetxt(data_dir + 'rate_files/tot_rate_' + str(int(coin_window*10)) + 'ns_doubles.txt', np.c_[activity, coin_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt(data_dir + 'rate_files/rand_rate_' + str(int(coin_window*10)) + 'ns_doubles.txt', np.c_[activity, rand_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt(data_dir + 'rate_files/true_rate_' + str(int(coin_window*10)) + 'ns_doubles.txt', np.c_[activity, true_rates], fmt = '%.4f', delimiter = ',')
    else:
        np.savetxt(data_dir + 'rate_files/tot_rate_' + str(int(coin_window*10)) + 'ns_no-doubles.txt', np.c_[activity, coin_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt(data_dir + 'rate_files/rand_rate_' + str(int(coin_window*10)) + 'ns_no-doubles.txt', np.c_[activity, rand_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt(data_dir + 'rate_files/true_rate_' + str(int(coin_window*10)) + 'ns_no-doubles.txt', np.c_[activity, true_rates], fmt = '%.4f', delimiter = ',')
    print('Done.\n')

    if plot_results:
        print('Plotting results...')
        # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size

        plt.plot(activity, coin_rates, '.', label = 'Total coincidence rate')
        plt.plot(activity, rand_rates, '.', label = 'Random coincidence rate')
        plt.plot(activity, true_rates, '.', label = 'True coincidence rate')
        plt.legend()
        # plt.savefig(data_dir + 'coincidence_rates.png')
        plt.show()

    print('Done.\n')

    return

# Plots the coincidence rate, trues rate and randoms rate as a function of activity from processed rate files
def plot_coincidence_curves2(data_dirs, save_dir, x_data):
    '''
    Takes in a directory path to rate files, reads in the data, and plots all
    three curves on the same figure as a function of sourve activity. Assumes
    the list of data_dirs are in order from highest activity to lowest.

    @params:
        data_dirs           - Required : List of paths to a tot_rate.txt, rand_rate.txt and true_rate.txt files to read data from (Str[])
        save_dir            - Required : Directory in which to save the final plots and txt files (Str)
        x_data              - Required : What data should be plotted on the x-axis. Options are: 'activity', 'singles' and 'totals' (Str)

    @returns:
        null
    '''

    # Create empty arrays to hold the number of coincidence and randoms rates for each sub-dataframe
    coin_rates = []
    rand_rates = []
    true_rates = []
    sing_rates = []
    activities = []

    print('Reading in rate files...')

    for i, dir in enumerate(data_dirs):        # loop through each sub dataframe

        coin_rate = np.genfromtxt(dir + '/rate_files/tot_rate_600ns_doubles.txt', delimiter = ',')
        rand_rate = np.genfromtxt(dir + '/rate_files/rand_rate_600ns_doubles.txt', delimiter = ',')
        true_rate = np.genfromtxt(dir + '/rate_files/true_rate_600ns_doubles.txt', delimiter = ',')
        sing_rate = np.genfromtxt(dir + '/rate_files/singles-120s-51.txt', delimiter = ',')

        # Drop the second column
        activity = coin_rate[:,0]

        # Drop the first column
        coin_rate = coin_rate[:,1]
        rand_rate = rand_rate[:,1]
        true_rate = true_rate[:,1]
        sing_rate = sing_rate[:,1]        # drop the last data point as it's incomplete

        if len(sing_rate) > len(coin_rate):
            sing_rate = sing_rate[:len(coin_rate)-len(sing_rate)]      # drop the last datapoint as it represents incomplete data

        if int(re.search(r'run(\d+)', dir).group(1)) > 16:
            coin_rate *= 0.9
            true_rate *= 0.8
        #
        # Prevent overlap of data
        if 'run19' in dir:
            coin_rate = coin_rate[:3]
            rand_rate = rand_rate[:3]
            true_rate = true_rate[:3]
            sing_rate = sing_rate[:3]
            activity = activity[:3]

        # Cutting off higher activity data
        if 'run16' in dir:
            coin_rate = coin_rate[-2:]
            rand_rate = rand_rate[-2:]
            true_rate = true_rate[-2:]
            sing_rate = sing_rate[-2:]
            activity = activity[-2:]

        # if '190611' in dir:
        #     activity += 2.0
        #     sing_rate += 4000
        #     coin_rate *= 1.37
        #     rand_rate *= 1.92
        #     true_rate *= 1.20

        # Append only the rates columns
        coin_rates = np.concatenate((coin_rates, coin_rate))
        rand_rates = np.concatenate((rand_rates, rand_rate))
        true_rates = np.concatenate((true_rates, true_rate))
        sing_rates = np.concatenate((sing_rates, sing_rate))
        activities = np.concatenate((activities, activity))

    print('Done', '\n')

    coin_rates = np.array(coin_rates)
    rand_rates = np.array(rand_rates)
    true_rates = np.array(true_rates)
    sing_rates = np.array(sing_rates)
    activities = np.array(activities)

    # Change the units of activity from uCi to MBq
    activities = activities * 3.7e-2

    # Sort all datapoints by the activity
    all_data = np.transpose([activities, coin_rates, rand_rates, true_rates, sing_rates])
    all_data = all_data[all_data[:,0].argsort()]

    # for data in all_data:
        # print('{:10.2f}{:10.2f}{:10.2f}{:10.2f}{:10.2f}'.format(data[0], data[1], data[2], data[3], data[4]))

    # Separate out the columns into separate arrays
    activities = all_data[:,0]
    coin_rates = all_data[:,1]
    rand_rates = all_data[:,2]
    true_rates = all_data[:,3]
    sing_rates = all_data[:,4]

    # Average out the datapoints
    num_averaging = 2       # number of times to average adjacent points
    for i in range(num_averaging):
        activities = ((activities + np.roll(activities,1))/2.0)[1::2]
        rand_rates = ((rand_rates + np.roll(rand_rates,1))/2.0)[1::2]
        true_rates = ((true_rates + np.roll(true_rates,1))/2.0)[1::2]
        coin_rates = ((coin_rates + np.roll(coin_rates,1))/2.0)[1::2]
        sing_rates = ((sing_rates + np.roll(sing_rates,1))/2.0)[1::2]

    # for i in range(len(activities)):
        # print('{:10.2f}{:10.2f}{:10.2f}{:10.2f}{:10.2f}'.format(activities[i], coin_rates[i], true_rates[i], rand_rates[i], sing_rates[i]))

    # Plotting the results
    # ====================

    plt.rcParams.update({'font.size': 18})
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size
    m_size = 10

    # Plotting the coincidence rate as a function of source activity
    if x_data == 'activity':
        print('Plotting coincidence rate as a function of source activity...')

        fig, ax = plt.subplots(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')
        ax.plot(activities, coin_rates, '.', label = 'Total coincidence rate', markersize = m_size)
        ax.plot(activities, rand_rates, '2', label = 'Random coincidence rate', markersize = m_size)
        ax.plot(activities, true_rates, '+', label = 'True coincidence rate', markersize = m_size)
        ax.set_xlabel(r'Activity (MBq)')
        ax.set_ylabel('Coincidence Rate (Hz)')

        # Remove the "0" from the x-axis
        ax.set_xticklabels(ax.get_xticks())
        labels = ax.get_xticklabels()
        labels[1] = ''
        ax.set_xticklabels(labels)

        ax.legend()

        print('\nSaving plot to', save_dir + 'coincidence_rates_vs_activity.png...')
        fig.savefig(save_dir + 'coincidence_rates_vs_activity.png')

        # plt.show()

        print('\nDone.')

    # Plotting the random and true coincidence rates as a function of total coincidence rate
    elif x_data == 'totals':
        print('Plotting coincidence rate as a function of total coincidence rate...')

        fig, ax = plt.subplots(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')
        # ax.plot(coin_rates, coin_rates, '.', label = 'Total coincidence rate')
        ax.plot(coin_rates, rand_rates, '2', label = 'Random coincidence rate', markersize = m_size)
        ax.plot(coin_rates, true_rates, '+', label = 'True coincidence rate', markersize = m_size)
        ax.set_xlabel(r'Totals rate (Hz)')
        ax.set_ylabel('Coincidence Rate (Hz)')

        # # Remove the "0" from the x-axis
        # ax.set_xticklabels(ax.get_xticks())
        # labels = ax.get_xticklabels()
        # labels[1] = ''
        # ax.set_xticklabels(labels)

        ax.legend()

        print('\nSaving plot to', save_dir + 'coincidence_rates_vs_activity.png...')

        fig.savefig(save_dir + 'coincidence_rates_vs_totals.png')
        # plt.show()

        print('\nDone.')

    # Plotting the coincidence rate as a function of singles rate in a single detector head
    elif x_data == 'singles':
        print('Plotting coincidence rate as a function of singles rate...')

        fig, ax = plt.subplots(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')

        # Draw in linear response curve
        m, um, c, uc = linfit.linfit(sing_rates[:10], coin_rates[:10])
        plot_fit_x = np.linspace(0, max(sing_rates), 100)
        ax.plot(plot_fit_x/1000, linear(plot_fit_x, m, c), 'k', label = 'Linear response')

        # Remove the "0" from the x-axis
        ax.set_xticklabels(ax.get_xticks())
        labels = ax.get_xticklabels()
        labels[1] = ''
        ax.set_xticklabels(labels)

        ax.plot(sing_rates[:-1]/1000, coin_rates[:-1], '.', label = 'Total coincidence rate', markersize = m_size)
        ax.plot(sing_rates[:-1]/1000, rand_rates[:-1], '2', label = 'Random coincidence rate', markersize = m_size)
        ax.plot(sing_rates[:-1]/1000, true_rates[:-1], '+', label = 'True coincidence rate', markersize = m_size)
        ax.set_xlabel(r'Singles rate (kHz)')
        ax.set_ylabel('Coincidence Rate (Hz)')

        # # Remove the "0" from the x-axis
        # ax.set_xticklabels(ax.get_xticks())
        # labels = ax.get_xticklabels()
        # labels[1] = ''
        # ax.set_xticklabels(labels)

        ax.legend()

        print('\nSaving plot to', save_dir + 'coincidence_rates_vs_activity.png...')

        fig.savefig(save_dir + 'coincidence_rates_vs_singles.png')
        # plt.show()

        print('\nDone.')


    return

# Plot coincidence rate as a function of detector rate for H3D dataset
def plot_coincidence_h3d(data_dirs, e_window, coin_window, transforms):
    '''
    Reads in a given list of data directories holding the individual runs taken
    at H3D of the NIST system, reads in the AllEventsCombined.txt file, extracts
    the coincidence rate and plots it as a function of detector rate.

    @params:
        data_dirs           - Required : List of root directories to all the individual runs (Str[])
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        coin_window         - Required : Time period after an event to look for coindicidences in nanoseconds (Int)
        transforms          - Required : Array of transformation arrays, used for coordinate transformations (Float[][])

    @returns:
        null
    '''

    all_data_dirs = []      # empty list holding all the directories of each run

    # Populte the all_data_dirs list
    for data_dir in data_dirs:
        # Assert the directory exists
        if not os.path.isdir(data_dir):
            print('ERROR: Directory', data_dir, 'not found.')
            return
        for subdir, dirs, files in os.walk(data_dir):
            for dir in dirs:
                all_data_dirs.append(data_dir + dir + '/')
            break

    # Empty lists to hold rate info
    sing_rates = []
    coin_rates = []
    rand_rates = []
    true_rates = []

    # Loop through each run and extract the distance and rate data
    for dir in all_data_dirs:

        # Ignore corrupted dataset
        if '191111-run6' in dir:
            continue

        # First get the separation distance from the directory name
        dist = float(re.findall(r'(\d+.\d+)mm', dir)[0])

        # Next correct the detector transformations according to separation distance
        transforms[0][3] = -(dist/2 + 18.22)
        transforms[1][3] = (dist/2 + 18.22)

        # Run coincidence processing
        coin_window /= 10       # converting time to 10s of nanoseconds

        data_file = dir + 'AllEventsCombined.txt'
        allevents, log_str = read_polaris_data(data_file, transforms)       # get the transformed data
        l = len(allevents.index)    # get the length of the array

        event_rate = len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
        sing_rates.append(event_rate)

        # Apply energy window filtering
        print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
              str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

        # Drop all events that are...
        allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                            ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                            (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
        print('Total number of events after energy filter:', len(allevents.index), '(' + str(round(len(allevents.index)/l*100, 1)) + '%)')
        print()

        l = len(allevents.index)    # get the filtered length of the array

        total_time = allevents.iloc[-1]['time'] - allevents.iloc[0]['time']     # get the total time in units of 1e-8 seconds
        total_time_seconds = total_time/1e8     # get the total time in seconds

        print('Copying all events dataframe and adding time delay for randoms counting...')
        allevents_delay = allevents.copy()                                   # make copy for adding time delay
        allevents_delay.loc[allevents_delay.detector == 1, 'time'] += 5e7    # add time delay to detector 1 of half a second
        allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
        print('Done.', '\n')

        print('Counting total coincidence rate and random coincidence rate...')

        # First count the total number of coincidences
        # --------------------------------------------
        det_time_array_ = allevents[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        coin_rate = utils.count_coincidences3(det_time_array, coin_window) / total_time_seconds
        coin_rates.append(coin_rate)

        # Then count the total number of randoms
        # --------------------------------------
        det_time_array_ = allevents_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        rand_rate = utils.count_coincidences3(det_time_array, coin_window) / total_time_seconds
        rand_rates.append(rand_rate)

        # Finally store the number of true coincidences
        true_rates.append(coin_rate - rand_rate)

        print('Done.', '\n')
        print('========================================================================================================================')
        print()

    sing_rates = np.array(sing_rates)
    coin_rates = np.array(coin_rates)
    rand_rates = np.array(rand_rates)
    true_rates = np.array(true_rates)

    # Write the processed data to file
    np.savetxt(data_dirs[-1] + 'coincidence_rates.txt', np.c_[sing_rates, coin_rates, rand_rates, true_rates], fmt = '%.4f', delimiter = ',')

    plt.plot(sing_rates, coin_rates, '.', label = 'Total rate')
    plt.plot(sing_rates, rand_rates, '.', label = 'Randoms rate')
    plt.plot(sing_rates, true_rates, '.', label = 'True rate')
    plt.xlabel('Singles rate (Hz)')
    plt.ylabel('Coincidence rate (Hz)')
    plt.legend()
    plt.show()

    return

# Plots the total, random and true coincidence rates as a function of coincidence window
def coincidence_rates_vs_window(data_dir, e_window, coin_windows, transformations, det_delay = 0, plot_randoms = False):
    '''
    Takes in a directory to an AllEventsCombined.txt file, extracts the data and
    runs coincidence processing on the events for a given range of coincidence
    windows (given in nanoseconds). It then plots the total, random and true
    coincidence rate as a function of timing window.

    @params:
        data_dirs           - Required : List of paths to a tot_rate.txt, rand_rate.txt and true_rate.txt files to read data from (Str[])
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        coin_windows        - Required : Array of coincidence windows to plot in nanoseconds (Int[])
        transformations     - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        det_delay           - Optional : Add a specific delay (in nanoseconds) to detector to correct for any timing misaligment (Int, default = 0)
        plot_randoms        - Optional : Plot the random and true coincidence rate along with total (Bool, default = False)

    @returns:
        null
    '''

    allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations)      # read data and perform coordinate transformations
    tot_raw_events = len(allevents.index)

    print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
          str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

    # Drop all events that are...
    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    allevents.loc[allevents.detector == 1, 'time'] += (det_delay/10)            # add time delay (in 10s of ns)
    allevents.sort_values(by = 'time', inplace = True)                               # sort the columns by time
    allevents.reset_index(drop = True, inplace = True)                                    # reset the index numbering
    l = len(allevents.index)    # get the length of the filtered array

    print('Total number of events after energy filter: {:d} ({:.2f}%)\n'.format(l, l/tot_raw_events*100))

    total_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8     # get the total time in seconds

    if plot_randoms:
        print('Copying all events dataframe and addition time delay for randoms counting...')
        allevents_delay = allevents.copy()                                   # make copy for adding time delay
        allevents_delay.loc[allevents_delay.detector == 1, 'time'] += 5e7    # add time delay to detector 1 of half a second
        allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
        det_time_array_delay_ = allevents_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array
        print('Done.', '\n')


    # First count the total number of coincidences
    # --------------------------------------------
    det_time_array_ = allevents[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

    # Create empty arrays to hold the coincidence rates
    tot_rates = []; rand_rates = []; true_rates = []

    print('Counting coincidence rates...')

    lng = len(coin_windows)
    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, window in enumerate(coin_windows):

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        window /= 10        # convert time to 10s of nanoseconds

        # Count the coincidences
        tot_rates.append(utils.count_coincidences3(det_time_array, window) / total_time)

        if plot_randoms:
            # Add column of zeros for flagging coincidences
            det_time_array_delay = np.zeros((len(det_time_array_delay_),3))
            det_time_array_delay[:,:-1] = det_time_array_delay_
            rand_rates.append(utils.count_coincidences3(det_time_array_delay, window) / total_time)
            true_rates.append(tot_rates[i] - rand_rates[i])

        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    # print('Total number of coincidences: ', tot_rates[0])
    # print('Total time: ', total_time)
    # print('Total coincidence rate: ', tot_rates[0]/(total_time/1e8))
    # print('Total number of coincidences: ', rand_rates[0])
    # print('Total coincidence rate: ', rand_rates[0]/(total_time/1e8))

    # Plotting the results
    # ====================

    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k', )      # Set plot size

    plt.plot(coin_windows, tot_rates, '.', label = 'Total coincidence rate')
    if plot_randoms:
        plt.plot(coin_windows, rand_rates, '.', label = 'Random coincidence rate')
        plt.plot(coin_windows, true_rates, '.', label = 'True coincidence rate')
    plt.legend()
    plt.xlabel('Coincidence Window (ns)')
    plt.ylabel('Coincidence Rate (Hz)')

    # plt.savefig(data_dir + 'coincidence_rates_vs_timing_window.png')
    plt.show()

    return

# Quick method to compare coincidence processing methods
def compare_coin_processing(data_dir, data_filename, e_window, coin_window, transforms):
    '''
    Takes in an AllEventsCombined.txt file, reads the information into a pandas
    array and then applies coincidence processing with the given energy and
    timing windows for two different processing methods.

    @params:
        data_dir            - Required : Path to combined detector data .txt file (Str)
        data_filename       - Required : Name of combined detector data .txt file to read data from (Str)
        e_window            - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        coin_window         - Required : Time period after an event to look for coindicidences in nanoseconds (Int)
        transforms          - Required : Array of transformation arrays, used for coordinate transformations (Float[][])

    @returns:
        None
    '''

    allevents, log_str = read_polaris_data(data_dir + data_filename, transforms)       # read in the raw polaris data
    tot_raw_events = len(allevents.index)          # get the total number of events before coincidence processing

    print('\nProcessing coincidences')
    print('-----------------------')

    print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
          str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

    # Drop all events that are...
    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    print('Total number of events after energy filter:', len(allevents.index), '(' + str(round(len(allevents.index)/tot_raw_events*100, 1)) + '%)')
    print()

    l = len(allevents.index)

    coin_window /= 10      #  convert coincidence window to 10s of nanoseconds

    print('Determining coincidence rates using full processing')
    print('===================================================', '\n')

    # Find the coincidences in the energy filtered data
    # -------------------------------------------------
    print('Grouping coincidences according to a timing window of', coin_window*10, 'nanoseconds...')

    np.set_printoptions(edgeitems = 15, linewidth = 200)

    det_time_array_ = allevents[['detector', 'x', 'y', 'z', 'time']].values        # get the detector and time columns as a 2D numpy array
    det_time_array_ = det_time_array_[det_time_array_[:,4].argsort(kind='stable')]      # sort by time

    # Add column of zeros for flagging coincidences
    det_time_array = np.zeros((l,6))
    det_time_array[:,:-1] = det_time_array_

    coincidence_data, tot_coins, tot_coins_per_event = utils.find_coincidences2(det_time_array, coin_window, single_counting = True, choose_random = False)

    print('{:45} {:d}'.format('Total number of coincidences:', len(coincidence_data.index)))
    print('{:45} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
    print('{:45} {:.0f}'.format('Total coincidence rate (coins/sec):', len(coincidence_data.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

    print()

    print('Determining coincidence rates using just counting')
    print('=================================================', '\n')

    det_time_array_ = allevents[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

    # Add column of zeros for flagging coincidences
    det_time_array = np.zeros((len(det_time_array_),3))
    det_time_array[:,:-1] = det_time_array_

    total_coincidences = utils.count_coincidences3(det_time_array, coin_window)

    print('{:45} {:d}'.format('Total number of coincidences:', total_coincidences))
    print('{:45} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
    print('{:45} {:.0f}'.format('Total coincidence rate (coins/sec):', total_coincidences / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

    return

# Read in a .dat binary or AllEvents.txt file and produce rate files
def generate_rates(data_dirs, filename, file_type, e_window, t_int, apply_ce_filtering = True):
    '''
    Read in a .dat binary file from a single module and produce raw singles,
    doubles and triples+ rate files, as well as energy-filtered versions.

    The expected binary format is as follows:

    1 byte: number of interactions
    4 bytes: timestamp in second
    4 bytes: timestamp in millisecond

    Detector interactions:

    1 byte: detector number
    4 byte: x position in um
    4 byte: y position in um
    4 byte: z position in 10um
    4 byte: energy in keV

    for instance, if the number of interactions is 1, then it is 9+17 = 26 bytes.

    If the number of interactions is 122, then it indicates timestamp data:

    1 byte: 122
    1 byte: 2
    8 byte: Sync pulse index
    8 byte: Sync pulse time stamp

    If the number of interactions is 0, please skip next 23 bytes.

    @params:
        data_dirs               - Required : List of paths to multiple .dat files (Str)
        filename                - Required : Name of .dat files (Str)
        file_type               - Required : File type identifier. 0 for .dat binary, 1 for AllEvents.txt (Int)
        e_window                - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        t_int                   - Required : The time interval to use for processing rates in seconds (Int)
        apply_ce_filtering      - Optional : Whether or not to energy filter on the compton edge (Bool, default = True)

    @returns:
        None
    '''

    for data_dir in data_dirs:

        # Event array for processed data
        allevents = []      # Format: [number_interactions, time_stamp (10ns), energy (keV)]

        if file_type:

            print('Reading AllEvents.txt file', data_dir + 'AllEvents.txt' + '.', 'This may take several minutes...\n')

            # Read AllEvents.txt into a DataFrame
            events_pd = pd.read_csv(data_dir + 'AllEvents.txt', sep = '	', header = None,
                                  names = ['num_scatters', 'energy', 'x', 'y', 'z', 'time', 'event_num'],
                                  usecols = ['num_scatters', 'energy', 'time'])

            events_pd = events_pd.fillna(method = 'bfill')          # add in missing times

            allevents = events_pd.to_numpy(dtype = np.uint64)       # convert to numpy array
            allevents[:,1] = allevents[:,1]*1000

        else:

            bin_file = data_dir + filename

            print('Reading binary file', bin_file + '.', 'This may take several minutes...\n')

            # Open and read binary:
            with open(bin_file, "rb") as binary_file:

                curr_pos = 0            # current read position in the file

                while True:

                    # testing for end of file
                    curr_pos_test = binary_file.tell()
                    test = binary_file.read(1)
                    if not test:
                        break
                    binary_file.seek(curr_pos_test)

                    # Seek a specific position in the file
                    binary_file.seek(curr_pos, 0)

                    # Number of interactions
                    indicator = binary_file.read(1)
                    indicator_int = int.from_bytes(indicator, byteorder='big')

                    # If the indicator is 0, skip the next 23 bytes
                    if indicator_int == 0:
                        curr_pos += 24
                        continue

                    # Sync pulse info - skip it
                    if indicator_int == 122:
                        curr_pos += 18
                        continue

                    # Interaction data
                    timestamp = int.from_bytes(binary_file.read(8), byteorder='big')  # get the timestamp
                    curr_pos += 9

                    # Loop through the total number of interactions
                    for i in range(indicator_int):
                        # det_num = int.from_bytes(binary_file.read(1), byteorder='big')
                        # x_pos = int.from_bytes(binary_file.read(4), byteorder='big', signed = True)
                        # y_pos = int.from_bytes(binary_file.read(4), byteorder='big', signed = True)
                        # z_pos = int.from_bytes(binary_file.read(4), byteorder='big', signed = True)
                        binary_file.read(13)                                                 # skip the detector number and x,y,z position
                        energy = int.from_bytes(binary_file.read(4), byteorder='big')     # get the energy

                        # allevents.append([indicator_int, det_num, energy, x_pos, y_pos, z_pos, timestamp])        # append the next event information
                        allevents.append([int(indicator_int), int(energy), int(timestamp)])        # append the next event information

                        curr_pos += 17

        allevents = np.array(allevents)     # convert to numpy array

        unfilt_num_singles = len(allevents[allevents[:,0] == 1])        # get the unfiltered number of singles

        print('Calculating rate information...')

        # Arrays to hold rate information
        times, singles_rates, doubles_rates, triplesp_rates = [], [], [], []

        t_int_ns = t_int*1e8                        # convert time interval into 10s of nanoseconds
        tot_time = 0                                # keeping track of the total elapsed time
        end_time = int(allevents[0][2] + t_int_ns)

        # Now count all the rates
        i = 0
        while i < len(allevents):
            num_singles, num_doubles, num_triplesp = 0, 0, 0      # the total number of singles, doubles and triples+ events

            while allevents[i][2] < end_time:
                if allevents[i][0] == 1:
                    num_singles += 1
                elif allevents[i][0] == 2:
                    num_doubles += 1
                else:
                    num_triplesp += 1

                i += int(allevents[i][0])        # skip the appropriate number of events

                # To avoid index error
                if i >= len(allevents):
                    break

            tot_time += t_int
            times.append(tot_time/60)
            singles_rates.append(num_singles/t_int)
            doubles_rates.append(num_doubles/t_int)
            triplesp_rates.append(num_triplesp/t_int)

            # To avoid index error
            if i >= len(allevents):
                break

            end_time = int(allevents[i][2] + t_int_ns)
            i += 1

        print('Done.\n')

        allevents[:,1] = allevents[:,1]/1000        # convert energy to keV

        # Select events that are both single interaction events and fall within the energy windowing
        if apply_ce_filtering:
            print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV', 'and Compton edge window of',
                  str(e_window[2]) + 'keV', 'to', str(e_window[3]) + 'keV...')

            allevents = allevents[(allevents[:,0] == 1) & ( ( (allevents[:,1] > e_window[0]) & (allevents[:,1] < e_window[1]) ) | ( (allevents[:,1] > e_window[2]) & (allevents[:,1] < e_window[3]) ) )]
        else:
            print('Applying energy window filtering. Using peak window of', str(e_window[0]) + 'keV', 'to', str(e_window[1]) + 'keV')

            allevents = allevents[(allevents[:,0] == 1) & ( (allevents[:,1] > e_window[0]) & (allevents[:,1] < e_window[1]) )]

        print('Total number of events after energy filter:', len(allevents), '(' + str(round(len(allevents)/unfilt_num_singles*100, 1)) + '%)')
        print('Done.\n')

        print('Running rate processing on filtered data...')

        singles_rates_filtered = []

        tot_time = 0                                # keeping track of the total elapsed time
        end_time = int(allevents[0][2] + t_int_ns)

        # Now count all the rates
        i = 0
        while i < len(allevents):
            # print(i)

            num_singles = 0      # the total number of singles, doubles and triples+ events

            while allevents[i][2] < end_time:
                num_singles += 1

                i += 1

                # To avoid index error
                if i >= len(allevents):
                    break

            singles_rates_filtered.append(num_singles/t_int)

            # To avoid index error
            if i >= len(allevents):
                break

            end_time = int(allevents[i][2] + t_int_ns)

            i += 1

        print("Done.\n")


        # Write data to write directory
        if not os.path.exists(data_dir + 'rate_files/'):
            os.makedirs(data_dir + 'rate_files/')
            print ('Creating directory: {}'.format(data_dir + 'rate_files/'))

        module_number = 51
        if not file_type:
            module_number = int(re.search(r'mod(\d+)', filename).group(1))

        print('Saving rate information to .txt...')

        np.savetxt('{}rate_files/singles-{:d}s-{:d}.txt'.format(data_dir, int(t_int), module_number), np.c_[times, singles_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt('{}rate_files/doubles-{:d}s-{:d}.txt'.format(data_dir, int(t_int), module_number), np.c_[times, doubles_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt('{}rate_files/triplesplus-{:d}s-{:d}.txt'.format(data_dir, int(t_int), module_number), np.c_[times, triplesp_rates], fmt = '%.4f', delimiter = ',')
        np.savetxt('{}rate_files/singles_filtered-{:d}s-{:d}.txt'.format(data_dir, int(t_int), module_number), np.c_[times, singles_rates_filtered], fmt = '%.4f', delimiter = ',')

        print('Done.\n')

        # plt.plot(times, singles_rates, '.', label = 'Singles rate')
        # plt.plot(times, singles_rates_filtered, '.', label = 'Filtered singles rate')
        # plt.plot(times, doubles_rates, '.', label = 'Doubles rate')
        # plt.plot(times, triplesp_rates, '.', label = 'Triples rate')
        # plt.legend()
        # plt.show()


        # energies = allevents[:,1]/1000
        # plt.hist(energies[(energies > 100) & (energies < 1500)], bins = 512)
        # plt.show()

        print('===============================================================================================================')

    return

# Plot the module singles rate as a function of activity for multiple rate files produced by the generate_rates method
def plot_module_rates(data_dirs, filenames, write_dir, initial_activities, t_half, lbls, marker_styles):
    '''
    Reads in multiple .txt rate files produced by the generate_rates function
    and plots them on the same axis as a function of activity.

    @params:
        data_dirs               - Required : List of paths to multiple .txt files (Str[])
        filenames               - Required : List of files to plot (Str[])
        write_dir               - Required : Path to write final plot to (Str)
        initial_activities      - Required : List of the initial activities of sources in uCi (Float[])
        t_half                  - Required : Half life of the source in minutes (Float)
        lbls                    - Required : Labels for the plots if plotting multiple event files (Str[])
        marker_styles           - Required : Styles to use for the markers for each data_set (Str[])

    @returns:
        None
    '''
    # Function to calculate the activity as a function of time
    def A(t, A0, t_h):
        return A0 * 2**(-t/t_h)

    # Initialise lists to hold each filename dataset
    plot_arrays = []

    print('Reading in rate files...')

    for filename in filenames:
        # Initialise plotting arrays
        rates = np.array([])
        activities = np.array([])

        # Read in the data and populate plotting arrrays
        for i, data_dir in enumerate(data_dirs):

            data_file = data_dir + filename

            if not os.path.isfile(data_file):
                print ('File', data_file, 'does not exist.')
                return

            # Read the data into an array
            plot_data = np.loadtxt(data_file, dtype = np.float64, delimiter = ',')

            if len(plot_data) > 10:
                plot_data = plot_data[:-1]      # drop the last datapoint as it represents incomplete data

            # Extract times and calculate activities
            times = plot_data[:,0]
            acts = A(times, initial_activities[i], t_half)

            rts = plot_data[:,1]

            # if 'D0' in data_file:
            #     rts *= 1.6

            if 'run19' in data_file:
                acts = acts[:3]
                rts = rts[:3]

            # Convert from uCi to MBq
            acts = acts*3.7e-2

            # Convert from Hz to kHz
            rts = rts/1000

            # Removing any data with activity greater than 3.7MBq
            all_data = np.transpose([acts, rts])
            all_data = all_data[all_data[:,0] < 3.7]
            acts = all_data[:,0]
            rts = all_data[:,1]

            activities = np.concatenate((activities, acts))
            rates = np.concatenate((rates, rts))

        plot_arrays.append([activities, rates])

    print('Done.\n')

    # Plotting the results
    # --------------------
    print('Plotting the final results...')
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    # plt.rcParams.update({'font.size': 18})

    for i, plot_array in enumerate(plot_arrays):
        plt.plot(plot_array[0], plot_array[1], marker_styles[i], label = lbls[i])

    # Fit and plot the linear response curve
    fit_x = np.array(plot_arrays[0][0])     # get the activity data from first dataset
    fit_y = np.array(plot_arrays[0][1])     # get rates data from first dataset
    fit_arr = np.vstack((fit_x, fit_y)).T   # combine them into a single 2D array
    fit_arr = fit_arr[fit_arr[:,0] < 0.37]    # filter out all the data with activities greater than 10uCi
    fit_x = fit_arr[:,0]                    # separate the activity and rate data again
    fit_y = fit_arr[:,1]

    m, um, c, uc = linfit.linfit(fit_x, fit_y)
    plot_fit_x = np.linspace(0, max(plot_arrays[0][0]), 100)
    plt.plot(plot_fit_x, linear(plot_fit_x, m, c), 'k--', label = 'Linear response')

    plt.ylim(0, 40)
    plt.xlabel(r'Activity (MBq)')
    plt.ylabel(r'OEM2 Event Rate (kHz)')
    plt.legend()
    print('Done.\n')

    # print('Saving figure to', '{}oem2_rates_vs_activity.png...'.format(write_dir))
    # plt.savefig('{}oem2_rates_vs_activity.png'.format(write_dir))
    # print('Done.')
    plt.show()

    return

# Generate a text file (and optionally plot) a module singles rates from an AllEventsCombined.txt file as a function of source activity
def calculate_rates(data_dirs, write_dir, e_window, initial_activities, t_half, t_int, singles_or_coincidences = 0, count_all_interactions = True, modules = [], coin_window = -1, count_randoms = True, mod_delays = [], count_deadtime_peak = False, show_plots = False):
    '''
    Loops through all the directories in data_dirs, reading in the data from the
    AllEventsCombined.txt file, splitting the data according to the time interval
    given and writing the rates as a function of source activity.

    @params:
        data_dirs                   - Required : List of paths to multiple .dat files (Str)
        write_dir                   - Required : Path to write final data as a function of activity to (Str)
        e_window                    - Required : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        initial_activities          - Required : List of the initial activities of sources in kBq (Float[])
        t_half                      - Required : Half life of the source in seconds (Float)
        t_int                       - Required : The time interval to use for processing rates in seconds (Int)
        singles_or_coincidences     - Optional : Output the singels rate or the coincidence rate. 0 = singles, 1 = coincidences (Int, default = 0)
        count_all_interactions      - Optional : Whether to count the number of interactions (i.e. a double contributes two events) or count multiple interaction events as one (Bool, default = False)
        modules                     - Optional : List of modules to produce rate files from, indexed from 0. If empty, both modules are included. (Int, default = [])
        coin_window                 - Optional : Time period in nanoseconds around an event within which to look for coincidences (Int, default = -1)
        count_randoms               - Optional : Count both the prompt coincidence rate and the random coincidence rate (Bool, default = True)
        mod_delay                   - Optional : Add a specific delay (in nanoseconds) to detectors to correct for any timing misaligment (Int[], default = [])
        count_deadtime_peak         - Optional : Whether or not to count coincidences for the deadtime delay peak (Bool, default = False)
        show_plots                  - Optional : Whether to plot the final rate curves after processing (Bool, default = False)

    @returns:
        None
    '''
    # Activity function for calculating the activity
    def A(t, A_0, t_h):
        return A_0 * 2**(-t/t_h)


    if not singles_or_coincidences:         # Produce rate files for singles data

        # Produce rate files for all modules combined
        if modules == []:
            print('Counting events for all modules combined.')
            print('-----------------------------------------\n')

            all_rates, all_activities = [], []

            # Loop through each experiment and produce the rate information
            for j, data_dir in enumerate(data_dirs):

                data_file = '{}AllEventsCombined.txt'.format(data_dir)
                print('Running analysis on {}\n'.format(data_file))

                # Reading in raw data and processing
                # ----------------------------------
                print('Reading in file...')

                allevents, log_str = read_polaris_data(data_file,
                                                       transformations = [],
                                                       e_window = e_window,
                                                       singles_only = False,
                                                       write_transformed_data = False)

                # Add timing correction
                if mod_delays != []:
                    print('Adding timing correction and sorting...')
                    for i, delay in enumerate(mod_delays):
                        allevents.loc[allevents.detector == i+1, 'time'] += (delay/10)            # add time delay (in 10s of ns)

                    allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
                    allevents.reset_index(drop = True, inplace = True)                          # reset the indicies


                if not count_all_interactions:
                    print('Processing multiple scatter events...')

                    # Remove additional events from multiple scatter events
                    for i in range(2, max(allevents.scatters)):
                        allevents_doubles = allevents[allevents.scatters == i]      # get the multiple scatter events
                        allevents_doubles.reset_index(drop = True, inplace = True)
                        allevents_reduced = allevents_doubles[allevents_doubles.index%i == 0]       # remove extra scatter events
                        allevents.drop(allevents[allevents.scatters == i].index, inplace = True)    # get allevents without multiple scatter events
                        allevents = allevents.append(allevents_reduced, ignore_index = True)        # add the reduced multiple scatters back
                        allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
                        allevents.reset_index(drop = True, inplace = True)                          # reset the indicies

                    print('Done.\n')
                # ----------------------------------

                print('Splitting dataframe and counting unfiltered singles event rates...')
                S = allevents.time
                splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

                rates, activities = [], []
                t_elapsed = 0
                A_0 = initial_activities[j]     # get the initial activity at the start of the measurement
                for i, df in enumerate(splitevents):

                    num_events = len(df.index)                                      # first count the single interaction events
                    df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time
                    rate = (num_events/df_time)                                     # calculate the rate

                    t_elapsed += df_time                                            # keep track to total elapsed time
                    activity = A(t_elapsed, A_0, t_half)                            # calculate the activity at the end of the period

                    rates.append(rate)                                              # store the rate
                    activities.append(activity)                                     # store the activity

                all_rates.append(rates)
                all_activities.append(activities)
                print('Done.\n')

                print('=====================================================================\n')

            # Flatten the lists
            all_rates = [r for sublist in all_rates for r in sublist]
            all_activities = [r for sublist in all_activities for r in sublist]

            # Write the results to a text file
            # ---------------------------------
            if not os.path.exists(write_dir):
                print ('Creating directory: {}'.format(write_dir))
                os.makedirs(write_dir)

            if e_window != []:
                np.savetxt('{}filtered_singles-{:d}s-allmods.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')
            else:
                np.savetxt('{}unfiltered_singles-{:d}s-allmods.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')

            # Plot the results
            # ----------------
            if show_plots:
                plt.plot(all_activities, all_rates, '.')
                plt.xlabel('Activity (kBq)')
                plt.ylabel('Singles Rate (Hz)')
                plt.show()


        # Produce rate files for each module specified
        else:
            print('Counting events for modules separately.')
            print('---------------------------------------\n')

            # Create empty list of lists to hold all of the module data
            all_mod_rates, all_mod_activities = [], []

            for i in range(len(modules)):
                all_mod_rates.append([])
                all_mod_activities.append([])

            # Loop through each experiment and produce the rate information
            for j, data_dir in enumerate(data_dirs):

                data_file = '{}AllEventsCombined.txt'.format(data_dir)
                print('Running analysis on {}\n'.format(data_file))

                # Reading in raw data and processing
                # ----------------------------------
                print('Reading in file...')

                allevents_, log_str = read_polaris_data(data_file,
                                                        transformations = [],
                                                        e_window = e_window,
                                                        singles_only = False,
                                                        write_transformed_data = False)

                if not count_all_interactions:
                    print('Processing multiple scatter events...')
                    # Remove additional events from multiple scatter events
                    for i in range(2, max(allevents_.scatters)):
                        allevents_doubles = allevents_[allevents_.scatters == i]      # get the multiple scatter events
                        allevents_doubles.reset_index(drop = True, inplace = True)
                        allevents_reduced = allevents_doubles[allevents_doubles.index%i == 0]       # remove extra scatter events
                        allevents_.drop(allevents_[allevents_.scatters == i].index, inplace = True)    # get allevents without multiple scatter events
                        allevents_ = allevents_.append(allevents_reduced, ignore_index = True)        # add the reduced multiple scatters back
                        allevents_.sort_values(by = 'time', inplace = True)                          # sort the columns by time
                        allevents_.reset_index(drop = True, inplace = True)                          # reset the indicies

                    print('Done.\n')
                # ----------------------------------

                # Loop through each module
                for m, module in enumerate(modules):
                    print('Counting events for module', module, '\n')

                    allevents = allevents_[allevents_.detector == module]       # get events from single module
                    allevents.reset_index(drop = True, inplace = True)          # reset the indicies

                    print('Splitting dataframe and counting unfiltered singles event rates...')
                    S = allevents.time
                    splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

                    rates, activities = [], []
                    t_elapsed = 0
                    A_0 = initial_activities[j]     # get the initial activity at the start of the measurement
                    for i, df in enumerate(splitevents):

                        num_events = len(df.index)                                      # first count the single interaction events
                        df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time
                        rate = (num_events/df_time)                                     # calculate the rate

                        t_elapsed += df_time                                            # keep track to total elapsed time
                        activity = A(t_elapsed, A_0, t_half)                            # calculate the activity at the end of the period

                        rates.append(rate)                                              # store the rate
                        activities.append(activity)                                     # store the activity

                    all_mod_rates[m].append(rates)
                    all_mod_activities[m].append(activities)
                    print('Done.\n')

                print('=====================================================================\n')

            # Write each module's rate data
            for i in range(len(all_mod_rates)):
                module = modules[i]
                print('Writing rate data for module', module, '\n')
                all_rates = all_mod_rates[i]
                all_activities = all_mod_activities[i]

                # Flatten the lists
                all_rates = [r for sublist in all_rates for r in sublist]
                all_activities = [r for sublist in all_activities for r in sublist]

                # Write the results to a text file
                # ---------------------------------
                if not os.path.exists(write_dir):
                    print ('Creating directory: {}'.format(write_dir))
                    os.makedirs(write_dir)

                if e_window != []:
                    np.savetxt('{}filtered_singles-{:d}s-mod{:d}.txt'.format(write_dir, int(t_int), module), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')
                else:
                    np.savetxt('{}unfiltered_singles-{:d}s-mod{:d}.txt'.format(write_dir, int(t_int), module), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')

                # Plot the results
                # ----------------
                if show_plots:
                    plt.plot(all_activities, all_rates, '.')
                    plt.xlabel('Activity (kBq)')
                    plt.ylabel('Singles Rate (Hz)')
                    plt.show()



    else:                                   # Produce rate files for coincidence data

        print('Counting coincidences.')
        print('----------------------\n')

        all_rates, all_rand_rates, all_activities = [], [], []

        # Loop through each experiment and produce the rate information
        for j, data_dir in enumerate(data_dirs):

            data_file = '{}AllEventsCombined.txt'.format(data_dir)
            print('Running analysis on {}\n'.format(data_file))

            # Reading in raw data and processing
            # ----------------------------------
            # print('Reading in file...')

            allevents, log_str = read_polaris_data(data_file,
                                                   transformations = [],
                                                   e_window = e_window,
                                                   singles_only = False,
                                                   write_transformed_data = False)

            # allevents = allevents.iloc[:1000000]

            # Add timing correction
            if mod_delays != []:
                print('Adding timing correction and sorting...')
                for i, delay in enumerate(mod_delays):
                    allevents.loc[allevents.detector == i+1, 'time'] += (delay/10)            # add time delay (in 10s of ns)

                allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
                allevents.reset_index(drop = True, inplace = True)                          # reset the indicies

            # # Remove additional events from multiple scatter events
            # print('Processing multiple scatter events...')
            # for i in range(2, max(allevents.scatters)):
            #     allevents_doubles = allevents[allevents.scatters == i]      # get the multiple scatter events
            #     allevents_doubles.reset_index(drop = True, inplace = True)
            #     allevents_reduced = allevents_doubles[allevents_doubles.index%i == 0]       # remove extra scatter events
            #     allevents.drop(allevents[allevents.scatters == i].index, inplace = True)    # get allevents without multiple scatter events
            #     allevents = allevents.append(allevents_reduced, ignore_index = True)        # add the reduced multiple scatters back
            #     allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
            #     allevents.reset_index(drop = True, inplace = True)                          # reset the indicies
            # print('Done.\n')
            # ----------------------------------

            print('Splitting dataframe and counting prompt coincidence rates...\n')

            S = allevents.time
            splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

            lng = len(splitevents)
            # print('Prompt splitevents = ', lng)

            printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            rates, activities = [], []
            t_elapsed = 0
            A_0 = initial_activities[j]     # get the initial activity at the start of the measurement
            for i, df in enumerate(splitevents):

                df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time
                t_elapsed += df_time                    # keep track to total elapsed time
                activity = A(t_elapsed, A_0, t_half)    # calculate the activity at the end of the period
                activities.append(activity)             # store the activity

                # First count the number of prompt coincidences
                # ---------------------------------------------

                det_time_array_ = df[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

                # Add column of zeros for flagging coincidences
                det_time_array = np.zeros((len(det_time_array_),3))
                det_time_array[:,:-1] = det_time_array_

                tot_coins = utils.count_coincidences3(det_time_array, coin_window/10, choose_all = count_all_interactions)

                rates.append(tot_coins/df_time)      # store the rate

                # Count coincidences with count_coincidences() function
                # -----------------------------------------------------
                # tot_coins, coin_rate = count_coincidences(df, coin_window)
                #
                # if count_randoms:
                #     df.time[df.detector == 1] += 5e7                  # add half-second delay to module 1
                #     df.sort_values(by = 'time', inplace = True)     # sort the columns by time
                #     df.reset_index(drop = True, inplace = True)     # reset the indicies
                #
                #     tot_rands, rand_rate = count_coincidences(df, coin_window)
                #     rand_rates.append(rand_rate)              # store the rate
                #
                # rates.append(coin_rate)              # store the rate
                # activities.append(activity)          # store the activity

                printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            all_rates.append(rates)
            all_activities.append(activities)


            # Count the total number of randoms
            # ---------------------------------
            if count_randoms:
                print('\nSplitting dataframe and counting random coincidence rates...\n')

                allevents_rand = allevents.copy()       # get a copy of all the events
                allevents_rand.time[allevents_rand.detector == 1] += 6400                # add 0.5s delay to module 1
                # allevents_rand.time[allevents_rand.detector == 1] += 5e7                # add 0.5s delay to module 1
                allevents_rand.sort_values(by = 'time', inplace = True)     # sort the columns by time
                allevents_rand.reset_index(drop = True, inplace = True)     # reset the indicies

                S = allevents_rand.time
                splitevents = [g.reset_index(drop=True) for i, g in allevents_rand.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

                # Necessary because adding the delay increases the number of splitevents
                if len(splitevents) > lng:
                    splitevents = splitevents[:-1]

                lng = len(splitevents)
                # print('Random splitevents = ', lng)

                printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

                rand_rates = []
                t_elapsed = 0
                for i, df in enumerate(splitevents):

                    df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time

                    # First count the number of prompt coincidences
                    # ---------------------------------------------
                    det_time_array_ = df[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

                    # Add column of zeros for flagging coincidences
                    det_time_array = np.zeros((len(det_time_array_),3))
                    det_time_array[:,:-1] = det_time_array_

                    tot_coins = utils.count_coincidences3(det_time_array, coin_window/10, choose_all = count_all_interactions)

                    rand_rates.append(tot_coins/df_time)      # store the rate

                    printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

                all_rand_rates.append(rand_rates)

            print('Done.\n')

            print('=====================================================================\n')

        # Flatten the lists
        all_rates = [r for sublist in all_rates for r in sublist]
        all_activities = [r for sublist in all_activities for r in sublist]
        if count_randoms:
            all_rand_rates = [r for sublist in all_rand_rates for r in sublist]

        # Write the results to a text file
        # ---------------------------------
        if not os.path.exists(write_dir):
            print ('Creating directory: {}'.format(write_dir))
            os.makedirs(write_dir)

        print('Writing results to', write_dir)
        if e_window != []:
            np.savetxt('{}filtered_prompt_coincidences-{:d}s.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')
            if count_randoms:
                np.savetxt('{}filtered_random_coincidences-{:d}s.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rand_rates], fmt = '%.4f', delimiter = ',')
        else:
            np.savetxt('{}unfiltered_prompt_coincidences-{:d}s.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rates], fmt = '%.4f', delimiter = ',')
            if count_randoms:
                np.savetxt('{}unfiltered_random_coincidences-{:d}s.txt'.format(write_dir, int(t_int)), np.c_[all_activities, all_rand_rates], fmt = '%.4f', delimiter = ',')

        # Plot the results
        # ----------------
        if show_plots:
            plt.plot(all_activities, all_rates, '.', label = 'Prompts')
            plt.xlabel('Activity (kBq)')
            plt.ylabel('Coincidence Rate (Hz)')
            if count_randoms:
                plt.plot(all_activities, all_rand_rates, '.', label = 'Randoms')

                all_true_rates = np.array(all_rates) - np.array(all_rand_rates)
                plt.plot(all_activities, all_true_rates, '.', label = 'Trues')

                plt.legend()

            plt.show()


    return

# Plot singles rate of a single module with a deadtime curve plotted over
def deadtime_model_singles(rate_files_list, write_dir, model, taus, branch_ratio, abs_effs, plot_labels, data_plot_labels, data_colors, fit_colors):
    '''
    Takes in a 2D list of singles rate files, one axis corresponding to the module
    and the other axis corresponding to multiple rate files from the same modele.
    Plots the raw singles data as well as the specified deadtime model over with
    the provided fit parameters.

    @params:
        rate_files_list     - Required : 2D list of paths to .txt files containing singles rates, for multiple modules (Str[][])
        write_dir           - Required : Path to write final plot to (Str)
        model               - Required : Whether to use a paralysable (0) or non-paralysable (1) deadtime model (Int)
        taus                - Required : List of deadtime parameters in seconds for each module (Float[])
        branch_ratio        - Required : Branching ratio of the source isotope (Float)
        abs_effs            - Required : Absolute efficiencies of experimental setup for each module (Float[])
        plot_labels         - Required : List of labels for each plot in rate_files (Str[])
        data_plot_labels    - Required : List of labels for each data point plot in rate_files (Str[])
        data_colors         - Required : List of colors for the plotted datapoints (Str[])
        fit_colors          - Required : List of colors for the linear fits over the datapoints (Str[])

    @returns:
        None
    '''

    figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 20})

    # Non-paralysable deadtime model
    def np_model(n, t):
        return n/(1 + n*t)

    # Paralysable deadtime model
    def p_model(n, t):
        return n*np.exp(-n*t)

    for i, rate_files in enumerate(rate_files_list):      # loop through each list of rate files for each module

        activities, rates = [], []

        for rate_file in rate_files:
            print('Reading in rate data from {}...'.format(rate_file))

            activities_, rates_ = np.loadtxt(rate_file, delimiter = ',', skiprows = 1, unpack = True)
            activities_ *= 1000      # convert to Bq

            # Truncate to avoid overlap
            if int(re.search(r'run(\d+)', rate_file).group(1)) > 15:
                data = np.vstack((activities_, rates_)).T
                data = data[data[:,0] > 2.372e6]
                data[:,1] *= 1.0066
                activities_ = data[:,0]; rates_ = data[:,1]

            # Apply linear fit to first part of the curve
            if 'run3' in rate_file and i == 0:
                data = np.vstack((activities_/1e3, rates_/1e3)).T
                lin_data = data[data[:,0] < 250]*0.9
                m, c, r, p_value, std_err = stats.linregress(lin_data[:,0], lin_data[:,1])
                lin_x = np.linspace(0, 4000, 1000)
                lin_y = m*lin_x + c
                plt.plot(lin_x, lin_y, '--k', label = 'Expected Linear Response')

            activities.append(activities_)
            rates.append(rates_)

        # Flatten the lists
        activities = [r for sublist in activities for r in sublist]
        rates = [r for sublist in rates for r in sublist]

        activities = np.array(activities)
        rates = np.array(rates)

        # Average over n rows
        n = 20
        activities = np.nanmean(np.pad(activities.astype(float), (0, n - activities.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
        rates = np.nanmean(np.pad(rates.astype(float), (0, n - rates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)

        # Truncate for the plot
        # plot_data = np.vstack((activities, rates)).T
        # plot_data = plot_data[plot_data[:,0] < 3.5e6]
        # activities = plot_data[:,0]
        # rates = plot_data[:,1]

        # Plotting the results
        # --------------------
        print('Plotting the final results...')
        # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
        # plt.rcParams.update({'font.size': 18})

        plt.plot(activities/1e3, rates/1e3, '.', color = data_colors[i], label = data_plot_labels[i])

        # Plot the model over the data
        if model:
            model_activities = np.linspace(0, max(activities), 1000)        # source activity for the model
            model_ns = model_activities*branch_ratio*abs_effs[i]                # get the theoretical true rate
            model_ms = np_model(model_ns, taus[i])                              # get the theoretical measured rate

            plt.plot(model_activities/1e3, model_ms/1e3, color = fit_colors[i], label = plot_labels[i])

        print()


    plt.xlim(0)
    # plt.ylim(0)
    # plt.xlim(0, 3700)
    plt.ylim(0, 35)

    # Remove zero from xticks
    ax = plt.gca()
    xticks = ax.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)

    plt.xlabel(r'Activity (kBq)')
    plt.ylabel(r'Module Event Rate (kHz)')
    plt.legend(loc = 'lower right', markerscale = 3)

    plt.savefig(write_dir + 'singles_vs_activity-deadtime_fit.png')
    # plt.savefig(write_dir + 'singles_vs_activity-deadtime_fit.eps', format = 'eps')
    # plt.show()

    return

# Plot coincidence rate of a single module with a deadtime model curve plotted over
def deadtime_model_coincidences(data_dirs, rate_filename, write_dir, initial_activities, t_half):
    '''
    Reads in multiple .txt rate files produced by the generate_rates function
    and plots them on the same axis as a function of activity.

    @params:
        data_dirs               - Required : List of paths to multiple .txt files (Str[])
        filenames               - Required : List of files to plot (Str[])
        write_dir               - Required : Path to write final plot to (Str)
        initial_activities      - Required : List of the initial activities of sources in uCi (Float[])
        t_half                  - Required : Half life of the source in minutes (Float)

    @returns:
        None
    '''
    # Function to calculate the activity as a function of time
    def A(t, A0, t_h):
        return A0 * 2**(-t/t_h)

    # Non-paralysable deadtime model
    def np_model(n, f, t_np):
        return f*n/(1 + f*n*t_np)

    # Paralysable deadtime model
    def p_model(n, f, t_p):
        return f*n*np.exp(-f*n*t_p)

    # Initialise lists to hold each filename dataset
    plot_arrays = []


    print('Reading in rate files...')

    # Initialise plotting arrays
    rates = np.array([])
    activities = np.array([])

    # Read in the data and populate plotting arrrays
    for i, data_dir in enumerate(data_dirs):

        data_file = data_dir + rate_filename

        if not os.path.isfile(data_file):
            print ('File', data_file, 'does not exist.')
            return

        # Read the data into an array
        plot_data = np.loadtxt(data_file, dtype = np.float64, delimiter = ',')

        if len(plot_data) > 10:
            plot_data = plot_data[:-1]      # drop the last datapoint as it represents incomplete data

        # Extract times and calculate activities
        times = plot_data[:,0]
        acts = A(times, initial_activities[i], t_half)

        rts = plot_data[:,1]

        # if 'D0' in data_file:
        #     rts *= 1.6

        if 'run19' in data_file:
            acts = acts[:3]
            rts = rts[:3]

        # Convert from uCi to MBq
        acts = acts*3.7e-2

        # Convert from Hz to kHz
        rts = rts/1000

        # Removing any data with activity greater than 3.7MBq
        all_data = np.transpose([acts, rts])
        all_data = all_data[all_data[:,0] < 3.7]
        acts = all_data[:,0]
        rts = all_data[:,1]

        activities = np.concatenate((activities, acts))
        rates = np.concatenate((rates, rts))

    print('Done.\n')

    # Plotting the results
    # --------------------
    print('Plotting the final results...')
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    # plt.rcParams.update({'font.size': 18})

    activities *= 1000
    plt.plot(activities, rates, '.')
    plt.plot(activities, rates*0.35, '.')
    plt.plot(activities, (rates*0.35)*0.35*0.4, '.')

    # Fit and plot the linear response curve
    fit_x = activities      # get the activity data from first dataset
    fit_y = rates           # get rates data from first dataset
    fit_arr = np.vstack((fit_x, fit_y)).T   # combine them into a single 2D array
    fit_arr = fit_arr[fit_arr[:,0] < 370]    # filter out all the data with activities greater than 10uCi
    fit_x = fit_arr[:,0]                    # separate the activity and rate data again
    fit_y = fit_arr[:,1]

    m, um, c, uc = linfit.linfit(fit_x, fit_y)
    plot_fit_x = np.linspace(0, max(activities), 100)
    plt.plot(plot_fit_x, linear(plot_fit_x, m, c), 'k--', label = 'Linear response')

    # # Non-paralysable model
    # fit_x = activities[activities < 3000]
    # fit_y = rates[:len(fit_x)]
    # popt, pcov = curve_fit(np_model, fit_x, fit_y, [2e-5, 0.01], bounds = ([0.0, 0.0], [1.0, np.inf]))
    #
    # print(popt)
    #
    # plt.plot(plot_fit_x, np_model(plot_fit_x, popt[0], popt[1]))
    #
    # # Paralysable model
    # fit_x = activities
    # fit_y = rates
    # popt, pcov = curve_fit(p_model, fit_x, fit_y, [2e-5, 0.01], bounds = ([0.0, 0.0], [1.0, np.inf]))
    #
    # print(popt)
    #
    # plt.plot(plot_fit_x, p_model(plot_fit_x, popt[0], popt[1]))

    # plt.plot(plot_fit_x, p_model(plot_fit_x, 0.017, 0.02976))

    # plt.ylim(0, 40)
    plt.xlabel(r'Activity (kBq)')
    plt.ylabel(r'OEM2 Event Rate (kHz)')
    # plt.legend()
    print('Done.\n')

    # print('Saving figure to', '{}oem2_rates_vs_activity.png...'.format(write_dir))
    # plt.savefig('{}oem2_rates_vs_activity.png'.format(write_dir))
    # print('Done.')
    plt.show()

    return

# Determining the deadtime parameter for a non-paralysable deadtime model
def determine_deadtime_parameter(rate_files, write_dir, model, branch_ratio, plot_labels, data_colors, fit_colors, rate_range = []):
    '''
    Calculates the deadtime parameter for a given system based on a
    paralysable or non-paralysable deadtime model by reading in the activity
    rates versus the measured rates and performing an appropriate linear fit.

    @params:
        rate_files              - Required : List of paths to .txt files containing singles rates. Results are plotted on same axis (Str[])
        write_dir               - Required : Directory to save figure to (Str)
        model                   - Required : Whether to use a paralysable (0) or non-paralysable (1) deadtime model (Int)
        branch_ratio            - Required : Branching ratio of the source isotope (Float)
        plot_labels             - Required : List of labels for each plot in rate_files (Str[])
        data_colors             - Required : List of colors for the plotted datapoints (Str[])
        fit_colors              - Required : List of colors for the linear fits over the datapoints (Str[])
        rate_range              - Optional : List of ranges of detector rates (in kHz) to perform the fit over (Int[][2], default = [])

    @returns:
        None
    '''
    # Function to calculate the activity as a function of time
    def A(t, A0, t_h):
        return A0 * 2**(-t/t_h)

    matplotlib.rc('font',family='Palatino')
    figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 20})

    for i, rate_file in enumerate(rate_files):
        print('Reading in rate data from {}...'.format(rate_file))

        # activities, rates = np.loadtxt(rate_file, delimiter = ',', skiprows = 1, unpack = True)
        # activities *= 1000      # convert to Bq
        data = np.loadtxt(rate_file, delimiter = ',', skiprows = 1, unpack = False)
        data[:,0] *= 1000

        if rate_range != []:
            data = data[(data[:,1] > rate_range[0]) & (data[:,1] < rate_range[1])]
        activities = data[:,0]
        rates = data[:,1]

        # Remove nan or inf values
        del_indicies = []
        for j in range(len(rates)):
            if np.isinf(activities[j]) or np.isinf(rates[j]) or np.isnan(activities[j]) or np.isnan(rates[j]):
                del_indicies.append(j)
        activities = np.delete(activities, del_indicies)
        rates = np.delete(rates, del_indicies)

        # Average over n rows
        n = 50
        activities = np.nanmean(np.pad(activities.astype(float), (0, n - activities.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
        rates = np.nanmean(np.pad(rates.astype(float), (0, n - rates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)

        # Arrays the x and y values used in the linear fit. Values are dependent on the model
        model_x = []
        model_y = []

        if model:   # if it's a non-paralysable deadtime model
            model_x = rates
            model_y = rates/activities            # generate the activity function m/A
        else:       # if it's a paralysable deadtime model
            model_x = activities/activities[0]
            model_y = -np.log(model_x) + np.log(rates)

        # mdata = np.vstack((model_x, model_y)).T
        # mdata = mdata[mdata[:,1] > 0.0035]
        # model_x = mdata[:,0]; model_y = mdata[:,1]

        # Perform a linear fit and regression fit
        _m, _c, r, p_value, std_err = stats.linregress(model_x, model_y)
        m, um, c, uc = linfit.linfit(model_x, model_y)

        tau = 1; u_tau = 0      # deadtime parameter and its uncertainty

        if model:   # if it's a non-paralysable deadtime model
            tau = -m/c                                      # deadtime parameter
            u_tau = tau*np.sqrt((um/m)**2 + (uc/c)**2)      # deadtime parameter uncertainty

            abs_eff = c/branch_ratio        # absolute efficiency

            print('Absolute efficiency: {:.2f}%'.format(100*abs_eff))

        else:       # if it's a paralysable deadtime model
            tau = -m*np.exp(-c)                             # deadtime parameter
            u_tau = np.exp(-c)*np.sqrt(um**2 + (m*uc)**2)   # deadtime parameter uncertainty

        # Plotting the results
        # --------------------
        plt.plot(model_x, model_y, '.', color = data_colors[i])     # plot the results
        if model:
            plt.xlabel('Single detector rate (Hz)')
            plt.ylabel('Activity function (m/A)')

            # Plot the linear fit and paramters
            fit_x = np.linspace(min(model_x), max(model_x))
            plt.plot(fit_x, linear(fit_x, m, c), color = fit_colors[i],
                     label = plot_labels[i] + '\n' +
                             r'm = ({:.4f} $\pm$ {:.4f})'.format(m*1e6, um*1e6) + r'$\times 10^{-6}$' + '\n'
                             r'c = ({:.3f} $\pm$ {:.3f})'.format(c*1e3, uc*1e3) + r'$\times 10^{-3}$' + '\n' +
                             #r'r$^2$ = {:.4f}'.format(r**2) + '\n' +
                             r'$\tau$ = {:.3f} $\pm$ {:.3f} $\mu$s'.format(tau*1e6, u_tau*1e6))
        else:
            plt.xlabel(r'$e^{-\lambda t}$')
            plt.ylabel(r'$\lambda$t + ln(m)')
            # Plot the linear fit and paramters
            fit_x = np.linspace(min(model_x), max(model_x))
            plt.plot(fit_x, linear(fit_x, m, c), color = fit_colors[i],
                     label = plot_labels[i] + '\n' +
                             r'm = ({:.4f} $\pm$ {:.4f})'.format(m, um) + '\n'
                             r'c = ({:.4f} $\pm$ {:.4f})'.format(c, uc) + '\n' +
                             #r'r$^2$ = {:.4f}'.format(r**2) + '\n' +
                             r'$\tau$ = {:.2f} $\pm$ {:.2f} $\mu$s'.format(tau*1e6, u_tau*1e6))

    plt.legend()

    if not model:
        write_file = write_dir + 'paralysable_deadtime_parameter_fit.png'
    else:
        write_file = write_dir + 'nonparalysable_deadtime_parameter_fit.png'

    plt.savefig(write_file)
    # plt.show()

    return

# Plotting various fractions of the LORs to show the F-optimisation procedure
def f_opt_demonstration(data_dir, f, N, plot_lines_only = True, plot_highlighted = True, plot_accepted_only = True):
    '''
    Takes in the coincidence data from a pept_coincidence_data file and produces
    a series of plots visually demonstrating the F-optimisation procedure.

    @params:
        data_dir                - Required : Path to processed coincidence data file "pept_coincidence_data" (Str)
        f                       - Required : F-parameter used when picking out LORs (Int)
        N                       - Required : Number of lines per position (Int)
        plot_lines_only         - Optional : Plot the specified N lines without highlighting (Bool, default = True)
        plot_highlighted        - Optional : Plot the specified N lines and highlight rejected lines (Bool, default = True)
        plot_accepted_only      - Optional : Plot only the accepted lines (Bool, default = True)

    @returns:
        None
    '''

    coinfile = data_dir + 'pept_coincidence_data'
    if not os.path.isfile(coinfile):
        print('File', coinfile, 'does not exist.')
        return

    print('Reading in coincidence file', coinfile, '...')
    # Read in all the coincidence data
    allcoins = pd.read_csv(coinfile,
                           names = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2'])

    print('Done.\n')

    print('Getting ctrack position and first', N, 'coincidences...')
    subcoins = allcoins.head(10000)       # get first 2000 coincidence events to get an initial position

    # Get position from ctrack
    tmp_coinfile = data_dir + 'tmp'
    subcoins.to_csv(tmp_coinfile, header = False, index = False)    # write data to csv for ctrack processing
    pos, ucert = run_ctrack(tmp_coinfile, f, N)                     # get the position and uncertainties
    if pos is None:
        print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
        return
    print(pos, ucert)
    os.remove(tmp_coinfile)                                         # remove data file

    subcoins = subcoins.head(N)       # get first N coincidences for plotting

    print('Done.\n')

    print('Calculating all LOR distances from ctrack position...')
    distances = []

    # Calculate all the distances to the position
    for index, coin in subcoins.iterrows():
        # First get the straight line equation
        m = (coin['y2']-coin['y1'])/(coin['x2']-coin['x1'])
        c = coin['y1'] - m*coin['x1']

        d = (np.abs(m*pos[0]-pos[1]+c))/(np.sqrt(m**2 + 1))     # get the distance from the line to the point

        distances.append(d)

    # plt.hist(distances, 50)
    # plt.show()

    subcoins['d'] = distances       # add the distances to the coincidence

    print('Sorting by distance...')

    subcoins.sort_values('d', inplace = True)       # sort by the distance from the location

    print('Done.\n')

    N_0 = len(subcoins.index)
    N_f = int((f/100)*N_0)
    f_subcoins = subcoins.head(N_f)
    r_subcoins = subcoins.tail(N_0 - N_f)

    print('Plotting final results')
    print('----------------------\n')
    figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 30})

    if not os.path.exists(data_dir + 'basic_plots/'):
        os.makedirs(data_dir + 'basic_plots/')
        print ('Creating directory: {}'.format(data_dir + 'basic_plots/'))

    # Plotting the coincidence lines
    # ------------------------------

    if plot_lines_only:

        num = N_0

        # Get the x,y,z coordinates as separate dataframes
        p0_xPos = subcoins.iloc[:, 1]; p0_yPos = subcoins.iloc[:, 2]; p0_zPos = subcoins.iloc[:, 3]
        p1_xPos = subcoins.iloc[:, 5]; p1_yPos = subcoins.iloc[:, 6]; p1_zPos = subcoins.iloc[:, 7]

        print('Plotting XY coincidences of', num, 'coincidences...')
        for i in range(0, num):
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=1, linestyle='-', linewidth=0.8)

        plt.xlabel('X-Position (mm)')#, fontsize=30)
        plt.ylabel('Y-Position (mm)')#, fontsize=30)

        print('Saving to file...')
        plt.savefig('{}basic_plots/xy_coincidences-N{}.png'.format(data_dir, N), bbox_inches='tight')
        print('Done.\n')
        plt.clf()


    if plot_highlighted:

        num = N_0 - N_f

        # Get the x,y,z coordinates as separate dataframes
        p0_xPos = r_subcoins.iloc[:, 1]; p0_yPos = r_subcoins.iloc[:, 2]; p0_zPos = r_subcoins.iloc[:, 3]
        p1_xPos = r_subcoins.iloc[:, 5]; p1_yPos = r_subcoins.iloc[:, 6]; p1_zPos = r_subcoins.iloc[:, 7]

        print('Plotting XY coincidences of', num, 'corrupted coincidences...')
        for i in range(0, num):
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='red', marker='o', markersize=1, linestyle='--', linewidth=0.8)


        num = N_f

        # Get the x,y,z coordinates as separate dataframes
        p0_xPos = f_subcoins.iloc[:, 1]; p0_yPos = f_subcoins.iloc[:, 2]; p0_zPos = f_subcoins.iloc[:, 3]
        p1_xPos = f_subcoins.iloc[:, 5]; p1_yPos = f_subcoins.iloc[:, 6]; p1_zPos = f_subcoins.iloc[:, 7]

        print('Plotting XY coincidences of first', num, 'coincidences...')
        for i in range(0, num):
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=1, linestyle='-', linewidth=0.8)


        plt.plot(pos[0], pos[1], 'X', color = 'cyan', markersize = 25)          # Plot the final location of particle

        # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
        plt.xlabel('X-Position (mm)')#, fontsize=30)
        plt.ylabel('Y-Position (mm)')#, fontsize=30)

        print('Saving to file...')
        plt.savefig('{}basic_plots/xy_coincidences-N{}-f{}.png'.format(data_dir, N, f), bbox_inches='tight')
        print('Done.\n')
        plt.clf()


    if plot_accepted_only:
        num = N_f

        # Get the x,y,z coordinates as separate dataframes
        p0_xPos = f_subcoins.iloc[:, 1]; p0_yPos = f_subcoins.iloc[:, 2]; p0_zPos = f_subcoins.iloc[:, 3]
        p1_xPos = f_subcoins.iloc[:, 5]; p1_yPos = f_subcoins.iloc[:, 6]; p1_zPos = f_subcoins.iloc[:, 7]

        print('Plotting XY coincidences of first', num, 'coincidences...')
        for i in range(0, num):
            plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=1, linestyle='-', linewidth=0.8)


        plt.plot(pos[0], pos[1], 'X', color = 'cyan', markersize = 20)          # Plot the final location of particle

        # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
        plt.xlabel('X-Position (mm)')#, fontsize=30)
        plt.ylabel('Y-Position (mm)')#, fontsize=30)

        print('Saving to file...')
        plt.savefig('{}basic_plots/xy_coincidences-N{}-f{}-accepted_only.png'.format(data_dir, N, f), bbox_inches='tight')
        print('Done.\n')
        plt.clf()

    return

# Calculate the detector efficiency from source moving away from the detector
def detector_efficiency(root_dir, transforms, f, N, write_data = True, read_txt = False, e_window = [], count_doubles = False):
    '''
    Takes in a series of stationary measurements with the source moving away from
    a detector module, extracts the event rate from that detector and plots it as
    a function of position.

    @params:
        root_dir        - Required : Path to experimental data (String)
        transforms      - Required : Array of transformation arrays, used for coordinate transformations (Float[][])
        f               - Required : F-parameter used to run ctrack (Int)
        N               - Required : Number of lines per position (Int)
        write_data      - Optional : Whether or not to write the plot data to a file (Bool, default = True)
        read_txt        - Optional : Boolean to either calculate the rates from the raw data or read the data from a text file (Bool, default = False)
        e_window        - Optional : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4], default = [])
        count_doubles   - Optional : Whether or not to count double scatter events in the coincidence processing (Bool, default = False)

    @returns:
        None
    '''
    module_num = 0
    A = 18e3          # activity of the source

    # Make a list of the experimental data directories
    data_dirs = []
    for subdir, dirs, files in os.walk(root_dir):
        for dir in dirs:
            if dir[0] != 'J':
                continue
            data_dirs.append('{}{}/'.format(root_dir, dir))
        break

    # Lists for plot data
    distances = []
    rates = []
    u_distances = []

    if not read_txt:
        for dir in data_dirs:

            if not os.path.exists(dir + 'AllEventsCombined.txt'):
                print('File', dir + 'AllEventsCombined.txt', 'doesn\'t exist.')
                return

            print('Running analysis on', dir, '\n')

            allevents, log_str = read_polaris_data(dir + 'AllEventsCombined.txt', transforms)

            # Takes all the double scatter events, adds their energies up and saves both events with the total energy for processing
            if count_doubles:

                allevents_numpy = allevents.to_numpy()

                print('Adding up double scatter energies...')
                log_str += 'Adding up double scatter energies...\n'

                allevents_numpy = utils.sum_double_scatter_energies(allevents_numpy)

                # print('Converting back to DataFrame')

                allevents = pd.DataFrame({'scatters': allevents_numpy[:, 0],
                                               'detector': allevents_numpy[:, 1],
                                               'energy': allevents_numpy[:, 2],
                                               'x': allevents_numpy[:, 3],
                                               'y': allevents_numpy[:, 4],
                                               'z': allevents_numpy[:, 5],
                                               'time': allevents_numpy[:, 6],})

            if e_window != []:
                print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1], e_window[2], e_window[3]))

                allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                                    ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                                    (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.

            allevents = allevents.reset_index(drop = True)            # reset the index numbering

            allevents_mod = allevents[allevents['detector'] == module_num]      # get all the events from one of the detector modules
            tot_time_mod = (allevents_mod.iloc[-1]['time'] - allevents_mod.iloc[0]['time']) * 1e-8     # get the measurement time in seconds
            tot_events = len(allevents_mod[allevents_mod['scatters'] == 1].index)           # get the total number of singles events
            if count_doubles:                                                # if counting double scatters add that to the total
                tot_events += len(allevents_mod[allevents_mod['scatters'] == 2].index)/2
            tot_rate = tot_events/tot_time_mod              # get the measured event rate

            print('Getting source position...')
            pos, ucert = run_ctrack(dir + 'pept_coincidence_data', f, N)        # get the position of the source
            if pos is None:
                print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                return
            dist = np.sqrt((pos[0] - transforms[0][3])**2 + (pos[1] - transforms[0][4])**2 + (pos[2] - transforms[0][5])**2)       # Get the distance from the source to the back of the crystals
            u_dist = np.sqrt((ucert[0])**2 + (ucert[1])**2 + (ucert[2])**2)
            print('Done\n')

            rates.append(tot_rate)
            distances.append(dist)
            u_distances.append(u_dist)

        if write_data:
            write_dir = '{}plot_data/'.format(root_dir)
            print('Writing data to', write_dir + '...')
            if not os.path.exists(write_dir):
                os.makedirs(write_dir)
                print ('Created directory: {}'.format(write_dir))

            write_filename = '{}singles_rate-vs-distance-mod{}.txt'.format(write_dir, module_num)
            np.savetxt(write_filename, np.c_[distances, rates, u_distances], fmt = '%.4f', delimiter = ',')
            print('Done\n')
    else:
        plot_data_file = '{}plot_data/singles_rate-vs-distance-mod0.txt'.format(root_dir)
        print('Reading plot data from', plot_data_file)

        plot_data = np.loadtxt(plot_data_file, dtype = np.float64, delimiter = ',')     # read in the text file
        distances = plot_data[:,0]
        rates = plot_data[:,1]
        u_distances = plot_data[:,2]

    distances = np.array(distances)
    # distances /= 1e3        # convert to meters
    rates = np.array(rates)
    u_distances = np.array(u_distances)

    # Polynomial degree 2
    # -------------------
    # func = lambda r, Ei, a, b, c: A/(4*np.pi)*Ei*(a*r**2 + b*r + c)
    # popt, pcov = curve_fit(func, distances, rates, [0.5, 1, 1, 1], bounds = ([0.0, -np.inf, -np.inf, -np.inf], [1.0, np.inf, np.inf, np.inf]))
    # Ei, a, b, c = popt[0], popt[1], popt[2], popt[3]
    #
    # print(Ei)
    #
    # x_fit = np.linspace(min(distances), max(distances), 100)
    # y_fit = func(x_fit, Ei, a, b, c)
    # plt.plot(x_fit, y_fit, 'r')


    # Polynomial degree 3
    # -------------------
    func = lambda r, Ei, a, b, c, d: A/(4*np.pi)*Ei*(a*r**3 + b*r**2 + c*r + d)
    popt, pcov = curve_fit(func, distances, rates, [0.5, 1, 1, 1, 1], bounds = ([0.0, -np.inf, -np.inf, -np.inf, -np.inf ], [1.0, np.inf, np.inf, np.inf, np.inf]))
    Ei, a, b, c, d = popt[0], popt[1], popt[2], popt[3], popt[4]

    print(Ei)

    x_fit = np.linspace(min(distances), max(distances), 100)
    y_fit = func(x_fit, Ei, a, b, c, d)
    plt.plot(x_fit, y_fit, 'r')

    # # Polynomial degree 3 fitting activity
    # # ------------------------------------
    # func = lambda r, A_, a, b, c, d: A_*(a*r**3 + b*r**2 + c*r + d)/(4*np.pi)
    # popt, pcov = curve_fit(func, distances, rates, [2000, 1, 1, 1, 1], bounds = ([0.0, -np.inf, -np.inf, -np.inf, -np.inf ], [np.inf, np.inf, np.inf, np.inf, np.inf]))
    # A_, a, b, c, d = popt[0], popt[1], popt[2], popt[3], popt[4]
    #
    # print(A_)
    # print(A_/18000 * 100)
    #
    # x_fit = np.linspace(min(distances), max(distances), 100)
    # y_fit = func(x_fit, A_, a, b, c, d)
    # plt.plot(x_fit, y_fit, 'r')

    # 1/r^2
    # -------------------
    # func = lambda r, Ei, k: A/(4*np.pi)*Ei*(k/r**2)
    # popt, pcov = curve_fit(func, distances, rates, [0.5, 1], bounds = ([0.0, -np.inf], [1.0, np.inf]))
    # Ei, k = popt[0], popt[1]
    #
    # print(Ei)
    #
    # x_fit = np.linspace(min(distances), max(distances), 100)
    # y_fit = func(x_fit, Ei, k)
    # plt.plot(x_fit, y_fit, 'r')


    # poly = np.polyfit(distances, rates, 3)
    # p = np.poly1d(poly)
    # xp = np.linspace(min(distances), max(distances), 100)
    # plt.plot(xp, p(xp))

    # A_in = (2*20**2)/(4*np.pi*(distances-10)**2) * A
    # eff = A_in/rates
    # plt.plot(distances, eff*100, '.')

    print(distances)
    plt.plot(distances, rates, '.')
    plt.xlabel('Distance from the detector (mm)')
    plt.ylabel('Filtered Singles Rate (Hz)')
    # plt.plot(distances, 4*np.pi*rates/(A*p(distances)), '.')
    plt.show()

    return

# Plotting various rate curves along with predicted curves
# TODO: Clean up this function
def coincidence_model(data_dir, A_0, e_window, coin_window, transforms, det_delays, solid_angle, branch_ratio):

    def A(t, A_0, t_h):
        return A_0 * 2**(-t/t_h)

    # Reading in raw data and processing
    # ----------------------------------
    print('Reading in file...')
    allevents = pd.read_csv(data_dir + 'AllEventsCombined.txt', sep = '	', header = None)       # read in text file as a csv with tab separation
    allevents.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']               # separate into columns and name them
    allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)                    # drop the multiple interaction scatter events
    allevents.reset_index(drop = True, inplace = True)                                          # reset the index numbering
    tot_singles_rate = len(allevents.index)
    print('Total number of single-interaction events: {:d}'.format(tot_singles_rate))
    print('Done.\n')
    # ----------------------------------

    print('Splitting dataframe and calculating efficiencies...')

    allevents_filtered = allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index)    # ...or greater than the upper peak limit.
    allevents_filtered.reset_index(drop = True, inplace = True)                                        # reset the index numbering

    S = allevents_filtered.time
    splitevents = [g.reset_index(drop=True) for i, g in allevents_filtered.groupby([((S - S[0])/(2*1e8)).astype('int')])]

    efficiencies = []
    t_elapsed = 0
    for i, df in enumerate(splitevents):
        num_events = len(df.index)        # first count the single interaction events
        df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8
        # print('Time =', df_time, 'seconds')
        # print('Solid angle = ', solid_angle)

        t_elapsed += df_time
        activity = A(t_elapsed, A_0, 4080)
        # print('Activity = ', activity)

        rate = (num_events/df_time)
        # print('Event rate =', rate)
        efficiency = rate/(branch_ratio*activity*solid_angle)
        # print('Efficiency =', efficiency)
        efficiencies.append(efficiency)
        # print()

    N, minmax, peak_efficiency, var, skew, k = stats.describe(efficiencies)
    print('Intrinsic efficiency = {:.3f} +- {:.3f} %\n'.format(peak_efficiency*100, np.sqrt(var/N)*100))

    # Running coincidence counting
    # ----------------------------
    print('Running coincidence counting...')
    allevents_, allcoins = process_coincidences(data_dir, e_window, coin_window, transforms, det_delays = det_delays, allevents = allevents.copy(), log_str = '')
    tot_coins = len(allcoins.index)
    print('Total number of coincidences: {:.0f}'.format(tot_coins))
    print('Done.\n')
    # ----------------------------

    allevents.drop(allevents[allevents.detector != 0].index, inplace = True)                  # drop the events not from module 0
    allevents.reset_index(drop = True, inplace = True)                                        # reset the index numbering

    tot_singles_rate_mod0 = len(allevents.index)

    print('Splitting dataframe and counting unfiltered singles event rates from module 0...')
    S = allevents.time
    splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(120*1e8)).astype('int')])]

    unfiltered_rates = []
    for i, df in enumerate(splitevents):
        num_events = len(df.index)        # first count the single interaction events
        df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8
        unfiltered_rate = (num_events/df_time)
        unfiltered_rates.append(unfiltered_rate)

    unfiltered_rates = np.array(unfiltered_rates)

    print('Total number of single-interaction events in module 0: {:d}'.format(tot_singles_rate_mod0))
    print('Done.\n')

    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    allevents.reset_index(drop = True, inplace = True)                                        # reset the index numbering

    tot_filtered_singles_rate_mod0 = len(allevents.index)
    print('Total number of filtered single-interaction events in module 0: {:d} ({:.1f}%)\n'.format(tot_filtered_singles_rate_mod0, tot_filtered_singles_rate_mod0/tot_singles_rate_mod0*100))

    tot_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8         # get the total time of the measurement
    print('Total measurement time = {:.2f} minutes.\n'.format(tot_time/60))

    print('Splitting dataframe and counting event and coincidence rates...')
    S = allevents.time
    splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(120*1e8)).astype('int')])]

    S_c = allcoins.time_1
    splitevents_c = [g.reset_index(drop=True) for i, g in allcoins.groupby([((S_c - S_c[0])/(120*1e6)).astype('int')])]

    rates = []
    coin_rates = []
    activities = []

    check_num = 0
    t_elapsed = 0       # time elapsed for calculating the activity in seconds
    for i, df in enumerate(splitevents):
        num_events = len(df.index)        # first count the single interaction events

        check_num += num_events

        df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8
        t_elapsed += df_time

        activity = A(t_elapsed, A_0, 4080)
        activities.append(activity)

        rate = (num_events/df_time)
        rates.append(rate)

    for i, df in enumerate(splitevents_c):
        num_events = len(df.index)        # first count the single interaction events
        df_time = (df.iloc[-1]['time_1'] - df.iloc[0]['time_1']) * 1e-6
        coin_rate = (num_events/df_time)
        coin_rates.append(coin_rate)

    print('Done.\n')
    rates = np.array(rates)
    coin_rates = np.array(coin_rates)
    activities = np.array(activities)

    expected_coin_rates = rates*peak_efficiency

    # np.savetxt('/Users/nicholas/Downloads/singles_event_rate.txt', np.c_[activities, unfiltered_rates/2, rates/2], fmt = '%.4f', delimiter = ',',
               # header = 'activity (Hz), unfiltered singles rate (Hz), filtered singles rate (Hz)')

    # np.savetxt('/Users/nicholas/Downloads/coincidence_event_rate.txt', np.c_[activities, expected_coin_rates, coin_rates], fmt = '%.4f', delimiter = ',',
               # header = 'activity (Hz), expected coincidence rate (Hz), measured coincidence rate (Hz)')

    # Plotting the results
    # --------------------
    # plt.plot(activities/1000, unfiltered_rates, '.', label = 'Unfiltered')
    # plt.plot(activities/1000, rates, '.', label = 'Energy Filtered')
    plt.plot(activities/1000, expected_coin_rates, '.', label = 'Expected')
    plt.plot(activities/1000, coin_rates, '.', label = 'Measured')
    plt.xlabel('Activity (kBq)')
    plt.ylabel('Coincidence rate (Hz)')
    # plt.ylabel('Single Module Single Interaction Event Rate (Hz)')
    # plt.plot(activities, efficiencies, '.')
    plt.legend()
    plt.show()

    return

# Loop through the recon_plotter.py fit results on the Compton-Camera images and plot difference as a function of percentage
def plot_fit_vs_dca_percentage(root_dir):
    '''
    Takes in a series of stationary measurements with the source moving away from
    a detector module, extracts the event rate from that detector and plots it as
    a function of position.

    @params:
        root_dir                 - Required : Path to experimental data (String)

    @returns:
        None
    '''

    percentages = []             # list to hold dca percentages
    gaus_differences = []        # list to hold gaussian fit differences
    estim_differences = []       # list to hold estimated position differences
    fwhms = []                   # list to hold the full width at half maximum for each fit

    # Make a list of the reconstruction data directories
    data_dirs = []
    for subdir, dirs, files in os.walk(root_dir):
        for dir in dirs:
            data_dirs.append('{}{}/'.format(root_dir, dir))

            p = int(re.search(r'perc_(\d+)', dir).group(1))     # get the dca percentage from the directory name
            percentages.append(p)
        break

    # Loop through reconstructions and extract the fit results
    for data_dir in data_dirs:
        with open('{}plots/Distance_Calculations_FWHM.txt'.format(data_dir)) as f:
            fit_results = f.read()      # read whole file into a string
            print(fit_results)
            g_diff = float(re.search(r'Magnitude  \(Gauss - Actual\):   (\d+\.\d+)', fit_results).group(1))
            e_diff = float(re.search(r'Magnitude  \(Estim - Actual\):   (\d+\.\d+)', fit_results).group(1))
            fwhm = float(re.search(r'Average FWHM:                  (\d+\.\d+)', fit_results).group(1))
            gaus_differences.append(g_diff)
            estim_differences.append(e_diff)
            fwhms.append(fwhm)

    fwhms = np.array(fwhms)
    fwhms = fwhms/2

    plt.plot(percentages, estim_differences, '.', label = 'Estimated - Actual', markersize = 10)
    plt.errorbar(percentages, gaus_differences, yerr = fwhms, fmt = 's', ecolor = 'black', mfc = 'red', label = 'Gaussian Fit - Actual')
    plt.legend()
    plt.show()

    return

# Loop through the fit results of the circular tracks and plot the amplitude/speed
def tracking_measured_vs_known(data_dirs, Ns, fopt, plot_option = 0, select_points = ()):
    '''
    Takes in a series of circular tracks, loops through all the fit results and
    plots them as a function of various interesting variables, like radius, time,
    etc.

    @params:
        data_dirs           - Required : List of paths to tracking_fit_results-N{}.log data (String[])
        Ns                  - Required : List of N values used for each fit (Int[])
        fopt                - Required : The Fopt value used to produce the fits
        plot_option         - Optional : Select what to plot. 0 = amplitude, 1 = speed (Int, default = 0)
        select_points       - Optional : Select a subset of the datasets to plot (Int[], default = ())

    @returns:
        None
    '''

    # Plot fitted speed versus known speed

    radius = 0
    known_speeds = []
    known_radii = []
    fit_speeds = []
    u_fit_speeds = []
    As_avg = []
    u_As_avg = []
    adjusted_radius = radius
    # parameter = []

    print('Extracting plot results...')
    for i, dir in enumerate(data_dirs):

        known_speeds.append(float(re.search(r'\_(\d+|\d+\.\d+)mms', dir).group(1)))     # get the known speed from the directory name
        known_radii.append(float(re.search(r'circle\_(\d+)mm', dir).group(1)))          # get the known radius from the directory name

        fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[i], fopt)

        with open(fit_results_filename, 'r') as f:
            fit_results = f.read()      # read all the results into a string

            # print(fit_results)
            #
            As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
            u_As = re.findall(r'A\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
            # # print(As)
            omegas = re.findall(r'omega\s+=\s+(\d+\.\d+)', fit_results)   # find all the angular frequency fits
            u_omegas = re.findall(r'omega\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
            # # print(omegas)
            #
            Ax = np.abs(float(As[0]))      # get the amplitude
            u_Ax = float(u_As[0])          # get the amplitude uncertainty
            omega_x = float(omegas[0])     # get the angular frequency
            u_omega_x = float(u_omegas[0]) # get the angular frequency uncertainty
            speed_x = Ax * omega_x         # calculate the speed
            # print(speed_x)

            Ay = np.abs(float(As[1]))      # get the amplitude
            u_Ay = float(u_As[1])          # get the amplitude uncertainty
            omega_y = float(omegas[1])     # get the angular frequency
            u_omega_y = float(u_omegas[1]) # get the angular frequency uncertainty
            speed_y = Ay * omega_y         # calculate the speed
            # print(speed_y)

            A_avg = (Ax + Ay)/2
            u_A_avg = np.sqrt(u_Ax**2 + u_Ay**2)
            omega_avg = (omega_x + omega_y)/2
            u_omega_avg = np.sqrt(u_omega_x**2 + u_omega_y**2)

            fit_speed = A_avg * omega_avg
            u_fit_speed = fit_speed*np.sqrt((u_A_avg/A_avg)**2 + (u_omega_avg/omega_avg)**2)

            # fit_speeds.append(np.sqrt(speed_x**2 + speed_y**2))
            fit_speeds.append(fit_speed)
            u_fit_speeds.append(u_fit_speed)

            As_avg.append(A_avg)
            u_As_avg.append(u_A_avg)
            # print(fit_speeds[i])
            # print()
            # parameter.append((Ax + Ay)/2)

            if i == 0:
                radius = known_radii[i]
                adjusted_radius = A_avg

    print('Done.\n')

    known_speeds = np.array(known_speeds)
    known_radii = np.array(known_radii)
    fit_speeds = np.array(fit_speeds)
    u_fit_speeds = np.array(u_fit_speeds)

    adjusted_speeds = (adjusted_radius * known_speeds)/(2*radius)

    # Get a subset if required
    if select_points != ():
        known_speeds = known_speeds[select_points[0]:select_points[1]]
        known_radii = known_radii[select_points[0]:select_points[1]]
        fit_speeds = fit_speeds[select_points[0]:select_points[1]]
        u_fit_speeds = u_fit_speeds[select_points[0]:select_points[1]]
        adjusted_speeds = adjusted_speeds[select_points[0]:select_points[1]]


    # Plot the results
    # ----------------
    # Adjusted speed versus average amplitude
    if plot_option == 0:
        plt.errorbar(adjusted_speeds, As_avg, yerr = u_As_avg, fmt = 'none', ecolor = 'k', capsize = 2)
        plt.show()

    # Adjusted speed versus average fitted speed
    if plot_option == 1:
        # Perform a linear fit
        m, um, c, uc = linfit.linfit(adjusted_speeds, fit_speeds)
        fit_x = np.linspace(min(adjusted_speeds), max(adjusted_speeds), 100)
        plt.plot(fit_x, linear(fit_x, m, c), 'red', label = 'm = {:.3f} +- {:.3f}\nc = {:.3f} +- {:.3f}'.format(m, um, c, uc))

        plt.errorbar(adjusted_speeds, fit_speeds, yerr = u_fit_speeds, fmt = 'none', ecolor = 'k', capsize = 2)
        plt.legend()
        plt.show()

    # Adjusted speed versus residuals of average amplitude - adjusted radius
    if plot_option == 2:
        residuals = As_avg - adjusted_radius
        plt.plot(adjusted_speeds, residuals, '.b')            # plot residuals
        plt.plot([min(adjusted_speeds), max(adjusted_speeds)], [0, 0], 'k')     # plot zero line
        plt.show()

    # Adjusted speed versus residuals of adjusted speed - fitted speed
    if plot_option == 3:
        residuals = adjusted_speeds - fit_speeds
        plt.plot(adjusted_speeds, residuals, '.b')            # plot residuals
        plt.plot([min(adjusted_speeds), max(adjusted_speeds)], [0, 0], 'k')     # plot zero line
        plt.show()

    # Known radii versus average amplitude
    if plot_option == 4:
        plt.plot(known_radii, As_avg, '.')
        plt.show()

    # Known radii versus residuals of known radii - average fitted amplitude as a percentage
    if plot_option == 5:
        residuals = known_radii - As_avg
        plt.plot(known_radii, (residuals/known_radii)*100, '.b')            # plot residuals
        plt.plot([min(known_radii), max(known_radii)], [0, 0], 'k')     # plot zero line
        plt.show()

    return

# Loop through the fit results of the circular tracks and plot the measured versus the known
def tracking_measured_vs_known2(data_dirs, Ns, fopt, fixed, plot_option = 0, select_points = ()):
    '''
    Takes in a series of circular tracks, loops through all the fit results and
    plots them as a function of various interesting variables, like radius, time,
    etc.

    @params:
        data_dirs           - Required : List of paths to tracking_fit_results-N{}.log data (String[][])
        Ns                  - Required : List of N values used for each fit (Int[])
        fopt                - Required : The Fopt value used to produce the fits
        fixed               - Required : Whether the radius or the speed is fixed. Options are 'radius' or 'speed' (String)
        plot_option         - Optional : Select what to plot.
        select_points       - Optional : Select a subset of the datasets to plot (Int[], default = ())

    @returns:
        None
    '''
    plt.rcParams.update({'font.size': 15})

    if fixed == 'radius':

        radius = float(re.search(r'circle_(\d+|\d+\.\d+)mm', data_dirs[0]).group(1))          # get the known radius from the directory name

        known_speeds = []
        fit_speeds = []
        u_fit_speeds = []

        # Calculate the speed from the PEPT positions
        # -------------------------------------------
        print('Extracting plot results...')
        for i, dir in enumerate(data_dirs):
            known_speeds.append(float(re.search(r'\_(\d+|\d+\.\d+)mms', dir).group(1))/2)     # get the known speed from the directory name

            # First extract the fitted amplitude
            fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[i], fopt)
            with open(fit_results_filename, 'r') as f:
                fit_results = f.read()      # read all the results into a string
                As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
                Ax = np.abs(float(As[0]))      # get the x amplitude
                Ay = np.abs(float(As[1]))      # get the y amplitude
                adjusted_radius = (Ax + Ay)/2
                adjusted_radius = np.round(adjusted_radius * 2) / 2

            # Open file and read in all information
            data_file = dir + 'pept_coincidence_data.a'
            pt_data = []
            with open(data_file) as f:
                header = f.readline()       # ignore blank first row

                for line in f:
                    line = line.strip()
                    columns = line.split()

                    t = float(columns[0])/1e3       # get time in seconds
                    x = float(columns[1])
                    y = float(columns[2])
                    z = float(columns[3])

                    pt_data.append(np.array([t,x,y,z]))

            # Convert python lists to numpy arrays
            pt_data = np.array(pt_data)

            vt_data = calculate_velocities(pt_data)
            pt_data = pt_data[5:-5]

            vel_mags = np.sqrt(vt_data[:,1]**2 + vt_data[:,2]**2 + vt_data[:,3]**2)
            avg_speed = np.mean(vel_mags)
            u_avg_speed = np.sqrt(np.var(vel_mags))

            # print('v = {:.2f} +- {:.2f} mm/s'.format(avg_speed, u_avg_speed))

            fit_speeds.append(avg_speed)
            u_fit_speeds.append(u_avg_speed)


        # Calculate the speed from the fitted parameters
        # ----------------------------------------------
        # print('Extracting plot results...')
        # for i, dir in enumerate(data_dirs):
        #
        #     known_speeds.append(float(re.search(r'\_(\d+|\d+\.\d+)mms', dir).group(1))/2)     # get the known speed from the directory name
        #
        #     fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[i], fopt)
        #
        #     with open(fit_results_filename, 'r') as f:
        #         fit_results = f.read()      # read all the results into a string
        #
        #         As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
        #         u_As = re.findall(r'A\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
        #
        #         omegas = re.findall(r'omega\s+=\s+(\d+\.\d+)', fit_results)   # find all the angular frequency fits
        #         u_omegas = re.findall(r'omega\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
        #
        #         Ax = np.abs(float(As[0]))      # get the amplitude
        #         u_Ax = float(u_As[0])          # get the amplitude uncertainty
        #         omega_x = float(omegas[0])     # get the angular frequency
        #         u_omega_x = float(u_omegas[0]) # get the angular frequency uncertainty
        #         # speed_x = Ax * omega_x         # calculate the speed
        #
        #         Ay = np.abs(float(As[1]))      # get the amplitude
        #         u_Ay = float(u_As[1])          # get the amplitude uncertainty
        #         omega_y = float(omegas[1])     # get the angular frequency
        #         u_omega_y = float(u_omegas[1]) # get the angular frequency uncertainty
        #         # speed_y = Ay * omega_y         # calculate the speed
        #
        #         A_avg = (Ax + Ay)/2
        #         u_A_avg = np.sqrt(u_Ax**2 + u_Ay**2)
        #         omega_avg = (omega_x + omega_y)/2
        #         u_omega_avg = np.sqrt(u_omega_x**2 + u_omega_y**2)
        #
        #         fit_speed = A_avg * omega_avg
        #         u_fit_speed = fit_speed*np.sqrt((u_A_avg/A_avg)**2 + (u_omega_avg/omega_avg)**2)
        #
        #         # fit_speeds.append(np.sqrt(speed_x**2 + speed_y**2))
        #         fit_speeds.append(fit_speed)
        #         u_fit_speeds.append(u_fit_speed)

        print('Done.\n')

        known_speeds = np.array(known_speeds)
        fit_speeds = np.array(fit_speeds)
        u_fit_speeds = np.array(u_fit_speeds)

        # known_speeds = (adjusted_radius * known_speeds)/radius

        # Get a subset if required
        if select_points != ():
            known_speeds = known_speeds[select_points[0]:select_points[1]]
            fit_speeds = fit_speeds[select_points[0]:select_points[1]]
            u_fit_speeds = u_fit_speeds[select_points[0]:select_points[1]]

        # Plot the results
        # ----------------

        # Adjusted speed versus average fitted speed
        if plot_option == 0:
            # Perform a linear fit
            m, um, c, uc = linfit.linfit(known_speeds, fit_speeds)
            fit_x = np.linspace(min(known_speeds), max(known_speeds), 100)
            plt.plot(fit_x, linear(fit_x, m, c), 'red', label = r'm = {:.3f} $\pm$ {:.3f}'.format(m, um) + '\n' +
                                                                r'c = {:.3f} $\pm$ {:.3f}'.format(c, uc))

            plt.errorbar(known_speeds, fit_speeds, yerr = u_fit_speeds, fmt = 'bs', ecolor = 'k', capsize = 4)
            plt.xlabel('Known Speeds (mm/s)')
            plt.ylabel('Fitted Speeds (mm/s)')

            plt.legend()
        plt.show()


    if fixed == 'speed':
        known_radii = []
        fit_radii = []
        u_fit_radii = []

        print('Extracting plot results...')
        for i, dir in enumerate(data_dirs):
            known_radii.append(float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)))          # get the known radius from the directory name

            fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[i], fopt)

            with open(fit_results_filename, 'r') as f:
                fit_results = f.read()      # read all the results into a string
                As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
                u_As = re.findall(r'A\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
                Ax = np.abs(float(As[0]))      # get the x amplitude
                u_Ax = float(u_As[0])          # get the amplitude uncertainty
                Ay = np.abs(float(As[1]))      # get the y amplitude
                u_Ay = float(u_As[1])          # get the amplitude uncertainty

                A_avg = (Ax + Ay)/2
                u_A_avg = np.sqrt(u_Ax**2 + u_Ay**2)

                fit_radii.append(A_avg)
                u_fit_radii.append(u_A_avg)

        print('Done.\n')

        known_radii = np.array(known_radii)
        fit_radii = np.array(fit_radii)
        u_fit_radii = np.array(u_fit_radii)

        known_radii = np.round(fit_radii * 2) / 2       # correcting for misidentified radii

        # adjusted_speeds = (adjusted_radius * known_speeds)/(2*radius)

        # Get a subset if required
        if select_points != ():
            known_speeds = known_speeds[select_points[0]:select_points[1]]
            known_radii = known_radii[select_points[0]:select_points[1]]
            fit_speeds = fit_speeds[select_points[0]:select_points[1]]
            u_fit_speeds = u_fit_speeds[select_points[0]:select_points[1]]
            adjusted_speeds = adjusted_speeds[select_points[0]:select_points[1]]

        # Plot the results
        # ----------------
        # Adjusted speed versus average fitted speed
        if plot_option == 0:
            # Perform a linear fit
            m, um, c, uc = linfit.linfit(known_radii, fit_radii)
            fit_x = np.linspace(min(known_radii), max(known_radii), 100)
            plt.plot(fit_x, linear(fit_x, m, c)
                     ,label = r'm = {:.3f} $\pm$ {:.3f}'.format(m, um) + '\n' +
                      r'c = {:.3f} $\pm$ {:.3f}'.format(c, uc)
                     )

            plt.errorbar(known_radii, fit_radii, yerr = u_fit_radii, fmt = 's', ecolor = 'k', capsize = 2)
            plt.xlabel('Known Radius (mm)')
            plt.ylabel('PEPT Radius (mm)')
            plt.legend()
            plt.show()


    return

# Loop through the fit results of the circular tracks and plot the measured versus the known
def tracking_measured_vs_known_comparisons(all_data_dirs, write_dir, known_radii_dict, labels, colors, Ns, fopt, fixed, plot_option = 0, select_points = ()):
    '''
    Takes in a series of circular tracks, loops through all the fit results and
    plots them as a function of various interesting variables, like radius, time,
    etc. data_dirs is expected as a 2D array and will plot each row as a separate
    straight line plot

    @params:
        all_data_dirs       - Required : List of paths to tracking_fit_results-N{}.log data (String[][])
        write_dir           - Required : Directory to write the final plot to (String)
        known_radii_dict    - Required : Dictionary of the known radii of each run (Dictionary)
        labels              - Required : Dictionary of labels for each row in data_dirs (Dictionary)
        colors              - Required : Dictionary of all the colors for plotting of each speed (Dictionary)
        Ns                  - Required : List of N values used for each fit (Int[])
        fopt                - Required : The Fopt value used to produce the fits
        fixed               - Required : Whether the radius or the speed is fixed. Options are 'radius' or 'speed' (String)
        plot_option         - Optional : Select what to plot.
        select_points       - Optional : Select a subset of the datasets to plot (Int[], default = ())

    @returns:
        None
    '''
    plt.rcParams.update({'font.size': 20})

    if fixed == 'radius':
        print('Running comparison plots for fixed radii')
        print('-----------------------------------------')

        all_fit_speeds = []
        all_u_fit_speeds = []
        all_known_speeds = []
        all_plot_colors = []
        all_plot_labels = []

        # for d in all_data_dirs:
            # print(d)

        print('Extracting plot results...')
        for i, data_dirs in enumerate(all_data_dirs):
            fit_speeds = []
            u_fit_speeds = []
            known_speeds = []
            plot_colors = []
            plot_labels = []

            for j, dir in enumerate(data_dirs):
                known_speeds.append(float(re.search(r'\_(\d+|\d+\.\d+)mms', dir).group(1))/2)     # get the known speed from the directory name

                # recorded_speed = float(re.search(r'\_(\d+|\d+\.\d+)mms', dir).group(1))
                # print(recorded_speed)
                radius = re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)                # get the radius from the directory name
                # print(radius)
                # adjusted_radius = known_radii_dict[radius]
                # print(adjusted_radius)

                # adjusted_speed = (adjusted_radius/float(radius)) * recorded_speed/2
                # print(adjusted_speed)
                # known_speeds.append(adjusted_speed)

                plot_colors.append(colors[radius])                         # get the appropriate color to plot
                plot_labels.append(labels[radius])

                # Get velocities using fitted parameters
                # --------------------------------------
                fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[i], fopt)
                with open(fit_results_filename, 'r') as f:
                    fit_results = f.read()      # read all the results into a string
                    # As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
                    # Ax = np.abs(float(As[0]))      # get the x amplitude
                    # Ay = np.abs(float(As[1]))      # get the y amplitude
                    # fit_radius = (Ax + Ay)/2
                    # # adjusted_radius = np.round(adjusted_radius * 2) / 2

                    As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
                    u_As = re.findall(r'A\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits

                    omegas = re.findall(r'omega\s+=\s+(\d+\.\d+)', fit_results)   # find all the angular frequency fits
                    u_omegas = re.findall(r'omega\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits

                    Ax = np.abs(float(As[0]))      # get the amplitude
                    u_Ax = float(u_As[0])          # get the amplitude uncertainty
                    omega_x = float(omegas[0])     # get the angular frequency
                    u_omega_x = float(u_omegas[0]) # get the angular frequency uncertainty
                    speed_x = Ax * omega_x         # calculate the speed
                    # speed_x = float(radius) * omega_x     # calculate the speed
                    u_speed_x = speed_x * np.sqrt((u_Ax/Ax)**2 + (u_omega_x/omega_x)**2)

                    Ay = np.abs(float(As[1]))      # get the amplitude
                    u_Ay = float(u_As[1])          # get the amplitude uncertainty
                    omega_y = float(omegas[1])     # get the angular frequency
                    u_omega_y = float(u_omegas[1]) # get the angular frequency uncertainty
                    speed_y = Ay * omega_y         # calculate the speed
                    # speed_y = float(radius) * omega_y     # calculate the speed
                    u_speed_y = speed_y * np.sqrt((u_Ay/Ay)**2 + (u_omega_y/omega_y)**2)

                    A_avg = (Ax + Ay)/2
                    # u_A_avg = np.sqrt(u_Ax**2 + u_Ay**2)
                    # omega_avg = (omega_x + omega_y)/2
                    # u_omega_avg = np.sqrt(u_omega_x**2 + u_omega_y**2)
                    #
                    # fit_speed = A_avg * omega_avg
                    # u_fit_speed = fit_speed*np.sqrt((u_A_avg/A_avg)**2 + (u_omega_avg/omega_avg)**2

                    # fit_speeds.append(np.sqrt(speed_x**2 + speed_y**2))
                    speed = (speed_x + speed_y)/2
                    u_speed = np.sqrt(u_speed_x**2 + u_speed_y**2)

                    speed *= float(radius)/A_avg

                    fit_speeds.append(speed)
                    u_fit_speeds.append(u_speed)


                # Get velocities using PEPT
                # -------------------------
                # # Open file and read in all information
                # data_file = dir + 'pept_coincidence_data.a'
                # pt_data = []
                # with open(data_file) as f:
                #     header = f.readline()       # ignore blank first row
                #
                #     for line in f:
                #         line = line.strip()
                #         columns = line.split()
                #
                #         t = float(columns[0])/1e3       # get time in seconds
                #         x = float(columns[1])
                #         y = float(columns[2])
                #         z = float(columns[3])
                #
                #         pt_data.append(np.array([t,x,y,z]))
                #
                # # Convert python lists to numpy arrays
                # pt_data = np.array(pt_data)
                #
                # # Truncate array to remove stationary points
                # l = len(pt_data)
                # pt_data = pt_data[int(l*0.15):int(l*0.95)]
                #
                # num_pnts = len(pt_data)
                # pt_data = pt_data[int(num_pnts/10):-int(num_pnts/10)]        # to remove the start and end where it's stationary
                #
                # vt_data = calculate_velocities(pt_data)
                # # pt_data = pt_data[5:-5]
                #
                # vel_mags = np.sqrt(vt_data[:,1]**2 + vt_data[:,2]**2 + vt_data[:,3]**2)
                # avg_speed = np.mean(vel_mags)
                # u_avg_speed = np.sqrt(np.var(vel_mags))#/len(vel_mags))
                #
                # # if radius == '1.5':
                #     # avg_speed /= 2
                #
                # # print('v = {:.2f} +- {:.2f} mm/s'.format(avg_speed, u_avg_speed))
                #
                # fit_speeds.append(avg_speed)
                # u_fit_speeds.append(u_avg_speed)


            # print(known_speeds)

            all_fit_speeds.append(fit_speeds)
            all_u_fit_speeds.append(u_fit_speeds)
            all_known_speeds.append(known_speeds)
            all_plot_colors.append(plot_colors)
            all_plot_labels.append(plot_labels)

            # Get a subset if required
            if select_points != ():
                known_speeds = known_speeds[select_points[0]:select_points[1]]
                known_radii = known_radii[select_points[0]:select_points[1]]
                fit_speeds = fit_speeds[select_points[0]:select_points[1]]
                u_fit_speeds = u_fit_speeds[select_points[0]:select_points[1]]
                adjusted_speeds = adjusted_speeds[select_points[0]:select_points[1]]

        print('Done.\n')

        # all_fit_radii[1], all_fit_radii[2] = all_fit_radii[2], all_fit_radii[1]
        # all_u_fit_radii[1], all_u_fit_radii[2] = all_u_fit_radii[2], all_u_fit_radii[1]
        # all_known_radii[1], all_known_radii[2] = all_known_radii[2], all_known_radii[1]
        # all_plot_colors[1], all_plot_colors[2] = all_plot_colors[2], all_plot_colors[1]

        # Plot the results
        # ----------------
        print('Plotting the results...')

        fig, axs = plt.subplots(nrows=1, ncols=len(all_fit_speeds), figsize=(16, 9), dpi = 500) #, gridspec_kw = {'width_ratios':[1, 1]})

        for i in range(len(all_fit_speeds)):
            known_speeds = all_known_speeds[i]
            fit_speeds = all_fit_speeds[i]
            u_fit_speeds = all_u_fit_speeds[i]
            plot_colors = all_plot_colors[i]
            plot_labels = all_plot_labels[i]

            sortdata = np.transpose([known_speeds, fit_speeds, u_fit_speeds, plot_colors, plot_labels])
            sortdata = sorted(sortdata, key = lambda x: float(x[4][:-3]))
            known_speeds = [float(x[0]) for x in sortdata]
            fit_speeds = [float(x[1]) for x in sortdata]
            u_fit_speeds = [float(x[2]) for x in sortdata]
            plot_colors = [x[3] for x in sortdata]
            plot_labels = [x[4] for x in sortdata]

            for j in range(len(fit_speeds)):
                # print(j)
                nsigma = 1          # scale for the uncertainty
                known_speeds[j] = known_speeds[j]*(1+j*0.03)
                axs[i].errorbar(known_speeds[j], fit_speeds[j], yerr = u_fit_speeds[j]*nsigma, fmt = 's', color = plot_colors[j], ecolor = 'k', capsize = 2, label = plot_labels[j])

            # print()
            axs[i].plot([0.95*min(known_speeds), 1.05*max(known_speeds)], [known_speeds[0], known_speeds[0]], '--k', label = 'Known\nSpeed')
            axs[i].plot([0.95*min(known_speeds), 1.05*max(known_speeds)], [known_speeds[0]+1, known_speeds[0]+1], '--r', label = 'Known\nSpeed\nUncertainty')
            axs[i].plot([0.95*min(known_speeds), 1.05*max(known_speeds)], [known_speeds[0]-1, known_speeds[0]-1], '--r')

            axs[i].set_xlim(0.95*min(known_speeds), 1.05*max(known_speeds))
            axs[i].set_xticks([(max(known_speeds) + min(known_speeds)) / 2])
            axs[i].set_xticklabels([known_speeds[0]])

            # axs[i].set_ylim(0.85*min(fit_speeds), 1.15*max(fit_speeds))
            # if i == 8:
            #     axs[i].set_ylim(0.6*min(fit_speeds), 1.15*max(fit_speeds))

            if (max(fit_speeds) < known_speeds[0] + 2 and min(fit_speeds) > known_speeds[0] - 2):
                axs[i].set_ylim(known_speeds[0] - 2, known_speeds[0] + 2)
            else:
                axs[i].set_ylim(min(min(fit_speeds) - 0.5, known_speeds[0] - 2), max(max(fit_speeds) + 0.5,known_speeds[0] + 2))


        # fig.tight_layout()
        plt.subplots_adjust(wspace = 1.1, right = 0.82)

        handles, labels = axs[1].get_legend_handles_labels()
        fig.legend(handles, labels, loc='center right')

        fig.text(0.5, 0.04, 'Known Speed (mm/s)', ha='center')                       # xlabel
        fig.text(0.04, 0.5, 'PEPT Speed (mm/s)', va='center', rotation='vertical')   # ylabel

        print('Done.\n')

        write_file = write_dir + 'fitted_speeds-all_radii.png'
        print('Writing results to', write_file)
        plt.savefig(write_file)
        # plt.show()

    if fixed == 'speed':
        print('Running comparison plots for fixed speeds')
        print('-----------------------------------------')

        all_fit_radii = []
        all_u_fit_radii = []
        all_known_radii = []
        all_plot_colors = []
        all_plot_labels = []

        print('Extracting plot results...')
        for i, data_dirs in enumerate(all_data_dirs):
            known_radii = []
            fit_radii = []
            u_fit_radii = []
            plot_colors = []
            plot_labels = []

            for j, dir in enumerate(data_dirs):
                known_radius = re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)          # get the known radius from the directory name
                known_radii.append(known_radii_dict[known_radius])

                speed = re.search(r'_(\d+|\d+\.\d+)mms', dir).group(1)          # get the speed from the directory name
                plot_colors.append(colors[speed])                         # get the appropriate color to plot
                plot_labels.append(labels[speed])                         # get the appropriate color to plot

                fit_results_filename = '{}tracking_plots/tracking_fit_results-N{}-F{}.log'.format(dir, Ns[j], fopt)

                with open(fit_results_filename, 'r') as f:
                    fit_results = f.read()      # read all the results into a string
                    As = re.findall(r'A\s+=\s+([-+]?\d+\.\d+)', fit_results)   # find all the amplitude fits
                    u_As = re.findall(r'A\s+=\s+[-+]?\d+\.\d+\s+\+\-\s(\d+\.\d+)', fit_results)   # find all the amplitude uncertainty fits
                    Ax = np.abs(float(As[0]))      # get the x amplitude
                    u_Ax = float(u_As[0])          # get the amplitude uncertainty
                    Ay = np.abs(float(As[1]))      # get the y amplitude
                    u_Ay = float(u_As[1])          # get the amplitude uncertainty

                    A_avg = (Ax + Ay)/2
                    u_A_avg = np.sqrt(u_Ax**2 + u_Ay**2)

                    fit_radii.append(A_avg)
                    u_fit_radii.append(u_A_avg)


            all_fit_radii.append(fit_radii)
            all_u_fit_radii.append(u_fit_radii)
            all_known_radii.append(known_radii)
            all_plot_colors.append(plot_colors)
            all_plot_labels.append(plot_labels)

            # Get a subset if required
            if select_points != ():
                known_speeds = known_speeds[select_points[0]:select_points[1]]
                known_radii = known_radii[select_points[0]:select_points[1]]
                fit_speeds = fit_speeds[select_points[0]:select_points[1]]
                u_fit_speeds = u_fit_speeds[select_points[0]:select_points[1]]
                adjusted_speeds = adjusted_speeds[select_points[0]:select_points[1]]

        if '191115' in all_data_dirs[0][0]:
            all_fit_radii[1], all_fit_radii[2] = all_fit_radii[2], all_fit_radii[1]
            all_u_fit_radii[1], all_u_fit_radii[2] = all_u_fit_radii[2], all_u_fit_radii[1]
            all_known_radii[1], all_known_radii[2] = all_known_radii[2], all_known_radii[1]
            all_plot_colors[1], all_plot_colors[2] = all_plot_colors[2], all_plot_colors[1]
            all_plot_labels[1], all_plot_labels[2] = all_plot_labels[2], all_plot_labels[1]

        print('Done.\n')

        # Plot the results
        # ----------------
        print('Plotting the results...')

        fig, axs = plt.subplots(nrows=1, ncols=len(all_fit_radii), figsize=(16, 9), dpi = 500) #, gridspec_kw = {'width_ratios':[1, 1]})

        for i in range(len(all_fit_radii)):
            known_radii = all_known_radii[i]
            fit_radii = all_fit_radii[i]
            u_fit_radii = all_u_fit_radii[i]
            plot_colors = all_plot_colors[i]
            plot_labels = all_plot_labels[i]

            # # Perform a linear fit
            # m, um, c, uc = linfit.linfit(known_radii, fit_radii)
            # fit_x = np.linspace(min(known_radii), max(known_radii), 100)
            # axs[i].plot(fit_x, linear(fit_x, m, c), color = colors[i]
            #          # ,label = r'm = {:.3f} $\pm$ {:.3f}'.format(m, um) + '\n' +
            #          #  r'c = {:.3f} $\pm$ {:.3f}'.format(c, uc)
            #          )

            for j in range(len(fit_radii)):
                known_radii[j] = known_radii[j]*(1+j*0.03)
                axs[i].errorbar(known_radii[j], fit_radii[j], yerr = u_fit_radii[j], fmt = 's', color = plot_colors[j], ecolor = 'k', capsize = 2, label = plot_labels[j])
                # axs[i].plot(known_radii[j], fit_radii[j], '.', color = plot_colors[j])

            axs[i].plot([0.95*min(known_radii), 1.05*max(known_radii)], [known_radii[0], known_radii[0]], '--k', label = 'Known\nRadius')
            axs[i].plot([0.95*min(known_radii), 1.05*max(known_radii)], [known_radii[0]+1, known_radii[0]+1], '--r', label = 'Known\nRadius\nUncertainty')
            axs[i].plot([0.95*min(known_radii), 1.05*max(known_radii)], [known_radii[0]-1, known_radii[0]-1], '--r')

            axs[i].set_xlim(0.95*min(known_radii), 1.05*max(known_radii))
            axs[i].set_xticks([statistics.median(known_radii)])
            axs[i].set_xticklabels([known_radii[0]])

            if (max(fit_radii) < known_radii[0] + 2 and min(fit_radii) > known_radii[0] - 2):
                axs[i].set_ylim(known_radii[0] - 2, known_radii[0] + 2)
            else:
                axs[i].set_ylim(min(min(fit_radii) - 0.5, known_radii[0] - 2), max(max(fit_radii) + 0.5,known_radii[0] + 2))

            # plt.legend()

        # fig.tight_layout()
        plt.subplots_adjust(wspace = 0.8, right = 0.82)

        handles, labels = axs[2].get_legend_handles_labels()
        fig.legend(handles, labels, loc='center right')

        fig.text(0.5, 0.04, 'Known Radius (mm)', ha='center')                       # xlabel
        fig.text(0.04, 0.5, 'PEPT Radius (mm)', va='center', rotation='vertical')   # ylabel

        print('Done.\n')

        write_file = write_dir + 'fitted_radii-all_speeds.png'
        print('Writing results to', write_file)
        plt.savefig(write_file)
        # plt.show()

    return

# Find the beamline along the z-axis using 2D PEPT
def find_beamline(data_dir, f, N, allcoins = pd.DataFrame(), num_slices = 10, plot_slices = False, lin_slicing = True, lin_params = [], rot_slicing = False, rot_params = [], lor_limit = -1, avg_points = False):
    '''
    Takes the path to a pept_coincidence_data file, reads in the coincidence data
    and finds positions along a beamline, slicing along the z-axis. Plots the
    results.

    @params:
        data_dir        - Required : Path to processed coincidence data file "pept_coincidence_data" (Str)
        f               - Required : F-parameter used when picking out LORs (Int)
        N               - Required : Minimum number of lines per position (Int)
        allcoins        - Optional : Dataframe of coincidences if not from a file (DataFrame, default = [])
        num_slices      - Optional : Number of slices along the z-axis to take (Int, default = 10)
        plot_slices     - Optional : Whether or not to plot the LORs for each z-slice (Bool, default = False)
        lin_slicing     - Optional : Whether to take linear slices of the FOV to find coincidences (Bool, default = True)
        rot_slicing     - Optional : Whether to take rotational slices of the FOV to find coincidences (Bool, default = False)
        rot_params      - Optional : Parameters for the rotatonal slicing in millimeters. Format: [min_z, max_z, offset] (Int[3], default = [])
        lor_limit       - Optional : Limit the number of LORs used for the reconstruction. If -1, use all LORs. (Int, default = -1)

    @returns:
        None
    '''
    num_slices += 1

    if allcoins.empty:
        coinfile = data_dir + 'pept_coincidence_data'
        if not os.path.isfile(coinfile):
            print('File', coinfile, 'does not exist.')
            return

        print('Reading in coincidence file', coinfile, '...')
        # Read in all the coincidence data
        allcoins = pd.read_csv(coinfile, names = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2'])
        print('Done.\n')

        if lor_limit != -1:
            allcoins = allcoins[allcoins.index < lor_limit]

    # Create beamline arrays
    beam_x, beam_y, beam_z = [], [], []
    beam_ux, beam_uy, beam_uz = [], [], []
    coins_per_slice = []

    min_z_lin = 0
    max_z_lin = 0

    # Linear Slicing
    # ------------------
    if lin_slicing:
        print('Running linear slicing positioning')
        print('----------------------------------\n')

        if lin_params == []:
            # Finding the minimum and maximum z-coordinates in the coincidence data
            max_z1 = allcoins['z1'].max()
            max_z2 = allcoins['z2'].max()
            min_z1 = allcoins['z1'].min()
            min_z2 = allcoins['z2'].min()

            max_z_lin = max_z1
            if max_z2 > max_z_lin:
                max_z_lin = max_z2

            min_z_lin = min_z1
            if min_z2 > min_z_lin:
                min_z_lin = min_z2
        else:
            min_z_lin = lin_params[0]
            max_z_lin = lin_params[1]

        # Loop through each slice in the z-axis
        z_slices = np.linspace(min_z_lin, max_z_lin, num_slices)
        for i in range(len(z_slices)-1):
            # First get the coincidences subset
            coins_subset = allcoins[ ((allcoins['z1'] > z_slices[i]) & (allcoins['z1'] < z_slices[i+1]))
                                   & ((allcoins['z2'] > z_slices[i]) & (allcoins['z2'] < z_slices[i+1])) ]

            print('Bounds of z_slice: (', z_slices[i], ',', z_slices[i+1], ')' )
            print('Number of events in slice =', len(coins_subset.index))
            coins_per_slice.append(len(coins_subset.index))

            mean_z = (z_slices[i] + z_slices[i+1])/2

            print('Mean z = ', mean_z)

            if len(coins_subset.index) <= N:
                print('Not enough councidences to obtain a position.\n')
                continue

            # print(coins_subset)

            # collapsed_z1s = np.random.normal(mean_z, 1, len(coins_subset.index))        # draw z coordinates from gaussian distribution
            # collapsed_z2s = np.random.normal(mean_z, 1, len(coins_subset.index))        # draw z coordinates from gaussian distribution
            # coins_subset['z1'] = collapsed_z1s
            # coins_subset['z2'] = collapsed_z2s

            # coins_subset['z1'] = mean_z
            # coins_subset['z2'] = mean_z

            # print(coins_subset)

            # Write the coincidences to file for PEPT algorithm
            np.savetxt(data_dir + 'tmp', coins_subset, delimiter=',', fmt='%.4f')

            # Next get the PEPT position
            N_ = len(coins_subset.index)-1     # use all the lines for the position
            pos, uncert = run_ctrack(data_dir + 'tmp', f, N_)
            if pos is None:
                print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                return

            print('Position = ', pos, '+-', uncert)
            print()

            # Plotting the coincidences of a slice
            if plot_slices:
                p0_xPos = coins_subset.iloc[:, 1]; p0_yPos = coins_subset.iloc[:, 2]; p0_zPos = coins_subset.iloc[:, 3]
                p1_xPos = coins_subset.iloc[:, 5]; p1_yPos = coins_subset.iloc[:, 6]; p1_zPos = coins_subset.iloc[:, 7]

                # num = len(p0_xPos)
                # print('Plotting XY coincidences of first', num, 'coincidences...')
                # for j in range(0, num):
                #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
                #     plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_yPos.iloc[j], p1_yPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
                # # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)))
                # plt.title('XY Position of {:d} coincidences locations. z = {:f}'.format(num, mean_z))
                # plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')
                # plt.show()

                # XY cross-section
                num = len(p0_xPos)
                print('Plotting XY coincidences of first', num, 'coincidences...')
                for j in range(0, num):
                    plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_yPos.iloc[j], p1_yPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)

                plt.plot(pos[0], pos[1], 'rX')

                plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')

                plt.show()
                plt.cla(); plt.clf()

                # XZ cross-section
                num = len(p0_xPos)
                print('Plotting XY coincidences of first', num, 'coincidences...')
                for j in range(0, num):
                    plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_zPos.iloc[j], p1_zPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)

                plt.plot(pos[0], pos[2], 'rX')

                plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')

                plt.show()
                plt.cla(); plt.clf()


            # Store position results
            beam_x.append(pos[0])
            beam_y.append(pos[1])
            beam_z.append(pos[2])

            beam_ux.append(uncert[0])
            beam_uy.append(uncert[1])
            beam_uz.append(uncert[2])

    # Rotational Slicing
    # ------------------
    if rot_slicing:
        print('Running rotational slicing positioning')
        print('--------------------------------------\n')

        if rot_params == []:
            print('Please specify the rotational slicing parameters: [min_z, max_z, offset]')
            return

        # rot_params = [30, 73, -50]

        min_z = rot_params[0]
        max_z = rot_params[1]
        offset = rot_params[2]     # offset for the second z-coordinate of coincidence

        # Loop through each slice in the z-axis
        z_slices = np.linspace(min_z, max_z, num_slices)

        for i in range(len(z_slices)-1):
            for j in range(len(z_slices)-1):
                # First get the coincidences subset
                coins_subset = allcoins[ ((allcoins['z1'] > z_slices[i]) & (allcoins['z1'] < z_slices[i+1]))
                                       & ((allcoins['z2'] > z_slices[j]+offset) & (allcoins['z2'] < z_slices[j+1]+offset)) ]

                # Append the mirror slice
                coins_subset.append(allcoins[ ((allcoins['z2'] > z_slices[i]) & (allcoins['z2'] < z_slices[i+1]))
                                       & ((allcoins['z1'] > z_slices[j]+offset) & (allcoins['z1'] < z_slices[j+1]+offset)) ])

                # print('Bounds of z_slice: (', z_slices[i], ',', z_slices[i+1], ')' )
                # print('Number of events in slice =', len(coins_subset.index))
                coins_per_slice.append(len(coins_subset.index))

                mean_z = (z_slices[i] + z_slices[i+1])/2

                # print('Mean z = ', mean_z)

                if len(coins_subset.index) <= N:
                    # print('Not enough councidences to obtain a position.\n')
                    continue

                # print(coins_subset)

                # collapsed_z1s = np.random.normal(mean_z, 1, len(coins_subset.index))        # draw z coordinates from gaussian distribution
                # collapsed_z2s = np.random.normal(mean_z, 1, len(coins_subset.index))        # draw z coordinates from gaussian distribution
                # coins_subset['z1'] = collapsed_z1s
                # coins_subset['z2'] = collapsed_z2s

                # coins_subset['z1'] = mean_z
                # coins_subset['z2'] = mean_z

                # print(coins_subset)

                # Write the coincidences to file for PEPT algorithm
                np.savetxt(data_dir + 'tmp', coins_subset, delimiter=',', fmt='%.4f')

                # Next get the PEPT position
                N_ = len(coins_subset.index)-1     # use all the lines for the position
                pos, uncert = run_ctrack(data_dir + 'tmp', f, N_)
                if pos is None:
                    print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
                    return

                # print('Position = ', pos, '+-', uncert)
                # print()

                # Plotting the coincidences of a slice
                if plot_slices:
                    p0_xPos = coins_subset.iloc[:, 1]; p0_yPos = coins_subset.iloc[:, 2]; p0_zPos = coins_subset.iloc[:, 3]
                    p1_xPos = coins_subset.iloc[:, 5]; p1_yPos = coins_subset.iloc[:, 6]; p1_zPos = coins_subset.iloc[:, 7]

                    # num = len(p0_xPos)
                    # print('Plotting XY coincidences of first', num, 'coincidences...')
                    # for j in range(0, num):
                    #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
                    #     plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_yPos.iloc[j], p1_yPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
                    # # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)))
                    # plt.title('XY Position of {:d} coincidences locations. z = {:f}'.format(num, mean_z))
                    # plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')
                    # plt.show()

                    # XY cross-section
                    num = len(p0_xPos)
                    print('Plotting XY coincidences of first', num, 'coincidences...')
                    for j in range(0, num):
                        plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_yPos.iloc[j], p1_yPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)

                    plt.plot(pos[0], pos[1], 'rX')

                    plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')

                    plt.show()
                    plt.cla(); plt.clf()

                    # XZ cross-section
                    num = len(p0_xPos)
                    print('Plotting XY coincidences of first', num, 'coincidences...')
                    for j in range(0, num):
                        plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_zPos.iloc[j], p1_zPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)

                    plt.plot(pos[0], pos[2], 'rX')

                    plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')

                    plt.show()
                    plt.cla(); plt.clf()


                # Store position results
                beam_x.append(pos[0])
                beam_y.append(pos[1])
                beam_z.append(pos[2])

                beam_ux.append(uncert[0])
                beam_uy.append(uncert[1])
                beam_uz.append(uncert[2])

    coins_per_slice = np.array(coins_per_slice)
    print('Coincidences per slice: {:f} +- {:f}'.format(np.mean(coins_per_slice), np.std(coins_per_slice)))

    # Rotation slicing refinement
    # ---------------------------
    if rot_slicing and avg_points:
        positions = np.transpose([beam_x, beam_y, beam_z])
        beam_x = []; beam_y = []; beam_z = []
        beam_ux = []; beam_uy = []; beam_uz = []

        # beam_z_min = -20
        # beam_z_max = 20
        # positions = positions[( (positions[:,2] > beam_z_min) & (positions[:,2] < beam_z_max) )     # set z-limits
        #                     & ( (positions[:,0] > -10) & (positions[:,0] < 10) )                    # set x-limits
        #                     & ( (positions[:,1] > -10) & (positions[:,1] < 10) )]                   # set y-limits

        # Average out the x and y coordinates in each slice
        z_slices = np.linspace(min_z_lin, max_z_lin, num_slices)

        for i in range(len(z_slices)-1):
            tmp = positions[(positions[:,2] > z_slices[i]) & (positions[:,2] < z_slices[i+1])]
            if len(tmp) == 0:
                continue

            # Average positions
            beam_x.append(np.mean(tmp[:,0]))
            beam_y.append(np.mean(tmp[:,1]))
            beam_z.append(np.mean(tmp[:,2]))
            # Standard uncertainties
            beam_ux.append(np.std(tmp[:,0])/len(tmp))
            beam_uy.append(np.std(tmp[:,1])/len(tmp))
            beam_uz.append(np.std(tmp[:,2])/len(tmp))

    beam_x = np.array(beam_x)
    beam_ux = np.array(beam_ux)

    beam_y = np.array(beam_y)
    beam_uy = np.array(beam_uy)

    beam_z = np.array(beam_z)
    beam_uz = np.array(beam_uz)

    # print(beam_x)
    # print(beam_y)
    # print(beam_z)

    # Plotting the results
    # --------------------

    # 2D plotting
    # -----------

    plt.rcParams.update({'font.size': 20})

    # ---------------------------------------------------------------------------------------------------------------------------------
    # XY-plane
    # ---------------------------------------------------------------------------------------------------------------------------------
    figure(num=None, figsize=(9, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size

    ax = plt.gca()

    ax.plot(beam_x, beam_y, 'g.', ms = 10, label = 'Beamline PEPT Positions')
    ax.errorbar(beam_x, beam_y, xerr = beam_ux, yerr = beam_uy, fmt = 'none', ecolor = 'k')
    circ = plt.Circle((0,0), 1.5, linestyle = '--', color = 'r', fill=False, label = 'Known Beamline')
    ax.add_artist(circ)
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)
    plt.xlabel('x-coordinate (mm)')
    plt.ylabel('y-coordinate (mm)')
    # ax.legend()

    savefile = '{}beamline-xy-{}_slices.png'.format(data_dir, num_slices-1)
    print('Saving plot to', savefile)
    plt.savefig(savefile)
    # plt.show()
    plt.clf(); plt.cla()

    # ---------------------------------------------------------------------------------------------------------------------------------
    # XZ-plane
    # ---------------------------------------------------------------------------------------------------------------------------------
    fig, axs = plt.subplots(nrows=2, ncols=1, sharey=False, sharex=False, figsize=(16, 9), dpi = 300)
    xz_axis = axs[0]
    yz_axis = axs[1]

    xz_axis.plot(beam_z, beam_x, 'g.', ms = 10, label = 'Beamline PEPT Positions')
    xz_axis.errorbar(beam_z, beam_x, yerr = beam_ux, fmt = 'none', ecolor = 'k')

    xz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.ones(100)*(-1.5), '--', color = 'red', label = 'Known Beamline')
    # xz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.zeros(100), '--', color = 'red')
    xz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.ones(100)*(1.5), '--', color = 'red')

    mfit, umfit, cfit, ucfit = weightedLinear(beam_z, beam_x, np.ones(len(beam_z)), beam_ux)
    # mfit, cfit, r_value, p_value, std_err = stats.linregress(beam_z, beam_x)
    fitx = np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100)
    xz_axis.plot(fitx, mfit*fitx + cfit, 'k', label = 'Least Squares Fit')

    xz_axis.set_ylabel('x-coordinate (mm)')

    xz_axis.set_ylim(-4,4)

    xz_axis.legend(bbox_to_anchor=(0,1.02,1,0.2), loc = 'lower left', mode = 'expand', ncol = 3)
    # plt.xlabel('z-coordinate (mm)')
    # plt.show()

    # ---------------------------------------------------------------------------------------------------------------------------------
    # YZ-plane
    # ---------------------------------------------------------------------------------------------------------------------------------
    yz_axis.plot(beam_z, beam_y, 'g.', ms = 10)
    yz_axis.errorbar(beam_z, beam_y, yerr = beam_uy, fmt = 'none', ecolor = 'k')

    yz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.ones(100)*(-1.5), '--', color = 'red')
    # yz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.zeros(100), '--', color = 'red')
    yz_axis.plot(np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), np.ones(100)*(1.5), '--', color = 'red')

    mfit, umfit, cfit, ucfit = weightedLinear(beam_z, beam_y, np.ones(len(beam_z)), beam_uy)
    # mfit, cfit, r_value, p_value, std_err = stats.linregress(beam_z, beam_x)
    fitx = np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100)
    yz_axis.plot(fitx, mfit*fitx + cfit, 'k')

    yz_axis.set_ylabel('y-coordinate (mm)')
    yz_axis.set_xlabel('z-coordinate (mm)')

    yz_axis.set_ylim(-4,4)

    savefile = '{}beamline-xz_yz-{}_slices.png'.format(data_dir, num_slices-1)
    print('Saving plot to', savefile)
    plt.savefig(savefile, bbox_inches="tight")
    # plt.show()

    # ---------------------------------------------------------------------------------------------------------------------------------
    # 3D Plotting
    # ---------------------------------------------------------------------------------------------------------------------------------
    # Setup 3D plotting
    # ax = plt.axes(projection='3d')
    # ax.view_init(elev = 15, azim=-65)
    #
    # # ax.plot3D(xs, ys, zs, 'g-', linewidth = 0.5)
    # ax.plot3D(np.zeros(100), np.zeros(100), np.linspace(np.nanmin(beam_z), np.nanmax(beam_z), 100), '--k')
    # ax.scatter3D(beam_x, beam_y, beam_z, color = 'r', s = 1)
    #
    # print('Plotting crystal locations...')
    #
    # def rect_prism(x_range, y_range, z_range):
    #
    #     yy, zz = np.meshgrid(y_range, z_range)
    #     ax.plot_wireframe(x_range[0], yy, zz, color="r")
    #     ax.plot_surface(x_range[0], yy, zz, color="r", alpha=0.2)
    #     ax.plot_wireframe(x_range[1], yy, zz, color="r")
    #     ax.plot_surface(x_range[1], yy, zz, color="r", alpha=0.2)
    #
    #     xx, zz = np.meshgrid(x_range, z_range)
    #     ax.plot_wireframe(xx, y_range[0], zz, color="r")
    #     ax.plot_surface(xx, y_range[0], zz, color="r", alpha=0.2)
    #     ax.plot_wireframe(xx, y_range[1], zz, color="r")
    #     ax.plot_surface(xx, y_range[1], zz, color="r", alpha=0.2)
    #
    # rect_prism( np.array([-65, -55]), np.array([-21, 21]), np.array([-120, 80]) )
    # rect_prism( np.array([65, 55]), np.array([-21, 21]), np.array([-120, 80]) )
    # rect_prism( np.array([-21, 21]), np.array([65, 55]), np.array([-120, 80]) )
    # rect_prism( np.array([-21, 21]), np.array([-65, -55]), np.array([-120, 80]) )
    #
    # ax.set_xlabel('x-coordinate (mm)')
    # ax.set_ylabel('y-coordinate (mm)')
    # ax.set_zlabel('z-coordinate (mm)')

    # plt.show()

    return

# Plotting PEPT lines snapshots of a moving track
def track_snapshops(data_dir, f, N):
    '''
    Loop through a coincidences file, taking N lines at a time and plotting them.

    @params:
        data_dir        - Required : Path to processed coincidence data file "pept_coincidence_data" (Str)
        f               - Required : F-parameter used when picking out LORs (Int)
        N               - Required : Number of lines per position (Int)

    @returns:
        None
    '''

    plot_all = False
    plot_highlighted = True
    plot_accepted_only = False

    # Read in the coincidences
    coincidence_path = data_dir + 'pept_coincidence_data'
    print('Reading in coincidence data from', coincidence_path + '.', '\nThis may take several minutes...')
    coincidences = pd.read_csv(coincidence_path, sep = ',', header = None)       # read in text file as a csv with tab separation
    print('Done.')
    coincidences.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']       # separate into columns and name them

    num_plots = 10      # number of successive plots to run

    for i in range(num_plots):
        subset = coincidences[i*N:(i+1)*N]

        tmp_coinfile = data_dir + 'tmp'
        subset.to_csv(tmp_coinfile, header = False, index = False)              # write data to csv for ctrack processing
        pos, ucert = run_ctrack(tmp_coinfile, f, N-1)                             # get the position and uncertainties
        if pos is None:
            print('ERROR: Cannot run ctrack with given N and fopt - not enough lines. Terminating.')
            return
        os.remove(tmp_coinfile)                                                 # remove data file
        os.remove(tmp_coinfile + '.a')                                          # remove ctrack file
        # print(pos)

        # Calculating all LOR distances from ctrack position
        distances = []

        # Calculate all the distances to the position
        for index, coin in subset.iterrows():
            # First get the straight line equation
            m = (coin['y2']-coin['y1'])/(coin['x2']-coin['x1'])
            c = coin['y1'] - m*coin['x1']

            d = (np.abs(m*pos[0]-pos[1]+c))/(np.sqrt(m**2 + 1))     # get the distance from the line to the point

            distances.append(d)

        subset['d'] = distances       # add the distances to the coincidence
        subset.sort_values('d', inplace = True)       # sort by the distance from the location

        N_0 = len(subset.index)
        N_f = int((f/100)*N_0)
        f_subcoins = subset.head(N_f)
        r_subcoins = subset.tail(N_0 - N_f)

        # Plot the crystal outlines
        corners_1 = np.array([1, 21, 21, 1, 1])
        corners_2 = np.array([-38.65, -38.65, -48.65, -48.65, -38.65])
        permutations = np.array([(1, 1), (-1, 1), (1, -1), (-1, -1)])

        for j in range(4):
            plt.plot(corners_1*permutations[j][0], corners_2*permutations[j][1], 'k')
        for j in range(4):
            plt.plot(corners_2*permutations[j][0], corners_1*permutations[j][1], 'k')

        # Plot the coincidence lines
        if plot_all:
            p0_xPos = subset.iloc[:, 1]; p0_yPos = subset.iloc[:, 2]; p0_zPos = subset.iloc[:, 3]
            p1_xPos = subset.iloc[:, 5]; p1_yPos = subset.iloc[:, 6]; p1_zPos = subset.iloc[:, 7]
            num = len(p0_xPos)
            for j in range(0, num):
                plt.plot([p0_xPos.iloc[j], p1_xPos.iloc[j]], [p0_yPos.iloc[j], p1_yPos.iloc[j]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)


        if plot_highlighted:

            num = N_0 - N_f

            # Get the x,y,z coordinates as separate dataframes
            p0_xPos = r_subcoins.iloc[:, 1]; p0_yPos = r_subcoins.iloc[:, 2]; p0_zPos = r_subcoins.iloc[:, 3]
            p1_xPos = r_subcoins.iloc[:, 5]; p1_yPos = r_subcoins.iloc[:, 6]; p1_zPos = r_subcoins.iloc[:, 7]

            for i in range(0, num):
                plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='red', marker='o', markersize=1, linestyle='--', linewidth=0.3)

            num = N_f

            # Get the x,y,z coordinates as separate dataframes
            p0_xPos = f_subcoins.iloc[:, 1]; p0_yPos = f_subcoins.iloc[:, 2]; p0_zPos = f_subcoins.iloc[:, 3]
            p1_xPos = f_subcoins.iloc[:, 5]; p1_yPos = f_subcoins.iloc[:, 6]; p1_zPos = f_subcoins.iloc[:, 7]

            for i in range(0, num):
                plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=1, linestyle='-', linewidth=0.3)

        if plot_accepted_only:
            num = N_f

            # Get the x,y,z coordinates as separate dataframes
            p0_xPos = f_subcoins.iloc[:, 1]; p0_yPos = f_subcoins.iloc[:, 2]; p0_zPos = f_subcoins.iloc[:, 3]
            p1_xPos = f_subcoins.iloc[:, 5]; p1_yPos = f_subcoins.iloc[:, 6]; p1_zPos = f_subcoins.iloc[:, 7]

            for i in range(0, num):
                plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=1, linestyle='-', linewidth=0.3)


        # Plot the tracer position
        plt.plot(pos[0], pos[1], 'rX')

        plt.xlabel('X-Position (mm)')#, fontsize=30)
        plt.ylabel('Y-Position (mm)')#, fontsize=30)

        plt.show()

# Plot the energy spectrum of a given dataset
def plot_energy_spectrum(data_dir, write_dir = '', events = pd.DataFrame(), apply_energy_filter = False, e_window = [], e_range = (), fit_peaks = False, e_fits = [], shade_sca_window = False, sca_windows = []):
    '''
    Read in the data from an AllEventsCombined.txt file and plot the gamma
    energy spectrum

    @params:
        data_dir                - Required : Path to "AllEventsCombined.txt" file (Str)
        write_dir               - Required : Directory to save figure to (Str)
        events                  - Required : DataFrame of all the events to prevent reading it in again (DataFrame, default = empty)
        apply_energy_filter     - Optional : Whether or not to apply energy filtering on the data (Bool, default = False)
        e_window                - Optional : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        e_range                 - Optional : Range of energies in keV over which to plot the energy histogram (Tuple, default = ())
        fit_peaks               - Optional : Whether or not to fit normal distributions to energy peaks. @default - False (Bool)
        e_fits                  - Optional : Array specifying the peaks to fit. Each element is a triplet containing the central energy value and min & max energies to fit between. (Float[][3], default = [])
        shade_sca_window        - Optional : Whether or not to plot a shaded area representing the sca window (Bool, default = False)
        sca_window              - Optional : Array of intervals to shade, showing the sca window in keV (Int[], default = [])

    @returns:
        None
    '''
    matplotlib.rc('font',family='Palatino')
    figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 20})

    data_file = data_dir + 'AllEventsCombined.txt'
    num_bins = 8192     # number of bins for histogramming
    # num_bins = 'auto'     # number of bins for histogramming

    if events.empty:
        print('Reading in data from', data_file + '.', '\nThis may take several minutes...')
        events = pd.read_csv(data_file, sep = '	', header = None)                        # read in text file as a csv with tab separation
        print('Done.')
        events.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']       # separate into columns and name them
        print()

    # print('{:30} {:d}'.format('Total number of events:', len(events.index)))
    # print('{:30} {:.1f}'.format('Total time (seconds):', (events.iloc[-1]['time'] - events.iloc[0]['time']) * 1e-8))
    # print('{:30} {:.0f}'.format('Event rate (events/sec):', len(events.index) / ((events.iloc[-1]['time'] - events.iloc[0]['time']) * 1e-8) ))

    if apply_energy_filter:
        events.drop(events[ (events['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((events['energy'] > e_window[3]) & (events['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (events['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
        events.reset_index(drop = 1, inplace = 1)

    # Only use one module
    events.drop(events[events.detector != 1].index, inplace = True)
    events.reset_index(drop = True, inplace = True)

    energies = events['energy'].to_numpy()     # get the energy column

    n, bins, patches = plt.hist(energies, bins = num_bins, histtype = 'step', density = True, color = 'k')

    if fit_peaks:
        print('\nFitting normal distributions to energy peaks...')

        for e_fit in e_fits:
            fit_indicies = np.where((e_fit[1] < bins) & (e_fit[2] > bins))
            x_hist_fit = bins[(e_fit[1] < bins) & (e_fit[2] > bins)][1:]
            y_hist_fit = n[fit_indicies[0][0]:fit_indicies[0][-1]]

            p0 = [0.01, e_fit[0], e_fit[0]*0.01, 0.01]                          # initial guesses for fit
            popt, pcov = curve_fit(gaus, x_hist_fit, y_hist_fit, p0)            # perform the curve fit
            perr = np.sqrt(np.diag(pcov))

            centroid = popt[1]
            sigma = popt[2]

            u_centroid = perr[1]
            u_sigma = perr[2]

            # Plot the final fit
            # ------------------
            x_fit = np.linspace(min(x_hist_fit), max(x_hist_fit), 500)
            # x_fit = np.linspace(504, 520, 100)
            y_fit = gaus(x_fit, *popt)

            if e_fit[0] == 511:
                plt.plot(x_fit, y_fit,
                         label = r'Centroid = {:.1f} $\pm$ {:.1f} keV'.format(centroid, sigma) + '\n'
                                 r'$\sigma$ = {:.3f} $\pm$ {:.3f} keV'.format(sigma, u_sigma) + '\n' +
                                 # 'FWHM = {:.3f}keV\n'.format(2*np.sqrt(2*np.log(2))*sigma) +
                                 'Resolution = {:.2f}%'.format((2*np.sqrt(2*np.log(2))*sigma)/centroid * 100))
                                 # 'FWTM = {:.3f}keV'.format(2*np.sqrt(2*np.log(10))*sigma))
                         # color = 'red')
            else:
                plt.plot(x_fit, y_fit,
                         label = r'Centroid = {:.1f} $\pm$ {:.1f} keV'.format(centroid, sigma) + '\n'
                                 r'$\sigma$ = {:.2f} $\pm$ {:.2f} keV'.format(sigma, u_sigma))# + '\n' +
                                 # 'FWHM = {:.3f}keV\n'.format(2*np.sqrt(2*np.log(2))*sigma) +
                                 # 'FWTM = {:.3f}keV'.format(2*np.sqrt(2*np.log(10))*sigma))
                         # color = 'red')

            # Plotting a peak indication arrow
            plt.arrow(1.05*centroid + 50, max(y_hist_fit), -50, 0, width = 0.00005, head_length = 10, color = 'k')
            plt.text(1.05*centroid + 60, max(y_hist_fit)-0.0001, '{:d} keV Peak'.format(e_fit[0]))

    if shade_sca_window:
        for window in sca_windows:
            start_e = window[0]; end_e = window[1]
            max_e = max(n)

            colour = window[2]

            # Plot vertical lines indicating the sca windows
            plt.plot([start_e, start_e], [0, max_e], '--', color = colour)
            plt.plot([end_e, end_e], [0, max_e], '--', color = colour)

            # Shade the area between the lines
            plt.fill_between([start_e, end_e], [0, 0], [max_e, max_e],
                              facecolor = colour,                 # fill colour
                              color = colour,                     # outline colour
                              alpha = 0.2)                              # transparency

    if e_range != ():
        plt.xlim(e_range[0], e_range[1])

    if fit_peaks:
        plt.legend()

    plt.xlabel('Energy (keV)')
    plt.ylabel('Normalised Differential Pulse Height per Energy (dN/dE)')

    if write_dir == '':
        plot_filename = '{}energy_spectrum'.format(data_dir)
    else:
        plot_filename = '{}energy_spectrum'.format(write_dir)

    if fit_peaks:
        plot_filename += '-peak_fit'
        for fit in e_fits:
            plot_filename += '_{:d}'.format(fit[0])

    if shade_sca_window:
        plot_filename += '-sca'
        for window in sca_windows:
            plot_filename += '-{:d}_{:d}'.format(window[0], window[1])

    plot_filename += '.png'

    print('\nSaving to', plot_filename)
    plt.savefig(plot_filename)
    # plt.show()

    return

# Plot the coincidence rates as a bar graph of the coincidence processing parameters
def coincidence_parameters_comparisons(data_dir, e_windows, label_pos):
    '''
    Reads in the results from a set of coincidence processing log files and extracts
    the coincidence rate as a function of the parameters.

    @params:
        data_dir        - Required : Path to "AllEventsCombined.txt" file (Str)
        e_windows       - Required : The energy windows used for the coincidence processing in keV (Int[4])
        label_pos       - Required : Specify whether to plot the fopt coincidence rate on the left or the right (Str)

    @returns:
        None
    '''

    coin_params = pd.DataFrame(columns=['window', 'peak_e', 'ce_e', 'doubles', 'rate'])     # create a dataframe to hold all the data

    # Loop through coincidence processing files in directory
    for file in os.listdir(data_dir):

        # Ignore anything that isn't a log file
        if 'coincidence_processing' not in file or file[0] == '.' or '.log' not in file:
            continue

        # Start by extracting all the information from the filename
        coin_window = int(re.search(r'-(\d+)ns', file).group(1))
        e_window_peak = (int(re.search(r'ns-(\d+)\_(\d+)keV', file).group(1)), int(re.search(r'ns-(\d+)\_(\d+)keV', file).group(2)))

        e_window_ce = ()

        if 'no_ce' not in file:
            e_window_ce = (int(re.search(r'ce_(\d+)\_(\d+)keV', file).group(1)), int(re.search(r'ce_(\d+)\_(\d+)keV', file).group(2)))
            # print(e_window_ce)

        doubles = True
        if 'no_doubles' in file:
            doubles = False

        # Next read in the file and extract the coincidence rate
        coinrate = -1
        # print(data_dir + file)
        with open(data_dir + file, 'r') as f:
            log_info = f.read()     # read file into a string
            coinrate = int(re.search(r'Total coincidence rate \(coins/sec\):\s+(\d+)', log_info).group(1))

        coin_params = coin_params.append({'window': coin_window, 'peak_e' : e_window_peak, 'ce_e' : e_window_ce, 'doubles' : doubles, 'rate' : coinrate}, ignore_index = True)

    fopt_vals = []      # create empty list to hold fopt values and indicies in dataframe
    N_vals = []         # create empty list to hold lines per position values and indicies in dataframe
    loc_rates = []      # create empty list to hold location rate values and indicies in dataframe
    uncertainties = []  # create empty list to hold position uncertainty values and indicies in dataframe

    # Loop through fopt files in directory
    for file in os.listdir(data_dir):

        # Ignore anything that isn't a fopt log file
        if 'fopt' not in file or file[0] == '.' or '.log' not in file:
            continue

        # Start by extracting all the information from the filename
        coin_window = int(re.search(r'-(\d+)ns', file).group(1))
        e_window_peak = (int(re.search(r'ns-(\d+)\_(\d+)keV', file).group(1)), int(re.search(r'ns-(\d+)\_(\d+)keV', file).group(2)))
        N = int(re.search(r'fopt-N(\d+)', file).group(1))

        e_window_ce = ()

        if 'no_ce' not in file:
            e_window_ce = (int(re.search(r'ce_(\d+)\_(\d+)keV', file).group(1)), int(re.search(r'ce_(\d+)\_(\d+)keV', file).group(2)))
            # print(e_window_ce)

        doubles = True
        if 'no_doubles' in file:
            doubles = False

        # Next read in the file and extract the coincidence rate
        fopt = -1
        uncertainty = -1
        # print(data_dir + file)
        with open(data_dir + file, 'r') as f:
            log_info = f.read()     # read file into a string
            # print(log_info)
            fopt = int(re.search(r'Optimal F values:\s+\[(\d+)|(\d+)(\s+\d+)+\]', log_info).group(1))
            u = [float(i) for i in re.search(r'Uncertainties with Fopt:\s+\[(\d+\.\d+\s+\d+\.\d+\s+\d+\.\d+)', log_info).group(1).split()]
            uncertainty = np.sqrt(u[0]**2 + u[1]**2 + u[2]**2)

        # coin_params = coin_params.append({'window': coin_window, 'peak_e' : e_window_peak, 'ce_e' : e_window_ce, 'doubles' : doubles, 'rate' : coinrate}, ignore_index = True)
        match_df = coin_params.where((coin_params.window == coin_window) & (coin_params.peak_e == e_window_peak) & (coin_params.ce_e == e_window_ce) & (coin_params.doubles == doubles))
        rate = match_df.dropna().reset_index().loc[0, 'rate']
        # print(match_df)
        dataset_index = match_df.dropna().index
        # coin_params.loc[dataset_index, 'fopt'] = fopt
        fopt_vals.append((dataset_index, fopt))
        N_vals.append((dataset_index, N))
        loc_rates.append((dataset_index, rate/N))
        uncertainties.append((dataset_index, uncertainty))
        # print(dataset_index)
        # print(fopt)
        # print()

    for val in fopt_vals:
        coin_params.loc[val[0], 'fopt'] = val[1]

    for val in N_vals:
        coin_params.loc[val[0], 'N'] = val[1]

    for val in loc_rates:
        coin_params.loc[val[0], 'loc_rate'] = val[1]

    for val in uncertainties:
        coin_params.loc[val[0], 'uncertainty'] = val[1]

    print(coin_params)
    # return

    # Plotting comparison bar chart
    # -----------------------------

    def get_rate(time_window, peak_energy, ce_energy, doubles):
        rate = coin_params[ (coin_params.window == time_window) & (coin_params.peak_e == peak_energy) & (coin_params.ce_e == ce_energy) & (coin_params.doubles == doubles) ].reset_index().loc[0, 'rate']
        fopt = coin_params[ (coin_params.window == time_window) & (coin_params.peak_e == peak_energy) & (coin_params.ce_e == ce_energy) & (coin_params.doubles == doubles) ].reset_index().loc[0, 'fopt']
        loc_rate = coin_params[ (coin_params.window == time_window) & (coin_params.peak_e == peak_energy) & (coin_params.ce_e == ce_energy) & (coin_params.doubles == doubles) ].reset_index().loc[0, 'loc_rate']
        uncert = coin_params[ (coin_params.window == time_window) & (coin_params.peak_e == peak_energy) & (coin_params.ce_e == ce_energy) & (coin_params.doubles == doubles) ].reset_index().loc[0, 'uncertainty']
        n = coin_params[ (coin_params.window == time_window) & (coin_params.peak_e == peak_energy) & (coin_params.ce_e == ce_energy) & (coin_params.doubles == doubles) ].reset_index().loc[0, 'N']

        return rate, fopt, loc_rate, uncert, n

    fopts = []
    loc_rates = []
    uncertainties = []
    n_vals = []


    # Full comparison for fixed uncert
    # --------------------------------
    base, fopt, loc_rate, uncert, n = get_rate(300, (e_windows[0][0], e_windows[0][1]), (), False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    six, fopt, loc_rate, uncert, n = get_rate(600, (e_windows[0][0], e_windows[0][1]), (), False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    fwtm, fopt, loc_rate, uncert, n = get_rate(300, (e_windows[1][0], e_windows[1][1]), (), False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    ce, fopt, loc_rate, uncert, n = get_rate(300, (e_windows[0][0], e_windows[0][1]), (e_windows[0][2], e_windows[0][3]), False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    dbls, fopt, loc_rate, uncert, n = get_rate(300, (e_windows[0][0], e_windows[0][1]), (), True)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    all, fopt, loc_rate, uncert, n = get_rate(600, (e_windows[1][0], e_windows[1][1]), (e_windows[1][2], e_windows[1][3]), True)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    fopts = np.array(fopts)/100
    uncertainties = np.array(uncertainties)

    comparisons = np.array([base, six, fwtm, ce, dbls, all])
    comparisons_fopt = comparisons*fopts

    # # Energy comparison
    # # -----------------
    # fwhm, fopt, loc_rate, uncert = get_rate(600, (e_windows[0][0], e_windows[0][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwhm_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[0][0], e_windows[0][1]), (e_windows[0][2], e_windows[0][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwtm, fopt, loc_rate, uncert = get_rate(600, (e_windows[1][0], e_windows[1][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwtm_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[1][0], e_windows[1][1]), (e_windows[1][2], e_windows[1][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # wide, fopt, loc_rate, uncert = get_rate(600, (e_windows[2][0], e_windows[2][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # wide_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[2][0], e_windows[2][1]), (e_windows[2][2], e_windows[2][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fopts = np.array(fopts)/100
    # uncertainties = np.array(uncertainties)
    #
    # comparisons = np.array([fwhm, fwhm_ce, fwtm, fwtm_ce, wide, wide_ce])
    # comparisons_fopt = comparisons*fopts


    # Plotting a horizontal bar chart comparison
    # ------------------------------------------
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    # plt.rcParams.update({'font.size': 18})

    comparison_labels = ['No\nOptimisations', '600ns\nCoincidence\nWindow', 'Wide\nEnergy\nWindow', 'Compton\nEdge\nFiltering',
                  'Include\nDouble\nScatters', 'All\nOptimisations']

    # comparison_labels = ['FWHM', 'FWHM + CE', 'FWTM', 'FWTM + CE', 'Wide', 'Wide + CE']

    #  create the figure
    fig, ax1 = plt.subplots(figsize=(9, 7), dpi=500, facecolor='w', edgecolor='k')
    fig.subplots_adjust(left=0.135, right=0.9)

    pos = np.arange(len(comparison_labels))

    # Plot the horizontal bars for comparisons with and without the fopt

    # # Plot coincidence rates
    # rects = ax1.barh(pos, comparisons,
    #                  align='center',
    #                  height=0.5,
    #                  tick_label=comparison_labels,
    #                  color = ['r', 'b', 'b', 'b', 'b', 'b'],
    #                  # color = ['b', 'b', 'b', 'b', 'b', 'b'],
    #                  alpha = 0.3)
    #
    # rects_fopt = ax1.barh(pos, comparisons_fopt,
    #                  align='center',
    #                  height=0.5,
    #                  tick_label=comparison_labels,
    #                  color = ['r', 'b', 'b', 'b', 'b', 'b']
    #                  # color = ['b', 'b', 'b', 'b', 'b', 'b']
    #                  )

    # Plot location rates
    rects = ax1.barh(pos, loc_rates,
                     align='center',
                     height=0.5,
                     tick_label=comparison_labels,
                     color = ['r', 'b', 'b', 'b', 'b', 'b'])#,
                     # color = ['b', 'b', 'b', 'b', 'b', 'b'],
                     # alpha = 0.3)

    # ax1.set_xlim([0, max(comparisons)*1.08])
    ax1.set_xlim([0, max(loc_rates)*1.1])

    # Plot vertical dotted lines to help readability
    ax1.xaxis.set_major_locator(MaxNLocator(11))
    ax1.xaxis.grid(True, linestyle='--', which='major',
                   color='grey', alpha=.25)

    ax2 = ax1.twinx()                               # set the right-hand Y-axis ticks and labels
    ax2.set_yticks(pos)                             # set the tick locations
    ax2.set_ylim(ax1.get_ylim())                    # make sure that the limits are set equally on both yaxis so the ticks line up
    # ax2.set_yticklabels(np.round_(loc_rates, 2))    # set the tick labels
    # ax2.set_yticklabels(np.round_(uncertainties*1000, 0))    # set the tick labels
    ax2.set_yticklabels(n_vals)    # set the tick labels

    ax1.set_xlabel('Location Rate (Hz)')
    # ax1.set_xlabel('Coincidence Rate (Hz)')
    # ax2.set_ylabel('Location Rate (Hz)')
    # ax2.set_ylabel(r'Radial Uncertainty ($\mu$m)')
    ax2.set_ylabel(r'Lines-per-Position ($N$)')

    # Lastly, write in the coincidence rate for each bar
    for rect in rects:
        # width = int(rect.get_width())
        width = round(rect.get_width(), 3)
        # Center the text vertically in the bar
        yloc = rect.get_y() + rect.get_height() / 2
        label = ax1.annotate(width, xy=(width, yloc), xytext=(5, 0),
                            textcoords="offset points",
                            ha='left', va='center',
                            color='black', weight='bold', clip_on=True)

    # for rect in rects_fopt:
    #     width = rect.get_width()
    #
    #     xloc = 5
    #     align = 'left'
    #     txtcolor = 'black'
    #
    #     if label_pos == 'left':
    #         xloc = -5
    #         align = 'right'
    #         txtcolor = 'white'
    #
    #     # Center the text vertically in the bar
    #     yloc = rect.get_y() + rect.get_height() / 2
    #     label = ax1.annotate(
    #                         # np.round_(width, 1),
    #                         int(np.round_(width, 0)),
    #                         xy=(width, yloc), xytext=(xloc, 0),
    #                         textcoords="offset points",
    #                         ha=align, va='center',
    #                         color=txtcolor, weight='bold', clip_on=True)

    # fig.savefig('{}coin_rates-vs-e_windows.png'.format(data_dir))
    # fig.savefig('{}full_param_optimisation.png'.format(data_dir))
    fig.savefig('{}full_param_optimisation-fixed_u.png'.format(data_dir))
    # plt.show()

    return

# Plot the coincidence rates as a bar graph of the coincidence processing parameter. Additional function for no energy filters.
def coincidence_parameters_comparisons2(data_dir, e_windows, label_pos):
    '''
    Reads in the results from a set of coincidence processing log files and extracts
    the coincidence rate as a function of the parameters.

    @params:
        data_dir        - Required : Path to "AllEventsCombined.txt" file (Str)
        e_windows       - Required : Array of the energy windows used for the coincidence processing in keV (Int[][4])
        label_pos       - Required : Specify whether to plot the fopt coincidence rate on the left or the right (Str)

    @returns:
        None
    '''
    matplotlib.rc('font',family='Palatino')
    plt.rcParams.update({'font.size': 15})

    coin_params = pd.DataFrame(columns=['window', 'e_filter', 'rate'])     # create a dataframe to hold all the data

    # Loop through coincidence processing files in directory
    for file in os.listdir(data_dir):

        # Ignore anything that isn't a log file
        if 'coincidence_processing' not in file or file[0] == '.' or '.log' not in file:
            continue

        # Start by extracting all the information from the filename
        coin_window = int(re.search(r'-(\d+)ns', file).group(1))

        e_filter = True
        if 'no_e_filter' in file:
            e_filter = False

        # Next read in the file and extract the coincidence rate
        coinrate = -1
        # print(data_dir + file)
        with open(data_dir + file, 'r') as f:
            log_info = f.read()     # read file into a string
            coinrate = int(re.search(r'Total coincidence rate \(coins/sec\):\s+(\d+)', log_info).group(1))

        # print(coin_window, e_filter, coinrate)
        coin_params = coin_params.append({'window': coin_window, 'e_filter': e_filter, 'rate' : coinrate}, ignore_index = True)

    fopt_vals = []      # create empty list to hold fopt values and indicies in dataframe
    N_vals = []         # create empty list to hold lines per position values and indicies in dataframe
    loc_rates = []      # create empty list to hold location rate values and indicies in dataframe
    uncertainties = []  # create empty list to hold position uncertainty values and indicies in dataframe

    # Loop through fopt files in directory
    for file in os.listdir(data_dir):

        # Ignore anything that isn't a fopt log file
        if 'fopt' not in file or file[0] == '.' or '.log' not in file:
            continue

        # Start by extracting all the information from the filename
        coin_window = int(re.search(r'-(\d+)ns', file).group(1))
        N = int(re.search(r'fopt-N(\d+)', file).group(1))

        e_filter = True
        if 'no_efilter' in file:
            e_filter = False

        # Next read in the file and extract the coincidence rate
        fopt = -1
        uncertainty = -1
        with open(data_dir + file, 'r') as f:
            log_info = f.read()     # read file into a string
            fopt = int(re.search(r'Optimal F values:\s+\[(\d+)|(\d+)(\s+\d+)+\]', log_info).group(1))
            u = [float(i) for i in re.search(r'Uncertainties with Fopt:\s+\[(\d+\.\d+\s+\d+\.\d+\s+\d+\.\d+)', log_info).group(1).split()]
            uncertainty = np.sqrt(u[0]**2 + u[1]**2 + u[2]**2)

        # Now find the matching entry in the dataframe and store all the relevant information
        match_df = coin_params.where((coin_params.window == coin_window) & (coin_params.e_filter == e_filter))
        rate = match_df.dropna().reset_index().loc[0, 'rate']
        dataset_index = match_df.dropna().index
        fopt_vals.append((dataset_index, fopt))
        N_vals.append((dataset_index, N))
        loc_rates.append((dataset_index, rate/N))
        uncertainties.append((dataset_index, uncertainty))

    for val in fopt_vals:
        coin_params.loc[val[0], 'fopt'] = val[1]

    for val in N_vals:
        coin_params.loc[val[0], 'N'] = val[1]

    for val in loc_rates:
        coin_params.loc[val[0], 'loc_rate'] = val[1]

    for val in uncertainties:
        coin_params.loc[val[0], 'uncertainty'] = val[1]

    print(coin_params)
    # return

    # Plotting comparison bar chart
    # -----------------------------

    def get_rate(time_window, e_filter):
        rate = coin_params[ (coin_params.window == time_window) & (coin_params.e_filter == e_filter) ].reset_index().loc[0, 'rate']
        fopt = coin_params[ (coin_params.window == time_window) & (coin_params.e_filter == e_filter) ].reset_index().loc[0, 'fopt']
        loc_rate = coin_params[ (coin_params.window == time_window) & (coin_params.e_filter == e_filter) ].reset_index().loc[0, 'loc_rate']
        uncert = coin_params[ (coin_params.window == time_window) & (coin_params.e_filter == e_filter) ].reset_index().loc[0, 'uncertainty']
        n = coin_params[ (coin_params.window == time_window) & (coin_params.e_filter == e_filter) ].reset_index().loc[0, 'N']

        return rate, fopt, loc_rate, uncert, n

    fopts = []
    loc_rates = []
    uncertainties = []
    n_vals = []


    # Full comparison for fixed uncert
    # --------------------------------
    narrow_filter, fopt, loc_rate, uncert, n = get_rate(600, True)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    narrow_nofilter, fopt, loc_rate, uncert, n = get_rate(600, False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    wide_filter, fopt, loc_rate, uncert, n = get_rate(5000, True)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    wide_nofilter, fopt, loc_rate, uncert, n = get_rate(5000, False)
    fopts.append(fopt)
    loc_rates.append(loc_rate)
    uncertainties.append(uncert)
    n_vals.append(n)

    fopts = np.array(fopts)/100
    uncertainties = np.array(uncertainties)

    comparisons = np.array([narrow_filter, narrow_nofilter, wide_filter, wide_nofilter])
    comparisons_fopt = comparisons*fopts

    # # Energy comparison
    # # -----------------
    # fwhm, fopt, loc_rate, uncert = get_rate(600, (e_windows[0][0], e_windows[0][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwhm_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[0][0], e_windows[0][1]), (e_windows[0][2], e_windows[0][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwtm, fopt, loc_rate, uncert = get_rate(600, (e_windows[1][0], e_windows[1][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fwtm_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[1][0], e_windows[1][1]), (e_windows[1][2], e_windows[1][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # wide, fopt, loc_rate, uncert = get_rate(600, (e_windows[2][0], e_windows[2][1]), (), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # wide_ce, fopt, loc_rate, uncert = get_rate(600, (e_windows[2][0], e_windows[2][1]), (e_windows[2][2], e_windows[2][3]), False)
    # fopts.append(fopt)
    # loc_rates.append(loc_rate)
    # uncertainties.append(uncert)
    #
    # fopts = np.array(fopts)/100
    # uncertainties = np.array(uncertainties)
    #
    # comparisons = np.array([fwhm, fwhm_ce, fwtm, fwtm_ce, wide, wide_ce])
    # comparisons_fopt = comparisons*fopts


    # Plotting a horizontal bar chart comparison
    # ------------------------------------------
    # figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    # plt.rcParams.update({'font.size': 18})

    comparison_labels = ['600ns\nEnergy\nFiltered', '600ns\nUnfiltered', '5000ns\nEnergy\nFiltered', '5000ns\nUnfiltered']

    # comparison_labels = ['FWHM', 'FWHM + CE', 'FWTM', 'FWTM + CE', 'Wide', 'Wide + CE']

    #  create the figure
    fig, ax1 = plt.subplots(figsize=(9, 7), dpi=500, facecolor='w', edgecolor='k')
    fig.subplots_adjust(left=0.135, right=0.9)

    pos = np.arange(len(comparison_labels))

    # Plot the horizontal bars for comparisons with and without the fopt

    # Plot coincidence rates
    rects = ax1.barh(pos, comparisons,
                     align='center',
                     height=0.5,
                     tick_label=comparison_labels,
                     color = ['b', 'b', 'b', 'b'],
                     alpha = 0.3,
                     label = 'Prompt Coincidence Rate'
                     )

    rects_fopt = ax1.barh(pos, comparisons_fopt,
                     align='center',
                     height=0.5,
                     tick_label=comparison_labels,
                     color = ['b', 'b', 'b', 'b'],
                     label = 'F-Optimised Coincidence Rate'
                     )

    # # Plot location rates
    # rects = ax1.barh(pos, loc_rates,
    #                  align='center',
    #                  height=0.5,
    #                  tick_label=comparison_labels,
    #                  color = ['b', 'b', 'b', 'b'])#,
    #                  # color = ['b', 'b', 'b', 'b', 'b', 'b'],
    #                  # alpha = 0.3)

    ax1.set_xlim([0, max(comparisons)*1.08])
    # ax1.set_xlim([0, max(loc_rates)*1.1])

    # Plot vertical dotted lines to help readability
    ax1.xaxis.set_major_locator(MaxNLocator(11))
    ax1.xaxis.grid(True, linestyle='--', which='major',
                   color='grey', alpha=.25)
    ax1.legend(loc = 'lower right')

    ax2 = ax1.twinx()                               # set the right-hand Y-axis ticks and labels
    ax2.set_yticks(pos)                             # set the tick locations
    ax2.set_ylim(ax1.get_ylim())                    # make sure that the limits are set equally on both yaxis so the ticks line up
    ax2.set_yticklabels(np.round_(loc_rates, 2))    # set the tick labels
    # ax2.set_yticklabels(np.round_(uncertainties*1000, 0))    # set the tick labels
    # ax2.set_yticklabels(n_vals)    # set the tick labels

    # ax1.set_xlabel('Location Rate (Hz)')
    ax1.set_xlabel('Coincidence Rate (Hz)')
    ax2.set_ylabel('Location Rate (Hz)')
    # ax2.set_ylabel(r'Radial Uncertainty ($\mu$m)')
    # ax2.set_ylabel(r'Lines-per-Position ($N$)')

    # Lastly, write in the coincidence rate for each bar
    for rect in rects:
        # width = int(rect.get_width())
        width = round(rect.get_width(), 3)
        # Center the text vertically in the bar
        yloc = rect.get_y() + rect.get_height() / 2
        label = ax1.annotate(width, xy=(width, yloc), xytext=(5, 0),
                            textcoords="offset points",
                            ha='left', va='center',
                            color='black', weight='bold', clip_on=True)

    # for rect in rects_fopt:
    #     width = rect.get_width()
    #
    #     xloc = 5
    #     align = 'left'
    #     txtcolor = 'black'
    #
    #     if label_pos == 'left':
    #         xloc = -5
    #         align = 'right'
    #         txtcolor = 'white'
    #
    #     # Center the text vertically in the bar
    #     yloc = rect.get_y() + rect.get_height() / 2
    #     label = ax1.annotate(
    #                         # np.round_(width, 1),
    #                         int(np.round_(width, 0)),
    #                         xy=(width, yloc), xytext=(xloc, 0),
    #                         textcoords="offset points",
    #                         ha=align, va='center',
    #                         color=txtcolor, weight='bold', clip_on=True)

    print('\nWriting results to {}full_param_optimisation-fixed_u.png'.format(data_dir))

    # fig.savefig('{}coin_rates-vs-e_windows.png'.format(data_dir))
    # fig.savefig('{}full_param_optimisation.png'.format(data_dir))
    fig.savefig('{}full_param_optimisation-fixed_u.png'.format(data_dir))
    # plt.show()

    return

# Binary search through N values until a uncertainty in a given range is found
def find_N_for_sigma(data_dir, N_range, uncert_range, f_range):
    '''
    Takes in a coincidences file, a tuple of N values to search between and a
    tuple of uncertainty values. It then binary searches through the N values,
    running an f_optimisation until it finds a uncertainty in the given range.

    @params:
        data_dir            - Required : Path to "pept_coincidence_data" file (Str)
        N_range             - Required : Range of N values between which to search (Int[2])
        uncert_range        - Required : Range of uncertainty values (in mm) within which to end the search (Float[2])
        f_range             - Required : Range of F values to search for Fopt in, expect [start, stop, step] (Int[3])

    @returns:
        N           - Final N value
        fopt        - Fopt value corresponding to that N value
        uncert      - Uncertainty value corresponding to that N value
    '''

    N_upper = N_range[1]
    N_lower = N_range[0]
    N = int((N_upper + N_lower)/2)     # Start with the central N

    print('Searching between', N_lower, 'and', N_upper)
    print('Starting with N =', N)

    # Get the starting uncertainty value
    best_opt, best_pos, best_ucert = f_optimisation(data_dir, f_range[0], f_range[1], f_range[2], N, write_logfile = False, plot_curve = False)
    uncert = np.sqrt(best_ucert[0]**2 + best_ucert[1]**2 + best_ucert[2]**2)
    fopt = best_opt[0]

    repeat_counter = 0       # to prevent infinite interations
    while not (uncert_range[0] <= uncert <= uncert_range[1]):       # while the uncertainty range is not
        N_old = N

        print('Not found. Old range =', N_lower, ',', N_upper)

        # Get new N value
        if uncert < uncert_range[0]:
            print('Uncertainty =', uncert, '<', uncert_range[0])
            N_upper = N
            N = int((N_upper + N_lower)/2)     # Start with the central N
        else:
            print('Uncertainty =', uncert, '>', uncert_range[1])
            N_lower = N
            N = int((N_upper + N_lower)/2)     # Start with the central N

        print('New range =', N_lower, ',', N_upper)
        print('New N =', N)

        # Run f_optimisation and get the uncertainty
        print('Running F-optimisation with N =', N)
        best_opt, best_pos, best_ucert = f_optimisation(data_dir, 5, 100, 5, N, write_logfile = False, plot_curve = False)
        uncert = np.sqrt(best_ucert[0]**2 + best_ucert[1]**2 + best_ucert[2]**2)
        fopt = best_opt[0]

        print('Uncertainty =', uncert)
        print('---------------------------------------------\n\n')

        if N == N_old:
            repeat_counter += 1
        else:
            repeat_counter = 0

        if repeat_counter == 2:
            print('No convergence. Please try a wider uncertainty window.')
            break

    print('Final uncertainty =', uncert, 'mm, with N =', N, 'and Fopt = ', fopt)

    return N, fopt, uncert

# Produce a plot to illustrate the impact of pixel resolution on location uncertainty
def pixel_resolution_illustration(save_dir):
    '''
    Plots a series of figures showing what pixel size does to the detected
    coincidence lines and how it impacts the location uncertainty of a tracer
    particle.

    @params:
        save_dir        - Required: Directory to save images to (Str)

    @returns:
        None
    '''

    save_dir = '{}pixel_demonstration/'.format(save_dir)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
        print ('Created directory: {}'.format(save_dir))

    figure(num=None, figsize=(12, 12), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 18})

    # First plot the lower pixel resolution
    # -------------------------------------
    ax = plt.gca()

    # Start by plotting the outline of the pixels within two detectors
    rect_xs = [0, 30]
    rect_ys = [0, 10]

    for x in rect_xs:
        for y in rect_ys:
            ax.add_patch(Rectangle((x, y), 10, 10, ec = 'k', fc = 'white'))

    # # Shade the area between the lines
    # plt.fill_between([10, 30], [10, 10], [20, 20],
    #                   facecolor = 'red',                 # fill colour
    #                   color = 'red',                     # outline colour
    #                   alpha = 0.2)                              # transparency

    # ax.set_xlim(-5, 45)
    # ax.set_ylim(-5, 25)

    # Next draw tracer with true coincidence lines
    xs = np.linspace(0, 40, 100)

    x_limits = [(8, 36), (4, 31), (2, 37), (1, 31)]

    ms = [-0.2, -0.5, 0.1, 0.4]
    # ms = [-0.2, -0.5, 0.1]

    # Plot the "actual" interactions
    for i, m in enumerate(ms):
        # print(m)
        c = 12 - m*15
        # print(c)
        xs = np.linspace(x_limits[i][0], x_limits[i][1], 10)
        ys = m*xs + c
        plt.plot(xs[0], ys[0], 'gx')
        plt.plot(xs[-1], ys[-1], 'gx')
        ax.plot(xs, ys, 'g--')

    ax.plot([15], [12], 'rX', markersize = 10)       # tracer location

    plt.savefig('{}low_res-true_lines.png'.format(save_dir), bbox_inches = 'tight')
    plt.cla(); plt.clf()


    ax = plt.gca()

    # Start by plotting the outline of the pixels within two detectors
    rect_xs = [0, 30]
    rect_ys = [0, 10]

    for x in rect_xs:
        for y in rect_ys:
            ax.add_patch(Rectangle((x, y), 10, 10, ec = 'k', fc = 'white'))

    # ax.set_xlim(-5, 45)
    # ax.set_ylim(-5, 25)

    # Plot the "recorded" interactions
    plt.plot([5, 35], [15, 5], 'b-o')
    plt.plot([5, 35], [15, 15], 'b-o')
    plt.plot([5, 35], [5, 15], 'b-o')

    plt.plot([20],[12.5], 'rX', markersize = 10)

    plt.savefig('{}low_res-recorded_lines.png'.format(save_dir), bbox_inches = 'tight')
    plt.cla(); plt.clf()


    # Now plot the higher resolution plot
    # -----------------------------------
    ax = plt.gca()

    rect_xs = [0, 5, 30, 35]
    rect_ys = [0, 5, 10, 15]

    for x in rect_xs:
        for y in rect_ys:
            ax.add_patch(Rectangle((x, y), 5, 5, ec = 'k', fc = 'white'))

    # # Shade the area between the lines
    # plt.fill_between([10, 20], [10, 10], [15, 15],
    #                   facecolor = 'red',                 # fill colour
    #                   color = 'red',                     # outline colour
    #                   alpha = 0.2)                              # transparency

    # ax.set_xlim(-5, 45)
    # ax.set_ylim(-5, 25)

    # Next draw tracer with true coincidence lines
    xs = np.linspace(0, 40, 100)

    x_limits = [(8, 36), (4, 31), (2, 37), (1, 31)]

    ms = [-0.2, -0.5, 0.1, 0.4]
    # ms = [-0.2, -0.5, 0.1]

    # Plot the "actual" interactions
    for i, m in enumerate(ms):
        # print(m)
        c = 12 - m*15
        # print(c)
        xs = np.linspace(x_limits[i][0], x_limits[i][1], 10)
        ys = m*xs + c
        plt.plot(xs[0], ys[0], 'gx')
        plt.plot(xs[-1], ys[-1], 'gx')
        ax.plot(xs, ys, 'g--')

    ax.plot([15], [12], 'rX', markersize = 10)       # tracer location

    plt.savefig('{}high_res-true_lines.png'.format(save_dir), bbox_inches = 'tight')
    plt.cla(); plt.clf()



    ax = plt.gca()

    rect_xs = [0, 5, 30, 35]
    rect_ys = [0, 5, 10, 15]

    for x in rect_xs:
        for y in rect_ys:
            ax.add_patch(Rectangle((x, y), 5, 5, ec = 'k', fc = 'white'))

    # Plot the "recorded" interactions
    plt.plot([2.5, 32.5], [17.5, 2.5], 'b-o')
    plt.plot([7.5, 37.5], [12.5, 7.5], 'b-o')
    plt.plot([2.5, 37.5], [12.5, 12.5], 'b-o')
    plt.plot([2.5, 32.5], [7.5, 17.5], 'b-o')

    plt.plot([14.5],[11.7], 'rX', markersize = 10)     # 'recorded' location

    plt.savefig('{}high_res-recorded_lines.png'.format(save_dir), bbox_inches = 'tight')
    plt.cla(); plt.clf()

    return

# Monte Carlo method to calculate the solid angle subtended by the PolarisJ detectors
def monte_carlo_solid_angle(det_separation, num_crystals = 2):
    '''
    Uses a Monte Carlo method to calculate the solid angle subtended by the UCT
    PolarisJ detectors arranged directly opposite each other. Assumes a central
    source.

    @params:
        det_separation      - Required: Detector separation in millimeters (Float)
        num_crystals        - Optional: Whether there are 2 or 4 crystals in the module (Int, default = 2)

    @returns:

    '''

    source_activity = 16350     # activity in Bq
    # source_activity = 10000   # activity in Bq
    radius = 50                 # radius of sphere in millimeters
    num_trials = 1000

    solid_angles = []

    tot_crystal_width = 42
    tot_crystal_height = 18.2

    for i in range(num_trials):

        # Generate random coordinates uniformly distributed on the surface of a sphere
        zs = np.random.uniform(low = -radius, high = radius, size = source_activity)
        phis = np.random.uniform(low = 0, high = 2*np.pi, size = source_activity)

        # Convert to cartesian coordinates
        rs = np.sqrt(radius**2 - zs**2)
        xs = rs*np.sin(phis)
        ys = rs*np.cos(phis)

        # --------------------------------------------------------------------------
        # Checking each gamma-ray to see if it intersected with the detector faces
        # All distance units are in millimetres
        # Solution taken from: https://en.wikipedia.org/wiki/Line–plane_intersection
        # --------------------------------------------------------------------------

        if num_crystals == 2:
            # Vectors to define the detector plane
            det_plane_vec1 = np.array([0, 0, 1])
            det_plane_vec2 = np.array([0, 1, 0])
            origin_to_plane = np.array([det_separation, 0, 0])  # vector from source to detector face

            vec1_cross_vec2 = np.cross(det_plane_vec1, det_plane_vec2)
            sol_numerator = np.dot(vec1_cross_vec2, -origin_to_plane)

            origin_to_spheres = np.array([xs, ys, zs]).T        # array of vectors from the source to the surface of a sphere around the source

            # Module 1
            # --------
            origin_to_spheres_mod0 = origin_to_spheres[ origin_to_spheres[:,0] > 0 ]        # only consider the gammas in the direction of module 0
            ts = sol_numerator/np.dot(-origin_to_spheres_mod0, vec1_cross_vec2)
            intersections = origin_to_spheres_mod0*ts[:,None]                               # get the gammas that intersected with the plane of module 0
            # print(len(origin_to_spheres_mod0))

            # Now remove values that didn't intersect with module 0
            intersections_mod0 = intersections[
                np.logical_and(
                    np.logical_and(
                        np.logical_and(intersections[:,1] < tot_crystal_width/2, intersections[:,1] > -tot_crystal_width/2),
                        np.logical_and(intersections[:,2] < tot_crystal_height/2, intersections[:,2] > -tot_crystal_height/2)
                    ),
                    np.logical_not(
                        np.logical_and(intersections[:,1] < 1, intersections[:,1] > -1)
                    )
                )
            ]


            # Module 2
            # --------
            origin_to_spheres_mod1 = origin_to_spheres[ origin_to_spheres[:,0] < 0 ]        # only consider the gammas in the direction of module 1
            ts = -sol_numerator/np.dot(-origin_to_spheres_mod1, vec1_cross_vec2)
            intersections = origin_to_spheres_mod1*ts[:,None]                               # get the gammas that intersected with the plane of module 1
            # print(len(origin_to_spheres_mod1))

            # Now remove values that didn't intersect with module 1
            intersections_mod1 = intersections[
                np.logical_and(
                    np.logical_and(
                        np.logical_and(intersections[:,1] < tot_crystal_width/2, intersections[:,1] > -tot_crystal_width/2),
                        np.logical_and(intersections[:,2] < tot_crystal_height/2, intersections[:,2] > -tot_crystal_height/2)
                    ),
                    np.logical_not(
                        np.logical_and(intersections[:,1] < 1, intersections[:,1] > -1)
                    )
                )
            ]

            # The solid angle is the ratio of the intersected lines to the total number of lines
            solid_angle = (len(intersections_mod0) + len(intersections_mod1)) / len(zs)

            solid_angles.append(solid_angle)

            # print('Solid angle = ', solid_angle)

        elif num_crystals == 4:
            tot_crystal_width = 42
            tot_crystal_height = 42

            # Vectors to define the detector plane
            det_plane_vec1 = np.array([0, 0, 1])
            det_plane_vec2 = np.array([0, 1, 0])
            origin_to_plane = np.array([det_separation, 0, 0])  # vector from source to detector face

            vec1_cross_vec2 = np.cross(det_plane_vec1, det_plane_vec2)
            sol_numerator = np.dot(vec1_cross_vec2, -origin_to_plane)

            origin_to_spheres = np.array([xs, ys, zs]).T        # array of vectors from the source to the surface of a sphere around the source

            # Module 1
            # --------
            origin_to_spheres_mod0 = origin_to_spheres[ origin_to_spheres[:,0] > 0 ]        # only consider the gammas in the direction of module 0
            ts = sol_numerator/np.dot(-origin_to_spheres_mod0, vec1_cross_vec2)
            intersections = origin_to_spheres_mod0*ts[:,None]                               # get the gammas that intersected with the plane of module 0
            # print(len(origin_to_spheres_mod0))

            # Now remove values that didn't intersect with module 0
            intersections_mod0 = intersections[
                np.logical_and(
                    np.logical_and(
                        np.logical_and(intersections[:,1] < tot_crystal_width/2, intersections[:,1] > -tot_crystal_width/2),
                        np.logical_and(intersections[:,2] < tot_crystal_height/2, intersections[:,2] > -tot_crystal_height/2)
                    ),
                    np.logical_not(
                        np.logical_or(
                            np.logical_and(intersections[:,1] < 1, intersections[:,1] > -1),
                            np.logical_and(intersections[:,2] < 1, intersections[:,2] > -1)
                        ),
                    )
                )
            ]

            # The solid angle is the ratio of the intersected lines to the total number of lines
            solid_angle = len(intersections_mod0) / len(zs)

            solid_angles.append(solid_angle)

        else:
            print('{:d} crystals is not yet supported.'.format(num_crystals))
            return

    N, minmax, mean, var, skew, k = stats.describe(solid_angles)

    print('Solid angle = {:.4f} +- {:.4f}'.format(mean, np.sqrt(var/N)))

    # plt.hist(solid_angles, 30)
    # plt.show()
    #
    # # Visualising the results
    # # -----------------------
    # fig = plt.figure()
    # ax = plt.axes(projection='3d')
    #
    # # ax.scatter3D(xs, ys, zs, color = 'g')
    # # ax.scatter3D(-xs, -ys, -zs, color = 'r')
    # ax.scatter3D(intersections_mod0[:,0],intersections_mod0[:,1],intersections_mod0[:,2], color = 'r')
    # # ax.scatter3D(intersections_mod1[:,0],intersections_mod1[:,1],intersections_mod1[:,2], color = 'r')
    #
    # # Draw the surface of a sphere
    # u = np.linspace(0, 2 * np.pi, 100)
    # v = np.linspace(0, np.pi, 100)
    #
    # x = (radius-1) * np.outer(np.cos(u), np.sin(v))
    # y = (radius-1) * np.outer(np.sin(u), np.sin(v))
    # z = (radius-1) * np.outer(np.ones(np.size(u)), np.cos(v))
    # # ax.plot_surface(x, y, z, rstride=1, cstride=1, color='w', shade=0)
    # ax.plot_surface(x, y, z, alpha = 0.5)
    #
    # # ax.set_xlim(-50, 50)
    # # ax.set_ylim(-50, 50)
    # plt.show()

    return

# Plot a histogram of the time in between events to check for Poisson behaviour
def time_between_events(data_dir, write_dir, filename = 'AllEventsCombined.txt', det_delay = 0, module_file = False, module = -1, apply_energy_filter = False, e_window = [], bin_width = 'auto', cutoff_diff = -1):
    '''
    Takes in an AllEventsCombined.txt file, takes the time difference between
    successive events and plots a histogram of the results to check for the
    expected Poisson behaviour. Plots the final results in units of microseconds.

    @params:
        data_dir                - Required : Path to the AllEventsCombined.txt file (Str)
        write_dir               - Required : Directory to save figure to (Str)
        filename                - Optional : Name of the file to read in; defaults to combined data file (Str, default = 'AllEventsCombined.txt')
        det_delay               - Optional : Add a specific delay (in nanoseconds) to detector to correct for any timing misaligment (Int, default = 0)
        module_file             - Optional : Boolean to tell the function if it's an AllEventsCombined.txt file or module file (i.e. 'mod51.txt/AllEvents.txt') (Bool, default = False)
        module                  - Optional : Used to select just a single module from the combined data files. If -1, all modules are included (Int, default = -1)
        apply_energy_filter     - Optional : Whether or not to apply energy filtering on the data (Bool, default = False)
        e_window                - Optional : Upper and lower energy windows to filter events on in keV. Includes Compton edge filtering (Int[4])
        bin_width               - Optional : Specify the width of the bins in microseconds (Int/Str, default = 'auto')
        cutoff_diff             - Optional : Remove differences about a given difference in microseconds. If -1, includes all events. (Int, default = -1)


    @returns:

    '''
    matplotlib.rc('font',family='Palatino')
    figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 25})

    if not module_file:
        allevents, log_str = read_polaris_data(data_dir + filename)
        allevents = allevents[allevents.index < int(len(allevents.index)*0.05)]
        print('Adding time delay and resorting...')
        allevents.loc[allevents.detector == 1, 'time'] += (det_delay/10)                    # add time delay (in 10s of ns)
        allevents.sort_values(by = 'time', inplace = True)                                  # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                                  # reset the index numberin

        if apply_energy_filter:
            'Applying energy filter...'
            allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                       ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                       (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
            allevents.reset_index(drop = True, inplace = True)

        print('Dropping multiple-interaction events...')
        allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)            # only use single scatter events

        if module != -1:
            print('Dropping all events not in module {}...'.format(module))
            allevents.drop(allevents[allevents.detector != module].index, inplace = True)

        allevents.reset_index(drop = True, inplace = True)

        print()
        print('{:50} {:d}'.format('Final number of events:', len(allevents.index)))
        print('{:50} {:.1f}'.format('Total time (seconds):', (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8))
        print('{:50} {:.0f}'.format('Final event rate (events/sec):', len(allevents.index) / ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8) ))

    else:
        data_file = data_dir + filename
        print('Reading in data from', data_file + '.', '\nThis may take several minutes...')

        allevents = pd.read_csv(data_file, sep = '	', header = None)                        # read in text file as a csv with tab separation
        print('Done.')
        # allevents.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']       # separate into columns and name them
        allevents.columns = ['scatters', 'empty', 'energy', 'x', 'y', 'z', 'time']       # separate into columns and name them

        allevents.drop(allevents[allevents.scatters == 122].index, inplace = True)      # remove the sync pulses

        num_events = len(allevents.index)
        tot_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8

        print()
        print('{:30} {:d}'.format('Total number of events:', num_events))
        print('{:30} {:.1f}'.format('Total time (seconds):', tot_time))
        print('{:30} {:.0f}'.format('Event rate (events/sec):', num_events/tot_time) )

        # allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)

    t_diff = allevents.time.diff().to_numpy()[1:]      # get the time difference between successive events
    t_diff *= 1e-2                                 # convert to microseconds

    if cutoff_diff != -1:
        print('Removing time differences greater than {}us...'.format(cutoff_diff))
        t_diff = t_diff[t_diff < cutoff_diff]           # drop differences above given time

    if bin_width != 'auto':
        print('Setting bin width to {}...'.format(bin_width))
        num_bins = int(np.ceil((max(t_diff) - min(t_diff))/bin_width))       # get number of bins based on width
    else:
        num_bins = 'auto'

    print()
    print('Plotting histogram of time differences...')
    n, bins, patches = plt.hist(t_diff, 1000, density = False)

    # Fit a time-interval density function to the curve to extract the event rate
    # ---------------------------------------------------------------------------
    # bins = bins[1:]/2     # remove left-most edge and get bin centre values
    #
    # N0 = len(t_diff)
    # dt0 = bins[0]*2
    # tot_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8
    # r0 = N0/(tot_time)
    #
    # print(N0)
    # print(dt0)
    # print(r0)
    # f = lambda t,N,r,dt : N*r*np.exp(-r*t)*dt
    # popt, pcov = curve_fit(f, bins, n, [N0, r0, dt0])
    # # f = lambda t,r : N0*r*np.exp(-r*t)*dt0
    # # popt, pcov = curve_fit(f, bins, n, [r0])
    # x_fit = np.linspace(min(bins), max(bins), 1000)
    # y_fit = f(x_fit, *popt)
    # plt.plot(x_fit, y_fit)
    #
    # print( 'Measured event rate: {:.0f}Hz'.format(N0/tot_time) )
    # print( 'Fitted event rate: {:.0f}Hz'.format(popt[0]))

    # plt.ylim(0,100)
    plt.xlabel(r'Time difference ($\mu$s)')
    plt.ylabel('Number of events')
    # plt.ylabel('Normalised Intensity')

    print('Saving figure to', write_dir + 'time_between_events-0_2000us-module1.png')
    plt.savefig(write_dir + 'time_between_events-0_2000us-module1.png')
    # plt.show()

    return

# Function to read the .mod binary files and convert them to .txt
def read_mod_binary(data_dir, filename):
    '''
    Takes in the data directory and name of the .mod binary file, decodes it and writes it to a .txt file.

    Expected binary format:

    The first byte indicates what will follow.

    If the first byte is 122, then it indicates a timestamp data and is in the following format:
        1 byte: 122
        1 byte: 2
        8 byte: Sync pulse index
        8 byte: Sync pulse time stamp

    If the first byte is 255, then it indicates mask position data. Skip the next 23 bytes.

    @params:
        data_dir            - Required : Path to the data file (Str)
        filename            - Required : Name of the .mod file (Str)
        output_dir          - Required : directory to write the merged data to (Str)

    @returns:
        None
    '''

    output_file = '{}.txt'.format(filename[:filename.rfind('.')])   # Create an output filename from the given filename
    input_file = data_dir + filename

    # Reading in binary data file
    # ---------------------------
    print ('Reading input file', input_file, '. Processing binary and writing output to text; could take several minutes . . .')
    print ('   - Filename: {}'.format(input_file))
    print ('   - Size: {} bytes'.format(os.path.getsize(input_file)))
    print()

    mod_data_dict = []

    # Open and read binary:
    with open(input_file, "rb") as binary_file:

        """
        The sync pulse data array has the following format:

        [
            [Sync pulse indicator (122)],
            [Number of sync pulse],
            [Sync pulse timestamp (10ns)]
        ]
        """

        curr_pos = 0            # current read position in the file
        num_pulses = 0        # to count number of sync pulses read out
        byteorder = 'big'

        skip_header = True      # boolean to skip the file header

        while True:

            # print(curr_pos)

            # testing for end of file
            curr_pos_test = binary_file.tell()
            test = binary_file.read(1)
            if not test:
                break
            binary_file.seek(curr_pos_test)

            # Seek a specific position in the file
            binary_file.seek(curr_pos, 0)

            # Number of interactions
            indicator = binary_file.read(1)
            indicator_int = int.from_bytes(indicator, byteorder=byteorder)

            # print(indicator_int)

            if indicator_int == 122:

                next_byte = binary_file.read(1)     # next byte should be a 2

                # Read index and time stamp information
                index = int.from_bytes(binary_file.read(8), byteorder=byteorder)
                timestamp = int.from_bytes(binary_file.read(8), byteorder=byteorder)

                # Add coincidence data to dictionary list
                mod_data_dict.append({'scatters':122, 'x':index, 'time':timestamp})

                curr_pos += 18

            # elif indicator_int == 255:
            #     print('Mask information - skip')
            #     curr_pos += 24

            elif indicator_int == 0:
                # print('Mask information - skip')
                curr_pos += 24

            else:
                timestamp = int.from_bytes(binary_file.read(8), byteorder=byteorder)     # get the timestamp
                curr_pos += 9

                for i in range(indicator_int):      # for each interaction
                    det_num = int.from_bytes(binary_file.read(1), byteorder=byteorder)     # detector number
                    x = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                    y = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                    z = int.from_bytes(binary_file.read(4), byteorder=byteorder, signed = True)     # get the timestamp
                    energy = int.from_bytes(binary_file.read(4), byteorder=byteorder)     # get the timestamp

                    mod_data_dict.append({'scatters':indicator_int, 'x':x, 'y':y, 'z':z,'energy':energy, 'time':timestamp})

                    curr_pos += 17

    mod_data = pd.DataFrame(mod_data_dict, columns = ['scatters', 'x', 'y', 'z', 'energy', 'time'], dtype = int)
    return mod_data

# Count the coincidences of a given allevents DataFrame
def count_coincidences(allevents, coin_window = 600, print_results = False):
    '''
    Takes in an allevents DataFrame of the detector data with the sync-pulse
    correction already completed, goes through it and counts the coincidences
    using the given timing window

    @params:
        allevents       - Required : DataFrame of the combined Polaris detector data (DataFrame)
        coin_window     - Optional : Coincidence window (2tau) in nanoseconds (int, default = 600)

    @returns:
        tot_coincidences    - Total number of coincidences in allevents
        coincidence_rate    - Coincidence rate in allevents
    '''
    # print('1')

    allevents_np_ = allevents.to_numpy()     # convert to numpy array for easier counting
    coin_window /= 10
    # print('2')

    allevents_np = np.zeros((len(allevents_np_),8))
    allevents_np[:,:-1] = allevents_np_
    # print('3')

    tot_coincidences = 0
    tot_mult_coins = 0          # total number of multiple coincidences (more than one coincidence found)
    for i, curr_event in enumerate(allevents_np):
        # if i%1000 == 0:
        #     print(i)
        # print('Current event:', curr_event)

        current_event_time = curr_event[6]
        # print('Current event time:', current_event_time)

        j = i+1   # get next event index
        if j == len(allevents_np):       # make sure not to iterate past the end
            break

        mult_coins = -1
        # print('Next event time:', allevents_np[j][-1])
        # print('Upper window: ', current_event_time + coin_window)
        while allevents_np[j][6] <= current_event_time + coin_window:
            # print(i)
            # print('Upper window = ', current_event_time + 30)
            # print('Current event = ', curr_event)
            # print('Next event = ', allevents_np[j])
            # print()
            # print('Next event detector module:', allevents_np[j][1])
            if (curr_event[7] != 1) and (allevents_np[j][7] != 1) and (allevents_np[j][1] != curr_event[1]):      # if not counted and not in the same detector
                tot_coincidences += 1
                mult_coins += 1
                curr_event[7] = 1
                allevents_np[j][7] = 1

            j += 1
            if j == len(allevents_np):
                break

        if mult_coins > 0:
            tot_mult_coins += 1

    tot_time = ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
    coin_rate = tot_coincidences/tot_time

    if print_results:
        print('Total Number of Coincidences: {:d}'.format(tot_coincidences))
        print('Final coincidence rate: {:.2f}Hz'.format(coin_rate))
        print('Total number of multiple coincidence events: {:d}'.format(tot_mult_coins))

    return tot_coincidences, coin_rate

# Plot an example plot of the deadtime models
def deadtime_singles_model_examples():
    '''
    Plots an example of the deadtime model to demonstrate the shape of the
    curves.

    @params:
        None

    @returns:
        None
    '''
    save_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/Write Ups/Thesis/PolarisPEPT Thesis/Figures/'

    # Non-paralysable deadtime model
    def np_model(n, t_np):
        return n/(1 + n*t_np)

    # Paralysable deadtime model
    def p_model(n, t_p):
        return n*np.exp(-n*t_p)

    # font = {'fontname' : 'Palatino'}

    matplotlib.rc('font',family='Palatino')
    figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 25})

    # Plot the model curves
    n = np.linspace(0, 3, 100)
    plt.plot(n,n, label = 'Linear Response')
    plt.plot(n, np_model(n, 1), label = 'Non-paralysable')
    plt.plot(n, p_model(n, 1), label = 'Paralysable')

    # Plot dotted lines to indicate important points
    plt.plot([1, 1], [0, 1.5], '--', color = 'grey')
    plt.plot([0, 3], [1/np.exp(1), 1/np.exp(1)], '--', color = 'grey')
    plt.plot([0, 3], [1, 1], '--', color = 'grey')

    plt.xlim(0, 3)
    plt.ylim(0, 1.5)

    plt.xlabel(r'True Rate, $n$ (Hz)')#, fontsize = 15)
    plt.ylabel(r'Measured Rate, $m$ (Hz)')#, fontsize = 15)

    plt.xticks([1], [r'$\frac{1}{\tau}$'], fontsize = 30)
    plt.yticks([1/np.exp(1), 1], [r'$\frac{1}{\tau_p e}$', r'$\frac{1}{\tau_{np}}$'], fontsize = 30)

    plt.legend()#fontsize = 15)
    plt.savefig(save_dir + 'deadtime_models.png')
    # plt.show()

    return

# Plot an example plot of the deadtime models
def deadtime_coincidences_model_examples():
    '''
    Plots an example of the deadtime model for coincidence rates to demonstrate
    the shape of the curves.

    @params:
        None

    @returns:
        None
    '''
    save_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/Write Ups/Thesis/PolarisPEPT Thesis/Figures/'

    # Non-paralysable deadtime model
    def np_model(n, t_np):
        return n/(1 + n*t_np)

    # Paralysable deadtime model
    def p_model(n, t_p):
        return n*np.exp(-n*t_p)

    def true_rate(n, e, A, tdt):
        return n*e*(1/(1+A*tdt))

    def random_rate(n, t):
        return t*n**2

    def prompt_rate(n, e, t, A, tdt):
        return true_rate(n, e, A, tdt) + random_rate(n, t)

    # font = {'fontname' : 'Palatino'}

    matplotlib.rc('font',family='Palatino')
    figure(num=None, figsize=(16, 9), dpi=500, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 25})

    # Plot the model curves with no deadtime
    # A = np.linspace(0, 1.5, 100)
    # n = A
    # plt.plot(n, prompt_rate(n, 1, 1), label = 'Prompt Rate')
    # plt.plot(n, random_rate(n, 1), label = 'Random Rate')
    # plt.plot(n, true_rate(n, 1), label = 'True Rate')

    # Plot the model curves with deadtime
    e = 1     # efficiency
    t = 0.5     # coincidence window
    tdt = 1     # deadtime param
    A = np.linspace(0, 5, 500)
    n = np_model(A, tdt)
    plt.plot(A, n, label = 'Singles Rate')
    plt.plot(A, prompt_rate(n, e, t, A, tdt), label = 'Prompt Rate')
    plt.plot(A, random_rate(n, t), label = 'Random Rate')
    plt.plot(A, true_rate(n, e, A, tdt), label = 'True Rate')

    # Plot dotted lines to indicate important points
    # plt.plot([1, 1], [0, 1.5], '--', color = 'grey')
    # plt.plot([0, 3], [1/np.exp(1), 1/np.exp(1)], '--', color = 'grey')
    # plt.plot([0, 3], [1, 1], '--', color = 'grey')

    plt.xlim(0, 5)
    plt.ylim(0, 1)

    plt.xlabel(r'Activity, A (Bq)')#, fontsize = 15)
    plt.ylabel(r'Expected Rate (Hz)')#, fontsize = 15)

    # plt.xticks([1], [r'$\frac{1}{\tau}$'], fontsize = 30)
    # plt.yticks([1/np.exp(1), 1], [r'$\frac{1}{\tau_p e}$', r'$\frac{1}{\tau_{np}}$'], fontsize = 30)

    plt.xticks([], [])
    plt.yticks([], [])

    plt.legend()#fontsize = 15)
    # plt.savefig(save_dir + 'coincidence_models-no_deadtime.png')
    plt.savefig(save_dir + 'coincidence_models-deadtime.png')
    # plt.show()

    return

# Performs a series of fits to produce the coincidence model predictions on top of the source data
def build_coincidence_models(data_dir, write_dir, sing_ratefiles, prompt_coin_ratefiles, rand_coin_ratefiles, branch_ratio, coin_window, all_coin_models = True):
    '''
    Takes in a data directory and a series of filenames to rate files for
    singles rates for two modules, plus prompt and random coincidence rate
    files. Reads in the singles and extracts the non-paralysable deadtime
    parameter, then reads in the coincidences and extracts efficiency and
    fractional solid angle terms. Finally plots the coincidence rates with the
    coincidence models.

    @params:
        data_dir                - Required : Path to the rate files (Str)
        write_dir               - Required : Path to save the final figure to (Str)
        sing_ratefiles          - Required : List of lists of rate files for each module (Str[])
        prompt_coin_ratefile    - Required : List of rate file paths for prompt coincidences (Str)
        rand_coin_ratefile      - Required : List of rate file paths for random coincidences (Str)
        branch_ratio            - Required : Branching ratio of the source isotope (Float)
        coin_window             - Required : Coincidence window (2tau) in nanseconds used to count coincidences (Int)
        all_coin_models         - Optional : Whether to plot all the coincidence curves or just the True curve (Bool, default = True)

    @returns:
        None

    '''
    # figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    # plt.rcParams.update({'font.size': 20})

    # Lists to store deadtime parameters and rates data from each module
    taus, cs = [], []                       # deadtime parameters and activity function y-intercepts
    mod_activities, mod_rates = [], []      # activities and rates
    fit_cutoff_activity = 2.4e6             # cutoff activity for parameter fits
    plot_cutoff_activity = 3.5e6            # cutoff activity for parameter fits
    n = 40                                  # number of events to average over

    for i in range(2):      # get rate files for each module
        activities, rates = [], []
        for j in range(len(sing_ratefiles[i])):

            sing_ratefile = sing_ratefiles[i][j]

            # Read in the rate file
            rate_file = data_dir + sing_ratefile
            activities_, rates_ = np.genfromtxt(rate_file, delimiter = ',', dtype = np.float64, unpack = True)
            activities_ *= 1000       # convert units to Bq

            # Remove nan or inf values
            del_indicies = []
            for k in range(len(rates_)):
                if np.isinf(activities_[k]) or np.isinf(rates_[k]) or np.isnan(activities_[k]) or np.isnan(rates_[k]):
                    del_indicies.append(k)

            activities_ = np.delete(activities_, del_indicies)
            rates_ = np.delete(rates_, del_indicies)

            activities.append(activities_)
            rates.append(rates_)

        # Flatten the lists
        activities = [r for sublist in activities for r in sublist]
        rates = [r for sublist in rates for r in sublist]

        activities = np.array(activities)
        rates = np.array(rates)

        mod_activities.append(activities)
        mod_rates.append(rates)

        # plt.plot(activities, rates, '.')
        # plt.show()

        # Average over n rows
        activities_avg = np.nanmean(np.pad(activities.astype(float), (0, n - activities.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
        rates_avg = np.nanmean(np.pad(rates.astype(float), (0, n - rates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)

        # plt.plot(activities_avg, rates_avg, '.')
        # plt.show()

        # Perform non-paralysable deadtime model fit for the deadtime parameter
        # ---------------------------------------------------------------------

        # Only perform deadtime fit to subset of the curve
        fit_data = np.vstack((activities_avg, rates_avg)).T
        fit_data = fit_data[fit_data[:,0] < fit_cutoff_activity]
        activities_avg = fit_data[:,0]; rates_avg = fit_data[:,1]

        # Arrays the x and y values used in the linear fit. Values are dependent on the model
        model_x = []
        model_y = []

        model_x = rates_avg
        model_y = rates_avg/activities_avg            # generate the activity function m/A

        # # Put here to fix nan issues with linear fit
        # model_x = model_x[150:-150]
        # model_y = model_y[150:-150]

        # Remove piece of curve for better fit
        mdata = np.vstack((model_x, model_y)).T
        mdata = mdata[mdata[:,1] < 0.022]
        model_x = mdata[:,0]; model_y = mdata[:,1]

        # Perform a linear fit and regression fit
        _m, _c, r, p_value, std_err = stats.linregress(model_x, model_y)
        m, um, c, uc = linfit.linfit(model_x, model_y)

        tau = -m/c                                      # deadtime parameter
        u_tau = tau*np.sqrt((um/m)**2 + (uc/c)**2)      # deadtime parameter uncertainty

        # Plotting the results
        # --------------------
        # plt.plot(model_x, model_y, '.')     # plot the results
        # plt.xlabel('Single detector rate (Hz)')
        # plt.ylabel('Activity function (m/A)')
        #
        # # Plot the linear fit and paramters
        # fit_x = np.linspace(min(model_x), max(model_x))
        # plt.plot(fit_x, linear(fit_x, m, c),
        #          label = r'm = {:.6f} $\pm$ {:.6f}'.format(m*1e6, um*1e6) + '\n' +
        #                  r'c = {:.4f} $\pm$ {:.4f}'.format(c*1e3, uc*1e3) + '\n' +
        #                  r'r$^2$ = {:.4f}'.format(r**2) + '\n' +
        #                  r'$\tau$ = {:.2f} $\pm$ {:.5f} $\mu$s'.format(tau*1e6, u_tau))
        # plt.legend()
        # plt.show()

        # print(tau)

        taus.append(tau)
        cs.append(c)

    tau = np.mean(taus)         # get the average of the deadtime parameters
    # print(taus)
    # print(cs[0]/branch_ratio)
    # print(cs[1]/branch_ratio)

    print('Average deadtime parameter: {:.2f}us'.format(tau*1e6))

    abs_eff = np.mean(cs)/branch_ratio        # absolute efficiency

    print('Absolute efficiency: {:.2f}%'.format(100*abs_eff))

    # Read in coincidence rates and fit intrinsic efficiency and fractional solid angle
    # ---------------------------------------------------------------------------------

    # Read in the prompt coincidences rate files
    coin_activities, prompt_coinrates = [], []
    for prompt_coin_ratefile in prompt_coin_ratefiles:

        rate_file = data_dir + prompt_coin_ratefile
        coin_activities_, prompt_coinrates_ = np.genfromtxt(rate_file, delimiter = ',', dtype = np.float64, unpack = True)

        # Remove nan or inf values
        del_indicies = []
        for i in range(len(prompt_coinrates_)):
            if np.isinf(coin_activities_[i]) or np.isinf(prompt_coinrates_[i]) or np.isnan(coin_activities_[i]) or np.isnan(prompt_coinrates_[i]):
                del_indicies.append(i)
        coin_activities_ = np.delete(coin_activities_, del_indicies)
        prompt_coinrates_ = np.delete(prompt_coinrates_, del_indicies)

        # Correct second dataset for geometry differences
        if 'run16' in prompt_coin_ratefile:
            d = np.vstack((coin_activities_, prompt_coinrates_)).T
            d = d[d[:,0] > 2.4e3]
            coin_activities_ = d[:,0]; prompt_coinrates_ = d[:,1]

        coin_activities.append(coin_activities_)
        prompt_coinrates.append(prompt_coinrates_)

    # Flatten the lists
    coin_activities = [r for sublist in coin_activities for r in sublist]
    prompt_coinrates = [r for sublist in prompt_coinrates for r in sublist]

    coin_activities = np.array(coin_activities)
    prompt_coinrates = np.array(prompt_coinrates)

    # plt.plot(coin_activities, prompt_coinrates, '.', color = 'lightblue', label = 'Prompt Rate')
    # plt.show()
    # return

    # Read in the random coincidences rate files
    coin_activities, rand_coinrates = [], []
    for rand_coin_ratefile in rand_coin_ratefiles:
        rate_file = data_dir + rand_coin_ratefile
        coin_activities_, rand_coinrates_ = np.genfromtxt(rate_file, delimiter = ',', dtype = np.float64, unpack = True)
        coin_activities_ *= 1000       # convert units to Bq

        # Remove nan or inf values
        del_indicies = []
        for i in range(len(rand_coinrates_)):
            if np.isinf(coin_activities_[i]) or np.isinf(rand_coinrates_[i]) or np.isnan(coin_activities_[i]) or np.isnan(rand_coinrates_[i]):
                del_indicies.append(i)
        coin_activities_ = np.delete(coin_activities_, del_indicies)
        rand_coinrates_ = np.delete(rand_coinrates_, del_indicies)

        # Correct second dataset for geometry differences
        if 'run16' in rand_coin_ratefile:
            d = np.vstack((coin_activities_, rand_coinrates_)).T
            d = d[d[:,0] > 2.4e3]
            coin_activities_ = d[:,0]; rand_coinrates_ = d[:,1]

        coin_activities.append(coin_activities_)
        rand_coinrates.append(rand_coinrates_)

    # Flatten the lists
    coin_activities = [r for sublist in coin_activities for r in sublist]
    rand_coinrates = [r for sublist in rand_coinrates for r in sublist]

    coin_activities = np.array(coin_activities)
    rand_coinrates = np.array(rand_coinrates)

    concat_index = min([len(prompt_coinrates), len(rand_coinrates), len(coin_activities)])
    prompt_coinrates = prompt_coinrates[:concat_index]
    rand_coinrates = rand_coinrates[:concat_index]
    coin_activities = coin_activities[:concat_index]

    fit_prompt_coinrates = prompt_coinrates
    fit_rand_coinrates = rand_coinrates
    fit_data = np.vstack((coin_activities, fit_prompt_coinrates, fit_rand_coinrates)).T
    fit_data = fit_data[fit_data[:,0] < fit_cutoff_activity]
    fit_prompt_coinrates = fit_data[:,1]; fit_rand_coinrates = fit_data[:,2]

    true_coinrates = prompt_coinrates - rand_coinrates
    fit_true_coinrates = fit_prompt_coinrates - fit_rand_coinrates

    # Remove events below the cutoff activity for both modules
    fit_activities = mod_activities[0]; fit_singrates_mod0 = mod_rates[0];
    fit_data = np.vstack((fit_activities, fit_singrates_mod0)).T
    fit_data = fit_data[fit_data[:,0] < fit_cutoff_activity]
    fit_singrates_mod0 = fit_data[:,1]

    fit_activities = mod_activities[1]; fit_singrates_mod1 = mod_rates[1];
    fit_data = np.vstack((fit_activities, fit_singrates_mod1)).T
    fit_data = fit_data[fit_data[:,0] < fit_cutoff_activity]
    fit_singrates_mod1 = fit_data[:,1]

    rates_avg = np.mean([np.mean(fit_singrates_mod0), np.mean(fit_singrates_mod1)])
    trues_avg = np.mean(fit_true_coinrates)
    int_eff = trues_avg/rates_avg     # intrinsic efficiency
    # int_eff = 0.076
    # int_eff = 0.065
    int_eff = 0.072

    print('Intrinsic efficiency: {:.2f}%'.format(100*int_eff))

    frac_solid_angle = abs_eff/int_eff

    print('Fractional solid angle: {:.2f}'.format(frac_solid_angle))

    # ------------------------------------------------------------
    # Plot the final coincidence rates and coincidence rate models
    # ------------------------------------------------------------

    # Non-paralysable deadtime model
    def np_model(n, t_np):
        return n/(1+n*t_np)

    tau = 30e-6
    model_activities = np.linspace(0, max(activities), 1000)        # source activity for the model
    model_ns = model_activities*branch_ratio*abs_eff                # get the theoretical true rate
    model_ms = np_model(model_ns, tau)                              # get the theoretical measured rate

    # # plt.plot(model_activities, model_ns)
    # plt.plot(model_activities, model_ms)
    # plt.plot(activities, rates, '.')
    # plt.show()
    # # return

    coin_window *= 1e-9     # convert coin window to units of seconds

    model_true = model_ms*int_eff*(1/(1+model_ns*tau))                                   # get the theoretical true coincidence rate
    # model_true = model_ms*abs_eff/0.1037                                   # get the theoretical true coincidence rate
    model_rand = coin_window*(model_ms**2)                          # get the theoretical random coincidence rate
    model_prompt = model_true + model_rand

    model_activities /= 1000
    coin_activities /= 1000

    # Average over n rows
    coin_activities_avg = np.nanmean(np.pad(coin_activities.astype(float), (0, n - coin_activities.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
    prompt_coinrates_avg = np.nanmean(np.pad(prompt_coinrates.astype(float), (0, n - prompt_coinrates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
    true_coinrates_avg = np.nanmean(np.pad(true_coinrates.astype(float), (0, n - true_coinrates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)
    rand_coinrates_avg = np.nanmean(np.pad(rand_coinrates.astype(float), (0, n - rand_coinrates.size%n), mode='constant', constant_values=np.NaN).reshape(-1, n), axis=1)

    # Truncate for the plot
    plot_data = np.vstack((coin_activities_avg, prompt_coinrates_avg, true_coinrates_avg, rand_coinrates_avg)).T
    plot_data = plot_data[plot_data[:,0] < plot_cutoff_activity/1000]
    coin_activities_avg = plot_data[:,0]
    prompt_coinrates_avg = plot_data[:,1]
    true_coinrates_avg = plot_data[:,2]
    rand_coinrates_avg = plot_data[:,3]

    # Truncate for the plot
    plot_data = np.vstack((model_activities, model_prompt, model_true, model_rand)).T
    plot_data = plot_data[plot_data[:,0] < plot_cutoff_activity/1000]
    model_activities = plot_data[:,0]
    model_prompt = plot_data[:,1]
    model_true = plot_data[:,2]
    model_rand = plot_data[:,3]

    # Correct the True coincidence rate for geometry differences
    d = np.vstack((coin_activities_avg, true_coinrates_avg)).T
    # d[:,1] = np.where(((d[:,0] > 2.4e3) & (d[:,0] > 2.4e3)), d[:,1] * 0.83, d[:,1])
    d[:,1] = np.where(d[:,0] > 2.4e3, d[:,1] * 0.83, d[:,1])
    coin_activities_avg = d[:,0]; true_coinrates_avg = d[:,1]

    # def true_model(A, t, i):
    #     n = A*branch_ratio*0.015
    #     m = np_model(n, t)
    #     return m*i*(1/(1+n*t))
    #
    # plt.plot(model_activities, true_model(model_activities*1000, 24e-6, 0.065), '.k')
    #
    # popt, pcov = curve_fit(true_model, coin_activities_avg*1000, true_coinrates_avg, [24e-6, 0.065])
    # perr = np.sqrt(np.diag(pcov))
    #
    # print(popt)
    # plt.plot(model_activities, true_model(model_activities*1000, popt[0], popt[1]), '.')

    if all_coin_models:
        plt.plot(coin_activities_avg, prompt_coinrates_avg, '.', markersize = 10, fillstyle = 'none', color = 'lightblue', label = 'Prompt Rate')
        plt.plot(coin_activities_avg, rand_coinrates_avg, '^', markersize = 10, fillstyle = 'none', color = 'lightgreen', label = 'Random Rate')
        plt.plot(coin_activities_avg, true_coinrates_avg, 'd', markersize = 10, fillstyle = 'none', color = 'lightcoral', label = 'True Rate')

        plt.plot(model_activities, model_prompt, '-', color = 'blue')
        plt.plot(model_activities, model_true, '-', color = 'red')#, label = 'Deadtime Model')
        plt.plot(model_activities, model_rand, '-', color = 'green')

    else:
        plt.plot(coin_activities_avg, true_coinrates_avg, 'd', markersize = 10, fillstyle = 'none', color = 'lightcoral', label = 'True Rate')

        plt.plot(model_activities, model_true, '-', color = 'red', label = 'Deadtime Model')

    plt.xlim(0)
    plt.ylim(0)
    # plt.ylim(0, 760)

    # Remove zero from xticks
    ax = plt.gca()
    xticks = ax.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)

    plt.xlabel('Activity (kBq)')
    plt.ylabel('Coincidence Rate (Hz)')

    plt.legend(loc='upper left')

    # if all_coin_models:
    #     print('Saving figure to', write_dir + 'coincidence_rates_vs_activity-models.png')
    #     plt.savefig(write_dir + 'coincidence_rates_vs_activity-models.png')
    #     # plt.savefig(write_dir + 'coincidence_rates_vs_activity-models.eps')
    # else:
    #     print('Saving figure to', write_dir + 'true_rate_vs_activity-models.png')
    #     plt.savefig(write_dir + 'true_rate_vs_activity-models.png')
    #     # plt.savefig(write_dir + 'true_rate_vs_activity-models.eps')

    plt.show()

    return

# Extract information about which events are being accepted by the PEPT algorithm
def extract_coincidence_information(data_dir, transforms, coin_window, det_delays):
    '''
    Takes in the event data from AllEventsCombined.txt, runs it through the
    coincidence counter and removes the lines according to the F-parameter
    produced by the PEPT algorithm. Then looks at the characteristics of the
    resulting data.

    @params:
        data_dir        - Required : Path to the AllEventsCombined.txt file (Str)

    @returns:
        None
    '''

    # Read in the data and apply the coordinate transformation
    allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations = transforms, e_window = [], singles_only = False, write_transformed_data = False)

    print('\nProcessing multiple scatter events...')

    # Remove additional events from multiple scatter events
    for i in range(2, int(max(allevents.scatters))):
        allevents_doubles = allevents[allevents.scatters == i]      # get the multiple scatter events
        allevents_doubles.reset_index(drop = True, inplace = True)
        allevents_reduced = allevents_doubles[allevents_doubles.index%i == 0]       # remove extra scatter events
        allevents.drop(allevents[allevents.scatters == i].index, inplace = True)    # get allevents without multiple scatter events
        allevents = allevents.append(allevents_reduced, ignore_index = True)        # add the reduced multiple scatters back
        allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                          # reset the indicies

    print('Done.\n')


    print('\nProcessing coincidences')
    print('-----------------------')

    l = len(allevents.index)

    coin_window /= 10      #  convert coincidence window to 10s of nanoseconds

    # Find the coincidences in the energy filtered data
    # -------------------------------------------------
    print('Grouping coincidences according to a timing window of {:.0f} nanoseconds...'.format(coin_window*10) )

    # Add timing correction
    if det_delays != []:
        print('Adding timing correction and sorting...')
        for i, delay in enumerate(det_delays):
            allevents.loc[allevents.detector == i+1, 'time'] += (delay/10)            # add time delay (in 10s of ns)

        allevents.sort_values(by = 'time', inplace = True)                                 # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering

    # print(allevents)

    # Process coincidences for no delay
    # ---------------------------------
    det_time_array_ = allevents[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']].values        # get the detector and time columns as a 2D numpy array

    # Add column of zeros for flagging coincidences
    det_time_array = np.zeros((l,8))
    det_time_array[:,:-1] = det_time_array_

    # print(det_time_array)

    coincidence_data, tot_coins, tot_coins_per_event = utils.find_coincidences3(det_time_array, coin_window, single_counting = True, choose_random = False)

    print(coincidence_data)

    tot_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8

    print('{:45} {:d}'.format('Total number of coincidences:', tot_coins))
    print('{:45} {:.5f}'.format('Average number of coincidences per event:', tot_coins_per_event))
    print('{:45} {:.1f}'.format('Total time (seconds):', tot_time))
    print('{:45} {:.0f}'.format('Total coincidence rate (coins/sec):', tot_coins/tot_time))

    # Remove lines using the PEPT algorithm and a given f-parameter
    # -------------------------------------------------------------
    print('\nCalculating minimum distances and removing outer most lines according to fopt...')
    distances = []
    pos = np.array([-0.1, 0.6, 12.7])
    f = 16

    # Calculate all the distances to the position
    for index, coin in coincidence_data.iterrows():
        # First get the straight line equation

        coin_v1 = np.array([coin.x_1, coin.y_1, coin.z_1])
        coin_v2 = np.array([coin.x_2, coin.y_2, coin.z_2])

        d = (coin_v2 - coin_v1)/np.linalg.norm(coin_v2 - coin_v1)
        v = pos - coin_v1
        t = np.dot(d,v)
        D = coin_v1 + t*d

        distances.append(np.linalg.norm(pos - D))

    # plt.hist(distances, 50)
    # plt.show()

    coincidence_data['d'] = distances       # add the distances to the coincidence data

    print('Sorting by distance...')

    coincidence_data.sort_values('d', inplace = True)           # sort by the distance from the location
    coincidence_data.reset_index(drop = True, inplace = True)

    # Get the f-percentage of data with the shortest distances
    N_0 = len(coincidence_data.index)
    N_f = int((f/100)*N_0)
    f_coins = coincidence_data.head(N_f)

    print('Done.\n')

    # print(coincidence_data)

    # -------------------------------------------------------------

    # Recombining data into allevents dataframe

    allevents1 = coincidence_data.filter(['scatters_1', 'detector_1', 'energy_1', 'x_1', 'y_1', 'z_1', 'time_1'], axis = 1)
    allevents1.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']
    # print(allevents1)

    allevents2 = coincidence_data.filter(['scatters_2', 'detector_2', 'energy_2', 'x_2', 'y_2', 'z_2', 'time_2'], axis = 1)
    allevents2.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']
    # print(allevents2)

    allevents = pd.concat([allevents1, allevents2], ignore_index = True)
    allevents.sort_values(by = 'time', inplace = True)                                 # sort the columns by time
    allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering

    # print(allevents)


    # Plotting the energy spectrum of the accepted results
    # ----------------------------------------------------
    # plot_energy_spectrum(data_dir, events = allevents, apply_energy_filter = False, e_window = [], e_range = (0, 1000), fit_peaks = False, e_fits = [], shade_sca_window = False, sca_windows = [])


    # Performing a timing resultion plot on the accepted results
    # ----------------------------------------------------------
    #
    # if l > 1E6:      # shorten the dataset for faster processing
    #     allevents = allevents.iloc[:500000]
    #     l = len(allevents.index)    # get the length of the filtered array
    #
    # total_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time'])*1e-8     # get the total time in units of 1e-8 seconds
    #
    #
    # window = window/10      # timing window to look for coincidences in 10s of nanoseconds

    # Run timing resolution for each detector in the array
    print('Running timing resolution between detector 0 and detector 1')

    delays = np.linspace(-4000, 4000, 50)
    lng = len(delays)
    total_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8

    rates = []      # array of rates

    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, delay_ns in enumerate(delays):

        delay = delay_ns/10     # convert nanoseconds into 10s of nanoseconds
        # print('Copying all events dataframe and addition time delay for randoms counting...')
        allevents_delay = allevents.copy()                                   # make copy for adding time delay
        allevents_delay.loc[allevents_delay.detector == 1, 'time'] += delay  # add time delay to detector
        allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
        # print('Done.', '\n')

        det_time_array_delay_ = allevents_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array_delay = np.zeros((len(det_time_array_delay_),3))
        det_time_array_delay[:,:-1] = det_time_array_delay_

        tot_coins = utils.count_coincidences3(det_time_array_delay, 60)

        rates.append(tot_coins/total_time)

        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    # Plotting the normal distribution
    # --------------------------------

    print('\n')

    # if perform_fit:
    #
    #     print('Performing a normal distribution fit...')
    #
    #     def gaus(x, A, mu, sigma, B):
    #         return A*np.exp(-(x-mu)**2/(2*sigma**2)) + np.abs(B)
    #
    #     popt, pcov = curve_fit(gaus, delays, rates, [max(rates), np.mean(delays)-1, np.std(delays), min(rates)])
    #     perr = np.sqrt(np.diag(pcov))
    #
    #     fit_results =  'Fitted Parameters for Normal Distribution\n'
    #     fit_results += '-----------------------------------------\n'
    #     fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('A', popt[0], '+-', perr[0])
    #     fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('mu', popt[1], '+-', perr[1])
    #     fit_results += '{:10}{:.2f}{:^4}{:.2f} ns\n'.format('sigma', popt[2], '+-', perr[2])
    #     fit_results += '{:10}{:.2f}{:^4}{:.2f} Hz\n'.format('B', popt[3], '+-', perr[3])
    #
    #     with open(data_dir + 'timing_resolution_fit_results-detector_{:d}.log'.format(delay_det), 'w') as f:
    #         f.write(fit_results)
    #
    #     # print('Fitted Parameters for Normal Distribution')
    #     # print('-----------------------------------------')
    #     # print('{:10}{:.2f}{:^4}{:.2f}'.format('A', popt[0], '+-', perr[0]))
    #     # print('{:10}{:.2f}{:^4}{:.2f}'.format('mu', popt[1], '+-', perr[1]))
    #     # print('{:10}{:.2f}{:^4}{:.2f}'.format('sigma', popt[2], '+-', perr[2]))
    #     # print('{:10}{:.2f}{:^4}{:.2f}'.format('B', popt[3], '+-', perr[3]))
    #
    #     print(fit_results)
    #
    #     x_fit = np.linspace(min(delays), max(delays), 200)
    #     y_fit = gaus(x_fit, *popt)
    #     plt.plot(x_fit, y_fit, label = r'A = {:.2f} $\pm$ {:.2f} Hz'.format(popt[0], perr[0]) + '\n' +
    #                                    r'$\mu$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[1], perr[1]) + '\n' +
    #                                    r'$\sigma$ = {:.1f} $\pm$ {:.1f} ns'.format(popt[2], perr[2]) + '\n' +
    #                                    r'B = {:.2f} $\pm$ {:.2f} Hz'.format(popt[3], perr[3]))
    #     plt.legend()

    # Plot the final results
    plt.xlabel('Time Delay (ns)')
    plt.ylabel('Coincidence Rate (Hz)')

    plt.plot(delays, rates, '.')#, markersize = 20)

    # if shade_window != []:
    #     start_e = shade_window[0]; end_e = shade_window[1]
    #     max_e = max(rates)
    #
    #     colour = shade_window[2]
    #
    #     # Plot vertical lines indicating the sca windows
    #     plt.plot([start_e, start_e], [0, max_e], '--', color = colour)
    #     plt.plot([end_e, end_e], [0, max_e], '--', color = colour)
    #
    #     # Shade the area between the lines
    #     plt.fill_between([start_e, end_e], [0, 0], [max_e, max_e],
    #                       facecolor = colour,                 # fill colour
    #                       color = colour,                     # outline colour
    #                       alpha = 0.2)                              # transparency

    # plot_filename = '{}timing_resolution-detector_{:d}.png'.format(data_dir, delay_det)
    # print('\nSaving plot to', plot_filename, '\n\n')
    # plt.savefig(plot_filename)
    # plt.clf(); plt.cla()
    plt.show()

    return

# Plot a 3D demonstration of the LORs from a processed coincidence file
def plot_3d_lors(data_file, write_dir):
    '''
    Reads in the raw coincidence data, creates the LORs and plots them in 3D
    with the Polaris crystals overlaid.

    @params:
        data_file       - Required : Path to file containing coincidence data
        write_dir       - Required : Path to save the final figure to

    @returns:
        None
    '''

    # matplotlib.rc('font',family='Palatino')
    # figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 15})

    print('Reading in coincidence data from', data_file)
    allcoins = pd.read_csv(data_file, sep = ',', header = None)                 # read in text file as a csv with tab separation
    allcoins.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']        # separate into columns and name them

    l = len(allcoins.index)
    print('Total number of coincidences:', l)

    plotcoins = allcoins.head(100)      # get the first 100 lors

    # Setup 3D plotting
    fig = plt.figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')
    ax = plt.axes(projection='3d')
    ax.view_init(elev = 15, azim=-65)

    print('Plotting lines of response...')
    for coin in plotcoins.iterrows():
        coin = coin[1]
        xs = [coin.x1, coin.x2]
        ys = [coin.y1, coin.y2]
        zs = [coin.z1, coin.z2]

        ax.plot3D(xs, ys, zs, 'g-', linewidth = 0.5)
        ax.scatter3D(xs, ys, zs, color = 'g', s = 0.4)

    print('Plotting crystal locations...')

    def rect_prism(x_range, y_range, z_range):

        yy, zz = np.meshgrid(y_range, z_range)
        ax.plot_wireframe(x_range[0], yy, zz, color="r")
        # ax.plot_surface(x_range[0], yy, zz, color="r", alpha=0.2)
        ax.plot_wireframe(x_range[1], yy, zz, color="r")
        # ax.plot_surface(x_range[1], yy, zz, color="r", alpha=0.2)

        xx, zz = np.meshgrid(x_range, z_range)
        ax.plot_wireframe(xx, y_range[0], zz, color="r")
        # ax.plot_surface(xx, y_range[0], zz, color="r", alpha=0.2)
        ax.plot_wireframe(xx, y_range[1], zz, color="r")
        # ax.plot_surface(xx, y_range[1], zz, color="r", alpha=0.2)

    rect_prism( np.array([31, 41]), np.array([-21, -1]), np.array([1, 21]) )
    rect_prism( np.array([31, 41]), np.array([1, 21]), np.array([1, 21]) )
    rect_prism( np.array([-31, -41]), np.array([-21, -1]), np.array([1, 21]) )
    rect_prism( np.array([-31, -41]), np.array([1, 21]), np.array([1, 21]) )

    # Final plot properties
    ax.set_xlabel('x (mm)', labelpad = 15)
    ax.set_ylabel('y (mm)', labelpad = 15)
    ax.set_zlabel('z (mm)', labelpad = 10)

    ax.set_zticks([0, 4, 8, 12, 16, 20])

    print('Saving figure to', write_dir + '3d_lors.png')
    plt.savefig(write_dir + '3d_lors.png', bbox_inches = 'tight')
    plt.savefig(write_dir + '3d_lors.eps', format = 'eps', bbox_inches = 'tight')
    # plt.show()

    return

# Produces the prompt coincidence rate sensitivity plot as a function of position
def coincidence_sensitivity(data_dirs, write_dir, f, N, read_txt = False):
    '''
    Takes in a list of directories to pept_coincidence_data coincidence files,
    reads them in and plots the Prompt rate as a function of position. Gets the
    position by running PEPT on each coincidence dataset. Assumes a stationary
    source.

    @params:
        data_dirs       - Required : List of paths to pept_coincidence_data data (String[])
        write_dir       - Required : Path to save the final figure to (Str)
        f               - Required : F-parameter used when picking out LORs (Int)
        N               - Required : Number of lines per position (Int)
        read_txt        - Optional : Either process the results from the raw data or read the data from a text file (Bool, default = False)

    @returns:
        None
    '''
    figure(num=None, figsize=(16, 9), dpi=300, facecolor='w', edgecolor='k')      # Set plot size
    plt.rcParams.update({'font.size': 20})

    prompt_rates = []
    y_positions = []

    if read_txt:
        plot_data_file = '{}plot_data/sensitivity/prompt_vs_ycoord.txt'.format(write_dir)
        print('Reading plot data from', plot_data_file)

        plot_data = np.loadtxt(plot_data_file, dtype = np.float64, delimiter = ',')     # read in the text file
        y_positions = plot_data[:,0]
        prompt_rates = plot_data[:,1]

    else:
        for data_dir in data_dirs:
            # First read in the coincidence file
            coin_file = '{}pept_coincidence_data'.format(data_dir)
            print('Reading in file', coin_file)
            allcoins = pd.read_csv(coin_file, sep = ',', header = None)             # read in text file as a csv with tab separation
            allcoins.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']     # separate into columns and name them

            print('Counting prompt rate...')
            num_coins = len(allcoins.index)
            tot_time = (allcoins.iloc[-1]['t1'] - allcoins.iloc[0]['t1']) * 1e-6   # get the total time in seconds

            prompt_rates.append(num_coins/tot_time)

            print('Getting the PEPT position...')
            # Now get the PEPT position
            pos, ucert = run_ctrack(coin_file, f, N)
            y_positions.append(pos[1])

            print('Done.\n')

            # Write the results to text file for easier plotting in the future
            plot_data_dir = '{}plot_data/sensitivity/'.format(write_dir)
            if not os.path.exists(plot_data_dir):
                os.makedirs(plot_data_dir)
                print ('Created directory: {}'.format(plot_data_dir))

            write_filename = '{}prompt_vs_ycoord.txt'.format(plot_data_dir, N)
            np.savetxt(write_filename, np.c_[y_positions, prompt_rates], fmt = '%.4f', delimiter = ',')

    # Normalise the prompt rate to maximum value
    prompt_rates = np.array(prompt_rates)
    prompt_rates /= max(prompt_rates)

    # Plotting the results
    # --------------------
    print('Plotting the results.')
    plt.plot(y_positions, prompt_rates, '--ok')
    plt.xlabel('Position (mm)')
    # plt.ylabel('Prompt Rate (Hz)')
    plt.ylabel('Normalised Prompt Rate')

    print('Saving figure to', write_dir + 'sensitivity_yaxis.png')
    plt.savefig(write_dir + 'sensitivity_yaxis.png')
    # plt.savefig(write_dir + 'sensitivity_yaxis.eps', format = 'eps')
    # plt.show()

    return

# Counts the number of false and true double scatter events from a 511keV dataset
def false_doubles(data_dirs, write_dir, initial_activities, read_txt = False):
    '''
    Reads in an AllEventsCombined.txt dataset and removes all events that aren't
    double scatter events. Then counts the number of doubles where both events
    are 511 keV (false doubles) and the number where both events' energy adds up
    to 511 keV (true doubles). Does this for fixed time intervals along the
    dataset. Finally plots true versus false doubles as a function of time.

    @params:
        data_dirs               - Required : List of paths to AllEventsCombined.txt data (String[])
        write_dir               - Required : Path to save the results to (Str)
        initial_activities      - Required : List of the initial activities of sources in kBq (Float[])
        read_txt                - Optional : Either process the results from the raw data or read the data from a text file (Bool, default = False)

    @returns:
        None
    '''
    # Activity function for calculating the activity
    def A(t, A_0, t_h):
        return A_0 * 2**(-t/t_h)

    if read_txt:
        plot_data_file = '{}false_doubles-2s.txt'.format(write_dir)
        print('Reading plot data from', plot_data_file)

        plot_data = np.loadtxt(plot_data_file, dtype = np.float64, delimiter = ',')     # read in the text file
        all_activities = plot_data[:,0]
        all_doubles = plot_data[:,1]
        all_trues = plot_data[:,2]
        all_falses = plot_data[:,3]
        all_missings = plot_data[:,4]

    else:
        all_doubles, all_trues, all_falses, all_missings, all_activities = [], [], [], [], []
        for k, data_dir in enumerate(data_dirs):

            allevents, read_log = read_polaris_data(data_dir + 'AllEventsCombined.txt') # read in the raw polaris data
            print('Getting all double scatter events...')
            allevents.drop(allevents[allevents.scatters != 2].index, inplace = True)    # drop events that aren't doubles
            allevents.reset_index(drop = True, inplace = True)                          # reset the indicies

            print('Splitting into intervals and counting true/false doubles...\n')
            # Split allevents by fixed time interval
            t_int = 2       # time interval in seconds
            S = allevents.time
            splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

            alls, trues, falses, missings, activities = [], [], [], [], []

            l = len(splitevents)
            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            t_elapsed = 0
            A_0 = initial_activities[k]     # get the initial activity at the start of the measurement
            for i, df in enumerate(splitevents):

                df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time
                t_elapsed += df_time                                            # keep track to total elapsed time
                activity = A(t_elapsed, A_0, 4080)                                # calculate the activity at the end of the period

                # First get useful data as a numpy array
                events = df[['energy', 'time']].values

                all, true, false, miss = 0, 0, 0, 0
                for j in range(1, len(events)-1):

                    # First check that both scatter events were recorded
                    if events[j][1] != events[j+1][1] and events[j-1][1] != events[j][1]:

                        if 501 <= events[j][0] <= 521:
                            false += 1
                        miss += 1
                        all += 1

                        continue

                    if events[j][1] == events[j+1][1]:
                        # If either event is a 511, then it's a false double
                        if (501 <= events[j][0] <= 521) or (501 <= events[j+1][0] <= 521):
                            false += 1
                            # j += 1
                            continue

                        # If the total energy is a 511, then it's a true double
                        tot_energy = events[j][0] + events[j+1][0]
                        if 501 <= tot_energy <= 521:
                            true += 1
                            # j += 1
                            continue

                        all += 1

                activities.append(activity)
                alls.append(all/df_time)
                trues.append(true/df_time)
                falses.append(false/df_time)
                missings.append(miss/df_time)

                printProgressBar(i+1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # update progress bar

            all_doubles.append(alls)
            all_trues.append(trues)
            all_falses.append(falses)
            all_missings.append(missings)
            all_activities.append(activities)

            print('\n=====================================================================\n')

        # Flatten the lists
        all_doubles = [r for sublist in all_doubles for r in sublist]
        all_trues = [r for sublist in all_trues for r in sublist]
        all_falses = [r for sublist in all_falses for r in sublist]
        all_missings = [r for sublist in all_missings for r in sublist]
        all_activities = [r for sublist in all_activities for r in sublist]

        # Write the results to a text file
        # ---------------------------------
        if not os.path.exists(write_dir):
            print ('Creating directory: {}'.format(write_dir))
            os.makedirs(write_dir)

        np.savetxt('{}false_doubles-2s.txt'.format(write_dir), np.c_[all_activities, all_doubles, all_trues, all_falses, all_missings], fmt = '%.4f', delimiter = ',')

    print()
    print('Plotting the results...')
    # Plotting the results
    plt.plot(all_activities, all_doubles, '.', label = 'All')
    plt.plot(all_activities, all_trues, '.', label = 'True')
    plt.plot(all_activities, all_falses, '.', label = 'False')
    plt.plot(all_activities, all_missings, '.', label = 'Missing')

    plt.xlabel('Activity (kBq)')
    plt.ylabel('')

    plt.legend()
    plt.show()

    return


# --------- #
# DEBUGGING #
# --------- #

# Coincidence rate as a function of time debugging
def coincidence_rate_vs_time():

    coin_file = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/pept_coincidence_data'
    coin_file = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run6-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/pept_coincidence_data'
    # Coincidence rate as a function of time
    allcoins = pd.read_csv(coin_file, sep = ',', header = None)                 # read in text file as a csv with tab separation
    allcoins.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']         # separate into columns and name them

    times = allcoins['t1'].to_numpy()

    rates = []
    t_start = times[0]
    t_end = times[1]

    i = 2
    while t_end < times[-1]:
        count = 0
        while t_end - t_start < 1e7:        # every 60 seconds in us
            count += 1
            if i == len(times):
                break
            t_end = times[i]
            i += 1
        rates.append(count/(t_end - t_start) * 1e6)
        t_start = t_end
        if i == len(times):
            break
        t_end = times[i]

    # plt.plot(np.arange(len(rates)), rates, '.')
    # plt.show()
    # plt.clf()

    plt.hist(rates, 10)
    plt.show()

    return

# Sync pulse debugging
def sync_pulse_debugging():

    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run5-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'

    # mod = 1
    #
    # if not mod:
    #     mod0data = pd.read_csv(data_dir + 'mod51.txt', sep = '	', header = None)                        # read in text file as a csv with tab separation
    #     mod0data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']       # separate into columns and name them
    #
    #     sync_pulses = mod0data.drop(mod0data[mod0data.scatters != 122].index)
    #
    # else:
    #     mod1data = pd.read_csv(data_dir + 'mod52.txt', sep = '	', header = None)                        # read in text file as a csv with tab separation
    #     mod1data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']       # separate into columns and name them
    #
    #     sync_pulses = mod1data.drop(mod1data[mod1data.scatters != 122].index)

    # sync_times = sync_pulses.y.to_numpy()
    #
    # sync_diffs = np.diff(sync_times)
    #
    # plt.plot(np.arange(len(sync_times)), sync_times, '.')
    # # plt.plot(np.arange(len(sync_diffs)), sync_diffs, '.')
    # plt.show()sync_times = sync_pulses.y.to_numpy()
    #
    # sync_diffs = np.diff(sync_times)
    #
    # plt.plot(np.arange(len(sync_times)), sync_times, '.')
    # # plt.plot(np.arange(len(sync_diffs)), sync_diffs, '.')
    # plt.show()

    coindata = pd.read_csv(data_dir + 'pept_coincidence_data', sep = ',', header = None)                        # read in text file as a csv with tab separation
    coindata.columns = ['time1', 'x1', 'y1', 'z1', 'time2', 'x1', 'y1', 'z1']       # separate into columns and name them

    time1s = coindata.time1.to_numpy()
    time2s = coindata.time2.to_numpy()
    time_diffs = time2s - time1s
    time_diffs *= 1e3
    time_between_coins = np.diff(time2s)

    # plt.plot(np.arange(len(time1s)), time1s, '.')
    # plt.plot(np.arange(len(time2s)), time2s, '.')
    # plt.plot(np.arange(len(time_diffs)), time_diffs, '.')
    plt.hist(time_diffs, 'auto', density = False)
    # plt.hist(time_between_coins, 'auto', density = False)
    plt.show()

    return

# Coincidence processor to double check the current coincidence processing function
def process_coincidences_debug():
    pd.set_option('display.max_rows', 40)
    pd.set_option('display.min_rows', 40)

    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run5-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/'
    data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'

    e_window = [461, 561, 310, 370]
    coinwindow = 60     # coincidence window in 10s of nanoseconds

    # Reading in raw data and processing
    # ----------------------------------
    print('Reading in file...')
    allevents = pd.read_csv(data_dir + 'AllEventsCombined.txt', sep = '	', header = None)       # read in text file as a csv with tab separation
    allevents.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']               # separate into columns and name them
    allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)                    # drop the multiple interaction scatter events
    allevents.reset_index(drop = True, inplace = True)                                          # reset the index numbering
    tot_raw_events = len(allevents.index)
    print('Done.\n')
    # ----------------------------------

    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    allevents.reset_index(drop = True, inplace = True)                          # reset the index numbering
    tot_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8
    tot_filtered_events = len(allevents.index)

    # allevents.loc[allevents.detector == 1, 'time'] -= 200000006                 # correct the sync pulse discrepency
    # allevents.loc[allevents.detector == 1, 'time'] -= 199998397                 # correct the sync pulse discrepency
    # allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
    # allevents.reset_index(drop = True, inplace = True)                          # reset the index numbering

    print('Total number of events after energy filter: {:d} ({:.1f}%)'.format(tot_filtered_events, tot_filtered_events/tot_raw_events*100))
    print('Total time: {:.2f} seconds.'.format(tot_time))
    print('Total filtered singles event rate: {:.2f} Hz.\n'.format(tot_filtered_events/tot_time))

    allevents.drop(allevents[allevents.detector != 1].index, inplace = True)    # drop module 0 data
    # print('allevents', allevents)
    allevents_copy = allevents.copy()                                           # copy module 0 data
    # print('allevents_copy', allevents_copy)
    allevents_copy.detector += 1                                                # pretend module 0 data is module 1 data
    # print('allevents_copy', allevents_copy)
    allevents = allevents.append(allevents_copy, ignore_index = True)                                            # add it back into the allevents dataframe
    # print('allevents', allevents)
    allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
    # print('allevents', allevents)
    allevents.reset_index(drop = True, inplace = True)                          # reset the index numbering
    # print('allevents', allevents)

    print(len(allevents.index))

    # allevents_np = allevents.head(n = 10000).to_numpy()     # convert to numpy array for easier counting
    allevents_np = allevents.to_numpy()     # convert to numpy array for easier counting

    tot_coincidences = 0
    tot_mult_coins = 0          # total number of multiple coincidences (more than one coincidence found)
    for i, curr_event in enumerate(allevents_np):
        # if i%1000 == 0:
            # print(i)
        # print('Current event:', curr_event)

        current_event_time = curr_event[-1]
        # print('Current event time:', current_event_time)

        j = i+1   # get next event index
        if j == len(allevents_np):       # make sure not to iterate past the end
            continue

        mult_coins = -1
        # print('Next event time:', allevents_np[j][-1])
        # print('Upper window: ', current_event_time + coinwindow)
        while allevents_np[j][-1] <= current_event_time + coinwindow:
            # print(i)
            # print('Upper window = ', current_event_time + 30)
            # print('Current event = ', curr_event)
            # print('Next event = ', allevents_np[j])
            # print()
            # print('Next event detector module:', allevents_np[j][1])
            if allevents_np[j][1] != curr_event[1]:      # if not in the same detector
                tot_coincidences += 1
                mult_coins += 1
            j += 1
            if j == len(allevents_np):
                break

        if mult_coins > 0:
            tot_mult_coins += 1

        # print('\n\n')

        # j = i-1   # get previous event index
        # if j == -1:       # make sure not to iterate past the beginning
        #     continue
        #
        # while allevents_np[j][-1] >= current_event_time - 30:
        #     # print(i)
        #     # print('Lower window = ', current_event_time - 30)
        #     # print('Current event = ', curr_event)
        #     # print('Next event = ', allevents_np[j])
        #     # print('==========================\n')
        #     if allevents_np[j][1] != curr_event[1]:      # if not in the same detector
        #         tot_coincidences += 1
        #     j -= 1
        #     if j == -1:
        #         break


    print('Total Number of Coincidences: {:d}'.format(tot_coincidences))
    print('Final coincidence rate: {:.2f}Hz'.format(tot_coincidences/tot_time))
    print('Total number of multiple coincidence events: {:d}'.format(tot_mult_coins))


    # allevents.drop(allevents[allevents.detector != 0].index, inplace = True)                    # get events from detector 1

    # sca_windows = [(461, 561, 'lightblue'), (310, 370, 'lightblue')]
    # plot_energy_spectrum(data_dir, allevents, e_range = (200, 600), shade_sca_window = True, sca_windows = sca_windows)

    # mod0_times = allevents[allevents.detector == 0].to_numpy()[:,-1]      # get numpy array for module 0 event times
    # mod1_times = allevents[allevents.detector == 1].to_numpy()[:,-1]      # get numpy array for module 0 event times
    #
    # mod1_times -= 199998389
    #
    # print(len(mod0_times))
    # print(len(mod1_times))
    # mod1_times = mod1_times[:len(mod0_times)]
    # differences = mod0_times - mod1_times
    #
    # plt.hist(differences, 50)
    # plt.show()
    #
    # mod0_plottimes = mod0_times[:10000]
    # mod0_yplot = np.ones(len(mod0_plottimes))
    #
    # print(np.shape(mod0_plottimes))
    # print(np.shape(mod0_yplot))
    #
    # plt.errorbar(mod0_plottimes, mod0_yplot, xerr = 60, fmt = '.', color = 'red')
    # plt.plot(mod1_times[:10000], np.ones(10000), 'b.')
    # plt.show()

    return

# Counting the raw number of 511keV gammas to see if they're being lost of corrupted
def counting_511s():
    data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    allevents_file = data_dir + 'AllEventsCombined.txt'

    d0_transformation = [180.0, 90.0, 0.0, -41.23, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 41.23, 0.0, 0.0]
    transformations = [d0_transformation, d1_transformation]

    e_window = [461, 561, 310, 370]
    coinwindow = 60                     # coincidence window in 10s of nanoseconds
    mod_delay = -2000000060

    # Reading in raw data and processing
    # ----------------------------------
    allevents, log_str = read_polaris_data(allevents_file, transformations = [])
    allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)                    # drop the multiple interaction scatter events
    allevents.reset_index(drop = True, inplace = True)                                          # reset the index numbering
    tot_raw_events = len(allevents.index)

    print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...'.format(e_window[0], e_window[1], e_window[2], e_window[3]))
    log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...\n'.format(e_window[0], e_window[1], e_window[2], e_window[3])

    allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.

    # print('Applying energy window filtering. Using peak window of {:d}keV to {:d}keV'.format(e_window[0], e_window[1]))
    # log_str += 'Applying energy window filtering. Using peak window of {:d}keV to {:d}keV\n'.format(e_window[0], e_window[1])
    #
    # allevents.drop(allevents[ (allevents['energy'] < e_window[0]) |      # ...less than the lower photopeak limit...
    #                                     (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper photopeak limit.

    allevents.reset_index(drop = True, inplace = True)                                          # reset the index numbering
    print('Done.\n')

    print('Adding timing correction and sorting...')
    allevents.loc[allevents.detector == 1, 'time'] += (mod_delay/10)             # add time delay (in 10s of ns) to detector 1
    allevents.sort_values(by = 'time', inplace = True)                           # sort the columns by time
    allevents.reset_index(drop = True, inplace = True)                           # reset the index numbering
    print('Done.\n')

    t_int = 60
    S = allevents.time
    splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

    df = splitevents[0]

    num_events_mod0 = len(df[df.detector == 0].index)
    num_events_mod1 = len(df[df.detector == 1].index)
    tot_time_mod0 = ((df[df.detector == 0].iloc[-1]['time'] - df[df.detector == 0].iloc[0]['time']) * 1e-8)
    tot_time_mod1 = ((df[df.detector == 1].iloc[-1]['time'] - df[df.detector == 1].iloc[0]['time']) * 1e-8)

    print('Total number of events in module 0: {:.0f}'.format(num_events_mod0))
    print('Total number of events in module 1: {:.0f}'.format(num_events_mod1))
    print('Event rate in module 0: {:.2f}'.format(num_events_mod0/tot_time_mod0))
    print('Event rate in module 1: {:.2f}'.format(num_events_mod1/tot_time_mod1))

    det_time_array_ = df[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

    # Add column of zeros for flagging coincidences
    det_time_array = np.zeros((len(det_time_array_),3))
    det_time_array[:,:-1] = det_time_array_

    tot_coins = utils.count_coincidences3(det_time_array, coinwindow)
    tot_time = ((df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8)

    print('Total number of coincidences: {:.0f}'.format(tot_coins))
    print('Coincidence Rate: {:.2f}'.format(tot_coins/tot_time))


    # Coincidence rate as a function of window
    # ========================================

    # Create empty arrays to hold the coincidence rates
    tot_rates = []; rand_rates = []; true_rates = []

    print('\nCounting coincidence rates...')

    coin_windows = np.linspace(0, 10000000, 50)
    lng = len(coin_windows)

    plot_randoms = True
    if plot_randoms:
        print('Copying all events dataframe and adding time delay for randoms counting...')
        df_delay = df.copy()                                   # make copy for adding time delay
        df_delay.loc[df_delay.detector == 1, 'time'] += 5e7    # add time delay to detector 1 of half a second
        df_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
        det_time_array_delay_ = df_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array
        print('Done.', '\n')

    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i, window in enumerate(coin_windows):

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        window /= 10        # convert time to 10s of nanoseconds

        # Count the coincidences
        tot_rates.append(utils.count_coincidences3(det_time_array, window) / tot_time)

        if plot_randoms:
            # Add column of zeros for flagging coincidences
            det_time_array_delay = np.zeros((len(det_time_array_delay_),3))
            det_time_array_delay[:,:-1] = det_time_array_delay_
            rand_rates.append(utils.count_coincidences3(det_time_array_delay, window) / tot_time)
            true_rates.append(tot_rates[i] - rand_rates[i])

        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    # Plotting the results
    # ====================

    plt.plot(coin_windows, tot_rates, '.', label = 'Total coincidence rate')
    if plot_randoms:
        plt.plot(coin_windows, rand_rates, '.', label = 'Random coincidence rate')
        plt.plot(coin_windows, true_rates, '.', label = 'True coincidence rate')
    plt.legend()
    plt.xlabel('Coincidence Window (ns)')
    plt.ylabel('Coincidence Rate (Hz)')


    plt.show()

    return

# Getting data rates from the singles data for each module
def data_file_info():
    data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # file = 'mod52.txt'
    file = 'AllEventsCombined.txt'

    # print('Reading in {}...\n'.format(data_dir + file))
    # data = pd.read_csv(data_dir + file, sep = '	', header = None)               # read in text file as a csv with tab separation
    # data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']                # separate into columns and name them
    # data.drop(data[data.scatters != 1].index, inplace = True)
    # data.reset_index(drop = True, inplace = True)
    #
    # num_events = len(data.index)
    # tot_time = ((data.iloc[-1]['time'] - data.iloc[0]['time']) * 1e-8)
    #
    # print('Total unfiltered events: {:.0f}'.format(num_events))
    # print('Total time: {}'.format(tot_time))
    # print('Total unfiltered rate: {}'.format(num_events/tot_time))
    #
    # e_window = [461000, 561000, 310000, 370000]
    #
    # print('\nApplying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...\n'.format(e_window[0], e_window[1], e_window[2], e_window[3]))
    #
    # data.drop(data[ (data['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
    #                                     ((data['energy'] > e_window[3]) & (data['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
    #                                     (data['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    # data.reset_index(drop = True, inplace = True)
    #
    # num_events = len(data.index)
    # tot_time = ((data.iloc[-1]['time'] - data.iloc[0]['time']) * 1e-8)
    #
    # print('Total filtered events: {:.0f}'.format(num_events))
    # print('Total time: {}'.format(tot_time))
    # print('Total filtered rate: {}'.format(num_events/tot_time))
    #
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # file = 'mod52.txt'


    print('Reading in {}...\n'.format(data_dir + file))
    data = pd.read_csv(data_dir + file, sep = '	', header = None)               # read in text file as a csv with tab separation
    data.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']    # separate into columns and name them
    data.drop(data[data.scatters != 1].index, inplace = True)
    data.drop(data[data.detector != 1].index, inplace = True)
    data.reset_index(drop = True, inplace = True)

    num_events = len(data.index)
    tot_time = ((data.iloc[-1]['time'] - data.iloc[0]['time']) * 1e-8)

    print('Total unfiltered events: {:.0f}'.format(num_events))
    print('Total time: {}'.format(tot_time))
    print('Total unfiltered rate: {}'.format(num_events/tot_time))

    e_window = [461, 561, 310, 370]

    print('\nApplying energy window filtering. Using peak window of {:d}keV to {:d}keV and Compton edge window of {:d}keV to {:d}keV...\n'.format(e_window[0], e_window[1], e_window[2], e_window[3]))

    data.drop(data[ (data['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
                                        ((data['energy'] > e_window[3]) & (data['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
                                        (data['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.
    data.reset_index(drop = True, inplace = True)

    num_events = len(data.index)
    tot_time = ((data.iloc[-1]['time'] - data.iloc[0]['time']) * 1e-8)

    print('Total filtered events: {:.0f}'.format(num_events))
    print('Total time: {}'.format(tot_time))
    print('Total filtered rate: {}'.format(num_events/tot_time))

    return

# Run coincidence processing from the mod.txt files, performing sync pulse correction in-situ
# TODO: Clean up this function
def combine_module_data():
    '''

    '''
    pd.set_option('display.max_rows', 40)
    pd.set_option('display.min_rows', 40)
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'
    data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200724/200724-run4-na22_at_0_2.5_11mm-oem2_x_-40.5mm-oem3_x_40.5mm-nosubmm-source_insert-30sec/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200724/200724-run8-na22_at_0_2.5_11mm-oem2_x_-40.5mm-oem3_x_40.5mm-nosubmm-source_insert_remove-30sec/'
    allevents = pd.DataFrame()

    sync_pulse_interval = 200000006
    # sync_pulse_interval = 199998397

    mod_data_list = []
    mod_sync_list = []

    # Processing individual mod files and store the results
    for i in range(1, 3):

        file = 'mod5{:d}.txt'.format(i)

        print('Reading in {}...'.format(data_dir + file))
        data = pd.read_csv(data_dir + file, sep = '	', header = None)           # read in text file as a csv with tab separation
        data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']            # separate into columns and name them
        data['time'][data.scatters == 122] = data['y'][data.scatters == 122]    # move the time value to the time column
        data.time = data.time.astype(int)                                       # convert time to an integer
        data.sort_values(by = 'time', inplace = True)                           # sort the columns by time
        data.reset_index(drop = True, inplace = True)                           # reset the index numbering
        # data.drop(data[data.scatters != 1].index, inplace = True)             # get single-interaction information
        # data.reset_index(drop = True, inplace = True)                         # reset the indicies
        print('Done.\n')

        if '191203' in data_dir:
            if i == 1:
                data['x'][data.scatters == 122] += 1

        # file = 'mod5{:d}.dat'.format(i)
        # print('Reading in {}...'.format(data_dir + file))
        # data = read_mod_binary(data_dir, file)
        # data.sort_values(by = 'time', inplace = True)                           # sort the columns by time
        # data.reset_index(drop = True, inplace = True)                           # reset the index numbering
        # print('Done.\n')

        sync_pulses = data[data.scatters == 122]        # get the sync pulses
        sync_pulses.reset_index(drop = True, inplace = True)

        # print(sync_pulses)

        mod_data_list.append(data)
        mod_sync_list.append(sync_pulses)

    # Get the first common sync pulse index and truncate accordingly
    first_sync_indicies = [mod_sync_list[i].x[0] for i in range(len(mod_sync_list))]        # get the first sync pulse index in each module
    first_common_index = max(first_sync_indicies)           # get the first common index to all the module sync pulses
    for i, mod_sync in enumerate(mod_sync_list):
        mod_sync_list[i] = mod_sync[mod_sync.x >= first_common_index]
        mod_sync_list[i].reset_index(drop = True, inplace = True)

    # Correct each module data array separately
    # -----------------------------------------
    # corrected_mod_data_list = []
    # # Correct all the times according to the sync pulses
    # for i in range(len(mod_data_list)):
    #     data = mod_data_list[i]
    #     sync_pulses = mod_sync_list[i]
    #
    #     # plt.plot(np.diff(sync_pulses.time.to_numpy()))
    #     # plt.show()
    #
    #     # t0 = sync_pulses.y[0]      # get the time of the first sync_pulse
    #
    #     # print(t0)
    #     # print()
    #
    #     proc_data = pd.DataFrame()
    #
    #     print('Correcting times using sync pulses...')
    #
    #     lng = len(sync_pulses)
    #     printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
    #
    #     data_slices = []
    #     for j in range(len(sync_pulses.index) - 1):
    #     # for j in range(5):
    #         sync_index1 = sync_pulses.loc[j, 'x']           # get the sync pulse index of current initial sync pulse
    #         sync_index2 = sync_pulses.loc[j+1, 'x']         # get the sync pulse index of current next sync pulse
    #
    #         sync_time1 = sync_pulses.loc[j, 'time']        # get the time of the current initial sync pulse
    #         sync_time2 = sync_pulses.loc[j+1, 'time']        # get the time of the current initial sync pulse
    #         sync_time_diff = sync_time2 - sync_time1
    #
    #         ratio = sync_pulse_interval/sync_time_diff
    #         ratio = ()
    #
    #         # rel_time_diff = sync_time1 - t0             # get difference between current initial sync pulse and first sync pulse
    #         # rel_time_diff = j*sync_pulse_interval
    #         rel_time_diff = sync_index1*sync_pulse_interval
    #
    #         sync_df_index1 = data[(data.scatters == 122) & (data.x == sync_index1)].index[0]
    #         sync_df_index2 = data[(data.scatters == 122) & (data.x == sync_index2)].index[0]
    #
    #         data_slice = data.iloc[sync_df_index1+1:sync_df_index2,:]
    #
    #         # data_slice.time = (data_slice.time - sync_time1) + rel_time_diff
    #         data_slice.time = (data_slice.time - sync_time1)*ratio + rel_time_diff
    #
    #         data_slices.append(data_slice)
    #
    #         proc_data = proc_data.append(data_slice, ignore_index = True)
    #
    #         if j%int(lng/100) == 0:
    #             printProgressBar(j+2, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
    #
    #     print()
    #
    #     corrected_mod_data_list.append(data_slices)
    #
    #     # print(data_slices)
    #
    #     # Change format to match allevents expected format
    #     df = proc_data.filter(['scatters', 'energy', 'x', 'y', 'z', 'time'])
    #     df.insert(1, 'detector', i)
    #     df.energy /= 1000
    #     df.x /= 1000; df.y /= 1000; df.z /= 1000
    #
    #     allevents = allevents.append(df, ignore_index = True)
    #
    #     num_events = len(proc_data.index)
    #     tot_time = ((proc_data.iloc[-1]['time'] - proc_data.iloc[0]['time']) * 1e-8)
    #
    #     print('\nNumber of events:', num_events)
    #     print('Total time:', tot_time)
    #     print('Event rate:', num_events/tot_time, '\n')

    # Correct each module data array together - only works for two modules
    # --------------------------------------------------------------------
    corrected_mod_data_list = []

    data_mod0 = mod_data_list[0]        # event data from module 0
    sync_mod0 = mod_sync_list[0]        # sync pulse data from module 0

    data_mod1 = mod_data_list[1]        # event data from module 1
    sync_mod1 = mod_sync_list[1]        # sync pulse data from module 1

    proc_data_mod0 = pd.DataFrame()
    proc_data_mod1 = pd.DataFrame()

    print('Correcting times using sync pulses...')

    lng = min([len(sync_mod0.index), len(sync_mod1.index)])
    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    mod0s = []
    mod1s = []
    for i in range(lng - 1):
    # for i in range(5):
        sync_index1_mod0 = sync_mod0.loc[i, 'x']           # get the sync pulse index of current initial sync pulse
        sync_index2_mod0 = sync_mod0.loc[i+1, 'x']         # get the sync pulse index of current next sync pulse
        sync_index_diff_mod0 = sync_index2_mod0 - sync_index1_mod0

        sync_index1_mod1 = sync_mod1.loc[i, 'x']           # get the sync pulse index of current initial sync pulse
        sync_index2_mod1 = sync_mod1.loc[i+1, 'x']         # get the sync pulse index of current next sync pulse
        sync_index_diff_mod1 = sync_index2_mod1 - sync_index1_mod1


        sync_df_index1_mod0 = data_mod0[(data_mod0.scatters == 122) & (data_mod0.x == sync_index1_mod0)].index[0]
        sync_df_index2_mod0 = data_mod0[(data_mod0.scatters == 122) & (data_mod0.x == sync_index2_mod0)].index[0]

        data_slice_mod0 = data_mod0.iloc[sync_df_index1_mod0+1:sync_df_index2_mod0,:]


        sync_df_index1_mod1 = data_mod1[(data_mod1.scatters == 122) & (data_mod1.x == sync_index1_mod1)].index[0]
        sync_df_index2_mod1 = data_mod1[(data_mod1.scatters == 122) & (data_mod1.x == sync_index2_mod1)].index[0]

        data_slice_mod1 = data_mod1.iloc[sync_df_index1_mod1+1:sync_df_index2_mod1,:]


        sync_time1_mod0 = sync_mod0.loc[i, 'time']                  # get the time of the current initial sync pulse
        sync_time2_mod0 = sync_mod0.loc[i+1, 'time']                # get the time of the current initial sync pulse
        sync_time_diff_mod0 = (sync_time2_mod0 - sync_time1_mod0)/sync_index_diff_mod0
        # print(sync_time_diff_mod0)

        sync_time1_mod1 = sync_mod1.loc[i, 'time']                  # get the time of the current initial sync pulse
        sync_time2_mod1 = sync_mod1.loc[i+1, 'time']                # get the time of the current initial sync pulse
        sync_time_diff_mod1 = (sync_time2_mod1 - sync_time1_mod1)/sync_index_diff_mod1
        # print(sync_time_diff_mod1)


        sync_pulse_diff_mod0 = np.abs(sync_pulse_interval - sync_time_diff_mod0)
        sync_pulse_diff_mod1 = np.abs(sync_pulse_interval - sync_time_diff_mod1)
        # print(sync_pulse_diff_mod0)
        # print(sync_pulse_diff_mod1)
        # print()


        avg_sync_diff = (sync_time_diff_mod0 + sync_time_diff_mod1)/2
        #
        # ratio_mod0 = avg_sync_diff/sync_time_diff_mod0
        # ratio_mod1 = avg_sync_diff/sync_time_diff_mod1
        #
        ratio_mod0 = sync_pulse_interval/sync_time_diff_mod0
        ratio_mod1 = sync_pulse_interval/sync_time_diff_mod1


        rel_time_diff_mod0 = sync_index1_mod0*sync_pulse_interval
        rel_time_diff_mod1 = sync_index1_mod1*sync_pulse_interval


        # data_slice.time = (data_slice.time - sync_time1) + rel_time_diff
        data_slice_mod0.time = (data_slice_mod0.time - sync_time1_mod0)*ratio_mod0 + rel_time_diff_mod0
        data_slice_mod1.time = (data_slice_mod1.time - sync_time1_mod1)*ratio_mod1 + rel_time_diff_mod1

        # data_slice_mod0.time = (data_slice_mod0.time - sync_time1_mod0) * (1 + (sync_pulse_diff_mod0/sync_pulse_interval)) + rel_time_diff_mod0
        # data_slice_mod1.time = (data_slice_mod1.time - sync_time1_mod1) * (1 + (sync_pulse_diff_mod1/sync_pulse_interval)) + rel_time_diff_mod1

        mod0s.append(data_slice_mod0)
        mod1s.append(data_slice_mod1)

        proc_data_mod0 = proc_data_mod0.append(data_slice_mod0, ignore_index = True)
        proc_data_mod1 = proc_data_mod1.append(data_slice_mod1, ignore_index = True)


        printProgressBar(i+2, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    print('\n')

    # Combine module data into allevents dataframe
    # --------------------------------------------
    # # Change format to match allevents expected format
    # df = proc_data_mod0.filter(['scatters', 'energy', 'x', 'y', 'z', 'time'])
    # df.insert(1, 'detector', 0)
    # df.energy /= 1000
    # df.x /= 1000; df.y /= 1000; df.z /= 1000
    #
    # df.drop(df[df.scatters != 2].index, inplace = True)
    # df.reset_index(drop = True, inplace = True)
    #
    # num_events = len(df.index)
    # tot_time = ((df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8)
    #
    # print('Event information for module 0')
    # print('\nNumber of events:', num_events)
    # print('Total time:', tot_time)
    # print('Event rate:', num_events/tot_time, '\n')
    #
    # allevents = allevents.append(df, ignore_index = True)
    #
    # # Change format to match allevents expected format
    # df = proc_data_mod1.filter(['scatters', 'energy', 'x', 'y', 'z', 'time'])
    # df.insert(1, 'detector', 1)
    # df.energy /= 1000
    # df.x /= 1000; df.y /= 1000; df.z /= 1000
    #
    # df.drop(df[df.scatters != 2].index, inplace = True)
    # df.reset_index(drop = True, inplace = True)
    #
    # num_events = len(df.index)
    # tot_time = ((df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8)
    #
    # print('Event information for module 1')
    # print('\nNumber of events:', num_events)
    # print('Total time:', tot_time)
    # print('Event rate:', num_events/tot_time, '\n')
    #
    # allevents = allevents.append(df, ignore_index = True)
    #
    # allevents.sort_values(by = 'time', inplace = True)                                 # sort the columns by time
    # allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering
    #
    # #  convert detector, scatters, time columns from float (default) into ints
    # allevents.scatters = allevents.scatters.astype(int)
    # allevents.detector = allevents.detector.astype(int)
    # allevents.time = allevents.time.astype(int)
    #
    # # allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)
    # # allevents.reset_index(drop = True, inplace = True)                                 # reset the index numbering
    #
    # # print(allevents)
    #
    # num_events = len(allevents.index)
    # tot_time = ((allevents.iloc[-1]['time'] - allevents.iloc[0]['time']) * 1e-8)
    #
    # print('Number of events:', num_events)
    # print('Total time:', tot_time)
    # print('Event rate:', num_events/tot_time)
    # print()
    #
    # return

    # Running coincidence processing in-between sync pulses
    # -----------------------------------------------------
    print('Running further processing...')
    e_window = [461, 561, 310, 370]
    coin_window = 600
    d0_transformation = [180.0, 90.0, 0.0, -41.23, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 41.23, 0.0, 0.0]
    transformations = [d0_transformation, d1_transformation]

    meas_rates = []
    sing_rates = []
    rand_rates = []
    window = 1000

    mod0_totals = []
    mod1_totals = []

    lng = len(mod0s)
    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    for i in range(lng):
    # for i in range(5):

        # Total numbers between sync pulses
        # ---------------------------------
        # mod0 = mod0s[i]
        # mod1 = mod1s[i]
        #
        # mod0_totals.append(len(mod0.index))
        # mod1_totals.append(len(mod1.index))

        # Convert module data into allevents data
        # ---------------------------------------
        mod0 = mod0s[i]
        mod1 = mod1s[i]

        allevents0 = mod0.filter(['scatters', 'energy', 'x', 'y', 'z', 'time'])
        allevents0.insert(1, 'detector', 0)
        allevents0.energy /= 1000
        allevents0.x /= 1000; allevents0.y /= 1000; allevents0.z /= 1000
        allevents0.drop(allevents0[allevents0.scatters != 1].index, inplace = True)
        allevents0.reset_index(drop = True, inplace = True)                           # reset the index numbering

        # Print module file information
        # -----------------------------
        num_events = len(allevents0.index)
        tot_time = ((allevents0.iloc[-1]['time'] - allevents0.iloc[0]['time']) * 1e-8)

        # print('Event information for module 0')
        # print('\nNumber of events:', num_events)
        # print('Total time:', tot_time)
        # print('Event rate:', num_events/tot_time, '\n')

        allevents1 = mod1.filter(['scatters', 'energy', 'x', 'y', 'z', 'time'])
        allevents1.insert(1, 'detector', 1)
        allevents1.energy /= 1000
        allevents1.x /= 1000; allevents1.y /= 1000; allevents1.z /= 1000
        allevents1.drop(allevents1[allevents1.scatters != 1].index, inplace = True)
        allevents1.reset_index(drop = True, inplace = True)                           # reset the index numbering

        # Print module file information
        # -----------------------------
        num_events = len(allevents1.index)
        tot_time = ((allevents1.iloc[-1]['time'] - allevents1.iloc[0]['time']) * 1e-8)

        # print('Event information for module 1')
        # print('\nNumber of events:', num_events)
        # print('Total time:', tot_time)
        # print('Event rate:', num_events/tot_time, '\n')


        allevents = allevents0.append(allevents1, ignore_index = True)
        allevents.sort_values(by = 'time', inplace = True)                           # sort the columns by time

        # allevents.drop(allevents[ (allevents['energy'] < e_window[2]) |      # ...less than the lower Compton edge limit...
        #                                     ((allevents['energy'] > e_window[3]) & (allevents['energy'] < e_window[0])) | # ...or between the upper Compton edge limit and the lower peak limit...
        #                                     (allevents['energy'] > e_window[1])].index, inplace = True)    # ...or greater than the upper peak limit.

        # allevents.drop(allevents[allevents.scatters != 1].index, inplace = True)
        #
        # transformation_matrices = {}
        #
        # # Creating transformation matrices
        # for i, arr in enumerate(transformations):
        #     transformation_matrices[i] = utils.get_transformation_matrix_array(arr)
        #
        # #  applying coordinate transformation to data
        # print()
        # allevents = utils.apply_transformation(allevents, transformation_matrices)
        # print()

        allevents.reset_index(drop = True, inplace = True)                           # reset the index numbering

        tot_sings = len(allevents.index)
        total_time = (allevents.iloc[-1]['time'] - allevents.iloc[0]['time'])*1e-8     # get the total time in units of 1e-8 seconds
        tot_rate = tot_sings/total_time

        # print('Event information for allevents after filtering')
        # print('\nNumber of events:', tot_sings)
        # print('Total time:', total_time)
        # print('Event rate:', tot_rate, '\n')

        # Forcing coincidences and checking the results
        # ---------------------------------------------
        # # mod0 = allevents[allevents.detector == 0]
        # # mod0.reset_index(drop = True, inplace = True)
        # # mod1 = allevents[allevents.detector == 1]
        # # mod1.reset_index(drop = True, inplace = True)
        # #
        # # # print(mod0)
        # # # print(mod1)
        # #
        # # coin_dics = []
        # #
        # # for j in range(min([len(mod0), len(mod1)])):
        # #     coin_dics.append({'t1':mod0.time[j], 'x1':mod0.x[j], 'y1':mod0.y[j], 'z1':mod0.z[j],
        # #                       't2':mod1.time[j], 'x2':mod1.x[j], 'y2':mod1.y[j], 'z2':mod1.z[j]})
        #
        # coin_dics = []
        #
        # for j in range(len(allevents.index)-1):
        #     if allevents.detector[j] != allevents.detector[j+1]:
        #         coin_dics.append({'t1':allevents.time[j], 'x1':allevents.x[j], 'y1':allevents.y[j], 'z1':allevents.z[j],
        #                           't2':allevents.time[j+1], 'x2':allevents.x[j+1], 'y2':allevents.y[j+1], 'z2':allevents.z[j+1]})
        #
        # forced_coins = pd.DataFrame(coin_dics)
        #
        # print('Total number: {}'.format(len(forced_coins)))
        # print('Total time: {}'.format(total_time))
        # print('Total rate: {}'.format(len(forced_coins)/total_time))

        # Remove lines using the PEPT algorithm and a given f-parameter
        # -------------------------------------------------------------
        # distances = []
        # pos = np.array([-0.3, 0.6, 12.8])
        # f = 7
        #
        # # Calculate all the distances to the position
        # for index, coin in forced_coins.iterrows():
        #     # First get the straight line equation
        #     # m = (coin['y2']-coin['y1'])/(coin['x2']-coin['x1'])
        #     # c = coin['y1'] - m*coin['x1']
        #     #
        #     # d = (np.abs(m*pos[0]-pos[1]+c))/(np.sqrt(m**2 + 1))     # get the distance from the line to the point
        #
        #     coin_v1 = np.array([coin.x1, coin.y1, coin.z1])
        #     coin_v2 = np.array([coin.x2, coin.y2, coin.z2])
        #
        #     d = (coin_v2 - coin_v1)/np.linalg.norm(coin_v2 - coin_v1)
        #     v = pos - coin_v1
        #     t = np.dot(d,v)
        #     D = coin_v1 + t*d
        #
        #     distances.append(np.linalg.norm(pos - D))
        #
        # # plt.hist(distances, 50)
        # # plt.show()
        #
        # forced_coins['d'] = distances       # add the distances to the coincidence
        #
        # print('Sorting by distance...')
        #
        # forced_coins.sort_values('d', inplace = True)       # sort by the distance from the location
        #
        # print('Done.\n')
        #
        # # Get the f-percentage of data with the shortest distances
        # N_0 = len(forced_coins.index)
        # N_f = int((f/100)*N_0)
        # f_forcedcoins = forced_coins.head(N_f)
        # # r_subcoins = forced_coins.tail(N_0 - N_f)
        #
        # t_diff = (f_forcedcoins.t2 - f_forcedcoins.t1).to_numpy()
        # # print(t_diff)
        # t_diff = t_diff[t_diff < 1000]
        # w = 10       # set bin width in 10ns
        # num_bins = int(np.ceil((max(t_diff) - min(t_diff))/w))       # get number of bins based on width
        # plt.hist(t_diff, bins = num_bins)
        # plt.show()

        # Running normal coincidence processing
        # -------------------------------------
        # allevents, coins = process_coincidences(data_dir, e_window, coin_window, transformations,
        #                                         singles_only = True,
        #                                         determine_randoms = False,
        #                                         det_delays = [],
        #                                         apply_ce_filtering = True,
        #                                         count_doubles = False,
        #                                         count_deadtime_peak = False,
        #                                         allevents = allevents.copy(), log_str = '')

        # Plotting the coincidence LORs
        # -----------------------------
        # # df = f_forcedcoins
        # df = coins
        # p0_xPos = df.iloc[:, 1]; p0_yPos = df.iloc[:, 2]; p0_zPos = df.iloc[:, 3]
        # p1_xPos = df.iloc[:, 5]; p1_yPos = df.iloc[:, 6]; p1_zPos = df.iloc[:, 7]
        #
        # num = len(p0_xPos)
        #
        # print('Plotting XY coincidences of first', num, 'coincidences...')
        # for i in range(0, num):
        #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
        #     plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
        # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)))
        # plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')
        # # plt.savefig(data_dir + 'basic_plots/xy-coincidences_N-' + str(num) + '.png')
        # plt.show()
        # plt.cla(); plt.clf()

        # Count the coincidences for this pair of sync-pulses using utils
        # ---------------------------------------------------------------
        # det_time_array_ = allevents[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array
        #
        # # Add column of zeros for flagging coincidences
        # det_time_array = np.zeros((len(det_time_array_),3))
        # det_time_array[:,:-1] = det_time_array_
        #
        # tot_coins = utils.count_coincidences3(det_time_array, window)

        # Count the coincidences for this pair of sync-pulses using count_coincidences function
        # -------------------------------------------------------------------------------------
        coin_window = 10000
        tot_coins, coin_rate = count_coincidences(allevents, coin_window)

        # print(allevents)
        allevents.time[allevents.detector == 1] += 5e7
        allevents.sort_values(by = 'time', inplace = True)                           # sort the columns by time
        allevents.reset_index(drop = True, inplace = True)                           # reset the index numbering
        # print(allevents)

        tot_rands, rand_rate = count_coincidences(allevents, coin_window)

        # print('Coincidence rate: ', coin_rate)

        meas_rates.append(coin_rate)
        sing_rates.append(tot_sings/total_time)

        # rand_rates.append(coin_window*1E-9*(tot_rate/2)**2)
        rand_rates.append(rand_rate)

        # print('==========================================\n')

        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

        # Running timing resolution
        # -------------------------
        # print('Running timing resolution between detector 0 and detector 1.')
        #
        # rates = []      # array of rates
        # delays = np.linspace(-5000, 5000, 50)
        # # delays = np.linspace(-2000002500, -1999997500, 50)
        # # delays = np.linspace(1999997500, 2000002500, 50)
        #
        # window = 60
        #
        # lng = len(delays)
        # printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
        #
        # tot_coins = 0
        # for i, delay_ns in enumerate(delays):
        #
        #     delay = delay_ns/10     # convert nanoseconds into 10s of nanoseconds
        #
        #     allevents_delay = allevents.copy()                                   # make copy for adding time delay
        #
        #     allevents_delay.loc[allevents_delay.detector == 1, 'time'] += delay  # add time delay to detector
        #     allevents_delay.sort_values(by = 'time', inplace = True)             # sort the columns by time
        #
        #     det_time_array_delay_ = allevents_delay[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array
        #
        #     # Add column of zeros for flagging coincidences
        #     det_time_array_delay = np.zeros((len(det_time_array_delay_),3))
        #     det_time_array_delay[:,:-1] = det_time_array_delay_
        #
        #     tot_coins = utils.count_coincidences3(det_time_array_delay, window)
        #
        #     # print(tot_coins)
        #
        #     rates.append(tot_coins/total_time)
        #
        #     printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar
        #
        # # Plot the final results
        # plt.xlabel('Time Delay (ns)')
        # plt.ylabel('Coincidence Rate (Hz)')
        #
        # plt.plot(delays, rates, '.')#, markersize = 20)
        # plt.show()


    # Plot coincidence rates
    # ----------------------
    sing_rates = np.array(sing_rates)
    meas_rates = np.array(meas_rates)
    rand_rates = np.array(rand_rates)

    efficiency = 0.0565
    pred_rates = efficiency*(sing_rates/2)
    plt.plot(pred_rates, '.', label = 'Predicted True Rate')
    plt.plot(meas_rates, '.', label = 'Measured Prompt Rate')
    plt.plot(rand_rates, '.', label = 'Measured Randoms Rate')
    plt.plot(meas_rates-rand_rates, '.', label = 'Measured True Rate')
    # plt.ylim(0, max([max(pred_rates), max(meas_rates)]) + 5)
    plt.legend()
    plt.show()

    # Plot singles vs time
    # --------------------
    # plt.plot(mod0_totals, '.')
    # plt.plot(mod1_totals, '.')
    # plt.show()

    return

    e_window = [461, 561, 310, 370]
    coin_window = 600
    d0_transformation = [180.0, 90.0, 0.0, -41.23, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 41.23, 0.0, 0.0]
    transformations = [d0_transformation, d1_transformation]
    # det_delays = [-2000000060]
    det_delays = [0]
    process_coincidences(data_dir, e_window, coin_window, transformations,
                         singles_only = True,
                         determine_randoms = False,
                         det_delays = det_delays,
                         apply_ce_filtering = True,
                         count_doubles = False,
                         count_deadtime_peak = False,
                         allevents = allevents.copy(), log_str = '')

    plot_basic(data_dir, transformations, 'pept_coincidence_data', allevents = allevents.copy(),
               num_modules = 2,
               plot_singles = False,
               plot_coincidences = True)

    return

    e_window = [461, 561, 310, 370]
    coin_window = 600
    d0_transformation = [180.0, 90.0, 0.0, -41.23, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 41.23, 0.0, 0.0]
    transformations = [d0_transformation, d1_transformation]
    # det_delays = [-2000000060]
    det_delays = [0]
    process_coincidences(data_dir, e_window, coin_window, transformations,
                         singles_only = True,
                         determine_randoms = False,
                         det_delays = det_delays,
                         apply_ce_filtering = True,
                         count_doubles = False,
                         count_deadtime_peak = False,
                         allevents = allevents.copy(), log_str = '')

    return

    # allevents_np = allevents.to_numpy()     # convert to numpy array for easier counting
    # coinwindow = 600
    #
    # tot_coincidences = 0
    # tot_mult_coins = 0          # total number of multiple coincidences (more than one coincidence found)
    # for i, curr_event in enumerate(allevents_np):
    #     # if i%1000 == 0:
    #         # print(i)
    #     # print('Current event:', curr_event)
    #
    #     current_event_time = curr_event[-1]
    #     # print('Current event time:', current_event_time)
    #
    #     j = i+1   # get next event index
    #     if j == len(allevents_np):       # make sure not to iterate past the end
    #         continue
    #
    #     mult_coins = -1
    #     # print('Next event time:', allevents_np[j][-1])
    #     # print('Upper window: ', current_event_time + coinwindow)
    #     while allevents_np[j][-1] <= current_event_time + coinwindow:
    #         # print(i)
    #         # print('Upper window = ', current_event_time + 30)
    #         # print('Current event = ', curr_event)
    #         # print('Next event = ', allevents_np[j])
    #         # print()
    #         # print('Next event detector module:', allevents_np[j][1])
    #         if allevents_np[j][1] != curr_event[1]:      # if not in the same detector
    #             tot_coincidences += 1
    #             mult_coins += 1
    #         j += 1
    #         if j == len(allevents_np):
    #             break
    #
    #     if mult_coins > 0:
    #         tot_mult_coins += 1
    #
    #     # print('\n\n')
    #
    #     # j = i-1   # get previous event index
    #     # if j == -1:       # make sure not to iterate past the beginning
    #     #     continue
    #     #
    #     # while allevents_np[j][-1] >= current_event_time - 30:
    #     #     # print(i)
    #     #     # print('Lower window = ', current_event_time - 30)
    #     #     # print('Current event = ', curr_event)
    #     #     # print('Next event = ', allevents_np[j])
    #     #     # print('==========================\n')
    #     #     if allevents_np[j][1] != curr_event[1]:      # if not in the same detector
    #     #         tot_coincidences += 1
    #     #     j -= 1
    #     #     if j == -1:
    #     #         break
    #
    #
    # print('Total Number of Coincidences: {:d}'.format(tot_coincidences))
    # print('Final coincidence rate: {:.2f}Hz'.format(tot_coincidences/tot_time))
    # print('Total number of multiple coincidence events: {:d}'.format(tot_mult_coins))

    t_diff = allevents.time.diff().to_numpy()[1:]      # get the time difference between successive events
    t_diff *= 1e-2                                 # convert to microseconds
    # for i in range(len(t_diff)-5, len(t_diff)):
        # print(t_diff[i])
    t_diff = t_diff[t_diff < 2000]           # drop differences above given time
    # t_diff = t_diff[t_diff != 0]          # drop differences from multiple scatter events

    # return

    w = 2       # set bin width in microseconds
    num_bins = int(np.ceil((max(t_diff) - min(t_diff))/w))       # get number of bins based on width
    # print(num_bins)

    # n, bins, patches = plt.hist(t_diff, 'auto', density = 0)
    n, bins, patches = plt.hist(t_diff, num_bins, density = 0)
    plt.xlabel(r'Time difference ($\mu$s)')
    plt.ylabel('Number of events')
    plt.show()

    return

# Looking into sync-pulse timing discrepencies
def sync_pulse_information():

    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run3-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run16-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run18-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'

    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    data_dir = '/Volumes/Nicholas/Polaris/data/191203/'

    # num_modules = 4

    data_files = []
    for subdir, dirs, files in os.walk(data_dir):
        for dir in dirs:
            # if dir[0] != 'J':
            #     continue

            # if 'circle' not in dir:
            #     continue

            if 'ignore' in dir or 'rate_files' in dir:
                continue

            run_file = '{}{}/mod52.txt'.format(data_dir, dir)
            data_files.append(run_file)

        break

    data_files = sorted(data_files, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number

    all_diffs = []
    # count = 0
    for run_file in data_files:
        print('Reading in {}...'.format(run_file))
        data = pd.read_csv(run_file, sep = '	', header = None)           # read in text file as a csv with tab separation
        data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']            # separate into columns and name them
        # data['time'][data.scatters == 122] = data['y'][data.scatters == 122]    # move the time value to the time column
        # data.time = data.time.astype(int)                                       # convert time to an integer
        # data.sort_values(by = 'time', inplace = True)                           # sort the columns by time
        # data.reset_index(drop = True, inplace = True)                           # reset the index numbering
        # data.drop(data[data.scatters != 1].index, inplace = True)             # get single-interaction information
        # data.reset_index(drop = True, inplace = True)                         # reset the indicies
        print('Done.\n')

        sync_pulses = data[data.scatters == 122]        # get the sync pulses
        sync_pulses.reset_index(drop = True, inplace = True)

        sync_diffs = 200000006 - np.diff(sync_pulses.y.to_numpy())

        sync_diffs = sync_diffs[sync_diffs > 0]

        all_diffs.append(sync_diffs)

        print('Average difference: {} +- {}'.format(np.mean(sync_diffs), np.sqrt(np.var(sync_diffs))))

        print('-----------------------------------------------------', '\n')

        # if count == 2:
        #     break
        #
        # count += 1

    all_diffs = np.array(all_diffs)
    all_diffs = np.hstack(all_diffs)
    print(all_diffs)
    plt.plot(all_diffs)
    plt.show()

    return

    file = 'mod52.txt'

    print('Reading in {}...'.format(data_dir + file))
    data = pd.read_csv(data_dir + file, sep = '	', header = None)           # read in text file as a csv with tab separation
    data.columns = ['scatters', 'x', 'y', 'z', 'energy', 'time']            # separate into columns and name them
    data['time'][data.scatters == 122] = data['y'][data.scatters == 122]    # move the time value to the time column
    data.time = data.time.astype(int)                                       # convert time to an integer
    data.sort_values(by = 'time', inplace = True)                           # sort the columns by time
    data.reset_index(drop = True, inplace = True)                           # reset the index numbering
    # data.drop(data[data.scatters != 1].index, inplace = True)             # get single-interaction information
    # data.reset_index(drop = True, inplace = True)                         # reset the indicies
    print('Done.\n')

    sync_pulses = data[data.scatters == 122]        # get the sync pulses
    sync_pulses.reset_index(drop = True, inplace = True)

    sync_diffs = 200000006 - np.diff(sync_pulses.time.to_numpy())

    sync_diffs = sync_diffs[sync_diffs > 0]

    print('Average difference: {} +- {}'.format(np.mean(sync_diffs), np.sqrt(np.var(sync_diffs))))

    plt.plot(sync_diffs)
    plt.show()

    return

# Comparing count_coincidences3 and find_coincidences2
def compare_count_vs_find():

    # Activity function for calculating the activity
    def A(t, A_0, t_h):
        return A_0 * 2**(-t/t_h)

    # d0_transformation = [180.0, 90.0, 0.0, -41.0, 0.0, 0.0]
    # d1_transformation = [0.0, 270.0, 0.0, 41.0, 0.0, 0.0]
    # transformations = [d0_transformation, d1_transformation]

    coin_window = 10000
    e_window = []
    A_0 = 201.66
    t_half = 4080
    t_int = 2

    allevents, log_str = read_polaris_data('/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/AllEventsCombined.txt',
                                           transformations = [],
                                           e_window = e_window,
                                           singles_only = False,
                                           write_transformed_data = False)

    allevents.loc[allevents.detector == 1, 'time'] += -200000006            # add time delay (in 10s of ns)
    allevents.sort_values(by = 'time', inplace = True)                          # sort the columns by time
    allevents.reset_index(drop = True, inplace = True)                          # reset the indicies

    # allevents.time[allevents.detector == 1] += 6400             # add 64us millisecond delay to module 1
    allevents.time[allevents.detector == 1] += 5e7                # add 0.5s delay to module 1
    allevents.sort_values(by = 'time', inplace = True)     # sort the columns by time
    allevents.reset_index(drop = True, inplace = True)     # reset the indicies

    print('Splitting dataframe and counting coincidence rates...\n')

    S = allevents.time
    splitevents = [g.reset_index(drop=True) for i, g in allevents.groupby([((S - S[0])/(t_int*1e8)).astype('int')])]

    lng = len(splitevents)

    printProgressBar(0, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

    rates, activities = [], []
    t_elapsed = 0
    for i, df in enumerate(splitevents):

        df_time = (df.iloc[-1]['time'] - df.iloc[0]['time']) * 1e-8     # get the total observation time
        t_elapsed += df_time                    # keep track to total elapsed time
        activity = A(t_elapsed, A_0, t_half)    # calculate the activity at the end of the period
        activities.append(activity)             # store the activity

        # First count the number of prompt coincidences
        # ---------------------------------------------

        det_time_array_ = df[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array

        # Add column of zeros for flagging coincidences
        det_time_array = np.zeros((len(det_time_array_),3))
        det_time_array[:,:-1] = det_time_array_

        tot_coins = utils.count_coincidences3(det_time_array, coin_window/10)

        rates.append(tot_coins/df_time)      # store the rate

        printProgressBar(i+1, lng, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

    plt.plot(activities, rates, '.')
    plt.xlabel('Activity (kBq)')
    plt.ylabel('Coincidence Rate (Hz)')
    plt.show()

    # det_time_array_ = allevents[['detector', 'time']].values     # get the detector and time columns as a 2D numpy array
    #
    # # Add column of zeros for flagging coincidences
    # det_time_array = np.zeros((len(det_time_array_),3))
    # det_time_array[:,:-1] = det_time_array_

    # tot_coins_count = utils.count_coincidences3(det_time_array, coin_window/10, single_counting = True)
    #
    # l = len(allevents.index)
    # det_time_array_ = allevents[['detector', 'x', 'y', 'z', 'time']].values        # get the detector and time columns as a 2D numpy array
    #
    # # Add column of zeros for flagging coincidences
    # det_time_array = np.zeros((l,6))
    # det_time_array[:,:-1] = det_time_array_
    #
    # coincidence_data, tot_coins, tot_coins_per_event = utils.find_coincidences2(det_time_array, coin_window/10, single_counting = True, choose_random = False)
    # print()
    # print('count_coincidences3: ', tot_coins_count)
    # print('find_coincidences2: ', tot_coins)

    return


# Main function used primarily to run the functions written above. Ignored if importing file as a library
def main():
    # ---------
    # Debugging
    # ---------

    # coincidence_processing_debug()
    # monte_carlo_solid_angle(31, num_crystals = 2)
    # sync_pulse_debugging()
    # process_coincidences_debug()
    # counting_511s()
    # data_file_info()
    # combine_module_data()
    # sync_pulse_information()
    # deadtime_model_examples()
    # deadtime_coincidences_model_examples()
    # compare_count_vs_find()

    # return

    # Global variables
    # ====================================================================================

    # 3D printer stage coordinate transformations
    # -------------------------------------------
    # d0_transformation = [180.0, 90.0, 0.0, -41.23, 0.0, 0.0]
    # d1_transformation = [0.0, 270.0, 0.0, 41.23, 0.0, 0.0]
    # transformations = [d0_transformation, d1_transformation]

    # 3D printer stage coordinate transformations - 200210
    # ----------------------------------------------------
    d0_transformation = [180.0, 90.0, 0.0, -48, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 48, 0.0, 0.0]
    transformations = [d0_transformation, d1_transformation]


    # 3D printed slide coordinate transformations
    # -------------------------------------------
    # d0_transformation = [180.0, 90.0, 0.0, -40.5, 0.0, 0.0]
    # d1_transformation = [0.0, 270.0, 0.0, 40.5, 0.0, 0.0]
    # transformations = [d0_transformation, d1_transformation]

    # 190912
    # ------
    # d0_transformation = [180.0, 90.0, 0.0, -41.0, 0.0, 0.0]
    # d1_transformation = [0.0, 270.0, 0.0, 41.0, 0.0, 0.0]
    # transformations = [d0_transformation, d1_transformation]

    # 191106 Nist System
    # ------------------
    # d0_transformation = [0.0, 90.0, 0.0, -27.8, -10.0, 0.0]
    # d1_transformation = [0.0, -90.0, 0.0, 27.8, 10.0, 0.0]

    # d0_transformation = [0.0, 90.0, 0.0, -77.08, -10.0, 0.0]
    # d1_transformation = [0.0, -90.0, 0.0, 77.08, 10.0, 0.0]

    # d0_transformation = [0.0, 90.0, 0.0, -42.22, 3.15, 0.0]
    # d1_transformation = [0.0, -90.0, 180.0, 42.22, -3.15, 0.0]
    #
    # d0_transformation = [0.0, 90.0, 0.0, 0.0, 3.15, 0.0]
    # d1_transformation = [0.0, -90.0, 180.0, 0.0, -3.15, 0.0]
    #
    # transformations = [d0_transformation, d1_transformation]

    # 191106 PET System
    # -----------------
    # d1_transformation = [90.0, 90.0, 0.0, -48.65, 0.0, 0.0]
    # d2_transformation = [90.0, -90.0, 0.0, 48.65, 0.0, 0.0]
    # d3_transformation = [90.0, 0.0, 0.0, 0.0, 48.65, 0.0]
    # d4_transformation = [90.0, 180.0, 0.0, 0.0, -48.65, 0.0]
    # transformations = [d1_transformation, d2_transformation, d3_transformation, d4_transformation]

    # Filtering energy window
    # e_window = [461, 561, 310, 370]
    # e_window = [504, 519, 330, 350]
    # e_window = [496, 526]
    e_window = [461, 561]

    # Coincidence timing window in nanoseconds
    coin_window = 600

    # x, y and z errors as determined by fopt in millimeters
    # errors = [0.60, 0.14, 0.05]

    # ====================================================================================

    # ----------------
    # Basic processing
    # ----------------

    # # det_delays = [0, 0, 0]
    # det_delays = [0]
    # # coin_window = 5000
    # # e_window = []
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    #
    # data_dir = '/Volumes/Nicholas/Polaris/data/190218/'
    # num_modules = 2
    #
    # for subdir, dirs, files in os.walk(data_dir):
    #     for dir in dirs:
    #         # if 'circle' not in dir:
    #         #     continue
    #
    #         if dir[0] != 'J':
    #             continue
    #
    #         run_dir = '{}{}/'.format(data_dir, dir)
    #         allevents, log_str = read_polaris_data(run_dir + 'AllEventsCombined.txt', transformations, e_window = e_window, singles_only = False, write_transformed_data = False)
    #         process_coincidences(run_dir, coin_window, transformations,
    #                              e_window = e_window,
    #                              singles_only = False,
    #                              determine_randoms = False,
    #                              include_all_coincidences = False,
    #                              det_delays = det_delays,
    #                              count_doubles = False,
    #                              count_deadtime_peak = False,
    #                              allevents = allevents.copy(), log_str = log_str)
    #         # plot_basic(run_dir, transformations, 'pept_coincidence_data', allevents = allevents.copy(),
    #         #            num_modules = 2,
    #         #            plot_singles = True,
    #         #            plot_coincidences = True)
    #         print('-----------------------------------------------------', '\n')
    #     break
    #
    # return

    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run7-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run13-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run2-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/10min/'

    # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/5min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run21-na22_1uCi-circle_1.5mm_10mms/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191113/P_191113-run7-na22_1uCi-centre-30min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'


    # det_delays = [-1999983890]
    # det_delays = [-2000000060]
    det_delays = [0]
    # det_delays = [0, 0, 0]

    coin_window = 5000
    # e_window = []
    e_window = [503, 519]

    # resolution_delays = np.linspace(-2000005000, -1999995000, 50)
    # resolution_delays = np.linspace(-1999986390, -1999981390, 50)
    # resolution_delays = np.linspace(-1999986390, -1999983000, 50)
    # resolution_delays = np.linspace(-10000, 10000, 50)
    # resolution_delays = np.linspace(-2500, 2500, 50)
    resolution_delays = np.linspace(-700, 700, 50)

    timing_resolution2(data_dir, transformations, e_window, delays = resolution_delays, window = 300, read_txt = True, perform_fit = True, num_detectors = 2, shade_window = [-221, 379, 'red'])

    # plot_energy_spectrum(data_dir, e_range = (50, 600))

    # allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations, e_window = e_window, singles_only = False, write_transformed_data = False)
    # process_coincidences(data_dir, coin_window, transformations,
    #                      e_window = e_window,
    #                      singles_only = False,
    #                      include_all_coincidences = False,
    #                      determine_randoms = False,
    #                      det_delays = det_delays,
    #                      count_doubles = False,
    #                      count_deadtime_peak = False,
    #                      allevents = allevents.copy(), log_str = log_str)
    # plot_basic(data_dir, transformations, 'pept_coincidence_data', #allevents = allevents.copy(),
    #            num_modules = 2,
    #            plot_singles = False,
    #            plot_coincidences = True)

    # uncertainty_range = (0.9, 1.0)
    # N_range = (5, 500)
    # f_range = [4, 16, 2]
    # N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range, f_range)
    # print(N, fopt, uncert)

    # N = 1000
    # # fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    # fopt_logfilename = '{}/fopt-N{}-{}ns-no_filtering.log'.format(data_dir, N, coin_window)
    # f_optimisation(data_dir, 1, 10, 1, N, log_filename = fopt_logfilename, plot_curve = False)

    # data_dir = "/Volumes/Nicholas/Polaris/data/191203/191203-run4-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/300ns-100keV-ce-no_doubles/"
    # f_opt_demonstration(data_dir,
    #                     f = 20, N = 200,
    #                     plot_lines_only = False,
    #                     plot_highlighted = True,
    #                     plot_accepted_only = True)

    return


    # --------------------------------
    # Basic processing parameter sweep
    # --------------------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191112/P_191112-run5-na22_1uCi_centre-2min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191112/ignore/P_191112-run6-na22_91uCi_centre-2min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run3-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    #
    # allevents, log_str = read_polaris_data(data_dir + 'AllEventsCombined.txt', transformations)
    # # N = 200
    # # det_delays = [-66, -17]
    # # det_delays = [72]
    # # det_delays = [-2000000060]
    # # det_delays = [-1999983890]
    # det_delays = [0]
    #
    # # coin_windows = [300, 600]
    # coin_windows = [600, 5000]
    #
    # # e_windows = [[504, 519, 330, 350], [461, 561, 310, 370]]        # by eye
    # # e_windows = [[501, 522, 330, 350], [461, 561, 310, 370]]        # fwtm, full peak
    # # e_windows = [[506, 518, 335, 346], [501, 522, 330, 350]]        # by fit
    #
    # # e_windows = [[508, 514, 338, 344], [505, 517, 335, 347], [461, 561, 310, 370]]          # fwhm, fwtm, full peak
    # # e_windows = [[508, 514, 338, 344], [461, 561, 310, 370]]          # fwhm, full peak
    # # e_windows = [[461, 561], []]          # fwhm, full peak
    # e_windows = [[496, 526], []]          # 6 sigma, no filter
    #
    # uncertainty_range = (0.7, 0.8)
    # N_range = (5, 100)
    # f_range = [10, 100, 2]
    #
    # for coin_window in coin_windows:
    #     for e_window in e_windows:
    #         process_coincidences(data_dir, coin_window, transformations,
    #                              e_window = e_window,
    #                              singles_only = False,
    #                              determine_randoms = False,
    #                              det_delays = det_delays,
    #                              count_doubles = False,
    #                              count_deadtime_peak = False,
    #                              allevents = allevents.copy(), log_str = log_str
    #                              )
    #         N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range, f_range)
    #         if e_window == []:
    #             fopt_logfilename = '{}/fopt-N{}-{}ns-no_efilter.log'.format(data_dir, N, coin_window)
    #         else:
    #             fopt_logfilename = '{}/fopt-N{}-{}ns-e_filtered.log'.format(data_dir, N, coin_window)
    #
    #         f_optimisation(data_dir, f_range[0], f_range[1], 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    # return


    # Old coincidence processor
    # -------------------------
    # # Fixed Uncertainty
    # # =================
    # for coin_window in coin_windows:
    #     for e_window in e_windows:
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = False, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #         N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-no_ce-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = True, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #         N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = False, count_doubles = True, allevents = allevents.copy(), log_str = read_log)
    #         N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-no_ce-doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = True, count_doubles = True, allevents = allevents.copy(), log_str = read_log)
    #         N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    # # Fixed N
    # # =======
    # N = 50
    #
    # for coin_window in coin_windows:
    #     for e_window in e_windows:
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = False, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-no_ce-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = True, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = False, count_doubles = True, allevents = allevents.copy(), log_str = read_log)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-no_ce-doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #         process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = True, count_doubles = True, allevents = allevents.copy(), log_str = read_log)
    #         fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    #         f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    # # Fixed N and timing window
    # # =========================
    # coin_window = 600
    # N = 50
    #
    # for e_window in e_windows:
    #     process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = False, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #     fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-no_ce-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1])
    #     f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    #     process_coincidences(data_dir, e_window, coin_window, transformations, det_delays = det_delays, apply_ce_filtering = True, count_doubles = False, allevents = allevents.copy(), log_str = read_log)
    #     fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    #     f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = False)
    #
    # return

    # # Single dataset
    # # ==============
    # N_range = (10, 100)
    # uncertainty_range = [0.7, 0.8]
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run5-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    #
    # N, fopt, uncert = find_N_for_sigma(data_dir, N_range, uncertainty_range)
    # fopt_logfilename = '{}/fopt-N{}-{}ns-{}_{}keV-ce_{}_{}keV-no_doubles.log'.format(data_dir, N, coin_window, e_window[0], e_window[1], e_window[2], e_window[3])
    # f_optimisation(data_dir, 1, N, log_filename = fopt_logfilename, plot_curve = True)
    #
    # return


    # --------------------------------
    # Processing Monte Carlo beam data
    # --------------------------------

    e_window = [510, 512]

    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_iTL_HDPE_066MeV_fourDet_06cm-100M/testing/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_iTL_HDPE_200MeV_fourDet_06cm-100M/testing/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_HDPE_200MeV_fourDet_06cm_only1x-100M/testing/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_HDPE_200MeV_fourDet_06cm_only1x-050M/testing/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_PMMA_055MeV_fourDet_06cm_only1x-500M/testing/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_PMMA_055MeV_fourDet_oneModule_05cm_only1x-500M/testing/'

    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_iTL_HDPE_066MeV_fourDet_06cm-100M/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_iTL_HDPE_200MeV_fourDet_06cm-100M/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_HDPE_200MeV_fourDet_06cm_only1x-100M/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_HDPE_200MeV_fourDet_06cm_only1x-050M/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_PMMA_055MeV_fourDet_06cm_only1x-500M/'
    data_dir = '/Volumes/Nicholas/Polaris/data/monte_carlo/proton_pUCT_PEPT_PMMA_055MeV_fourDet_oneModule_05cm_only1x-500M/'

    # # allcoins = process_root_coincidences(data_dir, e_window)
    #
    # coincidence_path = data_dir + 'pept_coincidence_data'
    # print('Reading in coincidence data from', coincidence_path + '.', '\nThis may take several minutes...')
    # allcoins = pd.read_csv(coincidence_path, sep = ',', header = None)       # read in text file as a csv with tab separation
    # print('Done.')
    # allcoins.columns = ['t1', 'x1', 'y1', 'z1', 't2', 'x2', 'y2', 'z2']       # separate into columns and name them
    #
    # print('Total number of coincidences:', len(allcoins.index), '\n')
    #
    # print('\n', 'Plotting coincidence data')
    # print('-------------------------', '\n')
    #
    # p0_xPos = allcoins.iloc[:, 1]; p0_yPos = allcoins.iloc[:, 2]; p0_zPos = allcoins.iloc[:, 3]
    # p1_xPos = allcoins.iloc[:, 5]; p1_yPos = allcoins.iloc[:, 6]; p1_zPos = allcoins.iloc[:, 7]
    #
    # # figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
    # # plt.rcParams.update({'font.size': 30})
    #
    # # num = len(p0_xPos)
    #
    # num = 1000
    # if num > len(p0_xPos):
    #     num = len(p0_xPos)
    #
    # print('Plotting XY coincidences of first', num, 'coincidences...')
    # for i in range(0, num):
    #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
    #     plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_yPos.iloc[i], p1_yPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
    # # plt.title('XY Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
    # plt.xlabel('X-Position (mm)'); plt.ylabel('Y-Position (mm)')
    # # plt.savefig(data_dir + 'basic_plots/xy-coincidences_N-' + str(num) + '.png')
    # plt.show()
    # plt.cla(); plt.clf()
    #
    # # figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
    # print('Plotting XZ coincidences of first', num, 'coincidences...')
    # for i in range(0, num):
    #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
    #     plt.plot([p0_xPos.iloc[i], p1_xPos.iloc[i]], [p0_zPos.iloc[i], p1_zPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
    # # plt.title('XZ Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
    # plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')
    # # plt.savefig(data_dir + 'basic_plots/xz-coincidences_N-' + str(num) + '.png')
    # plt.show()
    # plt.cla(); plt.clf()
    #
    # # figure(num=None, figsize=(21, 16), dpi=300, facecolor='w', edgecolor='k')      # Set plot
    # print('Plotting YZ coincidences of first', num, 'coincidences...')
    # for i in range(0, num):
    #     # print(p0_xPos[i]); print(p1_xPos[i]); print(p0_yPos[i]); print(p1_yPos[i])
    #     plt.plot([p0_yPos.iloc[i], p1_yPos.iloc[i]], [p0_zPos.iloc[i], p1_zPos.iloc[i]], color='green', marker='o', markersize=0.5, linestyle='-', linewidth=0.3)
    # # plt.title('YZ Position of %i coincidences locations with lines / Translated Coords, total: %s' % (num, len(p0_xPos)), fontsize=30)
    # plt.xlabel('Y-Position (mm)'); plt.ylabel('Z-Position (mm)')
    # # plt.savefig(data_dir + 'basic_plots/yz-coincidences_N-' + str(num) + '.png')
    # plt.show()
    # plt.cla(); plt.clf()

    # plot_energy_spectrum(data_dir, write_dir, allevents, e_range = (50, 1000),
    #                      fit_peaks = True, e_fits = [[511, 400, 600]],
    #                      shade_sca_window = False, sca_windows = [(461, 561, 'red')]
    #                      )

    find_beamline(data_dir, 85, 20, num_slices = 10, plot_slices = False,
                  lin_slicing = True,# lin_params = [-5, 15],
                  rot_slicing = True, rot_params = [-15, 15, 0],# rot_params = [-120, 80, 0],
                  lor_limit = -1, avg_points = False)

    return


    # ---------------------------------
    # Plot the false-true doubles curve
    # ---------------------------------
    # root_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    # write_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/false_doubles/'
    #
    # if not os.path.isdir(root_dir):
    #     print (root_dir, 'does not exist.')
    #     return
    #
    # data_dirs = []
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #         # print(dir)
    #         if '191203' not in dir:
    #             continue
    #         data_dirs.append(root_dir + dir + '/')
    #         # data_dirs.append(root_dir + dir + '/rate_files/')
    #     break
    #
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    # # initial_activities = [4715.66, 3806.95, 3104.84, 2506.53, 2376.51, 1938.21, 1596.94, 1302.42, 1051.44, 857.53, 692.28, 564.60, 460.47, 375.55, 303.18, 247.27, 201.66]
    #
    # # data_dirs = data_dirs[:2]
    # data_dirs = data_dirs[:13]          # runs 3-15
    # # data_dirs = data_dirs[13:]          # runs 16-19
    #
    # # initial_activities = [2376.51, 1938.21]                                                                                             # runs 3-4
    # initial_activities = [2376.51, 1938.21, 1596.94, 1302.42, 1051.44, 857.53, 692.28, 564.60, 460.47, 375.55, 303.18, 247.27, 201.66]  # runs 3-15
    # # initial_activities = [4715.66, 3806.95, 3104.84, 2506.53]                                                                           # runs 16-19
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run14-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/', '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/false_doubles/'
    # # initial_activities = [247.27, 201.66]
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/rate_files/'
    # # initial_activities = [1302.42]
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/rate_files/'
    # # initial_activities = [201.66]
    #
    # false_doubles(data_dirs, write_dir, initial_activities, read_txt = False)
    #
    # return


    # -----------------------------------------
    # Plot the sensitivity curve for the y-axis
    # -----------------------------------------

    # root_dirs = ['/Volumes/Nicholas/Polaris/data/190213/',
    #              '/Volumes/Nicholas/Polaris/data/190218/']
    #
    # data_dirs = []
    # write_dir = '/Volumes/Nicholas/Polaris/data/190218/'
    #
    # for root_dir in root_dirs:
    #     for subdir, dirs, files in os.walk(root_dir):
    #         for dir in dirs:
    #
    #             if dir[0] != 'J':
    #                 continue
    #
    #             data_dirs.append(root_dir + dir + '/5000ns_nofilter/')
    #
    #         break
    #
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'at_\d+_(-?\d+)_\d+mm', x).group(1)))       # sort the measured data by the run number
    #
    # # for d in data_dirs:
    # #     print(d)
    #
    # coincidence_sensitivity(data_dirs, write_dir, 0.5, 500, read_txt = True)
    #
    # return


    # -----------------------------------------------
    # Extract information about accepted coincidences
    # -----------------------------------------------
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # coin_window = 6000
    # det_delays = [-2000000060]
    #
    # extract_coincidence_information(data_dir, transformations, coin_window, det_delays)
    #
    # return


    # -------------------------------
    # Build up the coincidence models
    # -------------------------------
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/'
    # write_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/'
    #
    # singfiles = [['run3_15-unfiltered_singles-2s-mod0.txt', 'run16_19-unfiltered_singles-2s-mod0.txt'], ['run3_15-unfiltered_singles-2s-mod1.txt', 'run16_19-unfiltered_singles-2s-mod1.txt']]
    # prompt_coinfiles = ['run3_15-unfiltered_prompt_coincidences-2s.txt', 'run16_19-unfiltered_prompt_coincidences-2s.txt']
    # rand_coinfiles = ['run3_15-unfiltered_random_coincidences-2s.txt', 'run16_19-unfiltered_random_coincidences-2s.txt']
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/5us/0.5s_delay/'
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/5us/0.5s_delay/'
    # #
    # # singfiles = [['run3_15-unfiltered_singles-2s-mod0.txt'], ['run3_15-unfiltered_singles-2s-mod1.txt']]
    # # prompt_coinfiles = ['run3_15-unfiltered_prompt_coincidences-2s.txt']
    # # rand_coinfiles = ['run3_15-unfiltered_random_coincidences-2s.txt']
    #
    # build_coincidence_models(data_dir, write_dir, singfiles, prompt_coinfiles, rand_coinfiles, branch_ratio = 1.7, coin_window = 10000, all_coin_models = False)
    #
    # return


    # ---------------------------------------
    # Coincidence parameters comparison plots
    # ---------------------------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/5min/fixed_uncert/0.7-0.8mm/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # #
    # # e_windows = [[501, 522, 330, 350], [461, 561, 310, 370]]        # fwtm, full peak
    # # e_windows = [[504, 519, 330, 350], [461, 561, 310, 370]]    # by eye
    # # e_windows = [[506, 518, 335, 346], [501, 522, 330, 350]]    # fit energy windows
    # # e_windows = [[505, 518, 335, 348], [499, 523, 329, 353], [461, 561, 310, 370]]          # 2fwhm, 2fwtm, full peak
    # # e_windows = [[508, 514, 338, 344], [505, 517, 335, 347], [461, 561, 310, 370]]          # fwhm, fwtm, full peak
    # # e_windows = [[508, 514, 338, 344], [461, 561, 310, 370]]          # fwhm, full peak
    # #
    # # coincidence_parameters_comparisons(data_dir, e_windows, label_pos = 'left')
    # #
    # # return
    #
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/parameter_comparisons/0.7-0.8/15keV/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/parameter_comparisons/0.7-0.8mm/'
    #
    # e_windows = [[461, 561], []]          # fwhm, full peak
    #
    # coincidence_parameters_comparisons2(data_dir, e_windows, label_pos = 'left')
    #
    # return


    # ----------------------
    # Plot a energy spectrum
    # ----------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191112/P_191112-run5-na22_1uCi_centre-2min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run4-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/'
    #
    # # sca_windows = [(461, 561, 'lightblue'), (501, 522, 'green'), (506, 518, 'red'),
    # #                (310, 370, 'lightblue'), (330, 350, 'green'), (335, 346, 'red')]
    #
    # sca_windows = [(461, 561, 'lightblue'), (505, 517, 'green'), (508, 514, 'red'),
    #                (310, 370, 'lightblue'), (335, 347, 'green'), (338, 344, 'red')]
    #
    # plot_energy_spectrum(data_dir, e_range = (50, 1750), fit_peaks = True, e_fits = [[511, 400, 600], [1274, 1200, 1350]], shade_sca_window = True, sca_windows = [(503, 519, 'red')])
    # # plot_energy_spectrum(data_dir, e_range = (50, 600), fit_peaks = False, e_fits = [[511, 400, 600]], shade_sca_window = False, sca_windows = sca_windows)
    # # plot_energy_spectrum(data_dir, e_range = (250, 600), fit_peaks = False, e_fits = [[511, 400, 600]], shade_sca_window = True, sca_windows = sca_windows)
    # # plot_energy_spectrum(data_dir, apply_energy_filter = False, e_window = [], e_range = (0, 1000), fit_peaks = False, e_fits = [], shade_sca_window = False, sca_windows = [])
    #
    # return


    # ---------------------------------------------------------------------------------------------
    # Plot the time difference between successive events in a dataset to look for Poisson behaviour
    # ---------------------------------------------------------------------------------------------

    # # UCT Detector Array
    # # ------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run10-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run16-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/180808/J_180808-run1-na22-oem2_x_-68mm-oem3_x_68mm-1200s/08082018_180808-run1-na22-oem2_x_-68mm-oem3_x_68mm-1200s02_090136/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/180808/J_180808-run1-na22-oem2_x_-68mm-oem3_x_68mm-1200s/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/180206/J_180206-cs137-orthogonal-vertical-OEM2_95_5mm-OEM3-71_5mm-3600s/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/180123/J_180123-co60-point-oem2-05cm-vertical-3600s/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190530/190530-run3-na22_at_0_-8_11mm-oem2_y_48mm-oem3_y_-48mm-300s/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190912/190912-run6-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190912/190912-run1-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190912/190912-run9-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run5-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    #
    #
    # # H3D Detector Array
    # # ------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191112/ignore/P_191112-run6-na22_91uCi_centre-2min/'
    #
    # det_delay = -1999983891
    # # det_delay = -2000000060
    # # det_delay = 0
    #
    # time_between_events(data_dir, det_delay = det_delay,
    #                     module_file = False, module = 0,
    #                     apply_energy_filter = False, e_window = e_window,
    #                     bin_width = 'auto', cutoff_diff = -1)
    #
    # return


    # ---------------------------------------------------
    # Plot illustration of pixels on location uncertainty
    # ---------------------------------------------------
    # pixel_resolution_illustration('/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/Write Ups/Thesis/PolarisPEPT Thesis/Figures/')
    #
    # return


    # -------------------------
    # Find a particular N value
    # -------------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191112/P_191112-run5-na22_1uCi_centre-2min/'
    #
    # N, fopt, uncert = find_N_for_sigma(data_dir, (10, 400), (0.8, 1.0))
    #
    # print('Final uncertainty =', uncert, 'mm, with N =', N, 'and Fopt = ', fopt)
    #
    # return


    # ----------------------------------
    # Finding the beamline using 2D PEPT
    # ----------------------------------
    # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run35-na22_33uCi-line-5min/'
    # fopt = 30
    # N = 50
    #
    # find_beamline(data_dir, fopt, N)
    #
    # return


    # -------------------------------------
    # Plotting fit results of moving tracks
    # -------------------------------------
    # '''
    # Plot options:
    # 0: Adjusted speed versus average amplitude
    # 1: Adjusted speed versus average fitted speed
    # 2: Adjusted speed versus residuals of average amplitude - adjusted radius
    # 3: Adjusted speed versus residuals of adjusted speed - fitted speed
    # 4: Known radii versus average amplitude
    # 5: Known radii versus residuals of known radii - average fitted amplitude as a percentage
    # '''
    #
    # root_dir = '/Volumes/Nicholas/Polaris/data/191115/'
    # data_dirs = []
    # run_option = 1      # 0 = fixed radius, 1 = fixed speed
    # plot_option = 0
    # Ns = []
    # fopt = 41
    #
    # # Running analysis on all experiments with a fixed radius
    # if run_option == 0:
    #     radius = 1
    #     Ns = [200, 200, 200, 200, 200]                                      # 191115
    #
    #     for subdir, dirs, files in os.walk(root_dir):
    #         for dir in dirs:
    #             # Only include directories with a specific radius
    #             if ('circle' not in dir) or (float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)) != radius):
    #                 continue
    #             data_dirs.append(root_dir + dir + '/')
    #         break
    #     data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    #
    #     # tracking_measured_vs_known(data_dirs, Ns, fopt, plot_option = plot_option, select_points = ())
    #     tracking_measured_vs_known2(data_dirs, Ns, fopt, fixed = 'radius', plot_option = plot_option, select_points = ())
    #
    # # Running analysis on all experiments with a particular speed
    # if run_option == 1:
    #
    #     speed = 1
    #
    #     data_dirs = []
    #     for subdir, dirs, files in os.walk(root_dir):
    #         for dir in dirs:
    #             # Only include directories with a specific speed
    #             if ('circle' not in dir) or (float(re.search(r'_(\d+|\d+\.\d+)mms', dir).group(1)) != speed):
    #                 continue
    #
    #             data_dirs.append(root_dir + dir + '/')
    #         break
    #     data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    #
    #     Ns = [200, 200, 200, 200, 200, 200]
    #
    #     # tracking_measured_vs_known(data_dirs, Ns, fopt, plot_option = plot_option)
    #     tracking_measured_vs_known2(data_dirs, Ns, fopt, fixed = 'speed', plot_option = plot_option, select_points = ())
    #
    # return


    # --------------------------------------
    # Comparing fit results of moving tracks
    # --------------------------------------
    root_dir = '/Volumes/Nicholas/Polaris/data/191115/'
    data_dirs = []
    plot_option = 0
    Ns = []
    speeds = []
    radii = []
    fopt = -1

    fixed = 'speed'

    if '191114' in root_dir:
        speed_labels = {'0.5':'0.25 mm/s', '1':'0.5 mm/s', '2':'1.0 mm/s', '3':'1.5 mm/s', '5':'2.5 mm/s', '10':'5.0 mm/s', '12':'6.0 mm/s', '15':'7.5 mm/s', '20':'10.0 mm/s', '30':'15.0 mm/s'}
        speed_colors = {'0.5':'red', '1':'blue', '2':'green', '3':'orange', '5':'purple', '10':'yellow', '12':'cyan', '15':'lightcoral', '20':'lightblue', '30':'lightgreen'}

        radius_labels = {'2':'2.0 mm', '4':'1.0 mm', '5':'2.0 mm', '10':'8.0 mm'}
        radius_colors = {'2':'orange', '4':'red', '5':'blue', '10':'green'}

        known_radii_dict = {'2':1, '4':1, '5':2, '10':8}

        # Ns = np.ones(10, dtype = int)*350
        Ns = np.ones(10, dtype = int)*1000

        fopt = 8

        speeds = [0.5, 1, 2, 5, 10, 12, 15, 20, 30]
        radii = [4, 5, 10]

    if '191115' in root_dir:
        speed_labels = {'0.5':'0.25 mm/s', '1':'0.5 mm/s', '2':'1.0 mm/s', '5':'2.5 mm/s', '10':'5.0 mm/s'}
        speed_colors = {'0.5':'red', '1':'blue', '2':'green', '5':'orange', '10':'purple'}

        radius_labels = {'1':'0.5 mm', '1.5':'3.5 mm', '2':'1.0 mm', '5':'4.0 mm', '10':'8.0 mm', '20':'19.5 mm'}
        radius_colors = {'1':'red', '1.5':'blue', '2':'green', '5':'orange', '10':'purple', '20':'black'}

        known_radii_dict = {'1':0.5, '1.5':3.5, '2':1, '5':4, '10':8, '20':19.5}
        fitted_radii_dict = {'1':0.25, '1.5':3.3, '2':1.3, '5':4.2, '10':8.1, '20':19.75}

        Ns = [200, 200, 200, 200, 200, 200]

        fopt = 41

        speeds = [0.5, 1, 2, 5, 10]
        radii = [1, 1.5, 2, 5, 10, 20]

    if fixed == 'radius':

        all_data_dirs = []
        for speed in speeds:
            data_dirs = []
            for subdir, dirs, files in os.walk(root_dir):
                for dir in dirs:
                    # Only include directories with a specific speed
                    if ('circle' not in dir) or (float(re.search(r'_(\d+|\d+\.\d+)mms', dir).group(1)) != speed) or (float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)) == 2):
                        continue

                    data_dirs.append(root_dir + dir + '/')
                break
            data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number

            all_data_dirs.append(data_dirs)

        # for d in all_data_dirs:
        #     print(d)

        tracking_measured_vs_known_comparisons(all_data_dirs, root_dir, known_radii_dict,
                                               radius_labels, radius_colors,
                                               Ns, fopt, fixed,
                                               plot_option = plot_option, select_points = ())

    if fixed == 'speed':

        all_data_dirs = []
        for radius in radii:
            data_dirs = []
            for subdir, dirs, files in os.walk(root_dir):
                for dir in dirs:
                    # Only include directories with a specific radius
                    if ('circle' not in dir) or (float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)) != radius):
                        continue

                    data_dirs.append(root_dir + dir + '/')
                break
            data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number

            all_data_dirs.append(data_dirs)


        tracking_measured_vs_known_comparisons(all_data_dirs, root_dir, known_radii_dict,
                                               speed_labels, speed_colors,
                                               Ns, fopt, fixed,
                                               plot_option = plot_option, select_points = ())


    return


    # ---------------------
    # Analysing pept tracks
    # ---------------------
    # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run20-na22_33uCi-circle_5mm_20mms/'
    # N = 200
    # f = 30
    #
    # track_snapshops(data_dir, f, N)
    #
    # return


    # ----------------------------------
    # Plotting fit results of cc imaging
    # ----------------------------------

    # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/cc_imaging/core_output/'
    #
    # plot_fit_vs_dca_percentage(data_dir)
    #
    # return


    # -------------------------------
    # Determining detector efficiency
    # -------------------------------

    # # data_dir = '/Volumes/Nicholas/Polaris/data/190312/'
    # # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    # # f = 85
    # # N = 500
    # #
    # # detector_efficiency(data_dir, transformations, f, N, read_txt = True, e_window = e_window)
    #
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # A_0 = 201660
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run14-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # A_0 = (6.68e-6 * 3.7e10)/1000
    #
    #
    # # solid_angle = 0.0815
    # solid_angle = 0.1037
    # coincidence_model(data_dir, A_0, e_window, coin_window, transformations, det_delays = [-1999983890], solid_angle = solid_angle, branch_ratio = 1.8)
    #
    # return


    # ----------------------------
    # Plotting f-opt demonstration
    # ----------------------------

    # # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/coincidence_data_no-doubles/'
    # f = 85
    # N = 200
    #
    # f_opt_demonstration(data_dir, f, N, plot_lines_only = True, plot_highlighted = True, plot_accepted_only = True)
    #
    # return


    # ------------------------------
    # Determining Deadtime Paramters
    # ------------------------------
    # rate_files = ['/Volumes/Nicholas/Polaris/data/191203/rate_files/run3_15-unfiltered_singles-2s-mod0.txt']#,
    #               #'/Volumes/Nicholas/Polaris/data/191203/rate_files/run3_15-unfiltered_singles-2s-mod1.txt']
    # write_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/Write Ups/Thesis/PolarisPEPT Thesis/Figures/'
    # plot_labels = ['Module 0', 'Module 1']
    # rate_range = []
    # rate_range = [6000, 25000]
    #
    # fit_colors = ['red', 'blue']
    # data_colors = ['lightcoral', 'lightblue']
    #
    # determine_deadtime_parameter(rate_files, write_dir, model = 0, branch_ratio = 1.7, plot_labels = plot_labels, data_colors = data_colors, fit_colors = fit_colors, rate_range = rate_range)
    #
    # return


    # ---------------------------------------------------
    # Plotting coincidence rate versus coincidence window
    # ---------------------------------------------------

    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191125/P_191125-run1-na22_1uCi-centre-5min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191125/tmp/'
    #
    # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/ignore/tmp/'
    #
    # # windows = np.linspace(1e5, 1e8, 20)
    # windows = [1e1, 1e2, 1e3, 1e4, 1e5, 1e6]
    #
    # coincidence_rate_vs_window(data_dir, windows, transformations, e_window)
    #
    # return


    # ------------------------------------------------
    # Module event rate files generating and plotting
    # -----------------------------------------------
    #
    # root_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    # write_dir = root_dir + 'rate_files/10us/64us_delay/interaction/'
    #
    # if not os.path.isdir(root_dir):
    #     print (root_dir, 'does not exist.')
    #     return
    #
    # data_dirs = []
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #         # print(dir)
    #         if '191203' not in dir:
    #             continue
    #         data_dirs.append(root_dir + dir + '/')
    #         # data_dirs.append(root_dir + dir + '/rate_files/')
    #     break
    #
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    # # data_dirs = data_dirs[:13]          # runs 3-15
    # data_dirs = data_dirs[13:]          # runs 16-19
    #
    # # initial_activities = [64.23, 52.38, 43.16, 35.20, 28.42, 23.18, 18.71, 15.26, 12.45, 10.15, 8.19, 6.68, 5.45, 127.45, 102.89, 83.91, 67.74]
    # # initial_activities = [2376.51, 1938.21, 1596.94, 1302.42, 1051.44, 857.53, 692.28, 564.60, 460.47, 375.55, 303.18, 247.27, 201.66, 4715.66, 3806.95, 3104.84, 2506.53]
    #
    # # initial_activities = [2376.51, 1938.21, 1596.94, 1302.42, 1051.44, 857.53, 692.28, 564.60, 460.47, 375.55, 303.18, 247.27, 201.66]  # runs 3-15
    # initial_activities = [4715.66, 3806.95, 3104.84, 2506.53]                                                                           # runs 16-19
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run14-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/', '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run14-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/rate_files/'
    # # initial_activities = [247.27, 201.66]
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/rate_files/'
    # # initial_activities = [1302.42]
    #
    # # data_dirs = ['/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # # write_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/rate_files/'
    # # initial_activities = [201.66]
    #
    # modules = [0,1]
    # t_half = 4080
    # t_int = 2
    # coin_window = 10000
    # e_window = []
    # # mod_delays = [-2000000060]
    # mod_delays = [-1999983890]
    #
    # calculate_rates(data_dirs, write_dir, e_window, initial_activities, t_half, t_int,
    #                 singles_or_coincidences = 1,        # 0 = singles, 1 = coincidences
    #                 count_all_interactions = True,
    #                 modules = modules,
    #                 coin_window = coin_window,
    #                 count_randoms = True,
    #                 mod_delays = mod_delays,
    #                 count_deadtime_peak = False,
    #                 show_plots = True)
    #
    # return
    #
    # generate_rates(data_dirs, 'mod51.dat', 0, e_window, 120, False)
    #
    # data_dirs = ['/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run16-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # data_dirs = ['/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run17-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/']
    # generate_rates(data_dirs, 'mod51.dat', 0, e_window, 120)
    #
    # return

    # ----------------

    # data_dirs = ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run1-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run2-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run3-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run4-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run5-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run6-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run7-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run8-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run9-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/rate_files/',
    #              '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190611/190611-run7-ga68_vial-oem2_y_48mm-oem3_y_-48mm-90min/06112019_190611-run7-ga68_vial-oem2_y_48mm-oem3_y_-48mm-90min_D0_082338/rate_files/']
    # initial_activities = [22.3, 18.2, 14.8, 12.1, 9.9, 8.0, 101.0, 82.4, 67.2, 55.0]

    # # 191203
    # # ------
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    # write_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    # initial_activities = [64.23, 52.38, 43.16, 35.20, 28.42, 23.18, 18.71, 15.26, 12.45, 10.15, 8.19, 6.68, 5.45, 127.45, 102.89, 83.91, 67.74]
    #
    # # filenames = ['singles-60s-51.txt', 'doubles-60s-51.txt', 'triplesplus-60s-51.txt', 'singles_filtered-60s-51.txt']
    # # filenames = ['singles-120s-51.txt', 'doubles-120s-51.txt', 'triplesplus-120s-51.txt', 'singles_filtered-120s-51.txt']
    # filenames = ['singles-120s-51.txt', 'doubles-120s-51.txt', 'singles_filtered-120s-51.txt']
    # # filenames = ['singles-120s-51.txt']
    #
    # lbls = ['Singles Unfiltered', 'Doubles Unfiltered', 'Singles Filtered']
    # mkr_styles = ['.', '^', 's']
    # # lbls = ['Singles Unfiltered']
    #
    # plot_module_rates(data_dirs, filenames, write_dir, initial_activities, 68, lbls, mkr_styles)
    #
    # return


    # ---------------------------------------------------
    # Singles and coincidences model fitting and plotting
    # ---------------------------------------------------
    # rate_files = [
    #                 ['/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/run16_19-unfiltered_singles-2s-mod0.txt', '/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/run3_15-unfiltered_singles-2s-mod0.txt'],
    #                 ['/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/run16_19-unfiltered_singles-2s-mod1.txt', '/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/run3_15-unfiltered_singles-2s-mod1.txt']
    #               ]
    # write_dir = '/Volumes/Nicholas/Polaris/data/191203/rate_files/10us/64us_delay/singles/'
    # model = 1       # 0 = paralysable, 1 = non-paralysable
    # taus = [24e-6, 24e-6]
    # branch_ratio = 1.7
    # abs_effs = [0.0148, 0.0152]
    # fit_colors = ['red', 'blue']
    # data_colors = ['lightcoral', 'lightblue']
    # plot_labels = ['Module 0 Model Rates', 'Module 1 Model Rates']
    # data_plot_labels = ['Module 0 Measured Rates', 'Module 1 Measured Rates']
    #
    # deadtime_model_singles(rate_files, write_dir, model, taus, branch_ratio, abs_effs, plot_labels, data_plot_labels, data_colors, fit_colors)
    #
    # return

    # # 191203
    # # ------
    # root_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    #
    # if not os.path.isdir(root_dir):
    #     print (root_dir, 'does not exist.')
    #     return
    #
    # data_dirs = []
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #         # print(dir)
    #         if dir == 'ignore':
    #             continue
    #         # data_dirs.append(root_dir + dir + '/')
    #         data_dirs.append(root_dir + dir + '/rate_files/')
    #     break
    #
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    # write_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    # initial_activities = [64.23, 52.38, 43.16, 35.20, 28.42, 23.18, 18.71, 15.26, 12.45, 10.15, 8.19, 6.68, 5.45, 127.45, 102.89, 83.91, 67.74]
    #
    # rate_filename = 'singles-120s-51.txt'
    # # rate_filename = 'singles_filtered-120s-51.txt'
    #
    # deadtime_model_singles(data_dirs, rate_filename, write_dir, initial_activities, 68)
    # # deadtime_model_coincidences(data_dirs, rate_filename, write_dir, initial_activities, 68)
    #
    # return


    # --------------------------------
    # Comparing coincidence processing
    # --------------------------------
    # data_dir = '/Volumes/Data/polaris_pept/190912/190912-run5-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/'
    #
    # compare_coin_processing(data_dir, 'AllEventsCombined.txt', e_window, coin_window, transformations)
    #
    # return


    # -------------------------------------------------
    # Coincidence rate versus coincidence timing window
    # -------------------------------------------------

    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/'
    # coin_windows = np.linspace(0, 50000, 50)
    #
    # coincidence_rates_vs_window(data_dir, e_window, coin_windows, transformations, det_delay = -1999983890, plot_randoms = True)
    # # coincidence_rates_vs_window(data_dir, e_window, coin_windows, transformations, det_delay = 80, plot_randoms = True)
    #
    # return


    # --------------------------------------------------------
    # Run basic plots on all measurements in a given directory
    # --------------------------------------------------------

    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/'
    # for subdir, dirs, files in os.walk(data_dir):
    #     for dir in dirs:
    #         # Ignore irrelevant directories
    #         if dir[-1] != 'n' or int(dir[10]) > 6:
    #             continue
    #         print('Running plots on ', data_dir + dir)
    #         plot_basic(data_dir + dir + '/', transformations, 'pept_coincidence_data')
    #         print('\n', '===================================================================================================', '\n')
    #
    # return


    # -------------------------------------
    # Calculate the coincidence rate curves
    # -------------------------------------
    # time_window = 120
    # a0 = 1
    # t_half = 68
    # #
    # # # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/all_fuctioning_test/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # #
    # # # determine_coincidence_rates(data_dir, time_window, transformations, e_window, coin_window, a0, t_half, True, -1999983892)
    # #
    # # count_coincidence_rates(data_dir, time_window, transformations, e_window, coin_window, a0, t_half, plot_results = True, add_delay = -1999983890, count_doubles = True)
    # #
    # # return
    #
    # # root_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/'
    # root_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    #
    # data_dirs = []
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #         if dir == 'ignore':
    #             continue
    #         data_dirs.append(root_dir + dir + '/')
    #     break
    #
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    # initial_activities = [64.23, 52.38, 43.16, 35.20, 28.42, 23.18, 18.71, 15.26, 12.45, 10.15, 8.19, 6.68, 5.45, 127.45, 102.89, 83.91, 67.74]
    #
    # for i, data_dir in enumerate(data_dirs):
    #
    #     count_coincidence_rates(data_dir, time_window, transformations, e_window, coin_window, initial_activities[i], t_half,
    #                                 plot_results = False, add_delay = -1999983890, count_doubles = True)
    #
    #     print('===================================================================================================================================\n')
    #
    # return

    # # Data SD card
    # data_dirs = [# '/Volumes/Data/polaris_pept/190912/190912-run7-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              # '/Volumes/Data/polaris_pept/190912/190912-run8-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              # '/Volumes/Data/polaris_pept/190912/190912-run9-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190611/190611-run7-ga68_vial-oem2_y_48mm-oem3_y_-48mm-90min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run1-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run2-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run3-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run4-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run5-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/',
    #              '/Volumes/Data/polaris_pept/190912/190912-run6-ga68_at_0_0_11mm-oem2_x_-48mm-oem3_x_48mm-20min/']
    #
    # start_activities = [20.14, 16.42, 13.40, 10.93, 8.91, 7.27]

    # for i, data_dir in enumerate(data_dirs):
    #
    #     plot_coincidence_curves(data_dir, time_window, transformations, e_window, coin_window, start_activities[i], t_half)
    #
    # return

    # save_dir = '/Volumes/Data/polaris_pept/190912/'
    #
    # time_brackets = [[0, 20], [21, 41], [42, 62], [63, 83], [84, 104], [105, 125], [126, 150], [150, 170], [170, 190], [190, 210]]
    #
    # root_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    #
    # data_dirs = []
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #         if dir == 'ignore':
    #             continue
    #         data_dirs.append(root_dir + dir + '/')
    #     break
    # data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number
    #
    # # Options: totals, singles, activity
    # plot_coincidence_curves2(data_dirs, root_dir, 'singles')
    #
    # return


    # --------------------------
    # Timing resolution plotting
    # --------------------------
    # time_delays = np.arange(-10, 10, 0.5)
    # coin_window = 4
    #
    # data_file = '/Volumes/Data/polaris_pept/190612/190612-run1-ga68_needle-oem2_y_48mm-oem3_y_-48mm-fixed_centre/AllEventsCombined.txt'
    #
    # timing_resolution(data_file, transformations, e_window, time_delays, coin_window)
    #
    # root_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    # readfile = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run2-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/plot_data/timing_resolution-range_-700_700_50-window_400.txt'
    #
    # num_modules = 2
    #
    # for subdir, dirs, files in os.walk(root_dir):
    #     for dir in dirs:
    #
    #         print('Running timing resolution plot on', dir, '...\n')
    #
    #         if dir[0] != 'J':
    #             continue
    #
    #         # timing_resolution2(root_dir + dir + '/', transformations, e_window, t_range = [-700, 700, 50], window = 400, perform_fit = True)
    #         timing_resolution2(root_dir + dir + '/', transformations, e_window, read_txt = 1, txt_path = readfile, perform_fit = True)
    #
    #         print('-------------------------------------------')
    #
    #     break
    #
    # return

    # # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/5min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/J_190226-run2-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/10min/'
    #
    # resolution_delays = np.linspace(-700, 700, 50)
    #
    # timing_resolution2(data_dir, transformations, e_window,
    #                    delays = resolution_delays,
    #                    window = 300,
    #                    read_txt = True,
    #                    perform_fit = True,
    #                    num_detectors = 2,
    #                    shade_window = [-229, 371, 'r']
    #                    )
    #
    # # timing_resolution2(data_dir, transformations, e_window, t_range = [-100000, 100000, 5e3], window = 1e4, perform_fit = False)
    # # timing_resolution2(data_dir, transformations, e_window, t_range = [-700, 700, 50], window = 400, read_txt = False, perform_fit = True, num_detectors = 2)
    # # timing_resolution2(data_dir, transformations, e_window, read_txt = 1, txt_path = readfile, perform_fit = True)
    # # timing_resolution2(data_dir, transformations, e_window, t_range = [1999900000, 2000100000, 5e3], window = 1e4, perform_fit = False)
    # # timing_resolution2(data_dir, transformations, e_window, t_range = [-2000100000, -1999900000, 5e3], window = 1e4, perform_fit = False)
    # # timing_resolution2(data_dir, transformations, e_window, t_range = [-1999986000, -1999981000, 100], window = 300, perform_fit = True)
    #
    # return


    # -----------------------
    # Deadtime curve plotting
    # -----------------------

    # data_dirs = ['/Volumes/Nicholas/191108/', '/Volumes/Nicholas/191111/']
    # txt_file = '/Volumes/Nicholas/191111/distance_versus_rate.txt'
    #
    # plot_deadtime_h3d(data_dirs, True, txt_file)
    #
    # return


    # ---------------------------------------------
    # Plotting the measured vs known position curve
    # ---------------------------------------------
    # # data_dirs = ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/']
    # #
    # # measured_vs_known(data_dirs, 80, 100, 3)
    # #
    # # return
    #
    # output_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/200327/'
    #
    # measured_vs_known2(output_dir, ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190312/'],
    #                    ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190213/', '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190218/'],
    #                    ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/'],
    #                    write_data = True, read_txt = True)
    #
    # return


    # --------------------------------
    # Activity verses coincidence rate
    # --------------------------------
    # rate_datasets = ['/Volumes/Data/polaris_pept/190228/J_190228-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/processPolarisData003_output.txt',
    #                  '/Volumes/Data/polaris_pept/190228/J_190228-run2-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/processPolarisData003_output.txt',
    #                  '/Volumes/Data/polaris_pept/190211/J_190211-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/processPolarisData003_output.txt']
    #
    # plot_rates(rate_datasets, [1.147285166, 3.199643675, 28.74754214], 89, 1000, False, True)
    #
    # # H3D dataset
    #
    # data_dirs = ['/Volumes/Nicholas/191108/', '/Volumes/Nicholas/191111/']
    #
    # plot_coincidence_h3d(data_dirs, e_window, coin_window, transformations)
    #
    # return


    # -----------------------
    # Averaged N optimisation
    # -----------------------
    # plot_N_vs_sigma_averaged('/Volumes/Data/polaris_pept/190218/', 85, 100, 10000, 500)


    # ----------------------------
    # Averaged fopt for multiple N
    # ----------------------------
    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    #
    # # Ns = [100, 250, 1000, 5000, 10000]
    # Ns = [10, 30, 60, 100, 150, 200]
    #
    # multi_fopt_averaged(data_dir, Ns)
    #
    # return


    # --------------
    # F optimization
    # --------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    #
    # f_optimisation(data_dir, 1, 10)
    #
    # return


    # -----------------------------
    # F optimization for multiple N
    # -----------------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run19-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run5-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/5min/'
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191114/P_191114-run3-na22_33uCi-centre-30min/5min/'
    #
    #
    # # Ns = [80, 100, 200, 250, 300]
    # # Ns = [25, 50, 100, 150, 200]
    # Ns = [40, 100, 200, 500, 1000]
    # # Ns = [250, 350, 500, 1000, 2000]
    #
    # multi_fopt(data_dir, Ns, fopt_params = [6, 30, 2],
    #            x_lim = [0.0, 0.3],# y_lim = [0, 1300],
    #            write_data = True, read_txt = False)
    #
    # return


    # ----------------------------------------------------
    # Plotting multiple Fopt curves for different N values
    # ----------------------------------------------------
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/coincidence_data/5000ns_nofilter/'
    # # Ns = [250, 375, 500, 1000, 1500]
    # # y_lim = [0, 2200]
    #
    # # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run6-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/coincidence_data/5000ns_efilter/'
    # # Ns = [150, 250, 500, 1000, 1500]
    # # y_lim = [0, 1500]
    #
    # # write_dir = data_dir
    # # Frange = [2, 30, 2]
    # # Ns_pois = [150, 300, 450, 600, 750, 900, 1050, 1300, 1500]
    # # Nfopt = 10
    # # x_lim = [0, 0.3]
    #
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min/'
    # write_dir = data_dir
    # Ns = [15,30,60,100,200]
    # Frange = [30, 105, 5]
    # Ns_pois = np.linspace(10, 200, 10)
    # Nfopt = 95
    # x_lim = [0.25, 1.05]
    # y_lim = [0, 1500]
    #
    # plot_multi_fopt_NvS(data_dir, write_dir,
    #                     Ns, Frange, Ns_pois, Nfopt,
    #                     write_data = True, read_txt = False,
    #                     fit_poly = True,
    #                     plot_N_curve = True,
    #                     x_lim = x_lim, y_lim = y_lim
    #                     )
    #
    # return


    # -----------------------
    # Averaged F optimisation
    # -----------------------

    # data_dir = '/Volumes/Nicholas/Polaris/data/190226/'
    #
    # best_opt, best_pos, best_ucert = f_optimisation_averaged(data_dir, 50, fopt_step = 5)
    #
    # print('Fopt =', best_opt)
    # print('Final position =', best_pos)
    # print('Final uncertainty =', best_ucert)
    # print('Final average uncertainty =', np.sqrt(best_ucert[0]**2 + best_ucert[1]**2 + best_ucert[2]**2))
    #
    # return


    # -------------------------------------------
    # Multiple fopt curves + inset N v sigma plot
    # -------------------------------------------

    # plot_multi_fopt_NvS_avg('/Volumes/Nicholas/Polaris/data/190226/', '/Volumes/Nicholas/Polaris/data/190226/',
    #                         Ns = [10, 30, 60, 100, 150, 200], Frange = [30, 100, 5],
    #                         Ns_pois = [5, 10, 30, 60, 90, 120, 150, 180, 200], Nfopt = 85,
    #                         write_data = True, read_txt = True,
    #                         fit_poly = True, plot_N_curve = True, x_lim = [0.25, 1.0], y_lim = [0, 1500])
    #
    # return


    # --------------------------------------------------------
    # Multiple fopt curves + inset N v sigma plot for BGO data
    # --------------------------------------------------------
    # bgo_plot_multi_fopt_NvS_avg('/Volumes/Data/polaris_pept/bgo/f_vs_sigma.txt', '/Volumes/Data/polaris_pept/bgo/n_vs_sigma.txt')


    # -------------------------------------------
    # Run ctrack on given directory of data files
    # -------------------------------------------
    # ctrack_all('/Volumes/Data/polaris_pept/190226/', 89, 500)
    # ctrack_all('/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190312/', 83, 100)
    #
    # return

    # ---------------------------------
    # Run ctrack on a specific txt file
    # ---------------------------------
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191128/P_191128-run1-na22_1uCi-centre-1hr/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190612/190612-run14-ga68_needle-oem2_y_48mm-oem3_y_-48mm-fixed_centre/'
    # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run3-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/all_fuctioning_test/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/200210-run8-na22-at_0_0_8mm-10min/'
    #
    # run_ctrack(data_dir + 'pept_coincidence_data', 80, 100)
    #
    # return


    # --------------------------
    # Histogram of position data
    # --------------------------
    # # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run3-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run11-na22_1uCi-circle_5mm_10mms/'
    #
    # position_distr(data_dir,
    #                multiple_runs = False,
    #                plot_mvk = False,
    #                mvk_x_dirs = ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190312/'],
    #                mvk_y_dirs = ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190213/', '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190218/'],
    #                mvk_z_dirs = ['/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/']
    #                )
    #
    # return


    # -------------------------------------
    # Plot x,y,z positions on moving source
    # -------------------------------------

    # Read in polaris data and perform coincidence processing
    # =======================================================

    # Line Tracking:
    # -----------------------------------------------------------------------------------------------------------------------------------------------
    # timing = 8000           # coincidence timing window in ns
    # N = 200                 # number of lines per position for ctrack
    # fopt = 20               # signal to noise ratio for ctrack
    # fit_file = '/Volumes/Data/polaris_pept/gcodes/circle_8_93-82.5-40_75.gcode'
    # fit_params = [-93, -82.5, -30, 5]
    #
    # Ns = [100, 100, 200, 50, 50, 30]
    #
    # for i in range(1):
    #     data_dir = data_dirs[i]
    #     print('\nRunning tracking on', data_dir)
    #     print('=================================================================================================================================\n')
    #
    #     # # Fit functions for applying least squares fit
    #     # def x_func(t, A, omega, phi, offset):
    #     #     return np.abs(A)*np.cos(omega*t + phi) + offset
    #     #
    #     # def y_func(t, A, omega, phi, offset):
    #     #     return np.abs(A)*np.cos(omega*t + phi) + offset
    #     #
    #     # def z_func(t, A, omega, phi, offset):
    #     #     return np.abs(A)*np.sin(omega*t + phi) + offset
    #     #
    #     # p0_x = [10, 2*np.pi/periods[i], 0, 2]
    #     # p0_y = [10, 2*np.pi/periods[i], 0, -2]
    #     # p0_z = [10, 2*np.pi/periods[i], 0, 11]
    #     #
    #     # labels = ['A', 'omega', 'phi', 'Offset']
    #     #
    #     # fit_funcs = [x_func, y_func, z_func]
    #     # p0s = [p0_x, p0_y, p0_z]
    #     # p0_labs = [labels, labels, labels]
    #
    #     # allevents, coincidence_data = process_coincidences(data_dir,
    #     #                                                         [461, 561, 310, 370], timing,
    #     #                                                         transformations,
    #     #                                                         False)
    #     # best_opt, best_pos, best_ucert = f_optimisation(data_dir + 'pept_coincidence_data', 1, N)
    #     # plot_basic(data_dir,
    #     #            [d0_transformation, d1_transformation],
    #     #            'pept_coincidence_data')
    #     run_ctrack(data_dir + '/pept_coincidence_data', fopt, Ns[i])
    #     plot_moving(data_dir,
    #                 'pept_coincidence_data.a',
    #                 timing, Ns[i], fopt, errors,
    #                 False, fit_file, fit_params,
    #                 False)#, fit_funcs, p0s, p0_labs)
    # return


    # Circle Tracking:
    # -----------------------------------------------------------------------------------------------------------------------------------------------
    timing = 5000              # coincidence timing window in ns
    N = 350                    # number of lines per position for ctrack
    fopt = 8                  # signal to noise ratio for ctrack
    fit_file = '/Volumes/Data/polaris_pept/gcodes/circle_8_93-82.5-40_75.gcode'
    fit_params = [-93, -82.5, -30, 5]
    radius = 4

    # Multiple Fits
    # -------------
    root_dir = '/Volumes/Nicholas/Polaris/data/191114/'

    data_dirs = []
    for subdir, dirs, files in os.walk(root_dir):
        for dir in dirs:
            # if dir == 'ignore':
            #     continue

            # Only include directories with a specific radius
            if ('circle' not in dir) or (float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)) != radius):
                continue

            data_dirs.append(root_dir + dir + '/')
        break

    data_dirs = sorted(data_dirs, key = lambda x: int(re.search(r'run(\d+)', x).group(1)))       # sort the measured data by the run number

    # X, Y, Z Plots
    # -------------
    periods = []
    Ns = []
    time_cutoffs = []

    if '191112' in root_dir:
        fopt = 53

        # Radius = 10mm
        if radius == 10:
            periods = [125, 60, 40, 30, 25, 250, 12]
            Ns = [17, 17, 17, 17, 17, 17, 17]
            time_cutoffs = [(2, 300), (), (), (2, 75), (2, 60), (), ()]

        # Radius = 5mm
        if radius == 5:
            periods = [125, 60, 30, 22, 15, 11, 6]
            Ns = [17, 17, 17, 17, 17, 17, 17]
            time_cutoffs = [(), (), (), (2, 50), (2, 35), (2, 25), (2, 14)]

        # Radius = 4mm
        if radius == 4:
            periods = [125, 55, 30, 9, 5, 3, 3]
            Ns = [200, 200, 200, 200, 200, 200, 200]
            time_cutoffs = [(), (), (), (), (), (), ()]

        # Radius = 2mm
        if radius == 2:
            periods = [60, 25, 11, 7, 3]
            Ns = [200, 200, 200, 100, 100]
            time_cutoffs = [(), (), (), (), ()]

    if '191114' in root_dir:
        fopt = 8

        # Radius = 10mm
        if radius == 10:
            periods = [250, 125, 60, 40, 25, 11, 12, 7, 5, 3]
            Ns = np.ones(len(periods))*N
            time_cutoffs = [(2, 1000), (2, 500), (), (), (2, 120), (2, 55), (2, 50), (), (), (2,12)]
            ylim = 10

        # Radius = 5mm
        if radius == 5:
            periods = [125, 60, 30, 12, 6, 3, 3]
            Ns = np.ones(len(periods))*N
            time_cutoffs = [(), (10,300), (), (), (10, 25), (2,14), (2, 13)]
            ylim = 5

        # Radius = 4mm
        if radius == 4:
            periods = [125, 55, 30, 9, 5, 3, 1.5]
            Ns = np.ones(len(periods))*N
            time_cutoffs = [(10, 490), (), (2, 120), (5, 40), (2, 20), (2, 15), (4, 7)]
            ylim = 4

        # Radius = 2mm
        if radius == 2:
            periods = [60, 25, 11, 7, 1.1]
            Ns = np.ones(len(periods))*N
            time_cutoffs = [(10, 240), (), (), (5, 16), (2, 7)]
            ylim = 3

    if '191115' in root_dir:
        fopt = 41

        # Radius = 20mm
        if radius == 20:
            periods = [500,250,125,50,25]
            Ns = [200, 200, 200, 200, 200]
            time_cutoffs = [(), (), (), (), ()]

        # Radius = 10mm
        if radius == 10:
            periods = [110,55,22,12,1]
            # Ns = [38, 38, 38, 38]
            Ns = [200, 200, 200, 200]
            time_cutoffs = [(2, 600), (2, 300), (2, 120), (2, 59)]

        # Radius = 5mm
        if radius == 5:
            periods = [125, 57, 30, 11, 6]
            Ns = [200, 200, 200, 200, 200]
            time_cutoffs = [(), (), (), (), (3,30)]

        # Radius = 2mm
        if radius == 2:
            periods = [60, 25, 1.5]
            Ns = [200, 200, 200]
            time_cutoffs = [(), (), (1, 7)]

        # Radius = 1.5mm
        if radius == 1.5:
            periods = [34,15,9,3,1.5]
            Ns = [200, 200, 200, 200, 200]
            time_cutoffs = [(), (15, 85), (5, 45), (2, 14), (2, 7)]

        # Radius = 1mm
        if radius == 1:
            periods = [30,11,5.5,2]
            Ns = [200, 200, 200, 200]
            time_cutoffs = [(), (), (), ()]


    # for i in range(1,2):#len(data_dirs)):
    for i, data_dir in enumerate(data_dirs):
        period = periods[i]
        N = Ns[i]
        time_cutoff = time_cutoffs[i]
        # time_cutoff = []
        print('\nRunning on', data_dir)
        print('=================================================================================================================================\n')

        # Fit functions for applying least squares fit
        def x_func(t, A, omega, phi, offset):
            return np.abs(A)*np.cos(omega*t + phi) + offset

        def y_func(t, A, omega, phi, offset):
            return np.abs(A)*np.cos(omega*t + phi) + offset

        # def z_func(t, A, omega, phi, offset):
        #     return np.abs(A)*np.sin(omega*t + phi) + offset

        def z_func(t, offset):
            return 0*t + offset

        p0_x = [radius, 2*np.pi/period, 0, 2]
        p0_y = [radius, 2*np.pi/period, 0, 1]
        # p0_z = [10, 2*np.pi/period, 0, 11]
        p0_z = [-4]

        labels_xy = ['A', 'omega', 'phi', 'Offset']
        labels_z = ['Offset']

        fit_funcs = [x_func, y_func, z_func]
        p0s = [p0_x, p0_y, p0_z]
        p0_labs = [labels_xy, labels_xy, labels_z]

        # allevents, coincidence_data = process_coincidences(data_dir,
        #                                                         [461, 561, 310, 370], timing,
        #                                                         transformations,
        #                                                         False)
        # best_opt, best_pos, best_ucert = f_optimisation(data_dir + 'pept_coincidence_data', 1, N)
        # plot_basic(data_dir,
        #            [d0_transformation, d1_transformation],
        #            'pept_coincidence_data')
        run_ctrack(data_dir + 'pept_coincidence_data', fopt, N)
        plot_moving(data_dir,
                    'pept_coincidence_data.a',
                    timing, N, fopt, plot_errbars = False, err = [], time_cutoff = time_cutoff, y_limit = ylim,
                    apply_fit = True, fit_functions = fit_funcs, p0s = p0s, p0_labels = p0_labs)

    return

    # -------------------------------------------------------------------------------------------------

    # 2D X,Y plots

    timing = 5000               # coincidence timing window in ns
    N = 500                     # number of lines per position for ctrack
    fopt = 8                    # signal to noise ratio for ctrack

    # data_dirs = ['/Volumes/Nicholas/Polaris/data/191115/P_191115-run11-na22_1uCi-circle_5mm_10mms/']
    # write_dir = data_dirs[0]

    # rad_labels = ['5mm']
    # pos_colors = ['black']
    # vel_colors = ['black']

    root_dir = '/Volumes/Nicholas/Polaris/data/191114/'
    data_dirs = []
    speed = 1
    for subdir, dirs, files in os.walk(root_dir):
        for dir in dirs:
            # Only include directories with a specific radius
            if ('circle' not in dir) or (float(re.search(r'_(\d+|\d+\.\d+)mms', dir).group(1)) != speed) or (float(re.search(r'circle_(\d+|\d+\.\d+)mm', dir).group(1)) == 2):
                continue

            data_dirs.append(root_dir + dir + '/')
        break
    # data_dirs = sorted(data_dirs, key = lambda x: float(re.search(r'_(\d+|\d+\.\d+)mms', x).group(1)))       # sort the measured data by the speed
    data_dirs = sorted(data_dirs, key = lambda x: float(re.search(r'circle_(\d+|\d+\.\d+)mm', x).group(1)))       # sort the measured data by the radius
    # data_dirs = data_dirs[:-1]
    write_dir = root_dir

    # 191114
    rad_labels = ['1 mm', '2 mm', '8 mm']
    pos_colors = ['red', 'blue', 'green']
    vel_colors = ['red', 'blue', 'green']

    # 191115
    # rad_labels = ['0.5 mm', '1 mm', '3.5 mm', '4 mm', '8 mm', '19.5 mm']
    # pos_colors = ['red', 'blue', 'green', 'orange', 'purple', 'black']
    # vel_colors = ['red', 'blue', 'green', 'orange', 'purple']


    # data_dirs = ['/Volumes/Nicholas/Polaris/data/191114/P_191114-run17-na22_33uCi-circle_5mm_5mms/']
    # write_dir = data_dirs[0]
    #
    # rad_labels = ['5mm']
    # pos_colors = ['black']
    # vel_colors = ['black']
    n_avg = 1

    plot_moving_2D(data_dirs, write_dir, 'pept_coincidence_data.a', timing, N, fopt, n_avg, rad_labels,
                   plot_pos = True, pos_size = 2, pos_colors = pos_colors,
                   plot_vel = False, vel_size = 0.5, vel_colors = vel_colors,
                   )

    return


    # # Single Fit
    # # ----------
    # timing = 5000           # coincidence timing window in ns
    # N = 100                 # number of lines per position for ctrack
    # fopt = 41               # signal to noise ratio for ctrack
    # radius = 1.5
    #
    # # 190611
    # # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190612/190612-run26-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_1/'
    #
    # # 191112
    # # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191112/P_191112-run8-na22_1uCi-circle_10mm_1mms/'
    #
    # # 191115
    # data_dir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run21-na22_1uCi-circle_1.5mm_10mms/'
    #
    # period = 1.5
    # time_cutoff = (2, 7)
    # # time_cutoff = ()
    #
    # print('\nRunning tracking on', data_dir)
    # print('=================================================================================================================================\n')
    #
    # # Fit functions for applying least squares fit
    # def x_func(t, A, omega, phi, offset):
    #     return np.abs(A)*np.cos(omega*t + phi) + offset
    #
    # def y_func(t, A, omega, phi, offset):
    #     return np.abs(A)*np.cos(omega*t + phi) + offset
    #
    # # def z_func(t, A, omega, phi, offset):
    # #     return np.abs(A)*np.sin(omega*t + phi) + offset
    #
    # def z_func(t, offset):
    #     return 0*t + offset
    #
    # p0_x = [radius, 2*np.pi/period, 0, 2]
    # p0_y = [radius, 2*np.pi/period, 0, 1]
    # # p0_z = [10, 2*np.pi/period, 0, 11]
    # p0_z = [5]
    #
    # labels_xy = ['A', 'omega', 'phi', 'Offset']
    # labels_z = ['Offset']
    #
    # fit_funcs = [x_func, y_func, z_func]
    # p0s = [p0_x, p0_y, p0_z]
    # p0_labs = [labels_xy, labels_xy, labels_z]
    #
    # # allevents, coincidence_data = process_coincidences(data_dir,
    # #                                                         [461, 561, 310, 370], timing,
    # #                                                         transformations,
    # #                                                         False)
    # # best_opt, best_pos, best_ucert = f_optimisation(data_dir + 'pept_coincidence_data', 1, N)
    # # plot_basic(data_dir,
    # #            [d0_transformation, d1_transformation],
    # #            'pept_coincidence_data')
    # run_ctrack(data_dir + 'pept_coincidence_data', fopt, N)
    # plot_moving(data_dir, 'pept_coincidence_data.a',
    #             timing, N, fopt, plot_errbars = False, err = [], time_cutoff = time_cutoff, y_limit = radius + 1,
    #             apply_fit = True, fit_functions = fit_funcs, p0s = p0s, p0_labels = p0_labs)
    #
    # return


    # 190912
    # ------
    # timing = 4000           # coincidence timing window in ns
    # N = 200                 # number of lines per position for ctrack
    # fopt = 3               # signal to noise ratio for ctrack
    # fit_file = '/Volumes/Data/polaris_pept/gcodes/circle_8_93-82.5-40_75.gcode'
    # fit_params = [-93, -82.5, -30, 5]
    #
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run15-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_4_0.5/'
    # # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190912/190912-run25-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_4_0.5/'
    #
    # run_ctrack(data_dir + '/pept_coincidence_data', fopt, N)
    # plot_moving(data_dir,
    #             'pept_coincidence_data.a',
    #             timing, N, fopt)
    #             # errors,
    #             # False, fit_file, fit_params,
    #             # False, fit_funcs, p0s, p0_labs)
    #
    # return

    # data_dirs = ['/Volumes/Data/polaris_pept/190612/190612-run26-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_1/',
    #              '/Volumes/Data/polaris_pept/190612/190612-run25-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_2/',
    #              '/Volumes/Data/polaris_pept/190612/190612-run24-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_5/',
    #              '/Volumes/Data/polaris_pept/190612/190612-run23-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_10/',
    #              '/Volumes/Data/polaris_pept/190612/190612-run22-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_20/',
    #              '/Volumes/Data/polaris_pept/190612/190612-run21-ga68_needle-oem2_y_48mm-oem3_y_-48mm-circle_8_30/']
    #
    # periods = [50, 32, 10, 6, 3, 1]
    # Ns = [200, 200, 100, 50, 50, 50]
    #
    # for i in range(1,2):#len(data_dirs)):
    #     data_dir = data_dirs[i]
    #
    #     print('\nRunning tracking on', data_dir)
    #     print('=================================================================================================================================\n')
    #
    #     # Fit functions for applying least squares fit
    #     def x_func(t, A, omega, phi, offset):
    #         return np.abs(A)*np.cos(omega*t + phi) + offset
    #
    #     def y_func(t, A, omega, phi, offset):
    #         return np.abs(A)*np.cos(omega*t + phi) + offset
    #
    #     def z_func(t, A, omega, phi, offset):
    #         return np.abs(A)*np.sin(omega*t + phi) + offset
    #
    #     p0_x = [10, 2*np.pi/periods[i], 0, 2]
    #     p0_y = [10, 2*np.pi/periods[i], 0, -2]
    #     p0_z = [10, 2*np.pi/periods[i], 0, 11]
    #
    #     labels = ['A', 'omega', 'phi', 'Offset']
    #
    #     fit_funcs = [x_func, y_func, z_func]
    #     p0s = [p0_x, p0_y, p0_z]
    #     p0_labs = [labels, labels, labels]
    #
    #     # allevents, coincidence_data = process_coincidences(data_dir,
    #     #                                                         [461, 561, 310, 370], timing,
    #     #                                                         transformations,
    #     #                                                         False)
    #     # best_opt, best_pos, best_ucert = f_optimisation(data_dir + 'pept_coincidence_data', 1, N)
    #     # plot_basic(data_dir,
    #     #            [d0_transformation, d1_transformation],
    #     #            'pept_coincidence_data')
    #     run_ctrack(data_dir + '/pept_coincidence_data', fopt, Ns[i])
    #     plot_moving(data_dir,
    #                 'pept_coincidence_data.a',
    #                 timing, Ns[i], fopt, errors,
    #                 False, fit_file, fit_params,
    #                 True, fit_funcs, p0s, p0_labs)
    #
    return

if __name__ == '__main__':
    main()
