#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <iostream.h>
#include <fcntl.h>
//#include <io.h>
#include <math.h>
//#include <dos.h>



int getevent(void);
void getinput(int *, char *);
void getlonginput(long *, char *);
void ReadData(void);
void initarray(void);
void calculate(void);
void iterate(void);
void result(void);

int nevent = 0;
int position=0;
float xA,xB,yA,yB,zA,zB;
double itime, firstt ;
long First=1; long Last=999999999L;
long rlocs = 10 ;
int Nevent=250; int Minfin=20;
int Maxno=10000; int Slice=1; int Cfactor=120;
int Tdis=20; int Emin=3000; int Emax=6000;
int Fopt=50;
int errmax=100;
long timerev=0,timeang0;
int iang,nrev=0;
float sumrev=0;
float angle=0;
double ttt[100000];
float xx1[100000];
float xx2[100000];
float yy1[100000];
float yy2[100000];
float zz1[100000];
float zz2[100000];
float ang[100000];
float pos[100000];
int use[20480];
double x12[20480];
double y12[20480];
double z12[20480];
double r12[20480];
double q12[20480];
double p12[20480];
double a12[20480];
double b12[20480];
double c12[20480];
double d12[20480];
double e12[20480];
double f12[20480];
double r2[20480];
double dev[20480];
float angs[20480];
float poss[20480];
double tt,xx,yy,zz,er;
float xold[10],yold[10],zold[10];

float x,y,z,error,avtime,avang,avpos,dx,dy,dz,dd,dismin,dismax,temp,zmid;
float dd,Frac,Xmin,Ymin,Zmin,Errmin,Nusmin,Timemin,distance,Dmin;
double sumerr,sumx,sumy,sumz,sumx2,sumy2,sumz2;
int itype,ninit,nused,xoff,yoff,igap,i,i0;
int ninc,nfail,Nmin,Ninitn,ipass,ivariable,ibinary,ifile=0;

char cinput[10],inputname[1000],inputnamea[24],fname[1000],ccc[18];
char ctitle[120],outtext[80];
char cdumname[20],outputname[1000];

int oldtype,Tmax,Tmin,nwords,nreads,irec,ireads,iout,finished=0,nextread,istat=0,findex;
long j,ip,iloop,jm,imin,ir,lir,imax,nprev,nfin;
long ntries,nfirst,ltt,lxx,lyy,lzz,ler,ievent;
char cdtime[40],cgeom[40],chead[80]="",cread[8],cinit[8]="petlog";
FILE *input;
FILE *output;

int main(int argc, char *argv[])
{
// printf("Input file name :") ;
// scanf("%s",fname);
sprintf(fname, "%s", argv[1]);    // name of the file
findex=1;
sprintf(inputname,"%s",fname);
input=fopen(inputname,"rb");
ievent=lir=ir=i0=ip=0;
fflush(stdin);
if (!input){
                 printf("Unable to open file");
                 return 0;
                 }

//getlonginput(&First,"First event");
//getlonginput(&Last,"Last event");
//getlonginput(&rlocs,"Number of locations");
/*printf("Fixed/variable (F/V) [F]:");
gets(cinput);
if(cinput[0]=='V'||cinput[0]=='v'){
  ivariable=1;
  getinput(&Maxno,"Max events/slice");
  getinput(&Minfin,"Min no actually used");
  getinput(&Tdis,"Max distance (mm)");
  getinput(&Emin,"Min error");
  getinput(&Emax,"Max error");
  }
else{ */
  // getinput(&Nevent,"Events per location");
  Nevent=atoi(argv[3]);  // number of lines per position
  Maxno=Nevent;
//  }
//getinput(&Slice,"Locations/slice");
//getinput(&Cfactor,"Convergence (%)");
// getinput(&Fopt,"fopt (signal to noise ratio)");
Fopt = atoi(argv[2]);   // percentage of data to keep
//getinput(&errmax,"max allowable error");

  sprintf(outputname,"%s.a",fname);
 // printf("Output file name [%s]:",outputname);
 // if(strlen(fgets(cdumname, sizeof(cdumname), stdin)));//sprintf(outputname,"%s",cdumname);
//printf("Output Ascii/Binary (A/B) [A]:");
//fgets(cinput, sizeof(cinput), stdin);
/*if(cinput[0]=='B'||cinput[0]=='b'){
  ibinary=1;
  output=fopen(outputname,"wb");
  }
else{
*/
  output = fopen(outputname,"w");
/*
	fprintf(output, "\n\n File shows used events per location") ;
	fprintf(output, "\n The location is given first, in usual ctrack format") ;
	fprintf(output, "\n Followed by the LORs that contribute to the location...") ;
	fprintf(output, "\n Format for LORs is:") ;
	fprintf(output, "\nTime (ms) \tX1 (mm) \tY1 (mm)\tZ1 (mm)\tX2 (mm) \tY2 (mm)\tZ2 (mm)\t Used?\n\n") ;
*/
ibinary=0;
  //}



ReadData();

ninit=Nevent;
nused=ninit;
nfin=Nevent*Fopt/100;
Frac=0.01*Fopt;

// printf("test1");

// printf("\n%d\n", Maxno);


while(ievent<First){
  // printf("\n%d\n", i0);
  i0++;
  // if(i0+500>ip){
  if(i0>ip){
	if(finished)return 0;
	ReadData();
	}
  ievent++;
  }

  // printf("test2");


while((ievent+Nevent)<Last ){
  // if(i0+Maxno+500>ip)
  if(i0+Maxno>ip)
	{
	if(finished)break;
	ReadData();
	}

	 initarray();
	 iterate();
  	 result();

//fprintf(output, "\n\n") ;
//if(lir > rlocs-1) break ;
  //  if(i0+Maxno>ip)ReadData();
  }
sumx=sumx/lir;
sumy=sumy/lir;
sumz=sumz/lir;
printf("\n Number of raw events: %d", nevent);
printf("\n Expected number of locations: %5.0f", (float)(nevent/Nevent)) ;
printf("\n Number of events used per location: %5.0f", (float)(Fopt*Nevent/100));
printf("\n Actual number of events used: %5.0f", (float)(lir*Fopt*Nevent/100));
printf("\n%s\n",chead);
printf("Averages:\n");
printf("%8.1f%8.1f%8.1f\n",sumx,sumy,sumz);
sumx2=sqrt(sumx2/lir-sumx*sumx);
sumy2=sqrt(sumy2/lir-sumy*sumy);
sumz2=sqrt(sumz2/lir-sumz*sumz);
printf("%8.4f%8.4f%8.4f\n",sumx2,sumy2,sumz2);
sumerr=sumerr/lir;
sumx2=sqrt(sumx2*sumx2+sumy2*sumy2+sumz2*sumz2);
printf("delta, D:%8.4f%8.2f\n",sumx2,sumerr);
printf("no of locations=%ld\n",lir);
if(nrev>0){
  sumrev=sumrev/nrev/10000;
  printf("Average rotation period=%8.3fs\n",sumrev);
  }
// getchar();
return 0;

}




void ReadData()
{
igap=ip-i0;
for (i=0;i<igap;i++){
  xx1[i]=xx1[i+i0];
  xx2[i]=xx2[i+i0];
  yy1[i]=yy1[i+i0];
  yy2[i]=yy2[i+i0];
  zz1[i]=zz1[i+i0];
  zz2[i]=zz2[i+i0];
  ttt[i]=ttt[i+i0];
  ang[i]=ang[i+i0];
  pos[i]=pos[i+i0];
}
i0=0;
ip=igap;


while(ip<100000) {
		 i=ip;
		 if (!getevent()){
                   finished=1;
		   break;
		   }
		 xx1[i]=xA;
		 zz1[i]=zA;
		 xx2[i]=xB;
		 zz2[i]=zB;
		 yy1[i]=yA;
		 yy2[i]=yB;
		 ttt[i]=itime; //workout time data
		 ang[i]=angle;
		 pos[i]=position;
  ip=ip+1;
  }
}


void getinput(int * ip,char * string) {
  char string1[20];
  int dum=*ip;
  printf(" %s [%d] :",string,dum);
fflush(stdin);
	if(!strlen(fgets(string1, sizeof(string1), stdin)))
		*ip=dum;
	else
		sscanf(string1,"%d",ip);
	}

void getlonginput(long * ip,char * string) {
  char string1[20];
  long dum=*ip;
  printf(" %s [%ld] :",string,dum);
fflush(stdin);
	if(!strlen(fgets(string1, sizeof(string1), stdin)))
		*ip=dum;
	else
		sscanf(string1,"%ld",ip);
	}


void initarray() {
int i;
for (i=0;i<ninit;i++){
  use[i]=1;
  angs[i]=ang[i+i0];
  poss[i]=pos[i+i0];
  if(i>0 && angs[i]<angs[i-1])angs[i]=angs[i]+360;
  x12[i]=xx1[i+i0]-xx2[i+i0];
  y12[i]=yy1[i+i0]-yy2[i+i0];
  z12[i]=zz1[i+i0]-zz2[i+i0];
  r2[i]=x12[i]*x12[i]+y12[i]*y12[i]+z12[i]*z12[i];

  if(r2[i]==0)r2[i]=1e-6;

  r12[i]=y12[i]*z12[i]/r2[i];
  q12[i]=x12[i]*z12[i]/r2[i];
  p12[i]=x12[i]*y12[i]/r2[i];

  a12[i]=(y12[i]*y12[i]+z12[i]*z12[i])/r2[i];
  b12[i]=(x12[i]*x12[i]+z12[i]*z12[i])/r2[i];
  c12[i]=(y12[i]*y12[i]+x12[i]*x12[i])/r2[i];
  d12[i]=((yy2[i+i0]*xx1[i+i0]-yy1[i+i0]*xx2[i+i0])*y12[i]+(zz2[i+i0]*xx1[i+i0]-zz1[i+i0]*xx2[i+i0])*z12[i])/r2[i];
  e12[i]=((zz2[i+i0]*yy1[i+i0]-zz1[i+i0]*yy2[i+i0])*z12[i]+(xx2[i+i0]*yy1[i+i0]-xx1[i+i0]*yy2[i+i0])*x12[i])/r2[i];
  f12[i]=-((zz2[i+i0]*yy1[i+i0]-zz1[i+i0]*yy2[i+i0])*y12[i]+(zz2[i+i0]*xx1[i+i0]-zz1[i+i0]*xx2[i+i0])*x12[i])/r2[i];
  }
  nused=ninit;
}

void calculate(){
double suma,sumb,sumc,sumd,sume,sumf,sump,sumq,sumr,ab,dq,dp,ar,ac,den;
suma=sumb=sumc=sumd=sume=sumf=sump=sumq=sumr=0;
for (i=0;i<ninit;i++){
  if(use[i]==1){
	 suma=suma+a12[i];
	 sumb=sumb+b12[i];
	 sumc=sumc+c12[i];
	 sumd=sumd+d12[i];
	 sume=sume+e12[i];
	 sumf=sumf+f12[i];
	 sump=sump+p12[i];
	 sumq=sumq+q12[i];
	 sumr=sumr+r12[i];
	 }
  }

ab=suma*sumb-sump*sump;
dq=sumd*sumq+suma*sumf;
dp=sumd*sump+suma*sume;
ar=suma*sumr+sumq*sump;
ac=suma*sumc-sumq*sumq;
den=(ar*ar-ab*ac);

if(den==0)den=1.0e-6;
if(ar==0)ar=1.0e-6;
if(suma==0)suma=1.0e-6;

z=(ab*dq+dp*ar)/den;
y=(z*ac+dq)/ar;
x=(y*sump+z*sumq-sumd)/suma;

error=avtime=avang=avpos=0;

//work out errors and time
for (i=0;i<ninit;i++){
	dx=x-xx1[i+i0];
	dy=y-yy1[i+i0];
	dz=z-zz1[i+i0];

	dd=(dx*z12[i]-dz*x12[i])*(dx*z12[i]-dz*x12[i]);
	dd=dd+(dy*x12[i]-dx*y12[i])*(dy*x12[i]-dx*y12[i]);
	dd=dd+(dz*y12[i]-dy*z12[i])*(dz*y12[i]-dy*z12[i]);
	dev[i]=dd/r2[i];

	if(use[i]==1) {
		error+=dev[i];
		avtime+=ttt[i+i0];
	        avang+=angs[i];
		avpos+=poss[i];
        	}
	}
error=sqrt(error/nused);
avtime=avtime/nused;
avang=avang/nused;
if(avang>360)avang=avang-360;
avpos=avpos/nused;
}


void iterate(){
  while(ninit>0 ){

	 calculate();
	 if(nused==nfin)return;

	 nprev=nused;
	 nused=0;

	 for (i=0;i<ninit;i++){
		if(sqrt(dev[i])>(Cfactor*error/100))
		  use[i]=0;

		else  {
		  use[i]=1;
		  nused=nused+1;
		  }
		}

//Have reduced to too few events, so restore closest unused events
	 while(nused<nfin) {
		dismin=10000;
		for (i=0;i<ninit;i++)
		  if(use[i]==0&&dev[i]<dismin){
			 imin=i;
			 dismin=dev[i];
			 }
		use[imin]=1;
		nused=nused+1;
		}

//Haven't removed any, so remove furthest point
	 while(nused>=nprev) {
		dismax=0;
		for (i=0;i<ninit;i++)
		  if(use[i]==1&&dev[i]>dismax){
			 imax=i;
			 dismax=dev[i];
			 }
		use[imax]=0;
		nused=nused-1;
		}
   }

}


void result() {
int l;
float dum;
unsigned short xbin[4];
unsigned long tbin;
ninc=0;
nfirst=0;
for (i=0;i<ninit;i++)
  if(use[i]==1) {
	 if(nfirst==0) nfirst=i+1;
	 if(ninc<=i) ninc=i+1;
	 }


ninc=nfirst+(ninc-nfirst)/Slice+1;
// nused=ninit;
nfin=ninit*Fopt/100;
if(nfin<5)nfin=5;


if(error>errmax || (ir>=1&&avtime<tt)){
  ninc=5*(nfail+1);
  if(ninc>ninit/2)ninc=ninit/2;
//  if(cmfix!=1) Nevent=2*Nevent;
  nfail=nfail+1;
  }
else  {
  nfail=0;
  if(ivariable)Nevent=2*Nevent;
  ir++;
  lir++;
  ltt=tt=avtime;
  lxx=xx=x;
  lyy=yy=y;
  lzz=zz=z;
  ler=er=error;
  dum=0;
  ++istat;
  if(istat==50){
    istat=0;
    printf("\r processed to time = %10.1f secs",tt/1000);
    }
  if(ibinary){
    xbin[0]=(unsigned short)(xx*10);
    xbin[1]=(unsigned short)(yy*10);
    xbin[2]=(unsigned short)(zz*10);
    xbin[3]=(unsigned short)(er*10);
    tbin=(unsigned long)(tt*10);
    fwrite(&tbin,sizeof(tbin),1,output);
    fwrite(&xbin,sizeof(xbin[0]),4,output);
    }
  else

//fprintf(output, "\n Location was:");
fprintf(output,"\n%10.6f\t%10.6f\t%10.6f\t%10.6f\t%10.6f",tt,xx,yy,zz,er);
/*
fprintf(output, "\n From these data:") ;
for (i=0;i<ninit+1;i++)
{
//if(use[i]==0) continue ;
//fprintf(output, "\n%10.3f\n\t\t%10.3f\t%10.3f\t%10.3f\n\t\t%10.3f\t%10.3f\t%10.3f\t%d\n",
//	ttt[i+i0], xx1[i+i0], yy1[i+i0], zz1[i+i0], xx2[i+i0], yy2[i+i0], zz2[i+i0], use[i]) ;

fprintf(output, "\n%10.3f\t%10.3f\t%10.3f\t%10.3f\t%10.3f\t%10.3f\t%10.3f\t%d",
	ttt[i+i0-1], xx1[i+i0-1], yy1[i+i0-1], zz1[i+i0-1], xx2[i+i0-1], yy2[i+i0-1], zz2[i+i0-1], use[i]) ;

}
*/




  sumx+=xx;
  sumy+=yy;
  sumz+=zz;
  sumx2+=xx*xx;
  sumy2+=yy*yy;
  sumz2+=zz*zz;
  sumerr+=error;
  for(l=1;l<10;l++){      //remember the last 10 locations
	 xold[l]=xold[l-1];
	 yold[l]=yold[l-1];
	 zold[l]=zold[l-1];
	 }
  xold[0]=x;
  yold[0]=y;
  zold[0]=z;
  }
ievent=ievent+ninc;
i0=i0+ninc;
}







int getevent()
{
	float ta, tb, xa, xb, ya, yb, za, zb ;


	if(fscanf(input, "%f, %f, %f, %f, %f, %f, %f, %f", &ta, &xa, &ya, &za, &tb, &xb, &yb, &zb) != 8) return 0 ;
	nevent++;

	if(nevent%100000 == 0)
	{
		// Display running totals of buffers
		printf("\t%d", nevent/10000);
		for(i=0;i<log(nevent/10000);i++) printf("\b");
	}

	itime = (ta + tb) / 2e3 ;
	if(nevent == 1) firstt = itime ;

	itime -= firstt ;
	xA = xa ;
	yA = ya ;
	zA = za ;

	xB = xb ;
	yB = yb ;
	zB = zb ;

//	printf("\n%5.2f, %3.3f, %3.3f, %3.3f, %3.3f, %3.3f, %3.3f", itime, xa, ya, za, xb, yb, zb) ;

	return 1;
}
