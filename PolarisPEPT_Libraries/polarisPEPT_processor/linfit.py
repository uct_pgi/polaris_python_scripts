## Unweighted Least Squares Linear Fit Library
## Nicholas Hyslop
## 26 February 2019

import numpy as np
import matplotlib.pyplot as plt
from math import *

def getM(data):

    N = len(data[0])    # number of elements in array

    # Calculating the numerator
    num = N*fsum(data[0]*data[1])-fsum(data[0])*fsum(data[1])

    # Calculating the denominator
    den = N*fsum(data[0]**2)-(fsum(data[0])**2)

    return num/den  # Final Result


## CALCULATE THE Y-INTERCEPT FOR THE LINE OF BEST FIT
def getC(data):
    N = len(data[0])        # number of elements in array

    # Calculating the numerator
    num = fsum(data[0]**2)*fsum(data[1])-fsum(data[0]*data[1])*fsum(data[0])

    # Calculating the denominator
    den = N*fsum(data[0]**2)-(fsum(data[0])**2)

    return num/den      # Final Result


## GET THE DEVIATION VALUES FOR AN ARRAY OF GIVEN X INPUTS
def getDi(data):
    # Slope and intercept data
    m = getM(data)
    c = getC(data)

    diData = np.zeros(len(data[0]))      # array for deviations

    # Loop through and calculate all the deviation values, saving them to the array
    for i in range(len(data[0])):
        diData[i] = data[1][i] - (m*data[0][i] + c)

    return diData


## CALCULATING UNCERTAINTY FOR SLOPE OF THE LINE OF BEST FIT
def getUM(data):
    # Get the deviation data
    diData = getDi(data)

    N = len(data[0])         # number of items in the array

    # Calculating the numerator
    num = fsum(diData**2)

    # Calculating the denominator
    den = N*fsum(data[0]**2)-(fsum(data[0])**2)

    # Returning the final result
    return sqrt((num/den)*(N/(N-2)))


## CALCULATING UNCERTAINTY FOR THE Y-INTERCEPT OF THE LINE OF BEST FIT
def getUC(data):
    # Get the deviation data
    diData = getDi(data)

    N = len(data[0])        # number of items in the array

    # Calculating the numerator
    num = fsum(diData**2)*fsum(data[0]**2)

    # Calculating the denominator
    den = N*(N*fsum(data[0]**2)-fsum(data[0])**2)

    # Returning the final result
    return sqrt((num/(den))*(N/(N-2)))

def linfit(x, y):
    data = [x,y]
    return getM(data), getUM(data), getC(data), getUC(data)
