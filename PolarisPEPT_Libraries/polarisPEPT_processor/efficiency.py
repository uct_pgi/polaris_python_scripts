# Script to calculate the event rate
# Nicholas Hyslop & Tina Teik
# 11 July 2019

import polarisPEPT_processing as pol
from math import *
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import sys

import pyximport; pyximport.install()
sys.path.append('/home/user/Documents/code/polaris/polaris_data_processor/')                    # Steve laptop
import processPolarisData_utils as utils


def main():
    # 3D printer stage coordinate transformations
    d0_transformation = [180.0, 90.0, 0.0, -45.5, 0.0, 0.0]
    d1_transformation = [0.0, 270.0, 0.0, 45.5, 0.0, 0.0]

    timing = 4000
    directory = '/home/user/Documents/data/190711/190711-run1-na22_oem2_y_+48_oem3_y_-48mm-z_trav/'

    # allevents, coins = pol.process_coincidences(directory, 'AllEventsCombined.txt', [461, 561, 310, 370], timing, [d0_transformation, d1_transformation], False)

    # pol.plot_basic(directory, 'AllEventsCombineds.txt', [d0_transformation, d1_transformation], 'pept_coincidence_data')
    # return
    # allevents = pol.read_polaris_data(directory + 'AllEventsCombined.txt', [d0_transformation, d1_transformation])

    # t_start = coins['time_1'][0]
    # num_events = 0
    #
    # num_events_list = []
    #
    # # Coincidence Counting
    # for index, row in coins.iterrows():
    #   num_events = num_events + 1
    #   # print(num_events)
    #
    #   if row['time_1'] > t_start + 6e8:
    #       num_events_list.append(num_events)
    #
    #       num_events = 0
    #
    #       t_start = row['time_1']
    # #
    # print(num_events_list)

    #
    #
    # #
    # # # Calculating r
    # # -------------
    # ys = [0.0, 3.0, 6.0, 9.0, 12.0, 15.0]
    # h = 40.5
    # b = 11.0
    #
    # rs = []
    #
    # for y in ys:
    #     r_ = sqrt(y**2 + h**2)
    #     r = sqrt(r_**2 + b**2)
    #
    #     rs.append(r)
    #
    # print(rs)
    #
    # areas = []
    #
    # for r in rs:
    #     area = 4 * pi * r**2
    #     areas.append(area)
    #
    # print(areas)
    #
    # sq_width = sqrt(20**2 + 10**2)
    # sq_height = 20
    #
    # sq_area = sq_width * sq_height
    #
    # ratios = []
    #
    # for area in areas:
    #     ratio = (4*sq_area) / area
    #     ratios.append(ratio)
    #
    # print(ratios)
    #
    # activity_of_source = 27e3
    #
    # counts_into_detectors = []
    #
    # for ratio in ratios:
    #     counts = ratio*activity_of_source
    #     counts_into_detectors.append(counts)
    #
    # print(counts_into_detectors)
    # # #
    #
    # # CC Counting
    # # -----------
    #
    # # Variables
    # t_start = allevents['time'][0]
    #
    # num_tot = 0
    # num_doubles = 0
    # num_triples = 0
    # num_other = 0
    # #
    # num_tot_list = []
    # num_doubles_list = []
    # num_triples_list = []
    # num_other_list = []
    # #
    # l = len(allevents.index)
    #
    # # Double scatters processing with CL filtering
    # # ----------------------
    # COMPTON_LINE_RANGE = np.array( [0.960, 1.040] )  # min / max values (percentage) for Compton line filtering
    # COMPTON_LINE_ENERGIES = np.array( [0.511] )
    #
    # sub_dfs = np.array_split(allevents, 11)
    # for df in sub_dfs:
    #     scatters_2x = utils.get_interaction_data(df, False, 2)
    #     scatters_2x_out = utils.format_data_for_output(scatters_2x, 2)
    #     scatters_2x_out = utils.compton_line_filtering(scatters_2x_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
    #     num_doubles_list.append(len(scatters_2x_out))
    #
    # print(num_doubles_list)

    # for index, row in allevents.iterrows():
    #     if (index % int(l/100.0)) == 0:
    #         print(int(index/l * 100))
    #
    #     num_tot += 1
    #
    #     if row['scatters'] == 2:
    #         num_doubles = num_doubles + 1
    #     elif row ['scatters'] == 3:
    #         num_triples = num_triples +1
    #     elif row ['scatters'] >= 4:
    #         num_other = num_other + 1
    #     # print(num_tot)
    #
    #     if row['time'] > t_start + 6e10:
    #         num_tot_list.append(num_tot)
    #         num_doubles_list.append(num_doubles)
    #         num_triples_list.append(num_triples)
    #         num_other_list.append(num_other)
    #
    #         num_tot = 0
    #         num_other = 0
    #         num_doubles = 0
    #         num_triples = 0
    #
    #         t_start = row['time']

    # print(num_tot_list)
    # print(num_triples_list)
    # print(num_other_list)

    # Plot of measured count rate versus predicted count rate
    # ------------------------------------------------------
    measured_count_rate = np.array([814208, 849132, 878815, 906201, 925175, 937485, 941396, 937829, 924984, 902835, 876685])
    measured_count_rate = measured_count_rate/600
    # print(measured_count_rate)
    # #
    # predicted_count_rate = np.array([967, 1008, 1043, 1069, 1086, 1091, 1086, 1069, 1043, 1008, 967])
    # print(predicted_count_rate)
    # #
    # measured_1 = measured_count_rate[:6]
    # measured_2 = measured_count_rate[5:]
    #
    # predicted_1 = predicted_count_rate[:6]
    # predicted_2 = predicted_count_rate[5:]
    #
    # def linear(x, m, b):
    #     return m*x + b
    #
    # m0 = 1
    # b0 = -20
    #
    # popt, pcov = curve_fit(linear, measured_1, predicted_1, [m0, b0])
    #
    # perr = np.sqrt(np.diag(pcov))
    #
    # x_fit = np.linspace(min(measured_1), max(measured_1), 200)
    # y_fit = linear(x_fit, popt[0], popt[1])
    #
    # print(popt)
    #
    # plt.plot(x_fit, y_fit, label =  'Linear Fit\n' +
    #                                 'm = ' + str(round(popt[0], 2)) + r' $\pm$ ' + str(round(perr[0], 2)) + '\n' +
    #                                 'b = ' + str(round(popt[1], 2)) + r' $\pm$ ' + str(round(perr[1], 2)) + '\n')
    #
    # popt, pcov = curve_fit(linear, measured_2, predicted_2, [m0, b0])
    #
    # perr = np.sqrt(np.diag(pcov))
    #
    # x_fit = np.linspace(min(measured_2), max(measured_2), 200)
    # y_fit = linear(x_fit, popt[0], popt[1])
    #
    # print(popt)
    #
    # plt.plot(x_fit, y_fit, label =  'Linear Fit\n' +
    #                                 'm = ' + str(round(popt[0], 2)) + r' $\pm$ ' + str(round(perr[0], 2)) + '\n' +
    #                                 'b = ' + str(round(popt[1], 2)) + r' $\pm$ ' + str(round(perr[1], 2)) + '\n')
    #
    # plt.plot(measured_count_rate, predicted_count_rate, 'o')
    # plt.plot(predicted_count_rate, predicted_count_rate, '-')
    # plt.xlabel('Measured Rate (counts/second)')
    # plt.ylabel('Predicted Rate (counts/second)')
    # plt.legend()
    # plt.show()
    #
    # return
    # #
    # # # # Plot of Count rate versus distance
    # distances = np.array([15, 12, 9, 6, 3, 0, -3, -6, -9, -12, -15])
    #
    # doubles_count_rate = np.array([23285, 23506, 23600, 23879, 23743, 24198, 23859, 23835, 23829, 23570, 23285])                    # with CL filtering
    # # doubles_count_rate = np.array([167688, 175186, 182248, 188008, 192072, 194870, 195298, 193804, 191380, 185178, 180189])       # without CL filtering
    # triples_count_rate = np.array([25365, 26355, 26754, 27774, 28383, 29004, 29034, 28836, 28557, 27573, 26445])
    # unfiltered_doubles_count_rate = np.array([167688, 175186, 182248, 188008, 192072, 194870, 195298, 193804, 191380, 185178, 180189])
    #
    # doubles_count_rate = doubles_count_rate/600
    # # print(doubles_count_rate)
    # triples_count_rate = triples_count_rate/1800
    # unfiltered_doubles_count_rate = unfiltered_doubles_count_rate/1200
    # #
    # maxS= max (unfiltered_doubles_count_rate)
    # unfiltered_doubles_count_rate = unfiltered_doubles_count_rate/maxS
    # print(unfiltered_doubles_count_rate)
    #
    # maxS = max(predicted_count_rate)
    # predicted_count_rate = predicted_count_rate/maxS
    # print(predicted_count_rate)
    #
    # maxS = max(measured_count_rate)
    # measured_count_rate = measured_count_rate/maxS
    # print(measured_count_rate)
    #
    # maxS = max (doubles_count_rate)
    # doubles_count_rate = doubles_count_rate/maxS
    # print(doubles_count_rate)
    #
    # maxS = max (triples_count_rate)
    # triples_count_rate = triples_count_rate/maxS
    # print(triples_count_rate)
    # #
    # plt.plot(distances, predicted_count_rate, '.-', label = 'Predicted Rate')
    # plt.plot(distances, measured_count_rate, '.-', label = 'Total Measured')
    # plt.plot(distances, unfiltered_doubles_count_rate, '.-', label = 'Unfiltered Doubles Rate Measured')
    # plt.plot(distances, doubles_count_rate, '.-', label = 'Filtered Doubles Rate Measured')
    # plt.plot(distances, triples_count_rate, '.-', label = 'Triples Rate Measured')
    # plt.legend(loc = 'center')
    # plt.show()

    # Plot of coincidences versus distance
    # ------------------------------------
    # measured_count_rate = np.array([458, 540, 680, 2733, 5159, 7171, 7596, 5539, 3087, 1172, 528])
    # measured_count_rate = measured_count_rate/600
    #
    # distances = np.array([15, 12, 9, 6, 3, 0, -3, -6, -9, -12, -15])
    #
    # def gaus(x, A, mu, sigma):
    #     return A*np.exp(-(x-mu)**2/(2*sigma**2))
    #
    # A0 = 12.0
    # mu0 = -2.0
    # sigma0 = 14.0
    #
    # popt, pcov = curve_fit(gaus, distances, measured_count_rate, [A0, mu0, sigma0])
    #
    #
    # perr = np.sqrt(np.diag(pcov))
    #
    # x_fit = np.linspace(-15, 15, 200)
    # y_fit = gaus(x_fit, popt[0], popt[1], popt[2])
    #
    # print(x_fit[np.where(y_fit == max(y_fit))[0]])
    #
    # print(popt)
    #
    # plt.plot(distances, measured_count_rate, 'o')
    # plt.plot(x_fit, y_fit, label =  'Normal Distribution Fit\n' +
    #                                 'A = ' + str(round(popt[0], 2)) + r' $\pm$ ' + str(round(perr[0], 2)) + '\n' +
    #                                 r'$\mu$ = ' + str(round(popt[1], 2)) + r' $\pm$ ' + str(round(perr[1], 2)) + '\n' +
    #                                 r'$\sigma$ = ' + str(round(popt[2], 2)) + r' $\pm$ ' + str(round(perr[2], 2)))
    # plt.xlabel('Z Coordinate (mm)')
    # plt.ylabel('Coincidence Rate (coincidences/second)')
    # plt.legend()
    # plt.show()

    # Plot of Ratio between singles, doubles, and triples versus distance
    # -------------------------------------------------------------------
    # distances = np.array([15, 12, 9, 6, 3, 0, -3, -6, -9, -12, -15])
    #
    # ratioSD = 0
    # ratioST = 0
    # ratioDT = 0
    #
    # doubles_count_rate = np.array([23285, 23506, 23600, 23879, 23743, 24198, 23859, 23835, 23829, 23570, 23285])
    # triples_count_rate = np.array([25365, 26355, 26754, 27774, 28383, 29004, 29034, 28836, 28557, 27573, 26445])
    #
    # doubles_count_rate = doubles_count_rate/600
    # triples_count_rate = triples_count_rate/1800
    #
    # doubles_count_rate += 0.0
    # triples_count_rate += 0.0
    #
    # ratioSD = doubles_count_rate/measured_count_rate
    # # ratioSD = ratioSD/max(ratioSD)
    # # print(ratioSD)
    # ratioST = triples_count_rate/measured_count_rate
    # # ratioST = ratioST/max(ratioST)
    # # print(ratioST)
    # ratioDT = triples_count_rate/doubles_count_rate
    # # ratioDT = ratioDT/max(ratioDT)
    # print(ratioDT)
    #
    #
    # plt.semilogy(distances, ratioSD, '.-', label = 'Doubles/Singles')
    # plt.semilogy(distances, ratioST, '.-', label = 'Triples/Singles')
    # plt.semilogy(distances, ratioDT, '.-', label = 'Triples/Doubles')
    # plt.ylim(0.005, 100)
    # plt.legend(loc = 'upper right')
    # plt.show()

    #PLot of filtered/unfiltered ratio vs distance
    #----------------------------------
    # distances = np.array([15, 12, 9, 6, 3, 0, -3, -6, -9, -12, -15])
    #
    # doubles_count_rate = np.array([23285, 23506, 23600, 23879, 23743, 24198, 23859, 23835, 23829, 23570, 23285])
    # doubles_count_rate = doubles_count_rate/600              # with CL filtering
    #
    # unfiltered_doubles_count_rate = np.array([167688, 175186, 182248, 188008, 192072, 194870, 195298, 193804, 191380, 185178, 180189])          #without CL filtering
    # unfiltered_doubles_count_rate = unfiltered_doubles_count_rate/1200
    #
    # ratioFU = 0
    # ratioFU = doubles_count_rate/unfiltered_doubles_count_rate
    #
    # plt.plot(distances, ratioFU, 'o-', label = 'Filtered Rate/Unfiltered Rate')
    # plt.show()

    # Running imaging
    # ---------------
    # timing = 4000
    # N = 500
    # fopt = 93

    # allevents = pol.read_polaris_data(directory + 'AllEventsCombined.txt', [d0_transformation, d1_transformation], True)
    #
    # start_time = allevents.iloc[0]['time']
    # allevents.loc[:,'time'] -= start_time      # Normalised time to start at 0
    #
    # print('Extracting data at 0mm...')
    # centre_data = allevents.loc[(allevents.time >= 60*60e8) & (allevents.time <= 70*60e8)]    # get the data with source at the centre
    # print('Total number of events at 0mm =', len(centre_data.index))
    #
    # # print('Writing results to file...')
    # # # Write the centre results to file
    # # with open(directory + 'AllEventsCombined_0mm.txt', 'w') as write_file:
    # #     centre_data.time = centre_data.time.astype(int)
    # #     centre_data.to_csv(write_file, sep = '	', index = False, header = False, float_format = '%.2f')
    #
    # pol.process_coincidences(centre_data, directory, [461, 561, 310, 370], timing, [d0_transformation, d1_transformation], False)
    # pol.plot_basic(directory, 'AllEventsCombined_0mm.txt', [d0_transformation, d1_transformation], 'pept_coincidence_data')
    # best_opt, best_pos, best_ucert = pol.f_optimisation(directory + 'pept_coincidence_data', 1, N)
    # pol.run_ctrack(directory + 'pept_coincidence_data', fopt, N)




if __name__ == '__main__':
    main()
