# Splits up mod files to smaller sizes in order to convert to .txt files
# Nicholas Hyslop

import os

# file_dir = '/Volumes/Data/polaris_pept/190611/190611-run5-ga68_vial-oem2_y_48mm-oem3_y_-48mm-30min/'  # directory to .mod files
# write_dir = '/Volumes/Data/polaris_pept/190611/190611-run5-ga68_vial-oem2_y_48mm-oem3_y_-48mm-30min/mod_subfiles/'  # directory to write .mod subfiles to

file_dir = '/Volumes/Data/polaris_pept/190611/190611-run6-ga68_vial-oem2_y_48mm-oem3_y_-48mm-1hr/'  # directory to .mod files
write_dir = '/Volumes/Data/polaris_pept/190611/190611-run6-ga68_vial-oem2_y_48mm-oem3_y_-48mm-1hr/mod_subfiles/'  # directory to write .mod subfiles to


num_subfiles = 16        # number of subfiles to split mod files into

mod1_data = []
mod2_data = []

print('Reading mod51.dat...')
with open(file_dir + 'mod51.dat', 'rb') as f:
    mod1_data = f.read()

print('Reading mod52.dat...')
with open(file_dir + 'mod52.dat', 'rb') as f:
    mod2_data = f.read()

print()
for i in range(num_subfiles):
    print('Writing', (i+1), '/' + str(i+1) + ':')
    # First create folder to place subfiles into
    if not os.path.exists(write_dir + 'sub_' + str(i+1)):
        os.makedirs(write_dir + 'sub_' + str(i+1))

    print('Writing mod51.dat...')
    with open(write_dir + 'sub_' + str(i+1) + '/mod51.dat', 'wb') as f:
        start = int( (len(mod1_data) * i) / num_subfiles)
        stop = int( (len(mod1_data) * (i+1)) / num_subfiles)

        f.write(mod1_data[start:stop])

    print('Writing mod52.dat...')
    with open(write_dir + 'sub_' + str(i+1) + '/mod52.dat', 'wb') as f:
        start = int( (len(mod2_data) * i) / num_subfiles)
        stop = int( (len(mod2_data) * (i+1)) / num_subfiles)

        f.write(mod2_data[start:stop])
