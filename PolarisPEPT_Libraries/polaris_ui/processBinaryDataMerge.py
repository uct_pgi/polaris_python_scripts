#!/usr/bin/python
"""
Description:
    Python script that reads in raw (binary) data from Polaris (typically named: Measurement.dat)
    and can:
    - convert raw binary into a .txt file typically used in post processing.
    - merge two raw binaries into a combined .txt file for processing

    To Run: python processBinaryDataMerge.py
    Settings are handled through Global Variable under SETTINGS
      - INPUT_FILES                 = name (and directory) of input files (raw Polaris data file) as list
      - OUTPUT_FILES                = auto-generated names from INPUT_FILES list of output files
      - OUTPUT_DIR                  = directory to write the output files
      - MAX_EXP_INTER               = the maximum number of interactions expected in each detector
      - DET_NUMS                    = the crystal numbers of each detector (typically [1,2,3,4])
      - SHOW_INTER                  = only show events with this many interactions or more (currently disabled)

NOTES:
- written in Python 3.7.2

Code borrowed from Steve Peterson <steve.peterson@uct.ac.za>
"""

__author__ =    "Nicholas Hyslop <hysnic007@myuct.ac.za>"
__date__ =      "Saturday, 19 January 2019"
__version__ =   "0.1"

"""
Information regarding the file formats:

Binary Format
-------------
    The data format is regular network byte order, big endian.  Here is the example.
        1 byte: number of interactions
        4 bytes: timestamp in second
        4 bytes: timestamp in millisecond

    loop interactions with:
        1 byte: detector number
        4 byte: x position in um
        4 byte: y position in um
        4 byte: z position in 10um
        4 byte: energy in keV
    for instance, if the number of interactions is 1, then it is 9+17 = 26 bytes.

    If the number of interactions is 122, then it indicates a timestamp data.  Then it follows the following format.
        1 byte: 122
        1 byte: 2
        8 byte: Sync pulse index
        8 byte: Sync pulse time stamp

    If the number of interactions is 0, skip next 23 bytes.

Text Format
-----------
The output .txt files have each recorded event on a separate line, with each
piece of information separated by a tab (\t).

The columns for the individual detector data .txt files are as follows:

    Column 0 - Number of interactions
    Column 1 - X-position in um
    Column 2 - Y-position in um
    Column 3 - Z-position in um
    Column 4 - Energy in eV
    Column 6 - time stamp in 10 * nanoseconds

The columns for the AllEventsCombined.txt data are as follows:

    Column 0 - Number of interactions
    Column 1 - Detector Number [the first IP is System 0 and the second IP is system 1]
    Column 2 - Energy in keV
    Column 3 - X-position in mm
    Column 4 - Y-position in mm
    Column 5 - Z-position in mm
    Column 6 - time stamp in 10 * nanoseconds

"""

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------
import timeit
import os
import gc

import numpy as np
import pandas as pd

#------------------------------------------------------------------
# SETTINGS / DECLARATIONS
#------------------------------------------------------------------

# Input file must be fed in as a list. The code then merges all the data from all the files in the list.
# INPUT_FILES = ["/Users/nicholas/Google Drive/Work/code/polaris/binary_converter/data/190115-test003-OEM3_is_back-haijian_code-600s/mod51.dat",
#                "/Users/nicholas/Google Drive/Work/code/polaris/binary_converter/data/190115-test003-OEM3_is_back-haijian_code-600s/mod52.dat"]

INPUT_FILES = ["/Users/nicholas/Google Drive/Work/code/polaris/binary_converter/data/192901-na22-3600s/mod51.dat",
               "/Users/nicholas/Google Drive/Work/code/polaris/binary_converter/data/192901-na22-3600s/mod52.dat"]

OUTPUT_DIR = '/Users/nicholas/Google Drive/Work/code/polaris/binary_converter/data/192901-na22-3600s/'

MAX_EXP_INTER = 6       # the maximum number of interactions expected
DET_NUMS = [2,4]        # the number identifiers of the detectors
PRINT_STATS = 0         # print out the statistics of skipped bytes
SHOW_INTER = 1          # only show events with this many interactions or more

#------------------------------------------------------------------

'''
FIND NEXT PULSE FUNCTION
Find the next sync pulse in the array.
Assumes the data array is of a single detector and of the format:
[
    [Number of interactions],
    [Detector number],
    [Energy, 1 decimal place, (keV)],
    [x position (mm), 2 decimal places],
    [y position (mm), 2 decimal places],
    [z position (mm), 2 decimal places],
    [Time stamp (10*nanoseconds)]
]

Input:
- data: detector data array
- prev_sync: index of the last sync pulse

Returns: index of next sync pulse, or -1 if end of array is reached
'''
#------------------------------------------------------------------
def find_nxt_pulse(data, prev_pulse):
    nxt_pulse_index = prev_pulse + 1    # starting from just after the last pulse index

    while data[0][nxt_pulse_index] != 122:
        # print(nxt_pulse_index)
        # print(str(nxt_pulse_index) + ". " + str(data[0][nxt_pulse_index]))
        if nxt_pulse_index + 1 == len(data[0]):     # check end of array hasn't been reached
            return -1

        nxt_pulse_index += 1

    return nxt_pulse_index

#------------------------------------------------------------------
"""
MERGER FUNCTION
Takes in an array of detectors data and merges them according to the synchronisation pulses.
Roughly, the procedure is:
- Line up the files according to the first matching sync pulses.
- For each detector output, take the data between the first and second sync pulses
- Correct the time output of the slave detector according to the sync pulse time difference.
- Merge the two subsets, sorting by time stamp.
- Repeat for each data subset between sync pulses.

Writes the results to file and returns void.

Input:
data_arrays         = array of input data from each camera
output_dir          = directory to write the merged data to

NOTE:
- Only works with two dectectors

TODO:
- Generalise for n detectors
- Add exception handling
"""
#------------------------------------------------------------------
def merge(data_arrays, output_dir):

    # Sort the data arrays with the largest time stamp at the top
    data_arrays.sort(key = lambda x: x[6][0], reverse = 1)

    # Separate out the two detector arrays and add detector numbers
    det_1 = np.array(data_arrays[0])
    det_1 = np.append(det_1, [np.zeros(len(det_1[0]))], axis = 0)

    det_2 = np.array(data_arrays[1])
    det_2 = np.append(det_2, [np.ones(len(det_2[0]))], axis = 0)

    # Get the index of the first sync pulses
    sync_1 = np.argmax(det_1[0]==122)
    sync_2 = np.argmax(det_2[0]==122)

    print("First sync pulse for each detector: ")
    print('Detector 0 = ' + str(det_1[1][sync_1]))
    print('Detector 1 = ' + str(det_2[1][sync_2]))
    print()

    # Make sure the sync pulses start on the same pulse number
    # Assumes that the largest time stamped detector data has the lowest sync pulse number
    while det_1[1][sync_1] < det_2[1][sync_2]:
        sync_1 = find_nxt_pulse(det_1, sync_1)

    print("First common sync pulse: ")
    print('Detector 0 = ' + str(det_1[1][sync_1]))
    print('Detector 1 = ' + str(det_2[1][sync_2]))
    print()

    # For processing the arrays between sync pulses
    prev_sync_1 = sync_1
    prev_sync_2 = sync_2

    with open(output_dir + 'AllEventsCombined.txt', 'w') as write_file:

        print('Time correcting, sorting and writing combined data...')

        # First correct, merge and write all the data before the first common sync pulse
        # ------------------------------------------------------------------------------

        # Get the data between sync pulses
        det_1_tmp = det_1[:,0:sync_1]
        det_2_tmp = det_2[:,0:sync_2]

        # Remove the sync pulses in the temporary arrays
        det_1_tmp = det_1_tmp[:,det_1_tmp[0]!=122]
        det_2_tmp = det_2_tmp[:,det_2_tmp[0]!=122]

        # Correct the smaller time stamp data
        sync_diff = det_1[2][prev_sync_1] - det_2[2][prev_sync_2]           # get the time difference...
        det_2_tmp[6] = np.add(det_2_tmp[6], sync_diff, casting="unsafe")    # ...and correct

        combined = np.concatenate((det_1_tmp, det_2_tmp), axis=-1)      # combine the two arrays
        combined = combined.transpose()                                 # take the transpose for sorting
        combined = combined[combined[:,6].argsort()]                    # sort by the time stamp

        for i in range(0,len(combined)):
            write_line = "{:.0f}\t{:.0f}\t{:.1f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.0f}".format(
                combined[i][0],
                combined[i][7],
                combined[i][2]/1e3,
                combined[i][3]/1e3,
                combined[i][4]/1e3,
                combined[i][5]/1e3,
                combined[i][6]
            )
            # print(write_line)
            write_file.write(write_line + "\n")

        # Now correct, merge and write all the data after the first common sync pulse
        # ---------------------------------------------------------------------------
        while sync_1 != -1 and sync_2 != -1:            # while the end of the arrays hasn't been reached

            # print(det_1[1][sync_1])
            # print(det_2[1][sync_2])
            # print()

            # Get the next sync pulse for each array
            sync_1 = find_nxt_pulse(det_1, sync_1)
            sync_2 = find_nxt_pulse(det_2, sync_2)

            # Get the data between sync pulses
            det_1_tmp = det_1[:,prev_sync_1+1:sync_1]
            det_2_tmp = det_2[:,prev_sync_2+1:sync_2]

            # Correct the smaller time stamp data
            sync_diff = det_1[2][prev_sync_1] - det_2[2][prev_sync_2]     # get the time difference
            det_2_tmp[6] = np.add(det_2_tmp[6], sync_diff, casting="unsafe")    # correct

            combined = np.concatenate((det_1_tmp, det_2_tmp), axis=-1)      # combine the two arrays
            combined = combined.transpose()                                 # take the transpose for sorting
            combined = combined[combined[:,6].argsort()]                    # sort by the time stamp

            for i in range(0,len(combined)):
                write_line = "{:.0f}\t{:.0f}\t{:.1f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.0f}".format(
                    combined[i][0],
                    combined[i][7],
                    combined[i][2]/1e3,
                    combined[i][3]/1e3,
                    combined[i][4]/1e3,
                    combined[i][5]/1e3,
                    combined[i][6]
                )
                # print(write_line)
                write_file.write(write_line + "\n")

            prev_sync_1 = sync_1
            prev_sync_2 = sync_2

    return

#------------------------------------------------------------------
'''
READ IN BINARY FILE
Reads in the binary files produced by Polaris, writes each detector data to file
and returns an array containing all the data

Input:
input_files         = list of the paths to the binary files to read in
max_exp_inter       = the maximum number of interactions expected
det_nums            = the number identifiers of the crystals in the detectors
print_stats         = print out the statistics of skipped bytes or not
show_inter          = only show events with this many interactions or more
output_dir          = directory to write the merged data to

Output:
data_arrays         = List of detector data lists. Each detector data list has the following format:
                      [
                          [Number of interactions],
                          [Crystal number],
                          [Event energy (in eV)],
                          [X position (in micrometers)],
                          [Y position (in micrometers)],
                          [Z position (in micrometers)],
                          [Time Stamp (in 10*nanoseconds)]
                      ]

NOTES:
Further details for reading binary in python can be found at:
https://www.devdungeon.com/content/working-binary-data-python#writefile

Seek can be called one of two ways:
  x.seek(offset)
  x.seek(offset, starting_point)

starting_point can be 0, 1, or 2
0 - Default. Offset relative to beginning of file
1 - Start from the current position in the file
2 - Start from the end of a file (will require a negative offset)
'''
#------------------------------------------------------------------
def read_binary(input_files, max_exp_inter, det_nums, print_stats, show_inter, output_dir):

    output_files = [input_files[0][input_files[0].rfind('/')+1:input_files[0].rfind('.')] + '.txt',
                    input_files[1][input_files[1].rfind('/')+1:input_files[1].rfind('.')] + '.txt']

    data_arrays = []        # list to hold data arrays for each detector

    for file_num, INPUT_FILE in enumerate(input_files):

        # Reading in binary data file
        # ---------------------------
        print ('Reading input file, processing binary and writing output to text; could take several minutes . . .')
        print ('   - Filename: {}'.format(INPUT_FILE))
        print ('   - Size: {} bytes'.format(os.path.getsize(INPUT_FILE)))
        print()

        # Open and read binary:
        with open(INPUT_FILE, "rb") as binary_file:

            """
            The detector data array has the following format:

            [
                [Number of interactions],
                [Crystal number],
                [Event energy (in eV)],
                [X position (in micrometers)],
                [Y position (in micrometers)],
                [Z position (in micrometers)],
                [Time Stamp (in 10*nanoseconds)]
            ]
            """
            detector_data = [[],[],[],[],[],[],[]]

            # Tracking variables used for data format verification
            curr_pos = 0            # current read position in the file
            prev_pos = 0            # previous read position with correct data format
            des_num_events = -1     # how many events you want to read out. -1 reads the whole file

            # Print out output as per .txt format
            # print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format( "#it",
            #                                            "#inter",
            #                                            "det_num",
            #                                            "E(keV)",
            #                                            "x_pos",
            #                                            "y_pos",
            #                                            "z_pos",
            #                                            "t(ms)")
            # )

            stats = []
            num_events = 0        # to count number of events read out

            with open(output_dir + output_files[file_num], 'w') as write_file:   # open write file

                while True:

                    # Check if read out desired number of events
                    if des_num_events > 0:
                        if num_events > des_num_events:
                            break

                    # testing for end of file
                    curr_pos_test = binary_file.tell()
                    test = binary_file.read(1)
                    if not test:
                        break
                    binary_file.seek(curr_pos_test)

                    # Print out .txt formatted details for first n events:
                    # ----------------------------------------------------
                    # Seek a specific position in the file
                    binary_file.seek(curr_pos, 0)

                    # Number of interactions
                    num_inter = binary_file.read(1)
                    num_inter_int = int.from_bytes(num_inter, byteorder='big')

                    # Filler for sync pulse
                    filler = binary_file.read(1)
                    filler_int = int.from_bytes(filler, byteorder='big')

                    # Time stamp
                    binary_file.read(7)

                    # Number of the crystal
                    num_det = binary_file.read(1)
                    num_det_int = int.from_bytes(num_det, byteorder='big')

                    # Check if read line has expected format
                    if (num_inter_int == 122 and filler_int == 2) or (num_det_int in det_nums and 0 < num_inter_int <= max_exp_inter):
                        # Seek a specific position in the file
                        binary_file.seek(curr_pos, 0)
                        # Count the number of skipped bytes and store them
                        if curr_pos-prev_pos != 26:  # avoid regular byte skips
                            stats.append(curr_pos-prev_pos)

                        prev_pos = curr_pos

                    # otherwise check next byte in the sequence
                    else:
                        # err_str = "{}.\t{}\t{}".format(num_events,
                        #                                 num_inter_int,
                        #                                 num_det_int)
                        # print(err_str)
                        curr_pos += 1
                        continue

                    # Number of interactions
                    num_inter = binary_file.read(1)
                    num_inter_int = int.from_bytes(num_inter, byteorder='big')

                    # Process sync pulse
                    if num_inter_int == 122:
                        filler = int.from_bytes(binary_file.read(1), byteorder='big')
                        sync_pulse_index = int.from_bytes(binary_file.read(8), byteorder='big')
                        sync_pulse_timestamp = int.from_bytes(binary_file.read(8), byteorder='big')
                        #print('-- num_inter  sync_pulse_index                         time_stamp/10ns?  filler  --')

                        write_str = "{}\t{}\t{}".format(num_inter_int,
                                                        sync_pulse_index,
                                                        sync_pulse_timestamp)
                        # print(write_str)
                        write_file.write(write_str+"\n")

                        # Save the sync pulse data
                        detector_data[0].append(num_inter_int)
                        detector_data[1].append(sync_pulse_index)
                        detector_data[2].append(sync_pulse_timestamp)
                        detector_data[3].append(-1)
                        detector_data[4].append(-1)
                        detector_data[5].append(-1)
                        detector_data[6].append(-1)

                        curr_pos+=18
                        num_events += 1
                        continue

                    # Time in nanoseconds*10
                    time_ns = binary_file.read(8)
                    time_ns_int = int.from_bytes(time_ns, byteorder='big')

                    for j in range(num_inter_int):
                        # Number of the detector
                        num_det = binary_file.read(1)
                        num_det_int = int.from_bytes(num_det, byteorder='big')
                        # Interaction x-coordinate in micrometers
                        x_pos = binary_file.read(4)
                        x_pos_int = int.from_bytes(x_pos, byteorder='big', signed=True)
                        # Interaction y-coordinate in micrometers
                        y_pos = binary_file.read(4)
                        y_pos_int = int.from_bytes(y_pos, byteorder='big', signed=True)
                        # Interaction z-coordinate in micrometers
                        z_pos = binary_file.read(4)
                        z_pos_int = int.from_bytes(z_pos, byteorder='big', signed=True)
                        # Energy of interaction
                        eng = binary_file.read(4)
                        eng_int = int.from_bytes(eng, byteorder='big')

                        # Show event details
                        if num_inter_int >= show_inter:

                            print_str = "{}.\t{}\t{}\t{}\t{:.1f}\t{:.2f}\t{:.2f}\t{:.2f}\t{}".format(num_events,
                                                                                curr_pos,
                                                                                num_inter_int,
                                                                                num_det_int,
                                                                                eng_int/1000.0,
                                                                                x_pos_int/1000.0,
                                                                                y_pos_int/1000.0,
                                                                                z_pos_int/1000.0,
                                                                                time_ns_int)        # TODO: This should be in 10*ns
                            # print(print_str)

                            # print_str = "{}\t{}\t{}\t{}\t{}\t{}".format(num_inter_int,
                            #                                             x_pos_int,
                            #                                             y_pos_int,
                            #                                             z_pos_int,
                            #                                             eng_int,
                            #                                             time_ns_int)        # TODO: This should be in 10*ns
                            # print(print_str)

                        write_str = "{}\t{}\t{}\t{}\t{}\t{}".format(num_inter_int,
                                                                    x_pos_int,
                                                                    y_pos_int,
                                                                    z_pos_int,
                                                                    eng_int,
                                                                    time_ns_int)
                        # print(write_str)
                        write_file.write(write_str+"\n")

                        # Save the event data
                        detector_data[0].append(num_inter_int)
                        detector_data[1].append(num_det_int)
                        detector_data[2].append(eng_int)
                        detector_data[3].append(x_pos_int)
                        detector_data[4].append(y_pos_int)
                        detector_data[5].append(z_pos_int)
                        detector_data[6].append(time_ns_int)
                        num_events += 1

                    curr_pos+= 9 + num_inter_int*17

                # Append the detector data to the combined data array
                data_arrays.append(detector_data)

        if print_stats:
            print()
            print("Skipped Bytes Statistics")
            print("========================")
            print("Set of number of bytes skipped:")
            print(set(stats))
            print()
            print("Ordered set of number of bytes skipped with number of occurences of that number of bytes skipped")
            print("Tuple format: (<number of bytes skipped>, <number of occurences>)")
            occurences = []
            for item in stats:
                occurences.append((item, stats.count(item)))
            occurences = set(occurences)
            occurences = sorted(occurences, key=lambda tup: tup[1])
            print(occurences)
            print()
            print('-----------------------')
            print()

    return data_arrays

#------------------------------------------------------------------
# MAIN PROGRAM: START OF EXECUTION
#------------------------------------------------------------------
def main():
    print ('\n--- Running processBinaryMerge.py ---\n')

    # Measuring time required to run code - start time
    start = timeit.default_timer()

    # Read in the binary data
    data_arrays = read_binary(INPUT_FILES, MAX_EXP_INTER, DET_NUMS, PRINT_STATS, SHOW_INTER, OUTPUT_DIR)

    # Merge binaries
    print("Merging data arrays...\n")
    print()
    merge(data_arrays, OUTPUT_DIR)
    print()
    print('Merging complete.')

    # Measuring time required to run code - print final time
    print()
    print ('Total time required {} s'.format(timeit.default_timer() - start))

    # print(data_arrays)

    return data_arrays

if __name__ == '__main__':
    main()
