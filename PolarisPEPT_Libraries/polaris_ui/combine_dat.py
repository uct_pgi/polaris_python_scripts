import sys, os
#import preProcess
import numpy as np
from matplotlib.pyplot import *
from itertools import compress
import math
import timeit
import pandas as pd
#from openpyxl import load_workbook
##from readStreamingDat import *
sys.path.append('/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/polaris/polaris_ui/preProcess/')                    # Nicholas laptop
# sys.path.append('/home/nicholas/polarisPEPT/polaris/polaris_ui/preProcess/')                    # Steve server
# sys.path.append('/home/user/Documents/code/polaris/polaris_ui/preProcess/')                   # Polaris laptop
# sys.path.append('/Volumes/BATMAN/PGI/code/polaris/polaris_ui/preProcess/')                    # Steve laptop
# NOTE:
# I keep my python files that contain commonly used functions in a single folder for easy access so I don't need to constantly recopy them
# that's where this path points; it's where the preProcess functions are located
import preProcess

# Import cython utils library from polaris_data_processor
import pyximport; pyximport.install(setup_args={"include_dirs":np.get_include()})
sys.path.append('/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/polaris/polaris_ui/')       # Nicholas latop
# sys.path.append('/home/nicholas/polarisPEPT/polaris/polaris_data_processor/')                 # Steve server
# sys.path.append('/Volumes/BATMAN/PGI/code/polaris/polaris_data_processor/')                   # Steve laptop
import processPolarisData_utils_ui as utils

###notes: I generally uses this script by having it located one directory up from where all the different data folders are stored
### e.g. in a directory called "C:\Users\Paul.Maggi\Desktop\temp\nov_17_2018_proton"
### also in this directory are the data folders for each measurement, example "Nov17_2018_150MeV_20kMUmin_50kMU_run1" that has a full path
### of "C:\Users\Paul.Maggi\Desktop\temp\nov_17_2018_proton\Nov17_2018_150MeV_20kMUmin_50kMU_run1"
### it assumes the .dat files are all named "modN.dat" where N is an index from 51 to 66.

### This script has been modified for the UCT polaris detectors, which only has two detector units.

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

'''
DAT MERGER FUNCTION (doh)
-------------------
Takes in the directory that the .dat files are stored in and outputs an AllEventsCombined.txt file.
The AllEventsCombined.txt file is stored in the same directory as the .dat files.
Each line in the .txt file has the following format:
    Number of interactions
    Detector number
    Energy, 1 decimal place, (keV)
    x position (mm), 2 decimal places
    y position (mm), 2 decimal places
    z position (mm), 2 decimal places
    Time stamp (10*nanoseconds)
Where each value is separated by a tab (\t)
'''
def merge_dats(dataDir):
    # dataDir = './J_191102-run1-na22_at_0_0_10mm-oem2_x_-20mm-oem3_x_20mm-3600s/' #what data folder to look in

    syncPeriod = 200000006 #this is how many clock incrememts each sync period should be - it's essentially the difference in timestamps for module 51, which for our configuration is the master
    # syncPeriod = 199998397

    events = np.zeros((1, 7))
    syncs = np.zeros((1, 3))

    for iii in range(51, 53):
    # for iii in range(51, 52):
        print('\n  Reading in data from ' + dataDir + 'mod%i.dat...' % iii)
        test = dataDir + 'mod%i.dat' % iii
        print ('   - Size: {} bytes'.format(os.path.getsize(test)))

        if os.path.isfile(test):
            #  readCameraBinary2 returns: aa is the list of events (including timestamps), bb is the list of time stamps
            aa, bb = preProcess.readCameraBinary2(test, iii)

            # Write the individual module results to file
            print('  Writing data to ' + dataDir + 'mod%i.txt' % iii)
            print ('   - Number of interactions: {}'.format(len(aa)))
            #  save data (with timestamps) for each module as text file

            with open(dataDir + 'mod%i.txt' % iii, 'w') as write_file:
                aa_events = []  # temp array to store only interaction data
                l = len(aa)
                printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
                for i, evt in enumerate(aa):
                    # Progress bar
                    if i % int(l/100) == 0:
                        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

                    if evt[0] == 122:
                        #  writing out time stamp data
                        write_line = "{}\t{}\t{}".format( evt[0], evt[2], evt[3] )
                    else:
                        #  writing out interaction data
                        write_line = "{}\t{}\t{}\t{}\t{}\t{}".format( evt[0], evt[4], evt[5], evt[6], evt[3], evt[7] )
                        #  only storing interaction data (without timestamps)
                        aa_events.append(evt)
                    write_file.write(write_line + "\n")

                printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

                # convert interaction data (minus timestamps) into numpy array
                aa = np.array( aa_events )

            # removes chip number from events array
            tempEvent = np.array( aa[:, (0, 1, 3, 4, 5, 6, 7)].copy())
            #note: I have the code set up so that I only use data that occurs between timestamps so I can do accurate time correction
            startInd = 0
            while bb[0, 2] > tempEvent[startInd, -1]:       # while the time of the first sync pulse
                startInd += 1
            endInd = tempEvent.shape[0] - 1
            while bb[-1, 2] < tempEvent[endInd, -1]:
                endInd -= 1
            # truncates tempEvent array between first and last time stamps
            fixedScale = tempEvent[startInd:endInd, :].copy()
            # loops through time stamps [qqq is the index]
            l = bb.shape[0] - 1
            print('\nProcessing time stamp data')
            # printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            # x_axis = []
            # sync_pulses = []
            # num_pulses = []

            for qqq in range(bb.shape[0] - 1):
                # x_axis.append(qqq)

                # determining whether an event falls between two particular time stamps, if so -> True, else False
                inRange = np.logical_and(fixedScale[:, -1] >= bb[qqq, 2], fixedScale[:, -1] < bb[qqq + 1, 2])
                # num_pulses.append(len(fixedScale[inRange]))

                # time difference between successive time stamps
                tDiffSync = (bb[qqq + 1, 2] - bb[qqq, 2]) / (bb[qqq + 1, 1] - bb[qqq, 1])
                # sync_pulses.append(tDiffSync)

                # ratio of expected time difference (syncPeriod) to actual difference between time stamps
                tScl = syncPeriod / tDiffSync

                # calculates the time since the first time pulse, i.e. syncPeriod * timestamp number
                tAdd = bb[qqq, 1] * syncPeriod
                # print(tAdd)

                # re-adjusts the time stamp relative to first time stamp, i.e. event timestamp - timeSync value + time since first pulse
                fixedScale[inRange, -1] = np.round((fixedScale[inRange, -1] - bb[qqq, 2]) * (syncPeriod / tDiffSync) + tAdd)

                # Progress bar
                # printProgressBar(qqq + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # plot(x_axis, sync_pulses, '.')
            # plot(np.arange(len(num_pulses)), num_pulses, '.')
            # xlabel('Measurement time (s)')
            # ylabel('Sync pulse difference (10ns)')
            # show()

            # adds events and syncs to existing numpy arrays (converts values into floats)
            syncs = np.concatenate((syncs, np.array(bb)))
            events = np.concatenate((events, fixedScale))
            # removes temp events array
            del fixedScale

    # removes first row from events (which was zero)
    events = np.array(events[1:, :])
    # converts numpy array into pandas dataFrame
    events = pd.DataFrame(data = events)
    # sort events based on time stamp
    events.sort_values(6, inplace = True)
    #events is npx,mdl,ene,x,y,z,t
    # converts panda dataFrame back into numpy array
    events = np.array(events)
    events[:, 2] = events[:, 2] / 1E6  # this sets energy to MeV
    # removes first row from syncs (which was zero)
    syncs = np.array(syncs[1:,:])
    """  Comment out in order to get all raw data converted to text
    # savings indices for events with energy below 2.7 MeV (why????)
    inds = events[:, 2] <= 2.7
    # filters out events with energy above 2.7 MeV
    events = events[inds, :]
    # savings indices for events with energy above 0.075 MeV (why????)
    #inds = events[:, 2] >= 0.075
    # replaced above statement with statement saves indices with energy above 0.075 MeV OR any energy for npx > 1
    inds = np.logical_or(events[:, 2] >= 0.075, np.logical_and(events[:, 2] < 0.075, events[:, 0] > 1))
    # filters out events with energy below 0.075 MeV only if 1x events
    events = events[inds, :]
    """
    # shifting detector number by 51 (back to 0, 1, 2, etc...)
    events[:, 1] -= 51
    # removing any events with 15 events or higher
    events = events[events[:, 0] < 15, :]


    print('\n  Writing combined data to ' + dataDir + 'AllEventsCombined.txt')
    print ('   - Number of interactions: {}'.format(len(events)))

    # Write the final results to file
    with open(dataDir + 'AllEventsCombined.txt', 'w') as write_file:
        l = len(events)
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
        for i, event in enumerate(events):

            # Progress bar
            if i % int(l/100) == 0:
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            write_line = "{:.0f}\t{:.0f}\t{:.1f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.0f}".format(
                event[0],
                event[1],
                event[2]*1e3,
                event[3]/1e3,
                event[4]/1e3,
                event[5]/1e3,
                event[6]
            )
            # print(write_line)
            write_file.write(write_line + "\n")
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar


    print("  Write complete.")


'''
ALLEVENTS.TXT READER AND WRITER FUNCTION
----------------------------------------
Takes in a path to an AllEvents.txt file and produces two numpy arrays:
The first is an array of all the events with the time stamps inserted into the data.
The second is an an array of just the time stamp data.
This function is useful for writing out the individual module data.
'''
def allevents_reader_print(dataDir, module_num):
    # Read AllEvents.txt into a DataFrame
    events_pd = pd.read_csv(dataDir + 'AllEvents.txt', sep = '	', header = None,
                          names = ['num_scatters', 'energy', 'x', 'y', 'z', 'time', 'event_num'],
                          usecols = ['num_scatters', 'energy', 'x', 'y', 'z', 'time'])#,
                          #dtype = {'num_scatters': np.uint64, 'energy': np.float64, 'x': np.float64, 'y': np.float64, 'z': np.float64, 'time': np.uint64, 'event_num': np.uint64})

    np.set_printoptions(linewidth = 1000, edgeitems = 15)
    # print(events_pd.to_numpy(dtype = np.uint64), '\n')
    events_pd = events_pd.fillna(method = 'bfill')              # add in missing times
    # print(events_pd.to_numpy(dtype = np.uint64), '\n')

    # Add blank columns to match expected format
    events_pd.insert(1, 'chip', 0)
    events_pd.insert(1, 'module_num', module_num)
    # events_pd.insert(0, 'npx', 0)

    # Read sync pulses into a DataFrame; NOTE: column names were chosen to match events df, not the actual sync pulse information
    sync_pd = pd.read_csv(dataDir + 'SyncPulse.txt', sep = '	', header = None,
                          names = ['module_num', 'time'],
                          dtype = {'module_num': np.uint64, 'time': np.float64})

    sync_pd.insert(0, 'num_scatters', 122)      # add column for module number

    # Add blank columns to match events df shape
    sync_pd.insert(len(sync_pd.columns)-1, 'chip', 0)
    sync_pd.insert(len(sync_pd.columns)-1, 'energy', 0)
    sync_pd.insert(len(sync_pd.columns)-1, 'x', 0)
    sync_pd.insert(len(sync_pd.columns)-1, 'y', 0)
    sync_pd.insert(len(sync_pd.columns)-1, 'z', 0)

    # print(events_pd)
    # print(sync_pd)

    # Add sync pulses to events df
    events_pd = pd.concat([events_pd, sync_pd])       # concatenate syncs onto events
    events_pd = events_pd.sort_values(by = ['time'])              # sort rows by the time

    events_arr = events_pd.to_numpy()               # get the data out as a numpy array

    sync_pd.insert(0, 'actual_module_num', module_num)     # insert the actual module number column
    sync_arr = sync_pd[['actual_module_num', 'module_num', 'time']].to_numpy()      # convert to numpy array

    # print(events_pd)

    return events_arr, sync_arr

'''
ALLEVENTS.TXT READER FUNCTION
-----------------------------
Takes in a path to an AllEvents.txt file and produces two numpy arrays:
The first is an array of all the events with the time stamps inserted into the data.
The second is an an array of just the time stamp data.
'''
def allevents_reader(dataDir, module_num):
    # Read AllEvents.txt into a DataFrame
    events_pd = pd.read_csv(dataDir + 'AllEvents.txt', sep = '	', header = None,
                          names = ['num_scatters', 'energy', 'x', 'y', 'z', 'time', 'event_num'],
                          usecols = ['num_scatters', 'energy', 'x', 'y', 'z', 'time'])#,
                          #dtype = {'num_scatters': np.uint64, 'energy': np.float64, 'x': np.float64, 'y': np.float64, 'z': np.float64, 'time': np.uint64, 'event_num': np.uint64})

    np.set_printoptions(linewidth = 1000, edgeitems = 15)
    # print(events_pd.to_numpy(dtype = np.uint64), '\n')
    events_pd = events_pd.fillna(method = 'bfill')              # add in missing times
    # print(events_pd.to_numpy(dtype = np.uint64), '\n')

    # Add blank columns to match expected format
    events_pd.insert(1, 'chip', 0)
    events_pd.insert(1, 'module_num', module_num)
    # events_pd.insert(0, 'npx', 0)

    events_arr = events_pd.to_numpy()               # get the data out as a numpy array

    # Read sync pulses into a DataFrame; NOTE: column names were chosen to match events df, not the actual sync pulse information
    sync_pd = pd.read_csv(dataDir + 'SyncPulse.txt', sep = '	', header = None,
                          names = ['module_num', 'time'],
                          dtype = {'module_num': np.uint64, 'time': np.float64})

    # sync_pd.insert(0, 'num_scatters', 122)      # add column for module number
    #
    # # Add blank columns to match events df shape
    # sync_pd.insert(len(sync_pd.columns)-1, 'chip', 0)
    # sync_pd.insert(len(sync_pd.columns)-1, 'energy', 0)
    # sync_pd.insert(len(sync_pd.columns)-1, 'x', 0)
    # sync_pd.insert(len(sync_pd.columns)-1, 'y', 0)
    # sync_pd.insert(len(sync_pd.columns)-1, 'z', 0)


    sync_pd.insert(0, 'actual_module_num', module_num)     # insert the actual module number column
    sync_arr = sync_pd[['actual_module_num', 'module_num', 'time']].to_numpy()      # convert to numpy array

    # print(events_pd)

    return events_arr, sync_arr

'''
ALLEVENTS.TXT MERGER FUNCTION
-----------------------------
Takes in the directory that the AllEvent.txt files are stored in and outputs an AllEventsCombined.txt file.
The AllEventsCombined.txt file is stored in the same directory as the .dat files.
Each line in the .txt file has the following format:
    Number of interactions
    Detector number
    Energy, 1 decimal place, (keV)
    x position (mm), 2 decimal places
    y position (mm), 2 decimal places
    z position (mm), 2 decimal places
    Time stamp (10*nanoseconds)
Where each value is separated by a tab (\t)
'''
def merge_allevents(dataDirs, write_mod_files, num_detectors):

    # UCT PolarisJ System
    # syncPeriod = 200000006 #this is how many clock incrememts each sync period should be - it's essentially the difference in timestamps for module 51, which for our configuration is the master

    # H3D Small Animal PET system
    # syncPeriod = 100502 #this is how many clock incrememts each sync period should be - it's essentially the difference in timestamps for module 51, which for our configuration is the master
    syncPeriod = 200000006 #this is how many clock incrememts each sync period should be - it's essentially the difference in timestamps for module 51, which for our configuration is the master

    events = np.zeros((1, 7))
    syncs = np.zeros((1, 3))

    dir_num = 0

    for iii in range(51, 51 + num_detectors):

        dataDir = dataDirs[dir_num]     # get the directory for the next module

        print('\n  Reading in data from ' + dataDir + 'AllEvents.txt...')
        test = dataDir + 'AllEvents.txt'
        print ('   - Size: {} bytes'.format(os.path.getsize(test)))

        if os.path.isfile(test):
            if write_mod_files:

                #  readCameraBinary2 returns: aa is the list of events (including timestamps), bb is the list of time stamps
                aa, bb = allevents_reader_print(dataDir, iii)

                # Write the individual module results to file
                print('  Writing data to ' + dataDir + 'mod%i.txt' % iii)
                print ('   - Number of interactions: {}'.format(len(aa)))
                #  save data (with timestamps) for each module as text file

                with open(dataDir + 'mod%i.txt' % iii, 'w') as write_file:
                    aa_events = []  # temp array to store only interaction data
                    l = len(aa)
                    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
                    for i, evt in enumerate(aa):
                        # Progress bar
                        if i % int(l/100) == 0:
                            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

                        if evt[0] == 122:
                            #  writing out time stamp data
                            write_line = "{:.0f}\t{:.0f}\t{:.0f}".format( evt[0], evt[1], evt[7] )
                        else:
                            #  writing out interaction data
                            write_line = "{:.0f}\t{}\t{}\t{}\t{}\t{:.0f}".format( evt[0], evt[4], evt[5], evt[6], evt[3], evt[7] )
                            #  only storing interaction data (without timestamps)
                            aa_events.append(evt)
                        write_file.write(write_line + "\n")

                    printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

                # convert interaction data (minus timestamps) into numpy array
                aa = np.array( aa_events )
            else:
                aa, bb = allevents_reader(dataDir, iii)


            # removes chip number from events array
            tempEvent = np.array( aa[:, (0, 1, 3, 4, 5, 6, 7)].copy())
            #note: I have the code set up so that I only use data that occurs between timestamps so I can do accurate time correction
            startInd = 0
            while bb[0, 2] > tempEvent[startInd, -1]:
                startInd += 1
            endInd = tempEvent.shape[0] - 1
            while bb[-1, 2] < tempEvent[endInd, -1]:
                endInd -= 1
            # truncates tempEvent array between first and last time stamps
            fixedScale = tempEvent[startInd:endInd, :].copy()
            # loops through time stamps [qqq is the index]
            l = bb.shape[0] - 1
            print('\nProcessing time stamp data')

            printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar

            for qqq in range(bb.shape[0] - 1):

                # determining whether an event falls between two particular time stamps, if so -> True, else False
                inRange = np.logical_and(fixedScale[:, -1] >= bb[qqq, 2], fixedScale[:, -1] < bb[qqq + 1, 2])
                # time difference between successive time stamps
                tDiffSync = (bb[qqq + 1, 2] - bb[qqq, 2]) / (bb[qqq + 1, 1] - bb[qqq, 1])
                # ratio of expected time difference (syncPeriod) to actual difference between time stamps
                tScl = syncPeriod / tDiffSync
                # calculates the time since the first time pulse, i.e. syncPeriod * timestamp number
                tAdd = bb[qqq, 1] * syncPeriod
                # re-adjusts the time stamp relative to first time stamp, i.e. event timestamp - timeSync value + time since first pulse
                fixedScale[inRange, -1] = np.round((fixedScale[inRange, -1] - bb[qqq, 2]) * (syncPeriod / tDiffSync) + tAdd)

                # Progress bar
                # if qqq % int(l/100) == 0:
                printProgressBar(qqq, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            # adds events and syncs to existing numpy arrays (converts values into floats)
            syncs = np.concatenate((syncs, np.array(bb)))
            events = np.concatenate((events, fixedScale))
            # removes temp events array
            del fixedScale

        dir_num += 1

        print('-------------------------------------------------------------------------------')

    # removes first row from events (which was zero)
    events = np.array(events[1:, :])
    # converts numpy array into pandas dataFrame
    events = pd.DataFrame(data = events)
    # sort events based on time stamp
    events.sort_values(6, inplace = True)
    #events is npx,mdl,ene,x,y,z,t
    # converts panda dataFrame back into numpy array
    events = np.array(events)
    # events[:, 2] = events[:, 2] / 1E6  # this sets energy to MeV
    # removes first row from syncs (which was zero)
    syncs = np.array(syncs[1:,:])
    """  Comment out in order to get all raw data converted to text
    # savings indices for events with energy below 2.7 MeV (why????)
    inds = events[:, 2] <= 2.7
    # filters out events with energy above 2.7 MeV
    events = events[inds, :]
    # savings indices for events with energy above 0.075 MeV (why????)
    #inds = events[:, 2] >= 0.075
    # replaced above statement with statement saves indices with energy above 0.075 MeV OR any energy for npx > 1
    inds = np.logical_or(events[:, 2] >= 0.075, np.logical_and(events[:, 2] < 0.075, events[:, 0] > 1))
    # filters out events with energy below 0.075 MeV only if 1x events
    events = events[inds, :]
    """
    # shifting detector number by 51 (back to 0, 1, 2, etc...)
    events[:, 1] -= 51
    # removing any events with 15 events or higher
    events = events[events[:, 0] < 15, :]


    print('\n  Writing combined data to ' + dataDir + 'AllEventsCombined.txt')
    print ('   - Number of interactions: {}'.format(len(events)))

    # Write the final results to file
    with open(dataDir + 'AllEventsCombined.txt', 'w') as write_file:
        l = len(events)
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)      # create progress bar
        for i, event in enumerate(events):

            # Progress bar
            if i % int(l/100) == 0:
                printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar

            write_line = "{:.0f}\t{:.0f}\t{:.1f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.0f}".format(
                event[0],
                event[1],
                event[2],
                event[3],
                event[4],
                event[5],
                event[6]
            )
            # print(write_line)
            write_file.write(write_line + "\n")
        printProgressBar(l, l, prefix = 'Progress:', suffix = 'Complete', length = 50)  # update progress bar


    print("  Write complete.")

    return

def main():

    # ----------------
    # Timing Debugging
    # ----------------
    #
    # data_file = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/mod51.dat'
    # data_file = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191128/P_191128-run1-na22_1uCi-centre-1hr/mod51.dat'
    #
    # all_events, time_stamps = preProcess.readCameraBinary2(data_file, 51)
    #
    # plot(time_stamps[:,1], time_stamps[:,2], 'b.')
    #
    # # data_file = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/mod52.dat'
    # data_file = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191128/P_191128-run1-na22_1uCi-centre-1hr/mod52.dat'
    #
    # all_events, time_stamps = preProcess.readCameraBinary2(data_file, 52)
    #
    # plot(time_stamps[:,1], time_stamps[:,2], 'r.')
    #
    # show()
    #
    # return


    # ------------------------------------------------------------------------------------------------------------------------------------------------


    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191125/P_191125-run1-na22_1uCi-centre-5min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191125/P_191125-run2-na22_1uCi-centre-60min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191126/P_191126-run1-na22_1uCi-centre-5min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/190805/190805-run2-na22_at_-1_0_11mm-oem2_x_-48mm-oem3_x_48mm-NOsubmm-10min/'/
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191127/P_191127-run1-na22_1uCi-centre-10min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191127/P_191127-run2-na22_1uCi-centre-subpxl-10min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191127/test/standard/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191128/P_191128-run1-na22_1uCi-centre-1hr/'
    # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/191203-run15-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    # data_dir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/200210/all_fuctioning_test/'
    # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/200213/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200213/200213-run9-na22-at_3_0_10mm-20min/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/200210/200210-run7-na22-at_0_0_10mm-10min-coincidence_debugging/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/191203-run14-ga68_at_0_0_11mm-oem2_x_-41mm-oem3_x_41mm-20min/'
    #
    # merge_dats(data_dir)
    #
    # return
    #
    # # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191203/'
    # # data_dir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/200213/'
    # data_dir = '/Volumes/Nicholas/Polaris/data/191203/'
    #
    # for subdir, dirs, files in os.walk(data_dir):
    #     for dir in dirs:
    #         # Ignore irrelevant directories
    #         if '191203' not in dir:
    #             continue
    #
    #         print('Running process on ', data_dir + dir)
    #         merge_dats(data_dir + dir + '/')
    #     break
    #
    # return

    # H3D Visit Data
    # ------------------------------------------------------------------------------------------------------------------------------------------
    #
    # # dataDir = '/Volumes/Nicholas/191108/'
    # dataDir = '/Volumes/Nicholas/Polaris/data/191115/'
    #
    # # # _syncPeriod = 100502
    # _syncPeriod = 200000006
    # # _syncPeriod = 199999860
    #
    # for Subdir, Dirs, Files in os.walk(dataDir):
    #     for data_dir in Dirs:
    #         if 'ignore' in data_dir:
    #             continue
    #         dataDirs = []
    #         for subdir, dirs, files in os.walk(dataDir + data_dir):
    #             for dir in dirs:
    #                 if dir[0] == 'b' or dir[0] == 't' or dir == 'ignore':
    #                     continue
    #                 dataDirs.append(dataDir + data_dir + '/' + dir + '/')
    #             break
    #         # print(dataDirs)
    #         dataDirs.sort(key = lambda path: path[path.find('PET') + 3])        # sort the data_dirs array by the module number
    #         # print(dataDirs)
    #         utils.merge_allevents(dataDirs, write_mod_files = False, num_detectors = 4, syncPeriod = _syncPeriod)
    #     break
    #
    # return

    # 191113
    # ------
    # dataDir = '/Users/nicholas/Library/Mobile Documents/com~apple~CloudDocs/Work/code/data/191113/P_191113-run1-na22_1uCi-x_7mm-y_7mm-z_-5.5mm_-2min/'

    # dataDir = '/Volumes/MEDICAL_SHARED/PGI/data/uct/polaris-streaming/191112/P_191112-run1-na22_1uCi_centre-5min/'
    # dataDir = '/Volumes/Nicholas/Polaris/data/191115/P_191115-run1-na22_1uCi-centre-no_sub-30min/'
    dataDir = '/Volumes/Nicholas/Polaris/data/191113/P_191113-run7-na22_1uCi-centre-30min/'

    dataDirs = []
    for subdir, dirs, files in os.walk(dataDir):
        for dir in dirs:
            if dir[0] == 'b' or dir[0] == 't':
                continue
            dataDirs.append(dataDir + dir + '/')
        break
    dataDirs.sort(key = lambda path: path[path.find('PET') + 3])        # sort the data_dirs array by the module number

    utils.merge_allevents(dataDirs, write_mod_files = False, num_detectors = 4, syncPeriod = 200000006)

    # ------------------------------------------------------------------------------------------------------------------------------------------

    return

if __name__ == '__main__':
    main()
