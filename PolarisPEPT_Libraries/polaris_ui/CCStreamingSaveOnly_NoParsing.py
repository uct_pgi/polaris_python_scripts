# -*- coding: utf-8 -*-
'''
developmental:
    Adapting Haijian's code to work with the UCT Polaris detectors. Currently works to
    read in the binary data and save the data. Added library to generate text files.

current:
    using a carbon444 alike computer with 40 cores, change to two processes per module,
    one for streaming, one for parsing. with 16 modules, there will be 32 processes, taking
    up 32 cores

    add logging

    disable real time time stamp interpolation/extrapolation. during the acquisition,
    only need to plot spectra so far, for which, time stamp is not needed.
    one thing didn't think about before is that, when updating master time stamp
    and index, it's better to add a lock to it.

CCStreaming.v.0.1.0:
    saving binary instead of txt.
    when acquring, interactions are appended to a binary string, when the string
    is longer than 8096, it is placed into the savingQ for writing to file.

    added acquring for a predetermined time period.
    added saveBEF

CCStreaming.v.0.0.9:
    the mp.Manager().Queue seems to be slow.  to get around this, change the
    control of the commara such that now there are 16 child processes, each
    process works on a module:
        there are three threads:
            one constantly streaming data;
            one parsing the data, updat spectra and put parsed data into a queue
            for saving if it is acquring, when acquring is stopped, an writeFileNowEvent
            signal is sent to writing thread to start writing data.
            one writing thread, it waits for an writing event signal from parsing
            thread to starting writing, when it's done, it changes writeFileDone
            to 1.

        with three threads, queue.Queue() is used instead of mp.Manager().Queue().
        queue.Queue() is faster.

    saving is changed to saving to a folder, and instead of the user selecting
    a file for storing the data, the user input a measurementName, the name is used
    as the folder name and data from each module is placed in this folder with
    file name 'modxx.dat', where 'xx' is the module number.

CCStreaming.v0.0.8:
    re-added spectra:
        now the spectra are updated while parsing in the parsing child process.
        the spectra are changed from PER MODULE to PER CHIP.


CCStreaming.v0.0.7:
    try to solve the problem of losing data in outputQ:
        the reason for losing data is because the two processes are terminated prematurely, if
        we let p1 and p2 run their course, without using .terminate() method, we will get a lot data
        in outputQ.
        this is most likely because all 12 parsing threads are trying to access the oututQ,
        causing a bad traffic.

    instead of one outputQ for all 16 module, now we use 16 pasedQ's for 16 modules.

    parsing thread took too long ( about 2 minuts ) to finsh after disconnect.
    2 minutes is too long a lag. need to speed it up.

    changed the structure:
        one process is dedicated to streaming--the process contains 16 threads,
        each streams data from one module, the streamed data is put to a streamingQ,
        the streamingQ is defined in the main process, so that it can be shared

        16 processes are spawn for parsing the streamed data.
        each process parses data from one module, and put parsed data into a parsedQ.
        the parsedQ is also defined in the main process.


CCStreaming.v0.0.6:
    move towards using camera,
    locally save streaming data to .dat file and parsed data to .txt file, to compare if there is any
    missing data.
    in disconnect function, changed to waite p1, p2 and pData processes to finish, otherwise,
    these processes may be terminated prematurely, causing data lost.
    A problem: when putting data into outputQ, a lot data is lost, mabe a probelm of  racing condition,
    where 16 parsing threads are trying to put pased data into the queue.

CCStreaming.v0.0.5:
    change number of processes for streaming and parsing to 2, the laptop is quardcore,
    has only 4 physical CPU, there are maybe 8 logical CPU, but it seems better to stick
    with physical cores:
        https://stackoverflow.com/questions/40217873/multiprocessing-use-only-the-physical-cores
    so now, there are two dedicated processes ( hopefully two cores ) for streaming and parsing
    the data from the camera;
    one dedicated process to generate spectra;
    one thread in the main process that writes the data to a file when saving;

    add menubar;
    add clear spectra button;

    in the main process, command socket sends reset index to each module, also sends start/stop
    to the master module
    in the two processes that read and parse data, data sockets streaming data
    added shared module status array, each element is set to one if connection to the module
    is a success
    added shared disconnect module array, when an element is set to one, indicates close streaming
    socket to that module

CCStreaming.V0.0.4:
    change start mode to spawn, in consistant with Windows.
    added save data to file.

CCStreaming.V0.0.3:
    make plot zooming stick
    terminate running processes on close ( if there are still running )

CCStreaming.V0.0.2:
    when saving disable save as file line editor and button, also disable timed acquisition
    added QTableView
    QTableView select row(s), and get row indecices.
    status if each module is connected
    added module number to threads and subprocesses
    added spectra for each module
    plot spectra for selected modules


CCStreaming.V0.0.1:
    expand to 4 child process as planned.
    generat more realistic data format.
    specProcess is renamed to dataProcess, it now put data into lists of   npx, mdl, edep, x, y, z and t

CCStreaming.V0.0.0:
    The inital setup of the working flow.
    The idea is that the main process has:
        4 child processes, each child process contains 4 threads getting data from 4 modules in
        camera and 4 threads parsing the data, the parsed data is put into a FIFO queue
        1 child process that put the data from the FIFO into (a combined) spectrum

    As a proof of concept, here there are only two child processes streaming and parsing data,
    and each child process there are only two threads.

CREATED BY HAIJIAN CHEN FOR STREAMING DATA OUT OF CC.
'''


from PySide2 import QtCore, QtGui, QtWidgets
from streamingUI import Ui_CCAcquireDataMainWindow
import sys, threading, queue, time, ctypes, struct, os, socket, select, datetime
import multiprocessing as mp
import numpy as np
import matplotlib, os
# matplotlib.use( 'Qt4Agg' )
# matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import urllib.request
import logging

# import processBinaryDataMerge as pbm
import combine_dat as comb_dat
# import matplotlib.pyplot as plt


try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig)

# for plotting spectra ( combined, or modular ), each energy bin is 1 keV.
# this will alow a max of 12 MeV
eDepMax = 12000

# total number of module
nModules = 2 #16

# bytearray length,
# nBa = 16384
nBa = 536870912   # 512M,      512M * 16 = 8.192G
# the number of bytes to receive
nBytes = 8192
# energy spectrum bin width:
specRes = 1000.0

# time between refresh spectrum plots
refreshSpecPlotPeriod = 1000

# time between refresh camera status table
refreshStatusTablePeriod = 1000

# default location of saving data:
# defaultDataFolder = '/Volumes/BATMAN/PGI/data/streaming/' # './data/'
# defaultDataFolder = '/home/user/Documents/data/191302/'
# defaultDataFolder = '/home/user/Documents/data/190228/'
# defaultDataFolder = '/home/user/Documents/data/190312/'
# defaultDataFolder = '/home/user/Documents/data/190424/'
# defaultDataFolder = '/home/user/Documents/data/190530/'
# defaultDataFolder = '/home/user/Documents/data/190612/'
defaultDataFolder = '/home/user/Documents/data/190805/'


# Settings for the merge and text file generation
# ---------------------------------------------------------------------------------------------------
MAX_EXP_INTER = 6       # the maximum number of interactions expected
DET_NUMS = [2,4]        # the number identifiers of the detectors
PRINT_STATS = 0         # print out the statistics of skipped bytes
SHOW_INTER = 1          # only show events with this many interactions or more
# ---------------------------------------------------------------------------------------------------


# this is the thread dedicated to streaming data from ONE module
class streamingProcess( mp.Process ):
    '''
    This is a process dedicated to stream data from a module specified by module number.

    input:
        moduleNumber:
            module number to identify which module to stream data from.
        moduleStatus:
            a shared array defined in main process, the length of
            the array equals to the number of modules. It is initilized to 0 when the main
            process starts.
            IN THIS THREAD, when a data socket is successfully established to
            the specified module, the corresponding element in moduleStatus is
            set to 1, so other processes/threads knows data connection to this
            module has been established.
        disconnectModule:
            also a shared array defined in main process. Initally, all elements
            are set 0 when the main process starts. It remains 0 until the
            disconnectCamera button is clicked, then the main process sets
            it elements to 1. It is passed from main proces to child streaming process.
            When streaming process sees the element corresponding to this module is set to 1,
            the streaming process closes the streaming data scocket and then terminate.
        streamingQ
            streamingQ is a queue shared between streaming and parsing processes for this module
            ( identified by the module number ), data get from the camera is pushed
            into this queue, and then the parsing process pops data from the queue.
    '''

    def __init__(   self, moduleNumber, moduleStatus, disconnectModule,
                    acquring, writeFileNowTrigger, savingFolderName, savingMode, writeFileDone ):
        super( streamingProcess, self).__init__()

        self.nm = '{} streaming process'.format( moduleNumber )
        self.loggerP = logging.getLogger( self.nm )
        self.loggerP.setLevel( logging.DEBUG )
        self.loggerP_hndlr = logging.StreamHandler()
        self.loggerP_hndlr.setLevel(logging.DEBUG)
        self.loggerP_fmtr = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.loggerP_hndlr.setFormatter( self.loggerP_fmtr )
        self.loggerP.addHandler( self.loggerP_hndlr )

        self.moduleNumber = moduleNumber
        self.moduleStatus = moduleStatus
        self.disconnectModule = disconnectModule

        # now try to connect to module specified by moduleNumber:
        self.sData = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

        # self.sData.settimeout( 5.0 )
        #self.moduleIP = '192.168.2.' + str( self.moduleNumber )
        self.moduleIP = '192.168.3.' + str( self.moduleNumber )

        # if the module is not currently connected and it is not asked to disconnect:
        if self.moduleStatus[ self.moduleNumber - 51 ] == 0 and self.disconnectModule[ self.moduleNumber - 51 ] == 0:
            try:
                self.sData.connect( (self.moduleIP, 11503) )
                self.moduleStatus[ self.moduleNumber - 51 ] = 1
                self.sData.setblocking(0)
                self.loggerP.info( 'data socket established' )
            except OSError:
                self.loggerP.critical( 'data socket failed to establish' )
                # print( 'module ', self.moduleNumber, ' failed to connect' )
                self.moduleStatus[ self.moduleNumber - 51 ] = 0

        else:
            # need to add some exceptions here:
            if self.moduleStatus[ self.moduleNumber - 51 ] != 0:
                self.loggerP.debug( 'the data socket already established')
            elif self.disconnectModule[ self.moduleNumber - 51 ] != 0:
                self.loggerP.debug( 'a disconnecting command is in process' )

            raise OSError('module already connected or disconnect module requested')

        # self.data = b''
        self.data = bytearray( nBytes )
        self.nRecv = 0

        self.bSave = bytearray( nBa )
        self.bLen = 0

        self.acquring = acquring
        self.writeFileNowTrigger = writeFileNowTrigger
        self.savingFolderName = savingFolderName
        self.savingMode = savingMode
        self.writeFileDone = writeFileDone

        self.savingFileName = None
        self.fid = None



        # self.fid = open( './data/' + str(self.moduleNumber) + '.dat', 'wb' )
        # self.dataSave = b''


    def run( self ):
        # keep streaming as long as the not disconnectCamera button is not clicked:
        while self.disconnectModule[ self.moduleNumber - 51 ] == 0:
            # self.data = b''
            try:
                ready = select.select( [self.sData], [], [] )
                if ready[0]:
                    # self.data = self.sData.recv( nBytes )
                    # self.streamingQ.put( self.data )
                    self.nRecv = self.sData.recv_into( self.data )

                    if self.acquring.value and self.writeFileNowTrigger[self.moduleNumber - 51] == 0:
                        self.bSave[self.bLen:self.bLen+self.nRecv] = self.data[:self.nRecv]
                        self.bLen += self.nRecv

                    elif self.writeFileNowTrigger[self.moduleNumber - 51] != 0:
                        self.savingFileName = self.savingFolderName.value.decode() + 'mod' + str( self.moduleNumber ) + '.dat'
                        if self.savingMode.value.decode() == 'new':
                            self.fid = open( self.savingFileName, 'wb' )
                        elif self.savingMode.value.decode() == 'append':
                            self.fid = open( self.savingFileName, 'ab' )

                        self.fid.write( self.bSave[:self.bLen] )
                        self.fid.close()

                        self.bLen = 0
                        self.writeFileDone[ self.moduleNumber - 51 ] = 1
                        self.writeFileNowTrigger[self.moduleNumber - 51] = 0


            except socket.timeout:
                print( 'module ', self.moduleNumber, 'receving timed out' )
                break

        # when disconnectCamera button is clicked, close the data socket and
        # change moduleStatus:
        self.sData.close()
        self.moduleStatus[ self.moduleNumber - 51 ] = 0

        self.loggerP.info( ' streaming process finshed' )
        # self.fid.write( self.dataSave )
        # self.fid.close()




# class parsingProcess( mp.Process ):
#     '''
#     This is the process that parses data and place the data into a savingQ when
#     we want to saving data.
#     This is the second thread of the module child process.
#
#     input:
#         moduleNumber:
#             specifies which module, this must be the same as that in the streaming
#             thread for the same module child process
#         disconnectModule:
#             this is the 16-element shared array, defined in the main process, pass
#             to module child process and this parsing thread. It is initialized to 0
#             in the main process and remains 0 until disconnectCamera button is
#             clicked, then the main process set each element to 1 and child processes
#             can pick it up and see a disconnectCamera is issued, here the parsing
#             thread will terminate when element in disconnectModule corresponding
#             to this module is set to 1
#         streamingQ:
#             this is the queue defined by this module child process, where the data
#             from streamingQ is placed.
#         savingQ:
#             this is the queue defined by this module child proces, when the acquring
#             is True, the parsed data is pushed into this savingQ for the writing
#             thread to write it out
#         specChip0, specChip1, specChip2, specChip3, spec:
#             there are the spectra for chip 0, 1, 2, 3 and the module
#         acquring:
#             a shared value defined in the main process. It is set to True when
#             startAcquisitionButton is clicked in the main process so that all
#             child module prossses knows an acquisiton has been started; it is
#             reset to False when stopAcquisition button clicked.
#         writeFileNowTrigger:
#             also a 16-element shared array, when stopAcquisition is cliked, the
#             acquring is set to False, but it only stops pushing data into savingQ.
#             writeFileNowTrigger is set to 1 at the time stopAcquisition is clicked,
#             when the parsing thread sees it, it will trigger a writeFileNowEvent
#             to wake up the writing thread to write the file out
#         writeFileNowEvent:
#             this is the event between the parsing and the writing threads.
#         syncIndexMaster0, syncIndexMaster1, syncIndexMaster1, syncTimeMaster1:
#             these are used for interpolate the time stamp according the master
#             module.
#     '''
#
#     def __init__(   self,moduleNumber, disconnectModule, streamingQ,
#                     acquring, writeFileNowTrigger, savingFolderName, savingMode, writeFileDone
#                     ):
#         super( parsingProcess, self).__init__()
#
#
#         self.nm = '{} parsing process'.format( moduleNumber )
#         self.loggerP = logging.getLogger( self.nm )
#         self.loggerP.setLevel( logging.DEBUG )
#         self.loggerP_hndlr = logging.StreamHandler()
#         self.loggerP_hndlr.setLevel(logging.DEBUG)
#         self.loggerP_fmtr = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#         self.loggerP_hndlr.setFormatter( self.loggerP_fmtr )
#         self.loggerP.addHandler( self.loggerP_hndlr )
#
#
#         self.moduleNumber = moduleNumber
#         self.disconnectModule = disconnectModule
#         self.streamingQ = streamingQ
#
#         self.acquring = acquring
#         self.writeFileNowTrigger = writeFileNowTrigger
#         self.savingFolderName = savingFolderName
#         self.savingMode = savingMode
#         self.writeFileDone = writeFileDone
#
#         self.savingFileName = None
#         self.fid = None
#
#         self.b = b''            # this is for data popped from streamingQ
#
#         # define a 512 M buffer
#         self.bSave = bytearray( nBa )
#         self.bLen = 0
#
#         self.loggerP.info( 'parsing process started' )
#
#     def run( self ):
#
#         # keep going as long as disconnectCamera is not clicked:
#         while self.disconnectModule[ self.moduleNumber - 51 ] == 0:
#
#             self.b = self.streamingQ.get(block=True, timeout=10)
#
#             if self.acquring.value and self.writeFileNowTrigger[self.moduleNumber - 51] == 0:
#                 self.bSave[self.bLen:self.bLen+len( self.b)] = self.b
#                 self.bLen += len( self.b )
#
#             elif self.writeFileNowTrigger[self.moduleNumber - 51] != 0:
#                     self.savingFileName = self.savingFolderName.value.decode() + 'mod' + str( self.moduleNumber ) + '.dat'
#                     if self.savingMode.value.decode() == 'new':
#                         self.fid = open( self.savingFileName, 'wb' )
#                     elif self.savingMode.value.decode() == 'append':
#                         self.fid = open( self.savingFileName, 'ab' )
#
#                     self.fid.write( self.bSave[:self.bLen] )
#                     self.fid.close()
#
#                     self.bLen = 0
#                     self.writeFileDone[ self.moduleNumber - 51 ] = 1
#                     self.writeFileNowTrigger[self.moduleNumber - 51] = 0
#
#         # disconnectCamera is clicked and detected, there
#         # might still be some data in the streamingQ, get them all and keep parsing:
#         for i in range( self.streamingQ.qsize() ):
#             self.b = self.streamingQ.get( )
#
#         self.loggerP.info( ' parsing process finshed')


# class writingThread( threading.Thread ):
#     '''
#     This is thread that takes the data pushed into savingQ and write them into a
#     file, the file is named as 'modxx.dat', where 'xx' is the module number
#     This is the last thread in the module child process
#
#     input:
#         disconnectModule:
#             this is the 16-element shared array to indicate if the disconnectCamera
#             button has been clicked. When True, the thread quit the while look
#             and thread proceed to terminate
#         savingFolderName:
#             folder name of where to save the data. Folder name is the measurement
#             name;
#         savingMode:
#             when the measurement name already exists, determine if overwrite or
#             append with new data
#         moduleNumber:
#             module number, identifies which module's data to write, MUST be the same
#             for the streaming and parsing thread.
#         savingQ:
#             queue defined in the module child process, this is where the parsing
#             thread pushes data into when acquiring new data
#         writeFileNowEvent:
#             an event to awaken the writing thread. The writing thread is waiting
#             for this event to be set in the parsing thread. Once the it is set,
#             file is written out.
#         writeFileDone:
#             shared array, only when all elements in writeFileDone are 0, a new
#             acquisition could start.
#
#     '''
#     def __init__(   self, moduleNumber, disconnectModule, savingFolderName, savingMode, savingQ,
#                     writeFileNowEvent, writeFileDone ):
#         super(writingThread, self).__init__()
#         self.disconnectModule = disconnectModule
#         self.savingFolderName = savingFolderName
#         self.savingMode = savingMode
#         self.moduleNumber = moduleNumber
#         self.savingQ = savingQ
#         self.writeFileNowEvent = writeFileNowEvent
#         self.writeFileDone = writeFileDone
#         self.savingFileName = None
#         self.fid = None
#
#
#     def run( self ):
#         while True:
#             self.writeFileNowEvent.wait()
#             self.savingFileName = self.savingFolderName.value.decode() + 'mod' + str( self.moduleNumber ) + '.dat'
#             # print( self.moduleNumber, 'writing event set received, awakening at', time.time() )
#
#             # when disconnectCamera is clikced, quiting, this would allow parsing thread to quit:
#             if self.disconnectModule[ self.moduleNumber - 51 ] != 0:
#                 self.writeFileNowEvent.clear()
#                 break
#             if self.savingMode.value.decode() == 'new':
#                 self.fid = open( self.savingFileName, 'wb' )
#             elif self.savingMode.value.decode() == 'append':
#                 self.fid = open( self.savingFileName, 'ab' )
#
#             # just get and write. if get and stitch into one long array, it's slower:
#             for i in range( self.savingQ.qsize() ):
#                 self.fid.write( self.savingQ.get() )
#             self.fid.close()
#             # self.bSave = b''
#             # print( self.moduleNumber, 'writing file end at', time.time() )
#             self.writeFileDone[ self.moduleNumber - 51 ] = 1
#             self.writeFileNowEvent.clear()


# def streamingModuleProcess( moduleNumber, moduleStatus, disconnectModule, acquring,
#                             specChip0, specChip1, specChip2, specChip3, spec,
#                             syncIndexMaster0, syncTimeMaster0, syncIndexMaster1, syncTimeMaster1,
#                             savingFolderName, savingMode, writeFileNowTrigger, writeFileDone ):
#
#     streamingQ = queue.Queue()
#     savingQ = queue.Queue()
#     writefileNowEvent = threading.Event()
#
#     sThread = streamingThread(  moduleNumber, moduleStatus, disconnectModule, streamingQ )
#
#     pThread = parsingThread(    moduleNumber, disconnectModule, streamingQ, savingQ,
#                                 specChip0, specChip1, specChip2, specChip3, spec,
#                                 acquring, writeFileNowTrigger, writefileNowEvent,
#                                 syncIndexMaster0, syncTimeMaster0, syncIndexMaster1, syncTimeMaster1 )
#
#     wThread = writingThread( disconnectModule, savingFolderName, savingMode, moduleNumber, savingQ, writefileNowEvent, writeFileDone )
#
#     sThread.start()
#     pThread.start()
#     wThread.start()
#     sThread.join()
#     pThread.join()
#     wThread.join()
#     print( moduleNumber, 'process ends', time.time()  )


# class watchDogProcess( mp.Process ):
#     '''
#     This process is intent to be used for checking various status for problems
#     '''
#     def __init__( self, moduleStatus ):
#         super( watchDogProcess, self ).__init__()
#         self.moduleStatus = moduleStatus
#
#     def run( self ):
#         while True:
#             for iModule in range( nModules ):
#                 if self.moduleStatus[iModule] == 0:
#                     print( 'module ', iModule, ' lost connection')


class CCAcquireData(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        super(CCAcquireData, self).__init__()

        self.ccLogger = logging.getLogger( name='CCAcquireData' )
        self.ccLogger.setLevel( logging.DEBUG )
        self.ccLogger_hndlr = logging.StreamHandler()
        self.ccLogger_hndlr.setLevel( logging.DEBUG )
        self.ccLogger_hndlr.setFormatter( logging.Formatter( '%(asctime)s - %(name)s - %(levelname)s - %(message)s') )
        self.ccLogger.addHandler( self.ccLogger_hndlr )
        self.ccLogger.info( 'program started' )

        # initialize the GUI
        self.ui = Ui_CCAcquireDataMainWindow()
        self.ui.setupUi( self )

        # by default, do not save to USB
        self.ui.save2UsbCheckBox.setChecked(False)

        # in the beginning, set text color to green on connect to camera button
        # and start acquisition button
        self.ui.connectCCButton.setStyleSheet('QPushButton {color: green;}')
        self.ui.actionDisconnect.setEnabled( False )
        self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: green;}')
        self.ui.startacquisitionButton.setEnabled( False )


        self.ui.clearSpectraButton.setEnabled( False )

        # module number for master
        self.master = 51

        # a shared array.
        # when request to connect to camera, two processes are forked for each
        # module, one for streaming the data, the other for parsing the data
        # the elements of the array is set to 1 if the streaming process
        # established the streaming socket successfully. otherwise 0.
        self.moduleStatus = mp.Array( 'i', nModules, lock=True )

        # a shared array.
        # the streaming and parsing processes constantly monitor its value.
        # when the element is set 1, the streaming process will close the socket
        # and then finish; the parsing process will parse the remaining data
        # in queue and then finish
        self.disconnectModule =  mp.Array( 'i', nModules, lock=True )

        # to have the capability of reconnecting the camera, need to know if
        # we are connecting for the first time or we disconnected previously
        # and now are trying to reconnect.
        self.alreadyDisconnected = False

        # a shared array
        # the parsing process for each module will push data into a queue
        # during acquisiton, until the acquisiton is stopped, this array is used
        # to singal the parsing process to set a writing file event to the
        # writing thread.
        self.writeFileNowTrigger = mp.Array( 'i', nModules, lock=True )
        # for each module, when the data file is done written, need to send a message
        # back to the main process
        self.writeFileDone = mp.Array( 'i', nModules, lock=True )

        for i in range( nModules ):
            self.moduleStatus[i] = 0
            self.disconnectModule[i] = 0
            self.writeFileNowTrigger[i] = 0
            self.writeFileDone[i] = 0

        # booleans for if the camera is connected, it is True if all connections
        # to modules are established, i.e., self.mdlConnected.sum()=16
        self.CCConnected = False

        # for saving files
        self.savingFolderName = mp.Array(ctypes.c_char, 256 )
        self.savingFolderName.value = './data/temp/'.encode()
        self.savingMode = mp.Array(ctypes.c_char, 10 )
        self.savingMode.value = 'new'.encode()
        # self.saveFileFilter = None
        # self.saveFileID = None

        # boolean for if it's currently acquring data, i.e., saving data
        self.acquring = mp.Value( ctypes.c_bool, False )

        self.acqTime = 60000

        self.shutDownSent = False
        # sync pulse from the master, they need to be shared among all other
        # modules:
        # self.syncIndexMaster0 = mp.Value( ctypes.c_uint64, 0 )
        # self.syncTimeMaster0 = mp.Value( ctypes.c_uint64, 0 )
        # self.syncIndexMaster1 = mp.Value( ctypes.c_uint64, 0 )
        # self.syncTimeMaster1 = mp.Value( ctypes.c_uint64, 0 )

        # define a multiprocessing shared memory for spectra:
        # also need their numpy form so that we can add the module spectra
        # to get the combined spectrum.
        # the combined spectrum is not updated in the per module process
        # to avoid race condition
        self.specCombined = mp.Array( 'L', eDepMax, lock=True )
        self.specCombinedNp = np.ctypeslib.as_array( self.specCombined.get_obj() )

        self.specMdl0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl0Np = np.ctypeslib.as_array( self.specMdl0.get_obj() )
        self.specMdl0Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl0Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl0Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl0Chip3 = mp.Array( 'L', eDepMax, lock=True )


        self.specMdl1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl1Np = np.ctypeslib.as_array( self.specMdl1.get_obj() )
        self.specMdl1Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl1Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl1Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl1Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl2Np = np.ctypeslib.as_array( self.specMdl2.get_obj() )
        self.specMdl2Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl2Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl2Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl2Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl3 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl3Np = np.ctypeslib.as_array( self.specMdl3.get_obj() )
        self.specMdl3Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl3Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl3Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl3Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl4 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl4Np = np.ctypeslib.as_array( self.specMdl4.get_obj() )
        self.specMdl4Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl4Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl4Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl4Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl5 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl5Np = np.ctypeslib.as_array( self.specMdl5.get_obj() )
        self.specMdl5Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl5Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl5Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl5Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl6 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl6Np = np.ctypeslib.as_array( self.specMdl6.get_obj() )
        self.specMdl6Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl6Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl6Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl6Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl7 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl7Np = np.ctypeslib.as_array( self.specMdl7.get_obj() )
        self.specMdl7Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl7Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl7Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl7Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl8 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl8Np = np.ctypeslib.as_array( self.specMdl8.get_obj() )
        self.specMdl8Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl8Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl8Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl8Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl9 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl9Np = np.ctypeslib.as_array( self.specMdl9.get_obj() )
        self.specMdl9Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl9Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl9Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl9Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl10 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl10Np = np.ctypeslib.as_array( self.specMdl10.get_obj() )
        self.specMdl10Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl10Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl10Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl10Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl11 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl11Np = np.ctypeslib.as_array( self.specMdl11.get_obj() )
        self.specMdl11Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl11Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl11Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl11Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl12 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl12Np = np.ctypeslib.as_array( self.specMdl12.get_obj() )
        self.specMdl12Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl12Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl12Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl12Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl13 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl13Np = np.ctypeslib.as_array( self.specMdl13.get_obj() )
        self.specMdl13Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl13Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl13Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl13Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl14 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl14Np = np.ctypeslib.as_array( self.specMdl14.get_obj() )
        self.specMdl14Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl14Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl14Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl14Chip3 = mp.Array( 'L', eDepMax, lock=True )

        self.specMdl15 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl15Np = np.ctypeslib.as_array( self.specMdl15.get_obj() )
        self.specMdl15Chip0 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl15Chip1 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl15Chip2 = mp.Array( 'L', eDepMax, lock=True )
        self.specMdl15Chip3 = mp.Array( 'L', eDepMax, lock=True )

        # setup status table
        # self.tableColumnHeader = ['status', 'count rate', 'dead time', 'disc full', 'temp' ]
        self.tableColumnHeader = ['status', 'count rate', 'dead time', 'disc full', 'temp' , 'humidity' ]
        self.tableRowHeader = [ str( i ) for i in range( 51, 67) ]
        self.tableModel = QtGui.QStandardItemModel( len( self.tableRowHeader ), len( self.tableColumnHeader ) )
        self.tableModel.setHorizontalHeaderLabels( self.tableColumnHeader )
        self.tableModel.setVerticalHeaderLabels( self.tableRowHeader )
        # initialize the table
        for i in range( len( self.tableRowHeader ) ):
            item = QtGui.QStandardItem( 'N')
            self.tableModel.setItem( i, 0, item )
            for j in range( 1, len(self.tableColumnHeader ) ):
                item = QtGui.QStandardItem('0' )
                self.tableModel.setItem( i, j, item )
        self.ui.CCStatusTableView.setModel( self.tableModel )
        # set when click on the table, the entire row got selected.
        self.ui.CCStatusTableView.setSelectionBehavior( QtWidgets.QAbstractItemView.SelectRows )
        self.ui.CCStatusTableView.setSortingEnabled( False )
        # tableSelectionModel is need to void a segmentation fault:
        self.tableSelectionModel = self.ui.CCStatusTableView.selectionModel()
        # define a list for selected row indices
        self.tableSelectedRowIndices = []

        # the list for ip address/module
        # self.moduleNumbers = [51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66]
        # self.moduleNumbers = [51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 65, 66]
        self.moduleNumbers = [51, 52]

        # define multiprocessing queues for put streaming data.
        # self.mdl0StreamingQ = mp.Queue()
        # self.mdl1StreamingQ = mp.Queue()
        # self.mdl2StreamingQ = mp.Queue()
        # self.mdl3StreamingQ = mp.Queue()
        # self.mdl4StreamingQ = mp.Queue()
        # self.mdl5StreamingQ = mp.Queue()
        # self.mdl6StreamingQ = mp.Queue()
        # self.mdl7StreamingQ = mp.Queue()
        # self.mdl8StreamingQ = mp.Queue()
        # self.mdl9StreamingQ = mp.Queue()
        # self.mdl10StreamingQ = mp.Queue()
        # self.mdl11StreamingQ = mp.Queue()
        # self.mdl12StreamingQ = mp.Queue()
        # self.mdl13StreamingQ = mp.Queue()
        # self.mdl14StreamingQ = mp.Queue()
        # self.mdl15StreamingQ = mp.Queue()

        self.mdl0mdl0StreamingProc = None
        self.mdl1mdl0StreamingProc = None
        self.mdl2mdl0StreamingProc = None
        self.mdl3mdl0StreamingProc = None
        self.mdl4mdl0StreamingProc = None
        self.mdl5mdl0StreamingProc = None
        self.mdl6mdl0StreamingProc = None
        self.mdl7mdl0StreamingProc = None
        self.mdl8mdl0StreamingProc = None
        self.mdl9mdl0StreamingProc = None
        self.mdl10mdl0StreamingProc = None
        self.mdl11mdl0StreamingProc = None
        self.mdl12mdl0StreamingProc = None
        self.mdl13mdl0StreamingProc = None
        self.mdl14mdl0StreamingProc = None
        self.mdl15mdl0StreamingProc = None

        # self.mdl0mdl0ParsingProc = None
        # self.mdl1mdl0ParsingProc = None
        # self.mdl2mdl0ParsingProc = None
        # self.mdl3mdl0ParsingProc = None
        # self.mdl4mdl0ParsingProc = None
        # self.mdl5mdl0ParsingProc = None
        # self.mdl6mdl0ParsingProc = None
        # self.mdl7mdl0ParsingProc = None
        # self.mdl8mdl0ParsingProc = None
        # self.mdl9mdl0ParsingProc = None
        # self.mdl10mdl0ParsingProc = None
        # self.mdl11mdl0ParsingProc = None
        # self.mdl12mdl0ParsingProc = None
        # self.mdl13mdl0ParsingProc = None
        # self.mdl14mdl0ParsingProc = None
        # self.mdl15mdl0ParsingProc = None
        #



        # define a command socket, this is used for reseting sync pulse index
        # and start/stop sync pulse, start/stop saving BEF on the USB drive.
        self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

        # setup combined/per module spec plot:
        self.dpi = 100

        self.figCombine = Figure((4, 3), dpi=self.dpi)
        self.axesCombine = self.figCombine.add_subplot(111)
        # self.axesCombine.set_xlim( self.xlim )
        # self.axesCombine.set_autoscaley_on(True)
        self.canvasCombine = FigureCanvas( self.figCombine )
        self.mplToolbarCombine = NavigationToolbar(self.canvasCombine, self.ui.combinedSpecTab)
        self.vBoxLayoutCombinedSpecTab = QtWidgets.QVBoxLayout(self.ui.combinedSpecTab )
        self.vBoxLayoutCombinedSpecTab.addWidget(self.mplToolbarCombine)
        self.vBoxLayoutCombinedSpecTab.addWidget(self.canvasCombine, stretch=1)
        self.figCombine.tight_layout()

        self.figMdl = Figure((4, 3), dpi=self.dpi)
        self.axesMdl = self.figMdl.add_subplot(111)
        self.canvasMdl = FigureCanvas( self.figMdl )
        self.mplToolbarMdl = NavigationToolbar(self.canvasMdl, self.ui.selectedModuleSpecTab)
        self.vBoxLayoutMdlSpecTab = QtWidgets.QVBoxLayout(self.ui.selectedModuleSpecTab )
        self.vBoxLayoutMdlSpecTab.addWidget(self.mplToolbarMdl)
        self.vBoxLayoutMdlSpecTab.addWidget(self.canvasMdl, stretch=1)
        self.figMdl.tight_layout()

        self.xAxisLim = None
        self.yAxisLim = None
        self.xAxisLimSet = False
        self.yAxisLimSet = False

        # setup per chip spec plot:

        self.figChip = Figure((4, 3), dpi=self.dpi)
        self.axesChip0 = self.figChip.add_subplot(221)
        self.axesChip1 = self.figChip.add_subplot(222)
        self.axesChip2 = self.figChip.add_subplot(223)
        self.axesChip3 = self.figChip.add_subplot(224)
        self.canvasChip = FigureCanvas( self.figChip )
        self.mplToolbarChip = NavigationToolbar(self.canvasChip, self.ui.perChipSpecTab )
        self.vBoxLayoutPerChipSpecTab = QtWidgets.QVBoxLayout(self.ui.perChipSpecTab )
        self.vBoxLayoutPerChipSpecTab.addWidget(self.mplToolbarChip)
        self.vBoxLayoutPerChipSpecTab.addWidget(self.canvasChip)

        self.xAxisLimChip0 = None
        self.yAxisLimChip0 = None
        self.xAxisLimSetChip0 = False
        self.yAxisLimSetChip0 = False
        self.xAxisLimChip1 = None
        self.yAxisLimChip1 = None
        self.xAxisLimSetChip1 = False
        self.yAxisLimSetChip1 = False
        self.xAxisLimChip2 = None
        self.yAxisLimChip2 = None
        self.xAxisLimSetChip2 = False
        self.yAxisLimSetChip2 = False
        self.xAxisLimChip3 = None
        self.yAxisLimChip3 = None
        self.xAxisLimSetChip3 = False
        self.yAxisLimSetChip3 = False

        # a messageBox used for display various yes/no messages:
        self.messageBox = QtWidgets.QMessageBox()

        # QTimer for triggering re-plot spectrum and update the status table
        self.updatePlotTimer = QtCore.QTimer()
        self.updateStatusTableTimer = QtCore.QTimer()
        self.autoStopTimer = QtCore.QTimer()


        # signal/slot functions:
        self.updatePlotTimer.timeout.connect( self.plotSpec )
        self.updateStatusTableTimer.timeout.connect( self.updateStatusTable )
        # self.ui.saveAsPushButton.clicked.connect( self.onSaveAsPushButtonClicked )
        # self.ui.saveAsLineEdit.returnPressed.connect( self.onSaveAsLineEditReturnPressed )
        self.ui.measurementNameLineEdit.returnPressed.connect( self.onMeasurementNameLineEditReturnPressed )
        self.ui.save2UsbCheckBox.stateChanged.connect( self.onSave2UsbCheckBoxChanged )
        self.ui.timedAcquisitionCheckBox.stateChanged.connect( self.onTimeAcquisitionCheckBoxChanged )
        self.ui.connectCCButton.clicked.connect( self.onConnectCCButtonClicked )
        self.ui.actionConnect.triggered.connect( self.onConnectCCButtonClicked )
        self.ui.actionDisconnect.triggered.connect( self.onConnectCCButtonClicked )
        self.ui.actionShutdown.triggered.connect( self.onShutdownTriggered )
        self.ui.startacquisitionButton.clicked.connect( self.onStartAcquisitionButtonClicked )
        self.ui.clearSpectraButton.clicked.connect( self.onClearSpectraButtonClicked )
        self.tableSelectionModel.selectionChanged.connect( self.onCCStatusTableViewSelectionChanged )

        self.autoStopTimer.timeout.connect( self.onStartAcquisitionButtonClicked )

    def plotSpec( self ):
        # the first tab (index=0) plots the spectrum combines data from all modules
        if self.ui.plottingTab.currentIndex() == 0:
            self.specCombined = self.specMdl0Np + self.specMdl1Np + self.specMdl2Np + self.specMdl3Np + \
                                self.specMdl4Np + self.specMdl5Np + self.specMdl6Np + self.specMdl7Np + \
                                self.specMdl8Np + self.specMdl9Np + self.specMdl10Np + self.specMdl11Np + \
                                self.specMdl12Np + self.specMdl13Np + self.specMdl14Np + self.specMdl15Np

            # when user zoom on the plotted spectrum, the get_autoscalex_on would
            # return False, indicating the user want to change plotted region,
            # so we need to keep this wanted plotted region in the future plotting.
            self.xAxisLimSet = not self.axesCombine.get_autoscalex_on()
            # usually, get_autoscalex_on() and get_autoscaley_on return the same
            # it's hard to zoom using mouse and only zoom in x withouth any change
            # in y direction, so, only use x to judge if plotting region changed.
            # self.yAxisLimSet = not self.axesCombine.get_autoscaley_on()

            # find the x and y axes limits, if the user zoomed to the left
            # of the spectrum begining, or if the user zoomed to the right of the
            # spectrum ending, change plotting the left edge to spectrum begining
            # (eDep=0) or right to the spectrum ending( eDep=eDepMax)
            if self.xAxisLimSet:
                self.xAxisLim = self.axesCombine.get_xlim()
                xLeftInd = 0 if self.xAxisLim[0] < 0 else int( self.xAxisLim[0] )
                xRightInd = eDepMax if self.xAxisLim[1] > (eDepMax - 1) else int( self.xAxisLim[1] )
                yMin = min( self.specCombined[ xLeftInd:xRightInd ] )
                yMax = max( self.specCombined[ xLeftInd:xRightInd ] ) * 1.1
                self.yAxisLim = ( yMin, yMax )

            self.axesCombine.clear()
            self.axesCombine.plot( self.specCombined )
            if self.xAxisLimSet:
                # when user zoomed to a region where there is no counts at all,
                # i.e., zero every where, need to reset plot entire spectrum
                # otherwise, error.
                if yMax == 0:
                    self.axesCombine.set_autoscalex_on( True )
                else:
                    self.axesCombine.set_xlim( self.xAxisLim )
                    self.axesCombine.set_ylim( self.yAxisLim )
            self.canvasCombine.draw()

        # if the index==1, then it is plotting spectra from selected module.
        elif self.ui.plottingTab.currentIndex() == 1:
            if len( self.tableSelectedRowIndices ) > 0:
                self.xAxisLimSet = not self.axesMdl.get_autoscalex_on()
                if self.xAxisLimSet:
                    self.xAxisLim = self.axesMdl.get_xlim()
                    xLeftInd = 0 if self.xAxisLim[0] < 0 else int( self.xAxisLim[0] )
                    xRightInd = eDepMax if self.xAxisLim[1] > (eDepMax - 1) else int( self.xAxisLim[1] )
                    yMin=1e6
                    yMax=-1e6
                    self.yAxisLim = ( yMin, yMax )
                self.axesMdl.clear()

                # check each module to see if it is selected and find the y
                # axis limits
                for i in range( len( self.tableSelectedRowIndices ) ):
                    if self.tableSelectedRowIndices[i] == 0:
                        if self.xAxisLimSet:
                            if min( self.specMdl0[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl0[ xLeftInd:xRightInd ] )
                            if max( self.specMdl0[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl0[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl0 )

                    elif self.tableSelectedRowIndices[i] == 1:
                        if self.xAxisLimSet:
                            if min( self.specMdl1[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl1[ xLeftInd:xRightInd ] )
                            if max( self.specMdl1[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl1[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl1 )

                    elif self.tableSelectedRowIndices[i] == 2:
                        if self.xAxisLimSet:
                            if min( self.specMdl2[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl2[ xLeftInd:xRightInd ] )
                            if max( self.specMdl2[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl2[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl2 )

                    elif self.tableSelectedRowIndices[i] == 3:
                        if self.xAxisLimSet:
                            if min( self.specMdl3[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl3[ xLeftInd:xRightInd ] )
                            if max( self.specMdl3[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl3[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl3 )

                    elif self.tableSelectedRowIndices[i] == 4:
                        if self.xAxisLimSet:
                            if min( self.specMdl4[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl4[ xLeftInd:xRightInd ] )
                            if max( self.specMdl4[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl4[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl4 )

                    elif self.tableSelectedRowIndices[i] == 5:
                        if self.xAxisLimSet:
                            if min( self.specMdl5[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl5[ xLeftInd:xRightInd ] )
                            if max( self.specMdl5[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl5[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl5 )

                    elif self.tableSelectedRowIndices[i] == 6:
                        if self.xAxisLimSet:
                            if min( self.specMdl6[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl6[ xLeftInd:xRightInd ] )
                            if max( self.specMdl6[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl6[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl6 )

                    elif self.tableSelectedRowIndices[i] == 7:
                        if self.xAxisLimSet:
                            if min( self.specMdl7[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl7[ xLeftInd:xRightInd ] )
                            if max( self.specMdl7[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl7[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl7 )

                    elif self.tableSelectedRowIndices[i] == 8:
                        if self.xAxisLimSet:
                            if min( self.specMdl8[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl8[ xLeftInd:xRightInd ] )
                            if max( self.specMdl8[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl8[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl8 )

                    elif self.tableSelectedRowIndices[i] == 9:
                        if self.xAxisLimSet:
                            if min( self.specMdl9[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl9[ xLeftInd:xRightInd ] )
                            if max( self.specMdl9[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl9[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl9 )

                    elif self.tableSelectedRowIndices[i] == 10:
                        if self.xAxisLimSet:
                            if min( self.specMdl10[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl10[ xLeftInd:xRightInd ] )
                            if max( self.specMdl10[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl10[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl10 )

                    elif self.tableSelectedRowIndices[i] == 11:
                        if self.xAxisLimSet:
                            if min( self.specMdl11[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl11[ xLeftInd:xRightInd ] )
                            if max( self.specMdl11[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl11[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl11 )

                    elif self.tableSelectedRowIndices[i] == 12:
                        if self.xAxisLimSet:
                            if min( self.specMdl12[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl12[ xLeftInd:xRightInd ] )
                            if max( self.specMdl12[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl12[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl12 )

                    elif self.tableSelectedRowIndices[i] == 13:
                        if self.xAxisLimSet:
                            if min( self.specMdl13[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl13[ xLeftInd:xRightInd ] )
                            if max( self.specMdl13[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl13[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl13 )

                    elif self.tableSelectedRowIndices[i] == 14:
                        if self.xAxisLimSet:
                            if min( self.specMdl14[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl14[ xLeftInd:xRightInd ] )
                            if max( self.specMdl14[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl14[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl14 )

                    elif self.tableSelectedRowIndices[i] == 15:
                        if self.xAxisLimSet:
                            if min( self.specMdl15[ xLeftInd:xRightInd] ) < yMin:
                                yMin = min( self.specMdl15[ xLeftInd:xRightInd ] )
                            if max( self.specMdl15[ xLeftInd:xRightInd] ) > yMax:
                                yMax = max( self.specMdl15[ xLeftInd:xRightInd ] )
                        self.axesMdl.plot( self.specMdl15 )

                if self.xAxisLimSet:
                    if yMax == 0:
                        self.axesMdl.set_autoscalex_on( True )
                    else:
                        self.yAxisLim = ( yMin, yMax * 1.1 )
                        self.axesMdl.set_xlim( self.xAxisLim )
                        self.axesMdl.set_ylim( self.yAxisLim )

                self.canvasMdl.draw()

        # if the index==2, then it is plotting spectra from selected module on PER CHIP base
        elif self.ui.plottingTab.currentIndex() == 2:
            if len( self.tableSelectedRowIndices ) > 0:
                self.xAxisLimSetChip0 = not self.axesChip0.get_autoscalex_on()
                if self.xAxisLimSetChip0:
                    self.xAxisLimChip0 = self.axesChip0.get_xlim()
                    xLeftIndChip0 = 0 if self.xAxisLimChip0[0] < 0 else int( self.xAxisLimChip0[0] )
                    xRightIndChip0 = eDepMax if self.xAxisLimChip0[1] > (eDepMax - 1) else int( self.xAxisLimChip0[1] )
                    yMinChip0=1e6
                    yMaxChip0=-1e6
                    self.yAxisLimChip0 = ( yMinChip0, yMaxChip0 )
                self.axesChip0.clear()

                self.xAxisLimSetChip1 = not self.axesChip1.get_autoscalex_on()
                if self.xAxisLimSetChip1:
                    self.xAxisLimChip1 = self.axesChip1.get_xlim()
                    xLeftIndChip1 = 0 if self.xAxisLimChip1[0] < 0 else int( self.xAxisLimChip1[0] )
                    xRightIndChip1 = eDepMax if self.xAxisLimChip1[1] > (eDepMax - 1) else int( self.xAxisLimChip1[1] )
                    yMinChip1=1e6
                    yMaxChip1=-1e6
                    self.yAxisLimChip1 = ( yMinChip1, yMaxChip1 )
                self.axesChip1.clear()

                self.xAxisLimSetChip2 = not self.axesChip2.get_autoscalex_on()
                if self.xAxisLimSetChip2:
                    self.xAxisLimChip2 = self.axesChip2.get_xlim()
                    xLeftIndChip2 = 0 if self.xAxisLimChip2[0] < 0 else int( self.xAxisLimChip2[0] )
                    xRightIndChip2 = eDepMax if self.xAxisLimChip2[1] > (eDepMax - 1) else int( self.xAxisLimChip2[1] )
                    yMinChip2=1e6
                    yMaxChip2=-1e6
                    self.yAxisLimChip2 = ( yMinChip2, yMaxChip2 )
                self.axesChip2.clear()

                self.xAxisLimSetChip3 = not self.axesChip3.get_autoscalex_on()
                if self.xAxisLimSetChip3:
                    self.xAxisLimChip3 = self.axesChip3.get_xlim()
                    xLeftIndChip3 = 0 if self.xAxisLimChip3[0] < 0 else int( self.xAxisLimChip3[0] )
                    xRightIndChip3 = eDepMax if self.xAxisLimChip3[1] > (eDepMax - 1) else int( self.xAxisLimChip3[1] )
                    yMinChip3=1e6
                    yMaxChip3=-1e6
                    self.yAxisLimChip3 = ( yMinChip3, yMaxChip3 )
                self.axesChip3.clear()

                # check each module to see if it is selected and find the y
                # axis limits
                for i in range( len( self.tableSelectedRowIndices ) ):
                    if self.tableSelectedRowIndices[i] == 0:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl0Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl0Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl0Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl0Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl0Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl0Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl0Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl0Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl0Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl0Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl0Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl0Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl0Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl0Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl0Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl0Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl0Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl0Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl0Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl0Chip3 )

                    elif self.tableSelectedRowIndices[i] == 1:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl1Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl1Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl1Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl1Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl1Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl1Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl1Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl1Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl1Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl1Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl1Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl1Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl1Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl1Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl1Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl1Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl1Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl1Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl1Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl1Chip3 )


                    elif self.tableSelectedRowIndices[i] == 2:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl2Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl2Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl2Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl2Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl2Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl2Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl2Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl2Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl2Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl2Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl2Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl2Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl2Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl2Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl2Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl2Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl2Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl2Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl2Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl2Chip3 )

                    elif self.tableSelectedRowIndices[i] == 3:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl3Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl3Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl3Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl3Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl3Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl3Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl3Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl3Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl3Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl3Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl3Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl3Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl3Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl3Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl3Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl3Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl3Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl3Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl3Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl3Chip3 )

                    elif self.tableSelectedRowIndices[i] == 4:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl4Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl4Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl4Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl4Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl4Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl4Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl4Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl4Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl4Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl4Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl4Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl4Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl4Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl4Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl4Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl4Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl4Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl4Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl4Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl4Chip3 )

                    elif self.tableSelectedRowIndices[i] == 5:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl5Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl5Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl5Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl5Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl5Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl5Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl5Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl5Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl5Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl5Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl5Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl5Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl5Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl5Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl5Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl5Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl5Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl5Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl5Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl5Chip3 )

                    elif self.tableSelectedRowIndices[i] == 6:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl6Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl6Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl6Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl6Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl6Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl6Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl6Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl6Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl6Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl6Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl6Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl6Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl6Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl6Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl6Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl6Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl6Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl6Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl6Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl6Chip3 )

                    elif self.tableSelectedRowIndices[i] == 7:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl7Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl7Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl7Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl7Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl7Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl7Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl7Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl7Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl7Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl7Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl7Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl7Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl7Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl7Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl7Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl7Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl7Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl7Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl7Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl7Chip3 )

                    elif self.tableSelectedRowIndices[i] == 8:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl8Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl8Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl8Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl8Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl8Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl8Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl8Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl8Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl8Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl8Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl8Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl8Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl8Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl8Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl8Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl8Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl8Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl8Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl8Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl8Chip3 )

                    elif self.tableSelectedRowIndices[i] == 9:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl9Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl9Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl9Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl9Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl9Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl9Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl9Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl9Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl9Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl9Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl9Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl9Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl9Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl9Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl9Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl9Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl9Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl9Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl9Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl9Chip3 )

                    elif self.tableSelectedRowIndices[i] == 10:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl10Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl10Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl10Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl10Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl10Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl10Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl10Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl10Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl10Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl10Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl10Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl10Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl10Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl10Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl10Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl10Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl10Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl10Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl10Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl10Chip3 )

                    elif self.tableSelectedRowIndices[i] == 11:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl11Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl11Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl11Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl11Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl11Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl11Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl11Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl11Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl11Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl11Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl11Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl11Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl11Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl11Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl11Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl11Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl11Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl11Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl11Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl11Chip3 )

                    elif self.tableSelectedRowIndices[i] == 12:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl12Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl12Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl12Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl12Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl12Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl12Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl12Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl12Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl12Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl12Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl12Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl12Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl12Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl12Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl12Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl12Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl12Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl12Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl12Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl12Chip3 )

                    elif self.tableSelectedRowIndices[i] == 13:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl13Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl13Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl13Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl13Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl13Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl13Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl13Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl13Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl13Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl13Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl13Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl13Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl13Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl13Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl13Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl13Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl13Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl13Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl13Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl13Chip3 )

                    elif self.tableSelectedRowIndices[i] == 14:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl14Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl14Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl14Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl14Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl14Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl14Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl14Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl14Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl14Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl14Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl14Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl14Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl14Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl14Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl14Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl14Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl14Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl14Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl14Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl14Chip3 )

                    elif self.tableSelectedRowIndices[i] == 15:
                        if self.xAxisLimSetChip0:
                            if min( self.specMdl15Chip0[ xLeftIndChip0:xRightIndChip0 ] ) < yMinChip0:
                                yMinChip0 = min( self.specMdl15Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                            if max( self.specMdl15Chip0[ xLeftIndChip0:xRightIndChip0 ] ) > yMaxChip0:
                                yMaxChip0 = max( self.specMdl15Chip0[ xLeftIndChip0:xRightIndChip0 ] )
                        self.axesChip0.plot( self.specMdl15Chip0 )

                        if self.xAxisLimSetChip1:
                            if min( self.specMdl15Chip1[ xLeftIndChip1:xRightIndChip1 ] ) < yMinChip1:
                                yMinChip1 = min( self.specMdl15Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                            if max( self.specMdl15Chip1[ xLeftIndChip1:xRightIndChip1 ] ) > yMaxChip1:
                                yMaxChip1 = max( self.specMdl15Chip1[ xLeftIndChip1:xRightIndChip1 ] )
                        self.axesChip1.plot( self.specMdl15Chip1 )

                        if self.xAxisLimSetChip2:
                            if min( self.specMdl15Chip2[ xLeftIndChip2:xRightIndChip2 ] ) < yMinChip2:
                                yMinChip2 = min( self.specMdl15Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                            if max( self.specMdl15Chip2[ xLeftIndChip2:xRightIndChip2 ] ) > yMaxChip2:
                                yMaxChip2 = max( self.specMdl15Chip2[ xLeftIndChip2:xRightIndChip2 ] )
                        self.axesChip2.plot( self.specMdl15Chip2 )

                        if self.xAxisLimSetChip3:
                            if min( self.specMdl15Chip3[ xLeftIndChip3:xRightIndChip3 ] ) < yMinChip3:
                                yMinChip3 = min( self.specMdl15Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                            if max( self.specMdl15Chip3[ xLeftIndChip3:xRightIndChip3 ] ) > yMaxChip3:
                                yMaxChip3 = max( self.specMdl15Chip3[ xLeftIndChip3:xRightIndChip3 ] )
                        self.axesChip3.plot( self.specMdl15Chip3 )

                if self.xAxisLimSetChip0:
                    if yMaxChip0 == 0:
                        self.axesChip0.set_autoscalex_on( True )
                    else:
                        self.yAxisLimChip0 = ( yMinChip0, yMaxChip0 * 1.1 )
                        self.axesChip0.set_xlim( self.xAxisLimChip0 )
                        self.axesChip0.set_ylim( self.yAxisLimChip0 )
                if self.xAxisLimSetChip1:
                    if yMaxChip1 == 0:
                        self.axesChip1.set_autoscalex_on( True )
                    else:
                        self.yAxisLimChip1 = ( yMinChip1, yMaxChip1 * 1.1 )
                        self.axesChip1.set_xlim( self.xAxisLimChip1 )
                        self.axesChip1.set_ylim( self.yAxisLimChip1 )
                if self.xAxisLimSetChip2:
                    if yMaxChip2 == 0:
                        self.axesChip2.set_autoscalex_on( True )
                    else:
                        self.yAxisLimChip2 = ( yMinChip0, yMaxChip2 * 1.1 )
                        self.axesChip2.set_xlim( self.xAxisLimChip2 )
                        self.axesChip2.set_ylim( self.yAxisLimChip2 )
                if self.xAxisLimSetChip3:
                    if yMaxChip3 == 0:
                        self.axesChip3.set_autoscalex_on( True )
                    else:
                        self.yAxisLimChip3 = ( yMinChip3, yMaxChip3 * 1.1 )
                        self.axesChip3.set_xlim( self.xAxisLimChip3 )
                        self.axesChip3.set_ylim( self.yAxisLimChip3 )

                self.canvasChip.draw()


    # def onSaveAsPushButtonClicked( self ):
    #
    #     # this button should not become enabled if there is acquistion going on
    #
    #     fileDialog = QtGui.QFileDialog()
    #     fileDialog.setFileMode( QtGui.QFileDialog.DirectoryOnly)
    #     # folderName = fileDialog.getExistingDirectory(caption='Save data in')
    #     folderName = fileDialog.getOpenFileName(caption='Save data in')
    #     if ( len( folderName) ) == 0:
    #         return
    #     print( folderName )
        # if self.saveFileID is not None:
        #     if not self.saveFileID.closed:
        #         self.messageBox = QtGui.QMessageBox()
        #         self.messageBox.setWindowTitle( 'Save as' )
        #         self.messageBox.setText( 'File ' + os.path.basename(self.saveFileID.name) + ' is still open!')
        #         append_button = self.messageBox.addButton( 'Append', QtGui.QMessageBox.YesRole )
        #         close_and_new_button = self.messageBox.addButton( 'Close and open new', QtGui.QMessageBox.YesRole )
        #         results = self.messageBox.exec_()
        #         if self.messageBox.clickedButton() == append_button:
        #             self.saveFileName = self.saveFileID.name
        #             if self.ui.saveAsLineEdit.text() != self.saveFileName:
        #                 self.ui.saveAsLineEdit.setText( self.saveFileName )
        #             return
        #         elif self.messageBox.clickedButton() == close_and_new_button:
        #             while self.writingThread.is_alive():
        #                 time.sleep(1)
        #             self.saveFileID.close()
        #
        # # open the save file dialog, but with customized warning when an existing file is selected
        # self.saveFileName, self.saveFileFilter = QtGui.QFileDialog.getSaveFileName( self, 'Save Events As',
        #                                                                     '/home/lq/work/compton/streaming/CCStreamingTestData',
        #                                                                     '*.txt', options=QtGui.QFileDialog.DontConfirmOverwrite)
        # # when cancel is press in the file dialog:
        # if len( self.saveFileName ) == 0:
        #     return
        # else:
        #
        #     if not self.saveFileName.endswith('.txt'):
        #         self.saveFileName = self.saveFileName + '.txt'
        #
        #     # the selected file already exist:
        #     if os.path.isfile( self.saveFileName ):
        #         self.messageBox = QtGui.QMessageBox()
        #         self.messageBox.setWindowTitle( 'Save as' )
        #         self.messageBox.setText( 'The file already exist!')
        #         overwrite_button = self.messageBox.addButton( 'Overwrite', QtGui.QMessageBox.YesRole)
        #         append_button = self.messageBox.addButton( 'Append', QtGui.QMessageBox.YesRole)
        #         self.messageBox.addButton( QtGui.QMessageBox.Cancel )
        #         results = self.messageBox.exec_()
        #         if self.messageBox.clickedButton() == overwrite_button:
        #             self.ui.saveAsLineEdit.setText( self.saveFileName )
        #             try:
        #                 self.saveFileID = open( self.saveFileName, 'w' )
        #             except IOError:
        #                 self.messageBox = QtGui.QMessageBox()
        #                 self.messageBox.setWindowTitle( 'Save as' )
        #                 self.messageBox.setText( 'Unable to open file' )
        #                 self.messageBox.setStandardButtons( QtGui.QMessageBox.Ok )
        #                 if self.messageBox.extc() == QtGui.QMessageBox.Ok:
        #                     return
        #         elif self.messageBox.clickedButton() == append_button:
        #             self.ui.saveAsLineEdit.setText( self.saveFileName )
        #             try:
        #                 self.saveFileID = open( self.saveFileName, 'a' )
        #             except IOError:
        #                 self.messageBox = QtGui.QMessageBox()
        #                 self.messageBox.setWindowTitle( 'save as' )
        #                 self.messageBox.setText( 'Unable to open file' )
        #                 self.messageBox.setStandardButtons( QtGui.QMessageBox.Ok )
        #                 if self.messageBox.extc() == QtGui.QMessageBox.Ok:
        #                     return
        #         elif results == QtGui.QMessageBox.Cancel:
        #             # print( 'cancel button clicked' )
        #             return
        #     # the selected file does not exist
        #     else:
        #         self.ui.saveAsLineEdit.setText( self.saveFileName )
        #         try:
        #             self.saveFileID = open( self.saveFileName, 'w' )
        #             # self.saveFileID = open( self.saveFileName, 'wb' )
        #         except IOError:
        #             self.messageBox = QtGui.QMessageBox()
        #             self.messageBox.setWindowTitle( 'save as' )
        #             self.messageBox.setText( 'Unable to open file' )
        #             self.messageBox.setStandardButtons( QtGui.QMessageBox.Ok )
        #             if self.messageBox.exec_() == QtGui.QMessageBox.Ok:
        #                 return
        # return

    def onMeasurementNameLineEditReturnPressed( self ):


        # self.savingFolderName = defaultDataFolder + datetime.datetime.now().strftime('%Y%m%d%H%M%S' ) + \
        #                         '_' + self.ui.measurementNameLineEdit.text()

        fn = self.ui.measurementNameLineEdit.text()
        if len( fn ) < 1:
            self.messageBox = QtWidgets.QMessageBox()
            self.messageBox.setWindowTitle( 'Measurement name' )
            self.messageBox.setText( 'Measurement name can not be blank!' )
            self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok )
            if self.messageBox.exec_() == QtWidgets.QMessageBox.Ok:
                return
        if fn.startswith( '/' ):
            fn = fn[1:]
        if not fn.endswith( '/' ):
            fn = fn + '/'

        self.savingFolderName.value = ( defaultDataFolder + fn ).encode()

        if os.path.isdir( self.savingFolderName.value ):
            self.messageBox = QtWidgets.QMessageBox()
            self.messageBox.setWindowTitle( 'Measurement name' )
            self.messageBox.setText( 'The measurement name has been used!' )
            append_button = self.messageBox.addButton( 'Append', QtWidgets.QMessageBox.YesRole )
            overwrite_button = self.messageBox.addButton( 'Overwrite', QtWidgets.QMessageBox.YesRole )
            self.messageBox.addButton( QtWidgets.QMessageBox.Cancel )
            results = self.messageBox.exec_()
            if self.messageBox.clickedButton() == QtWidgets.QMessageBox.Cancel:
                return
            elif self.messageBox.clickedButton() == overwrite_button:
                self.savingMode.value = 'new'.encode()
            elif self.messageBox.clickedButton() == append_button:
                self.savingMode.value = 'append'.encode()
            else:
                pass
        else:
            self.savingMode.value = 'new'.encode()
            os.mkdir( self.savingFolderName.value )

    def onSave2UsbCheckBoxChanged( self ):
        # slot function for when save2UsbCheckBox changed

        return

    def onTimeAcquisitionCheckBoxChanged( self ):
        # slot function for timedAcquisitionCheckBox changed
        if self.ui.timedAcquisitionCheckBox.isChecked():
            self.ui.acquisitionTimeHourLabel.setEnabled( True )
            self.ui.acquisitionTimeHourSpinBox.setEnabled( True )
            self.ui.acquisitionTimeMinutesLabel.setEnabled( True )
            self.ui.acquisitionTimeMinutesSpinBox.setEnabled( True )
            self.ui.acquisitionTimeSecondsLabel.setEnabled( True )
            self.ui.acquisitionTimeSecondsSpinBox.setEnabled( True )
        else:
            self.ui.acquisitionTimeHourLabel.setEnabled( False )
            self.ui.acquisitionTimeHourSpinBox.setEnabled( False )
            self.ui.acquisitionTimeMinutesLabel.setEnabled( False )
            self.ui.acquisitionTimeMinutesSpinBox.setEnabled( False )
            self.ui.acquisitionTimeSecondsLabel.setEnabled( False )
            self.ui.acquisitionTimeSecondsSpinBox.setEnabled( False )
        return

    # @QtCore.Slot()
    def onConnectCCButtonClicked( self ):
        # when program starts, the camera is not connected
        # the click would connect the camera:
        if not self.CCConnected:
            # enable/disable sub-mm pixel position for each module:
            #   Calibration standard to turn sub-mm pixel positioning on
            #   Calibration AllCalibration to turn sub-mm pixel positioning off
            for mdlNumber in self.moduleNumbers:
                ip = '192.168.3.{}'.format( mdlNumber )
                self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                self.sCommand.connect( (ip, 11502 ) )
                #self.sCommand.sendall('Calibration standard\n'.encode() )
                self.sCommand.sendall('Calibration AllCalibration\n'.encode() )
                self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
                self.sCommand.close()

            # reset sync pulse index for each module:
            for mdlNumber in self.moduleNumbers:
                #ip = '192.168.2.{}'.format( mdlNumber )
                ip = '192.168.3.{}'.format( mdlNumber )
                self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                self.sCommand.connect( (ip, 11502 ) )
                self.sCommand.sendall('ResetIndex\n'.encode() )
                self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
                self.sCommand.close()

            # start sync pulse from master module:
            #   disable Start Sync Pulse for external timing
            self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            #self.sCommand.connect( ('192.168.2.{}'.format( self.master ), 11502 ) )
            self.sCommand.connect( ('192.168.3.{}'.format( self.master ), 11502 ) )
            self.sCommand.sendall('Start\n'.encode() )
            #self.ccLogger.info( '192.168.2.{} '.format( self.master ) + self.sCommand.recv( 1024 ).decode() )
            self.ccLogger.info( '192.168.3.{} '.format( self.master ) + self.sCommand.recv( 1024 ).decode() )
            self.sCommand.close()

            # if it's reconnecting from a previously disconected state, need to re-initiate the streaming and parsing
            # processes for each module:
            if self.alreadyDisconnected:
                self.acquring.value = False
                for i in range( nModules ):
                    self.moduleStatus[i] = 0
                    self.disconnectModule[i] = 0
                    self.writeFileNowTrigger[i] = 0
                    self.writeFileDone[i] = 0

                for i in range( eDepMax ):
                    self.specCombined[i] = 0
                    self.specMdl0[i] = 0
                    self.specMdl0Chip0[i] = 0; self.specMdl0Chip1[i] = 0; self.specMdl0Chip2[i] = 0; self.specMdl0Chip3[i] = 0;
                    self.specMdl1[i] = 0
                    self.specMdl1Chip0[i] = 0; self.specMdl1Chip1[i] = 0; self.specMdl1Chip2[i] = 0; self.specMdl1Chip3[i] = 0;
                    self.specMdl2[i] = 0
                    self.specMdl2Chip0[i] = 0; self.specMdl2Chip1[i] = 0; self.specMdl2Chip2[i] = 0; self.specMdl2Chip3[i] = 0;
                    self.specMdl3[i] = 0
                    self.specMdl3Chip0[i] = 0; self.specMdl3Chip1[i] = 0; self.specMdl3Chip2[i] = 0; self.specMdl3Chip3[i] = 0;
                    self.specMdl4[i] = 0
                    self.specMdl4Chip0[i] = 0; self.specMdl4Chip1[i] = 0; self.specMdl4Chip2[i] = 0; self.specMdl4Chip3[i] = 0;
                    self.specMdl5[i] = 0
                    self.specMdl5Chip0[i] = 0; self.specMdl5Chip1[i] = 0; self.specMdl5Chip2[i] = 0; self.specMdl5Chip3[i] = 0;
                    self.specMdl6[i] = 0
                    self.specMdl6Chip0[i] = 0; self.specMdl6Chip1[i] = 0; self.specMdl6Chip2[i] = 0; self.specMdl6Chip3[i] = 0;
                    self.specMdl7[i] = 0
                    self.specMdl7Chip0[i] = 0; self.specMdl7Chip1[i] = 0; self.specMdl7Chip2[i] = 0; self.specMdl7Chip3[i] = 0;
                    self.specMdl8[i] = 0
                    self.specMdl8Chip0[i] = 0; self.specMdl8Chip1[i] = 0; self.specMdl8Chip2[i] = 0; self.specMdl8Chip3[i] = 0;
                    self.specMdl9[i] = 0
                    self.specMdl9Chip0[i] = 0; self.specMdl9Chip1[i] = 0; self.specMdl9Chip2[i] = 0; self.specMdl9Chip3[i] = 0;
                    self.specMdl10[i] = 0
                    self.specMdl10Chip0[i] = 0; self.specMdl10Chip1[i] = 0; self.specMdl10Chip2[i] = 0; self.specMdl10Chip3[i] = 0;
                    self.specMdl11[i] = 0
                    self.specMdl11Chip0[i] = 0; self.specMdl11Chip1[i] = 0; self.specMdl11Chip2[i] = 0; self.specMdl11Chip3[i] = 0;
                    self.specMdl12[i] = 0
                    self.specMdl12Chip0[i] = 0; self.specMdl12Chip1[i] = 0; self.specMdl12Chip2[i] = 0; self.specMdl12Chip3[i] = 0;
                    self.specMdl13[i] = 0
                    self.specMdl13Chip0[i] = 0; self.specMdl13Chip1[i] = 0; self.specMdl13Chip2[i] = 0; self.specMdl13Chip3[i] = 0;
                    self.specMdl14[i] = 0
                    self.specMdl14Chip0[i] = 0; self.specMdl14Chip1[i] = 0; self.specMdl14Chip2[i] = 0; self.specMdl14Chip3[i] = 0;
                    self.specMdl15[i] = 0
                    self.specMdl15Chip0[i] = 0; self.specMdl15Chip1[i] = 0; self.specMdl15Chip2[i] = 0; self.specMdl15Chip3[i] = 0;
            if 51 in self.moduleNumbers:
                try:
                    self.mdl0StreamingProc = streamingProcess(  51, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 51 streaming process')
            if 52 in self.moduleNumbers:
                try:
                    self.mdl1StreamingProc = streamingProcess(  52, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 52 streaming process')
            if 53 in self.moduleNumbers:
                try:
                    self.mdl2StreamingProc = streamingProcess(  53, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 53 streaming process')
            if 54 in self.moduleNumbers:
                try:
                    self.mdl3StreamingProc = streamingProcess(  54, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 54 streaming process')
            if 55 in self.moduleNumbers:
                try:
                    self.mdl4StreamingProc = streamingProcess(  55, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate muodlue 55 streaming process')
            if 56 in self.moduleNumbers:
                try:
                    self.mdl5StreamingProc = streamingProcess(  56, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 56 streaming process')
            if 57 in self.moduleNumbers:
                try:
                    self.mdl6StreamingProc = streamingProcess(  57, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 57 streaming process')
            if 58 in self.moduleNumbers:
                try:
                    self.mdl7StreamingProc = streamingProcess(  58, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 58 streaming process')
            if 59 in self.moduleNumbers:
                try:
                    self.mdl8StreamingProc = streamingProcess(  59, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 59 streaming process')
            if 60 in self.moduleNumbers:
                try:
                    self.mdl9StreamingProc = streamingProcess(  60, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 60 streaming process')
            if 61 in self.moduleNumbers:
                try:
                    self.mdl10StreamingProc = streamingProcess(  61, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 61 streaming process')
            if 62 in self.moduleNumbers:
                try:
                    self.mdl11StreamingProc = streamingProcess(  62, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 62 streaming process')
            if 63 in self.moduleNumbers:
                try:
                    self.mdl12StreamingProc = streamingProcess(  63, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 63 streaming process')
            if 64 in self.moduleNumbers:
                try:
                    self.mdl13StreamingProc = streamingProcess(  64, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 64 streaming process')
            if 65 in self.moduleNumbers:
                try:
                    self.mdl14StreamingProc = streamingProcess(  65, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 65 streaming process')
            if 66 in self.moduleNumbers:
                try:
                    self.mdl15StreamingProc = streamingProcess(  66, self.moduleStatus, self.disconnectModule,
                                                                self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
                except:
                    raise Exception( 'failed to initiate module 66 streaming process')

            self.alreadyDisconnected = False

            # start the child process for each module:
            if 51 in self.moduleNumbers:
                self.mdl0StreamingProc.start(); # self.mdl0ParsingProc.start()
            if 52 in self.moduleNumbers:
                self.mdl1StreamingProc.start(); # self.mdl1ParsingProc.start()
            if 53 in self.moduleNumbers:
                self.mdl2StreamingProc.start(); # self.mdl2ParsingProc.start()
            if 54 in self.moduleNumbers:
                self.mdl3StreamingProc.start(); # self.mdl3ParsingProc.start()
            if 55 in self.moduleNumbers:
                self.mdl4StreamingProc.start(); # self.mdl4ParsingProc.start()
            if 56 in self.moduleNumbers:
                self.mdl5StreamingProc.start(); # self.mdl5ParsingProc.start()
            if 57 in self.moduleNumbers:
                self.mdl6StreamingProc.start(); # self.mdl6ParsingProc.start()
            if 58 in self.moduleNumbers:
                self.mdl7StreamingProc.start(); # self.mdl7ParsingProc.start()
            if 59 in self.moduleNumbers:
                self.mdl8StreamingProc.start(); # self.mdl8ParsingProc.start()
            if 60 in self.moduleNumbers:
                self.mdl9StreamingProc.start(); # self.mdl9ParsingProc.start()
            if 61 in self.moduleNumbers:
                self.mdl10StreamingProc.start(); # self.mdl10ParsingProc.start()
            if 62 in self.moduleNumbers:
                self.mdl11StreamingProc.start(); # self.mdl11ParsingProc.start()
            if 63 in self.moduleNumbers:
                self.mdl12StreamingProc.start(); # self.mdl12ParsingProc.start()
            if 64 in self.moduleNumbers:
                self.mdl13StreamingProc.start(); # self.mdl13ParsingProc.start()
            if 65 in self.moduleNumbers:
                self.mdl14StreamingProc.start(); # self.mdl14ParsingProc.start()
            if 66 in self.moduleNumbers:
                self.mdl15StreamingProc.start(); # self.mdl15ParsingProc.start()

            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl0StreamingProc.pid, self.mdl0ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl1StreamingProc.pid, self.mdl1ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl2StreamingProc.pid, self.mdl2ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl3StreamingProc.pid, self.mdl3ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl4StreamingProc.pid, self.mdl4ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl5StreamingProc.pid, self.mdl5ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl6StreamingProc.pid, self.mdl6ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl7StreamingProc.pid, self.mdl7ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl8StreamingProc.pid, self.mdl8ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl9StreamingProc.pid, self.mdl9ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl10StreamingProc.pid, self.mdl10ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl11StreamingProc.pid, self.mdl11ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl12StreamingProc.pid, self.mdl12ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl13StreamingProc.pid, self.mdl13ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl14StreamingProc.pid, self.mdl14ParsingProc.pid ) )
            # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl15StreamingProc.pid, self.mdl15ParsingProc.pid ) )

            # it takes a short period for each per module process to establish
            # a data socket, so give it time delay:
            time.sleep(2)
            self.ccLogger.info( 'reconnecting, module status sum is {}'.format( sum( self.moduleStatus) ) )

            # when module status for each module is set to 1 ( i.e. socket
            # established successfully ):
            if sum( self.moduleStatus ) == len(self.moduleNumbers):
                self.CCConnected = True
                for i in range( nModules ):
                    ind = self.tableModel.index(i,0)
                    self.tableModel.setData( ind, 'Y', QtCore.Qt.DisplayRole )

                # enable the start acquistion button
                self.ui.startacquisitionButton.setEnabled( True )
                self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: green;}')
                # enable disconnect menu
                self.ui.actionDisconnect.setEnabled( True )
                # change button to disconnect camera
                self.ui.connectCCButton.setText( _translate( "MainWindow", "Disconnect Camera", None) )
                self.ui.connectCCButton.setStyleSheet('QPushButton {color: red;}')

                # start update plot and status table
                # self.updatePlotTimer.start( refreshSpecPlotPeriod )
                self.updateStatusTableTimer.start( refreshStatusTablePeriod )

            # not all modules are connected successfully
            else:
                for i in range( nModules ):
                    if self.moduleStatus[i] != 1:
                        self.ccLogger.critical( 'data socket on module {} failed to connect'.format( self.moduleStatus[i]) )
                self.disconnectCamera()

        # when the camera is already connected, this click should disconnect
        else:
            self.disconnectCamera()


    def onStartAcquisitionButtonClicked( self ):

        # when the camera is acquring, click the button will stop acquisition
        if self.acquring.value:
            # set to False to stop sending parsed data to savingQ
            self.acquring.value = False
            if self.ui.timedAcquisitionCheckBox.isChecked():
                self.autoStopTimer.stop()
            # set write file trigger to 1 for each module to set off write file event:
            self.ccLogger.info( '{} setting write file trigger'.format( time.time() ) )
            for i in range( nModules ):
                self.writeFileNowTrigger[i] = 1

            # wait until all files are written:
            while sum( self.writeFileDone ) != len(self.moduleNumbers):
                time.sleep( 0.5 )

            """
            # --------------------------------------------------------------------------------------------------
            # Convert data to txt format
            # --------------------------------------------------------------------------------------------------
            print ('\n--- Running combine_dat.py ---\n')

            # Get .dat file directory
            INPUT_DIR = self.savingFolderName.value.decode()

            # Merge binaries
            print("Merging data arrays...\n")
            print()
            comb_dat.merge_dats(INPUT_DIR)
            print()
            print('Merging complete.')
            # --------------------------------------------------------------------------------------------------
            """

            for i in range( nModules ):
                self.writeFileNowTrigger[i] = 0
                self.writeFileDone[i] = 0

            if self.ui.save2UsbCheckBox.isChecked():
                for moduleNumber in self.moduleNumbers:
                    #ip = '192.168.2.' + str( moduleNumber )
                    ip = '192.168.3.' + str( moduleNumber )
                    self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                    self.sCommand.connect( (ip, 11502 ) )
                    self.sCommand.sendall( 'SaveBEF \n'.encode() )
                    self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
                    self.sCommand.close()


            self.ui.startacquisitionButton.setText( _translate( "MainWindow", "Start", None ) )
            self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: green;}')
            self.ui.measurementNameLabel.setDisabled( False )
            self.ui.measurementNameLineEdit.setDisabled( False )
            self.ui.save2UsbCheckBox.setDisabled( False )
            self.ui.timedAcquisitionCheckBox.setDisabled( False )
            self.ui.acquisitionTimeHourLabel.setDisabled( False )
            self.ui.acquisitionTimeHourSpinBox.setDisabled( False )
            self.ui.acquisitionTimeMinutesLabel.setDisabled( False )
            self.ui.acquisitionTimeMinutesSpinBox.setDisabled( False )
            self.ui.acquisitionTimeSecondsLabel.setDisabled( False )
            self.ui.acquisitionTimeSecondsSpinBox.setDisabled( False )
            self.ui.connectCCButton.setDisabled( False )

            # time.sleep(5)
            # self.onConnectCCButtonClicked()

        # when it is not currently acquring, click the button will start acquisition
        else:
            if self.CCConnected:

                fn = self.ui.measurementNameLineEdit.text()
                if len( fn ) < 1:
                    self.messageBox = QtWidgets.QMessageBox()
                    self.messageBox.setWindowTitle( 'Measurement name' )
                    self.messageBox.setText( 'Measurement name can not be blank!' )
                    self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok )
                    if self.messageBox.exec_() == QtWidgets.QMessageBox.Ok:
                        return
                if not fn.endswith( '/' ):
                    fn = fn + '/'

                self.savingFolderName.value = ( defaultDataFolder + fn ).encode()

                if os.path.isdir( self.savingFolderName.value ):
                    self.messageBox = QtWidgets.QMessageBox()
                    self.messageBox.setWindowTitle( 'Measurement name' )
                    self.messageBox.setText( 'The measurement name has been used!' )
                    append_button = self.messageBox.addButton( 'Append', QtWidgets.QMessageBox.YesRole )
                    overwrite_button = self.messageBox.addButton( 'Overwrite', QtWidgets.QMessageBox.YesRole )
                    self.messageBox.addButton( QtWidgets.QMessageBox.Cancel )
                    results = self.messageBox.exec_()
                    if results == QtWidgets.QMessageBox.Cancel:
                        print( 'Cancel button clicked')
                        return
                    elif self.messageBox.clickedButton() == overwrite_button:
                        self.savingMode.value = 'new'.encode()
                    elif self.messageBox.clickedButton() == append_button:
                        self.savingMode.value = 'append'.encode()
                    else:
                        pass
                else:
                    os.mkdir( self.savingFolderName.value )

                if self.ui.timedAcquisitionCheckBox.isChecked():
                    self.acqTime =  1000 * int(
                                    self.ui.acquisitionTimeHourSpinBox.value() * 3600 +
                                    self.ui.acquisitionTimeMinutesSpinBox.value() * 60 +
                                    self.ui.acquisitionTimeSecondsSpinBox.value() )
                    self.autoStopTimer.start( self.acqTime )

                # self.acquring.value = True

                fn = self.ui.measurementNameLineEdit.text()
                if fn.endswith( '/' ):
                    fn = fn[:-1]

                if self.ui.save2UsbCheckBox.isChecked():
                    for moduleNumber in self.moduleNumbers:
                        #ip = '192.168.2.' + str( moduleNumber )
                        ip = '192.168.3.' + str( moduleNumber )
                        self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                        self.sCommand.connect( (ip, 11502 ) )
                        cmd = 'SaveBEF ' + fn +'\n'
                        self.sCommand.sendall( cmd.encode() )
                        print( ip, self.sCommand.recv( 1024 ).decode() )
                        self.sCommand.close()

                self.acquring.value = True
                #     cmd = 'SaveBEF ' + os.path.splitext( os.path.basename( self.saveFileName) )[0] +'\n'
                #     print( cmd, ' command to be sent')
                #     ip = '192.168.2.53'
                #     self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                #     self.sCommand.connect( (ip, 11502 ) )
                #     self.sCommand.sendall( cmd.encode() )
                #     print( ip, self.sCommand.recv( 1024 ).decode() )
                #     self.sCommand.close()
                #
                #     ip = '192.168.2.66'
                #     self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                #     self.sCommand.connect( (ip, 11502 ) )
                #     self.sCommand.sendall( cmd.encode() )
                #     print( ip, self.sCommand.recv( 1024 ).decode() )
                #     self.sCommand.close()

                self.ui.startacquisitionButton.setText( _translate( "MainWindow", "Stop", None ) )
                self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: red;}')

                # while saving the data, the following are not allowed:
                self.ui.measurementNameLabel.setDisabled( True )
                self.ui.measurementNameLineEdit.setDisabled( True )
                self.ui.save2UsbCheckBox.setDisabled( True )
                self.ui.timedAcquisitionCheckBox.setDisabled( True )
                self.ui.acquisitionTimeHourLabel.setDisabled( True )
                self.ui.acquisitionTimeHourSpinBox.setDisabled( True )
                self.ui.acquisitionTimeMinutesLabel.setDisabled( True )
                self.ui.acquisitionTimeMinutesSpinBox.setDisabled( True )
                self.ui.acquisitionTimeSecondsLabel.setDisabled( True )
                self.ui.acquisitionTimeSecondsSpinBox.setDisabled( True )
                self.ui.connectCCButton.setDisabled( True )

            else:
                self.messageBox = QtWidgets.QMessageBox()
                self.messageBox.setWindowTitle( 'Starting acquisition' )
                self.messageBox.setText( 'Camera is not connected' )
                self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok )
                if self.messageBox.exec() == QtWidgets.QMessageBox.Ok:
                    return

    def onClearSpectraButtonClicked( self ):
        if self.CCConnected:
            # self.updatePlotTimer.stop()
            for i in range( eDepMax ):
                self.specCombined[i] = 0
                self.specMdl0[i] = 0
                self.specMdl0Chip0[i] = 0; self.specMdl0Chip1[i] = 0; self.specMdl0Chip2[i] = 0; self.specMdl0Chip3[i] = 0;
                self.specMdl1[i] = 0
                self.specMdl1Chip0[i] = 0; self.specMdl1Chip1[i] = 0; self.specMdl1Chip2[i] = 0; self.specMdl1Chip3[i] = 0;
                self.specMdl2[i] = 0
                self.specMdl2Chip0[i] = 0; self.specMdl2Chip1[i] = 0; self.specMdl2Chip2[i] = 0; self.specMdl2Chip3[i] = 0;
                self.specMdl3[i] = 0
                self.specMdl3Chip0[i] = 0; self.specMdl3Chip1[i] = 0; self.specMdl3Chip2[i] = 0; self.specMdl3Chip3[i] = 0;
                self.specMdl4[i] = 0
                self.specMdl4Chip0[i] = 0; self.specMdl4Chip1[i] = 0; self.specMdl4Chip2[i] = 0; self.specMdl4Chip3[i] = 0;
                self.specMdl5[i] = 0
                self.specMdl5Chip0[i] = 0; self.specMdl5Chip1[i] = 0; self.specMdl5Chip2[i] = 0; self.specMdl5Chip3[i] = 0;
                self.specMdl6[i] = 0
                self.specMdl6Chip0[i] = 0; self.specMdl6Chip1[i] = 0; self.specMdl6Chip2[i] = 0; self.specMdl6Chip3[i] = 0;
                self.specMdl7[i] = 0
                self.specMdl7Chip0[i] = 0; self.specMdl7Chip1[i] = 0; self.specMdl7Chip2[i] = 0; self.specMdl7Chip3[i] = 0;
                self.specMdl8[i] = 0
                self.specMdl8Chip0[i] = 0; self.specMdl8Chip1[i] = 0; self.specMdl8Chip2[i] = 0; self.specMdl8Chip3[i] = 0;
                self.specMdl9[i] = 0
                self.specMdl9Chip0[i] = 0; self.specMdl9Chip1[i] = 0; self.specMdl9Chip2[i] = 0; self.specMdl9Chip3[i] = 0;
                self.specMdl10[i] = 0
                self.specMdl10Chip0[i] = 0; self.specMdl10Chip1[i] = 0; self.specMdl10Chip2[i] = 0; self.specMdl10Chip3[i] = 0;
                self.specMdl11[i] = 0
                self.specMdl11Chip0[i] = 0; self.specMdl11Chip1[i] = 0; self.specMdl11Chip2[i] = 0; self.specMdl11Chip3[i] = 0;
                self.specMdl12[i] = 0
                self.specMdl12Chip0[i] = 0; self.specMdl12Chip1[i] = 0; self.specMdl12Chip2[i] = 0; self.specMdl12Chip3[i] = 0;
                self.specMdl13[i] = 0
                self.specMdl13Chip0[i] = 0; self.specMdl13Chip1[i] = 0; self.specMdl13Chip2[i] = 0; self.specMdl13Chip3[i] = 0;
                self.specMdl14[i] = 0
                self.specMdl14Chip0[i] = 0; self.specMdl14Chip1[i] = 0; self.specMdl14Chip2[i] = 0; self.specMdl14Chip3[i] = 0;
                self.specMdl15[i] = 0
                self.specMdl15Chip0[i] = 0; self.specMdl15Chip1[i] = 0; self.specMdl15Chip2[i] = 0; self.specMdl15Chip3[i] = 0;

            # self.updatePlotTimer.start(500)


    def onCCStatusTableViewSelectionChanged( self ):
        self.tableSelectedRowIndices = []
        selectedIndices = self.tableSelectionModel.selectedRows()
        for i in range( len(selectedIndices ) ):
            self.tableSelectedRowIndices.append( selectedIndices[i].row() )


    def getModuleInfo( self, a ):
        p = a.find( 'TriggerRate' )
        while a[p] != '<':
            if a[p] == '=':
                startP = p
            p += 1
        endP = p
        rate = a[startP+1:endP]

        p = a.find( 'DeadTime' )
        while a[p] != ')':
            if a[p] == ',':
                startP = p
            p += 1
        endP = p
        deadTime = a[startP+1:endP]

        p = a.find( 'CPUTemp' )
        while a[p] != '<':
            if a[p] == '=':
                startP = p
            p += 1
        endP = p
        T = a[startP+1:endP]

        p = a.find( 'Humidity' )
        while a[p] != '<':
            if a[p] == '=':
                startP = p
            p += 1
        endP = p
        humidity = a[startP+1:endP]

        p = a.find( 'USB Usage' )
        while a[p] != '%':
            if a[p] == '=':
                startP = p
            p += 1
        endP = p
        USB = a[startP+1:endP+1]

        # return rate, deadTime, T, USB
        return rate, deadTime, T, humidity, USB


    def updateStatusTable( self ):
        for i in range( nModules ):
            #address = 'http://192.168.2.'+str(i+51)+':8080/DAQStatus.html'
            address = 'http://192.168.3.'+str(i+51)+':8080/DAQStatus.html'
            with urllib.request.urlopen( address ) as r:
                # rate, deadTime, T, USB = self.getModuleInfo( r.read().decode() )
                rate, deadTime, T, humidity, USB = self.getModuleInfo( r.read().decode() )

            ind = self.tableModel.index( i, 0 )
            if self.moduleStatus[i] == 1:
                self.tableModel.setData( ind, 'Y', QtCore.Qt.DisplayRole )
            else:
                self.tableModel.setData( ind, 'N', QtCore.Qt.DisplayRole )
            ind = self.tableModel.index( i, 1 )
            self.tableModel.setData( ind, rate, QtCore.Qt.DisplayRole )
            ind = self.tableModel.index( i, 2 )
            self.tableModel.setData( ind, deadTime, QtCore.Qt.DisplayRole )
            ind = self.tableModel.index( i, 3 )
            self.tableModel.setData( ind, USB, QtCore.Qt.DisplayRole )
            ind = self.tableModel.index( i, 4 )
            self.tableModel.setData( ind, T, QtCore.Qt.DisplayRole )
            ind = self.tableModel.index( i, 5 )
            self.tableModel.setData( ind, humidity, QtCore.Qt.DisplayRole )

        return

    def closeEvent( self, event ):
        if self.CCConnected:
            self.disconnectCamera()
        self.close()

    def disconnectCamera( self ):
        self.ccLogger.info( 'starting disconnect at {}'.format( time.time() ) )

        # warn while it is still saving data
        if self.acquring.value:
            self.messageBox = QtWidgets.QMessageBox()
            self.messageBox.setWindowTitle( 'Disconnecting Camera' )
            self.messageBox.setText('Still acquring, do you wish to disconnect from camera?')
            self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel )
            results = self.messageBox.exec_()
            if results == QtWidgets.QMessageBox.Cancel:
                return
            elif results == QtWidgets.QMessageBox.Ok:
                self.onStartAcquisitionButtonClicked()


        # set disconnectModule to 1 for each module to send a signal to each
        # per module process to disconnect the data socket.
        for i in range( nModules ):
            self.disconnectModule[i] = 1

        time.sleep(1)

        # wait per module process to finish up:
        if 51 in self.moduleNumbers and self.mdl0StreamingProc.is_alive():
            self.mdl0StreamingProc.join()
        if 52 in self.moduleNumbers and self.mdl1StreamingProc.is_alive():
            self.mdl1StreamingProc.join()
        if 53 in self.moduleNumbers and self.mdl2StreamingProc.is_alive():
            self.mdl2StreamingProc.join()
        if 54 in self.moduleNumbers and self.mdl3StreamingProc.is_alive():
            self.mdl3StreamingProc.join()
        if 55 in self.moduleNumbers and self.mdl4StreamingProc.is_alive():
            self.mdl4StreamingProc.join()
        if 56 in self.moduleNumbers and self.mdl5StreamingProc.is_alive():
            self.mdl5StreamingProc.join()
        if 57 in self.moduleNumbers and self.mdl6StreamingProc.is_alive():
            self.mdl6StreamingProc.join()
        if 58 in self.moduleNumbers and self.mdl7StreamingProc.is_alive():
            self.mdl7StreamingProc.join()
        if 59 in self.moduleNumbers and self.mdl8StreamingProc.is_alive():
            self.mdl8StreamingProc.join()
        if 60 in self.moduleNumbers and self.mdl9StreamingProc.is_alive():
            self.mdl9StreamingProc.join()
        if 61 in self.moduleNumbers and self.mdl10StreamingProc.is_alive():
            self.mdl10StreamingProc.join()
        if 62 in self.moduleNumbers and self.mdl11StreamingProc.is_alive():
            self.mdl11StreamingProc.join()
        if 63 in self.moduleNumbers and self.mdl12StreamingProc.is_alive():
            self.mdl12StreamingProc.join()
        if 64 in self.moduleNumbers and self.mdl13StreamingProc.is_alive():
            self.mdl13StreamingProc.join()
        if 65 in self.moduleNumbers and self.mdl14StreamingProc.is_alive():
            self.mdl14StreamingProc.join()
        if 66 in self.moduleNumbers and self.mdl15StreamingProc.is_alive():
            self.mdl15StreamingProc.join()

        # if self.mdl0ParsingProc.is_alive():
        #     self.mdl0ParsingProc.join()
        # if self.mdl1ParsingProc.is_alive():
        #     self.mdl1ParsingProc.join()
        # if self.mdl2ParsingProc.is_alive():
        #     self.mdl2ParsingProc.join()
        # if self.mdl3ParsingProc.is_alive():
        #     self.mdl3ParsingProc.join()
        # if self.mdl4ParsingProc.is_alive():
        #     self.mdl4ParsingProc.join()
        # if self.mdl5ParsingProc.is_alive():
        #     self.mdl5ParsingProc.join()
        # if self.mdl6ParsingProc.is_alive():
        #     self.mdl6ParsingProc.join()
        # if self.mdl7ParsingProc.is_alive():
        #     self.mdl7ParsingProc.join()
        # if self.mdl8ParsingProc.is_alive():
        #     self.mdl8ParsingProc.join()
        # if self.mdl9ParsingProc.is_alive():
        #     self.mdl9ParsingProc.join()
        # if self.mdl10ParsingProc.is_alive():
        #     self.mdl10ParsingProc.join()
        # if self.mdl11ParsingProc.is_alive():
        #     self.mdl11ParsingProc.join()
        # if self.mdl12ParsingProc.is_alive():
        #     self.mdl12ParsingProc.join()
        # if self.mdl13ParsingProc.is_alive():
        #     self.mdl13ParsingProc.join()
        # if self.mdl14ParsingProc.is_alive():
        #     self.mdl14ParsingProc.join()
        # if self.mdl15ParsingProc.is_alive():
        #     self.mdl15ParsingProc.join()

        self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        #self.sCommand.connect( ('192.168.2.{}'.format( self.master ), 11502 ) )
        self.sCommand.connect( ('192.168.3.{}'.format( self.master ), 11502 ) )
        self.sCommand.sendall('Stop\n'.encode() )
        #self.ccLogger.info( '192.168.2.{} {}'.format( self.master, self.sCommand.recv( 1024 ).decode() ) )
        self.ccLogger.info( '192.168.3.{} {}'.format( self.master, self.sCommand.recv( 1024 ).decode() ) )
        self.sCommand.close()

        self.CCConnected = False

        # self.mdl0StreamingProc = None; self.mdl0ParsingProc = None
        # self.mdl1StreamingProc = None; self.mdl1ParsingProc = None
        # self.mdl2StreamingProc = None; self.mdl2ParsingProc = None
        # self.mdl3StreamingProc = None; self.mdl3ParsingProc = None
        # self.mdl4StreamingProc = None; self.mdl4ParsingProc = None
        # self.mdl5StreamingProc = None; self.mdl5ParsingProc = None
        # self.mdl6StreamingProc = None; self.mdl6ParsingProc = None
        # self.mdl7StreamingProc = None; self.mdl7ParsingProc = None
        # self.mdl8StreamingProc = None; self.mdl8ParsingProc = None
        # self.mdl9StreamingProc = None; self.mdl9ParsingProc = None
        # self.mdl10StreamingProc = None; self.mdl10ParsingProc = None
        # self.mdl11StreamingProc = None; self.mdl11ParsingProc = None
        # self.mdl12StreamingProc = None; self.mdl12ParsingProc = None
        # self.mdl13StreamingProc = None; self.mdl13ParsingProc = None
        # self.mdl14StreamingProc = None; self.mdl14ParsingProc = None
        # self.mdl15StreamingProc = None; self.mdl15ParsingProc = None


        # self.updatePlotTimer.stop()
        self.updateStatusTableTimer.stop()
        for i in range( nModules ):
            ind = self.tableModel.index(i,0)
            self.tableModel.setData( ind, 'N', QtCore.Qt.DisplayRole )

        self.alreadyDisconnected = True
        self.ui.startacquisitionButton.setEnabled( False )
        self.ui.actionDisconnect.setEnabled( False )

        self.ui.connectCCButton.setText( _translate( "MainWindow", "Reconnect Camera", None) )
        self.ui.connectCCButton.setStyleSheet('QPushButton {color: green;}')

        self.ccLogger.info( 'ending disconnect at {}'.format( time.time() ) )

    def onShutdownTriggered( self ):
        if self.CCConnected:
            self.disconnectCamera()

        for mdl in self.moduleNumbers:
            self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try:
                #self.sCommand.connect( ('192.168.2.{}'.format( mdl ), 11502 ) )
                self.sCommand.connect( ('192.168.3.{}'.format( mdl ), 11502 ) )
            except:
                self.ccLogger.critial( 'module {} command socket failed to connect, can not shut down'.format( mdl) )
                continue
            self.sCommand.sendall('Shutdown\n'.encode() )
            self.ccLogger.info( 'shutdown command set to module {}'.format( mdl) )
            self.sCommand.close()
        self.shutDownSent = True
        return


if __name__ == "__main__":
    # mp.set_start_method('spawn')
    app = QtWidgets.QApplication(sys.argv)
    myapp = CCAcquireData()
    myapp.show()
    sys.exit(app.exec_())
