# -*- coding: utf-8 -*-
'''
current:
    added shut down menu

streamingUI.V0.0.3:
    changed to one process per module, change to user input measurementName,
    the measurementName is used as folder name for saving data.

streamingUI.V0.0.2:
    add plot per chip tab

streamingUI.V0.0.1:
    add menubar
    add clear spectra button

streamingUI.V0.0.0:
    initial setup of the GUI

Created by Haijian Chen for streaming data from CC.
This is only the GUI
'''

from PySide2 import QtCore, QtGui, QtWidgets

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtCore.QCoreApplication.translate(context, text, disambig)

class Ui_CCAcquireDataMainWindow(object):
    def setupUi(self, CCAcquireDataMainWindow):
        CCAcquireDataMainWindow.setObjectName("CCAcquireDataMainWindow")
        CCAcquireDataMainWindow.resize(1024, 800)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(CCAcquireDataMainWindow.sizePolicy().hasHeightForWidth())
        CCAcquireDataMainWindow.setSizePolicy(sizePolicy)

        self.centralwidget = QtWidgets.QWidget(CCAcquireDataMainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.mainWindowGridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.mainWindowGridLayout.setObjectName("mainWindowGridLayout")

        self.measurementNameLabel = QtWidgets.QLabel(self.centralwidget)
        self.measurementNameLabel.setObjectName( "measurementName" )
        # self.measurementNameLabel.setFont(QtGui.QFont("Times",weight=QtGui.QFont.Bold))
        self.mainWindowGridLayout.addWidget( self.measurementNameLabel, 0, 0, 1, 2 )

        self.measurementNameLineEdit = QtWidgets.QLineEdit( self.centralwidget )
        self.measurementNameLineEdit.setObjectName( "measurementNameLineEdit" )
        self.measurementNameLineEdit.setToolTip( 'press Enter to accept file name')
        self.mainWindowGridLayout.addWidget( self.measurementNameLineEdit, 0, 2, 1, 10 )

        # self.saveAsPushButton = QtGui.QPushButton( self.centralwidget )
        # self.saveAsPushButton.setObjectName( _fromUtf8("saveAsPushButton") )
        # self.mainWindowGridLayout.addWidget( self.saveAsPushButton, 0, 10, 1, 2 )

        self.save2UsbCheckBox = QtWidgets.QCheckBox( self.centralwidget )
        self.save2UsbCheckBox.setObjectName( "save2UsbCheckBox" )
        self.save2UsbCheckBox.setChecked( True )
        self.mainWindowGridLayout.addWidget( self.save2UsbCheckBox, 1, 0, 1, 2 )

        self.timedAcquisitionCheckBox = QtWidgets.QCheckBox( self.centralwidget )
        self.timedAcquisitionCheckBox.setObjectName( "timedAcquisitionCheckBox" )
        self.timedAcquisitionCheckBox.setChecked( False )
        self.mainWindowGridLayout.addWidget( self.timedAcquisitionCheckBox, 1, 4, 1, 2 )

        self.acquisitionTimeHourLabel = QtWidgets.QLabel( self.centralwidget )
        self.acquisitionTimeHourLabel.setObjectName(  "acquisitionTimeHourLabel" )
        self.acquisitionTimeHourLabel.setAlignment( QtCore.Qt.AlignRight )
        self.acquisitionTimeHourLabel.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeHourLabel, 1, 6, 1, 1 )

        self.acquisitionTimeHourSpinBox = QtWidgets.QSpinBox( self.centralwidget )
        self.acquisitionTimeHourSpinBox.setMinimum( 0 )
        self.acquisitionTimeHourSpinBox.setMaximum( 1000 )
        self.acquisitionTimeHourSpinBox.setValue( 0 )
        self.acquisitionTimeHourSpinBox.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeHourSpinBox, 1, 7, 1, 1 )


        self.acquisitionTimeMinutesLabel = QtWidgets.QLabel( self.centralwidget )
        self.acquisitionTimeMinutesLabel.setObjectName(  "acquisitionTimeMinutesLabel" )
        self.acquisitionTimeMinutesLabel.setAlignment( QtCore.Qt.AlignRight )
        self.acquisitionTimeMinutesLabel.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeMinutesLabel, 1, 8, 1, 1 )

        self.acquisitionTimeMinutesSpinBox = QtWidgets.QSpinBox( self.centralwidget )
        self.acquisitionTimeMinutesSpinBox.setMinimum( 0 )
        self.acquisitionTimeMinutesSpinBox.setMaximum( 60 )
        self.acquisitionTimeMinutesSpinBox.setValue( 0 )
        self.acquisitionTimeMinutesSpinBox.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeMinutesSpinBox, 1, 9, 1, 1 )

        self.acquisitionTimeSecondsLabel = QtWidgets.QLabel( self.centralwidget )
        self.acquisitionTimeSecondsLabel.setObjectName(  "acquisitionTimeSecondsLabel" )
        self.acquisitionTimeSecondsLabel.setAlignment( QtCore.Qt.AlignRight )
        self.acquisitionTimeSecondsLabel.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeSecondsLabel, 1, 10, 1, 1 )

        self.acquisitionTimeSecondsSpinBox = QtWidgets.QSpinBox( self.centralwidget )
        self.acquisitionTimeSecondsSpinBox.setMinimum( 0 )
        self.acquisitionTimeSecondsSpinBox.setMaximum( 60 )
        self.acquisitionTimeSecondsSpinBox.setValue( 0 )
        self.acquisitionTimeSecondsSpinBox.setEnabled( False )
        self.mainWindowGridLayout.addWidget( self.acquisitionTimeSecondsSpinBox, 1, 11, 1, 1 )


        self.CCStatusTableView = QtWidgets.QTableView( self.centralwidget )
        self.CCStatusTableView.setObjectName( "CCStatusTableView")
        self.mainWindowGridLayout.addWidget( self.CCStatusTableView, 2, 0, 6, 12)

        self.connectCCButton = QtWidgets.QPushButton( self.centralwidget )
        self.connectCCButton.setObjectName( "connectCCButton" )
        # self.connectCCButton.setSizePolicy( QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding )
        self.connectCCButton.setSizePolicy( QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred )
        self.mainWindowGridLayout.addWidget( self.connectCCButton, 8, 0, 2, 2 )

        self.startacquisitionButton = QtWidgets.QPushButton( self.centralwidget )
        self.startacquisitionButton.setObjectName( "startacquisitionButton")
        self.startacquisitionButton.setSizePolicy( QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred )
        self.mainWindowGridLayout.addWidget( self.startacquisitionButton, 8, 4, 2, 2  )

        self.clearSpectraButton = QtWidgets.QPushButton( self.centralwidget )
        self.clearSpectraButton.setObjectName(  "clearSpectraButton" )
        self.clearSpectraButton.setSizePolicy( QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred )
        self.mainWindowGridLayout.addWidget( self.clearSpectraButton, 8, 8, 2, 2  )


        self.plottingTab = QtWidgets.QTabWidget(self.centralwidget)
        self.plottingTab.setObjectName("plottingTab")
        self.combinedSpecTab = QtWidgets.QWidget()
        self.combinedSpecTab.setObjectName("combinedSpecTab")
        self.plottingTab.addTab(self.combinedSpecTab, "")
        self.selectedModuleSpecTab = QtWidgets.QWidget()
        self.selectedModuleSpecTab.setObjectName("selectedModuleSpecTab")
        self.plottingTab.addTab(self.selectedModuleSpecTab, "")
        self.perChipSpecTab = QtWidgets.QWidget()
        self.perChipSpecTab.setObjectName("perChipSpecTab")
        self.plottingTab.addTab(self.perChipSpecTab, "")
        self.tbaTab = QtWidgets.QWidget()
        self.tbaTab.setObjectName("tbaTab")
        self.plottingTab.addTab(self.tbaTab, "")
        self.plottingTab.setCurrentIndex(0)
        self.mainWindowGridLayout.addWidget( self.plottingTab, 10, 0, 10, 12 )

        CCAcquireDataMainWindow.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(CCAcquireDataMainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1024, 25))
        self.menubar.setObjectName("menubar")
        self.menuCamera = QtWidgets.QMenu(self.menubar)
        self.menuCamera.setObjectName("menuCamera")
        # self.menubar.setNativeMenuBar( True )
        self.actionConnect = QtWidgets.QAction(CCAcquireDataMainWindow)
        self.actionConnect.setObjectName("actionConnect")
        self.actionDisconnect = QtWidgets.QAction(CCAcquireDataMainWindow)
        self.actionDisconnect.setObjectName("actionDisconnect")
        self.actionShutdown = QtWidgets.QAction( CCAcquireDataMainWindow )
        self.actionShutdown.setObjectName("actionShutdown")
        self.menuCamera.addAction(self.actionConnect)
        self.menuCamera.addAction(self.actionDisconnect)
        self.menuCamera.addAction(self.actionShutdown)
        self.menubar.addAction(self.menuCamera.menuAction() )
        CCAcquireDataMainWindow.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar(CCAcquireDataMainWindow)
        self.statusbar.setObjectName("statusbar")
        CCAcquireDataMainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(CCAcquireDataMainWindow)
        QtCore.QMetaObject.connectSlotsByName(CCAcquireDataMainWindow)



    def retranslateUi(self, CCAcquireDataMainWindow):
        CCAcquireDataMainWindow.setWindowTitle(_translate("CCAcquireDataMainWindow", "CC Acquiring Data", None))
        self.measurementNameLabel.setText( _translate("CCAcquireDataMainWindow", "Measurement Name:", None) )
        # self.saveAsPushButton.setText( _translate("CCAcquireDataMainWindow", "Open", None) )
        self.save2UsbCheckBox.setText( _translate("CCAcquireDataMainWindow", "Save to USB", None) )
        self.timedAcquisitionCheckBox.setText( _translate("CCAcquireDataMainWindow", "Timed Acquisition", None) )
        self.acquisitionTimeHourLabel.setText( _translate("CCAcquireDataMainWindow", "Hour:", None) )
        self.acquisitionTimeMinutesLabel.setText( _translate("CCAcquireDataMainWindow", "Minutes:", None) )
        self.acquisitionTimeSecondsLabel.setText( _translate("CCAcquireDataMainWindow", "Seconds:", None) )
        self.connectCCButton.setText( _translate("CCAcquireDataMainWindow", "Connect Camera", None) )
        self.startacquisitionButton.setText( _translate("CCAcquireDataMainWindow", "Start", None))
        self.clearSpectraButton.setText( _translate("CCAcquireDataMainWindow", "Clear Spectra", None))
        self.plottingTab.setTabText(self.plottingTab.indexOf(self.combinedSpecTab), _translate("CCAcquireDataMainWindow", "Combined Spec", None))
        self.plottingTab.setTabText(self.plottingTab.indexOf(self.selectedModuleSpecTab), _translate("CCAcquireDataMainWindow", "selected Module Spec", None))
        self.plottingTab.setTabText(self.plottingTab.indexOf(self.perChipSpecTab), _translate("CCAcquireDataMainWindow", "per Chip Spec", None))
        self.plottingTab.setTabText(self.plottingTab.indexOf(self.tbaTab), _translate("CCAcquireDataMainWindow", "tba", None))


        self.menuCamera.setTitle(_translate("CCAcquireDataMainWindow", "&Camera", None) )
        self.actionConnect.setText(_translate("CCAcquireDataMainWindow", "Co&nnect", None))
        self.actionDisconnect.setText(_translate("CCAcquireDataMainWindow", "&Disconnect", None))
        self.actionShutdown.setText(_translate("CCAcquireDataMainWindow", "&Shutdown", None))
