import struct
import numpy as np
def readStreamFile( fn, moduleNumber ):
    fid = open( fn, 'rb' )
    b = fid.read()
    fid.close()

    N = len( b )
#    if N > 1:
#        for i in range( 1, N ):
#            b = b + a[i]


    events = []
    sync = []
    pos = 0
    while pos < N :
        if b[pos] == 122:
            if pos + 18 > len( b ):
                break
            pos += 2
            syncIndex, syncTime = struct.unpack( '>QQ', b[ pos:pos + 16 ] )
            pos += 16
            sync.append( [ moduleNumber, syncIndex, syncTime ] )
            # events.append( temp )
        elif b[pos] == 0:
            if pos + 24 > len( b ):
                break
            pos += 24
        else:
            npx = b[pos]
            if pos + 1 + 8 + npx * 17 > len( b ):
                break
            pos += 1
            t = struct.unpack( '>Q', b[ pos:pos + 8 ] )[0]
            pos += 8
            for i in range( npx ):
                chip, x, y, z, edep = struct.unpack( '>Bllll', b[ pos:pos + 17 ] )
                pos += 17
                temp = [npx, moduleNumber, chip, edep, x, y, z, t]
                events.append( temp )
    return np.array( events ), np.array( sync )

def readStreamFile2( fn, moduleNumber ):
    fid = open( fn, 'rb' )
    b = fid.read()
    fid.close()

    N = len( b )
   # if N > 1:
       # for i in range( 1, N ):
           # b = b + a[i]


    events = []
    sync = []
    pos = 0
    while pos < N :
        if b[pos] == 0:
            if pos + 18 > len( b ):
                break
            pos += 1
            moduleNumber = b[pos]
            pos += 1
            syncIndex, syncTime = struct.unpack( '>QQ', b[ pos:pos + 16 ] )
            pos += 16
            sync.append( [ moduleNumber, syncIndex, syncTime ] )

        else:
            npx = b[pos]
            if pos + 1 + 8 + npx * 17 > len( b ):
                break
            pos += 1
            moduleNumber = b[pos]
            pos += 1
            t = struct.unpack( '>Q', b[ pos:pos + 8 ] )[0]
            pos += 8
            for i in range( npx ):
                chip, x, y, z, edep = struct.unpack( '>Bllll', b[ pos:pos + 17 ] )
                pos += 17
                temp = [npx, moduleNumber, chip, edep, x, y, z, t]
                events.append( temp )
    
    return np.array( events ), np.array( sync )
