from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(name='preProccess Library',
      ext_modules=cythonize("preProcess.pyx"),
      include_dirs=[numpy.get_include()]
     )
