'''
Reference python script containing functions from "CCStreamingSaveOnly_NoParsing.py" related to connecting to PolarisJ detectors
and streaming data from them.

Compiled by Nicholas Hyslop for Tom Leadbeater.

18 October 2019
'''


'''
This is the thread dedicated to streaming data from ONE module
'''
class streamingProcess( mp.Process ):
    '''
    This is a process dedicated to stream data from a module specified by module number.

    input:
        moduleNumber:
            module number to identify which module to stream data from.
        moduleStatus:
            a shared array defined in main process, the length of
            the array equals to the number of modules. It is initilized to 0 when the main
            process starts.
            IN THIS THREAD, when a data socket is successfully established to
            the specified module, the corresponding element in moduleStatus is
            set to 1, so other processes/threads knows data connection to this
            module has been established.
        disconnectModule:
            also a shared array defined in main process. Initally, all elements
            are set 0 when the main process starts. It remains 0 until the
            disconnectCamera button is clicked, then the main process sets
            it elements to 1. It is passed from main proces to child streaming process.
            When streaming process sees the element corresponding to this module is set to 1,
            the streaming process closes the streaming data scocket and then terminate.
        streamingQ
            streamingQ is a queue shared between streaming and parsing processes for this module
            ( identified by the module number ), data get from the camera is pushed
            into this queue, and then the parsing process pops data from the queue.
    '''

    def __init__(   self, moduleNumber, moduleStatus, disconnectModule,
                    acquring, writeFileNowTrigger, savingFolderName, savingMode, writeFileDone ):
        super( streamingProcess, self).__init__()

        self.nm = '{} streaming process'.format( moduleNumber )
        self.loggerP = logging.getLogger( self.nm )
        self.loggerP.setLevel( logging.DEBUG )
        self.loggerP_hndlr = logging.StreamHandler()
        self.loggerP_hndlr.setLevel(logging.DEBUG)
        self.loggerP_fmtr = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.loggerP_hndlr.setFormatter( self.loggerP_fmtr )
        self.loggerP.addHandler( self.loggerP_hndlr )

        self.moduleNumber = moduleNumber
        self.moduleStatus = moduleStatus
        self.disconnectModule = disconnectModule

        # now try to connect to module specified by moduleNumber:
        self.sData = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

        # self.sData.settimeout( 5.0 )
        #self.moduleIP = '192.168.2.' + str( self.moduleNumber )
        self.moduleIP = '192.168.3.' + str( self.moduleNumber )

        # if the module is not currently connected and it is not asked to disconnect:
        if self.moduleStatus[ self.moduleNumber - 51 ] == 0 and self.disconnectModule[ self.moduleNumber - 51 ] == 0:
            try:
                self.sData.connect( (self.moduleIP, 11503) )
                self.moduleStatus[ self.moduleNumber - 51 ] = 1
                self.sData.setblocking(0)
                self.loggerP.info( 'data socket established' )
            except OSError:
                self.loggerP.critical( 'data socket failed to establish' )
                # print( 'module ', self.moduleNumber, ' failed to connect' )
                self.moduleStatus[ self.moduleNumber - 51 ] = 0

        else:
            # need to add some exceptions here:
            if self.moduleStatus[ self.moduleNumber - 51 ] != 0:
                self.loggerP.debug( 'the data socket already established')
            elif self.disconnectModule[ self.moduleNumber - 51 ] != 0:
                self.loggerP.debug( 'a disconnecting command is in process' )

            raise OSError('module already connected or disconnect module requested')

        # self.data = b''
        self.data = bytearray( nBytes )
        self.nRecv = 0

        self.bSave = bytearray( nBa )
        self.bLen = 0

        self.acquring = acquring
        self.writeFileNowTrigger = writeFileNowTrigger
        self.savingFolderName = savingFolderName
        self.savingMode = savingMode
        self.writeFileDone = writeFileDone

        self.savingFileName = None
        self.fid = None



        # self.fid = open( './data/' + str(self.moduleNumber) + '.dat', 'wb' )
        # self.dataSave = b''


    def run( self ):
        # keep streaming as long as the not disconnectCamera button is not clicked:
        while self.disconnectModule[ self.moduleNumber - 51 ] == 0:
            # self.data = b''
            try:
                ready = select.select( [self.sData], [], [] )
                if ready[0]:
                    # self.data = self.sData.recv( nBytes )
                    # self.streamingQ.put( self.data )
                    self.nRecv = self.sData.recv_into( self.data )

                    if self.acquring.value and self.writeFileNowTrigger[self.moduleNumber - 51] == 0:
                        self.bSave[self.bLen:self.bLen+self.nRecv] = self.data[:self.nRecv]
                        self.bLen += self.nRecv

                    elif self.writeFileNowTrigger[self.moduleNumber - 51] != 0:
                        self.savingFileName = self.savingFolderName.value.decode() + 'mod' + str( self.moduleNumber ) + '.dat'
                        if self.savingMode.value.decode() == 'new':
                            self.fid = open( self.savingFileName, 'wb' )
                        elif self.savingMode.value.decode() == 'append':
                            self.fid = open( self.savingFileName, 'ab' )

                        self.fid.write( self.bSave[:self.bLen] )
                        self.fid.close()

                        self.bLen = 0
                        self.writeFileDone[ self.moduleNumber - 51 ] = 1
                        self.writeFileNowTrigger[self.moduleNumber - 51] = 0


            except socket.timeout:
                print( 'module ', self.moduleNumber, 'receving timed out' )
                break

        # when disconnectCamera button is clicked, close the data socket and
        # change moduleStatus:
        self.sData.close()
        self.moduleStatus[ self.moduleNumber - 51 ] = 0

        self.loggerP.info( ' streaming process finshed' )
        # self.fid.write( self.dataSave )
        # self.fid.close()


'''
These are functions taken from the GUI class that connects the detectors and starts/stops the data aquisition.
'''
# @QtCore.Slot()
def onConnectCCButtonClicked( self ):
    # when program starts, the camera is not connected
    # the click would connect the camera:
    if not self.CCConnected:
        # enable/disable sub-mm pixel position for each module:
        #   Calibration standard to turn sub-mm pixel positioning on
        #   Calibration AllCalibration to turn sub-mm pixel positioning off
        for mdlNumber in self.moduleNumbers:
            ip = '192.168.3.{}'.format( mdlNumber )
            self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            self.sCommand.connect( (ip, 11502 ) )
            #self.sCommand.sendall('Calibration standard\n'.encode() )
            self.sCommand.sendall('Calibration AllCalibration\n'.encode() )
            self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
            self.sCommand.close()

        # reset sync pulse index for each module:
        for mdlNumber in self.moduleNumbers:
            #ip = '192.168.2.{}'.format( mdlNumber )
            ip = '192.168.3.{}'.format( mdlNumber )
            self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            self.sCommand.connect( (ip, 11502 ) )
            self.sCommand.sendall('ResetIndex\n'.encode() )
            self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
            self.sCommand.close()

        # start sync pulse from master module:
        #   disable Start Sync Pulse for external timing
        self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        #self.sCommand.connect( ('192.168.2.{}'.format( self.master ), 11502 ) )
        self.sCommand.connect( ('192.168.3.{}'.format( self.master ), 11502 ) )
        self.sCommand.sendall('Start\n'.encode() )
        #self.ccLogger.info( '192.168.2.{} '.format( self.master ) + self.sCommand.recv( 1024 ).decode() )
        self.ccLogger.info( '192.168.3.{} '.format( self.master ) + self.sCommand.recv( 1024 ).decode() )
        self.sCommand.close()

        # if it's reconnecting from a previously disconected state, need to re-initiate the streaming and parsing
        # processes for each module:
        if self.alreadyDisconnected:
            self.acquring.value = False
            for i in range( nModules ):
                self.moduleStatus[i] = 0
                self.disconnectModule[i] = 0
                self.writeFileNowTrigger[i] = 0
                self.writeFileDone[i] = 0

            for i in range( eDepMax ):
                self.specCombined[i] = 0
                self.specMdl0[i] = 0
                self.specMdl0Chip0[i] = 0; self.specMdl0Chip1[i] = 0; self.specMdl0Chip2[i] = 0; self.specMdl0Chip3[i] = 0;
                self.specMdl1[i] = 0
                self.specMdl1Chip0[i] = 0; self.specMdl1Chip1[i] = 0; self.specMdl1Chip2[i] = 0; self.specMdl1Chip3[i] = 0;
                self.specMdl2[i] = 0
                self.specMdl2Chip0[i] = 0; self.specMdl2Chip1[i] = 0; self.specMdl2Chip2[i] = 0; self.specMdl2Chip3[i] = 0;
                self.specMdl3[i] = 0
                self.specMdl3Chip0[i] = 0; self.specMdl3Chip1[i] = 0; self.specMdl3Chip2[i] = 0; self.specMdl3Chip3[i] = 0;
                self.specMdl4[i] = 0
                self.specMdl4Chip0[i] = 0; self.specMdl4Chip1[i] = 0; self.specMdl4Chip2[i] = 0; self.specMdl4Chip3[i] = 0;
                self.specMdl5[i] = 0
                self.specMdl5Chip0[i] = 0; self.specMdl5Chip1[i] = 0; self.specMdl5Chip2[i] = 0; self.specMdl5Chip3[i] = 0;
                self.specMdl6[i] = 0
                self.specMdl6Chip0[i] = 0; self.specMdl6Chip1[i] = 0; self.specMdl6Chip2[i] = 0; self.specMdl6Chip3[i] = 0;
                self.specMdl7[i] = 0
                self.specMdl7Chip0[i] = 0; self.specMdl7Chip1[i] = 0; self.specMdl7Chip2[i] = 0; self.specMdl7Chip3[i] = 0;
                self.specMdl8[i] = 0
                self.specMdl8Chip0[i] = 0; self.specMdl8Chip1[i] = 0; self.specMdl8Chip2[i] = 0; self.specMdl8Chip3[i] = 0;
                self.specMdl9[i] = 0
                self.specMdl9Chip0[i] = 0; self.specMdl9Chip1[i] = 0; self.specMdl9Chip2[i] = 0; self.specMdl9Chip3[i] = 0;
                self.specMdl10[i] = 0
                self.specMdl10Chip0[i] = 0; self.specMdl10Chip1[i] = 0; self.specMdl10Chip2[i] = 0; self.specMdl10Chip3[i] = 0;
                self.specMdl11[i] = 0
                self.specMdl11Chip0[i] = 0; self.specMdl11Chip1[i] = 0; self.specMdl11Chip2[i] = 0; self.specMdl11Chip3[i] = 0;
                self.specMdl12[i] = 0
                self.specMdl12Chip0[i] = 0; self.specMdl12Chip1[i] = 0; self.specMdl12Chip2[i] = 0; self.specMdl12Chip3[i] = 0;
                self.specMdl13[i] = 0
                self.specMdl13Chip0[i] = 0; self.specMdl13Chip1[i] = 0; self.specMdl13Chip2[i] = 0; self.specMdl13Chip3[i] = 0;
                self.specMdl14[i] = 0
                self.specMdl14Chip0[i] = 0; self.specMdl14Chip1[i] = 0; self.specMdl14Chip2[i] = 0; self.specMdl14Chip3[i] = 0;
                self.specMdl15[i] = 0
                self.specMdl15Chip0[i] = 0; self.specMdl15Chip1[i] = 0; self.specMdl15Chip2[i] = 0; self.specMdl15Chip3[i] = 0;
        if 51 in self.moduleNumbers:
            try:
                self.mdl0StreamingProc = streamingProcess(  51, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 51 streaming process')
        if 52 in self.moduleNumbers:
            try:
                self.mdl1StreamingProc = streamingProcess(  52, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 52 streaming process')
        if 53 in self.moduleNumbers:
            try:
                self.mdl2StreamingProc = streamingProcess(  53, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 53 streaming process')
        if 54 in self.moduleNumbers:
            try:
                self.mdl3StreamingProc = streamingProcess(  54, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 54 streaming process')
        if 55 in self.moduleNumbers:
            try:
                self.mdl4StreamingProc = streamingProcess(  55, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate muodlue 55 streaming process')
        if 56 in self.moduleNumbers:
            try:
                self.mdl5StreamingProc = streamingProcess(  56, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 56 streaming process')
        if 57 in self.moduleNumbers:
            try:
                self.mdl6StreamingProc = streamingProcess(  57, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 57 streaming process')
        if 58 in self.moduleNumbers:
            try:
                self.mdl7StreamingProc = streamingProcess(  58, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 58 streaming process')
        if 59 in self.moduleNumbers:
            try:
                self.mdl8StreamingProc = streamingProcess(  59, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 59 streaming process')
        if 60 in self.moduleNumbers:
            try:
                self.mdl9StreamingProc = streamingProcess(  60, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 60 streaming process')
        if 61 in self.moduleNumbers:
            try:
                self.mdl10StreamingProc = streamingProcess(  61, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 61 streaming process')
        if 62 in self.moduleNumbers:
            try:
                self.mdl11StreamingProc = streamingProcess(  62, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 62 streaming process')
        if 63 in self.moduleNumbers:
            try:
                self.mdl12StreamingProc = streamingProcess(  63, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 63 streaming process')
        if 64 in self.moduleNumbers:
            try:
                self.mdl13StreamingProc = streamingProcess(  64, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 64 streaming process')
        if 65 in self.moduleNumbers:
            try:
                self.mdl14StreamingProc = streamingProcess(  65, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 65 streaming process')
        if 66 in self.moduleNumbers:
            try:
                self.mdl15StreamingProc = streamingProcess(  66, self.moduleStatus, self.disconnectModule,
                                                            self.acquring, self.writeFileNowTrigger, self.savingFolderName, self.savingMode, self.writeFileDone)
            except:
                raise Exception( 'failed to initiate module 66 streaming process')

        self.alreadyDisconnected = False

        # start the child process for each module:
        if 51 in self.moduleNumbers:
            self.mdl0StreamingProc.start(); # self.mdl0ParsingProc.start()
        if 52 in self.moduleNumbers:
            self.mdl1StreamingProc.start(); # self.mdl1ParsingProc.start()
        if 53 in self.moduleNumbers:
            self.mdl2StreamingProc.start(); # self.mdl2ParsingProc.start()
        if 54 in self.moduleNumbers:
            self.mdl3StreamingProc.start(); # self.mdl3ParsingProc.start()
        if 55 in self.moduleNumbers:
            self.mdl4StreamingProc.start(); # self.mdl4ParsingProc.start()
        if 56 in self.moduleNumbers:
            self.mdl5StreamingProc.start(); # self.mdl5ParsingProc.start()
        if 57 in self.moduleNumbers:
            self.mdl6StreamingProc.start(); # self.mdl6ParsingProc.start()
        if 58 in self.moduleNumbers:
            self.mdl7StreamingProc.start(); # self.mdl7ParsingProc.start()
        if 59 in self.moduleNumbers:
            self.mdl8StreamingProc.start(); # self.mdl8ParsingProc.start()
        if 60 in self.moduleNumbers:
            self.mdl9StreamingProc.start(); # self.mdl9ParsingProc.start()
        if 61 in self.moduleNumbers:
            self.mdl10StreamingProc.start(); # self.mdl10ParsingProc.start()
        if 62 in self.moduleNumbers:
            self.mdl11StreamingProc.start(); # self.mdl11ParsingProc.start()
        if 63 in self.moduleNumbers:
            self.mdl12StreamingProc.start(); # self.mdl12ParsingProc.start()
        if 64 in self.moduleNumbers:
            self.mdl13StreamingProc.start(); # self.mdl13ParsingProc.start()
        if 65 in self.moduleNumbers:
            self.mdl14StreamingProc.start(); # self.mdl14ParsingProc.start()
        if 66 in self.moduleNumbers:
            self.mdl15StreamingProc.start(); # self.mdl15ParsingProc.start()

        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl0StreamingProc.pid, self.mdl0ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl1StreamingProc.pid, self.mdl1ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl2StreamingProc.pid, self.mdl2ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl3StreamingProc.pid, self.mdl3ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl4StreamingProc.pid, self.mdl4ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl5StreamingProc.pid, self.mdl5ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl6StreamingProc.pid, self.mdl6ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl7StreamingProc.pid, self.mdl7ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl8StreamingProc.pid, self.mdl8ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl9StreamingProc.pid, self.mdl9ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl10StreamingProc.pid, self.mdl10ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl11StreamingProc.pid, self.mdl11ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl12StreamingProc.pid, self.mdl12ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl13StreamingProc.pid, self.mdl13ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl14StreamingProc.pid, self.mdl14ParsingProc.pid ) )
        # self.ccLogger.info( 'streaming pid {}, parsing pid {}'.format( self.mdl15StreamingProc.pid, self.mdl15ParsingProc.pid ) )

        # it takes a short period for each per module process to establish
        # a data socket, so give it time delay:
        time.sleep(2)
        self.ccLogger.info( 'reconnecting, module status sum is {}'.format( sum( self.moduleStatus) ) )

        # when module status for each module is set to 1 ( i.e. socket
        # established successfully ):
        if sum( self.moduleStatus ) == len(self.moduleNumbers):
            self.CCConnected = True
            for i in range( nModules ):
                ind = self.tableModel.index(i,0)
                self.tableModel.setData( ind, 'Y', QtCore.Qt.DisplayRole )

            # enable the start acquistion button
            self.ui.startacquisitionButton.setEnabled( True )
            self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: green;}')
            # enable disconnect menu
            self.ui.actionDisconnect.setEnabled( True )
            # change button to disconnect camera
            self.ui.connectCCButton.setText( _translate( "MainWindow", "Disconnect Camera", None) )
            self.ui.connectCCButton.setStyleSheet('QPushButton {color: red;}')

            # start update plot and status table
            # self.updatePlotTimer.start( refreshSpecPlotPeriod )
            self.updateStatusTableTimer.start( refreshStatusTablePeriod )

        # not all modules are connected successfully
        else:
            for i in range( nModules ):
                if self.moduleStatus[i] != 1:
                    self.ccLogger.critical( 'data socket on module {} failed to connect'.format( self.moduleStatus[i]) )
            self.disconnectCamera()

    # when the camera is already connected, this click should disconnect
    else:
        self.disconnectCamera()


def onStartAcquisitionButtonClicked( self ):

    # when the camera is acquring, click the button will stop acquisition
    if self.acquring.value:
        # set to False to stop sending parsed data to savingQ
        self.acquring.value = False
        if self.ui.timedAcquisitionCheckBox.isChecked():
            self.autoStopTimer.stop()
        # set write file trigger to 1 for each module to set off write file event:
        self.ccLogger.info( '{} setting write file trigger'.format( time.time() ) )
        for i in range( nModules ):
            self.writeFileNowTrigger[i] = 1

        # wait until all files are written:
        while sum( self.writeFileDone ) != len(self.moduleNumbers):
            time.sleep( 0.5 )

        """
        # --------------------------------------------------------------------------------------------------
        # Convert data to txt format
        # --------------------------------------------------------------------------------------------------
        print ('\n--- Running combine_dat.py ---\n')

        # Get .dat file directory
        INPUT_DIR = self.savingFolderName.value.decode()

        # Merge binaries
        print("Merging data arrays...\n")
        print()
        comb_dat.merge_dats(INPUT_DIR)
        print()
        print('Merging complete.')
        # --------------------------------------------------------------------------------------------------
        """

        for i in range( nModules ):
            self.writeFileNowTrigger[i] = 0
            self.writeFileDone[i] = 0

        if self.ui.save2UsbCheckBox.isChecked():
            for moduleNumber in self.moduleNumbers:
                #ip = '192.168.2.' + str( moduleNumber )
                ip = '192.168.3.' + str( moduleNumber )
                self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                self.sCommand.connect( (ip, 11502 ) )
                self.sCommand.sendall( 'SaveBEF \n'.encode() )
                self.ccLogger.info( ip + self.sCommand.recv( 1024 ).decode() )
                self.sCommand.close()


        self.ui.startacquisitionButton.setText( _translate( "MainWindow", "Start", None ) )
        self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: green;}')
        self.ui.measurementNameLabel.setDisabled( False )
        self.ui.measurementNameLineEdit.setDisabled( False )
        self.ui.save2UsbCheckBox.setDisabled( False )
        self.ui.timedAcquisitionCheckBox.setDisabled( False )
        self.ui.acquisitionTimeHourLabel.setDisabled( False )
        self.ui.acquisitionTimeHourSpinBox.setDisabled( False )
        self.ui.acquisitionTimeMinutesLabel.setDisabled( False )
        self.ui.acquisitionTimeMinutesSpinBox.setDisabled( False )
        self.ui.acquisitionTimeSecondsLabel.setDisabled( False )
        self.ui.acquisitionTimeSecondsSpinBox.setDisabled( False )
        self.ui.connectCCButton.setDisabled( False )

        # time.sleep(5)
        # self.onConnectCCButtonClicked()

    # when it is not currently acquring, click the button will start acquisition
    else:
        if self.CCConnected:

            fn = self.ui.measurementNameLineEdit.text()
            if len( fn ) < 1:
                self.messageBox = QtWidgets.QMessageBox()
                self.messageBox.setWindowTitle( 'Measurement name' )
                self.messageBox.setText( 'Measurement name can not be blank!' )
                self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok )
                if self.messageBox.exec_() == QtWidgets.QMessageBox.Ok:
                    return
            if not fn.endswith( '/' ):
                fn = fn + '/'

            self.savingFolderName.value = ( defaultDataFolder + fn ).encode()

            if os.path.isdir( self.savingFolderName.value ):
                self.messageBox = QtWidgets.QMessageBox()
                self.messageBox.setWindowTitle( 'Measurement name' )
                self.messageBox.setText( 'The measurement name has been used!' )
                append_button = self.messageBox.addButton( 'Append', QtWidgets.QMessageBox.YesRole )
                overwrite_button = self.messageBox.addButton( 'Overwrite', QtWidgets.QMessageBox.YesRole )
                self.messageBox.addButton( QtWidgets.QMessageBox.Cancel )
                results = self.messageBox.exec_()
                if results == QtWidgets.QMessageBox.Cancel:
                    print( 'Cancel button clicked')
                    return
                elif self.messageBox.clickedButton() == overwrite_button:
                    self.savingMode.value = 'new'.encode()
                elif self.messageBox.clickedButton() == append_button:
                    self.savingMode.value = 'append'.encode()
                else:
                    pass
            else:
                os.mkdir( self.savingFolderName.value )

            if self.ui.timedAcquisitionCheckBox.isChecked():
                self.acqTime =  1000 * int(
                                self.ui.acquisitionTimeHourSpinBox.value() * 3600 +
                                self.ui.acquisitionTimeMinutesSpinBox.value() * 60 +
                                self.ui.acquisitionTimeSecondsSpinBox.value() )
                self.autoStopTimer.start( self.acqTime )

            # self.acquring.value = True

            fn = self.ui.measurementNameLineEdit.text()
            if fn.endswith( '/' ):
                fn = fn[:-1]

            if self.ui.save2UsbCheckBox.isChecked():
                for moduleNumber in self.moduleNumbers:
                    #ip = '192.168.2.' + str( moduleNumber )
                    ip = '192.168.3.' + str( moduleNumber )
                    self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                    self.sCommand.connect( (ip, 11502 ) )
                    cmd = 'SaveBEF ' + fn +'\n'
                    self.sCommand.sendall( cmd.encode() )
                    print( ip, self.sCommand.recv( 1024 ).decode() )
                    self.sCommand.close()

            self.acquring.value = True
            #     cmd = 'SaveBEF ' + os.path.splitext( os.path.basename( self.saveFileName) )[0] +'\n'
            #     print( cmd, ' command to be sent')
            #     ip = '192.168.2.53'
            #     self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            #     self.sCommand.connect( (ip, 11502 ) )
            #     self.sCommand.sendall( cmd.encode() )
            #     print( ip, self.sCommand.recv( 1024 ).decode() )
            #     self.sCommand.close()
            #
            #     ip = '192.168.2.66'
            #     self.sCommand = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            #     self.sCommand.connect( (ip, 11502 ) )
            #     self.sCommand.sendall( cmd.encode() )
            #     print( ip, self.sCommand.recv( 1024 ).decode() )
            #     self.sCommand.close()

            self.ui.startacquisitionButton.setText( _translate( "MainWindow", "Stop", None ) )
            self.ui.startacquisitionButton.setStyleSheet('QPushButton {color: red;}')

            # while saving the data, the following are not allowed:
            self.ui.measurementNameLabel.setDisabled( True )
            self.ui.measurementNameLineEdit.setDisabled( True )
            self.ui.save2UsbCheckBox.setDisabled( True )
            self.ui.timedAcquisitionCheckBox.setDisabled( True )
            self.ui.acquisitionTimeHourLabel.setDisabled( True )
            self.ui.acquisitionTimeHourSpinBox.setDisabled( True )
            self.ui.acquisitionTimeMinutesLabel.setDisabled( True )
            self.ui.acquisitionTimeMinutesSpinBox.setDisabled( True )
            self.ui.acquisitionTimeSecondsLabel.setDisabled( True )
            self.ui.acquisitionTimeSecondsSpinBox.setDisabled( True )
            self.ui.connectCCButton.setDisabled( True )

        else:
            self.messageBox = QtWidgets.QMessageBox()
            self.messageBox.setWindowTitle( 'Starting acquisition' )
            self.messageBox.setText( 'Camera is not connected' )
            self.messageBox.setStandardButtons( QtWidgets.QMessageBox.Ok )
            if self.messageBox.exec() == QtWidgets.QMessageBox.Ok:
                return
