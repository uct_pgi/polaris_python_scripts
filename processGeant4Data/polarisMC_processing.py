#!/usr/bin/python
"""
Description:
  Python script that reads in simulated data (ROOT files) from POLARIS3 Geant4 model
    and can:
             does Big Energy sorting on the data
             do Compton line filtering on the data
             store source origin data and scatter tags to output file
             remove non-physical events (double scatters only)
             apply minimum energy threshold
             randomly shuffle events (doubles & triples only)
             creates csv data file
             output lots of interesting plots
             compute weighting potential - not yet functioning

  To Run: python polarisMC_processing.py &> output-log.txt &
    From main program, call function: filter_data(data_dir, root_tree_name, output_file_name)
              data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
              root_tree_name      - Required : Name of ROOT tree to read data from (Str)
              USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    Data (and plots) will be saved to the given data directory and plots are
      saved to the following directories (plots_raw, plots_transformed, plots_final_doubles)

    Additional settings are handled through Global Variable under SETTINGS / PARAMETERS (just before main)
      - PRODUCE_RAW_PLOTS                   = True (create raw plots) / False (do not create plots)
      - PRODUCE_TRANSFORMED_PLOTS           = True (create transformed plots) / False (do not create plots)
      - PRODUCE_FINAL_DOUBLE_PLOTS          = True (create final double energy plots) / False (do not create plots)
      - STORE_SINGLE_SCATTERS               = True (store -1x.csv files) / False (do not store, default), stores single-scatter data to file scatters_1x.csv
      - STORE_ORIGINS_TAGS                  = True (store -origins-tags.csv files) / False (do not store, default), duplicate files for scatters_2x.csv which include origin location and scatter tags
      - APPLY_BIG_ENERGY_SORTING            = apply big energy sorting (automatically arrange events so that bigger energy deposited comes first, default is False)
      - APPLY_ENERGY_THRESHOLD              = apply minimum energy threshold [not very effective, need to move it sooner in code] temp fix to add as min energy for alternate plots
      - MINIMUM_ENERGY_THRESHOLD            = set minimum energy threshold in MeV
      - REMOVE_UNPHYSICAL_EVENTS            = remove unphysical events, i.e. Compton scatter angle == nan (checks both ordering of two scatters and flips if original ordering in unphysical, default is False)
      - APPLY_COMPTON_LINE_FILTERING        = apply Compton line filtering (checks if E1 and theta1 are consistent with Compton formula, default is False)
      - COMPTON_LINE_RANGE                  = sets accepted energy range (min / max values (percentage) for Compton line filtering)
      - COMPTON_LINE_ENERGIES               = sets expected gamma energies to use for filtering (can take multiple arguments, see list of options below)
      - SHUFFLE_SCATTER_EVENTS              = shuffle the 2x & 3x scatter events (to overcome timing errors like runs 6, 7, 8 for 180315 data, default is False)
      - APPLY_WEIGHTING_POTENTIAL           = adjust energy deposited on the weighting potential method for signal induction

    Possible gamma energies for Compton Line Filtering
      - Oxygen Prompts: 2.742, 5.240, 6.129, 6.916, 7.116
      - Carbon: 4.444
      - Nitrogen: 1.635, 2.313, 5.269, 5.298
      - Boron: 0.718
      - Cobalt-60: 1.173, 1.332
      - Cesium-137: 0.6617
      - Sodium-22 : 0.5110

NOTES:
- written using python v2.7.15 / updated to also work in python 3.6.2
- processPolarisData_utils.pyx required to run this code
- some aspects of the code only function for double-scatters

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org> & Nicholas Hyslop <HYSNIC007@myuct.ac.za>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "January 28, 2020"
__version__ = "$Revision: 2.0.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import os
import sys
import timeit

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pandas as pd
import uproot

# Import cython utils library from polaris_data_processor
import pyximport; pyximport.install(setup_args={"include_dirs":np.get_include()}, language_level=3)
# sys.path.append('/Users/nicholas/Google Drive/Work/code/polaris/polaris_data_processor/')       # Nicholas latop
# sys.path.append('/home/nicholas/polarisPEPT/polaris/polaris_data_processor/')                 # Steve server
# sys.path.append('/home/steve/PGI/code/polaris/polaris_data_processor/')                 # Steve server
# sys.path.append('/Volumes/BATMAN/PGI/code/polaris/polaris_data_processor/')                 # Steve laptop
# sys.path.append('/Volumes/GoogleDrive/My\ Drive/BATMAN/PGI/code/polaris/polaris_data_processor/')                 # Steve laptop
#sys.path.append('/Volumes/GoogleDrive/My Drive/BATMAN/PGI/code/python/processGeant4Data/')                 # Steve laptop
#sys.path.append('/Volumes/BATMAN/PGI/code/python/processGeant4Data/')        # Steve laptop
import processPolarisData_utils as utils

#import pyximport; pyximport.install(language_level=3)  # fixes warning: Cython directive 'language_level' not set, using 2 for now (Py2)
#import polarisPGI_processing_utils as utils
#import processPolarisData_utils as utils


#------------------------------------------------------------------
# FUNCTION/CLASS DEFINITIONS
#------------------------------------------------------------------

# Read in data from ROOT files and called appropriate function (based on ROOT Tree name), which converts data into pandas array
def read_root_data(data_dir, root_tree_name, output_file_name):
    '''
    Calls appropriate function based on ROOT Tree name and returns the DataFrame.

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        output_file_name    - Optional : Name for output files to store data (Str), default is to use scatterData

    @returns:
        allevents_data      - pandas array of transformed simulated Geant4 data
        root_file_list      - list of root filenames
    '''

    if root_tree_name == 'scatterData':
        allevents_data, root_file_list = read_root_scatterData(data_dir, root_tree_name, output_file_name)        # read in the simulated scatter data

    elif root_tree_name == 'particleData':
        allevents_data, root_file_list = read_root_particleData(data_dir, root_tree_name, output_file_name)       # read in the simulated particle data

    elif root_tree_name == 'fullParticleData':
        allevents_data, root_file_list = read_root_fullParticleData(data_dir, root_tree_name, output_file_name)   # read in the simulated full particle data

    else:
        print('\n Please enter appropriate ROOT Tree name, either scatterData, particleData or fullParticleData')

    return allevents_data, root_file_list


# Read in scatter data from ROOT files and convert into pandas array
def read_root_scatterData(data_dir, root_tree_name, output_file_name):
    '''
    Reads in scatterTree from ROOT files in data_dir, reads the information into a
    python pandas array, and returns the DataFrame.

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        output_file_name    - Optional : Name for output files to store data (Str), default is to use scatterData

    @returns:
        allevents_data      - pandas array of transformed simulated Geant4 data
        root_file_list      - list of root filenames
    '''

    #  creating arrays to store data for conversion into pandas format
    event = []; numInt = []; detNum = []; engDep = []; xPos = []; yPos = []; zPos = []; sTag = []
    eOrg = []; xOrg = []; yOrg = []; zOrg = []; sStg = [];

    #  assign a variable to each leaf in the scatterData tree
    evt_leaf = "event";             trk_leaf = "track";             stp_leaf = "step";              det_leaf = "detector"
    dpos_leaf = "detPos";           dsize_leaf = "detSize";         pro_leaf = "process";           xpos_leaf = "pos_x";
    ypos_leaf = "pos_y";            zpos_leaf = "pos_z";            eng_leaf = "gammaEnergy";       dep_leaf = "energyDeposited";
    ang_leaf = "scatAng";           xorg_leaf = "origin_x";         yorg_leaf = "origin_y";         zorg_leaf = "origin_z"
    eorg_leaf = "origin_energy";    porg_leaf = "origin_process";   vorg_leaf = "origin_volume";    stag_leaf = "scatter_tag"

    # Looking at directory tree and storing the names of the ROOT files
    #   -> source: https://realpython.com/working-with-files-in-python/#traversing-directories-and-processing-files
    print ('  Reading in ROOT files from directory: {}'.format(data_dir))
    root_file_list = []
    for f_name in os.listdir(data_dir):
        if f_name.endswith('.root'):
            root_file_list.append(f_name)
    NUMBER_OF_INPUT_FILES = len(root_file_list)
    #NUMBER_OF_INPUT_FILES = 1
    print ('   - found {} files\n'.format(NUMBER_OF_INPUT_FILES))

    #  loop through input files
    print ('  Reading {} input files, could take several minutes . . .'.format(NUMBER_OF_INPUT_FILES))
    for i in range(NUMBER_OF_INPUT_FILES):

        temp_file_name = data_dir + root_file_list[i]
        print ('    {} -> Filename : {}'.format(i + 1, temp_file_name))
        print ('        - Size: {} bytes'.format(os.path.getsize(temp_file_name)))
        MyFile = uproot.open(temp_file_name)
        MyTree = MyFile[root_tree_name]

        #  form an array for each leaf
        evt = MyTree[evt_leaf].array();     trk = MyTree[trk_leaf].array();         stp = MyTree[stp_leaf].array();     det = MyTree[det_leaf].array()
        dPos = MyTree[dpos_leaf].array();   dSize = MyTree[dsize_leaf].array();     pro = MyTree[pro_leaf].array();     xpos = MyTree[xpos_leaf].array()
        ypos = MyTree[ypos_leaf].array();   zpos = MyTree[zpos_leaf].array();       gEng = MyTree[eng_leaf].array();    engD = MyTree[dep_leaf].array()
        sAng = MyTree[ang_leaf].array();    xorg = MyTree[xorg_leaf].array();       yorg = MyTree[yorg_leaf].array();   zorg = MyTree[zorg_leaf].array()
        eorg = MyTree[eorg_leaf].array();   porg = MyTree[porg_leaf].array();       vorg = MyTree[vorg_leaf].array();   stag = MyTree[stag_leaf].array()

        #  looping through numpy arrays to convert into panda data format
        #for i in range(1234):
        for i in range(len(evt)):
            #ni, dn, ed, xp, yp, zp, st = utils.convert_root_arrays(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i])
            # extend to include origin data + single-stage tag
            #ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo = utils.convert_root_arrays_origins(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i], eorg[i], xorg[i], yorg[i], zorg[i])
            #ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss = utils.convert_root_arrays_origins(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i], eorg[i], xorg[i], yorg[i], zorg[i])
            ev, ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss = utils.convert_root_arrays_origins(evt[i], det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i], eorg[i], xorg[i], yorg[i], zorg[i])
            #print(' i: {}, numInt: {}, detNum: {}, engDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, sTag: {}, sStg: {} \n'.format(i, ni, dn, ed, xp, yp, zp, st, ss))
            #  extend(): Iterates over its argument and adding each element to the list and extending the list. The length of the list increases by number of elements in it's argument.
            #  Append has constant time complexity i.e.,O(1).  Extend has time complexity of O(k). Where k is the length of list which need to be added.
            event.extend(ev);  numInt.extend(ni);  detNum.extend(dn);  engDep.extend(ed);  xPos.extend(xp);  yPos.extend(yp);  zPos.extend(zp);  sTag.extend(st)
            eOrg.extend(eo);  xOrg.extend(xo);  yOrg.extend(yo);  zOrg.extend(zo);  sStg.extend(ss)

    #  changing data format (default is float64) in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('\n  Converting the data into pandas format . . .')
    #print(' numInt: {}, detNum: {}, engDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, sTag: {} \n'.format(numInt, detNum, engDep, xPos, yPos, zPos, sTag))
    #pandas_data = {'scatters': numInt, 'detector': detNum, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'tag': sTag}
    #pandas_data = {'scatters': numInt, 'detector': detNum, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'tag': sTag, 'engOrg': eOrg, 'xOrg': xOrg, 'yOrg': yOrg, 'zOrg': zOrg}
    #pandas_data = {'scatters': numInt, 'detector': detNum, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'tag': sTag, 'engOrg': eOrg, 'xOrg': xOrg, 'yOrg': yOrg, 'zOrg': zOrg, 'sStg': sStg}
    pandas_data = {'event': event, 'scatters': numInt, 'detector': detNum, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'tag': sTag, 'engOrg': eOrg, 'xOrg': xOrg, 'yOrg': yOrg, 'zOrg': zOrg, 'sStg': sStg}
    allevents_data = pd.DataFrame(pandas_data)

    print('{:30} {:d}'.format('Total number of interactions:', len(allevents_data.index)))

    """
    print('  Reading in data from', data_dir + data_filename + '.', '\n  This may take several minutes...')
    allevents_data = pd.read_csv(data_dir + data_filename, sep = '	', header = None)                        # read in text file as a csv with tab separation
    print('  Done.')
    allevents_data.columns = ['scatters', 'detector', 'energy', 'x', 'y', 'z', 'time']       # separate into columns and name them

    print()
    print('{:30} {:d}'.format('Total number of events:', len(allevents_data.index)))
    print('{:30} {:.1f}'.format('Total time (seconds):', (allevents_data.iloc[-1]['time'] - allevents_data.iloc[0]['time']) * 1e-8))
    print('{:30} {:.0f}'.format('Event rate (events/sec):', len(allevents_data.index) / ((allevents_data.iloc[-1]['time'] - allevents_data.iloc[0]['time']) * 1e-8) ))
    """

    # Plotting basic detector plots using raw data
    if PRODUCE_RAW_PLOTS:
        plot_raw_data(data_dir, output_file_name, allevents_data)

    # reorder allevents_data into same order as input file
    #cc_dataframe = cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag']]
    #allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg']]
    #allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg', 'sStg']]
    #allevents_data = allevents_data[['event', 'scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg', 'sStg']]
    #  - moved event to end of pandas in order to miniize disruption to processing functions (like utils.get_interaction_data_tags_origin)
    allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg', 'sStg', 'event']]

    #  convert detector, scatters columns from float (default) into ints & tag columns from byte string into string
    allevents_data['detector'] = allevents_data['detector'].astype(int)
    allevents_data['scatters'] = allevents_data['scatters'].astype(int)
    #allevents_data.tag = allevents_data.tag.str.decode("utf-8")  # no longer needed

    print('\n  Done.')

    return allevents_data, root_file_list


# Read in particle data (new format) from ROOT files and convert into pandas array
def read_root_particleData(data_dir, root_tree_name, output_file_name):
    '''
    Reads in particleTree from ROOT files in data_dir, reads the information into a
    python pandas array, and returns the DataFrame.

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        output_file_name    - Optional : Name for output files to store data (Str), default is to use scatterData

    @returns:
        allevents_data      - pandas array of transformed simulated Geant4 data
        root_file_list      - list of root filenames
    '''

    #  creating arrays to store data for conversion into pandas format
    numSct = []; particle = []; detNum = []; module = []; process = []
    engDep = []; xPos = []; yPos = []; zPos = []; time = []; sStg = []
    sTag = []; rChain = []; nTag = []

    #  assign a variable to each leaf in the scatterData tree
    nsc_leaf = "num_scat";          evt_leaf = "event";             trk_leaf = "track";             part_leaf = "particle"
    mod_leaf = "module";            cry_leaf = "crystal";           pro_leaf = "process";           eng_leaf = "energyDeposited"
    xpos_leaf = "pos_x";            ypos_leaf = "pos_y";            zpos_leaf = "pos_z";            time_leaf = "time"
    stag_leaf = "scatter_tag";      reac_leaf = "reaction"

    # Looking at directory tree and storing the names of the ROOT files
    #   -> source: https://realpython.com/working-with-files-in-python/#traversing-directories-and-processing-files
    print ('  Reading in ROOT files from directory: {}'.format(data_dir))
    root_file_list = []
    for f_name in os.listdir(data_dir):
        if f_name.endswith('.root'):
            root_file_list.append(f_name)
    NUMBER_OF_INPUT_FILES = len(root_file_list)
    #NUMBER_OF_INPUT_FILES = 1
    print ('   - found {} files\n'.format(NUMBER_OF_INPUT_FILES))

    #  loop through input files
    print ('  Reading {} input files, could take several minutes . . .'.format(NUMBER_OF_INPUT_FILES))
    for i in range(NUMBER_OF_INPUT_FILES):

        temp_file_name = data_dir + root_file_list[i]
        print ('    {} -> Filename : {}'.format(i + 1, temp_file_name))
        print ('        - Size: {} bytes'.format(os.path.getsize(temp_file_name)))
        MyFile = uproot.open(temp_file_name)
        MyTree = MyFile[root_tree_name]

        #  form an array for each leaf
        nsct = MyTree[nsc_leaf].array();    evt = MyTree[evt_leaf].array();         trk = MyTree[trk_leaf].array();     part = MyTree[part_leaf].array()
        mod = MyTree[mod_leaf].array();     cry = MyTree[cry_leaf].array();         pro = MyTree[pro_leaf].array();     eDep = MyTree[eng_leaf].array()
        xpos = MyTree[xpos_leaf].array();   ypos = MyTree[ypos_leaf].array();       zpos = MyTree[zpos_leaf].array();   tim = MyTree[time_leaf].array()
        stag = MyTree[stag_leaf].array();   reac = MyTree[reac_leaf].array()

        #  looping through numpy arrays to convert into panda data format
        #for i in range(57510):
        for i in range(len(evt)):

            ns, pt, dn, md, cr, pr, ed, xp, yp, zp, ti, st, re, ss, nt = utils.convert_root_arrays_particle(nsct[i], part[i], mod[i], cry[i], pro[i], eDep[i], xpos[i], ypos[i], zpos[i], tim[i], stag[i], reac[i])
            #  extend(): Iterates over its argument and adding each element to the list and extending the list. The length of the list increases by number of elements in it's argument.
            #  Append has constant time complexity i.e.,O(1).  Extend has time complexity of O(k). Where k is the length of list which need to be added.
            #print(' i: {}, numSct: {}, particle: {}, detNum: {}, module: {}, crystal: {}, process: {}, engDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, time: {}, sStg: {} \n'.format(i, ns, pt, dn, md, cr, pr, ed, xp, yp, zp, ti, ss))
            numSct.extend(ns);  particle.extend(pt);  detNum.extend(dn);  module.extend(md);   process.extend(pr);  engDep.extend(ed)
            xPos.extend(xp);    yPos.extend(yp);      zPos.extend(zp);    time.extend(ti);     sTag.extend(st);     rChain.extend(re);     sStg.extend(ss);   nTag.extend(nt);
            # crystal.extend(cr)

    #  changing data format (default is float64) in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('\n  Converting the data into pandas format . . .')
    #print(' numInt: {}, detNum: {}, engDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, sTag: {} \n'.format(numInt, detNum, engDep, xPos, yPos, zPos, sTag))
    pandas_data = {'scatters': numSct, 'particle': particle, 'detector': detNum, 'module': module, 'process': process, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'time': time, 'tag': sTag, 'reaction': rChain, 'sStg': sStg, 'nTag': nTag}
    allevents_data = pd.DataFrame(pandas_data)
    #print(allevents_data)

    # print quick summary
    print('{:35} {:d}'.format('    Total number of interactions:', len(allevents_data.index)))
    df = allevents_data
    #   breakdown by particle
    ng = len(df.loc[ df.particle == 22 ]);  nn = len(df.loc[ df.particle == 2112 ]);  npr = len(df.loc[ df.particle == 2212 ])
    npo = len(df.loc[ df.particle == -11 ]);  ni = len(df.loc[ df.particle > 1e8 ]);  other = len(df.index) - ng - nn - npr - npo - ni
    print('    - Breakdown by particle - gamma[22]: {}, neutron[2112]: {}, proton[2212]: {}, positron[-11]: {}, ion[>1e8]: {}, other: {}'.format(ng, nn, npr, npo, ni, other))

    # Plotting basic detector plots using raw data
    if PRODUCE_RAW_PLOTS:
        plot_raw_data(data_dir, output_file_name, allevents_data)

    # reorder allevents_data into same order as input file (which input file?)
    allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'particle', 'process', 'time', 'tag', 'reaction', 'sStg', 'nTag']]

    #  convert detector, scatters columns from float (default) into ints & tag columns from byte string into string
    allevents_data.detector = allevents_data.detector.astype(int)
    allevents_data.scatters = allevents_data.scatters.astype(int)
    #allevents_data.process = allevents_data.process.str.decode("utf-8")  # no longer needed

    print('\n  Done.')

    return allevents_data, root_file_list


# Read in full particle data (old format) from ROOT files and convert into pandas array
def read_root_fullParticleData(data_dir, root_tree_name, output_file_name):
    '''
    Reads in fullParticleTree from ROOT files in data_dir, reads the information into a
    python pandas array, and returns the DataFrame.

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        output_file_name    - Optional : Name for output files to store data (Str), default is to use scatterData

    @returns:
        allevents_data      - pandas array of transformed simulated Geant4 data
        root_file_list      - list of root filenames
    '''

    #  creating arrays to store data for conversion into pandas format
    particle = []; detNum = []; module = []; engDep = []; xPos = []; yPos = []; zPos = []
    distance = []; time = []; timeLength = []; count = []

    #  assign a variable to each leaf in the scatterData tree
    evt_leaf = "event";             trk_leaf = "track";             prnt_leaf = "parent";           part_leaf = "particle"
    mod_leaf = "module";            cry_leaf = "crystal";           eng_leaf = "engDep";            eLost_leaf = "engLost";
    xpos_leaf = "pos_x";            ypos_leaf = "pos_y";            zpos_leaf = "pos_z";            dist_leaf = "distance";
    time_leaf = "time";             timLen_leaf = "lengthTime";     cnt_leaf = "count";

    # Looking at directory tree and storing the names of the ROOT files
    #   -> source: https://realpython.com/working-with-files-in-python/#traversing-directories-and-processing-files
    print ('  Reading in ROOT files from directory: {}'.format(data_dir))
    root_file_list = []
    for f_name in os.listdir(data_dir):
        if f_name.endswith('.root'):
            root_file_list.append(f_name)
    NUMBER_OF_INPUT_FILES = len(root_file_list)
    #NUMBER_OF_INPUT_FILES = 1
    print ('   - found {} files\n'.format(NUMBER_OF_INPUT_FILES))

    #  loop through input files
    print ('  Reading {} input files, could take several minutes . . .'.format(NUMBER_OF_INPUT_FILES))
    for i in range(NUMBER_OF_INPUT_FILES):

        temp_file_name = data_dir + root_file_list[i]
        print ('    {} -> Filename : {}'.format(i + 1, temp_file_name))
        print ('        - Size: {} bytes'.format(os.path.getsize(temp_file_name)))
        MyFile = uproot.open(temp_file_name)
        MyTree = MyFile[root_tree_name]

        #  form an array for each leaf
        evt = MyTree[evt_leaf].array();     trk = MyTree[trk_leaf].array();         prnt = MyTree[prnt_leaf].array();   part = MyTree[part_leaf].array()
        mod = MyTree[mod_leaf].array();     cry = MyTree[cry_leaf].array();         eDep = MyTree[eng_leaf].array();    eLost = MyTree[eLost_leaf].array()
        xpos = MyTree[xpos_leaf].array();   ypos = MyTree[ypos_leaf].array();       zpos = MyTree[zpos_leaf].array();   dis = MyTree[dist_leaf].array()
        tim = MyTree[time_leaf].array();    tLen = MyTree[timLen_leaf].array();     cnt = MyTree[cnt_leaf].array();

        #  looping through numpy arrays to convert into panda data format
        #for i in range(57510):
        for i in range(len(evt)):

            #ni, dn, ed, xp, yp, zp, st = utils.convert_root_arrays(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i])
            # extend to include origin data + single-stage tag
            #ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo = utils.convert_root_arrays_origins(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i], eorg[i], xorg[i], yorg[i], zorg[i])
            #ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss = utils.convert_root_arrays_origins(part[i], mod[i], cry [i], engD[i], xpos[i], ypos[i], zpos[i], dis[i], tim[i], tLen[i], cnt[i])
            #print(' i: {}, numInt: {}, detNum: {}, engDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, sTag: {}, sStg: {} \n'.format(i, ni, dn, ed, xp, yp, zp, st, ss))

            pa, dn, md, ed, xp, yp, zp, di, ti, tl, ct = utils.convert_root_arrays_full_particle(part[i], mod[i], cry[i], eDep[i], xpos[i], ypos[i], zpos[i], dis[i], tim[i], tLen[i], cnt[i])
            #  extend(): Iterates over its argument and adding each element to the list and extending the list. The length of the list increases by number of elements in it's argument.
            #  Append has constant time complexity i.e.,O(1).  Extend has time complexity of O(k). Where k is the length of list which need to be added.
            particle.extend(pa);  detNum.extend(dn);  module.extend(md);  engDep.extend(ed);  xPos.extend(xp);  yPos.extend(yp);  zPos.extend(zp)
            distance.extend(di);  time.extend(ti);  timeLength.extend(tl);  count.extend(ct)


    #  changing data format (default is float64) in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('\n  Converting the data into pandas format . . .')
    #print(' part: {}, mod: {}, eDep/MeV: {}, xPos/mm: {}, yPos/mm: {}, zPos/mm: {}, dis: {} \n'.format(part, mod, eDep, xpos, ypos, zpos, dis))
    #  NOTE: converted module into detector number (to compare), and dropped crystal number
    #    - in POLARIS3, each detector contains four modules and 16 crystals
    pandas_data = {'particle': particle, 'detector': detNum, 'module': module, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'distance': distance, 'time': time, 'timeLength': timeLength, 'count': count}
    allevents_data = pd.DataFrame(pandas_data)

    #print(allevents_data)

    print('{:35} {:d}'.format('    Total number of interactions:', len(allevents_data.index)))
    df = allevents_data
    ng = len(df.loc[ df.particle == 22 ])
    nn = len(df.loc[ df.particle == 2112 ])
    npr = len(df.loc[ df.particle == 2212 ])
    npo = len(df.loc[ df.particle == -11 ])
    ni = len(df.loc[ df.particle > 1e8 ])
    other = len(df.index) - ng - nn - npr - npo - ni
    print('    - Breakdown by particle - gamma[22]: {}, neutron[2112]: {}, proton[2212]: {}, positron[-11]: {}, ion[>1e8]: {}, other: {}'.format(ng, nn, npr, npo, ni, other))

    # Plotting basic detector plots using raw data
    if PRODUCE_RAW_PLOTS:
        plot_raw_data(data_dir, output_file_name, allevents_data)

    # # reorder allevents_data into same order as input file
    # #cc_dataframe = cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag']]
    # #allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg']]
    # allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg', 'sStg']]
    #
    #  convert detector, scatters columns from float (default) into ints & tag columns from byte string into string
    allevents_data['particle'] = allevents_data['particle'].astype(int);  allevents_data['module'] = allevents_data['module'].astype(int)
    allevents_data['detector'] = allevents_data['detector'].astype(int);  allevents_data['count'] = allevents_data['count'].astype(int)
    #allevents_data['energy'] = allevents_data['energy'].astype(float_t);  allevents_data['xpos'] = allevents_data['xpos'].astype(float_t)
    #allevents_data['ypos'] = allevents_data['ypos'].astype(float_t);  allevents_data['zpos'] = allevents_data['zpos'].astype(float_t)
    #allevents_data['time'] = allevents_data['time'].astype(float_t);  allevents_data['timeLength'] = allevents_data['timeLength'].astype(float_t)

    print('\n  Done.')

    return allevents_data, root_file_list


# Plotting basic detector plots using raw data
def add_tags_to_filename(data_filename, ending):

    tag_count = 0;
    #tagged_filename = data_filename.replace(".txt", "").replace(".dat", "").replace(".csv", "") + "-scatters"
    tagged_filename = data_filename.replace(".txt", "").replace(".dat", "").replace(".csv", "")
    if APPLY_BIG_ENERGY_SORTING:                tagged_filename = tagged_filename + "-BigE";      tag_count += 1
    if REMOVE_UNPHYSICAL_EVENTS:                tagged_filename = tagged_filename + "-UnPhy";     tag_count += 1
    if APPLY_COMPTON_LINE_FILTERING:            tagged_filename = tagged_filename + "-ComL";      tag_count += 1
    if APPLY_WEIGHTING_POTENTIAL:               tagged_filename = tagged_filename + "-WPot";      tag_count += 1
    if SHUFFLE_SCATTER_EVENTS:                  tagged_filename = tagged_filename + "-Shuf";      tag_count += 1
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE:   tagged_filename = tagged_filename + "-MScoin";    tag_count += 1
    if tag_count == 0:                          tagged_filename = tagged_filename + "-unfiltered"
    return tagged_filename + ending


# Plotting basic detector plots using raw data
def plot_raw_data(data_dir, output_file_name, cc_dataframe):

    #  checking if plots directory exists, if not, create
    plots_dir = data_dir + add_tags_to_filename(output_file_name, '-plots_raw/')
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
        print ('Creating directory: {}'.format(plots_dir))


    ### PLOTTING BASIC DETECTOR DATA ###
    print ('\n--- Making Basic Detector Plots (using raw data) ---')

    #  plotting raw data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
    utils.make_basic_detector_plots(cc_dataframe, 'raw_all', plots_dir)
    #  alternate plots for raw data of 2D xy profiles
    d0_xPos = cc_dataframe[cc_dataframe.detector == 0].x.to_numpy()
    d0_yPos = cc_dataframe[cc_dataframe.detector == 0].y.to_numpy()
    d1_xPos = cc_dataframe[cc_dataframe.detector == 1].x.to_numpy()
    d1_yPos = cc_dataframe[cc_dataframe.detector == 1].y.to_numpy()
    plt.cla(); plt.clf()
    plt.plot(d0_yPos, d0_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
    plt.title('XY Position of all scatters / D0 / Raw Data, total: %s'%(len(d0_yPos)))
    plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
    utils.save_to_file("raw_all_D0_y_x_alternate", plots_dir)
    plt.cla(); plt.clf()
    plt.plot(d1_yPos, d1_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
    plt.title('XY Position of all scatters / D1 / Raw Data, total: %s'%(len(d1_yPos)))
    plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
    utils.save_to_file("raw_all_D1_y_x_alternate", plots_dir)
    #  alternate plots for energy deposition of raw data
    #alternate_max_energy = (np.amax(COMPTON_LINE_ENERGIES) * 1000) + 100   # convert max Compton Line energy to keV and add 100 keV
    alternate_max_energy = np.amax(COMPTON_LINE_ENERGIES) + 0.1   # use max Compton Line energy and add 100 keV
    print ('  Creating 1D Plots - using alternate maximum energy of {} MeV . . .'.format(alternate_max_energy))
    plt.cla(); plt.clf()
    utils.create_plot_range(cc_dataframe.energy, 'Energy Deposited (keV)', 300, 0, alternate_max_energy)
    plt.title('Total energy deposted by all scatters, total: %s'%(len(cc_dataframe.energy)))
    utils.save_to_file("raw_all_Energy_Counts_alternate", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(cc_dataframe.energy, 'Energy Deposited (keV)', 300, 0, alternate_max_energy, True)
    plt.title('Total energy deposted by all scatters, total: %s'%(len(cc_dataframe.energy)))
    utils.save_to_file("raw_all_Energy_Counts_alternate_log", plots_dir)
    plt.cla(); plt.clf()
    d0_eng = cc_dataframe[cc_dataframe.detector == 0].energy.to_numpy()
    utils.create_plot_range(d0_eng, 'Energy Deposited (keV)', 300, 0, alternate_max_energy)
    plt.title('Total energy deposted by all scatters / D0, total: %s'%(len(d0_eng)))
    utils.save_to_file("raw_all_D0_Energy_Counts_alternate", plots_dir)
    d1_eng = cc_dataframe[cc_dataframe.detector == 1].energy.to_numpy()
    plt.cla(); plt.clf()
    utils.create_plot_range(d1_eng, 'Energy Deposited (keV)', 300, 0, alternate_max_energy)
    plt.title('Total energy deposted by all scatters / D1, total: %s'%(len(d1_eng)))
    utils.save_to_file("raw_all_D1_Energy_Counts_alternate", plots_dir)


# Plotting basic detector plots using transformed data
def plot_transformed_data(data_dir, output_file_name, cc_dataframe):

    #  checking if plots directory exists, if not, create
    plots_dir = data_dir + add_tags_to_filename(output_file_name, '-plots_transformed/')
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
        print ('  Creating directory: {}'.format(plots_dir))

    ### PLOTTING TRANSFORMED DETECTOR DATA ###
    print ('\n--- Making Basic Detector Plots (using transformed data) ---')

    #  plotting transformed data (1D profiles in x, y, z, energy + 2D profiles in xy, yz, xz)
    utils.make_basic_detector_plots(cc_dataframe, 'transformed_all', plots_dir)
    #  plotting based on number of pixel interactions (1, 2, 3, 4+)
    utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 1], 'transformed_1px', plots_dir)
    utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 2], 'transformed_2px', plots_dir)
    utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters == 3], 'transformed_3px', plots_dir)
    utils.make_basic_detector_plots(cc_dataframe[cc_dataframe.scatters > 3], 'transformed_4px+', plots_dir)
    #  plotting 1D profiles (x, y, z) across all detectors
    utils.plot_1D(cc_dataframe.x.to_numpy(), 200, "x", "Counts", "transformed_all", plots_dir)
    utils.plot_1D(cc_dataframe.y.to_numpy(), 200, "y", "Counts", "transformed_all", plots_dir)
    utils.plot_1D(cc_dataframe.z.to_numpy(), 200, "z", "Counts", "transformed_all", plots_dir)


# Plotting detector plots using final data
def plot_final_doubles_data(data_dir, output_file_name, scatters_2x_out, scatters_2x_d0_out, scatters_2x_d1_out):

    #  checking if plots directory exists, if not, create
    plots_dir = data_dir + add_tags_to_filename(output_file_name, '-plots_final_doubles/')
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
        print ('  Creating directory: {}'.format(plots_dir))

    ### PLOTTING FINAL DETECTOR DATA ###
    print ('\n--- Making Basic Detector Plots (using final doubles data) ---')

    #  producing required data
    MeCsq = 0.5109989461  # electron mass in energy units (MeV)
    #   - scatters_2x_out
    ds_1st_eng = scatters_2x_out[:, 0];  ds_2nd_eng = scatters_2x_out[:, 4];  ds_tot_eng = ds_1st_eng + ds_2nd_eng
    ds_all_eng = np.concatenate((ds_1st_eng, ds_2nd_eng), axis = 0)
    ds_theta1 = np.arccos( 1 + MeCsq * ( 1.0/(ds_tot_eng) - 1.0/(ds_tot_eng - ds_1st_eng) ) )
    #   - scatters_2x_d0_out
    ds_d0_1st_eng = scatters_2x_d0_out[:, 0];  ds_d0_2nd_eng = scatters_2x_d0_out[:, 4];  ds_d0_tot_eng = ds_d0_1st_eng + ds_d0_2nd_eng
    ds_d0_all_eng = np.concatenate((ds_d0_1st_eng, ds_d0_2nd_eng), axis = 0)
    ds_d0_theta1 = np.arccos( 1 + MeCsq * ( 1.0/(ds_d0_tot_eng) - 1.0/(ds_d0_tot_eng - ds_d0_1st_eng) ) )
    #   - scatters_2x_d1_out
    ds_d1_1st_eng = scatters_2x_d1_out[:, 0];  ds_d1_2nd_eng = scatters_2x_d1_out[:, 4];  ds_d1_tot_eng = ds_d1_1st_eng + ds_d1_2nd_eng
    ds_d1_all_eng = np.concatenate((ds_1st_eng, ds_d1_2nd_eng), axis = 0)
    ds_d1_theta1 = np.arccos( 1 + MeCsq * ( 1.0/(ds_d1_tot_eng) - 1.0/(ds_d1_tot_eng - ds_d1_1st_eng) ) )

    alternate_max_energy = np.amax(COMPTON_LINE_ENERGIES) + 0.1   # use max Compton Line energy and add 100 keV
    print ('  Creating 1D Plots - using alternate maximum energy of {} MeV . . .'.format(alternate_max_energy))

    #  plots for energy deposition of final data
    #   - scatters_2x_out
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_tot_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by all double scatters (summed total), total: %s'%(len(ds_tot_eng)))
    utils.save_to_file("final_doubles_all_Energy_Sum_Counts_alternate", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_tot_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy, True)
    plt.title('Energy deposited by all double scatters (summed total), total: %s'%(len(ds_tot_eng)))
    utils.save_to_file("final_doubles_all_Energy_Sum_Counts_alternate_log", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_all_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by all double scatters (individual scatters), total: %s'%(len(ds_all_eng)))
    utils.save_to_file("final_doubles_all_Energy_Each_Counts_alternate", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_all_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy, True)
    plt.title('Energy deposited by all double scatters (individual scatters), total: %s'%(len(ds_all_eng)))
    utils.save_to_file("final_doubles_all_Energy_Each_Counts_alternate_log", plots_dir)
    # IEEE Abstract
    plt.cla(); plt.clf()
    utils.create_plot_range_ieee(ds_all_eng, 'Energy (MeV)', 300, MINIMUM_ENERGY_THRESHOLD, alternate_max_energy)
    #plt.title('Energy deposted by all scatters, total: %s'%(len(cc_dataframe.energy)), fontsize = 16)
    utils.save_to_file("ieee-final_doubles_all_Energy_Each_Counts_alternate", plots_dir)
    #   - scatters_2x_d0_out
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_d0_tot_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by D0 double scatters (summed total), total: %s'%(len(ds_d0_tot_eng)))
    utils.save_to_file("final_doubles_D0_Energy_Sum_Counts_alternate", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_d0_all_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by D0 double scatters (individual scatters), total: %s'%(len(ds_d0_all_eng)))
    utils.save_to_file("final_doubles_D0_Energy_Each_Counts_alternate", plots_dir)
    #   - scatters_2x_d1_out
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_d1_tot_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by D1 double scatters (summed total), total: %s'%(len(ds_d1_tot_eng)))
    utils.save_to_file("final_doubles_D1_Energy_Sum_Counts_alternate", plots_dir)
    plt.cla(); plt.clf()
    utils.create_plot_range(ds_d1_all_eng, 'Energy Deposited (MeV)', 300, 0, alternate_max_energy)
    plt.title('Energy deposited by D1 double scatters (individual scatters), total: %s'%(len(ds_d1_all_eng)))
    utils.save_to_file("final_doubles_D1_Energy_Each_Counts_alternate", plots_dir)

    #  plotting E1 vs Theta1
    print ('  Creating energy 1 vs theta 1 plots . . .')
    alternate_max_energy_cl_plots = np.amax(COMPTON_LINE_ENERGIES)   # use maxCompton Line energy
    print ('    - using alternate maximum energy of {} MeV . . .'.format(alternate_max_energy_cl_plots))
    #   - scatters_2x_out
    plt.cla(); plt.clf()
    plt.plot(ds_theta1, ds_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
    plt.title('First Energy Deposited vs Calculated First Scatter Angle / All, total: %s'%(len(ds_theta1)))
    plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
    plt.axis([0, 3, 0, alternate_max_energy_cl_plots])
    utils.save_to_file("final_doubles_all_Energy1_Theta1", plots_dir)
    #   - scatters_2x_d0_out
    plt.cla(); plt.clf()
    plt.plot(ds_d0_theta1, ds_d0_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
    plt.title('First Energy Deposited vs Calculated First Scatter Angle / D0, total: %s'%(len(ds_d0_theta1)))
    plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
    plt.axis([0, 3, 0, alternate_max_energy_cl_plots])
    utils.save_to_file("final_doubles_D0_Energy1_Theta1", plots_dir)
    #   - scatters_2x_d1_out
    plt.cla(); plt.clf()
    plt.plot(ds_d1_theta1, ds_d1_1st_eng, color='blue', marker='o', linestyle='None', markersize=0.5)
    plt.title('First Energy Deposited vs Calculated First Scatter Angle / D1, total: %s'%(len(ds_d1_theta1)))
    plt.xlabel('Calculated First Scatter Angle (rad)');  plt.ylabel('Energy Deposited in First Scatter (MeV)')
    plt.axis([0, 3, 0, alternate_max_energy_cl_plots])
    utils.save_to_file("final_doubles_D1_Energy1_Theta1", plots_dir)


# Process the ROOT scatter data, filter it and store output csv files
def filter_data(data_dir, root_tree_name, USE_DIR_AS_OUTPUT = False):
    '''
    Takes in POLARIS3 generated ROOT file, reads the information into a pandas
    array and then filters data using the flagged parameters

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    @returns:
        scatters_2x_out     - numpy array of the processed double scatter data recorded by both polaris detectors
        scatters_2x_d0_out  - numpy array of the processed double scatter data recorded by polaris detector 0
        scatters_2x_d1_out  - numpy array of the processed double scatter data recorded by polaris detector 1
        scatters_3x_out     - numpy array of the processed triple scatter data recorded by both polaris detectors
    '''

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  Setting output_file_name, either breaking up data_dir or using root_tree_name (default)
    if USE_DIR_AS_OUTPUT:
        data_dir_split = data_dir.split('/')
        output_file_name = data_dir_split[-3]   # use third from end (drops "root" and blank space)
        print ('   - Using data_dir to set output filename -> {}'.format(output_file_name))
    else:
        output_file_name = root_tree_name

    #### NOTE: need to figure out how to drop data_dir down one level (remove /root/)

    allevents_data, root_file_list = read_root_data(data_dir, root_tree_name, output_file_name)       # read in the simulated scatter data
    tot_raw_events = len(allevents_data.index)          # get the total number of events before coincidence processing

    #  Counting gamma scatter interactions (before the interactions were grouped into events)
    nbInt_prompt = 0; nbInt_np_neutron = 0; nbInt_np_other = 0       # count prompt/neutron-induced/other events
    nbInt_zero = 0; nbInt_single = 0; nbInt_multiple = 0           # count zero/single/multiple scatter events
    nbInt_nonScat = 0; nbInt_tarScat = 0; nbInt_detScat = 0; nbInt_othScat = 0; nbInt_mulScat = 0      # count scatter in none/target/detector/other/multiple
    nbInt_tarCreat = 0; nbInt_detCreat = 0; nbInt_othCreat = 0     # count creations in target/detector/other

    for i in range(len(allevents_data['tag'])):
        #print (' ****** allevents_data[tag][{}]: {}'.format(i, allevents_data['tag'][i]))
        #  count prompt/nonprompt events -> tag[0] / 1st [particleType]: (P)rompt, (N)eutron-induced or (O)ther gamma
        if allevents_data['tag'][i][0] == 'P':  nbInt_prompt += 1
        if allevents_data['tag'][i][0] == 'N':  nbInt_np_neutron += 1
        else:  nbInt_np_other += 1
        #  count number of scatter events -> tag[1] / 2nd [scatterNumber]: (Z)ero, (S)ingle, (M)ultiple scatters
        if allevents_data['tag'][i][1] == 'Z':  nbInt_zero += 1
        elif allevents_data['tag'][i][1] == 'S':  nbInt_single += 1
        else:  nbInt_multiple += 1
        #  count number of scatter events -> tag[2] / 3rd [scatterLocation]: (T)arget, (D)etector, (O)ther, (M)ultiple, (N)one
        if allevents_data['tag'][i][2] == 'N':  nbInt_nonScat += 1
        elif allevents_data['tag'][i][2] == 'T':  nbInt_tarScat += 1
        elif allevents_data['tag'][i][2] == 'D':  nbInt_detScat += 1
        elif allevents_data['tag'][i][2] == 'O':  nbInt_othScat += 1
        else:  nbInt_mulScat += 1
        #  count number of scatter events -> tag[3] / 4th [creationLocation]: (T)arget, (D)etector, (O)ther
        if allevents_data['tag'][i][3] == 'T':  nbInt_tarCreat += 1
        elif allevents_data['tag'][i][3] == 'D':  nbInt_detCreat += 1
        else:  nbInt_othCreat += 1

    #  output gamma scatter details
    print ('\n  Counting gamma scatter interactions . . .')
    print ('   - Total Interactions: {} | prompt gamma interactions: {} ({:.2f}%) | non-prompt (neutron-induced) gamma interactions: {} ({:.2f}%) | non-prompt (other) gamma interactions: {} ({:.2f}%)'.format(len(allevents_data['tag']), nbInt_prompt, (float(nbInt_prompt)/len(allevents_data['tag']))*100.0, nbInt_np_neutron, (float(nbInt_np_neutron)/len(allevents_data['tag']))*100.0, nbInt_np_other, (float(nbInt_np_other)/len(allevents_data['tag']))*100.0))
    print ('   - Number of Scatters | zero: {} ({:.2f}%) | single: {} ({:.2f}%) | multiple: {} ({:.2f}%)'.format(nbInt_zero, (float(nbInt_zero)/len(allevents_data['tag']))*100.0, nbInt_single, (float(nbInt_single)/len(allevents_data['tag']))*100.0, nbInt_multiple, (float(nbInt_multiple)/len(allevents_data['tag']))*100.0))
    print ('   - Scatter Location | none: {} | target: {} | detector: {} | other: {} | multiple: {}'.format(nbInt_nonScat, nbInt_tarScat, nbInt_detScat, nbInt_othScat, nbInt_mulScat))
    print ('   - Creation Location | target: {} | detector: {} | other: {}'.format(nbInt_tarCreat, nbInt_detCreat, nbInt_othCreat))


    """
    #  output time details
    #   - convert time into seconds (t given in units of 10 ns clock cycles), so tS = t * 10E-9
    time_in_sec = allevents_data['time'] * (10E-9)
    time_start = np.amin(time_in_sec)
    time_end = np.amax(time_in_sec)
    #   - print to screen
    print ('   - Start time (s): {} | End time (s): {} | Run duration (s): {}'.format(time_start, time_end, (time_end - time_start)))
    print ('    - run activity (events/s): {}'.format(len(allevents_data) / (time_end - time_start)))
    """


    # Counters for run summary
    num_1px, num_2px, num_3px, num_4px = [float('nan')] * 4                # count events based on pixel number
    num_ss, num_ss_et, num_ss_fi = [float('nan')] * 3                      # count events for single scatters
    num_ds, num_ds_d0, num_ds_d1, num_ts = [float('nan')] * 4              # count total number of double/triple scatters
    num_ds_et, num_ds_d0_et, num_ds_d1_et, num_ts_et = [float('nan')] * 4  # count number of energy threshold events
    num_ds_pe, num_ds_d0_pe, num_ds_d1_pe = [float('nan')] * 3             # count number of physical events
    num_ds_cl, num_ds_d0_cl, num_ds_d1_cl = [float('nan')] * 3             # count number of Compton line filtered events
    num_ds_ms, num_ds_d0_ms, num_ds_d1_ms = [float('nan')] * 3             # count number of multistage coincidence events
    num_ds_fi, num_ds_d0_fi, num_ds_d1_fi, num_ts_fi = [float('nan')] * 4  # final number of double/triple scatters
    num_ms_total, num_ms_2px, num_ms_3px = [float('nan')] * 3              # count multi-stage interactions

    # Counters for scatter tags
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = [float('nan')] * 6
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = [float('nan')] * 6
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = [float('nan')] * 6

    # count number of pixel events
    num_1px = len(allevents_data[allevents_data.scatters == 1]);  num_2px = len(allevents_data[allevents_data.scatters == 2])
    num_3px = len(allevents_data[allevents_data.scatters == 3]);  num_4px = len(allevents_data[allevents_data.scatters  > 3])

    # count number of multi-stage interactions
    num_ms_total = len(allevents_data[allevents_data.sStg == False])
    num_ms_2px = len(allevents_data[(allevents_data.scatters == 2) & (allevents_data.sStg == False)])
    num_ms_3px = len(allevents_data[(allevents_data.scatters == 3) & (allevents_data.sStg == False)])

    print('\nFiltering data')
    print('-----------------------')

    # Plotting basic detector plots using transformed data
    if PRODUCE_TRANSFORMED_PLOTS:
        plot_transformed_data(data_dir, output_file_name, allevents_data)

    print ('\n--- Grouping Compton Scatter Data ---')

    if APPLY_BIG_ENERGY_SORTING:
        print ('  Sorting scatters to place largest energy deposition first . . .')
    #  returns interactions (E, X, Y, Z), tags (4 character string) and origins (E, X, Y, Z) for the specified number of scatters and detector number
    #   format -> def get_interaction_data_tags_origin(df, energy_sort = True, num_scatters = 2, det_num = None)
    scatters_1x, tags_1x, origins_1x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1)
    scatters_1x_d0, tags_1x_d0, origins_1x_d0 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1, 0)
    scatters_1x_d1, tags_1x_d1, origins_1x_d1 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1, 1)
    scatters_2x, tags_2x, origins_2x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_d0, tags_2x_d0, origins_2x_d0 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2, 0)
    scatters_2x_d1, tags_2x_d1, origins_2x_d1 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2, 1)
    scatters_3x, tags_3x, origins_3x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 3)

    ### FORMATTING DATA FOR OUTPUT ###

    print ('\n--- Formatting Compton Scatter Data for Output ---')
    print ('  Re-arranging data array for CSV output . . .')

    #  re-arranges interaction data into CSV format (entire event on one line)
    #   format -> def format_data_for_output(scatters, tags, num_scatters, det_num = None):
    scatters_1x_out = scatters_1x;  scatters_1x_d0_out = scatters_1x_d0;  scatters_1x_d1_out = scatters_1x_d1  # no rearrangment required
    #tags_1x_out = allevents_data[allevents_data.scatters == 1].tag.to_numpy()  # creating tags array
    tags_1x_out = tags_1x;  tags_1x_d0_out = tags_1x_d0;  tags_1x_d1_out = tags_1x_d1  # creating tags array
    origins_1x_out = origins_1x;  origins_1x_d0_out = origins_1x_d0;  origins_1x_d1_out = origins_1x_d1  # creating origins array
    scatters_2x_out, tags_2x_out, origins_2x_out = utils.format_data_for_output_tags_origins(scatters_2x, tags_2x, origins_2x, 2)
    scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.format_data_for_output_tags_origins(scatters_2x_d0, tags_2x_d0, origins_2x_d0, 2, 0)
    scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.format_data_for_output_tags_origins(scatters_2x_d1, tags_2x_d1, origins_2x_d1, 2, 1)
    scatters_3x_out, tags_3x_out, origins_3x_out = utils.format_data_for_output_tags_origins(scatters_3x, tags_3x, origins_3x, 3)

    # count initial number of double/triple scatters
    num_ss = len(scatters_1x)
    num_ds = len(scatters_2x_out);  num_ds_d0 = len(scatters_2x_d0_out)
    num_ds_d1 = len(scatters_2x_d1_out);  num_ts = len(scatters_3x_out)

    print ('\n  Counting 1x gamma scatter events . . .')
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = utils.count_gamma_scatter(scatters_1x_out, tags_1x_out)
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    print ('\n  Counting 3x gamma scatter events . . .')
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = utils.count_gamma_scatter(scatters_3x_out, tags_3x_out)


    ### FILTERING DETECTOR DATA ###

    print ('\n--- Filtering Detector Data ---')

    #  removing events below energy threshold (set by MINIMUM_ENERGY_THRESHOLD)
    #   - removes full event if any of the scatters fall below threshold
    if APPLY_ENERGY_THRESHOLD:
        print ('  Removing events below energy threshold ({} MeV) . . .'.format(MINIMUM_ENERGY_THRESHOLD))
        scatters_1x_out, tags_1x_out, origins_1x_out = utils.filtering_energy_threshold_tags_origins(scatters_1x, tags_1x_out, origins_1x_out, 1, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_out, tags_2x_out, origins_2x_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_3x_out, tags_3x_out, origins_3x_out = utils.filtering_energy_threshold_tags_origins(scatters_3x_out, tags_3x_out, origins_3x_out, 3, MINIMUM_ENERGY_THRESHOLD)

        # count number of remaining double/triple scatters after energy threshold applied
        num_ss_et = len(scatters_1x_out)
        num_ds_et = len(scatters_2x_out);  num_ds_d0_et = len(scatters_2x_d0_out)
        num_ds_d1_et = len(scatters_2x_d1_out);  num_ts_et = len(scatters_3x_out)

    print ('\n  Counting 1x gamma scatter events . . .')
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = utils.count_gamma_scatter(scatters_1x_out, tags_1x_out)
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    print ('\n  Counting 3x gamma scatter events . . .')
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = utils.count_gamma_scatter(scatters_3x_out, tags_3x_out)


    #  removing unphysical events (Compton scatter angle == nan)
    #   - checks both ordering of two scatters and flips if original ordering in unphysical
    #   - modified code from Matt Leigh's Filter.py & CPUFunctions.cu
    if REMOVE_UNPHYSICAL_EVENTS:
        print ('\n  Removing unphysical events (double scatters) . . .')
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out, tags_2x_out, origins_2x_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out)

        # count number of physical double scatters
        num_ds_pe = len(scatters_2x_out);  num_ds_d0_pe = len(scatters_2x_d0_out);  num_ds_d1_pe = len(scatters_2x_d1_out)

    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)


    #  applying Compton line filtering (checks if E1 and theta1 are consistent with Compton formula)
    #   - use COMPTON_LINE_RANGE (accepted energy range) & COMPTON_LINE_ENERGIES (expected gamma energies) global variables
    #   - modified code from Matt Leigh's Filter.py
    if APPLY_COMPTON_LINE_FILTERING:
        print ('\n  Applying Compton line filtering / Energies (MeV): {} / Range of values: {}'.format(COMPTON_LINE_ENERGIES, COMPTON_LINE_RANGE))
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out, tags_2x_out, origins_2x_out = utils.compton_line_filtering_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.compton_line_filtering_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.compton_line_filtering_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)

        # count number of Compton line filtered double scatters
        num_ds_cl = len(scatters_2x_out);  num_ds_d0_cl = len(scatters_2x_d0_out);  num_ds_d1_cl = len(scatters_2x_d1_out)

    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)


    ### APPLYING ENERGY WEIGHTING POTENTIAL ###

    print ('\n--- Applying Weighting Potential ---')

    #  groups events from different modules based on energy and time stamp
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_WEIGHTING_POTENTIAL:
        print ('  Scaling enegy deposited based on depth in crystal . . .')

        '''
        #  returns the energy deposited scaling factor based on the weighting potential method for signal induction.
        #  Weighting potential calculation is done using a fit calculated from the following reference:
        #  https://cztlab.engin.umich.edu/wp-content/uploads/sites/187/2015/03/zhangf.pdf

        #  The calculation assumes CZT plane (either 10 mm or 15 mm) with anode pointing away the radioactive source (i.e. anode at 0 mm in detector geometry)
        '''

        #  Scaling factor for 10 mm crystal
        #  p2A is the position of the back of the detector in mm
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11 * (x - p2A) / 10))   # for D0 located along x-axis (back of detector has max -x value)
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11 * (p2A - y) / 10))   # for D1 located along y-axis (back of detector has max +y value)

        #  Scaling factor for 10 mm crystal
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11*(y-p1A)/10))

        #  Scaling factor for 15 mm crystal
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11*(p2A-y)/15))

        print ('    ######  CURRENTLY NOT FUNCTIONING  ######')

    """
    def calcWeightingPotential(y,p1A,p2A):
        '''
        edepScale = calcWeightingPotential(y,p1A,p2A)
        returns the energy deposited scaling factor based on the weighting potential method for signal induction.
        Weighting potential calculation is done using a fit calculated from the following reference:
        https://cztlab.engin.umich.edu/wp-content/uploads/sites/187/2015/03/zhangf.pdf

        The calculation assumes two opposed CZT planes (one 10 mm, one 15 mm) with anodes on the exterior surfaces.

        plane1 (10 mm)            plane 2 (15 mm)
        |                    +y2 <------ y2=p2A |
        |                                       |
        |                                       |
        |y1=p1A ------> +y1                     |

        input:
            y : raw interaction depth
            p1A, p2A : y location of the two anodes

        output:
            edepScale : multiplicitive scaling factor for deposited energy
        '''
        edepScale = 1
        if y <= p1A+10.1:
            edepScale = 1 - (0.9808 * np.exp(-23.11*(y-p1A)/10))
        else:
            edepScale = 1 - (0.9808 * np.exp(-23.11*(p2A-y)/15))
        return edepScale
    """



    #  shuffle the 2x & 3x scatter events
    #   - to overcome timing errors (like runs 6, 7, 8 for 180315 data)
    if SHUFFLE_SCATTER_EVENTS:

        #  - np.random.shuffle: Modify a sequence in-place by shuffling its contents.
        np.random.shuffle(scatters_2x_out)
        np.random.shuffle(scatters_3x_out)


    ### GROUPING COINCIDENCE EVENTS ###

    print ('\n--- Grouping Coincidence Events ---')

    #  groups events from different modules based on energy and time stamp
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE:
        print ('  Grouping multi-stage coincidence events . . .')

        print ('    ######  CURRENTLY NOT FUNCTIONING  ######')

        # count number of multi-stage coincidence double scatters
        # num_ds_ms = len(scatters_2x_out);  num_ds_d0_ms = len(scatters_2x_d0_out);  num_ds_d1_ms = len(scatters_2x_d1_out)


    # Plotting detector plots using final doubles data
    if PRODUCE_FINAL_DOUBLE_PLOTS:
        plot_final_doubles_data(data_dir, output_file_name, scatters_2x_out, scatters_2x_d0_out, scatters_2x_d1_out)

    ### SAVING DATA TO CSV ###

    print ('\n--- Saving Polaris Data to CSV ---')

    #  adding origins data to scatters_1x
    scatters_1x_origins_out = np.concatenate((scatters_1x_out, origins_1x_out), axis = 1)
    scatters_1x_d0_origins_out = np.concatenate((scatters_1x_d0_out, origins_1x_d0_out), axis = 1)
    scatters_1x_d1_origins_out = np.concatenate((scatters_1x_d1_out, origins_1x_d1_out), axis = 1)

    #  adding origins data to scatters_2x
    scatters_2x_origins_out = np.concatenate((scatters_2x_out, origins_2x_out), axis = 1)
    scatters_2x_d0_origins_out = np.concatenate((scatters_2x_d0_out, origins_2x_d0_out), axis = 1)
    scatters_2x_d1_origins_out = np.concatenate((scatters_2x_d1_out, origins_2x_d1_out), axis = 1)

    #  adding tags to origins data for 1x
    df_1x_o = pd.DataFrame(scatters_1x_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_t = pd.DataFrame(tags_1x_out, index = df_1x_o.index)  # re-using index from scatters_1x_origins_out dataFrame
    scatters_1x_origins_tags_out = pd.concat([df_1x_o, df_1x_t], axis = 1, sort = False)
    #  adding tags to origins data for 1x D0 & D1
    df_1x_d0_o = pd.DataFrame(scatters_1x_d0_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_d0_t = pd.DataFrame(tags_1x_d0_out, index = df_1x_d0_o.index)  # re-using index from scatters_1x_d0_origins_out dataFrame
    scatters_1x_d0_origins_tags_out = pd.concat([df_1x_d0_o, df_1x_d0_t], axis = 1, sort = False)
    df_1x_d1_o = pd.DataFrame(scatters_1x_d1_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_d1_t = pd.DataFrame(tags_1x_d1_out, index = df_1x_d1_o.index)  # re-using index from scatters_1x_d1_origins_out dataFrame
    scatters_1x_d1_origins_tags_out = pd.concat([df_1x_d1_o, df_1x_d1_t], axis = 1, sort = False)

    #  adding tags to origins data for 2x D0 & D1
    df_2x_o = pd.DataFrame(scatters_2x_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_t = pd.DataFrame(tags_2x_out, index = df_2x_o.index)  # re-using index from scatters_2x_origins_out dataFrame
    scatters_2x_origins_tags_out = pd.concat([df_2x_o, df_2x_t], axis = 1, sort = False)
    #  adding tags to origins data for 2x D0 & D1
    df_2x_d0_o = pd.DataFrame(scatters_2x_d0_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_d0_t = pd.DataFrame(tags_2x_d0_out, index = df_2x_d0_o.index)  # re-using index from scatters_2x_d0_origins_out dataFrame
    scatters_2x_d0_origins_tags_out = pd.concat([df_2x_d0_o, df_2x_d0_t], axis = 1, sort = False)
    df_2x_d1_o = pd.DataFrame(scatters_2x_d1_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_d1_t = pd.DataFrame(tags_2x_d1_out, index = df_2x_d1_o.index)  # re-using index from scatters_2x_d1_origins_out dataFrame
    scatters_2x_d1_origins_tags_out = pd.concat([df_2x_d1_o, df_2x_d1_t], axis = 1, sort = False)


    #  creating output file names
    #   - add tags based on filter parameters
    output_file = data_dir + add_tags_to_filename(output_file_name, '.csv')
    output_file_1x = output_file.replace(".csv", "_1x.csv")
    output_file_1x_origins_tags = output_file.replace(".csv", "_1x-origins-tags.csv")

    output_file_1x_d0 = output_file.replace(".csv", "_1x_d0.csv")
    output_file_1x_d0_origins_tags = output_file.replace(".csv", "_1x_d0-origins-tags.csv")

    output_file_1x_d1 = output_file.replace(".csv", "_1x_d1.csv")
    output_file_1x_d1_origins_tags = output_file.replace(".csv", "_1x_d1-origins-tags.csv")

    output_file_2x = output_file.replace(".csv", "_2x.csv")
    output_file_2x_origins_tags = output_file.replace(".csv", "_2x-origins-tags.csv")

    output_file_2x_d0 = output_file.replace(".csv", "_2x_d0.csv")
    output_file_2x_d0_origins_tags = output_file.replace(".csv", "_2x_d0-origins-tags.csv")

    output_file_2x_d1 = output_file.replace(".csv", "_2x_d1.csv")
    output_file_2x_d1_origins_tags = output_file.replace(".csv", "_2x_d1-origins-tags.csv")

    output_file_3x = output_file.replace(".csv", "_3x.csv")
    output_file_2x_3x = output_file.replace(".csv", "_2x+3x.csv")


    #  outputting data to CSV
    #   format: eng1, x1, y1, z1, eng2, x2, y2, z2, (eng3, x3, y3, z3) <- if triple scatter
    #   units: time in us and pos in mm
    if STORE_SINGLE_SCATTERS == True:
        #  1x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_out), 'scatters_1x_out', output_file_1x))
        np.savetxt(output_file_1x, scatters_1x_out, delimiter=',', fmt='%.5f')
        #  1x - D0
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d0_out), 'scatters_1x_d0_out', output_file_1x_d0))
        np.savetxt(output_file_1x_d0, scatters_1x_d0_out, delimiter=',', fmt='%.5f')
        #  1x - D1
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d1_out), 'scatters_1x_d1_out', output_file_1x_d1))
        np.savetxt(output_file_1x_d1, scatters_1x_d1_out, delimiter=',', fmt='%.5f')

        if STORE_ORIGINS_TAGS == True:
            #  1x
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_origins_tags_out), 'scatters_1x_origins_tags_out', output_file_1x_origins_tags))
            scatters_1x_origins_tags_out.to_csv(output_file_1x_origins_tags, index = False, header = False, float_format = '%.5f')
            #  2x - D0
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d0_origins_tags_out), 'scatters_1x_d0_origins_tags_out', output_file_1x_d0_origins_tags))
            scatters_1x_d0_origins_tags_out.to_csv(output_file_1x_d0_origins_tags, index = False, header = False, float_format = '%.5f')
            #  2x - D1
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d1_origins_tags_out), 'scatters_1x_d1_origins_tags_out', output_file_1x_d1_origins_tags))
            scatters_1x_d1_origins_tags_out.to_csv(output_file_1x_d1_origins_tags, index = False, header = False, float_format = '%.5f')

    #  2x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x))
    np.savetxt(output_file_2x, scatters_2x_out, delimiter=',', fmt='%.5f')
    #  2x - D0
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_out), 'scatters_2x_d0_out', output_file_2x_d0))
    np.savetxt(output_file_2x_d0, scatters_2x_d0_out, delimiter=',', fmt='%.5f')
    #  2x - D1
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_out), 'scatters_2x_d1_out', output_file_2x_d1))
    np.savetxt(output_file_2x_d1, scatters_2x_d1_out, delimiter=',', fmt='%.5f')
    #  3x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_3x))
    np.savetxt(output_file_3x, scatters_3x_out, delimiter=',', fmt='%.5f')

    #  Store origin locations and scatter tags for 2x scatter events
    if STORE_ORIGINS_TAGS == True:

        #  2x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_origins_tags_out), 'scatters_2x_origins_tags_out', output_file_2x_origins_tags))
        scatters_2x_origins_tags_out.to_csv(output_file_2x_origins_tags, index = False, header = False, float_format = '%.5f')
        #  2x - D0
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_origins_tags_out), 'scatters_2x_d0_origins_tags_out', output_file_2x_d0_origins_tags))
        scatters_2x_d0_origins_tags_out.to_csv(output_file_2x_d0_origins_tags, index = False, header = False, float_format = '%.5f')
        #  2x - D1
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_origins_tags_out), 'scatters_2x_d1_origins_tags_out', output_file_2x_d1_origins_tags))
        scatters_2x_d1_origins_tags_out.to_csv(output_file_2x_d1_origins_tags, index = False, header = False, float_format = '%.5f')


    #  saving combined 2x and 3x data (using file.open in order to append data to file)
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x_3x))
    ofile = open(output_file_2x_3x, "w")
    np.savetxt(ofile, scatters_2x_out, delimiter=',', fmt='%.5f')
    print ('   - saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_2x_3x))
    np.savetxt(ofile, scatters_3x_out, delimiter=',', fmt='%.5f')
    ofile.close()

    # count final number of double/triple scatters
    num_ss_fi = len(scatters_1x_out)
    num_ds_fi = len(scatters_2x_out);  num_ds_d0_fi = len(scatters_2x_d0_out)
    num_ds_d1_fi = len(scatters_2x_d1_out);  num_ts_fi = len(scatters_3x_out)

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  save run summary to file
    summary_file = output_file.replace(".txt", "").replace(".dat", "").replace(".csv", "") + "-summary.txt"
    f = open(summary_file, "w+")
    f.write ('--- Completed processing / time required {} s'.format(stop - start))
    f.write ('\n-------  SUMMARY  -------')
    f.write ('\n-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    #print ('--- Start time (s): {} | End time (s): {} | Run duration (s): {} ({} hrs) | Run activity (events/s): {}'.format(time_start, time_end, (time_end - time_start), (time_end - time_start)/3600, len(cc_dataframe) / (time_end - time_start)))
    f.write ('\n-- Number of 1x pixel events: {}, 2x pixel events: {} ({} double scatters), 3x pixel events: {} ({} triple scatters), 4x+ pixel events: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    #print ('--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    f.write ('\n-- Run Options -> Big Energy Sort: {} / Apply Energy Threshold: {} / Remove Unphysical Events: {} / Compton Line Filtering: {} / Shuffle Events: {} / Multistage Coincidence: {}'.format(APPLY_BIG_ENERGY_SORTING, APPLY_ENERGY_THRESHOLD, REMOVE_UNPHYSICAL_EVENTS, APPLY_COMPTON_LINE_FILTERING, SHUFFLE_SCATTER_EVENTS, APPLY_COINCIDENCE_GROUPING_MULTISTAGE))
    if APPLY_COMPTON_LINE_FILTERING: f.write ('\n--- Compton line filter settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE: f.write ('\n--- Multistage coincidence settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    f.write ('\n-- Single Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ss, num_ss_et, num_ss_fi))
    f.write ('\n-- Double Scatters Events       -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds, num_ds_et, num_ds_pe, num_ds_cl, num_ds_ms, num_ds_fi, ((float(num_ds_fi)/num_ds) * 100.0)))
    f.write ('\n-- Double Scatters Events in D0 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d0, num_ds_d0_et, num_ds_d0_pe, num_ds_d0_cl, num_ds_d0_ms, num_ds_d0_fi, ((float(num_ds_d0_fi)/num_ds_d0) * 100.0)))
    f.write ('\n-- Double Scatters Events in D1 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d1, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_d1_fi, ((float(num_ds_d1_fi)/num_ds_d1) * 100.0)))
    f.write ('\n-- Triple Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts, num_ts_et, num_ts_fi))
    f.write ('\n--------------------------')
    f.close()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    print ('-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    #print ('--- Start time (s): {} | End time (s): {} | Run duration (s): {} ({} hrs) | Run activity (events/s): {}'.format(time_start, time_end, (time_end - time_start), (time_end - time_start)/3600, len(cc_dataframe) / (time_end - time_start)))
    print ('-- Number of 1x pixel interactions: {}, 2x pixel interactions: {} ({} double scatter events), 3x pixel interactions: {} ({} triple scatter events), 4x+ pixel interactions: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    print ('-- Multi-Stage interactions -> Total: {}, 2x pixel interactions: {} ({} events), 3x pixel interactions: {} ({} events)'.format(num_ms_total, num_ms_2px, num_ms_2px/2, num_ms_3px, num_ms_3px/3))
    #print ('--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    print ('-- Run Options -> Big Energy Sort: {} / Apply Energy Threshold: {} / Remove Unphysical Events: {} / Compton Line Filtering: {} / Shuffle Events: {} / Multistage Coincidence: {}'.format(APPLY_BIG_ENERGY_SORTING, APPLY_ENERGY_THRESHOLD, REMOVE_UNPHYSICAL_EVENTS, APPLY_COMPTON_LINE_FILTERING, SHUFFLE_SCATTER_EVENTS, APPLY_COINCIDENCE_GROUPING_MULTISTAGE))
    if APPLY_COMPTON_LINE_FILTERING: print ('--- Compton line filter settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE: print ('--- Multistage coincidence settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    print ('-- Single Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ss, num_ss_et, num_ss_fi))
    print ('-- Double Scatters Events       -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds, num_ds_et, num_ds_pe, num_ds_cl, num_ds_ms, num_ds_fi, ((float(num_ds_fi)/num_ds) * 100.0)))
    print ('-- Double Scatters Events in D0 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_d0, num_ds_d0_et, num_ds_d0_pe, num_ds_d0_cl, num_ds_d0_ms, num_ds_d0_fi, ((float(num_ds_d0_fi)/num_ds_d0) * 100.0)))
    print ('-- Double Scatters Events in D1 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_d1, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_d1_fi, ((float(num_ds_d1_fi)/num_ds_d1) * 100.0)))
    print ('-- Triple Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts, num_ts_et, num_ts_fi))
    print ('--------------------------')


# Process the ROOT particle data, filter it and store output csv files
def filter_particle_data(data_dir, root_tree_name, USE_DIR_AS_OUTPUT = False):
    '''
    Takes in an AllEventsCombined.txt file, reads the information into a pandas
    array and then filters data using the flagged parameters

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    @returns:
        scatters_2x_out     - numpy array of the processed double scatter data recorded by both polaris detectors
        scatters_2x_nns_out - numpy array of the processed double scatter data recorded by both polaris detectors (non-neutron-secondaries)
        scatters_2x_d0_out  - numpy array of the processed double scatter data recorded by polaris detector 0
        scatters_2x_d1_out  - numpy array of the processed double scatter data recorded by polaris detector 1
        scatters_3x_out     - numpy array of the processed triple scatter data recorded by both polaris detectors
    '''

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  Setting output_file_name, either breaking up data_dir or using root_tree_name (default)
    if USE_DIR_AS_OUTPUT:
        data_dir_split = data_dir.split('/')
        output_file_name = data_dir_split[-3]   # use third from end (drops "root" and blank space)
        print ('   - Using data_dir to set output filename -> {}'.format(output_file_name))
    else:
        output_file_name = root_tree_name

    #### NOTE: need to figure out how to drop data_dir down one level (remove /root/)

    allevents_data, root_file_list = read_root_data(data_dir, root_tree_name, output_file_name)       # read in the simulated scatter data
    tot_raw_events = len(allevents_data.index)          # get the total number of events before coincidence processing

    #  Counting gamma scatter interactions (before the interactions were grouped into events)
    nbInt_prompt = 0; nbInt_np_neutron = 0; nbInt_np_other = 0       # count prompt/neutron-induced/other events
    nbInt_zero = 0; nbInt_single = 0; nbInt_multiple = 0           # count zero/single/multiple scatter events
    nbInt_nonScat = 0; nbInt_tarScat = 0; nbInt_detScat = 0; nbInt_othScat = 0; nbInt_mulScat = 0      # count scatter in none/target/detector/other/multiple
    nbInt_tarCreat = 0; nbInt_detCreat = 0; nbInt_othCreat = 0    # count creations in target/detector/other

    for i in range(len(allevents_data['tag'])):
        # there are only tags for gammas, so skipping non-gammas, maybe add a counter here
        if ( len(allevents_data['tag'][i]) > 0):
            #print (' ****** allevents_data[tag][{}][0]: {}'.format(i, allevents_data['tag'][i][0]))
            #  count prompt/nonprompt events -> tag[0] / 1st [particleType]: (P)rompt, (N)eutron-induced or (O)ther gamma
            if allevents_data['tag'][i][0] == 'P':  nbInt_prompt += 1
            if allevents_data['tag'][i][0] == 'N':  nbInt_np_neutron += 1
            else:  nbInt_np_other += 1
            #  count number of scatter events -> tag[1] / 2nd [scatterNumber]: (Z)ero, (S)ingle, (M)ultiple scatters
            if allevents_data['tag'][i][1] == 'Z':  nbInt_zero += 1
            elif allevents_data['tag'][i][1] == 'S':  nbInt_single += 1
            else:  nbInt_multiple += 1
            #  count number of scatter events -> tag[2] / 3rd [scatterLocation]: (T)arget, (D)etector, (O)ther, (M)ultiple, (N)one
            if allevents_data['tag'][i][2] == 'N':  nbInt_nonScat += 1
            elif allevents_data['tag'][i][2] == 'T':  nbInt_tarScat += 1
            elif allevents_data['tag'][i][2] == 'D':  nbInt_detScat += 1
            elif allevents_data['tag'][i][2] == 'O':  nbInt_othScat += 1
            else:  nbInt_mulScat += 1
            #  count number of scatter events -> tag[3] / 4th [creationLocation]: (T)arget, (D)etector, (O)ther
            if allevents_data['tag'][i][3] == 'T':  nbInt_tarCreat += 1
            elif allevents_data['tag'][i][3] == 'D':  nbInt_detCreat += 1
            else:  nbInt_othCreat += 1

    #  output gamma scatter details
    print ('\n  Counting gamma scatter interactions . . .')
    print ('   - Total Interactions: {} | prompt gamma interactions: {} ({:.2f}%) | non-prompt (neutron-induced) gamma interactions: {} ({:.2f}%) | non-prompt (other) gamma interactions: {} ({:.2f}%)'.format(len(allevents_data['tag']), nbInt_prompt, (float(nbInt_prompt)/len(allevents_data['tag']))*100.0, nbInt_np_neutron, (float(nbInt_np_neutron)/len(allevents_data['tag']))*100.0, nbInt_np_other, (float(nbInt_np_other)/len(allevents_data['tag']))*100.0))
    print ('   - Number of Scatters | zero: {} ({:.2f}%) | single: {} ({:.2f}%) | multiple: {} ({:.2f}%)'.format(nbInt_zero, (float(nbInt_zero)/len(allevents_data['tag']))*100.0, nbInt_single, (float(nbInt_single)/len(allevents_data['tag']))*100.0, nbInt_multiple, (float(nbInt_multiple)/len(allevents_data['tag']))*100.0))
    print ('   - Scatter Location | none: {} | target: {} | detector: {} | other: {} | multiple: {}'.format(nbInt_nonScat, nbInt_tarScat, nbInt_detScat, nbInt_othScat, nbInt_mulScat))
    print ('   - Creation Location | target: {} | detector: {} | other: {}'.format(nbInt_tarCreat, nbInt_detCreat, nbInt_othCreat))


    # Counters for run summary
    num_1px, num_2px, num_3px, num_4px = [float('nan')] * 4                # count events based on pixel number
    num_ss, num_ss_et, num_ss_fi = [float('nan')] * 3                      # count events for single scatters
    num_ds, num_ds_d0, num_ds_d1, num_ts = [float('nan')] * 4              # count total number of double/triple scatters
    num_ds_et, num_ds_d0_et, num_ds_d1_et, num_ts_et = [float('nan')] * 4  # count number of energy threshold events
    num_ds_pe, num_ds_d0_pe, num_ds_d1_pe = [float('nan')] * 3             # count number of physical events
    num_ds_cl, num_ds_d0_cl, num_ds_d1_cl = [float('nan')] * 3             # count number of Compton line filtered events
    num_ds_ms, num_ds_d0_ms, num_ds_d1_ms = [float('nan')] * 3             # count number of multistage coincidence events
    num_ds_fi, num_ds_d0_fi, num_ds_d1_fi, num_ts_fi = [float('nan')] * 4  # final number of double/triple scatters
    num_ms_total, num_ms_2px, num_ms_3px = [float('nan')] * 3              # count multi-stage interactions

    # Counters for scatter tags
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = [float('nan')] * 6
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = [float('nan')] * 6
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = [float('nan')] * 6

    # count number of pixel events
    num_1px = len(allevents_data[allevents_data.scatters == 1]);  num_2px = len(allevents_data[allevents_data.scatters == 2])
    num_3px = len(allevents_data[allevents_data.scatters == 3]);  num_4px = len(allevents_data[allevents_data.scatters  > 3])

    # count number of multi-stage interactions
    num_ms_total = len(allevents_data[allevents_data.sStg == False])
    num_ms_2px = len(allevents_data[(allevents_data.scatters == 2) & (allevents_data.sStg == False)])
    num_ms_3px = len(allevents_data[(allevents_data.scatters == 3) & (allevents_data.sStg == False)])

    # count number of particles by type (only gammas have 2x and 3x)
    num_ga = len(allevents_data[allevents_data.particle == 22])

    # count number of single-scatters by particle type
    num_ss_ga = len(allevents_data[ (allevents_data.particle == 22) & (allevents_data.scatters == 1) ])
    num_ss_ne = len(allevents_data[ (allevents_data.particle == 2112) & (allevents_data.scatters == 1) ])
    num_ss_pr = len(allevents_data[ (allevents_data.particle == 2212) & (allevents_data.scatters == 1) ])
    num_ss_po = len(allevents_data[ (allevents_data.particle == -11) & (allevents_data.scatters == 1) ])
    num_ss_io = len(allevents_data[ (allevents_data.particle > 1e8) & (allevents_data.scatters == 1) ])
    num_ss_ot = num_1px - num_ss_ga - num_ss_ne - num_ss_pr - num_ss_po - num_ss_io

    # count number of particles based on origin (neutron-secondary)
    num_ns = len(allevents_data[ allevents_data.nTag == True ])
    num_ns_ga = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.particle == 22) ])
    num_ns_ne = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.particle == 2112) ])
    num_ns_pr = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.particle == 2212) ])
    num_ns_po = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.particle == -11) ])
    num_ns_io = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.particle > 1e8) ])
    num_ns_ot = num_ns - num_ns_ga - num_ns_ne - num_ns_pr - num_ns_po - num_ns_io
    # count number of particles based on origin (non-neutron-secondary)
    num_nns = len(allevents_data[ allevents_data.nTag == False ])
    num_nns_ga = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.particle == 22) ])
    num_nns_ne = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.particle == 2112) ])
    num_nns_pr = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.particle == 2212) ])
    num_nns_po = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.particle == -11) ])
    num_nns_io = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.particle > 1e8) ])
    num_nns_ot = num_nns - num_nns_ga - num_nns_ne - num_nns_pr - num_nns_po - num_nns_io

    # count single, double, triple scatters based on origin (neutron-secondary)
    num_ns_1x = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.scatters == 1) ])
    num_ns_2x = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.scatters == 2) ])
    num_ns_3x = len(allevents_data[ (allevents_data.nTag == True) & (allevents_data.scatters == 3) ])
    # count single, double, triple scatters based on origin (non-neutron-secondary)
    num_nns_1x = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.scatters == 1) ])
    num_nns_2x = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.scatters == 2) ])
    num_nns_3x = len(allevents_data[ (allevents_data.nTag == False) & (allevents_data.scatters == 3) ])

    # summing energy deposited by particle type
    ed_all = allevents_data.energy.sum()
    ed_ga = allevents_data[ allevents_data.particle == 22 ].energy.sum()
    ed_ne = allevents_data[ allevents_data.particle == 2112 ].energy.sum()
    ed_pr = allevents_data[ allevents_data.particle == 2212 ].energy.sum()
    ed_po = allevents_data[ allevents_data.particle == -11 ].energy.sum()
    ed_io = allevents_data[ allevents_data.particle > 1e8 ].energy.sum()

    # summing energy deposited by origin
    ed_ns = allevents_data[ allevents_data.nTag == True ].energy.sum()
    ed_nns = allevents_data[ allevents_data.nTag == False ].energy.sum()
    ed_ga_ns = allevents_data[ (allevents_data.particle == 22) & (allevents_data.nTag == True) ].energy.sum()
    ed_ga_nns = allevents_data[ (allevents_data.particle == 22) & (allevents_data.nTag == False) ].energy.sum()

    # summing time stamps by particle type
    timing_cutoff = 20  # 1e11 # in ns, 1e11 = 100 sec
    ts_all = allevents_data[ allevents_data.time < timing_cutoff ].time.sum()
    ts_ga = allevents_data[ (allevents_data.particle == 22) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_ne = allevents_data[ (allevents_data.particle == 2112) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_pr = allevents_data[ (allevents_data.particle == 2212) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_po = allevents_data[ (allevents_data.particle == -11) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_io = allevents_data[ (allevents_data.particle > 1e8) & (allevents_data.time < timing_cutoff) ].time.sum()

    # summing energy deposited by origin
    ts_ns = allevents_data[ (allevents_data.nTag == True) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_nns = allevents_data[ (allevents_data.nTag == False) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_ga_ns = allevents_data[ (allevents_data.particle == 22) & (allevents_data.nTag == True) & (allevents_data.time < timing_cutoff) ].time.sum()
    ts_ga_nns = allevents_data[ (allevents_data.particle == 22) & (allevents_data.nTag == False) & (allevents_data.time < timing_cutoff) ].time.sum()


    print('\nFiltering data')
    print('-----------------------')

    # Plotting basic detector plots using transformed data
    if PRODUCE_TRANSFORMED_PLOTS:
        plot_transformed_data(data_dir, output_file_name, allevents_data)

    print ('\n--- Grouping Compton Scatter Data ---')

    if APPLY_BIG_ENERGY_SORTING:
        print ('  Sorting scatters to place largest energy deposition first . . .')
    #  returns interactions (E, X, Y, Z), tags (4 character string) and origins (E, X, Y, Z) for the specified number of scatters and detector number
    #   format -> def get_interaction_data_tags_origin(df, energy_sort = True, num_scatters = 2, det_num = None)
    scatters_1x = utils.get_interaction_data(allevents_data, APPLY_BIG_ENERGY_SORTING, 1)
    scatters_2x = utils.get_interaction_data(allevents_data, APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_ns = utils.get_interaction_data(allevents_data[allevents_data.nTag == True], APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_nns = utils.get_interaction_data(allevents_data[allevents_data.nTag == False], APPLY_BIG_ENERGY_SORTING, 2)
    scatters_3x = utils.get_interaction_data(allevents_data, APPLY_BIG_ENERGY_SORTING, 3)
    scatters_3x_ns = utils.get_interaction_data(allevents_data[allevents_data.nTag == True], APPLY_BIG_ENERGY_SORTING, 3)
    scatters_3x_nns = utils.get_interaction_data(allevents_data[allevents_data.nTag == False], APPLY_BIG_ENERGY_SORTING, 3)
    """
    scatters_1x, tags_1x, origins_1x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1)
    scatters_1x_d0, tags_1x_d0, origins_1x_d0 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1, 0)
    scatters_1x_d1, tags_1x_d1, origins_1x_d1 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 1, 1)
    scatters_2x, tags_2x, origins_2x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2)
    scatters_2x_d0, tags_2x_d0, origins_2x_d0 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2, 0)
    scatters_2x_d1, tags_2x_d1, origins_2x_d1 = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 2, 1)
    scatters_3x, tags_3x, origins_3x = utils.get_interaction_data_tags_origin(allevents_data, APPLY_BIG_ENERGY_SORTING, 3)
    """

    ### FORMATTING DATA FOR OUTPUT ###

    print ('\n--- Formatting Compton Scatter Data for Output ---')
    print ('  Re-arranging data array for CSV output . . .')

    #  re-arranges interaction data into CSV format (entire event on one line)
    #   format -> def format_data_for_output(scatters, tags, num_scatters, det_num = None):
    scatters_1x_out = scatters_1x #;  scatters_1x_d0_out = scatters_1x_d0;  scatters_1x_d1_out = scatters_1x_d1  # no rearrangment required
    scatters_2x_out = utils.format_data_for_output(scatters_2x, 2)
    scatters_2x_ns_out = utils.format_data_for_output(scatters_2x_ns, 2)
    scatters_2x_nns_out = utils.format_data_for_output(scatters_2x_nns, 2)
    scatters_3x_out = utils.format_data_for_output(scatters_3x, 3)
    scatters_3x_ns_out = utils.format_data_for_output(scatters_3x_ns, 3)
    scatters_3x_nns_out = utils.format_data_for_output(scatters_3x_nns, 3)
    """
    #tags_1x_out = allevents_data[allevents_data.scatters == 1].tag.to_numpy()  # creating tags array
    tags_1x_out = tags_1x;  tags_1x_d0_out = tags_1x_d0;  tags_1x_d1_out = tags_1x_d1  # creating tags array
    origins_1x_out = origins_1x;  origins_1x_d0_out = origins_1x_d0;  origins_1x_d1_out = origins_1x_d1  # creating origins array
    scatters_2x_out, tags_2x_out, origins_2x_out = utils.format_data_for_output_tags_origins(scatters_2x, tags_2x, origins_2x, 2)
    scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.format_data_for_output_tags_origins(scatters_2x_d0, tags_2x_d0, origins_2x_d0, 2, 0)
    scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.format_data_for_output_tags_origins(scatters_2x_d1, tags_2x_d1, origins_2x_d1, 2, 1)
    scatters_3x_out, tags_3x_out, origins_3x_out = utils.format_data_for_output_tags_origins(scatters_3x, tags_3x, origins_3x, 3)
    """

    # count initial number of double/triple scatters
    num_ss = len(scatters_1x)
    num_ds = len(scatters_2x_out)
    # num_ds_d0 = len(scatters_2x_d0_out);  num_ds_d1 = len(scatters_2x_d1_out);
    num_ds_ns = len(scatters_2x_ns_out);  num_ds_nns = len(scatters_2x_nns_out);
    num_ts = len(scatters_3x_out)
    num_ts_ns = len(scatters_3x_ns_out);  num_ts_nns = len(scatters_3x_nns_out)

    """
    print ('\n  Counting 1x gamma scatter events . . .')
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = utils.count_gamma_scatter(scatters_1x_out, tags_1x_out)
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    print ('\n  Counting 3x gamma scatter events . . .')
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = utils.count_gamma_scatter(scatters_3x_out, tags_3x_out)
    """

    ### FILTERING DETECTOR DATA ###

    print ('\n--- Filtering Detector Data ---')

    #  removing events below energy threshold (set by MINIMUM_ENERGY_THRESHOLD)
    #   - removes full event if any of the scatters fall below threshold
    if APPLY_ENERGY_THRESHOLD:


        # NEED TO UPDATE UTILS.  NO non-tags, origins version of filtering_energy_threshold

        print ('  Removing events below energy threshold ({} MeV) . . .'.format(MINIMUM_ENERGY_THRESHOLD))
        scatters_1x_out, tags_1x_out, origins_1x_out = utils.filtering_energy_threshold_tags_origins(scatters_1x, tags_1x_out, origins_1x_out, 1, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_out, tags_2x_out, origins_2x_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.filtering_energy_threshold_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out, 2, MINIMUM_ENERGY_THRESHOLD)
        scatters_3x_out, tags_3x_out, origins_3x_out = utils.filtering_energy_threshold_tags_origins(scatters_3x_out, tags_3x_out, origins_3x_out, 3, MINIMUM_ENERGY_THRESHOLD)

        # count number of remaining double/triple scatters after energy threshold applied
        num_ss_et = len(scatters_1x_out)
        num_ds_et = len(scatters_2x_out);  num_ds_d0_et = len(scatters_2x_d0_out)
        num_ds_d1_et = len(scatters_2x_d1_out);  num_ts_et = len(scatters_3x_out)

    """
    print ('\n  Counting 1x gamma scatter events . . .')
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_PM, nbEvt_1x_NM = utils.count_gamma_scatter(scatters_1x_out, tags_1x_out)
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    print ('\n  Counting 3x gamma scatter events . . .')
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_PM, nbEvt_3x_NM = utils.count_gamma_scatter(scatters_3x_out, tags_3x_out)
    """

    #  removing unphysical events (Compton scatter angle == nan)
    #   - checks both ordering of two scatters and flips if original ordering in unphysical
    #   - modified code from Matt Leigh's Filter.py & CPUFunctions.cu
    if REMOVE_UNPHYSICAL_EVENTS:
        print ('\n  Removing unphysical events (double scatters) . . .')
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out = utils.filtering_unphysical_double_scatters(scatters_2x_out)
        scatters_2x_ns_out = utils.filtering_unphysical_double_scatters(scatters_2x_ns_out)
        scatters_2x_nns_out = utils.filtering_unphysical_double_scatters(scatters_2x_nns_out)
        #scatters_2x_out, tags_2x_out, origins_2x_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out)
        """
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.filtering_unphysical_double_scatters_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out)
        """
        # count number of physical double scatters
        num_ds_pe = len(scatters_2x_out); # num_ds_d0_pe = len(scatters_2x_d0_out);  num_ds_d1_pe = len(scatters_2x_d1_out)

    """
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    """

    #  applying Compton line filtering (checks if E1 and theta1 are consistent with Compton formula)
    #   - use COMPTON_LINE_RANGE (accepted energy range) & COMPTON_LINE_ENERGIES (expected gamma energies) global variables
    #   - modified code from Matt Leigh's Filter.py
    if APPLY_COMPTON_LINE_FILTERING:
        print ('\n  Applying Compton line filtering / Energies (MeV): {} / Range of values: {}'.format(COMPTON_LINE_ENERGIES, COMPTON_LINE_RANGE))
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_out', len(scatters_2x_out)))
        scatters_2x_out = utils.compton_line_filtering(scatters_2x_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        scatters_2x_ns_out = utils.compton_line_filtering(scatters_2x_ns_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        scatters_2x_nns_out = utils.compton_line_filtering(scatters_2x_nns_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        #scatters_2x_out, tags_2x_out, origins_2x_out = utils.compton_line_filtering_tags_origins(scatters_2x_out, tags_2x_out, origins_2x_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        """
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d0_out', len(scatters_2x_d0_out)))
        scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out = utils.compton_line_filtering_tags_origins(scatters_2x_d0_out, tags_2x_d0_out, origins_2x_d0_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        print ('   - Filtering {} | total events checked: {}'.format('scatters_2x_d1_out', len(scatters_2x_d1_out)))
        scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out = utils.compton_line_filtering_tags_origins(scatters_2x_d1_out, tags_2x_d1_out, origins_2x_d1_out, COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES)
        """
        # count number of Compton line filtered double scatters
        num_ds_cl = len(scatters_2x_out); # num_ds_d0_cl = len(scatters_2x_d0_out);  num_ds_d1_cl = len(scatters_2x_d1_out)

    """
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_PM, nbEvt_2x_NM = utils.count_gamma_scatter(scatters_2x_out, tags_2x_out)
    """

    ### APPLYING ENERGY WEIGHTING POTENTIAL ###

    print ('\n--- Applying Weighting Potential ---')

    #  groups events from different modules based on energy and time stamp
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_WEIGHTING_POTENTIAL:
        print ('  Scaling enegy deposited based on depth in crystal . . .')

        '''
        #  returns the energy deposited scaling factor based on the weighting potential method for signal induction.
        #  Weighting potential calculation is done using a fit calculated from the following reference:
        #  https://cztlab.engin.umich.edu/wp-content/uploads/sites/187/2015/03/zhangf.pdf

        #  The calculation assumes CZT plane (either 10 mm or 15 mm) with anode pointing away the radioactive source (i.e. anode at 0 mm in detector geometry)
        '''

        #  Scaling factor for 10 mm crystal
        #  p2A is the position of the back of the detector in mm
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11 * (x - p2A) / 10))   # for D0 located along x-axis (back of detector has max -x value)
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11 * (p2A - y) / 10))   # for D1 located along y-axis (back of detector has max +y value)

        #  Scaling factor for 10 mm crystal
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11*(y-p1A)/10))

        #  Scaling factor for 15 mm crystal
        #edepScale = 1 - (0.9808 * numpy.exp(-23.11*(p2A-y)/15))

        print ('    ######  CURRENTLY NOT FUNCTIONING  ######')

    """
    def calcWeightingPotential(y,p1A,p2A):
        '''
        edepScale = calcWeightingPotential(y,p1A,p2A)
        returns the energy deposited scaling factor based on the weighting potential method for signal induction.
        Weighting potential calculation is done using a fit calculated from the following reference:
        https://cztlab.engin.umich.edu/wp-content/uploads/sites/187/2015/03/zhangf.pdf

        The calculation assumes two opposed CZT planes (one 10 mm, one 15 mm) with anodes on the exterior surfaces.

        plane1 (10 mm)            plane 2 (15 mm)
        |                    +y2 <------ y2=p2A |
        |                                       |
        |                                       |
        |y1=p1A ------> +y1                     |

        input:
            y : raw interaction depth
            p1A, p2A : y location of the two anodes

        output:
            edepScale : multiplicitive scaling factor for deposited energy
        '''
        edepScale = 1
        if y <= p1A+10.1:
            edepScale = 1 - (0.9808 * np.exp(-23.11*(y-p1A)/10))
        else:
            edepScale = 1 - (0.9808 * np.exp(-23.11*(p2A-y)/15))
        return edepScale
    """


    #  shuffle the 2x & 3x scatter events
    #   - to overcome timing errors (like runs 6, 7, 8 for 180315 data)
    if SHUFFLE_SCATTER_EVENTS:

        #  - np.random.shuffle: Modify a sequence in-place by shuffling its contents.
        np.random.shuffle(scatters_2x_out)
        np.random.shuffle(scatters_3x_out)


    ### GROUPING COINCIDENCE EVENTS ###

    print ('\n--- Grouping Coincidence Events ---')

    #  groups events from different modules based on energy and time stamp
    #   - modified code from Paul Maggi's coincCheckMod function
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE:
        print ('  Grouping multi-stage coincidence events . . .')

        print ('    ######  CURRENTLY NOT FUNCTIONING  ######')

        # count number of multi-stage coincidence double scatters
        # num_ds_ms = len(scatters_2x_out);  num_ds_d0_ms = len(scatters_2x_d0_out);  num_ds_d1_ms = len(scatters_2x_d1_out)


    # Plotting detector plots using final doubles data
    if PRODUCE_FINAL_DOUBLE_PLOTS:
        plot_final_doubles_data(data_dir, output_file_name, scatters_2x_out, scatters_2x_d0_out, scatters_2x_d1_out)

    ### SAVING DATA TO CSV ###

    print ('\n--- Saving Polaris Data to CSV ---')

    """
    #  adding origins data to scatters_1x
    scatters_1x_origins_out = np.concatenate((scatters_1x_out, origins_1x_out), axis = 1)
    scatters_1x_d0_origins_out = np.concatenate((scatters_1x_d0_out, origins_1x_d0_out), axis = 1)
    scatters_1x_d1_origins_out = np.concatenate((scatters_1x_d1_out, origins_1x_d1_out), axis = 1)

    #  adding origins data to scatters_2x
    scatters_2x_origins_out = np.concatenate((scatters_2x_out, origins_2x_out), axis = 1)
    scatters_2x_d0_origins_out = np.concatenate((scatters_2x_d0_out, origins_2x_d0_out), axis = 1)
    scatters_2x_d1_origins_out = np.concatenate((scatters_2x_d1_out, origins_2x_d1_out), axis = 1)

    #  adding tags to origins data for 1x
    df_1x_o = pd.DataFrame(scatters_1x_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_t = pd.DataFrame(tags_1x_out, index = df_1x_o.index)  # re-using index from scatters_1x_origins_out dataFrame
    scatters_1x_origins_tags_out = pd.concat([df_1x_o, df_1x_t], axis = 1, sort = False)
    #  adding tags to origins data for 1x D0 & D1
    df_1x_d0_o = pd.DataFrame(scatters_1x_d0_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_d0_t = pd.DataFrame(tags_1x_d0_out, index = df_1x_d0_o.index)  # re-using index from scatters_1x_d0_origins_out dataFrame
    scatters_1x_d0_origins_tags_out = pd.concat([df_1x_d0_o, df_1x_d0_t], axis = 1, sort = False)
    df_1x_d1_o = pd.DataFrame(scatters_1x_d1_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_d1_t = pd.DataFrame(tags_1x_d1_out, index = df_1x_d1_o.index)  # re-using index from scatters_1x_d1_origins_out dataFrame
    scatters_1x_d1_origins_tags_out = pd.concat([df_1x_d1_o, df_1x_d1_t], axis = 1, sort = False)

    #  adding tags to origins data for 2x D0 & D1
    df_2x_o = pd.DataFrame(scatters_2x_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_t = pd.DataFrame(tags_2x_out, index = df_2x_o.index)  # re-using index from scatters_2x_origins_out dataFrame
    scatters_2x_origins_tags_out = pd.concat([df_2x_o, df_2x_t], axis = 1, sort = False)
    #  adding tags to origins data for 2x D0 & D1
    df_2x_d0_o = pd.DataFrame(scatters_2x_d0_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_d0_t = pd.DataFrame(tags_2x_d0_out, index = df_2x_d0_o.index)  # re-using index from scatters_2x_d0_origins_out dataFrame
    scatters_2x_d0_origins_tags_out = pd.concat([df_2x_d0_o, df_2x_d0_t], axis = 1, sort = False)
    df_2x_d1_o = pd.DataFrame(scatters_2x_d1_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_d1_t = pd.DataFrame(tags_2x_d1_out, index = df_2x_d1_o.index)  # re-using index from scatters_2x_d1_origins_out dataFrame
    scatters_2x_d1_origins_tags_out = pd.concat([df_2x_d1_o, df_2x_d1_t], axis = 1, sort = False)
    """

    #  creating output file names
    #   - add tags based on filter parameters
    output_file = data_dir + add_tags_to_filename(output_file_name, '.csv')
    output_file_1x = output_file.replace(".csv", "_1x.csv")
    #output_file_1x_origins_tags = output_file.replace(".csv", "_1x-origins-tags.csv")
    """
    output_file_1x_d0 = output_file.replace(".csv", "_1x_d0.csv")
    output_file_1x_d0_origins_tags = output_file.replace(".csv", "_1x_d0-origins-tags.csv")

    output_file_1x_d1 = output_file.replace(".csv", "_1x_d1.csv")
    output_file_1x_d1_origins_tags = output_file.replace(".csv", "_1x_d1-origins-tags.csv")
    """
    output_file_2x = output_file.replace(".csv", "_2x.csv")
    output_file_2x_ns = output_file.replace(".csv", "_2x_ns.csv")
    output_file_2x_nns = output_file.replace(".csv", "_2x_nns.csv")
    #output_file_2x_origins_tags = output_file.replace(".csv", "_2x-origins-tags.csv")
    """
    output_file_2x_d0 = output_file.replace(".csv", "_2x_d0.csv")
    output_file_2x_d0_origins_tags = output_file.replace(".csv", "_2x_d0-origins-tags.csv")

    output_file_2x_d1 = output_file.replace(".csv", "_2x_d1.csv")
    output_file_2x_d1_origins_tags = output_file.replace(".csv", "_2x_d1-origins-tags.csv")
    """
    output_file_3x = output_file.replace(".csv", "_3x.csv")
    output_file_3x_ns = output_file.replace(".csv", "_3x_ns.csv")
    output_file_3x_nns = output_file.replace(".csv", "_3x_nns.csv")
    output_file_2x_3x = output_file.replace(".csv", "_2x+3x.csv")


    #  outputting data to CSV
    #   format: eng1, x1, y1, z1, eng2, x2, y2, z2, (eng3, x3, y3, z3) <- if triple scatter
    #   units: time in us and pos in mm
    if STORE_SINGLE_SCATTERS == True:
        #  1x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_out), 'scatters_1x_out', output_file_1x))
        np.savetxt(output_file_1x, scatters_1x_out, delimiter=',', fmt='%.5f')
        """
        #  1x - D0
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d0_out), 'scatters_1x_d0_out', output_file_1x_d0))
        np.savetxt(output_file_1x_d0, scatters_1x_d0_out, delimiter=',', fmt='%.5f')
        #  1x - D1
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d1_out), 'scatters_1x_d1_out', output_file_1x_d1))
        np.savetxt(output_file_1x_d1, scatters_1x_d1_out, delimiter=',', fmt='%.5f')

        if STORE_ORIGINS_TAGS == True:
            #  1x
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_origins_tags_out), 'scatters_1x_origins_tags_out', output_file_1x_origins_tags))
            scatters_1x_origins_tags_out.to_csv(output_file_1x_origins_tags, index = False, header = False, float_format = '%.5f')
            #  2x - D0
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d0_origins_tags_out), 'scatters_1x_d0_origins_tags_out', output_file_1x_d0_origins_tags))
            scatters_1x_d0_origins_tags_out.to_csv(output_file_1x_d0_origins_tags, index = False, header = False, float_format = '%.5f')
            #  2x - D1
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_d1_origins_tags_out), 'scatters_1x_d1_origins_tags_out', output_file_1x_d1_origins_tags))
            scatters_1x_d1_origins_tags_out.to_csv(output_file_1x_d1_origins_tags, index = False, header = False, float_format = '%.5f')
        """
    #  2x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x))
    np.savetxt(output_file_2x, scatters_2x_out, delimiter=',', fmt='%.5f')
    #  2x - NS
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_ns_out), 'scatters_2x_ns_out', output_file_2x_ns))
    np.savetxt(output_file_2x_ns, scatters_2x_ns_out, delimiter=',', fmt='%.5f')
    #  2x - NNS
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_nns_out), 'scatters_2x_nns_out', output_file_2x_nns))
    np.savetxt(output_file_2x_nns, scatters_2x_nns_out, delimiter=',', fmt='%.5f')
    """
    #  2x - D0
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_out), 'scatters_2x_d0_out', output_file_2x_d0))
    np.savetxt(output_file_2x_d0, scatters_2x_d0_out, delimiter=',', fmt='%.5f')
    #  2x - D1
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_out), 'scatters_2x_d1_out', output_file_2x_d1))
    np.savetxt(output_file_2x_d1, scatters_2x_d1_out, delimiter=',', fmt='%.5f')
    """
    #  3x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_3x))
    np.savetxt(output_file_3x, scatters_3x_out, delimiter=',', fmt='%.5f')
    #  3x - NS
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_ns_out), 'scatters_3x_ns_out', output_file_3x_ns))
    np.savetxt(output_file_3x_ns, scatters_3x_ns_out, delimiter=',', fmt='%.5f')
    #  3x - NNS
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_nns_out), 'scatters_3x_nns_out', output_file_3x_nns))
    np.savetxt(output_file_3x_nns, scatters_3x_nns_out, delimiter=',', fmt='%.5f')

    """
    #  Store origin locations and scatter tags for 2x scatter events
    if STORE_ORIGINS_TAGS == True:

        #  2x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_origins_tags_out), 'scatters_2x_origins_tags_out', output_file_2x_origins_tags))
        scatters_2x_origins_tags_out.to_csv(output_file_2x_origins_tags, index = False, header = False, float_format = '%.5f')
        #  2x - D0
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d0_origins_tags_out), 'scatters_2x_d0_origins_tags_out', output_file_2x_d0_origins_tags))
        scatters_2x_d0_origins_tags_out.to_csv(output_file_2x_d0_origins_tags, index = False, header = False, float_format = '%.5f')
        #  2x - D1
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_d1_origins_tags_out), 'scatters_2x_d1_origins_tags_out', output_file_2x_d1_origins_tags))
        scatters_2x_d1_origins_tags_out.to_csv(output_file_2x_d1_origins_tags, index = False, header = False, float_format = '%.5f')
    """

    #  saving combined 2x and 3x data (using file.open in order to append data to file)
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x_3x))
    ofile = open(output_file_2x_3x, "w")
    np.savetxt(ofile, scatters_2x_out, delimiter=',', fmt='%.5f')
    print ('   - saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_2x_3x))
    np.savetxt(ofile, scatters_3x_out, delimiter=',', fmt='%.5f')
    ofile.close()

    # count final number of double/triple scatters
    num_ss_fi = len(scatters_1x_out)
    num_ds_fi = len(scatters_2x_out)
    # num_ds_d0_fi = len(scatters_2x_d0_out);  num_ds_d1_fi = len(scatters_2x_d1_out);
    num_ds_ns_fi = len(scatters_2x_ns_out);  num_ds_nns_fi = len(scatters_2x_nns_out)
    num_ts_fi = len(scatters_3x_out)
    num_ts_ns_fi = len(scatters_3x_ns_out);  num_ts_nns_fi = len(scatters_3x_nns_out)

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  save run summary to file
    summary_file = output_file.replace(".txt", "").replace(".dat", "").replace(".csv", "") + "-summary.txt"
    f = open(summary_file, "w+")
    f.write ('--- Completed processing / time required {} s'.format(stop - start))
    f.write ('\n-------  SUMMARY  -------')
    f.write ('\n-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    #f.write ('\n--- Start time (s): {} | End time (s): {} | Run duration (s): {} ({} hrs) | Run activity (events/s): {}'.format(time_start, time_end, (time_end - time_start), (time_end - time_start)/3600, len(cc_dataframe) / (time_end - time_start)))
    f.write ('\n-- Number of 1x pixel events: {}, 2x pixel events: {} ({} double scatters), 3x pixel events: {} ({} triple scatters), 4x+ pixel events: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    f.write ('\n-- Total Number of Particles: {}, Secondaries from Neutrons: {} ({:.2f}%), Non-Neutron Secondaries: {} ({:.2f}%)'.format(len(allevents_data.index), num_ns, (float(num_ns)/len(allevents_data.index))*100.0, num_nns, (float(num_nns)/len(allevents_data.index))*100.0))
    f.write ('\n    - breakdown by scatter   / from neutrons     -> 1x: {} ({:.2f}%), 2x: {} ({} events / {:.2f}%), 3x: {} ({} events / {:.2f}%)'.format(num_ns_1x, num_ns_1x*100.0/num_ns, num_ns_2x, num_ns_2x/2, num_ns_2x*100.0/num_ns, num_ns_3x, num_ns_3x/3, num_ns_3x*100.0/num_ns))
    f.write ('\n    - breakdown by scatter   / from non-neutrons -> 1x: {} ({:.2f}%), 2x: {} ({} events / {:.2f}%), 3x: {} ({} events / {:.2f}%)'.format(num_nns_1x, num_nns_1x*100.0/num_nns, num_nns_2x, num_nns_2x/2, num_nns_2x*100.0/num_nns, num_nns_3x, num_nns_3x/3, num_nns_3x*100.0/num_nns))
    f.write ('\n-- Total Number of Particles: {}, gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(len(allevents_data.index), num_ga, num_ga*100.0/len(allevents_data.index), num_ss_ne, num_ss_ne*100.0/len(allevents_data.index), num_ss_pr, num_ss_pr*100.0/len(allevents_data.index), \
    num_ss_po, num_ss_po*100.0/len(allevents_data.index), num_ss_io, num_ss_io*100.0/len(allevents_data.index), num_ss_ot, num_ss_ot*100.0/len(allevents_data.index)))
    f.write ('\n    - breakdown by particle  / from neutrons     -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_ns_ga, num_ns_ga*100.0/num_ns, num_ns_ne, num_ns_ne*100.0/num_ns, num_ns_pr, num_ns_pr*100.0/num_ns, num_ns_po, num_ns_po*100.0/num_ns, num_ns_io, num_ns_io*100.0/num_ns, num_ns_ot, num_ns_ot*100.0/num_ns))
    f.write ('\n    - breakdown by particle  / from non-neutrons -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_nns_ga, num_nns_ga*100.0/num_nns, num_nns_ne, num_nns_ne*100.0/num_nns, num_nns_pr, num_nns_pr*100.0/num_nns, num_nns_po, num_nns_po*100.0/num_nns, num_nns_io, num_nns_io*100.0/num_nns, num_nns_ot, num_nns_ot*100.0/num_nns))
    f.write ('\n-- Multi-Stage interactions -> Total: {}, 2x pixel interactions: {} ({} events), 3x pixel interactions: {} ({} events)'.format(num_ms_total, num_ms_2px, num_ms_2px/2, num_ms_3px, num_ms_3px/3))
    #f.write ('\n--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    f.write ('\n-- Run Options -> Big Energy Sort: {} / Apply Energy Threshold: {} / Remove Unphysical Events: {} / Compton Line Filtering: {} / Shuffle Events: {} / Multistage Coincidence: {}'.format(APPLY_BIG_ENERGY_SORTING, APPLY_ENERGY_THRESHOLD, REMOVE_UNPHYSICAL_EVENTS, APPLY_COMPTON_LINE_FILTERING, SHUFFLE_SCATTER_EVENTS, APPLY_COINCIDENCE_GROUPING_MULTISTAGE))
    if APPLY_COMPTON_LINE_FILTERING: f.write ('\n--- Compton line filter settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE: f.write ('\n--- Multistage coincidence settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    f.write ('\n-- Single Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ss, num_ss_et, num_ss_fi))
    f.write ('\n     Single Scatters by particle  -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_ss_ga, num_ss_ga*100.0/num_1px, num_ss_ne, num_ss_ne*100.0/num_1px, num_ss_pr, num_ss_pr*100.0/num_1px, num_ss_po, num_ss_po*100.0/num_1px, num_ss_io, num_ss_io*100.0/num_1px, num_ss_ot, num_ss_ot*100.0/num_1px))
    f.write ('\n-- Double Scatters Events       -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds, num_ds_et, num_ds_pe, num_ds_cl, num_ds_ms, num_ds_fi, ((float(num_ds_fi)/num_ds) * 100.0)))
    f.write ('\n-- Double Scatters Events in D0 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d0, num_ds_d0_et, num_ds_d0_pe, num_ds_d0_cl, num_ds_d0_ms, num_ds_d0_fi, ((float(num_ds_d0_fi)/num_ds_d0) * 100.0)))
    f.write ('\n-- Double Scatters Events in D1 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({}%)'.format(num_ds_d1, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_d1_fi, ((float(num_ds_d1_fi)/num_ds_d1) * 100.0)))
    f.write ('\n-- Double Scatters Events [NS]  -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_ns, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_ns_fi, ((float(num_ds_ns_fi)/num_ds_ns) * 100.0)))
    f.write ('\n-- Double Scatters Events [NNS] -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_nns, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_nns_fi, ((float(num_ds_nns_fi)/num_ds_nns) * 100.0)))
    f.write ('\n-- Triple Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts, num_ts_et, num_ts_fi))
    f.write ('\n-- Triple Scatters Events [NS]  -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts_ns, num_ts_et, num_ts_ns_fi))
    f.write ('\n-- Triple Scatters Events [NNS] -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts_nns, num_ts_et, num_ts_nns_fi))
    f.write ('\n ed_all: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_all, ed_all/len(allevents_data.index), len(allevents_data.index)))
    f.write ('\n ed_ga:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga, ed_ga/num_ga, num_ga))
    f.write ('\n ed_ne:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ne, ed_ne/num_ss_ne, num_ss_ne))
    f.write ('\n ed_pr:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_pr, ed_pr/num_ss_pr, num_ss_pr))
    f.write ('\n ed_po:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_po, ed_po/num_ss_po, num_ss_po))
    f.write ('\n ed_io:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_io, ed_io/num_ss_io, num_ss_io))
    f.write ('\n ed_ns:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ns, ed_ns/num_ns, num_ns))
    f.write ('\n ed_nns: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_nns, ed_nns/num_nns, num_nns))
    f.write ('\n ed_ga_ns:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga_ns, ed_ga_ns/num_ns_ga, num_ns_ga))
    f.write ('\n ed_ga_nns: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga_nns, ed_ga_nns/num_nns_ga, num_nns_ga))
    f.write ('\n   for values below using timing_cutoff: {:.2e} ns'.format(timing_cutoff))
    f.write ('\n ts_all: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_all, ts_all/len(allevents_data.index), len(allevents_data.index)))
    f.write ('\n ts_ga:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga, ts_ga/num_ga, num_ga))
    f.write ('\n ts_ne:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ne, ts_ne/num_ss_ne, num_ss_ne))
    f.write ('\n ts_pr:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_pr, ts_pr/num_ss_pr, num_ss_pr))
    f.write ('\n ts_po:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_po, ts_po/num_ss_po, num_ss_po))
    f.write ('\n ts_io:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_io, ts_io/num_ss_io, num_ss_io))
    f.write ('\n ts_ns:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ns, ts_ns/num_ns, num_ns))
    f.write ('\n ts_nns: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_nns, ts_nns/num_nns, num_nns))
    f.write ('\n ts_ga_ns:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga_ns, ts_ga_ns/num_ns_ga, num_ns_ga))
    f.write ('\n ts_ga_nns: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga_nns, ts_ga_nns/num_nns_ga, num_nns_ga))
    f.write ('\n--------------------------')
    f.close()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    print ('-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    #print ('--- Start time (s): {} | End time (s): {} | Run duration (s): {} ({} hrs) | Run activity (events/s): {}'.format(time_start, time_end, (time_end - time_start), (time_end - time_start)/3600, len(cc_dataframe) / (time_end - time_start)))
    print ('-- Number of 1x pixel interactions: {}, 2x pixel interactions: {} ({} double scatter events), 3x pixel interactions: {} ({} triple scatter events), 4x+ pixel interactions: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    print ('-- Total Number of Particles: {}, Secondaries from Neutrons: {} ({:.2f}%), Non-Neutron Secondaries: {} ({:.2f}%)'.format(len(allevents_data.index), num_ns, (float(num_ns)/len(allevents_data.index))*100.0, num_nns, (float(num_nns)/len(allevents_data.index))*100.0))
    print ('    - breakdown by scatter   / from neutrons     -> 1x: {} ({:.2f}%), 2x: {} ({} events / {:.2f}%), 3x: {} ({} events / {:.2f}%)'.format(num_ns_1x, num_ns_1x*100.0/num_ns, num_ns_2x, num_ns_2x/2, num_ns_2x*100.0/num_ns, num_ns_3x, num_ns_3x/3, num_ns_3x*100.0/num_ns))
    print ('    - breakdown by scatter   / from non-neutrons -> 1x: {} ({:.2f}%), 2x: {} ({} events / {:.2f}%), 3x: {} ({} events / {:.2f}%)'.format(num_nns_1x, num_nns_1x*100.0/num_nns, num_nns_2x, num_nns_2x/2, num_nns_2x*100.0/num_nns, num_nns_3x, num_nns_3x/3, num_nns_3x*100.0/num_nns))
    print ('-- Total Number of Particles: {}, gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(len(allevents_data.index), num_ga, num_ga*100.0/len(allevents_data.index), num_ss_ne, num_ss_ne*100.0/len(allevents_data.index), num_ss_pr, num_ss_pr*100.0/len(allevents_data.index), \
    num_ss_po, num_ss_po*100.0/len(allevents_data.index), num_ss_io, num_ss_io*100.0/len(allevents_data.index), num_ss_ot, num_ss_ot*100.0/len(allevents_data.index)))
    print ('    - breakdown by particle  / from neutrons     -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_ns_ga, num_ns_ga*100.0/num_ns, num_ns_ne, num_ns_ne*100.0/num_ns, num_ns_pr, num_ns_pr*100.0/num_ns, num_ns_po, num_ns_po*100.0/num_ns, num_ns_io, num_ns_io*100.0/num_ns, num_ns_ot, num_ns_ot*100.0/num_ns))
    print ('    - breakdown by particle  / from non-neutrons -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_nns_ga, num_nns_ga*100.0/num_nns, num_nns_ne, num_nns_ne*100.0/num_nns, num_nns_pr, num_nns_pr*100.0/num_nns, num_nns_po, num_nns_po*100.0/num_nns, num_nns_io, num_nns_io*100.0/num_nns, num_nns_ot, num_nns_ot*100.0/num_nns))
    print ('-- Multi-Stage interactions -> Total: {}, 2x pixel interactions: {} ({} events), 3x pixel interactions: {} ({} events)'.format(num_ms_total, num_ms_2px, num_ms_2px/2, num_ms_3px, num_ms_3px/3))
    #print ('--- Detector transformations: D0 -> {} / D1 -> {}'.format(D0_TRANSFORMATION, D1_TRANSFORMATION))
    print ('-- Run Options -> Big Energy Sort: {} / Apply Energy Threshold: {} / Remove Unphysical Events: {} / Compton Line Filtering: {} / Shuffle Events: {} / Multistage Coincidence: {}'.format(APPLY_BIG_ENERGY_SORTING, APPLY_ENERGY_THRESHOLD, REMOVE_UNPHYSICAL_EVENTS, APPLY_COMPTON_LINE_FILTERING, SHUFFLE_SCATTER_EVENTS, APPLY_COINCIDENCE_GROUPING_MULTISTAGE))
    if APPLY_COMPTON_LINE_FILTERING: print ('--- Compton line filter settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    if APPLY_COINCIDENCE_GROUPING_MULTISTAGE: print ('--- Multistage coincidence settings -> Range [min, max]: {} / Energies (MeV): {}'.format(COMPTON_LINE_RANGE, COMPTON_LINE_ENERGIES))
    print ('-- Single Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ss, num_ss_et, num_ss_fi))
    print ('     Single Scatters by particle  -> gamma[22]: {} ({:.2f}%), neutron[2112]: {} ({:.2f}%), proton[2212]: {} ({:.2f}%), positron[-11]: {} ({:.2f}%), ion[>1e8]: {} ({:.2f}%), other: {} ({:.2f}%)'.format(num_ss_ga, num_ss_ga*100.0/num_1px, num_ss_ne, num_ss_ne*100.0/num_1px, num_ss_pr, num_ss_pr*100.0/num_1px, num_ss_po, num_ss_po*100.0/num_1px, num_ss_io, num_ss_io*100.0/num_1px, num_ss_ot, num_ss_ot*100.0/num_1px))
    print ('-- Double Scatters Events       -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds, num_ds_et, num_ds_pe, num_ds_cl, num_ds_ms, num_ds_fi, ((float(num_ds_fi)/num_ds) * 100.0)))
    print ('-- Double Scatters Events in D0 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_d0, num_ds_d0_et, num_ds_d0_pe, num_ds_d0_cl, num_ds_d0_ms, num_ds_d0_fi, ((float(num_ds_d0_fi)/num_ds_d0) * 100.0)))
    print ('-- Double Scatters Events in D1 -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_d1, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_d1_fi, ((float(num_ds_d1_fi)/num_ds_d1) * 100.0)))
    print ('-- Double Scatters Events [NS]  -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_ns, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_ns_fi, ((float(num_ds_ns_fi)/num_ds_ns) * 100.0)))
    print ('-- Double Scatters Events [NNS] -> Initial: {} | Above Eng Thres: {} | Physically valid: {} | CL filtered: {} | MS coincidence: {} | Final: {} ({:.2f}%)'.format(num_ds_nns, num_ds_d1_et, num_ds_d1_pe, num_ds_d1_cl, num_ds_d1_ms, num_ds_nns_fi, ((float(num_ds_nns_fi)/num_ds_nns) * 100.0)))
    print ('-- Triple Scatters Events       -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts, num_ts_et, num_ts_fi))
    print ('-- Triple Scatters Events [NS]  -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts_ns, num_ts_et, num_ts_ns_fi))
    print ('-- Triple Scatters Events [NNS] -> Initial: {} | Above Eng Thres: {} | Final: {}'.format(num_ts_nns, num_ts_et, num_ts_nns_fi))
    print (' ed_all: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_all, ed_all/len(allevents_data.index), len(allevents_data.index)))
    print (' ed_ga:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga, ed_ga/num_ga, num_ga))
    print (' ed_ne:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ne, ed_ne/num_ss_ne, num_ss_ne))
    print (' ed_pr:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_pr, ed_pr/num_ss_pr, num_ss_pr))
    print (' ed_po:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_po, ed_po/num_ss_po, num_ss_po))
    print (' ed_io:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_io, ed_io/num_ss_io, num_ss_io))
    print (' ed_ns:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ns, ed_ns/num_ns, num_ns))
    print (' ed_nns: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_nns, ed_nns/num_nns, num_nns))
    print (' ed_ga_ns:  {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga_ns, ed_ga_ns/num_ns_ga, num_ns_ga))
    print (' ed_ga_nns: {:.2f} MeV, avg: {:.3f} MeV, num: {}'.format(ed_ga_nns, ed_ga_nns/num_nns_ga, num_nns_ga))
    print ('   for values below using timing_cutoff: {:.2e} ns'.format(timing_cutoff))
    print (' ts_all: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_all, ts_all/len(allevents_data.index), len(allevents_data.index)))
    print (' ts_ga:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga, ts_ga/num_ga, num_ga))
    print (' ts_ne:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ne, ts_ne/num_ss_ne, num_ss_ne))
    print (' ts_pr:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_pr, ts_pr/num_ss_pr, num_ss_pr))
    print (' ts_po:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_po, ts_po/num_ss_po, num_ss_po))
    print (' ts_io:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_io, ts_io/num_ss_io, num_ss_io))
    print (' ts_ns:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ns, ts_ns/num_ns, num_ns))
    print (' ts_nns: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_nns, ts_nns/num_nns, num_nns))
    print (' ts_ga_ns:  {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga_ns, ts_ga_ns/num_ns_ga, num_ns_ga))
    print (' ts_ga_nns: {:.2e} ns, avg: {:.2e} ns, num: {}'.format(ts_ga_nns, ts_ga_nns/num_nns_ga, num_nns_ga))
    print ('--------------------------')


# Read in ROOT particle data and compare it to scatterData
def compare_particle_data(data_dir, root_tree_name):
    '''
    Takes in an AllEventsCombined.txt file, reads the information into a pandas
    array and then filters data using the flagged parameters

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    @returns:

    '''
    #  measuring time required to run code - start time
    start = timeit.default_timer()

    # Read in particleData ROOT Tree into pandas array
    allevents_data_particle, root_file_list = read_root_data(data_dir, 'particleData', 'particleData')       # read in the simulated particle data
    tot_raw_particle_events = len(allevents_data_particle.index)          # get the total number of events before coincidence processing

    if APPLY_ENERGY_THRESHOLD:
        count_removed = 0
        df = allevents_data_particle
        print ('   - Applying energy threshold ({} MeV) for {} interactions in {} tree'.format(MINIMUM_ENERGY_THRESHOLD, len(df.index), 'particleData'))
        chosen = df.loc[ df.energy > MINIMUM_ENERGY_THRESHOLD ]
        #  print counts to screen
        count_removed = len(df.index) - len(chosen)
        print ('    - results of energy threshold -> number of interactions returned: {} | removed: {}'.format(len(chosen), count_removed))
        allevents_data_particle = chosen

    # Read in scatterData ROOT Tree into pandas array
    allevents_data_scatter, root_file_list = read_root_data(data_dir, 'scatterData', 'scatterData')       # read in the simulated scatter data
    tot_raw_scatter_events = len(allevents_data_scatter.index)          # get the total number of events before coincidence processing

    if APPLY_ENERGY_THRESHOLD:
        count_removed = 0
        df = allevents_data_scatter
        print ('   - Applying energy threshold ({} MeV) for {} interactions in {} tree'.format(MINIMUM_ENERGY_THRESHOLD, len(df.index), 'scatterData'))
        chosen = df.loc[ df.energy > MINIMUM_ENERGY_THRESHOLD ]
        #  print counts to screen
        count_removed = len(df.index) - len(chosen)
        print ('    - results of energy threshold -> number of interactions returned: {} | removed: {}'.format(len(chosen), count_removed))
        allevents_data_scatter = chosen


    #  checking if plots directory exists, if not, create
    output_file_name = root_tree_name
    plots_dir = data_dir + add_tags_to_filename(output_file_name, '-plots_raw/')
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
        print ('Creating directory: {}'.format(plots_dir))

    ### PLOTTING BASIC DETECTOR DATA ###
    print ('\n--- Making Basic Detector Plots (using raw data) ---')

    #alternate_max_energy = 5.0
    alternate_max_energy = np.amax(COMPTON_LINE_ENERGIES) + 0.1   # use max Compton Line energy and add 100 keV

    if PRODUCE_COMPARE_ENERGY_PLOTS:

        print ('  Creating 1D Comparison Energy Plots - using alternate maximum energy of {} MeV . . .'.format(alternate_max_energy))
        plt.cla(); plt.clf()

        # split data based on neutron-secondaries
        df = allevents_data_particle

        all_part_eng = df.energy.to_numpy()
        all_part_eng_gamma = df[df.particle == 22].energy.to_numpy()
        all_part_eng_proton = df[df.particle == 2212].energy.to_numpy()
        all_part_eng_positron = df[df.particle == -11].energy.to_numpy()
        all_part_eng_ion = df[df.particle > 1e8].energy.to_numpy()
        all_part_eng_neutron = df[df.particle == 2112].energy.to_numpy()
        all_part_eng_neu_sec = df[df.nTag == True].energy.to_numpy()
        all_part_eng_non_neu_sec = df[df.nTag == False].energy.to_numpy()
        gam_part_eng_neu_sec = df[(df.nTag == True) & (df.particle == 22)].energy.to_numpy()
        gam_part_eng_non_neu_sec = df[(df.nTag == False) & (df.particle == 22)].energy.to_numpy()
        timing_cutoff = 1e11 # in ns, 1e11 = 100 sec
        all_part_eng_tim_cut = df[df.time < timing_cutoff].energy.to_numpy()

        nb_bins = 100;  rng_min = 0.0;  rng_max = alternate_max_energy
        x_label = 'Energy Deposited (MeV)'
        font_size = 9 #10
        alpha_level = 0.7

        plt.hist(allevents_data_particle.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'scatterData', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited / total[PD]: %s, [SD]: %s'%(len(allevents_data_particle.energy), len(allevents_data_scatter.energy)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Energy-compare_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(allevents_data_particle.energy, bins = nb_bins, range = [rng_min, rng_max], alpha = alpha_level, log = True, label = 'particleDataf', histtype = 'step')
        plt.hist(allevents_data_scatter.energy, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_eng_tim_cut, bins = nb_bins, range = [rng_min, rng_max], alpha = alpha_level, log = True, label = 'PD/< 1e11 ns', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited / total[PD]: %s, [SD]: %s, [PD-1e11]: %s'%(len(allevents_data_particle.energy), len(allevents_data_scatter.energy), len(all_part_eng_tim_cut)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Energy-compare_timing_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        """
        plt.hist(allevents_data_particle.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited / total[PD]: %s, [SD]: %s'%(len(allevents_data_particle.energy), len(allevents_data_scatter.energy)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Energy-compare_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        """
        plt.hist(allevents_data_particle.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_eng_non_neu_sec, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(all_part_eng_neu_sec, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(allevents_data_particle.energy), len(allevents_data_scatter.energy), len(all_part_eng_neu_sec), len(all_part_eng_non_neu_sec)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Energy-compare_neutron_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_eng_gamma, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'gamma', histtype = 'step')
        plt.hist(all_part_eng_proton, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'proton', histtype = 'step')
        plt.hist(all_part_eng_positron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'positron', histtype = 'step')
        plt.hist(all_part_eng_ion, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'ion', histtype = 'step')
        plt.hist(all_part_eng_neutron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'neutron', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(len(all_part_eng), len(all_part_eng_gamma), len(all_part_eng_proton), len(all_part_eng_positron), len(all_part_eng_ion), len(all_part_eng_neutron)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Energy-PD_particle_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()


        ### AAPM 2020 Energy Plot
        plt.hist(all_part_eng_gamma, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'gamma', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_eng_proton, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'proton', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_eng_positron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'positron', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_eng_ion, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'ion', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_eng_neutron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'neutron', histtype = 'step', linewidth = 1.5)
        plt.xlabel(x_label, fontsize = 14);
        plt.ylabel('Number of Particles', fontsize = 14);
        #plt.title('Energy deposited / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(len(all_part_eng), len(all_part_eng_gamma), len(all_part_eng_proton), len(all_part_eng_positron), len(all_part_eng_ion), len(all_part_eng_neutron)), fontsize = font_size)
        plt.legend(fontsize = 12)
        utils.save_to_file('aapm2020-raw_all_Energy-PD_particle_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()


        df = allevents_data_particle
        part_gamma = df.loc[ df.particle == 22 ]
        plt.hist(part_gamma.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.energy, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.hist(gam_part_eng_non_neu_sec, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(gam_part_eng_neu_sec, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited [gammas] / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(part_gamma.energy), len(allevents_data_scatter.energy), len(gam_part_eng_neu_sec), len(gam_part_eng_non_neu_sec)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_gamma_Energy-compare_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        #  2x scatters
        part_gamma_2x = df.loc[ (df.particle == 22) & (df.scatters == 2) ]
        scat_gamma_2x = allevents_data_scatter.loc[ allevents_data_scatter.scatters == 2 ]
        part_gamma_2x_non_neu_sec = df[ (df.nTag == False) & (df.particle == 22) & (df.scatters == 2) ]
        part_gamma_2x_neu_sec = df[ (df.nTag == True) & (df.particle == 22) & (df.scatters == 2) ]
        plt.hist(part_gamma_2x.energy, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'particleData', histtype = 'step')
        plt.hist(scat_gamma_2x.energy, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.hist(part_gamma_2x_non_neu_sec.energy, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(part_gamma_2x_neu_sec.energy, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited [2x gammas, interaction] / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(part_gamma_2x.energy), len(scat_gamma_2x.energy), len(part_gamma_2x_non_neu_sec.energy), len(part_gamma_2x_neu_sec.energy)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_gamma_Energy-compare_2x_interaction_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()


        """
        #  alternate plots for raw data of 2D xy profiles
        d0_xPos = cc_dataframe[cc_dataframe.detector == 0].x.to_numpy()
        d0_yPos = cc_dataframe[cc_dataframe.detector == 0].y.to_numpy()
        d1_xPos = cc_dataframe[cc_dataframe.detector == 1].x.to_numpy()
        d1_yPos = cc_dataframe[cc_dataframe.detector == 1].y.to_numpy()
        plt.cla(); plt.clf()
        plt.plot(d0_yPos, d0_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / D0 / Raw Data, total: %s'%(len(d0_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_D0_y_x_alternate", plots_dir)
        plt.cla(); plt.clf()
        """

        # plotting energy of summed 2x scatters
        part_gamma_2x_eng_interaction = df.loc[ (df.particle == 22) & (df.scatters == 2) ].energy.to_numpy()
        part_gamma_2x_eng_summed = part_gamma_2x_eng_interaction[0::2] + part_gamma_2x_eng_interaction[1::2]
        scat_gamma_2x_eng_interaction = allevents_data_scatter.loc[ allevents_data_scatter.scatters == 2 ].energy.to_numpy()
        part_gamma_2x_eng_summed = scat_gamma_2x_eng_interaction[0::2] + scat_gamma_2x_eng_interaction[1::2]
        part_gamma_2x_eng_interaction_non_neu_sec = df[ (df.nTag == False) & (df.particle == 22) & (df.scatters == 2) ].energy.to_numpy()
        part_gamma_2x_eng_summed_non_neu_sec = part_gamma_2x_eng_interaction_non_neu_sec[0::2] + part_gamma_2x_eng_interaction_non_neu_sec[1::2]
        part_gamma_2x_eng_interaction_neu_sec = df[ (df.nTag == True) & (df.particle == 22) & (df.scatters == 2) ].energy.to_numpy()
        part_gamma_2x_eng_summed_neu_sec = part_gamma_2x_eng_interaction_neu_sec[0::2] + part_gamma_2x_eng_interaction_neu_sec[1::2]
        plt.hist(part_gamma_2x_eng_summed, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'particleData', histtype = 'step')
        plt.hist(part_gamma_2x_eng_summed, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'scatterData', histtype = 'step')
        plt.hist(part_gamma_2x_eng_summed_non_neu_sec, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(part_gamma_2x_eng_summed_neu_sec, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, log = True, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Energy deposited [2x gammas, summed] / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(part_gamma_2x.energy), len(scat_gamma_2x.energy), len(part_gamma_2x_non_neu_sec.energy), len(part_gamma_2x_neu_sec.energy)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_gamma_Energy-compare_2x_summed_log_%sMeV'%(rng_max), plots_dir)
        plt.cla(); plt.clf()



    if PRODUCE_COMPARE_2D_POSITION_PLOTS:

        print ('  Creating 2D Position Plots [scatterData] . . .')

        #  alternate plots for raw data of 2D xy profiles
        #    - different colors for different particles / change size based on energy

        df = allevents_data_scatter
        all_scat_xPos = df.x.to_numpy();  all_scat_yPos = df.y.to_numpy();  all_scat_zPos = df.z.to_numpy()
        # XY
        plt.plot(all_scat_yPos, all_scat_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / PD / Raw Data, total: %s'%(len(all_scat_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_PD_x_y_alternate", plots_dir)
        plt.cla(); plt.clf()
        # XZ
        plt.plot(all_scat_zPos, all_scat_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('YZ Position of all scatters / PD / Raw Data, total: %s'%(len(all_scat_zPos)))
        plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')
        utils.save_to_file("raw_all_PD_x_z_alternate", plots_dir)
        plt.cla(); plt.clf()

        # D0 / SD
        df = allevents_data_scatter
        d0_scat_xPos = df[df.detector == 0].x.to_numpy();  d0_scat_yPos = df[df.detector == 0].y.to_numpy();  d0_scat_zPos = df[df.detector == 0].z.to_numpy()
        # XY
        plt.plot(d0_scat_yPos, d0_scat_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position / D0 scatters / SD / Raw Data, total: %s'%(len(d0_scat_yPos)), fontsize = 'small')
        plt.xlabel('Y-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        utils.save_to_file("raw_all_SD_D0_x_y_alternate", plots_dir)
        plt.cla(); plt.clf()
        # XZ
        plt.plot(d0_scat_zPos, d0_scat_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XZ Position / D0 scatters / SD / Raw Data, total: %s'%(len(d0_scat_zPos)), fontsize = 'small')
        plt.xlabel('Z-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        utils.save_to_file("raw_all_SD_D0_x_z_alternate", plots_dir)
        plt.cla(); plt.clf()


        print ('  Creating 2D Position Plots [particleData] . . .')

        #  alternate plots for raw data of 2D xy profiles
        #    - different colors for different particles / change size based on energy

        df = allevents_data_particle
        all_part_xPos = df.x.to_numpy();  all_part_yPos = df.y.to_numpy();  all_part_zPos = df.z.to_numpy()
        # XY
        plt.plot(all_part_yPos, all_part_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position of all scatters / PD / Raw Data, total: %s'%(len(all_part_yPos)))
        plt.xlabel('Y-Position (mm)'); plt.ylabel('X-Position (mm)')
        utils.save_to_file("raw_all_PD_x_y_alternate", plots_dir)
        plt.cla(); plt.clf()
        # XZ
        plt.plot(all_part_zPos, all_part_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('YZ Position of all scatters / PD / Raw Data, total: %s'%(len(all_part_zPos)))
        plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')
        utils.save_to_file("raw_all_PD_x_z_alternate", plots_dir)
        plt.cla(); plt.clf()

        # D0 / PD
        df = allevents_data_particle
        d0_part_xPos = df[df.detector == 0].x.to_numpy();  d0_part_yPos = df[df.detector == 0].y.to_numpy();  d0_part_zPos = df[df.detector == 0].z.to_numpy()
        # XY
        plt.plot(d0_part_yPos, d0_part_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XY Position / D0 scatters / PD / Raw Data, total: %s'%(len(d0_part_yPos)), fontsize = 'small')
        plt.xlabel('Y-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        utils.save_to_file("raw_all_PD_D0_x_y_alternate", plots_dir)
        plt.cla(); plt.clf()
        # XZ
        plt.plot(d0_part_zPos, d0_part_xPos, color='green', marker='o', linestyle='None', markersize=0.5)
        plt.title('XZ Position / D0 scatters / PD / Raw Data, total: %s'%(len(d0_part_zPos)), fontsize = 'small')
        plt.xlabel('Z-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        utils.save_to_file("raw_all_PD_D0_x_z_alternate", plots_dir)
        plt.cla(); plt.clf()


    # Takes a long time to run these
    if PRODUCE_COMPARE_2D_POSITION_BREAKDOWN_PLOTS:

        print ('  Creating 2D Position Plots [particleData] - by particle & energy deposition . . .')

        """
        # D0 (breakdown by particle & energy deposition - markersize from 0.1 to 5.0)
        ms_min = 0.4;  ms_max = 7.0
        df = allevents_data_scatter
        minEngSD = df.energy.min();  maxEngSD = df.energy.max()
        print('  minimum energy in scatterData Tree: {} MeV / maximum energy in scatterData Tree: {} MeV'.format(minEngSD, maxEngSD))
        df = allevents_data_particle
        minEngPD = df.energy.min();  maxEngPD = df.energy.max()
        slopePD = (ms_max - ms_min) / (maxEngPD - minEngPD)
        print('  minimum energy in particleData Tree: {} MeV / maximum energy in particleData Tree: {} MeV / markersize slope for particleData Tree: {} pixels/MeV'.format(minEngPD, maxEngPD, slopePD))
        d0_part_xPos = df[df.detector == 0].x.to_numpy();  d0_part_yPos = df[df.detector == 0].y.to_numpy();  d0_part_zPos = df[df.detector == 0].z.to_numpy()
        d0_part = df[df.detector == 0]

        #  XY - looping through pandas - breakdown by energy and particle
        for i in d0_part.index:
            m_energy = d0_part['energy'][i];  xPos = d0_part['x'][i];  yPos = d0_part['y'][i];  pt = d0_part['particle'][i]
            m_size = slopePD * m_energy + ms_min
            if pt == 22: m_color = 'green'
            elif pt == 2112: m_color = 'black'
            elif pt == 2212: m_color = 'red'
            elif pt == -11: m_color = 'blue'
            elif pt > 1e8: m_color = 'purple'
            else: m_color = 'orange'
            plt.plot(yPos, xPos, color=m_color, marker='o', linestyle='None', markersize=m_size)

        plt.title('XY Position / D0 scatters / PD / Raw Data, total: %s'%(len(d0_part)), fontsize = 'small')
        plt.xlabel('Y-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        label_gam = mpatches.Patch(color='green', label='gam [%s]'%(d0_gam_cnt))
        label_neu = mpatches.Patch(color='black', label='neu [%s]'%(d0_neu_cnt))
        label_pro = mpatches.Patch(color='red', label='pro [%s]'%(d0_pro_cnt))
        label_pos = mpatches.Patch(color='blue', label='pos [%s]'%(d0_pos_cnt))
        label_ion = mpatches.Patch(color='purple', label='ion [%s]'%(d0_ion_cnt))
        label_oth = mpatches.Patch(color='orange', label='oth [%s]'%(d0_oth_cnt))
        plt.legend(handles=[label_gam, label_neu, label_pro, label_pos, label_ion, label_oth], fontsize = 'xx-small', loc = 'upper right')
        utils.save_to_file("raw_all_PD_D0_x_y_by_particle_and_energy", plots_dir)
        plt.cla(); plt.clf()
        """

        ### AAPM 2020
        # breakdown by particle & energy deposition - markersize from 0.4 to 7.0
        ms_min = 0.4;  ms_max = 7.0
        df = allevents_data_scatter
        minEngSD = df.energy.min();  maxEngSD = df.energy.max()
        print('  minimum energy in scatterData Tree: {} MeV / maximum energy in scatterData Tree: {} MeV'.format(minEngSD, maxEngSD))
        df = allevents_data_particle
        minEngPD = df.energy.min();  maxEngPD = df.energy.max()
        slopePD = (ms_max - ms_min) / (maxEngPD - minEngPD)
        print('  minimum energy in particleData Tree: {} MeV / maximum energy in particleData Tree: {} MeV / markersize slope for particleData Tree: {} pixels/MeV'.format(minEngPD, maxEngPD, slopePD))
        d0_part_xPos = df[df.detector == 0].x.to_numpy();  d0_part_yPos = df[df.detector == 0].y.to_numpy();  d0_part_zPos = df[df.detector == 0].z.to_numpy()
        #all_part_xPos = df.x.to_numpy();  all_part_yPos = df.y.to_numpy();  all_part_zPos = df.z.to_numpy()
        d0_part = df[df.detector == 0]
        #all_part = df

        #  XY - looping through pandas - breakdown by energy and particle
        #for i in all_part.index:
        for i in d0_part.index:
            m_energy = d0_part['energy'][i];  xPos = d0_part['x'][i];  yPos = d0_part['y'][i];  pt = d0_part['particle'][i]
            #m_energy = all_part['energy'][i];  xPos = all_part['x'][i];  yPos = all_part['y'][i];  pt = all_part['particle'][i]
            m_size = slopePD * m_energy + ms_min
            if pt == 22: m_color = 'green'
            elif pt == 2112: m_color = 'black'
            elif pt == 2212: m_color = 'red'
            elif pt == -11: m_color = 'blue'
            elif pt > 1e8: m_color = 'purple'
            else: m_color = 'orange'
            plt.plot(yPos, xPos, color = m_color, marker = 'o', linestyle = 'None', markersize = m_size)

        #plt.title('XY Position / D0 scatters / PD / Raw Data, total: %s'%(len(d0_part)), fontsize = 'small')
        plt.xlabel('Y-Position (mm)', fontsize = 14);  plt.ylabel('X-Position (mm)', fontsize = 14)
        label_gam = mpatches.Patch(color = 'green', label = 'gam')
        label_neu = mpatches.Patch(color = 'black', label = 'neu')
        label_pro = mpatches.Patch(color = 'red', label = 'pro')
        label_pos = mpatches.Patch(color = 'blue', label = 'pos')
        label_ion = mpatches.Patch(color = 'purple', label = 'ion')
        label_oth = mpatches.Patch(color = 'orange', label = 'oth')
        plt.legend(handles=[label_gam, label_neu, label_pro, label_pos, label_ion, label_oth], fontsize = 12, loc = 'upper right')
        utils.save_to_file("aapm2020-raw_all_PD_D0_x_y_by_particle_and_energy", plots_dir)
        #utils.save_to_file("aapm2020-raw_all_PD_x_y_by_particle_and_energy", plots_dir)
        plt.cla(); plt.clf()



        """
        # XZ - looping through pandas - breakdown by energy
        for i in d0_part.index:
            m_energy = d0_part['energy'][i];  xPos = d0_part['x'][i];  zPos = d0_part['z'][i]
            m_size = slopePD * m_energy + ms_min
            #print (i, ms_energy, xPos, zPos, ms_size)
            plt.plot(zPos, xPos, color='green', marker='o', linestyle='None', markersize=m_size)

        plt.title('XZ Position of all D0 scatters / PD / Raw Data, total: %s'%(len(d0_part_zPos_gam)))
        plt.xlabel('X-Position (mm)'); plt.ylabel('Z-Position (mm)')
        utils.save_to_file("raw_all_PD_D0_x_z_by_energy", plots_dir)
        plt.cla(); plt.clf()
        """

        """
        # XZ - looping through pandas - breakdown by energy and particle
        for i in d0_part.index:
            m_energy = d0_part['energy'][i];  xPos = d0_part['x'][i];  zPos = d0_part['z'][i];  pt = d0_part['particle'][i]
            m_size = slopePD * m_energy + ms_min
            if pt == 22: m_color = 'green'
            elif pt == 2112: m_color = 'black'
            elif pt == 2212: m_color = 'red'
            elif pt == -11: m_color = 'blue'
            elif pt > 1e8: m_color = 'purple'
            else: m_color = 'orange'
            plt.plot(zPos, xPos, color=m_color, marker='o', linestyle='None', markersize=m_size)

        plt.title('XZ Position / D0 scatters / PD / Raw Data, total: %s'%(len(d0_part)), fontsize = 'small')
        plt.xlabel('Z-Position (mm)', fontsize = 'small');  plt.ylabel('X-Position (mm)', fontsize = 'small')
        label_gam = mpatches.Patch(color='green', label='gam [%s]'%(d0_gam_cnt))
        label_neu = mpatches.Patch(color='black', label='neu [%s]'%(d0_neu_cnt))
        label_pro = mpatches.Patch(color='red', label='pro [%s]'%(d0_pro_cnt))
        label_pos = mpatches.Patch(color='blue', label='pos [%s]'%(d0_pos_cnt))
        label_ion = mpatches.Patch(color='purple', label='ion [%s]'%(d0_ion_cnt))
        label_oth = mpatches.Patch(color='orange', label='oth [%s]'%(d0_oth_cnt))
        plt.legend(handles=[label_gam, label_neu, label_pro, label_pos, label_ion, label_oth], fontsize = 'xx-small', loc = 'upper right')
        utils.save_to_file("raw_all_PD_D0_x_z_by_particle_and_energy", plots_dir)
        plt.cla(); plt.clf()
        """

    if PRODUCE_COMPARE_DEPTH_PLOTS:
        print ('  Creating 1D Depth of Interaction Plots [particleData] . . .')

        df = allevents_data_particle
        all_part_yPos = df.y.to_numpy()
        all_part_yPos_gamma = df[df.particle == 22].y.to_numpy()
        all_part_yPos_neutron = df[df.particle == 2112].y.to_numpy()
        all_part_yPos_proton = df[df.particle == 2212].y.to_numpy()
        all_part_yPos_positron = df[df.particle == -11].y.to_numpy()
        all_part_yPos_ion = df[df.particle > 1e8].y.to_numpy()
        minYPosPD = df.y.min();  maxYPosPD = df.y.max()
        print('  minimum y-position in particleData Tree: {} mm / maximum y-position in particleData Tree: {} mm'.format(minYPosPD, maxYPosPD))
        all_part_yPos_neu_sec = df[df.nTag == True].y.to_numpy()
        all_part_yPos_non_neu_sec = df[df.nTag == False].y.to_numpy()
        timing_cutoff = 1e11 # in ns, 1e11 = 100 sec
        all_part_yPos_tim_cut = df[df.time < timing_cutoff].y.to_numpy()

        nb_bins = 100;  rng_min = minYPosPD;  rng_max = maxYPosPD
        x_label = 'y-pos (mm)'
        font_size = 9 # 10
        alpha_level = 0.7
        # depth for all interactions (with timing cutoff)
        plt.hist(all_part_yPos, bins = nb_bins, alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.y, bins = nb_bins, alpha = alpha_level, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_yPos_tim_cut, bins = nb_bins, alpha = alpha_level, label = 'PD[<1e11 ns]', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Interaction Depth / total[PD]: %s, [SD]: %s, [PD<1e11]: %s'%(len(df.index), len(allevents_data_scatter.y), len(all_part_yPos_tim_cut)), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-compare_timing", plots_dir)
        plt.cla(); plt.clf()
        # depth for some particles
        plt.hist(all_part_yPos_gamma, bins = nb_bins, alpha = alpha_level, label = 'gamma', histtype = 'step')
        plt.hist(all_part_yPos_proton, bins = nb_bins, alpha = alpha_level, label = 'proton', histtype = 'step')
        plt.hist(all_part_yPos_positron, bins = nb_bins, alpha = alpha_level, label = 'positron', histtype = 'step')
        plt.hist(all_part_yPos_ion, bins = nb_bins, alpha = alpha_level, label = 'ion', histtype = 'step')
        plt.hist(all_part_yPos_neutron, bins = nb_bins, alpha = alpha_level, label = 'neutron', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Interaction Depth / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(len(df.index), len(df[(df.particle == 22)]), len(df[(df.particle == 2212)]), len(df[(df.particle == -11)]), len(df[(df.particle > 1e8)]), len(df[(df.particle == 2112)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-PD_particle", plots_dir)
        plt.cla(); plt.clf()
        # neutron-secondaries
        plt.hist(allevents_data_particle.y, bins = nb_bins, alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.y, bins = nb_bins, alpha = alpha_level, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_yPos_non_neu_sec, bins = nb_bins, alpha = alpha_level, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(all_part_yPos_neu_sec, bins = nb_bins, alpha = alpha_level, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Interaction Depth / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(allevents_data_particle.y), len(allevents_data_scatter.y), len(all_part_yPos_neu_sec), len(all_part_yPos_non_neu_sec)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Depth-compare_neutron-secondary', plots_dir)
        plt.cla(); plt.clf()

        all_part_eDep = df.energy.to_numpy()
        all_part_eDep_gamma = df[df.particle == 22].energy.to_numpy()
        all_part_eDep_neutron = df[df.particle == 2112].energy.to_numpy()
        all_part_eDep_proton = df[df.particle == 2212].energy.to_numpy()
        all_part_eDep_positron = df[df.particle == -11].energy.to_numpy()
        all_part_eDep_ion = df[df.particle > 1e8].energy.to_numpy()
        all_part_eng_neu_sec = df[df.nTag == True].energy.to_numpy()
        all_part_eng_non_neu_sec = df[df.nTag == False].energy.to_numpy()
        all_part_eng_tim_cut = df[df.time < timing_cutoff].energy.to_numpy()

        # depth for all interactions (weighted by energy deposited)
        plt.hist(all_part_yPos, bins = nb_bins, weights = all_part_eDep, alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.y, bins = nb_bins, weights = allevents_data_scatter.energy, alpha = alpha_level, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_yPos_tim_cut, bins = nb_bins, weights = all_part_eng_tim_cut, alpha = alpha_level, label = 'PD[<1e11 ns]', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Interaction Depth (eng-wgt) / total[PD]: %s, [SD]: %s, [PD<1e11]: %s'%(len(df.index), len(allevents_data_scatter.y), len(all_part_yPos_tim_cut)), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-compare_eng-wgt_timing", plots_dir)
        plt.cla(); plt.clf()
        # depth for some particles (weighted by energy deposited)
        plt.hist(all_part_yPos_gamma, bins = nb_bins, weights = all_part_eDep_gamma, alpha = alpha_level, label = 'gamma', histtype = 'step')
        plt.hist(all_part_yPos_proton, bins = nb_bins, weights = all_part_eDep_proton, alpha = alpha_level, label = 'proton', histtype = 'step')
        plt.hist(all_part_yPos_positron, bins = nb_bins, weights = all_part_eDep_positron, alpha = alpha_level, label = 'positron', histtype = 'step')
        plt.hist(all_part_yPos_ion, bins = nb_bins, weights = all_part_eDep_ion, alpha = alpha_level, label = 'ion', histtype = 'step')
        plt.hist(all_part_yPos_neutron, bins = nb_bins, weights = all_part_eDep_neutron, alpha = alpha_level, label = 'neutron', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Energy', fontsize = font_size);
        plt.title('Interaction Depth (eng-wgt) / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(len(df.index), len(df[(df.particle == 22)]), len(df[(df.particle == 2212)]), len(df[(df.particle == -11)]), len(df[(df.particle > 1e8)]), len(df[(df.particle == 2112)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-PD_eng-wgt_particle", plots_dir)
        plt.cla(); plt.clf()

        plt.hist(allevents_data_particle.y, bins = nb_bins, weights = all_part_eDep, alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(allevents_data_scatter.y, bins = nb_bins, weights = allevents_data_scatter.energy, alpha = alpha_level, label = 'scatterData', histtype = 'step')
        plt.hist(all_part_yPos_non_neu_sec, bins = nb_bins, weights = all_part_eng_non_neu_sec, alpha = alpha_level, label = 'PD/non-neu-sec', histtype = 'step')
        plt.hist(all_part_yPos_neu_sec, bins = nb_bins, weights = all_part_eng_neu_sec, alpha = alpha_level, label = 'PD/neu-second', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Interaction Depth (eng-wgt) / total[PD]: %s, [SD]: %s, [PD-ns]: %s, [PD-nns]: %s'%(len(allevents_data_particle.y), len(allevents_data_scatter.y), len(all_part_yPos_neu_sec), len(all_part_yPos_non_neu_sec)), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Depth-compare_eng-wgt_neutron-secondary', plots_dir)
        plt.cla(); plt.clf()

        all_part_yPos_gamma_01 = df[(df.particle == 22) & (df.energy > 0.0) & (df.energy < 1.0)].y.to_numpy()
        all_part_yPos_gamma_12 = df[(df.particle == 22) & (df.energy > 1.0) & (df.energy < 2.0)].y.to_numpy()
        all_part_yPos_gamma_23 = df[(df.particle == 22) & (df.energy > 2.0) & (df.energy < 3.0)].y.to_numpy()
        all_part_yPos_gamma_34 = df[(df.particle == 22) & (df.energy > 3.0) & (df.energy < 4.0)].y.to_numpy()
        all_part_yPos_gamma_45 = df[(df.particle == 22) & (df.energy > 4.0) & (df.energy < 5.0)].y.to_numpy()
        all_part_yPos_gamma_56 = df[(df.particle == 22) & (df.energy > 5.0) & (df.energy < 6.0)].y.to_numpy()
        all_part_yPos_gamma_67 = df[(df.particle == 22) & (df.energy > 6.0) & (df.energy < 7.0)].y.to_numpy()
        all_part_yPos_gamma_g7 = df[(df.particle == 22) & (df.energy > 7.0)].y.to_numpy()

        gam_01 = len(all_part_yPos_gamma_01);  gam_12 = len(all_part_yPos_gamma_12);  gam_23 = len(all_part_yPos_gamma_23);  gam_34 = len(all_part_yPos_gamma_34)
        gam_45 = len(all_part_yPos_gamma_45);  gam_56 = len(all_part_yPos_gamma_56);  gam_67 = len(all_part_yPos_gamma_67);  gam_g7 = len(all_part_yPos_gamma_g7)
        print('    - Breakdown of gammas by energy range - total: {}, 0 - 1 Mev: {}, 1 - 2 Mev: {}, 2 - 3 Mev: {}, 3 - 4 Mev: {}, 4 - 5 Mev: {}, 5 - 6 Mev: {}, 6 - 7 Mev: {}, > 7 Mev: {}'.format(len(all_part_yPos_gamma), gam_01, gam_12, gam_23, gam_34, gam_45, gam_56, gam_67, gam_g7))

        # depth for gammas (broken down by energy ranges)
        plt.hist(all_part_yPos_gamma_12, bins = nb_bins, alpha = alpha_level, label = '1 - 2 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_23, bins = nb_bins, alpha = alpha_level, label = '2 - 3 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_34, bins = nb_bins, alpha = alpha_level, label = '3 - 4 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_45, bins = nb_bins, alpha = alpha_level, label = '4 - 5 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_56, bins = nb_bins, alpha = alpha_level, label = '5 - 6 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_67, bins = nb_bins, alpha = alpha_level, label = '6 - 7 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_g7, bins = nb_bins, alpha = alpha_level, label = '> 7 MeV', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Energy', fontsize = font_size);
        plt.title('Interaction Depth (gammas) / total: %s, 1-2: %s, 2-3: %s, 3-4: %s, 4-5: %s, 5-6: %s, 6-7: %s'%(len(all_part_yPos_gamma), len(all_part_yPos_gamma_12), len(all_part_yPos_gamma_23), len(all_part_yPos_gamma_34), len(all_part_yPos_gamma_45), len(all_part_yPos_gamma_56), len(all_part_yPos_gamma_67)), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-PD_gammas_energy-range", plots_dir)
        plt.cla(); plt.clf()
        # depth for gammas (broken down by energy ranges) [log]
        plt.hist(all_part_yPos_gamma, bins = nb_bins, alpha = alpha_level, label = 'all eng', log = True, histtype = 'step')
        plt.hist(all_part_yPos_gamma_01, bins = nb_bins, alpha = alpha_level, label = '0 - 1 MeV', log = True, histtype = 'step')
        plt.hist(all_part_yPos_gamma_12, bins = nb_bins, alpha = alpha_level, label = '1 - 2 MeV', log = True, histtype = 'step')
        plt.hist(all_part_yPos_gamma_23, bins = nb_bins, alpha = alpha_level, label = '2 - 3 MeV', log = True, histtype = 'step')
        plt.hist(all_part_yPos_gamma_34, bins = nb_bins, alpha = alpha_level, label = '3 - 4 MeV', log = True, histtype = 'step')
        plt.hist(all_part_yPos_gamma_45, bins = nb_bins, alpha = alpha_level, label = '4 - 5 MeV', log = True, histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Energy', fontsize = font_size);
        plt.title('Interaction Depth (gammas) / total: %s, 0-1: %s, 1-2: %s, 2-3: %s, 3-4: %s, 4-5: %s'%(len(all_part_yPos_gamma), len(all_part_yPos_gamma_01), len(all_part_yPos_gamma_12), len(all_part_yPos_gamma_23), len(all_part_yPos_gamma_34), len(all_part_yPos_gamma_45)), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-PD_gammas_energy-range_log", plots_dir)
        plt.cla(); plt.clf()

        all_part_eDep_gamma_12 = df[(df.particle == 22) & (df.energy > 1.0) & (df.energy < 2.0)].energy.to_numpy()
        all_part_eDep_gamma_23 = df[(df.particle == 22) & (df.energy > 2.0) & (df.energy < 3.0)].energy.to_numpy()
        all_part_eDep_gamma_34 = df[(df.particle == 22) & (df.energy > 3.0) & (df.energy < 4.0)].energy.to_numpy()
        all_part_eDep_gamma_45 = df[(df.particle == 22) & (df.energy > 4.0) & (df.energy < 5.0)].energy.to_numpy()
        all_part_eDep_gamma_56 = df[(df.particle == 22) & (df.energy > 5.0) & (df.energy < 6.0)].energy.to_numpy()
        all_part_eDep_gamma_67 = df[(df.particle == 22) & (df.energy > 6.0) & (df.energy < 7.0)].energy.to_numpy()
        all_part_eDep_gamma_g7 = df[(df.particle == 22) & (df.energy > 7.0)].energy.to_numpy()

        # depth for gammas (broken down by energy ranges) [energy-weighted]
        plt.hist(all_part_yPos_gamma_12, bins = nb_bins, weights = all_part_eDep_gamma_12, alpha = alpha_level, label = '1 - 2 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_23, bins = nb_bins, weights = all_part_eDep_gamma_23, alpha = alpha_level, label = '2 - 3 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_34, bins = nb_bins, weights = all_part_eDep_gamma_34, alpha = alpha_level, label = '3 - 4 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_45, bins = nb_bins, weights = all_part_eDep_gamma_45, alpha = alpha_level, label = '4 - 5 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_56, bins = nb_bins, weights = all_part_eDep_gamma_56, alpha = alpha_level, label = '5 - 6 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_67, bins = nb_bins, weights = all_part_eDep_gamma_67, alpha = alpha_level, label = '6 - 7 MeV', histtype = 'step')
        plt.hist(all_part_yPos_gamma_g7, bins = nb_bins, weights = all_part_eDep_gamma_g7, alpha = alpha_level, label = '> 7 MeV', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Energy', fontsize = font_size);
        plt.title('Interaction Depth (gammas, eng-wgt) / total: %s, 1-2: %s, 2-3: %s, 3-4: %s, 4-5: %s, 5-6: %s, 6-7: %s'%(len(all_part_yPos_gamma), len(all_part_yPos_gamma_12), len(all_part_yPos_gamma_23), len(all_part_yPos_gamma_34), len(all_part_yPos_gamma_45), len(all_part_yPos_gamma_56), len(all_part_yPos_gamma_67)), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Depth-PD_gammas_energy-range_eng-wgt", plots_dir)
        plt.cla(); plt.clf()


    if PRODUCE_COMPARE_TIMESTAMP_PLOTS:
        print ('  Creating 1D Timestamp Plots [particleData] . . .')

        df = allevents_data_particle
        all_part_time = df.time.to_numpy()
        all_part_time_gamma = df[df.particle == 22].time.to_numpy()
        all_part_time_neutron = df[df.particle == 2112].time.to_numpy()
        all_part_time_proton = df[df.particle == 2212].time.to_numpy()
        all_part_time_positron = df[df.particle == -11].time.to_numpy()
        all_part_time_ion = df[df.particle > 1e8].time.to_numpy()
        all_part_time_neu_sec = df[df.nTag == True].time.to_numpy()
        all_part_time_non_neu_sec = df[df.nTag == False].time.to_numpy()
        minTimPD = df.time.min();  maxTimPD = df.time.max()
        print('  minimum time stamp in particleData Tree: {} ns / maximum time stamp in particleData Tree: {} ns'.format(minTimPD, maxTimPD))

        nb_bins = 100;  rng_min = 0.0;  rng_max = 20
        x_label = 'time (ns)'
        font_size = 9 # 10
        alpha_level = 0.7
        """
        plt.hist(all_part_time, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'particleData')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (< %s ns), total[PD]: %s'%(rng_max, len(df[df.time < rng_max])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time_short_all_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_time, bins=nb_bins, range=[rng_min,rng_max], log = True, alpha = alpha_level, label = 'particleData')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (< %s ns), total[PD]: %s'%(rng_max, len(df[df.time < rng_max])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time_short_all_log_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        """
        plt.hist(all_part_time, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(all_part_time_neu_sec, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'PD/neu-second', histtype = 'step')
        plt.hist(all_part_time_non_neu_sec, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'PD/non-neu-sec', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Time stamps (< %s ns) / PD-total: %s, PD-ns: %s, PD-nns: %s'%(rng_max, len(df[df.time < rng_max]), len(df[(df.time < rng_max) & (df.nTag == True)]), len(df[(df.time < rng_max) & (df.nTag == False)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time-PD_neutron-secondary_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_time, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'particleData', histtype = 'step')
        plt.hist(all_part_time_neu_sec, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'PD/neu-second', histtype = 'step')
        plt.hist(all_part_time_non_neu_sec, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'PD/non-neu-sec', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of Particles', fontsize = font_size);
        plt.title('Time stamps (< %s ns) / PD-total: %s, PD-ns: %s, PD-nns: %s'%(rng_max, len(df[df.time < rng_max]), len(df[(df.time < rng_max) & (df.nTag == True)]), len(df[(df.time < rng_max) & (df.nTag == False)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time-PD_neutron-secondary_log_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        ### AAPM 2020 Plot
        plt.hist(all_part_time, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'particleData', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_time_neu_sec, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'PD/neu-second', histtype = 'step', linewidth = 1.5)
        plt.hist(all_part_time_non_neu_sec, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'PD/non-neu-sec', histtype = 'step', linewidth = 1.5)
        plt.xlabel(x_label, fontsize = 14);
        plt.ylabel('Number of Particles', fontsize = 14);
        #plt.title('Time stamps (< %s ns) / PD-total: %s, PD-ns: %s, PD-nns: %s'%(rng_max, len(df[df.time < rng_max]), len(df[(df.time < rng_max) & (df.nTag == True)]), len(df[(df.time < rng_max) & (df.nTag == False)])), fontsize = font_size)
        plt.legend(fontsize = 12)
        utils.save_to_file('aapm2020-raw_all_Time-PD_neutron-secondary_log_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()


        """
        plt.hist(all_part_time_gamma, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'gamma')
        plt.hist(all_part_time_positron, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'positron')
        plt.hist(all_part_time_ion, bins=nb_bins, range=[rng_min,rng_max], alpha = alpha_level, label = 'ion')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (< %s ns), [PD-g]: %s, [PD-po] %s, [PD-i] %s'%(rng_max, len(df[(df.time < rng_max) & (df.particle == 22)]), len(df[(df.time < rng_max) & (df.particle == -11)]), len(df[(df.time < rng_max) & (df.particle > 1e8)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time_short_gam_pos_ion_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_time_gamma, bins=nb_bins, range=[rng_min,rng_max], log = True, alpha = alpha_level, label = 'gamma')
        plt.hist(all_part_time_positron, bins=nb_bins, range=[rng_min,rng_max], log = True, alpha = alpha_level, label = 'positron')
        plt.hist(all_part_time_ion, bins=nb_bins, range=[rng_min,rng_max], log = True, alpha = alpha_level, label = 'ion')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (< %s ns), [PD-g]: %s, [PD-po] %s, [PD-i] %s'%(rng_max, len(df[(df.time < rng_max) & (df.particle == 22)]), len(df[(df.time < rng_max) & (df.particle == -11)]), len(df[(df.time < rng_max) & (df.particle > 1e8)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time_short_gam_pos_ion_log_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        """

        plt.hist(all_part_time_gamma, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'gamma', histtype = 'step')
        plt.hist(all_part_time_proton, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'proton', histtype = 'step')
        plt.hist(all_part_time_positron, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'positron', histtype = 'step')
        plt.hist(all_part_time_ion, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'ion', histtype = 'step')
        plt.hist(all_part_time_neutron, bins = nb_bins, range = [rng_min,rng_max], alpha = alpha_level, label = 'neutron', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamps (< %s ns) / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(rng_max, len(df[df.time < rng_max]), len(df[(df.time < rng_max) & (df.particle == 22)]), len(df[(df.time < rng_max) & (df.particle == 2212)]), len(df[(df.time < rng_max) & (df.particle == -11)]), len(df[(df.time < rng_max) & (df.particle > 1e8)]), len(df[(df.time < rng_max) & (df.particle == 2112)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time-PD_particle_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_time_gamma, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'gamma', histtype = 'step')
        plt.hist(all_part_time_proton, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'proton', histtype = 'step')
        plt.hist(all_part_time_positron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'positron', histtype = 'step')
        plt.hist(all_part_time_ion, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'ion', histtype = 'step')
        plt.hist(all_part_time_neutron, bins = nb_bins, range = [rng_min,rng_max], log = True, alpha = alpha_level, label = 'neutron', histtype = 'step')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamps (< %s ns) / total: %s, gam: %s, pro: %s, pos: %s, ion: %s, neu: %s'%(rng_max, len(df[df.time < rng_max]), len(df[(df.time < rng_max) & (df.particle == 22)]), len(df[(df.time < rng_max) & (df.particle == 2212)]), len(df[(df.time < rng_max) & (df.particle == -11)]), len(df[(df.time < rng_max) & (df.particle > 1e8)]), len(df[(df.time < rng_max) & (df.particle == 2112)])), fontsize = font_size)
        plt.legend()
        utils.save_to_file('raw_all_Time-PD_particle_log_%sns'%(rng_max), plots_dir)
        plt.cla(); plt.clf()
        """
        plt.hist(all_part_time, bins=nb_bins, range=[rng_max,maxTimPD], alpha = alpha_level, label = 'particleData')
        plt.xlabel(x_label, fontsize = font_size);
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (> %s ns), total[PD]: %s'%(rng_max, len(df[df.time > rng_max])), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Time_long", plots_dir)
        plt.cla(); plt.clf()

        plt.hist(all_part_time, bins=nb_bins, range=[rng_max,maxTimPD], alpha = alpha_level, log = True, label = 'particleData')
        plt.xlabel(x_label, fontsize = font_size);
        plt.xscale('log')
        plt.ylabel('Number of particles', fontsize = font_size);
        plt.title('Time stamp for all interactions (> %s ns), total[PD]: %s'%(rng_max, len(df[df.time > rng_max])), fontsize = font_size)
        plt.legend()
        utils.save_to_file("raw_all_Time_long_log_log", plots_dir)
        plt.cla(); plt.clf()
        """

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    #print ('-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    print ('-- Total interactions [particleData] / initial: {} / after energy_threshold: {}'.format(tot_raw_particle_events, len(allevents_data_particle.index)))
    df = allevents_data_particle
    ng = len(df.loc[ df.particle == 22 ]);  nn = len(df.loc[ df.particle == 2112 ]);  npr = len(df.loc[ df.particle == 2212 ])
    npo = len(df.loc[ df.particle == -11 ]);  ni = len(df.loc[ df.particle > 1e8 ]);  other = len(df.index) - ng - nn - npr - npo - ni
    print('    - Breakdown by particle - gamma[22]: {}, neutron[2112]: {}, proton[2212]: {}, positron[-11]: {}, ion[>1e8]: {}, other: {}'.format(ng, nn, npr, npo, ni, other))
    ns_t = len(df.loc[ df.nTag == True ]);  ns_g = len(df.loc[(df.nTag == True) & (df.particle == 22)]);  ns_n = len(df.loc[(df.nTag == True) & (df.particle == 2112)]);  ns_p = len(df.loc[(df.nTag == True) & (df.particle == 2212)]);
    ns_s = len(df.loc[(df.nTag == True) & (df.particle == -11)]);  ns_i = len(df.loc[(df.nTag == True) & (df.particle > 1e8)]);  ns_o = ns_t - ns_g - ns_n - ns_p - ns_s - ns_i
    nns_t = len(df.loc[ df.nTag == False ]);  nns_g = len(df.loc[(df.nTag == False) & (df.particle == 22)]);  nns_n = len(df.loc[(df.nTag == False) & (df.particle == 2112)]);  nns_p = len(df.loc[(df.nTag == False) & (df.particle == 2212)]);
    nns_s = len(df.loc[(df.nTag == False) & (df.particle == -11)]);  nns_i = len(df.loc[(df.nTag == False) & (df.particle > 1e8)]);  nns_o = nns_t - nns_g - nns_n - nns_p - nns_s - nns_i
    print('    - Breakdown by neutron production     / total: {}'.format(len(allevents_data_particle.index)))
    print('      - particles produced by neutron     / total: {}, gamma[22]: {}, neutron[2112]: {}, proton[2212]: {}, positron[-11]: {}, ion[>1e8]: {}, other: {}'.format(ns_t, ns_g, ns_n, ns_p, ns_s, ns_i, ns_o))
    print('      - particles not produced by neutron / total: {}, gamma[22]: {}, neutron[2112]: {}, proton[2212]: {}, positron[-11]: {}, ion[>1e8]: {}, other: {}'.format(nns_t, nns_g, nns_n, nns_p, nns_s, nns_i, nns_o))
    print ('-- Total interactions [scatterData]  / initial: {} / after energy_threshold: {}'.format(tot_raw_scatter_events, len(allevents_data_scatter.index)))




#------------------------------------------------------------------
# SETTINGS / PARAMETERS
#------------------------------------------------------------------

### Plotting
PRODUCE_RAW_PLOTS = False             # produces 1D & 2D position and energy plots from raw data, produces plots for individual detectors
PRODUCE_TRANSFORMED_PLOTS = False     # produces 1D & 2D position and energy plots from coordinte-transformed data, also produces plots for each scatter number
PRODUCE_FINAL_DOUBLE_PLOTS = False    # produces 1D energy plots of filtered double events, also produced Compton Line plots (E1 vs Theta1)

### compare_particle_data plots
PRODUCE_COMPARE_ENERGY_PLOTS = False
PRODUCE_COMPARE_2D_POSITION_PLOTS = False
PRODUCE_COMPARE_2D_POSITION_BREAKDOWN_PLOTS = True
PRODUCE_COMPARE_DEPTH_PLOTS = False
PRODUCE_COMPARE_TIMESTAMP_PLOTS = False

### OUTPUT FILES
STORE_SINGLE_SCATTERS = False
STORE_ORIGINS_TAGS = False

### Data filtering
APPLY_BIG_ENERGY_SORTING = False
REMOVE_UNPHYSICAL_EVENTS = False
APPLY_ENERGY_THRESHOLD = False       # default value is True
MINIMUM_ENERGY_THRESHOLD = 0.05      # any events below this energy (in MeV) will be removed / default is 0.05 MeV
APPLY_COMPTON_LINE_FILTERING = False
COMPTON_LINE_RANGE = np.array( [0.960, 1.040] )     # min / max values (percentage) for Compton line filtering
COMPTON_LINE_ENERGIES = np.array( [7.0] )  # multiple values possible, i.e. np.array( [0.5110, 0.6617, 1.173, 1.332] ) / ALSO doubles as max energy for alternate plots
APPLY_WEIGHTING_POTENTIAL = False    # default value is False
SHUFFLE_SCATTER_EVENTS = False

### Coincidence grouping
APPLY_COINCIDENCE_GROUPING_MULTISTAGE = False

#------------------------------------------------------------------
# MAIN PROGRAM
#------------------------------------------------------------------

def main():
    print ('\n--- Running polarisMC_processing.py ---')

    """
    #### test Data ####
    # ----------------- #
    #filter_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/python_test/', 'scatterData')
    compare_particle_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/python_test/', 'particleData')
    #filter_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/python_test/', 'scatterData')
    #filter_particle_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/python_test/', 'particleData')
    #compare_particle_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/HDPE-175MeV_protons-no_timing_cutoff-100M/', 'particleData')
    #filter_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/HDPE-175MeV_protons-no_timing_cutoff-100M/', 'scatterData')
    #filter_particle_data('/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190806-p3-neutron_tracking/001-build/HDPE-175MeV_protons-no_timing_cutoff-100M/', 'particleData')
    """

    #### 2020-iTL Data ####
    # ----------------- #
    #filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/proton_pUCT_iTL_HDPE_200MeV_threeDet-050M/', 'scatterData')
    #filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/proton_pUCT_iTL_HDPE_200MeV_fourDet_06cm-100M/', 'scatterData')
    #filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/proton_pUCT_iTL_HDPE_066MeV_fourDet_06cm-100M/', 'scatterData')
    filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/test/', 'scatterData')
    #filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/proton_pUCT_iTL_HDPE_066MeV_fourDet_06cm_no1x-200M/', 'scatterData')
    #filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/2020-iTL/proton_pUCT_iTL_HDPE_200MeV_fourDet_06cm_no1x-100M/', 'scatterData')

    """
    #### 180315 Data ####
    # ----------------- #
    filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180315/180315-run7-cs137_at_05_-05_11mm-air-dbox-singles-single_multi-energyUncer-0.1-sourceWidth-1mm_020M/root/', 'scatterData')
    # filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180315/180315-run7-cs137_at_05_-05_11mm-air-dbox-doubles_triples-single_multi-energyUncer-0.1-sourceWidth-1mm_005M/root/', 'scatterData')
    # filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180315/180315-run7-cs137_at_05_-05_11mm-air-dbox-doubles_triples-single_multi-only_primaries-zero_scatter-perfect_020M/root/', 'scatterData')
    """
    """
    #### 190516 Data ####
    # ----------------- #
    filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/190516/190516-run1-co60_at_-3_-6_11mm-air-dbox-doubles_triples-single_multi-energyUncer-0.1-sourceWidth-1mm_020M/root/', 'scatterData')
    # filter_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/190516/run1_grid/190516-run1-co60_at_-20_020_11mm-air-dbox-doubles_triples-single_multi-energyUncer-0.1-sourceWidth-1mm_040M/', 'scatterData', True)
    # filter_data('/home/steve/PGI/data/uct/polaris-montecarlo/190516/run1_grid/190516-run1-co60_at_-40_-20_11mm-air-dbox-doubles_triples-single_multi-energyUncer-0.1-sourceWidth-1mm_040M/', 'scatterData', True)
    # filter_data('/home/steve/PGI/data/uct/polaris-montecarlo/190516/run1_grid/190516-run1-co60_at_-40_-40_11mm-air-dbox-doubles_triples-single_multi-energyUncer-0.1-sourceWidth-1mm_040M/', 'scatterData', True)
    """
    """
    #### 190711 Data ####
    # ----------------- #
    d0_transformation = [180.0, 90.0, 0.0, -45.5, 0.0, 0.0]  # run 1
    d1_transformation = [0.0, 270.0, 0.0, 45.5, 0.0, 0.0]    # run 1
    filter_data('/Volumes/Batman/PGI/data/uct/polaris-streaming/190711/190711-run1-na22_oem2_y_+48_oem3_y_-48mm-z_trav/tina_source_at_centre_data/', 'AllEventsCombined_0mm.txt', [d0_transformation, d1_transformation])
    """
    """
    #### 190805 Data ####
    # ----------------- #
    d0_transformation = [180.0, 90.0, 0.0, -48, 0.0, 0.0]  # run 1
    d1_transformation = [0.0, 270.0, 0.0, 48, 0.0, 0.0]    # run 1
    filter_data('/Volumes/Batman/PGI/data/uct/polaris-streaming/190805/190805-run1-na22_at_-1_0_11mm-oem2_x_-48mm-oem3_x_48mm-submm-10min/', 'AllEventsCombined.txt', [d0_transformation, d1_transformation])
    filter_data('/Volumes/Batman/PGI/data/uct/polaris-streaming/190805/190805-run2-na22_at_-1_0_11mm-oem2_x_-48mm-oem3_x_48mm-NOsubmm-10min/', 'AllEventsCombined.txt', [d0_transformation, d1_transformation])
    filter_data('/Volumes/Batman/PGI/data/uct/polaris-streaming/190805/190805-run3-na22_at_-1_0_11mm-oem2_x_-48mm-oem3_x_48mm-submm-10min/', 'AllEventsCombined.txt', [d0_transformation, d1_transformation])
    filter_data('/Volumes/Batman/PGI/data/uct/polaris-streaming/190805/190805-run4-na22_at_-1_0_11mm-oem2_x_-48mm-oem3_x_48mm-NOsubmm-10min/', 'AllEventsCombined.txt', [d0_transformation, d1_transformation])
    """



if __name__ == "__main__":

    main()
