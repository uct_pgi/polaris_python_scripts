#!/usr/bin/python
"""
Description:
  Python script that reads in all POLARIS3 Geant4 simulated data (ROOT files) in given directory and converts to csv
    and can:
             store source origin data and scatter tags to output file
             creates csv data file
             automatically saves csv files to save directory as ROOT files

  To Run: python scatterData_ROOT2CSV_multiFile.py &> output-log.txt &
    From main program, call function: convert_data(data_dir, root_tree_name, USE_DIR_AS_OUTPUT = False)
              data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
              root_tree_name      - Required : Name of ROOT tree to read data from (Str) / Also used
              USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    Additional settings are handled through Global Variable under SETTINGS / PARAMETERS (just before main)
      - STORE_SINGLE_SCATTERS               = True (store -1x.csv files) / False (do not store, default), stores single-scatter data to file scatters_1x.csv
      - STORE_ORIGINS_TAGS                  = True (store -origins-tags.csv files) / False (do not store, default), duplicate files for scatters_2x.csv which include origin location and scatter tags

NOTES:
- written using python v2.7.15 / updated to also work in python 3.6.2
- processPolarisData_utils.pyx required to run this code
- some aspects of the code only function for double-scatters

Code borrowed heavily from Dennis Mackin <dsmackin@mdanderson.org> & Nicholas Hyslop <HYSNIC007@myuct.ac.za>
"""
__author__ = "Steve Peterson <steve.peterson@uct.ac.za>"
__date__ = "September 24, 2019"
__version__ = "$Revision: 1.0.0$"

#------------------------------------------------------------------
# PYTHON IMPORT STATEMENTS
#------------------------------------------------------------------

import os
import sys
import timeit

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import uproot


#------------------------------------------------------------------
# FUNCTION/CLASS DEFINITIONS
#------------------------------------------------------------------

# Read in scatter data from ROOT files and convert into pandas array
def read_root_data(data_dir, root_tree_name, output_file_name):
    '''
    Reads in scatterTree from ROOT files in data_dir, reads the information into a
    python pandas array, and returns the DataFrame.

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        output_file_name    - Optional : Name for output files to store data (Str), default is to use scatterData

    @returns:
        allevents_data      - pandas array of transformed simulated Geant4 data
        root_file_list      - list of root filenames
    '''

    #  creating arrays to store data for conversion into pandas format
    numInt = []; detNum = []; engDep = []; xPos = []; yPos = []; zPos = []; sTag = []
    eOrg = []; xOrg = []; yOrg = []; zOrg = []; sStg = [];

    #  assign a variable to each leaf in the scatterData tree
    evt_leaf = "event";             trk_leaf = "track";             stp_leaf = "step";              det_leaf = "detector"
    dpos_leaf = "detPos";           dsize_leaf = "detSize";         pro_leaf = "process";           xpos_leaf = "pos_x";
    ypos_leaf = "pos_y";            zpos_leaf = "pos_z";            eng_leaf = "gammaEnergy";       dep_leaf = "energyDeposited";
    ang_leaf = "scatAng";           xorg_leaf = "origin_x";         yorg_leaf = "origin_y";         zorg_leaf = "origin_z"
    eorg_leaf = "origin_energy";    porg_leaf = "origin_process";   vorg_leaf = "origin_volume";    stag_leaf = "scatter_tag"

    # Looking at directory tree and storing the names of the ROOT files
    #   -> source: https://realpython.com/working-with-files-in-python/#traversing-directories-and-processing-files
    print ('  Reading in ROOT files from directory: {}'.format(data_dir))
    root_file_list = []
    for f_name in os.listdir(data_dir):
        if f_name.endswith('.root'):
            root_file_list.append(f_name)
    NUMBER_OF_INPUT_FILES = len(root_file_list)
    #NUMBER_OF_INPUT_FILES = 1
    print ('   - found {} files\n'.format(NUMBER_OF_INPUT_FILES))

    #  loop through input files
    print ('  Reading {} input files, could take several minutes . . .'.format(NUMBER_OF_INPUT_FILES))
    for i in range(NUMBER_OF_INPUT_FILES):

        temp_file_name = data_dir + root_file_list[i]
        print ('    {} -> Filename : {}'.format(i + 1, temp_file_name))
        print ('        - Size: {} bytes'.format(os.path.getsize(temp_file_name)))
        MyFile = uproot.open(temp_file_name)
        MyTree = MyFile[root_tree_name]

        #  form an array for each leaf
        evt = MyTree[evt_leaf].array();     trk = MyTree[trk_leaf].array();         stp = MyTree[stp_leaf].array();     det = MyTree[det_leaf].array()
        dPos = MyTree[dpos_leaf].array();   dSize = MyTree[dsize_leaf].array();     pro = MyTree[pro_leaf].array();     xpos = MyTree[xpos_leaf].array()
        ypos = MyTree[ypos_leaf].array();   zpos = MyTree[zpos_leaf].array();       gEng = MyTree[eng_leaf].array();    engD = MyTree[dep_leaf].array()
        sAng = MyTree[ang_leaf].array();    xorg = MyTree[xorg_leaf].array();       yorg = MyTree[yorg_leaf].array();   zorg = MyTree[zorg_leaf].array()
        eorg = MyTree[eorg_leaf].array();   porg = MyTree[porg_leaf].array();       vorg = MyTree[vorg_leaf].array();   stag = MyTree[stag_leaf].array()

        #  looping through numpy arrays to convert into panda data format
        for i in range(len(evt)):

            # extend to include origin data + single-stage tag
            ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss = convert_root_arrays_origins(det[i], gEng[i], engD[i], xpos[i], ypos[i], zpos[i], stag[i], eorg[i], xorg[i], yorg[i], zorg[i])
            #  extend(): Iterates over its argument and adding each element to the list and extending the list. The length of the list increases by number of elements in it's argument.
            #  Append has constant time complexity i.e.,O(1).  Extend has time complexity of O(k). Where k is the length of list which need to be added.
            numInt.extend(ni);  detNum.extend(dn);  engDep.extend(ed);  xPos.extend(xp);  yPos.extend(yp);  zPos.extend(zp);  sTag.extend(st)
            eOrg.extend(eo);  xOrg.extend(xo);  yOrg.extend(yo);  zOrg.extend(zo);  sStg.extend(ss)

    #  changing data format (default is float64) in order to run coordinate transformation [should figure out way to read data directly into pandas format]
    print ('\n  Converting the data into pandas format . . .')
    pandas_data = {'scatters': numInt, 'detector': detNum, 'energy': engDep, 'x': xPos, 'y': yPos, 'z': zPos, 'tag': sTag, 'engOrg': eOrg, 'xOrg': xOrg, 'yOrg': yOrg, 'zOrg': zOrg, 'sStg': sStg}
    allevents_data = pd.DataFrame(pandas_data)

    print('{:30} {:d}'.format('Total number of interactions:', len(allevents_data.index)))

    # reorder allevents_data into same order as input file
    allevents_data = allevents_data[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg', 'sStg']]

    #  convert detector, scatters columns from float (default) into ints & tag columns from byte string into string
    allevents_data['detector'] = allevents_data['detector'].astype(int)
    allevents_data['scatters'] = allevents_data['scatters'].astype(int)
    allevents_data.tag = allevents_data.tag.str.decode("utf-8")

    print('\n  Done.')

    return allevents_data, root_file_list


#  converts scatter (+ tag & origin) data from ROOT arrays and converts for use with panda dataframe
def convert_root_arrays_origins(det, geng, engd, xpos, ypos, zpos, tag, eorg, xorg, yorg, zorg):
    #  det, eng, xpos, ypos and zpos are arrays with size = 3
    #  count, tag, eorg, xorg, yorg and zorg are single values

    #  initializing variables
    count = 0
    di = []
    ni = []; dn = []; ed = []; xp = []; yp = []; zp = []; st = []
    eo = []; xo = []; yo = []; zo = []; ss = []

    #  loop through array to count number of interactions & convert detector string to index
    for i in range(3):
        if (geng[i] != 0):
            count += 1
            if b'D01' in det[i]: di.append(0)
            if b'D02' in det[i]: di.append(1)
            if b'D03' in det[i]: di.append(2)
            if b'D04' in det[i]: di.append(3)
            if b'D05' in det[i]: di.append(4)
            if b'D06' in det[i]: di.append(5)
            if b'D07' in det[i]: di.append(6)
            if b'D08' in det[i]: di.append(7)

    #  check for multi-stage events / set sStg flag (true = single-stage, false = multi-stage)
    if   (count == 1):                                              single_stage = True
    elif (count == 2 and di[0] == di[1]):                           single_stage = True
    elif (count == 3 and di[0] == di[1]):                           single_stage = True
    else:                                                           single_stage = False

    #  loop through values to fill lists to return
    for j in range(count):
        ni.append(count);  dn.append(di[j]);  ed.append(engd[j]);  xp.append(xpos[j]);  yp.append(ypos[j]);  zp.append(zpos[j]);  st.append(tag)
        eo.append(eorg);   xo.append(xorg);   yo.append(yorg);     zo.append(zorg);     ss.append(single_stage)

    return ni, dn, ed, xp, yp, zp, st, eo, xo, yo, zo, ss


#  returns interactions (E, X, Y, Z), tags (4 character string) and origins (E, X, Y, Z) for the specified number of scatters
def get_interaction_data_tags_origin(df, num_scatters = 2):
    # input dataframe format: cc_dataframe[['scatters', 'detector', 'energy', 'x', 'y', 'z', 'tag', 'engOrg', 'xOrg', 'yOrg', 'zOrg']]

    print ('   - Grouping data for scatter number {}'.format(num_scatters))

    #  stores the data with the relevant number of scatters
    chosen = df[np.isclose(df.scatters, num_scatters)]

    #  re-formatting scatter data
    #   - to_numpy() -> Convert the frame to its Numpy-array representation
    scatters = chosen.to_numpy()

    #  create array for scatter tags and origin data / needs to come before scatters array is truncated
    tags = scatters[:, 6]
    origins = scatters[:, [7, 8, 9, 10]]
    #  only store columns 2, 3, 4, 5 (i.e. eng, x, y, z)
    scatters = scatters[:, [2, 3, 4, 5]]

    #  store scatter (and origin) data as float instead of as object (default), important for calcuation later in code
    scatters = scatters[:, [0, 1, 2, 3]].astype(float)
    origins = origins[:, [0, 1, 2, 3]].astype(float)

    #  checks that the final number of event is evenly divisible by the number of scatters <- disabled warning
    if len(scatters) % num_scatters != 0:
        # Change Exception to Warning
        #raise Exception("Wrong number of scatters (%d) for scatters (%d), mod = (%d)" \
        #                % (len(scatters), num_scatters, len(scatters) % num_scatters))
        print ("!!! WARNING: Wrong number of scatters (%d) for scatters (%d), mod = (%d)" % (len(scatters), num_scatters, len(scatters) % num_scatters))

    #  print counts to screen
    print ('     - found {} interactions out of {}'.format(len(scatters), len(df)))

    return scatters, tags, origins


#  re-arranges scatter, tag, origin data from consecutive lines to all Compton scatter data on a single line for CSV output
def format_data_for_output_tags_origins(scatters, tags, origins, num_scatters):

    print ('   - Formatting {} interactions from number of scatters: {}'.format(len(scatters), num_scatters))

    #  check if any data in scatters array
    if scatters.shape[0] == 0:
        return scatters

    #  re-arranges double scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2 format
    if num_scatters == 2:
        #  moves next event (i + 1) to the same line as current event
        temp = np.concatenate((scatters[0:-1], scatters[1:]), axis = 1)
        #  stores data from every other line
        out = temp[0::2]
        tagOut = tags[0::2]
        originsOut = origins[0::2]

    #  re-arranges triple scatters into E1, X1, Y1, Z1, E2, X2, Y2, Z2, E3, X3, Y3, Z3 format
    if num_scatters == 3:
        #  moves next two events (i + 1 & i + 2) to the same line as current event
        temp = np.concatenate((scatters[0:-2], scatters[1:-1], scatters[2:]), axis = 1)
        #  stores data from every third line
        out = temp[0::3]
        tagOut = tags[0::3]
        originsOut = origins[0::3]

    print ('     - returned {} events'.format(len(out)))

    return out, tagOut, originsOut


#  counting gamma scatters using scatter_tag
def count_gamma_scatter(scatters, tags):

    count_PZ = 0; count_NZ = 0; count_OZ = 0; count_PS = 0; count_NS = 0; count_OS = 0; count_PM = 0; count_NM = 0; count_OM = 0

    if len(tags) > 0:
        for i in range(len(tags)):
            #  count prompt/nonprompt events + number of scatter events
            #   -> tag[0] / 1st [particleType]: (P)rompt, (N)eutron-induced or (O)ther gamma
            #   -> tag[1] / 2nd [scatterNumber]: (Z)ero, (S)ingle, (M)ultiple scatters
            if tags[i][:2] == 'PZ':  count_PZ += 1
            elif tags[i][:2] == 'NZ':  count_NZ += 1
            elif tags[i][:2] == 'OZ':  count_OZ += 1
            elif tags[i][:2] == 'PS':  count_PS += 1
            elif tags[i][:2] == 'NS':  count_NS += 1
            elif tags[i][:2] == 'OS':  count_OS += 1
            elif tags[i][:2] == 'PM':  count_PM += 1
            elif tags[i][:2] == 'NM':  count_NM += 1
            else:  count_OM += 1

        #  print to screen
        print ('   - Total Events: {} | PZ: {} ({:.2f}%) | PS: {} ({:.2f}%) | PM: {} ({:.2f}%)'.format(len(tags), count_PZ, (float(count_PZ)/len(tags))*100.0, count_PS, (float(count_PS)/len(tags))*100.0, count_PM, (float(count_PM)/len(tags))*100.0))
        print ('   -                  | NZ: {} ({:.2f}%) | NS: {} ({:.2f}%) | NM: {} ({:.2f}%)'.format(count_NZ, (float(count_NZ)/len(tags))*100.0, count_NS, (float(count_NS)/len(tags))*100.0, count_NM, (float(count_NM)/len(tags))*100.0))
        print ('   -                  | OZ: {} ({:.2f}%) | OS: {} ({:.2f}%) | OM: {} ({:.2f}%)'.format(count_OZ, (float(count_OZ)/len(tags))*100.0, count_OS, (float(count_OS)/len(tags))*100.0, count_OM, (float(count_OM)/len(tags))*100.0))
        print ('   ->  {}, {}, {}, {}, {}, {}, {}, {}, {}, {}'.format(len(tags), count_PZ, count_NZ, count_OZ, count_PS, count_NS, count_OS, count_PM, count_NM, count_OM))

    else:
        print('    - No events with this scatter number found.  Returning zeros')

    #  return counts
    return count_PZ, count_NZ, count_OZ, count_PS, count_NS, count_OS, count_PM, count_NM, count_OM



# Process the ROOT scatter data and store output csv files
def convert_data(data_dir, root_tree_name, USE_DIR_AS_OUTPUT = False):
    '''
    Takes in ROOT files, reads the information into a panda array, then outputs data into csv format

    @params:
        data_dir            - Required : Path to monte carlo detector ROOT data files (Str)
        root_tree_name      - Required : Name of ROOT tree to read data from (Str)
        USE_DIR_AS_OUTPUT   - Optional Flag : True = uses directory name to store output files (Bool), False (default) = uses root_tree_name

    @returns:
        scatters_2x_out     - numpy array of the processed double scatter data recorded by polaris detectors
        scatters_3x_out     - numpy array of the processed triple scatter data recorded by polaris detectors
    '''

    #  measuring time required to run code - start time
    start = timeit.default_timer()

    #  Setting output_file_name, either breaking up data_dir or using root_tree_name (default)
    if USE_DIR_AS_OUTPUT:
        data_dir_split = data_dir.split('/')
        output_file_name = data_dir_split[-3]   # use third from end (drops "root" and blank space)
        print ('   - Using data_dir to set output filename -> {}'.format(output_file_name))
    else:
        output_file_name = root_tree_name

    #### NOTE: need to figure out how to drop data_dir down one level (remove /root/)

    allevents_data, root_file_list = read_root_data(data_dir, root_tree_name, output_file_name)       # read in the simulated scatter data
    tot_raw_events = len(allevents_data.index)          # get the total number of events before coincidence processing

    #  Counting gamma scatter interactions (before the interactions were grouped into events)
    nbInt_prompt = 0; nbInt_neutron = 0; nbInt_other = 0          # count prompt/neutron/other events
    nbInt_zero = 0; nbInt_single = 0; nbInt_multiple = 0          # count zero/single/multiple scatter events
    nbInt_nonScat = 0; nbInt_tarScat = 0; nbInt_detScat = 0; nbInt_othScat = 0; nbInt_mulScat = 0      # count scatter in none/target/detector/other/multiple
    nbInt_tarCreat = 0; nbInt_detCreat = 0; nbInt_othCreat = 0    # count creations in target/detector/other

    """
    for i in range(len(allevents_data['tag'])):
        #print (' ****** allevents_data[tag][{}][0]: {}'.format(i, allevents_data['tag'][i][0]))
        #  count prompt/nonprompt events -> tag[0] / 1st [particleType]: (P)rompt, (N)eutron-induced or (O)ther gamma
        if allevents_data['tag'][i][0] == 'P':  nbInt_prompt += 1
        elif allevents_data['tag'][i][0] == 'N':  nbInt_neutron += 1
        else:  nbInt_other += 1
        #  count number of scatter events -> tag[1] / 2nd [scatterNumber]: (Z)ero, (S)ingle, (M)ultiple scatters
        if allevents_data['tag'][i][1] == 'Z':  nbInt_zero += 1
        elif allevents_data['tag'][i][1] == 'S':  nbInt_single += 1
        else:  nbInt_multiple += 1
        #  count number of scatter events -> tag[1] / 3rd [scatterLocation]: (T)arget, (D)etector, (O)ther, (M)ultiple, (N)one
        if allevents_data['tag'][i][2] == 'N':  nbInt_nonScat += 1
        elif allevents_data['tag'][i][2] == 'T':  nbInt_tarScat += 1
        elif allevents_data['tag'][i][2] == 'D':  nbInt_detScat += 1
        elif allevents_data['tag'][i][2] == 'O':  nbInt_othScat += 1
        else:  nbInt_mulScat += 1
        #  count number of scatter events -> tag[1] / 4th [creationLocation]: (T)arget, (D)etector, (O)ther
        if allevents_data['tag'][i][3] == 'T':  nbInt_tarCreat += 1
        elif allevents_data['tag'][i][3] == 'D':  nbInt_detCreat += 1
        else:  nbInt_othCreat += 1
    """

    #  output gamma scatter details
    print ('\n  Counting gamma scatter interactions . . .')
    print ('   - Total Gamma Interactions: {} | prompt: {} ({:.2f}%) | neutron-induced: {} ({:.2f}%) | other: {} ({:.2f}%)'.format(len(allevents_data['tag']), nbInt_prompt, (float(nbInt_prompt)/len(allevents_data['tag']))*100.0, nbInt_neutron, (float(nbInt_neutron)/len(allevents_data['tag']))*100.0, nbInt_other, (float(nbInt_other)/len(allevents_data['tag']))*100.0))
    print ('   - Number of Scatters | zero: {} ({:.2f}%) | single: {} ({:.2f}%) | multiple: {} ({:.2f}%)'.format(nbInt_zero, (float(nbInt_zero)/len(allevents_data['tag']))*100.0, nbInt_single, (float(nbInt_single)/len(allevents_data['tag']))*100.0, nbInt_multiple, (float(nbInt_multiple)/len(allevents_data['tag']))*100.0))
    print ('   - Scatter Location | none: {} | target: {} | detector: {} | other: {} | multiple: {}'.format(nbInt_nonScat, nbInt_tarScat, nbInt_detScat, nbInt_othScat, nbInt_mulScat))
    print ('   - Creation Location | target: {} | detector: {} | other: {}'.format(nbInt_tarCreat, nbInt_detCreat, nbInt_othCreat))


    # Counters for run summary
    num_1px, num_2px, num_3px, num_4px = [float('nan')] * 4                # count events based on pixel number
    num_ss, num_ss_et, num_ss_fi = [float('nan')] * 3                      # count events for single scatters
    num_ds, num_ds_d0, num_ds_d1, num_ts = [float('nan')] * 4              # count total number of double/triple scatters
    num_ds_et, num_ds_d0_et, num_ds_d1_et, num_ts_et = [float('nan')] * 4  # count number of energy threshold events
    num_ds_pe, num_ds_d0_pe, num_ds_d1_pe = [float('nan')] * 3             # count number of physical events
    num_ds_cl, num_ds_d0_cl, num_ds_d1_cl = [float('nan')] * 3             # count number of Compton line filtered events
    num_ds_ms, num_ds_d0_ms, num_ds_d1_ms = [float('nan')] * 3             # count number of multistage coincidence events
    num_ds_fi, num_ds_d0_fi, num_ds_d1_fi, num_ts_fi = [float('nan')] * 4  # final number of double/triple scatters
    num_ms_total, num_ms_2px, num_ms_3px = [float('nan')] * 3              # count multi-stage interactions

    # Counters for scatter tags
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_OZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_OS, nbEvt_1x_PM, nbEvt_1x_NM, nbEvt_1x_OM = [float('nan')] * 9
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_OZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_OS, nbEvt_2x_PM, nbEvt_2x_NM, nbEvt_2x_OM = [float('nan')] * 9
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_OZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_OS, nbEvt_3x_PM, nbEvt_3x_NM, nbEvt_3x_OM = [float('nan')] * 9

    # count number of pixel events
    num_1px = len(allevents_data[allevents_data.scatters == 1]);  num_2px = len(allevents_data[allevents_data.scatters == 2])
    num_3px = len(allevents_data[allevents_data.scatters == 3]);  num_4px = len(allevents_data[allevents_data.scatters  > 3])

    # count number of multi-stage interactions
    num_ms_total = len(allevents_data[allevents_data.sStg == False])
    num_ms_2px = len(allevents_data[(allevents_data.scatters == 2) & (allevents_data.sStg == False)])
    num_ms_3px = len(allevents_data[(allevents_data.scatters == 3) & (allevents_data.sStg == False)])

    print('\nFiltering data')
    print('-----------------------')

    print ('\n--- Grouping Compton Scatter Data ---')

    #  returns interactions (E, X, Y, Z), tags (4 character string) and origins (E, X, Y, Z) for the specified number of scatters
    #   format -> def get_interaction_data_tags_origin(df, num_scatters = 2)
    scatters_1x, tags_1x, origins_1x = get_interaction_data_tags_origin(allevents_data, 1)
    scatters_2x, tags_2x, origins_2x = get_interaction_data_tags_origin(allevents_data, 2)
    scatters_3x, tags_3x, origins_3x = get_interaction_data_tags_origin(allevents_data, 3)

    ### FORMATTING DATA FOR OUTPUT ###

    print ('\n--- Formatting Compton Scatter Data for Output ---')
    print ('  Re-arranging data array for CSV output . . .')

    #  re-arranges interaction data into CSV format (entire event on one line)
    #   format -> def format_data_for_output(scatters, tags, num_scatters):
    scatters_1x_out = scatters_1x  # no rearrangment required
    tags_1x_out = tags_1x  # creating tags array
    origins_1x_out = origins_1x  # creating origins array
    scatters_2x_out, tags_2x_out, origins_2x_out = format_data_for_output_tags_origins(scatters_2x, tags_2x, origins_2x, 2)
    scatters_3x_out, tags_3x_out, origins_3x_out = format_data_for_output_tags_origins(scatters_3x, tags_3x, origins_3x, 3)


    # count initial number of single/double/triple scatters
    num_ss = len(scatters_1x);  num_ds = len(scatters_2x_out);  num_ts = len(scatters_3x_out)

    print ('\n  Counting 1x gamma scatter events . . .')
    nbEvt_1x_PZ, nbEvt_1x_NZ, nbEvt_1x_OZ, nbEvt_1x_PS, nbEvt_1x_NS, nbEvt_1x_OS, nbEvt_1x_PM, nbEvt_1x_NM, nbEvt_1x_OM = count_gamma_scatter(scatters_1x_out, tags_1x_out)
    print ('\n  Counting 2x gamma scatter events . . .')
    nbEvt_2x_PZ, nbEvt_2x_NZ, nbEvt_2x_OZ, nbEvt_2x_PS, nbEvt_2x_NS, nbEvt_2x_OS, nbEvt_2x_PM, nbEvt_2x_NM, nbEvt_2x_OM = count_gamma_scatter(scatters_2x_out, tags_2x_out)
    print ('\n  Counting 3x gamma scatter events . . .')
    nbEvt_3x_PZ, nbEvt_3x_NZ, nbEvt_3x_OZ, nbEvt_3x_PS, nbEvt_3x_NS, nbEvt_3x_OS, nbEvt_3x_PM, nbEvt_3x_NM, nbEvt_3x_OM = count_gamma_scatter(scatters_3x_out, tags_3x_out)



    ### SAVING DATA TO CSV ###

    print ('\n--- Saving Polaris Data to CSV ---')

    #  adding origins data to scatters_1x
    scatters_1x_origins_out = np.concatenate((scatters_1x_out, origins_1x_out), axis = 1)

    #  adding origins data to scatters_2x
    scatters_2x_origins_out = np.concatenate((scatters_2x_out, origins_2x_out), axis = 1)

    #  adding origins data to scatters_3x
    scatters_3x_origins_out = np.concatenate((scatters_3x_out, origins_3x_out), axis = 1)

    #  adding tags to origins data for 1x
    df_1x_o = pd.DataFrame(scatters_1x_origins_out, columns=('e1','x1','y1','z1','eO','xO','yO','zO'))
    df_1x_t = pd.DataFrame(tags_1x_out, index = df_1x_o.index)  # re-using index from scatters_1x_origins_out dataFrame
    scatters_1x_origins_tags_out = pd.concat([df_1x_o, df_1x_t], axis = 1, sort = False)

    #  adding tags to origins data for 2x
    df_2x_o = pd.DataFrame(scatters_2x_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','eO','xO','yO','zO'))
    df_2x_t = pd.DataFrame(tags_2x_out, index = df_2x_o.index)  # re-using index from scatters_2x_origins_out dataFrame
    scatters_2x_origins_tags_out = pd.concat([df_2x_o, df_2x_t], axis = 1, sort = False)

    #  adding tags to origins data for 3x
    df_3x_o = pd.DataFrame(scatters_3x_origins_out, columns=('e1','x1','y1','z1','e2','x2','y2','z2','e3','x3','y3','z3','eO','xO','yO','zO'))
    df_3x_t = pd.DataFrame(tags_3x_out, index = df_3x_o.index)  # re-using index from scatters_2x_origins_out dataFrame
    scatters_3x_origins_tags_out = pd.concat([df_3x_o, df_3x_t], axis = 1, sort = False)


    #  creating output file names
    #   - add tags based on filter parameters
    output_file = data_dir + output_file_name.replace(".txt", "").replace(".dat", "").replace(".csv", "") + "-scatters.csv"
    output_file_1x = output_file.replace(".csv", "_1x.csv")
    output_file_1x_origins_tags = output_file.replace(".csv", "_1x-origins-tags.csv")

    output_file_2x = output_file.replace(".csv", "_2x.csv")
    output_file_2x_origins_tags = output_file.replace(".csv", "_2x-origins-tags.csv")

    output_file_3x = output_file.replace(".csv", "_3x.csv")
    output_file_3x_origins_tags = output_file.replace(".csv", "_3x-origins-tags.csv")

    output_file_2x_3x = output_file.replace(".csv", "_2x+3x.csv")
    output_file_2x_3x_origins_tags = output_file.replace(".csv", "_2x+3x-origins-tags.csv")


    #  outputting data to CSV
    #   format: eng1, x1, y1, z1, eng2, x2, y2, z2, (eng3, x3, y3, z3) <- if triple scatter
    #   units: time in us and pos in mm
    if STORE_SINGLE_SCATTERS == True:
        #  1x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_out), 'scatters_1x_out', output_file_1x))
        np.savetxt(output_file_1x, scatters_1x_out, delimiter=',', fmt='%.5f')

        if STORE_ORIGINS_TAGS == True:
            #  1x
            print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_1x_origins_tags_out), 'scatters_1x_origins_tags_out', output_file_1x_origins_tags))
            scatters_1x_origins_tags_out.to_csv(output_file_1x_origins_tags, index = False, header = False, float_format = '%.5f')

    #  2x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x))
    np.savetxt(output_file_2x, scatters_2x_out, delimiter=',', fmt='%.5f')
    #  3x
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_3x))
    np.savetxt(output_file_3x, scatters_3x_out, delimiter=',', fmt='%.5f')

    #  Store origin locations and scatter tags for 2x & 3x scatter events
    if STORE_ORIGINS_TAGS == True:

        #  2x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_origins_tags_out), 'scatters_2x_origins_tags_out', output_file_2x_origins_tags))
        scatters_2x_origins_tags_out.to_csv(output_file_2x_origins_tags, index = False, header = False, float_format = '%.5f')
        #  3x
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_origins_tags_out), 'scatters_3x_origins_tags_out', output_file_3x_origins_tags))
        scatters_3x_origins_tags_out.to_csv(output_file_3x_origins_tags, index = False, header = False, float_format = '%.5f')


    #  saving combined 2x and 3x data (using file.open in order to append data to file)
    print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_out), 'scatters_2x_out', output_file_2x_3x))
    ofile = open(output_file_2x_3x, "w")
    np.savetxt(ofile, scatters_2x_out, delimiter=',', fmt='%.5f')
    print ('   - saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_out), 'scatters_3x_out', output_file_2x_3x))
    np.savetxt(ofile, scatters_3x_out, delimiter=',', fmt='%.5f')
    ofile.close()

    #  Store origin locations and scatter tags for combined 2x and 3x data (using file.open in order to append data to file)
    if STORE_ORIGINS_TAGS == True:
        print ('  Saving {} events from data array ({}) to file (CSV format): {}'.format(len(scatters_2x_origins_tags_out), 'scatters_2x_origins_tags_out', output_file_2x_3x_origins_tags))
        ofile = open(output_file_2x_3x_origins_tags, "w")
        #np.savetxt(ofile, scatters_2x_origins_tags_out, delimiter=',', fmt='%.5f')
        np.savetxt(ofile, scatters_2x_origins_tags_out, fmt='%.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %s')
        print ('   - saving {} additional events from data array ({}) to file (CSV format): {}'.format(len(scatters_3x_origins_tags_out), 'scatters_3x_origins_tags_out', output_file_2x_3x_origins_tags))
        #np.savetxt(ofile, scatters_3x_origins_tags_out, delimiter=',', fmt='%.5f')
        np.savetxt(ofile, scatters_3x_origins_tags_out, fmt='%.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %s')
        ofile.close()


    # count final number of single/double/triple scatters
    num_ss_fi = len(scatters_1x_out);  num_ds_fi = len(scatters_2x_out);  num_ts_fi = len(scatters_3x_out)

    #  measuring time required to run code - stop time
    stop = timeit.default_timer()

    #  save run summary to file
    summary_file = output_file.replace(".txt", "").replace(".dat", "").replace(".csv", "") + "-summary.txt"
    f = open(summary_file, "w+")
    f.write ('--- Completed processing / time required {} s'.format(stop - start))
    f.write ('\n-------  SUMMARY  -------')
    f.write ('\n-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    f.write ('\n-- Number of 1x pixel events: {}, 2x pixel events: {} ({} double scatters), 3x pixel events: {} ({} triple scatters), 4x+ pixel events: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    f.write ('\n-- Multi-Stage interactions     -> Total: {}, 2x pixel interactions: {} ({} events), 3x pixel interactions: {} ({} events)'.format(num_ms_total, num_ms_2px, num_ms_2px/2, num_ms_3px, num_ms_3px/3))
    f.write ('\n-- Single Scatters Events       -> Final: {}'.format(num_ss))
    f.write ('\n-- Double Scatters Events       -> Final: {}'.format(num_ds))
    f.write ('\n-- Triple Scatters Events       -> Final: {}'.format(num_ts))
    f.write ('\n--------------------------')
    f.close()

    #  print out run summary to screen
    print ('\n--- Completed processing / time required {} s'.format(stop - start))
    print ('\n-------  SUMMARY  -------')
    print ('-- Total events: {} ({} interactions), in {} input files'.format((num_1px + (num_2px/2) + (num_3px/3) + (num_4px/4)), len(allevents_data.index), len(root_file_list)))
    print ('-- Number of 1x pixel interactions: {}, 2x pixel interactions: {} ({} double scatter events), 3x pixel interactions: {} ({} triple scatter events), 4x+ pixel interactions: {}'.format(num_1px, num_2px, num_2px/2, num_3px, num_3px/3, num_4px))
    print ('-- Multi-Stage interactions     -> Total: {}, 2x pixel interactions: {} ({} events), 3x pixel interactions: {} ({} events)'.format(num_ms_total, num_ms_2px, num_ms_2px/2, num_ms_3px, num_ms_3px/3))
    print ('-- Single Scatters Events       -> Final: {}'.format(num_ss))
    print ('-- Double Scatters Events       -> Final: {}'.format(num_ds))
    print ('-- Triple Scatters Events       -> Final: {}'.format(num_ts))
    print ('--------------------------')


#------------------------------------------------------------------
# SETTINGS / PARAMETERS
#------------------------------------------------------------------

### OUTPUT FILES
STORE_SINGLE_SCATTERS = False    # True (store -1x.csv files) / False (do not store, default), stores single-scatter data to file scatters_1x.csv
STORE_ORIGINS_TAGS = True       # True (store -origins-tags.csv files) / False (do not store, default), duplicate files for scatters_2x.csv which include origin location and scatter tags


#------------------------------------------------------------------
# MAIN PROGRAM
#------------------------------------------------------------------

def main():
    print ('\n--- Running scatterData_ROOT2CSV_multiFile.py ---')


    #### 180315 Data ####
    # ----------------- #
    # convert_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180315/180315-run7-cs137_at_05_-05_11mm-air-dbox-singles-single_multi-energyUncer-0.1-sourceWidth-1mm_020M/root/', 'scatterData')
    convert_data('/Users/stevepeterson/Desktop/rajesh/190925/root/', 'scatterData')
    # convert_data('/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180315/180315-run7-cs137_at_05_-05_11mm-air-dbox-doubles_triples-single_multi-only_primaries-zero_scatter-perfect_020M/root/', 'scatterData')



if __name__ == "__main__":

    main()
