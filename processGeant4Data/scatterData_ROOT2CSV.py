'''
The following code converts the reconData of the *.root file obtained from the POLARIS3.
into a *.csv file. It requires "uproot" preinstalled on the computer and may also require lz4.
If not already installed, these packages can be installed using:

$pip install uproot --user  OR  python -m pip uproot
$pip install lz4 --user     OR  python -m pip lz4
'''
#import ROOT
import uproot
import uproot._connect.to_pandas

### INPUT FILES
## 180618 Data ##
FILE_NAME = "/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180618/180618-run3-co60_-17_-11_11mm-singles_5M_060.1454067466341640.root"
#FILE_NAME = "/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180618/180618-run3-co60_-17_-11_11mm-singles_5M-total_25M.root"
TREE_NAME = "scatterData"

### OUTPUT FILES
## 180618 Data ##
OUT_FILE_NAME = "/Volumes/BATMAN/PGI/data/uct/polaris-montecarlo/180618/180618-run3-co60_-17_-11_11mm-singles_5M_060.1454067466341640.csv"

MyFile = uproot.open(FILE_NAME)
MyTree = MyFile[TREE_NAME]
#Display all the leaves of TREE_NAME
print('\n+ + Below are all the leaves of ' + '%s' %(TREE_NAME) +' tree: \n')
MyTree.show()
'''
print(MyTree.keys())
print('\n+ +\n')
'''
#Open file to write csv data
fout = open(OUT_FILE_NAME, 'w')

#Title of each column in the csv file (skipping detPos and detSize)
# (delete title row of csv file before using into reconstruction algorithm or comment out the following line)
fout.write("event,track,step,detector,process,x,y,z,gammaEnergy,energyDeposited,scatAng,origin_x,origin_y,origin_z,origin_energy,origin_process,origin_volume,scatter_tag \n")

print('\n\tProcessing data from:')
print('\t\t' '%s' % (FILE_NAME))

#Assign a variable to each leaf in the reconData tree
evt_leaf = "event"
trk_leaf = "track"
stp_leaf = "step"
det_leaf = "detector"
dpos_leaf = "detPos"
dsize_leaf = "detSize"
pro_leaf = "process"
xpos_leaf = "pos_x"
ypos_leaf = "pos_y"
zpos_leaf = "pos_z"
eng_leaf = "gammaEnergy"
dep_leaf = "energyDeposited"
ang_leaf = "scatAng"
xorg_leaf = "origin_x"
yorg_leaf = "origin_y"
zorg_leaf = "origin_z"
eorg_leaf = "origin_energy"
porg_leaf = "origin_process"
vorg_leaf = "origin_volume"
stag_leaf = "scatter_tag"

#Form an array for each leaf
evt = MyTree[evt_leaf].array()
trk = MyTree[trk_leaf].array()
stp = MyTree[stp_leaf].array()
det = MyTree[det_leaf].array()
dPos = MyTree[dpos_leaf].array()
dSize = MyTree[dsize_leaf].array()
pro = MyTree[pro_leaf].array()
xpos = MyTree[xpos_leaf].array()
ypos = MyTree[ypos_leaf].array()
zpos = MyTree[zpos_leaf].array()
gEng = MyTree[eng_leaf].array()
engD = MyTree[dep_leaf].array()
sAng = MyTree[ang_leaf].array()
xorg = MyTree[xorg_leaf].array()
yorg = MyTree[yorg_leaf].array()
zorg = MyTree[zorg_leaf].array()
eorg = MyTree[eorg_leaf].array()
porg = MyTree[porg_leaf].array()
vorg = MyTree[vorg_leaf].array()
stag = MyTree[stag_leaf].array()

print('  Number of Events: {}'.format(len(evt)))

# index = 4
# print(' Event: {}, Track: {}, Step: {}'.format(evt[index], trk[index], stp[index]))
# print(' Detector: {}, '.format(det[index]))
# print(' detPos: {}, '.format(dPos[index]))
# print(' detSize: {}, '.format(dSize[index]))
# print(' Process: {}, '.format(pro[index]))
# print(' XPos: {}, '.format(xpos[index]))
# print(' YPos: {}, '.format(ypos[index]))
# print(' ZPos: {}, '.format(zpos[index]))
# print(' gEng: {}, '.format(gEng[index]))
# print(' engD: {}, '.format(engD[index]))
# print(' sAng: {}, '.format(sAng[index]))
# print(' origin position: ({}, {}, {} mm)'.format(xorg[index], yorg[index], zorg[index]))
# print(' origin energy: {} MeV'.format(eorg[index]))
# print(' origin process: {}'.format(porg[index]))
# print(' origin volume: {}'.format(vorg[index]))
# print(' scatter tag: {}'.format(stag[index]))


# Write the formatted data
for i in range(len(evt)):
    for j in range(3):
        if (gEng[i][j] != 0):
            fout.write( "%i,%i,%i,%s,%s,%f,%f,%f," % (evt[i], trk[i], stp[i][j], det[i][j], pro[i][j], xpos[i][j], ypos[i][j], zpos[i][j]) )
            fout.write( "%f,%f,%f,%f,%f,%f,%f,%s,%s,%s\n" % (gEng[i][j], engD[i][j], sAng[i][j], xorg[i], yorg[i], zorg[i], eorg[i], porg[i], vorg[i], stag[i]) )
        # Prints an empty lines for single and double scatters
        else:
            fout.write( "%i,%i,%i,%s,%s,%f,%f,%f," % (evt[i], trk[i], stp[i][j], '', '', xpos[i][j], ypos[i][j], zpos[i][j]) )
            fout.write( "%f,%f,%f,%s,%s,%s,%s,%s,%s,%s\n" % (gEng[i][j], engD[i][j], sAng[i][j], '', '', '', '0', '', '', '') )


fout.close()
print('\n- - - Data has been saved to ' + '%s' %(OUT_FILE_NAME))
