'''
The following code converts the energyData of the *.root file obtained from the POLARIS3.
into a *.csv file. It requires "uproot" preinstalled on the computer and may also require lz4.
If not already installed, these packages can be installed using:

$pip install uproot --user
$pip install lz4 --user
'''
#import ROOT
import uproot
import uproot._connect.to_pandas

FILE_NAME = "/Volumes/BATMAN/Geant4/development/polaris/POLARIS3/190423-updating_tracking_detectorbox/001-build/junk_174.122056635210279.root"
TREE_NAME = "energyData"
OUT_FILE_NAME = "energyDataOut.csv"

MyFile = uproot.open(FILE_NAME)
MyTree = MyFile[TREE_NAME]
#Display all the leaves of TREE_NAME
print('\n+ + Below are all the leaves of ' + '%s' %(TREE_NAME) +' tree: \n')
MyTree.show()
'''
print(MyTree.keys())
print('\n+ +\n')
'''
#Open file to write csv data
fout = open(OUT_FILE_NAME, 'w')

#Title of each column in the csv file
# (delete title row of csv file before using into reconstruction algorithm or comment out the following line)
#fout.write("event,track,step,detector,process,x,y,z,gammaEnergy,energyDeposited,scatAng,origin_x,origin_y,origin_z,origin_energy,origin_process,origin_volume \n")
fout.write("event,detector,module,crystal,engDepTot,engDepEle,engDepGam,engDepNeu,engDepPro,engDepOth \n")

print('\n\tProcessing data from:')
print('\t\t' '%s' % (FILE_NAME))

#Assign a variable to each leaf in the reconData tree
evt_leaf = "event"
det_leaf = "detector"
mod_leaf = "module"
cry_leaf = "crystal"
edTot_leaf = "engDepTot"
edEle_leaf = "engDepEle"
edGam_leaf = "engDepGam"
edNeu_leaf = "engDepNeu"
edPro_leaf = "engDepPro"
edOth_leaf = "engDepOth"

#Form an array for each leaf
evt = MyTree[evt_leaf].array()
det = MyTree[det_leaf].array()
mod = MyTree[mod_leaf].array()
cry = MyTree[cry_leaf].array()
edt = MyTree[edTot_leaf].array()
ede = MyTree[edEle_leaf].array()
edg = MyTree[edGam_leaf].array()
edn = MyTree[edNeu_leaf].array()
edp = MyTree[edPro_leaf].array()
edo = MyTree[edOth_leaf].array()


print('  Number of Events: {}'.format(len(evt)))

# index = 4
# print(' Event: {}, Track: {}, Step: {}'.format(evt[index], trk[index], stp[index]))
# print(' Detector: {}, '.format(det[index]))
# print(' detPos: {}, '.format(dPos[index]))
# print(' detSize: {}, '.format(dSize[index]))
# print(' Process: {}, '.format(pro[index]))
# print(' XPos: {}, '.format(xpos[index]))
# print(' YPos: {}, '.format(ypos[index]))
# print(' ZPos: {}, '.format(zpos[index]))
# print(' gEng: {}, '.format(gEng[index]))
# print(' engD: {}, '.format(engD[index]))
# print(' sAng: {}, '.format(sAng[index]))
# print(' origin position: ({}, {}, {} mm)'.format(xorg[index], yorg[index], zorg[index]))
# print(' origin energy: {} MeV'.format(eorg[index]))
# print(' origin process: {}'.format(porg[index]))
# print(' origin volume: {}'.format(vorg[index]))


# Write the formatted data
for i in range(len(evt)):
    fout.write( "%i,%s,%i,%i,%f,%f,%f,%f,%f,%f\n" % (evt[i], det[i], mod[i], cry[i], edt[i], ede[i], edg[i], edn[i], edp[i], edo[i]) )


fout.close()
print('\n- - - Data has been saved to ' + '%s' %(OUT_FILE_NAME))
